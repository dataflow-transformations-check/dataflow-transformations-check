// __  ___ __ ___  _ __   ___  ___ 
// \ \/ / '__/ _ \| '_ \ / _ \/ __|
//  >  <| | | (_) | | | | (_) \__ \
// /_/\_\_|  \___/|_| |_|\___/|___/
// 
// Xronos synthesizer version
// Run date: Fri 23 Feb 2018 14:28:35 +0000
// 

module join_u5(in8_DATA, in15_COUNT, in16_COUNT, in1_COUNT, in4_ACK, in8_SEND, in11_SEND, in2_COUNT, in9_COUNT, in9_ACK, in5_ACK, in11_ACK, in8_ACK, in4_COUNT, in12_SEND, in2_ACK, in2_DATA, RESET, in14_SEND, in5_DATA, in8_COUNT, in13_DATA, in13_SEND, in13_ACK, in3_SEND, in3_COUNT, in3_ACK, in11_DATA, in2_SEND, in12_DATA, in10_COUNT, in12_ACK, in5_COUNT, in16_DATA, in15_ACK, in1_ACK, out1_ACK, in6_ACK, in3_DATA, in7_DATA, out1_SEND, in13_COUNT, in10_SEND, in14_COUNT, in10_ACK, in11_COUNT, in1_SEND, in16_ACK, in15_SEND, in4_SEND, in9_SEND, in7_COUNT, in9_DATA, in14_ACK, out1_DATA, in6_DATA, in1_DATA, out1_COUNT, in7_SEND, out1_RDY, in6_SEND, in6_COUNT, in5_SEND, in10_DATA, in7_ACK, CLK, in14_DATA, in16_SEND, in4_DATA, in12_COUNT, in15_DATA);
wire		fanIn6_done;
wire		fanIn13_done;
input	[7:0]	in8_DATA;
input	[15:0]	in15_COUNT;
wire		fanIn2_go;
wire		fanIn11_go;
input	[15:0]	in16_COUNT;
input	[15:0]	in1_COUNT;
output		in4_ACK;
input		in8_SEND;
wire		fanIn4_go;
input		in11_SEND;
input	[15:0]	in2_COUNT;
input	[15:0]	in9_COUNT;
output		in9_ACK;
output		in5_ACK;
wire		fanIn14_go;
output		in11_ACK;
wire		fanIn12_done;
output		in8_ACK;
wire		fanIn9_go;
input	[15:0]	in4_COUNT;
input		in12_SEND;
output		in2_ACK;
input	[7:0]	in2_DATA;
input		RESET;
input		in14_SEND;
wire		fanIn1_go;
input	[7:0]	in5_DATA;
input	[15:0]	in8_COUNT;
input	[7:0]	in13_DATA;
wire		fanIn16_go;
input		in13_SEND;
output		in13_ACK;
wire		fanIn9_done;
wire		fanIn14_done;
input		in3_SEND;
input	[15:0]	in3_COUNT;
output		in3_ACK;
input	[7:0]	in11_DATA;
input		in2_SEND;
input	[7:0]	in12_DATA;
wire		fanIn16_done;
input	[15:0]	in10_COUNT;
output		in12_ACK;
input	[15:0]	in5_COUNT;
input	[7:0]	in16_DATA;
output		in15_ACK;
output		in1_ACK;
input		out1_ACK;
wire		fanIn4_done;
wire		fanIn11_done;
output		in6_ACK;
input	[7:0]	in3_DATA;
input	[7:0]	in7_DATA;
output		out1_SEND;
input	[15:0]	in13_COUNT;
input		in10_SEND;
wire		fanIn12_go;
wire		fanIn3_go;
input	[15:0]	in14_COUNT;
output		in10_ACK;
input	[15:0]	in11_COUNT;
input		in1_SEND;
output		in16_ACK;
input		in15_SEND;
input		in4_SEND;
input		in9_SEND;
wire		fanIn6_go;
input	[15:0]	in7_COUNT;
wire		fanIn1_done;
input	[7:0]	in9_DATA;
wire		fanIn8_go;
output		in14_ACK;
wire		fanIn5_done;
output	[7:0]	out1_DATA;
wire		fanIn2_done;
wire		fanIn10_done;
input	[7:0]	in6_DATA;
input	[7:0]	in1_DATA;
output	[15:0]	out1_COUNT;
wire		fanIn7_go;
wire		fanIn3_done;
wire		fanIn10_go;
wire		fanIn13_go;
input		in7_SEND;
input		out1_RDY;
input		in6_SEND;
input	[15:0]	in6_COUNT;
input		in5_SEND;
input	[7:0]	in10_DATA;
output		in7_ACK;
wire		fanIn15_done;
input		CLK;
input	[7:0]	in14_DATA;
input		in16_SEND;
wire		fanIn7_done;
input	[7:0]	in4_DATA;
wire		fanIn15_go;
input	[15:0]	in12_COUNT;
wire		fanIn5_go;
wire		fanIn8_done;
input	[7:0]	in15_DATA;
wire	[15:0]	fanIn1;
wire		fanIn1_u9;
wire		fanIn1_u11;
wire		join_fanIn1_instance_DONE;
wire	[7:0]	fanIn1_u10;
wire		join_fanIn9_instance_DONE;
wire	[15:0]	fanIn9;
wire		fanIn9_u4;
wire	[7:0]	fanIn9_u3;
wire		fanIn9_u5;
wire		bus_4fc0fec9_;
wire		fanIn8_u5;
wire	[15:0]	fanIn8;
wire		join_fanIn8_instance_DONE;
wire	[7:0]	fanIn8_u4;
wire		fanIn8_u3;
wire		bus_002bc981_;
wire		bus_6cfffd61_;
wire	[15:0]	fanIn4;
wire		fanIn4_u11;
wire		join_fanIn4_instance_DONE;
wire	[7:0]	fanIn4_u9;
wire		fanIn4_u10;
wire		bus_7dbf85d1_;
wire		bus_2804a5e7_;
wire		bus_5a31b1d0_;
wire		bus_04753273_;
wire		or_30ed06ad_u0;
wire		bus_1792c60b_;
wire	[7:0]	fanIn14_u4;
wire		join_fanIn14_instance_DONE;
wire	[15:0]	fanIn14;
wire		fanIn14_u5;
wire		fanIn14_u3;
wire		bus_454dff98_;
wire		bus_05315b83_;
wire	[15:0]	fanIn10;
wire		join_fanIn10_instance_DONE;
wire	[7:0]	fanIn10_u3;
wire		fanIn10_u4;
wire		fanIn10_u5;
wire	[7:0]	fanIn6_u3;
wire		join_fanIn6_instance_DONE;
wire	[15:0]	fanIn6;
wire		fanIn6_u5;
wire		fanIn6_u4;
wire		scheduler_u800;
wire		scheduler_u805;
wire		scheduler_u807;
wire		scheduler_u803;
wire		scheduler;
wire		scheduler_u793;
wire		scheduler_u802;
wire	[3:0]	scheduler_u791;
wire		scheduler_u795;
wire		scheduler_u796;
wire		scheduler_u797;
wire		scheduler_u792;
wire		scheduler_u801;
wire		join_scheduler_instance_DONE;
wire		scheduler_u794;
wire		scheduler_u799;
wire		scheduler_u806;
wire		scheduler_u798;
wire		scheduler_u804;
wire	[15:0]	or_4b222f41_u0;
wire	[15:0]	fanIn13;
wire		fanIn13_u5;
wire		fanIn13_u3;
wire		join_fanIn13_instance_DONE;
wire	[7:0]	fanIn13_u4;
wire		bus_1c18c91a_;
wire	[15:0]	fanIn11;
wire		fanIn11_u5;
wire		join_fanIn11_instance_DONE;
wire	[7:0]	fanIn11_u4;
wire		fanIn11_u3;
wire		bus_06f3343d_;
wire	[3:0]	bus_451bcd25_;
wire		bus_58e92e53_;
wire		bus_02bcca43_;
wire	[7:0]	fanIn7_u4;
wire	[15:0]	fanIn7;
wire		fanIn7_u5;
wire		join_fanIn7_instance_DONE;
wire		fanIn7_u3;
wire	[7:0]	fanIn15_u4;
wire		join_fanIn15_instance_DONE;
wire	[15:0]	fanIn15_u3;
wire		fanIn15_u5;
wire		fanIn15;
wire	[15:0]	fanIn5_u3;
wire	[7:0]	fanIn5_u4;
wire		fanIn5_u5;
wire		fanIn5;
wire		join_fanIn5_instance_DONE;
wire	[15:0]	fanIn12_u3;
wire		join_fanIn12_instance_DONE;
wire		fanIn12_u5;
wire		fanIn12;
wire	[7:0]	fanIn12_u4;
wire		bus_5921aad1_;
wire	[15:0]	fanIn3;
wire		fanIn3_u9;
wire		join_fanIn3_instance_DONE;
wire		fanIn3_u11;
wire	[7:0]	fanIn3_u10;
wire	[7:0]	fanIn16_u4;
wire	[15:0]	fanIn16_u3;
wire		join_fanIn16_instance_DONE;
wire		fanIn16_u5;
wire		fanIn16;
wire		fanIn2_u10;
wire		fanIn2_u11;
wire	[15:0]	fanIn2;
wire		join_fanIn2_instance_DONE;
wire	[7:0]	fanIn2_u9;
wire		bus_415e9bcd_;
wire		bus_7143524c_;
wire		bus_2f3f2229_;
wire	[7:0]	or_681a48e6_u0;
assign fanIn6_done=bus_7dbf85d1_;
assign fanIn13_done=bus_2f3f2229_;
assign fanIn2_go=scheduler_u803;
assign fanIn11_go=scheduler_u804;
assign in4_ACK=fanIn4_u10;
assign fanIn4_go=scheduler_u792;
assign in9_ACK=fanIn9_u5;
assign in5_ACK=fanIn5;
assign fanIn14_go=scheduler_u799;
assign in11_ACK=fanIn11_u3;
assign fanIn12_done=bus_06f3343d_;
assign in8_ACK=fanIn8_u3;
assign fanIn9_go=scheduler_u806;
assign in2_ACK=fanIn2_u10;
assign fanIn1_go=scheduler_u793;
assign fanIn16_go=scheduler_u796;
assign in13_ACK=fanIn13_u3;
assign fanIn9_done=bus_454dff98_;
assign fanIn14_done=bus_5a31b1d0_;
assign in3_ACK=fanIn3_u9;
assign fanIn16_done=bus_1c18c91a_;
assign in12_ACK=fanIn12;
assign in15_ACK=fanIn15;
assign in1_ACK=fanIn1_u9;
assign fanIn4_done=bus_002bc981_;
assign fanIn11_done=bus_415e9bcd_;
assign in6_ACK=fanIn6_u4;
assign out1_SEND=or_30ed06ad_u0;
assign fanIn12_go=scheduler_u797;
assign fanIn3_go=scheduler_u798;
assign in10_ACK=fanIn10_u4;
assign in16_ACK=fanIn16;
assign fanIn6_go=scheduler_u794;
assign fanIn1_done=bus_1792c60b_;
assign fanIn8_go=scheduler_u802;
assign in14_ACK=fanIn14_u3;
assign fanIn5_done=bus_05315b83_;
assign out1_DATA=or_681a48e6_u0;
assign fanIn2_done=bus_5921aad1_;
assign fanIn10_done=bus_58e92e53_;
assign out1_COUNT=or_4b222f41_u0;
assign fanIn7_go=scheduler_u795;
assign fanIn3_done=bus_4fc0fec9_;
assign fanIn10_go=scheduler_u800;
assign fanIn13_go=scheduler_u805;
assign in7_ACK=fanIn7_u3;
assign fanIn15_done=bus_02bcca43_;
assign fanIn7_done=bus_2804a5e7_;
assign fanIn15_go=scheduler_u801;
assign fanIn5_go=scheduler_u807;
assign fanIn8_done=bus_04753273_;
join_fanIn1 join_fanIn1_instance(.CLK(CLK), .GO(fanIn1_go), .port_22416f29_(in1_DATA), 
  .DONE(join_fanIn1_instance_DONE), .RESULT(fanIn1), .RESULT_u2111(fanIn1_u9), 
  .RESULT_u2112(fanIn1_u10), .RESULT_u2113(fanIn1_u11));
join_fanIn9 join_fanIn9_instance(.CLK(CLK), .GO(fanIn9_go), .port_4d0acfe2_(in9_DATA), 
  .DONE(join_fanIn9_instance_DONE), .RESULT(fanIn9), .RESULT_u2114(fanIn9_u3), 
  .RESULT_u2115(fanIn9_u4), .RESULT_u2116(fanIn9_u5));
assign bus_4fc0fec9_=join_fanIn3_instance_DONE&{1{join_fanIn3_instance_DONE}};
join_fanIn8 join_fanIn8_instance(.CLK(CLK), .GO(fanIn8_go), .port_3cb43266_(in8_DATA), 
  .DONE(join_fanIn8_instance_DONE), .RESULT(fanIn8), .RESULT_u2117(fanIn8_u3), 
  .RESULT_u2118(fanIn8_u4), .RESULT_u2119(fanIn8_u5));
assign bus_002bc981_=join_fanIn4_instance_DONE&{1{join_fanIn4_instance_DONE}};
join_Kicker_55 join_Kicker_55_1(.CLK(CLK), .RESET(bus_7143524c_), .bus_6cfffd61_(bus_6cfffd61_));
join_fanIn4 join_fanIn4_instance(.CLK(CLK), .GO(fanIn4_go), .port_7285e2c7_(in4_DATA), 
  .DONE(join_fanIn4_instance_DONE), .RESULT(fanIn4), .RESULT_u2120(fanIn4_u9), 
  .RESULT_u2121(fanIn4_u10), .RESULT_u2122(fanIn4_u11));
assign bus_7dbf85d1_=join_fanIn6_instance_DONE&{1{join_fanIn6_instance_DONE}};
assign bus_2804a5e7_=join_fanIn7_instance_DONE&{1{join_fanIn7_instance_DONE}};
assign bus_5a31b1d0_=join_fanIn14_instance_DONE&{1{join_fanIn14_instance_DONE}};
assign bus_04753273_=join_fanIn8_instance_DONE&{1{join_fanIn8_instance_DONE}};
assign or_30ed06ad_u0=fanIn1_u11|fanIn2_u11|fanIn3_u11|fanIn4_u11|fanIn5_u5|fanIn6_u5|fanIn7_u5|fanIn8_u5|fanIn9_u4|fanIn10_u5|fanIn11_u5|fanIn12_u5|fanIn13_u5|fanIn14_u5|fanIn15_u5|fanIn16_u5;
assign bus_1792c60b_=join_fanIn1_instance_DONE&{1{join_fanIn1_instance_DONE}};
join_fanIn14 join_fanIn14_instance(.CLK(CLK), .GO(fanIn14_go), .port_575fe577_(in14_DATA), 
  .DONE(join_fanIn14_instance_DONE), .RESULT(fanIn14), .RESULT_u2123(fanIn14_u3), 
  .RESULT_u2124(fanIn14_u4), .RESULT_u2125(fanIn14_u5));
assign bus_454dff98_=join_fanIn9_instance_DONE&{1{join_fanIn9_instance_DONE}};
assign bus_05315b83_=join_fanIn5_instance_DONE&{1{join_fanIn5_instance_DONE}};
join_fanIn10 join_fanIn10_instance(.CLK(CLK), .GO(fanIn10_go), .port_44d86288_(in10_DATA), 
  .DONE(join_fanIn10_instance_DONE), .RESULT(fanIn10), .RESULT_u2126(fanIn10_u3), 
  .RESULT_u2127(fanIn10_u4), .RESULT_u2128(fanIn10_u5));
join_fanIn6 join_fanIn6_instance(.CLK(CLK), .GO(fanIn6_go), .port_0063347e_(in6_DATA), 
  .DONE(join_fanIn6_instance_DONE), .RESULT(fanIn6), .RESULT_u2129(fanIn6_u3), 
  .RESULT_u2130(fanIn6_u4), .RESULT_u2131(fanIn6_u5));
join_scheduler join_scheduler_instance(.CLK(CLK), .RESET(bus_7143524c_), .GO(bus_6cfffd61_), 
  .port_28bad23f_(bus_451bcd25_), .port_3bf82b87_(fanIn4_done), .port_6b671ec2_(fanIn11_done), 
  .port_32e6ae0a_(in14_SEND), .port_056dd8e6_(fanIn6_done), .port_0a4845c5_(fanIn13_done), 
  .port_4d65779c_(in10_SEND), .port_41b2255d_(fanIn3_done), .port_20a4c464_(in13_SEND), 
  .port_18e671ac_(in7_SEND), .port_04f3ed30_(out1_RDY), .port_164db26e_(fanIn9_done), 
  .port_1bdf2c61_(fanIn14_done), .port_33df9314_(in8_SEND), .port_2770715b_(in6_SEND), 
  .port_74abe77f_(in3_SEND), .port_62e4285d_(in1_SEND), .port_6e7a5501_(in5_SEND), 
  .port_382b33f1_(in11_SEND), .port_4f59f80d_(in15_SEND), .port_279b1a82_(in4_SEND), 
  .port_6f3435ef_(in9_SEND), .port_269445b1_(fanIn15_done), .port_6ee6bb59_(in2_SEND), 
  .port_13dedf6b_(fanIn16_done), .port_73fefad5_(in16_SEND), .port_1c9884e7_(fanIn1_done), 
  .port_7d4facf3_(fanIn7_done), .port_1d018034_(fanIn12_done), .port_7ae7204b_(fanIn5_done), 
  .port_1abf2536_(in12_SEND), .port_5a4b7221_(fanIn8_done), .port_4249ce07_(fanIn2_done), 
  .port_35e8f828_(fanIn10_done), .DONE(join_scheduler_instance_DONE), .RESULT(scheduler), 
  .RESULT_u2132(scheduler_u791), .RESULT_u2133(scheduler_u792), .RESULT_u2134(scheduler_u793), 
  .RESULT_u2135(scheduler_u794), .RESULT_u2136(scheduler_u795), .RESULT_u2137(scheduler_u796), 
  .RESULT_u2138(scheduler_u797), .RESULT_u2139(scheduler_u798), .RESULT_u2140(scheduler_u799), 
  .RESULT_u2141(scheduler_u800), .RESULT_u2142(scheduler_u801), .RESULT_u2143(scheduler_u802), 
  .RESULT_u2144(scheduler_u803), .RESULT_u2145(scheduler_u804), .RESULT_u2146(scheduler_u805), 
  .RESULT_u2147(scheduler_u806), .RESULT_u2148(scheduler_u807));
assign or_4b222f41_u0=fanIn1|fanIn2|fanIn3|fanIn4|fanIn5_u3|fanIn6|fanIn7|fanIn8|fanIn9|fanIn10|fanIn11|fanIn12_u3|fanIn13|fanIn14|fanIn15_u3|fanIn16_u3;
join_fanIn13 join_fanIn13_instance(.CLK(CLK), .GO(fanIn13_go), .port_0539d915_(in13_DATA), 
  .DONE(join_fanIn13_instance_DONE), .RESULT(fanIn13), .RESULT_u2149(fanIn13_u3), 
  .RESULT_u2150(fanIn13_u4), .RESULT_u2151(fanIn13_u5));
assign bus_1c18c91a_=join_fanIn16_instance_DONE&{1{join_fanIn16_instance_DONE}};
join_fanIn11 join_fanIn11_instance(.CLK(CLK), .GO(fanIn11_go), .port_2b05df54_(in11_DATA), 
  .DONE(join_fanIn11_instance_DONE), .RESULT(fanIn11), .RESULT_u2152(fanIn11_u3), 
  .RESULT_u2153(fanIn11_u4), .RESULT_u2154(fanIn11_u5));
assign bus_06f3343d_=join_fanIn12_instance_DONE&{1{join_fanIn12_instance_DONE}};
join_stateVar_fsmState_join join_stateVar_fsmState_join_1(.bus_36267731_(CLK), 
  .bus_63ac18a2_(bus_7143524c_), .bus_17d2639c_(scheduler), .bus_11a2624b_(scheduler_u791), 
  .bus_451bcd25_(bus_451bcd25_));
assign bus_58e92e53_=join_fanIn10_instance_DONE&{1{join_fanIn10_instance_DONE}};
assign bus_02bcca43_=join_fanIn15_instance_DONE&{1{join_fanIn15_instance_DONE}};
join_fanIn7 join_fanIn7_instance(.CLK(CLK), .GO(fanIn7_go), .port_0607a328_(in7_DATA), 
  .DONE(join_fanIn7_instance_DONE), .RESULT(fanIn7), .RESULT_u2155(fanIn7_u3), 
  .RESULT_u2156(fanIn7_u4), .RESULT_u2157(fanIn7_u5));
join_fanIn15 join_fanIn15_instance(.CLK(CLK), .GO(fanIn15_go), .port_53ccfeb0_(in15_DATA), 
  .DONE(join_fanIn15_instance_DONE), .RESULT(fanIn15), .RESULT_u2158(fanIn15_u3), 
  .RESULT_u2159(fanIn15_u4), .RESULT_u2160(fanIn15_u5));
join_fanIn5 join_fanIn5_instance(.CLK(CLK), .GO(fanIn5_go), .port_5a46b6cb_(in5_DATA), 
  .DONE(join_fanIn5_instance_DONE), .RESULT(fanIn5), .RESULT_u2161(fanIn5_u3), 
  .RESULT_u2162(fanIn5_u4), .RESULT_u2163(fanIn5_u5));
join_fanIn12 join_fanIn12_instance(.CLK(CLK), .GO(fanIn12_go), .port_4e987517_(in12_DATA), 
  .DONE(join_fanIn12_instance_DONE), .RESULT(fanIn12), .RESULT_u2164(fanIn12_u3), 
  .RESULT_u2165(fanIn12_u4), .RESULT_u2166(fanIn12_u5));
assign bus_5921aad1_=join_fanIn2_instance_DONE&{1{join_fanIn2_instance_DONE}};
join_fanIn3 join_fanIn3_instance(.CLK(CLK), .GO(fanIn3_go), .port_64b08ec5_(in3_DATA), 
  .DONE(join_fanIn3_instance_DONE), .RESULT(fanIn3), .RESULT_u2167(fanIn3_u9), 
  .RESULT_u2168(fanIn3_u10), .RESULT_u2169(fanIn3_u11));
join_fanIn16 join_fanIn16_instance(.CLK(CLK), .GO(fanIn16_go), .port_2eadda5f_(in16_DATA), 
  .DONE(join_fanIn16_instance_DONE), .RESULT(fanIn16), .RESULT_u2170(fanIn16_u3), 
  .RESULT_u2171(fanIn16_u4), .RESULT_u2172(fanIn16_u5));
join_fanIn2 join_fanIn2_instance(.CLK(CLK), .GO(fanIn2_go), .port_6154e88d_(in2_DATA), 
  .DONE(join_fanIn2_instance_DONE), .RESULT(fanIn2), .RESULT_u2173(fanIn2_u9), 
  .RESULT_u2174(fanIn2_u10), .RESULT_u2175(fanIn2_u11));
assign bus_415e9bcd_=join_fanIn11_instance_DONE&{1{join_fanIn11_instance_DONE}};
join_globalreset_physical_53032fd2_ join_globalreset_physical_53032fd2__1(.bus_5db0a9c7_(CLK), 
  .bus_64ddf646_(RESET), .bus_7143524c_(bus_7143524c_));
assign bus_2f3f2229_=join_fanIn13_instance_DONE&{1{join_fanIn13_instance_DONE}};
assign or_681a48e6_u0=fanIn1_u10|fanIn2_u9|fanIn3_u10|fanIn4_u9|fanIn5_u4|fanIn6_u3|fanIn7_u4|fanIn8_u4|fanIn9_u3|fanIn10_u3|fanIn11_u4|fanIn12_u4|fanIn13_u4|fanIn14_u4|fanIn15_u4|fanIn16_u4;
endmodule



module join_fanIn1(CLK, GO, port_22416f29_, RESULT, RESULT_u2111, RESULT_u2112, RESULT_u2113, DONE);
input		CLK;
input		GO;
input	[7:0]	port_22416f29_;
output	[15:0]	RESULT;
output		RESULT_u2111;
output	[7:0]	RESULT_u2112;
output		RESULT_u2113;
output		DONE;
wire		simplePinWrite;
wire		simplePinWrite_u551;
wire	[7:0]	simplePinWrite_u552;
wire	[15:0]	simplePinWrite_u553;
assign simplePinWrite=GO&{1{GO}};
assign simplePinWrite_u551=GO&{1{GO}};
assign simplePinWrite_u552=port_22416f29_&{8{GO}};
assign simplePinWrite_u553=16'h1&{16{1'h1}};
assign RESULT=simplePinWrite_u553;
assign RESULT_u2111=simplePinWrite;
assign RESULT_u2112=simplePinWrite_u552;
assign RESULT_u2113=simplePinWrite_u551;
assign DONE=GO;
endmodule



module join_fanIn9(CLK, GO, port_4d0acfe2_, RESULT, RESULT_u2114, RESULT_u2115, RESULT_u2116, DONE);
input		CLK;
input		GO;
input	[7:0]	port_4d0acfe2_;
output	[15:0]	RESULT;
output	[7:0]	RESULT_u2114;
output		RESULT_u2115;
output		RESULT_u2116;
output		DONE;
wire		simplePinWrite;
wire	[7:0]	simplePinWrite_u554;
wire	[15:0]	simplePinWrite_u555;
wire		simplePinWrite_u556;
assign simplePinWrite=GO&{1{GO}};
assign simplePinWrite_u554=port_4d0acfe2_&{8{GO}};
assign simplePinWrite_u555=16'h1&{16{1'h1}};
assign simplePinWrite_u556=GO&{1{GO}};
assign RESULT=simplePinWrite_u555;
assign RESULT_u2114=simplePinWrite_u554;
assign RESULT_u2115=simplePinWrite_u556;
assign RESULT_u2116=simplePinWrite;
assign DONE=GO;
endmodule



module join_fanIn8(CLK, GO, port_3cb43266_, RESULT, RESULT_u2117, RESULT_u2118, RESULT_u2119, DONE);
input		CLK;
input		GO;
input	[7:0]	port_3cb43266_;
output	[15:0]	RESULT;
output		RESULT_u2117;
output	[7:0]	RESULT_u2118;
output		RESULT_u2119;
output		DONE;
wire		simplePinWrite;
wire		simplePinWrite_u557;
wire	[15:0]	simplePinWrite_u558;
wire	[7:0]	simplePinWrite_u559;
assign simplePinWrite=GO&{1{GO}};
assign simplePinWrite_u557=GO&{1{GO}};
assign simplePinWrite_u558=16'h1&{16{1'h1}};
assign simplePinWrite_u559=port_3cb43266_&{8{GO}};
assign RESULT=simplePinWrite_u558;
assign RESULT_u2117=simplePinWrite;
assign RESULT_u2118=simplePinWrite_u559;
assign RESULT_u2119=simplePinWrite_u557;
assign DONE=GO;
endmodule



module join_Kicker_55(CLK, RESET, bus_6cfffd61_);
input		CLK;
input		RESET;
output		bus_6cfffd61_;
wire		bus_46fb78e5_;
wire		bus_0bc88b30_;
wire		bus_7bfc87e6_;
wire		bus_5529419a_;
reg		kicker_res=1'h0;
reg		kicker_1=1'h0;
reg		kicker_2=1'h0;
assign bus_46fb78e5_=kicker_1&bus_7bfc87e6_&bus_0bc88b30_;
assign bus_0bc88b30_=~kicker_2;
assign bus_7bfc87e6_=~RESET;
assign bus_5529419a_=bus_7bfc87e6_&kicker_1;
always @(posedge CLK)
begin
kicker_res<=bus_46fb78e5_;
end
assign bus_6cfffd61_=kicker_res;
always @(posedge CLK)
begin
kicker_1<=bus_7bfc87e6_;
end
always @(posedge CLK)
begin
kicker_2<=bus_5529419a_;
end
endmodule



module join_fanIn4(CLK, GO, port_7285e2c7_, RESULT, RESULT_u2120, RESULT_u2121, RESULT_u2122, DONE);
input		CLK;
input		GO;
input	[7:0]	port_7285e2c7_;
output	[15:0]	RESULT;
output	[7:0]	RESULT_u2120;
output		RESULT_u2121;
output		RESULT_u2122;
output		DONE;
wire		simplePinWrite;
wire	[15:0]	simplePinWrite_u560;
wire	[7:0]	simplePinWrite_u561;
wire		simplePinWrite_u562;
assign simplePinWrite=GO&{1{GO}};
assign simplePinWrite_u560=16'h1&{16{1'h1}};
assign simplePinWrite_u561=port_7285e2c7_&{8{GO}};
assign simplePinWrite_u562=GO&{1{GO}};
assign RESULT=simplePinWrite_u560;
assign RESULT_u2120=simplePinWrite_u561;
assign RESULT_u2121=simplePinWrite;
assign RESULT_u2122=simplePinWrite_u562;
assign DONE=GO;
endmodule



module join_fanIn14(CLK, GO, port_575fe577_, RESULT, RESULT_u2123, RESULT_u2124, RESULT_u2125, DONE);
input		CLK;
input		GO;
input	[7:0]	port_575fe577_;
output	[15:0]	RESULT;
output		RESULT_u2123;
output	[7:0]	RESULT_u2124;
output		RESULT_u2125;
output		DONE;
wire		simplePinWrite;
wire	[15:0]	simplePinWrite_u563;
wire	[7:0]	simplePinWrite_u564;
wire		simplePinWrite_u565;
assign simplePinWrite=GO&{1{GO}};
assign simplePinWrite_u563=16'h1&{16{1'h1}};
assign simplePinWrite_u564=port_575fe577_&{8{GO}};
assign simplePinWrite_u565=GO&{1{GO}};
assign RESULT=simplePinWrite_u563;
assign RESULT_u2123=simplePinWrite;
assign RESULT_u2124=simplePinWrite_u564;
assign RESULT_u2125=simplePinWrite_u565;
assign DONE=GO;
endmodule



module join_fanIn10(CLK, GO, port_44d86288_, RESULT, RESULT_u2126, RESULT_u2127, RESULT_u2128, DONE);
input		CLK;
input		GO;
input	[7:0]	port_44d86288_;
output	[15:0]	RESULT;
output	[7:0]	RESULT_u2126;
output		RESULT_u2127;
output		RESULT_u2128;
output		DONE;
wire		simplePinWrite;
wire	[7:0]	simplePinWrite_u566;
wire	[15:0]	simplePinWrite_u567;
wire		simplePinWrite_u568;
assign simplePinWrite=GO&{1{GO}};
assign simplePinWrite_u566=port_44d86288_&{8{GO}};
assign simplePinWrite_u567=16'h1&{16{1'h1}};
assign simplePinWrite_u568=GO&{1{GO}};
assign RESULT=simplePinWrite_u567;
assign RESULT_u2126=simplePinWrite_u566;
assign RESULT_u2127=simplePinWrite;
assign RESULT_u2128=simplePinWrite_u568;
assign DONE=GO;
endmodule



module join_fanIn6(CLK, GO, port_0063347e_, RESULT, RESULT_u2129, RESULT_u2130, RESULT_u2131, DONE);
input		CLK;
input		GO;
input	[7:0]	port_0063347e_;
output	[15:0]	RESULT;
output	[7:0]	RESULT_u2129;
output		RESULT_u2130;
output		RESULT_u2131;
output		DONE;
wire		simplePinWrite;
wire	[7:0]	simplePinWrite_u569;
wire	[15:0]	simplePinWrite_u570;
wire		simplePinWrite_u571;
assign simplePinWrite=GO&{1{GO}};
assign simplePinWrite_u569=port_0063347e_&{8{GO}};
assign simplePinWrite_u570=16'h1&{16{1'h1}};
assign simplePinWrite_u571=GO&{1{GO}};
assign RESULT=simplePinWrite_u570;
assign RESULT_u2129=simplePinWrite_u569;
assign RESULT_u2130=simplePinWrite;
assign RESULT_u2131=simplePinWrite_u571;
assign DONE=GO;
endmodule



module join_scheduler(CLK, RESET, GO, port_28bad23f_, port_3bf82b87_, port_6b671ec2_, port_32e6ae0a_, port_056dd8e6_, port_0a4845c5_, port_4d65779c_, port_41b2255d_, port_20a4c464_, port_18e671ac_, port_04f3ed30_, port_164db26e_, port_1bdf2c61_, port_33df9314_, port_2770715b_, port_74abe77f_, port_62e4285d_, port_6e7a5501_, port_382b33f1_, port_4f59f80d_, port_279b1a82_, port_6f3435ef_, port_269445b1_, port_6ee6bb59_, port_13dedf6b_, port_73fefad5_, port_1c9884e7_, port_7d4facf3_, port_1d018034_, port_7ae7204b_, port_1abf2536_, port_5a4b7221_, port_4249ce07_, port_35e8f828_, RESULT, RESULT_u2132, RESULT_u2133, RESULT_u2134, RESULT_u2135, RESULT_u2136, RESULT_u2137, RESULT_u2138, RESULT_u2139, RESULT_u2140, RESULT_u2141, RESULT_u2142, RESULT_u2143, RESULT_u2144, RESULT_u2145, RESULT_u2146, RESULT_u2147, RESULT_u2148, DONE);
input		CLK;
input		RESET;
input		GO;
input	[3:0]	port_28bad23f_;
input		port_3bf82b87_;
input		port_6b671ec2_;
input		port_32e6ae0a_;
input		port_056dd8e6_;
input		port_0a4845c5_;
input		port_4d65779c_;
input		port_41b2255d_;
input		port_20a4c464_;
input		port_18e671ac_;
input		port_04f3ed30_;
input		port_164db26e_;
input		port_1bdf2c61_;
input		port_33df9314_;
input		port_2770715b_;
input		port_74abe77f_;
input		port_62e4285d_;
input		port_6e7a5501_;
input		port_382b33f1_;
input		port_4f59f80d_;
input		port_279b1a82_;
input		port_6f3435ef_;
input		port_269445b1_;
input		port_6ee6bb59_;
input		port_13dedf6b_;
input		port_73fefad5_;
input		port_1c9884e7_;
input		port_7d4facf3_;
input		port_1d018034_;
input		port_7ae7204b_;
input		port_1abf2536_;
input		port_5a4b7221_;
input		port_4249ce07_;
input		port_35e8f828_;
output		RESULT;
output	[3:0]	RESULT_u2132;
output		RESULT_u2133;
output		RESULT_u2134;
output		RESULT_u2135;
output		RESULT_u2136;
output		RESULT_u2137;
output		RESULT_u2138;
output		RESULT_u2139;
output		RESULT_u2140;
output		RESULT_u2141;
output		RESULT_u2142;
output		RESULT_u2143;
output		RESULT_u2144;
output		RESULT_u2145;
output		RESULT_u2146;
output		RESULT_u2147;
output		RESULT_u2148;
output		DONE;
reg		reg_36cffcd9_u0=1'h0;
wire signed	[31:0]	equals_b_signed;
wire		equals;
wire signed	[31:0]	equals_a_signed;
wire		not_u816_u0;
wire		and_u3553_u0;
wire		and_u3554_u0;
wire		not_u817_u0;
wire		and_u3555_u0;
wire		and_u3556_u0;
wire		and_u3557_u0;
wire		not_u818_u0;
wire		and_u3558_u0;
wire		simplePinWrite;
wire		and_u3559_u0;
wire		and_u3560_u0;
wire		and_u3561_u0;
wire signed	[31:0]	equals_u215_b_signed;
wire signed	[31:0]	equals_u215_a_signed;
wire		equals_u215;
wire		and_u3562_u0;
wire		and_u3563_u0;
wire		not_u819_u0;
wire		and_u3564_u0;
wire		not_u820_u0;
wire		and_u3565_u0;
wire		and_u3566_u0;
wire		and_u3567_u0;
wire		not_u821_u0;
wire		simplePinWrite_u572;
wire		and_u3568_u0;
wire		and_u3569_u0;
wire		and_u3570_u0;
wire		equals_u216;
wire signed	[31:0]	equals_u216_a_signed;
wire signed	[31:0]	equals_u216_b_signed;
wire		not_u822_u0;
wire		and_u3571_u0;
wire		and_u3572_u0;
wire		not_u823_u0;
wire		and_u3573_u0;
wire		and_u3574_u0;
wire		not_u824_u0;
wire		and_u3575_u0;
wire		and_u3576_u0;
wire		simplePinWrite_u573;
wire		and_u3577_u0;
wire		and_u3578_u0;
wire		and_u3579_u0;
wire signed	[31:0]	equals_u217_b_signed;
wire		equals_u217;
wire signed	[31:0]	equals_u217_a_signed;
wire		and_u3580_u0;
wire		and_u3581_u0;
wire		not_u825_u0;
wire		not_u826_u0;
wire		and_u3582_u0;
wire		and_u3583_u0;
wire		not_u827_u0;
wire		and_u3584_u0;
wire		and_u3585_u0;
wire		simplePinWrite_u574;
wire		and_u3586_u0;
wire		and_u3587_u0;
wire		and_u3588_u0;
wire		equals_u218;
wire signed	[31:0]	equals_u218_a_signed;
wire signed	[31:0]	equals_u218_b_signed;
wire		and_u3589_u0;
wire		and_u3590_u0;
wire		not_u828_u0;
wire		and_u3591_u0;
wire		not_u829_u0;
wire		and_u3592_u0;
wire		not_u830_u0;
wire		and_u3593_u0;
wire		and_u3594_u0;
wire		simplePinWrite_u575;
wire		and_u3595_u0;
wire		and_u3596_u0;
wire		and_u3597_u0;
wire signed	[31:0]	equals_u219_b_signed;
wire signed	[31:0]	equals_u219_a_signed;
wire		equals_u219;
wire		not_u831_u0;
wire		and_u3598_u0;
wire		and_u3599_u0;
wire		and_u3600_u0;
wire		and_u3601_u0;
wire		not_u832_u0;
wire		and_u3602_u0;
wire		not_u833_u0;
wire		and_u3603_u0;
wire		simplePinWrite_u576;
wire		and_u3604_u0;
wire		and_u3605_u0;
wire		and_u3606_u0;
wire signed	[31:0]	equals_u220_b_signed;
wire		equals_u220;
wire signed	[31:0]	equals_u220_a_signed;
wire		not_u834_u0;
wire		and_u3607_u0;
wire		and_u3608_u0;
wire		and_u3609_u0;
wire		and_u3610_u0;
wire		not_u835_u0;
wire		and_u3611_u0;
wire		and_u3612_u0;
wire		not_u836_u0;
wire		simplePinWrite_u577;
wire		and_u3613_u0;
wire		and_u3614_u0;
wire		and_u3615_u0;
wire signed	[31:0]	equals_u221_a_signed;
wire signed	[31:0]	equals_u221_b_signed;
wire		equals_u221;
wire		and_u3616_u0;
wire		and_u3617_u0;
wire		not_u837_u0;
wire		and_u3618_u0;
wire		and_u3619_u0;
wire		not_u838_u0;
wire		and_u3620_u0;
wire		not_u839_u0;
wire		and_u3621_u0;
wire		simplePinWrite_u578;
wire		and_u3622_u0;
wire		and_u3623_u0;
wire		and_u3624_u0;
wire signed	[31:0]	equals_u222_b_signed;
wire		equals_u222;
wire signed	[31:0]	equals_u222_a_signed;
wire		and_u3625_u0;
wire		not_u840_u0;
wire		and_u3626_u0;
wire		and_u3627_u0;
wire		and_u3628_u0;
wire		not_u841_u0;
wire		not_u842_u0;
wire		and_u3629_u0;
wire		and_u3630_u0;
wire		simplePinWrite_u579;
wire		and_u3631_u0;
wire		and_u3632_u0;
wire		and_u3633_u0;
wire signed	[31:0]	equals_u223_a_signed;
wire		equals_u223;
wire signed	[31:0]	equals_u223_b_signed;
wire		not_u843_u0;
wire		and_u3634_u0;
wire		and_u3635_u0;
wire		and_u3636_u0;
wire		not_u844_u0;
wire		and_u3637_u0;
wire		and_u3638_u0;
wire		and_u3639_u0;
wire		not_u845_u0;
wire		simplePinWrite_u580;
wire		and_u3640_u0;
wire		and_u3641_u0;
wire		and_u3642_u0;
wire signed	[31:0]	equals_u224_a_signed;
wire		equals_u224;
wire signed	[31:0]	equals_u224_b_signed;
wire		not_u846_u0;
wire		and_u3643_u0;
wire		and_u3644_u0;
wire		and_u3645_u0;
wire		and_u3646_u0;
wire		not_u847_u0;
wire		not_u848_u0;
wire		and_u3647_u0;
wire		and_u3648_u0;
wire		simplePinWrite_u581;
wire		and_u3649_u0;
wire		and_u3650_u0;
wire		and_u3651_u0;
wire signed	[31:0]	equals_u225_a_signed;
wire signed	[31:0]	equals_u225_b_signed;
wire		equals_u225;
wire		not_u849_u0;
wire		and_u3652_u0;
wire		and_u3653_u0;
wire		not_u850_u0;
wire		and_u3654_u0;
wire		and_u3655_u0;
wire		and_u3656_u0;
wire		and_u3657_u0;
wire		not_u851_u0;
wire		simplePinWrite_u582;
wire		and_u3658_u0;
wire		and_u3659_u0;
wire		and_u3660_u0;
wire signed	[31:0]	equals_u226_a_signed;
wire signed	[31:0]	equals_u226_b_signed;
wire		equals_u226;
wire		not_u852_u0;
wire		and_u3661_u0;
wire		and_u3662_u0;
wire		and_u3663_u0;
wire		not_u853_u0;
wire		and_u3664_u0;
wire		and_u3665_u0;
wire		and_u3666_u0;
wire		not_u854_u0;
wire		simplePinWrite_u583;
wire		and_u3667_u0;
wire		and_u3668_u0;
wire		and_u3669_u0;
wire signed	[31:0]	equals_u227_b_signed;
wire		equals_u227;
wire signed	[31:0]	equals_u227_a_signed;
wire		and_u3670_u0;
wire		not_u855_u0;
wire		and_u3671_u0;
wire		and_u3672_u0;
wire		not_u856_u0;
wire		and_u3673_u0;
wire		not_u857_u0;
wire		and_u3674_u0;
wire		and_u3675_u0;
wire		simplePinWrite_u584;
wire		and_u3676_u0;
wire		and_u3677_u0;
wire		and_u3678_u0;
wire signed	[31:0]	equals_u228_a_signed;
wire		equals_u228;
wire signed	[31:0]	equals_u228_b_signed;
wire		and_u3679_u0;
wire		and_u3680_u0;
wire		not_u858_u0;
wire		and_u3681_u0;
wire		and_u3682_u0;
wire		not_u859_u0;
wire		not_u860_u0;
wire		and_u3683_u0;
wire		and_u3684_u0;
wire		simplePinWrite_u585;
wire		and_u3685_u0;
wire		and_u3686_u0;
wire		and_u3687_u0;
wire signed	[31:0]	equals_u229_b_signed;
wire signed	[31:0]	equals_u229_a_signed;
wire		equals_u229;
wire		and_u3688_u0;
wire		and_u3689_u0;
wire		not_u861_u0;
wire		and_u3690_u0;
wire		and_u3691_u0;
wire		not_u862_u0;
wire		and_u3692_u0;
wire		and_u3693_u0;
wire		not_u863_u0;
wire		simplePinWrite_u586;
wire		and_u3694_u0;
wire		and_u3695_u0;
wire		and_u3696_u0;
wire	[3:0]	mux_u1864;
wire		or_u1405_u0;
wire		and_u3697_u0;
wire		or_u1406_u0;
reg		reg_6bac3bb4_u0=1'h0;
reg		reg_403f364b_u0=1'h0;
wire		or_u1407_u0;
wire	[3:0]	mux_u1865_u0;
always @(posedge CLK or posedge RESET)
begin
if (RESET)
reg_36cffcd9_u0<=1'h0;
else reg_36cffcd9_u0<=and_u3697_u0;
end
assign equals_a_signed={28'b0, port_28bad23f_};
assign equals_b_signed=32'h0;
assign equals=equals_a_signed==equals_b_signed;
assign not_u816_u0=~equals;
assign and_u3553_u0=and_u3697_u0&equals;
assign and_u3554_u0=and_u3697_u0&not_u816_u0;
assign not_u817_u0=~port_62e4285d_;
assign and_u3555_u0=and_u3561_u0&port_62e4285d_;
assign and_u3556_u0=and_u3561_u0&not_u817_u0;
assign and_u3557_u0=and_u3560_u0&port_04f3ed30_;
assign not_u818_u0=~port_04f3ed30_;
assign and_u3558_u0=and_u3560_u0&not_u818_u0;
assign simplePinWrite=and_u3559_u0&{1{and_u3559_u0}};
assign and_u3559_u0=and_u3557_u0&and_u3560_u0;
assign and_u3560_u0=and_u3555_u0&and_u3561_u0;
assign and_u3561_u0=and_u3553_u0&and_u3697_u0;
assign equals_u215_a_signed={28'b0, port_28bad23f_};
assign equals_u215_b_signed=32'h1;
assign equals_u215=equals_u215_a_signed==equals_u215_b_signed;
assign and_u3562_u0=and_u3697_u0&equals_u215;
assign and_u3563_u0=and_u3697_u0&not_u819_u0;
assign not_u819_u0=~equals_u215;
assign and_u3564_u0=and_u3570_u0&port_6ee6bb59_;
assign not_u820_u0=~port_6ee6bb59_;
assign and_u3565_u0=and_u3570_u0&not_u820_u0;
assign and_u3566_u0=and_u3569_u0&port_04f3ed30_;
assign and_u3567_u0=and_u3569_u0&not_u821_u0;
assign not_u821_u0=~port_04f3ed30_;
assign simplePinWrite_u572=and_u3568_u0&{1{and_u3568_u0}};
assign and_u3568_u0=and_u3566_u0&and_u3569_u0;
assign and_u3569_u0=and_u3564_u0&and_u3570_u0;
assign and_u3570_u0=and_u3562_u0&and_u3697_u0;
assign equals_u216_a_signed={28'b0, port_28bad23f_};
assign equals_u216_b_signed=32'h2;
assign equals_u216=equals_u216_a_signed==equals_u216_b_signed;
assign not_u822_u0=~equals_u216;
assign and_u3571_u0=and_u3697_u0&not_u822_u0;
assign and_u3572_u0=and_u3697_u0&equals_u216;
assign not_u823_u0=~port_382b33f1_;
assign and_u3573_u0=and_u3579_u0&port_382b33f1_;
assign and_u3574_u0=and_u3579_u0&not_u823_u0;
assign not_u824_u0=~port_04f3ed30_;
assign and_u3575_u0=and_u3578_u0&not_u824_u0;
assign and_u3576_u0=and_u3578_u0&port_04f3ed30_;
assign simplePinWrite_u573=and_u3577_u0&{1{and_u3577_u0}};
assign and_u3577_u0=and_u3576_u0&and_u3578_u0;
assign and_u3578_u0=and_u3573_u0&and_u3579_u0;
assign and_u3579_u0=and_u3572_u0&and_u3697_u0;
assign equals_u217_a_signed={28'b0, port_28bad23f_};
assign equals_u217_b_signed=32'h3;
assign equals_u217=equals_u217_a_signed==equals_u217_b_signed;
assign and_u3580_u0=and_u3697_u0&not_u825_u0;
assign and_u3581_u0=and_u3697_u0&equals_u217;
assign not_u825_u0=~equals_u217;
assign not_u826_u0=~port_1abf2536_;
assign and_u3582_u0=and_u3588_u0&not_u826_u0;
assign and_u3583_u0=and_u3588_u0&port_1abf2536_;
assign not_u827_u0=~port_04f3ed30_;
assign and_u3584_u0=and_u3587_u0&port_04f3ed30_;
assign and_u3585_u0=and_u3587_u0&not_u827_u0;
assign simplePinWrite_u574=and_u3586_u0&{1{and_u3586_u0}};
assign and_u3586_u0=and_u3584_u0&and_u3587_u0;
assign and_u3587_u0=and_u3583_u0&and_u3588_u0;
assign and_u3588_u0=and_u3581_u0&and_u3697_u0;
assign equals_u218_a_signed={28'b0, port_28bad23f_};
assign equals_u218_b_signed=32'h4;
assign equals_u218=equals_u218_a_signed==equals_u218_b_signed;
assign and_u3589_u0=and_u3697_u0&not_u828_u0;
assign and_u3590_u0=and_u3697_u0&equals_u218;
assign not_u828_u0=~equals_u218;
assign and_u3591_u0=and_u3597_u0&not_u829_u0;
assign not_u829_u0=~port_20a4c464_;
assign and_u3592_u0=and_u3597_u0&port_20a4c464_;
assign not_u830_u0=~port_04f3ed30_;
assign and_u3593_u0=and_u3596_u0&not_u830_u0;
assign and_u3594_u0=and_u3596_u0&port_04f3ed30_;
assign simplePinWrite_u575=and_u3595_u0&{1{and_u3595_u0}};
assign and_u3595_u0=and_u3594_u0&and_u3596_u0;
assign and_u3596_u0=and_u3592_u0&and_u3597_u0;
assign and_u3597_u0=and_u3590_u0&and_u3697_u0;
assign equals_u219_a_signed={28'b0, port_28bad23f_};
assign equals_u219_b_signed=32'h5;
assign equals_u219=equals_u219_a_signed==equals_u219_b_signed;
assign not_u831_u0=~equals_u219;
assign and_u3598_u0=and_u3697_u0&not_u831_u0;
assign and_u3599_u0=and_u3697_u0&equals_u219;
assign and_u3600_u0=and_u3606_u0&not_u832_u0;
assign and_u3601_u0=and_u3606_u0&port_32e6ae0a_;
assign not_u832_u0=~port_32e6ae0a_;
assign and_u3602_u0=and_u3605_u0&port_04f3ed30_;
assign not_u833_u0=~port_04f3ed30_;
assign and_u3603_u0=and_u3605_u0&not_u833_u0;
assign simplePinWrite_u576=and_u3604_u0&{1{and_u3604_u0}};
assign and_u3604_u0=and_u3602_u0&and_u3605_u0;
assign and_u3605_u0=and_u3601_u0&and_u3606_u0;
assign and_u3606_u0=and_u3599_u0&and_u3697_u0;
assign equals_u220_a_signed={28'b0, port_28bad23f_};
assign equals_u220_b_signed=32'h6;
assign equals_u220=equals_u220_a_signed==equals_u220_b_signed;
assign not_u834_u0=~equals_u220;
assign and_u3607_u0=and_u3697_u0&equals_u220;
assign and_u3608_u0=and_u3697_u0&not_u834_u0;
assign and_u3609_u0=and_u3615_u0&not_u835_u0;
assign and_u3610_u0=and_u3615_u0&port_4f59f80d_;
assign not_u835_u0=~port_4f59f80d_;
assign and_u3611_u0=and_u3614_u0&not_u836_u0;
assign and_u3612_u0=and_u3614_u0&port_04f3ed30_;
assign not_u836_u0=~port_04f3ed30_;
assign simplePinWrite_u577=and_u3613_u0&{1{and_u3613_u0}};
assign and_u3613_u0=and_u3612_u0&and_u3614_u0;
assign and_u3614_u0=and_u3610_u0&and_u3615_u0;
assign and_u3615_u0=and_u3607_u0&and_u3697_u0;
assign equals_u221_a_signed={28'b0, port_28bad23f_};
assign equals_u221_b_signed=32'h7;
assign equals_u221=equals_u221_a_signed==equals_u221_b_signed;
assign and_u3616_u0=and_u3697_u0&equals_u221;
assign and_u3617_u0=and_u3697_u0&not_u837_u0;
assign not_u837_u0=~equals_u221;
assign and_u3618_u0=and_u3624_u0&port_73fefad5_;
assign and_u3619_u0=and_u3624_u0&not_u838_u0;
assign not_u838_u0=~port_73fefad5_;
assign and_u3620_u0=and_u3623_u0&port_04f3ed30_;
assign not_u839_u0=~port_04f3ed30_;
assign and_u3621_u0=and_u3623_u0&not_u839_u0;
assign simplePinWrite_u578=and_u3622_u0&{1{and_u3622_u0}};
assign and_u3622_u0=and_u3620_u0&and_u3623_u0;
assign and_u3623_u0=and_u3618_u0&and_u3624_u0;
assign and_u3624_u0=and_u3616_u0&and_u3697_u0;
assign equals_u222_a_signed={28'b0, port_28bad23f_};
assign equals_u222_b_signed=32'h8;
assign equals_u222=equals_u222_a_signed==equals_u222_b_signed;
assign and_u3625_u0=and_u3697_u0&not_u840_u0;
assign not_u840_u0=~equals_u222;
assign and_u3626_u0=and_u3697_u0&equals_u222;
assign and_u3627_u0=and_u3633_u0&port_74abe77f_;
assign and_u3628_u0=and_u3633_u0&not_u841_u0;
assign not_u841_u0=~port_74abe77f_;
assign not_u842_u0=~port_04f3ed30_;
assign and_u3629_u0=and_u3632_u0&port_04f3ed30_;
assign and_u3630_u0=and_u3632_u0&not_u842_u0;
assign simplePinWrite_u579=and_u3631_u0&{1{and_u3631_u0}};
assign and_u3631_u0=and_u3629_u0&and_u3632_u0;
assign and_u3632_u0=and_u3627_u0&and_u3633_u0;
assign and_u3633_u0=and_u3626_u0&and_u3697_u0;
assign equals_u223_a_signed={28'b0, port_28bad23f_};
assign equals_u223_b_signed=32'h9;
assign equals_u223=equals_u223_a_signed==equals_u223_b_signed;
assign not_u843_u0=~equals_u223;
assign and_u3634_u0=and_u3697_u0&not_u843_u0;
assign and_u3635_u0=and_u3697_u0&equals_u223;
assign and_u3636_u0=and_u3642_u0&not_u844_u0;
assign not_u844_u0=~port_279b1a82_;
assign and_u3637_u0=and_u3642_u0&port_279b1a82_;
assign and_u3638_u0=and_u3641_u0&port_04f3ed30_;
assign and_u3639_u0=and_u3641_u0&not_u845_u0;
assign not_u845_u0=~port_04f3ed30_;
assign simplePinWrite_u580=and_u3640_u0&{1{and_u3640_u0}};
assign and_u3640_u0=and_u3638_u0&and_u3641_u0;
assign and_u3641_u0=and_u3637_u0&and_u3642_u0;
assign and_u3642_u0=and_u3635_u0&and_u3697_u0;
assign equals_u224_a_signed={28'b0, port_28bad23f_};
assign equals_u224_b_signed=32'ha;
assign equals_u224=equals_u224_a_signed==equals_u224_b_signed;
assign not_u846_u0=~equals_u224;
assign and_u3643_u0=and_u3697_u0&equals_u224;
assign and_u3644_u0=and_u3697_u0&not_u846_u0;
assign and_u3645_u0=and_u3651_u0&port_6e7a5501_;
assign and_u3646_u0=and_u3651_u0&not_u847_u0;
assign not_u847_u0=~port_6e7a5501_;
assign not_u848_u0=~port_04f3ed30_;
assign and_u3647_u0=and_u3650_u0&port_04f3ed30_;
assign and_u3648_u0=and_u3650_u0&not_u848_u0;
assign simplePinWrite_u581=and_u3649_u0&{1{and_u3649_u0}};
assign and_u3649_u0=and_u3647_u0&and_u3650_u0;
assign and_u3650_u0=and_u3645_u0&and_u3651_u0;
assign and_u3651_u0=and_u3643_u0&and_u3697_u0;
assign equals_u225_a_signed={28'b0, port_28bad23f_};
assign equals_u225_b_signed=32'hb;
assign equals_u225=equals_u225_a_signed==equals_u225_b_signed;
assign not_u849_u0=~equals_u225;
assign and_u3652_u0=and_u3697_u0&equals_u225;
assign and_u3653_u0=and_u3697_u0&not_u849_u0;
assign not_u850_u0=~port_2770715b_;
assign and_u3654_u0=and_u3660_u0&port_2770715b_;
assign and_u3655_u0=and_u3660_u0&not_u850_u0;
assign and_u3656_u0=and_u3659_u0&not_u851_u0;
assign and_u3657_u0=and_u3659_u0&port_04f3ed30_;
assign not_u851_u0=~port_04f3ed30_;
assign simplePinWrite_u582=and_u3658_u0&{1{and_u3658_u0}};
assign and_u3658_u0=and_u3657_u0&and_u3659_u0;
assign and_u3659_u0=and_u3654_u0&and_u3660_u0;
assign and_u3660_u0=and_u3652_u0&and_u3697_u0;
assign equals_u226_a_signed={28'b0, port_28bad23f_};
assign equals_u226_b_signed=32'hc;
assign equals_u226=equals_u226_a_signed==equals_u226_b_signed;
assign not_u852_u0=~equals_u226;
assign and_u3661_u0=and_u3697_u0&equals_u226;
assign and_u3662_u0=and_u3697_u0&not_u852_u0;
assign and_u3663_u0=and_u3669_u0&not_u853_u0;
assign not_u853_u0=~port_18e671ac_;
assign and_u3664_u0=and_u3669_u0&port_18e671ac_;
assign and_u3665_u0=and_u3668_u0&port_04f3ed30_;
assign and_u3666_u0=and_u3668_u0&not_u854_u0;
assign not_u854_u0=~port_04f3ed30_;
assign simplePinWrite_u583=and_u3667_u0&{1{and_u3667_u0}};
assign and_u3667_u0=and_u3665_u0&and_u3668_u0;
assign and_u3668_u0=and_u3664_u0&and_u3669_u0;
assign and_u3669_u0=and_u3661_u0&and_u3697_u0;
assign equals_u227_a_signed={28'b0, port_28bad23f_};
assign equals_u227_b_signed=32'hd;
assign equals_u227=equals_u227_a_signed==equals_u227_b_signed;
assign and_u3670_u0=and_u3697_u0&equals_u227;
assign not_u855_u0=~equals_u227;
assign and_u3671_u0=and_u3697_u0&not_u855_u0;
assign and_u3672_u0=and_u3678_u0&port_33df9314_;
assign not_u856_u0=~port_33df9314_;
assign and_u3673_u0=and_u3678_u0&not_u856_u0;
assign not_u857_u0=~port_04f3ed30_;
assign and_u3674_u0=and_u3677_u0&port_04f3ed30_;
assign and_u3675_u0=and_u3677_u0&not_u857_u0;
assign simplePinWrite_u584=and_u3676_u0&{1{and_u3676_u0}};
assign and_u3676_u0=and_u3674_u0&and_u3677_u0;
assign and_u3677_u0=and_u3672_u0&and_u3678_u0;
assign and_u3678_u0=and_u3670_u0&and_u3697_u0;
assign equals_u228_a_signed={28'b0, port_28bad23f_};
assign equals_u228_b_signed=32'he;
assign equals_u228=equals_u228_a_signed==equals_u228_b_signed;
assign and_u3679_u0=and_u3697_u0&equals_u228;
assign and_u3680_u0=and_u3697_u0&not_u858_u0;
assign not_u858_u0=~equals_u228;
assign and_u3681_u0=and_u3687_u0&port_6f3435ef_;
assign and_u3682_u0=and_u3687_u0&not_u859_u0;
assign not_u859_u0=~port_6f3435ef_;
assign not_u860_u0=~port_04f3ed30_;
assign and_u3683_u0=and_u3686_u0&port_04f3ed30_;
assign and_u3684_u0=and_u3686_u0&not_u860_u0;
assign simplePinWrite_u585=and_u3685_u0&{1{and_u3685_u0}};
assign and_u3685_u0=and_u3683_u0&and_u3686_u0;
assign and_u3686_u0=and_u3681_u0&and_u3687_u0;
assign and_u3687_u0=and_u3679_u0&and_u3697_u0;
assign equals_u229_a_signed={28'b0, port_28bad23f_};
assign equals_u229_b_signed=32'hf;
assign equals_u229=equals_u229_a_signed==equals_u229_b_signed;
assign and_u3688_u0=and_u3697_u0&equals_u229;
assign and_u3689_u0=and_u3697_u0&not_u861_u0;
assign not_u861_u0=~equals_u229;
assign and_u3690_u0=and_u3696_u0&port_4d65779c_;
assign and_u3691_u0=and_u3696_u0&not_u862_u0;
assign not_u862_u0=~port_4d65779c_;
assign and_u3692_u0=and_u3695_u0&not_u863_u0;
assign and_u3693_u0=and_u3695_u0&port_04f3ed30_;
assign not_u863_u0=~port_04f3ed30_;
assign simplePinWrite_u586=and_u3694_u0&{1{and_u3694_u0}};
assign and_u3694_u0=and_u3693_u0&and_u3695_u0;
assign and_u3695_u0=and_u3690_u0&and_u3696_u0;
assign and_u3696_u0=and_u3688_u0&and_u3697_u0;
assign mux_u1864=({4{and_u3559_u0}}&4'h1)|({4{and_u3568_u0}}&4'h8)|({4{and_u3577_u0}}&4'h3)|({4{and_u3586_u0}}&4'h4)|({4{and_u3595_u0}}&4'h5)|({4{and_u3604_u0}}&4'h6)|({4{and_u3613_u0}}&4'h7)|({4{and_u3622_u0}}&4'h0)|({4{and_u3631_u0}}&4'h9)|({4{and_u3640_u0}}&4'ha)|({4{and_u3649_u0}}&4'hb)|({4{and_u3658_u0}}&4'hc)|({4{and_u3667_u0}}&4'hd)|({4{and_u3676_u0}}&4'he)|({4{and_u3685_u0}}&4'hf)|({4{and_u3694_u0}}&4'h2);
assign or_u1405_u0=and_u3559_u0|and_u3568_u0|and_u3577_u0|and_u3586_u0|and_u3595_u0|and_u3604_u0|and_u3613_u0|and_u3622_u0|and_u3631_u0|and_u3640_u0|and_u3649_u0|and_u3658_u0|and_u3667_u0|and_u3676_u0|and_u3685_u0|and_u3694_u0;
assign and_u3697_u0=or_u1406_u0&or_u1406_u0;
assign or_u1406_u0=reg_6bac3bb4_u0|reg_36cffcd9_u0;
always @(posedge CLK or posedge RESET)
begin
if (RESET)
reg_6bac3bb4_u0<=1'h0;
else reg_6bac3bb4_u0<=reg_403f364b_u0;
end
always @(posedge CLK or posedge RESET)
begin
if (RESET)
reg_403f364b_u0<=1'h0;
else reg_403f364b_u0<=GO;
end
assign or_u1407_u0=GO|or_u1405_u0;
assign mux_u1865_u0=(GO)?4'h0:mux_u1864;
assign RESULT=or_u1407_u0;
assign RESULT_u2132=mux_u1865_u0;
assign RESULT_u2133=simplePinWrite_u580;
assign RESULT_u2134=simplePinWrite;
assign RESULT_u2135=simplePinWrite_u582;
assign RESULT_u2136=simplePinWrite_u583;
assign RESULT_u2137=simplePinWrite_u578;
assign RESULT_u2138=simplePinWrite_u574;
assign RESULT_u2139=simplePinWrite_u579;
assign RESULT_u2140=simplePinWrite_u576;
assign RESULT_u2141=simplePinWrite_u586;
assign RESULT_u2142=simplePinWrite_u577;
assign RESULT_u2143=simplePinWrite_u584;
assign RESULT_u2144=simplePinWrite_u572;
assign RESULT_u2145=simplePinWrite_u573;
assign RESULT_u2146=simplePinWrite_u575;
assign RESULT_u2147=simplePinWrite_u585;
assign RESULT_u2148=simplePinWrite_u581;
assign DONE=1'h0;
endmodule



module join_fanIn13(CLK, GO, port_0539d915_, RESULT, RESULT_u2149, RESULT_u2150, RESULT_u2151, DONE);
input		CLK;
input		GO;
input	[7:0]	port_0539d915_;
output	[15:0]	RESULT;
output		RESULT_u2149;
output	[7:0]	RESULT_u2150;
output		RESULT_u2151;
output		DONE;
wire		simplePinWrite;
wire	[15:0]	simplePinWrite_u587;
wire	[7:0]	simplePinWrite_u588;
wire		simplePinWrite_u589;
assign simplePinWrite=GO&{1{GO}};
assign simplePinWrite_u587=16'h1&{16{1'h1}};
assign simplePinWrite_u588=port_0539d915_&{8{GO}};
assign simplePinWrite_u589=GO&{1{GO}};
assign RESULT=simplePinWrite_u587;
assign RESULT_u2149=simplePinWrite;
assign RESULT_u2150=simplePinWrite_u588;
assign RESULT_u2151=simplePinWrite_u589;
assign DONE=GO;
endmodule



module join_fanIn11(CLK, GO, port_2b05df54_, RESULT, RESULT_u2152, RESULT_u2153, RESULT_u2154, DONE);
input		CLK;
input		GO;
input	[7:0]	port_2b05df54_;
output	[15:0]	RESULT;
output		RESULT_u2152;
output	[7:0]	RESULT_u2153;
output		RESULT_u2154;
output		DONE;
wire		simplePinWrite;
wire		simplePinWrite_u590;
wire	[7:0]	simplePinWrite_u591;
wire	[15:0]	simplePinWrite_u592;
assign simplePinWrite=GO&{1{GO}};
assign simplePinWrite_u590=GO&{1{GO}};
assign simplePinWrite_u591=port_2b05df54_&{8{GO}};
assign simplePinWrite_u592=16'h1&{16{1'h1}};
assign RESULT=simplePinWrite_u592;
assign RESULT_u2152=simplePinWrite;
assign RESULT_u2153=simplePinWrite_u591;
assign RESULT_u2154=simplePinWrite_u590;
assign DONE=GO;
endmodule



module join_endianswapper_5ffbbe4b_(endianswapper_5ffbbe4b_in, endianswapper_5ffbbe4b_out);
input	[3:0]	endianswapper_5ffbbe4b_in;
output	[3:0]	endianswapper_5ffbbe4b_out;
assign endianswapper_5ffbbe4b_out=endianswapper_5ffbbe4b_in;
endmodule



module join_endianswapper_35131291_(endianswapper_35131291_in, endianswapper_35131291_out);
input	[3:0]	endianswapper_35131291_in;
output	[3:0]	endianswapper_35131291_out;
assign endianswapper_35131291_out=endianswapper_35131291_in;
endmodule



module join_stateVar_fsmState_join(bus_36267731_, bus_63ac18a2_, bus_17d2639c_, bus_11a2624b_, bus_451bcd25_);
input		bus_36267731_;
input		bus_63ac18a2_;
input		bus_17d2639c_;
input	[3:0]	bus_11a2624b_;
output	[3:0]	bus_451bcd25_;
wire	[3:0]	endianswapper_5ffbbe4b_out;
wire	[3:0]	endianswapper_35131291_out;
reg	[3:0]	stateVar_fsmState_join_u5=4'h0;
join_endianswapper_5ffbbe4b_ join_endianswapper_5ffbbe4b__1(.endianswapper_5ffbbe4b_in(stateVar_fsmState_join_u5), 
  .endianswapper_5ffbbe4b_out(endianswapper_5ffbbe4b_out));
join_endianswapper_35131291_ join_endianswapper_35131291__1(.endianswapper_35131291_in(bus_11a2624b_), 
  .endianswapper_35131291_out(endianswapper_35131291_out));
always @(posedge bus_36267731_ or posedge bus_63ac18a2_)
begin
if (bus_63ac18a2_)
stateVar_fsmState_join_u5<=4'h0;
else if (bus_17d2639c_)
stateVar_fsmState_join_u5<=endianswapper_35131291_out;
end
assign bus_451bcd25_=endianswapper_5ffbbe4b_out;
endmodule



module join_fanIn7(CLK, GO, port_0607a328_, RESULT, RESULT_u2155, RESULT_u2156, RESULT_u2157, DONE);
input		CLK;
input		GO;
input	[7:0]	port_0607a328_;
output	[15:0]	RESULT;
output		RESULT_u2155;
output	[7:0]	RESULT_u2156;
output		RESULT_u2157;
output		DONE;
wire		simplePinWrite;
wire	[15:0]	simplePinWrite_u593;
wire		simplePinWrite_u594;
wire	[7:0]	simplePinWrite_u595;
assign simplePinWrite=GO&{1{GO}};
assign simplePinWrite_u593=16'h1&{16{1'h1}};
assign simplePinWrite_u594=GO&{1{GO}};
assign simplePinWrite_u595=port_0607a328_&{8{GO}};
assign RESULT=simplePinWrite_u593;
assign RESULT_u2155=simplePinWrite;
assign RESULT_u2156=simplePinWrite_u595;
assign RESULT_u2157=simplePinWrite_u594;
assign DONE=GO;
endmodule



module join_fanIn15(CLK, GO, port_53ccfeb0_, RESULT, RESULT_u2158, RESULT_u2159, RESULT_u2160, DONE);
input		CLK;
input		GO;
input	[7:0]	port_53ccfeb0_;
output		RESULT;
output	[15:0]	RESULT_u2158;
output	[7:0]	RESULT_u2159;
output		RESULT_u2160;
output		DONE;
wire		simplePinWrite;
wire	[15:0]	simplePinWrite_u596;
wire	[7:0]	simplePinWrite_u597;
wire		simplePinWrite_u598;
assign simplePinWrite=GO&{1{GO}};
assign simplePinWrite_u596=16'h1&{16{1'h1}};
assign simplePinWrite_u597=port_53ccfeb0_&{8{GO}};
assign simplePinWrite_u598=GO&{1{GO}};
assign RESULT=simplePinWrite;
assign RESULT_u2158=simplePinWrite_u596;
assign RESULT_u2159=simplePinWrite_u597;
assign RESULT_u2160=simplePinWrite_u598;
assign DONE=GO;
endmodule



module join_fanIn5(CLK, GO, port_5a46b6cb_, RESULT, RESULT_u2161, RESULT_u2162, RESULT_u2163, DONE);
input		CLK;
input		GO;
input	[7:0]	port_5a46b6cb_;
output		RESULT;
output	[15:0]	RESULT_u2161;
output	[7:0]	RESULT_u2162;
output		RESULT_u2163;
output		DONE;
wire		simplePinWrite;
wire		simplePinWrite_u599;
wire	[15:0]	simplePinWrite_u600;
wire	[7:0]	simplePinWrite_u601;
assign simplePinWrite=GO&{1{GO}};
assign simplePinWrite_u599=GO&{1{GO}};
assign simplePinWrite_u600=16'h1&{16{1'h1}};
assign simplePinWrite_u601=port_5a46b6cb_&{8{GO}};
assign RESULT=simplePinWrite;
assign RESULT_u2161=simplePinWrite_u600;
assign RESULT_u2162=simplePinWrite_u601;
assign RESULT_u2163=simplePinWrite_u599;
assign DONE=GO;
endmodule



module join_fanIn12(CLK, GO, port_4e987517_, RESULT, RESULT_u2164, RESULT_u2165, RESULT_u2166, DONE);
input		CLK;
input		GO;
input	[7:0]	port_4e987517_;
output		RESULT;
output	[15:0]	RESULT_u2164;
output	[7:0]	RESULT_u2165;
output		RESULT_u2166;
output		DONE;
wire		simplePinWrite;
wire	[15:0]	simplePinWrite_u602;
wire	[7:0]	simplePinWrite_u603;
wire		simplePinWrite_u604;
assign simplePinWrite=GO&{1{GO}};
assign simplePinWrite_u602=16'h1&{16{1'h1}};
assign simplePinWrite_u603=port_4e987517_&{8{GO}};
assign simplePinWrite_u604=GO&{1{GO}};
assign RESULT=simplePinWrite;
assign RESULT_u2164=simplePinWrite_u602;
assign RESULT_u2165=simplePinWrite_u603;
assign RESULT_u2166=simplePinWrite_u604;
assign DONE=GO;
endmodule



module join_fanIn3(CLK, GO, port_64b08ec5_, RESULT, RESULT_u2167, RESULT_u2168, RESULT_u2169, DONE);
input		CLK;
input		GO;
input	[7:0]	port_64b08ec5_;
output	[15:0]	RESULT;
output		RESULT_u2167;
output	[7:0]	RESULT_u2168;
output		RESULT_u2169;
output		DONE;
wire		simplePinWrite;
wire		simplePinWrite_u605;
wire	[7:0]	simplePinWrite_u606;
wire	[15:0]	simplePinWrite_u607;
assign simplePinWrite=GO&{1{GO}};
assign simplePinWrite_u605=GO&{1{GO}};
assign simplePinWrite_u606=port_64b08ec5_&{8{GO}};
assign simplePinWrite_u607=16'h1&{16{1'h1}};
assign RESULT=simplePinWrite_u607;
assign RESULT_u2167=simplePinWrite;
assign RESULT_u2168=simplePinWrite_u606;
assign RESULT_u2169=simplePinWrite_u605;
assign DONE=GO;
endmodule



module join_fanIn16(CLK, GO, port_2eadda5f_, RESULT, RESULT_u2170, RESULT_u2171, RESULT_u2172, DONE);
input		CLK;
input		GO;
input	[7:0]	port_2eadda5f_;
output		RESULT;
output	[15:0]	RESULT_u2170;
output	[7:0]	RESULT_u2171;
output		RESULT_u2172;
output		DONE;
wire		simplePinWrite;
wire		simplePinWrite_u608;
wire	[7:0]	simplePinWrite_u609;
wire	[15:0]	simplePinWrite_u610;
assign simplePinWrite=GO&{1{GO}};
assign simplePinWrite_u608=GO&{1{GO}};
assign simplePinWrite_u609=port_2eadda5f_&{8{GO}};
assign simplePinWrite_u610=16'h1&{16{1'h1}};
assign RESULT=simplePinWrite;
assign RESULT_u2170=simplePinWrite_u610;
assign RESULT_u2171=simplePinWrite_u609;
assign RESULT_u2172=simplePinWrite_u608;
assign DONE=GO;
endmodule



module join_fanIn2(CLK, GO, port_6154e88d_, RESULT, RESULT_u2173, RESULT_u2174, RESULT_u2175, DONE);
input		CLK;
input		GO;
input	[7:0]	port_6154e88d_;
output	[15:0]	RESULT;
output	[7:0]	RESULT_u2173;
output		RESULT_u2174;
output		RESULT_u2175;
output		DONE;
wire		simplePinWrite;
wire		simplePinWrite_u611;
wire	[7:0]	simplePinWrite_u612;
wire	[15:0]	simplePinWrite_u613;
assign simplePinWrite=GO&{1{GO}};
assign simplePinWrite_u611=GO&{1{GO}};
assign simplePinWrite_u612=port_6154e88d_&{8{GO}};
assign simplePinWrite_u613=16'h1&{16{1'h1}};
assign RESULT=simplePinWrite_u613;
assign RESULT_u2173=simplePinWrite_u612;
assign RESULT_u2174=simplePinWrite;
assign RESULT_u2175=simplePinWrite_u611;
assign DONE=GO;
endmodule



module join_globalreset_physical_53032fd2_(bus_5db0a9c7_, bus_64ddf646_, bus_7143524c_);
input		bus_5db0a9c7_;
input		bus_64ddf646_;
output		bus_7143524c_;
reg		cross_u55=1'h0;
wire		or_287d9dbf_u0;
reg		sample_u55=1'h0;
reg		glitch_u55=1'h0;
wire		and_1e3db525_u0;
wire		not_33fb2282_u0;
reg		final_u55=1'h1;
always @(posedge bus_5db0a9c7_)
begin
cross_u55<=sample_u55;
end
assign or_287d9dbf_u0=bus_64ddf646_|final_u55;
always @(posedge bus_5db0a9c7_)
begin
sample_u55<=1'h1;
end
always @(posedge bus_5db0a9c7_)
begin
glitch_u55<=cross_u55;
end
assign and_1e3db525_u0=cross_u55&glitch_u55;
assign bus_7143524c_=or_287d9dbf_u0;
assign not_33fb2282_u0=~and_1e3db525_u0;
always @(posedge bus_5db0a9c7_)
begin
final_u55<=not_33fb2282_u0;
end
endmodule


