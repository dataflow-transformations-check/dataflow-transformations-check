-- ----------------------------------------------------------------------------
-- __  ___ __ ___  _ __   ___  ___ 
-- \ \/ / '__/ _ \| '_ \ / _ \/ __|
--  >  <| | | (_) | | | | (_) \__ \
-- /_/\_\_|  \___/|_| |_|\___/|___/
-- ----------------------------------------------------------------------------
-- Xronos synthesizer
-- Top level Network: medianParallel16 
-- Date: 2018/02/23 14:28:55
-- ----------------------------------------------------------------------------

-- ----------------------------------------------------------------------------
-- Clock Domain(s) Information on the Network "medianParallel16"
--
-- Network input port(s) clock domain:
--	inPort --> CLK
-- Network output port(s) clock domain:
-- 	outPort --> CLK
-- Actor(s) clock domains:
--	split (split) --> CLK
--	join (join) --> CLK
--	medianRow1 (medianRow1) --> CLK
--	medianRow2 (medianRow2) --> CLK
--	medianRow16 (medianRow16) --> CLK
--	medianRow15 (medianRow15) --> CLK
--	medianRow14 (medianRow14) --> CLK
--	medianRow13 (medianRow13) --> CLK
--	medianRow12 (medianRow12) --> CLK
--	medianRow11 (medianRow11) --> CLK
--	medianRow10 (medianRow10) --> CLK
--	medianRow9 (medianRow9) --> CLK
--	medianRow8 (medianRow8) --> CLK
--	medianRow7 (medianRow7) --> CLK
--	medianRow6 (medianRow6) --> CLK
--	medianRow5 (medianRow5) --> CLK
--	medianRow4 (medianRow4) --> CLK
--	medianRow3 (medianRow3) --> CLK

library ieee;
library SystemBuilder;

use ieee.std_logic_1164.all;

-- ----------------------------------------------------------------------------
-- Entity Declaration
-- ----------------------------------------------------------------------------
entity medianParallel16 is
port(
	 -- XDF Network Input(s)
	 inPort_data : in std_logic_vector(7 downto 0);
	 inPort_send : in std_logic;
	 inPort_ack : out std_logic;
	 inPort_rdy : out std_logic;
	 inPort_count : in std_logic_vector(15 downto 0);
	 -- XDF Network Output(s)
	 outPort_data : out std_logic_vector(7 downto 0);
	 outPort_send : out std_logic;
	 outPort_ack : in std_logic;
	 outPort_rdy : in std_logic;
	 outPort_count : out std_logic_vector(15 downto 0);
	 -- Clock(s) and Reset
	 CLK : in std_logic;
	 RESET : in std_logic);
end entity medianParallel16;

-- ----------------------------------------------------------------------------
-- Architecture Declaration
-- ----------------------------------------------------------------------------
architecture rtl of medianParallel16 is
	-- --------------------------------------------------------------------------
	-- Internal Signals
	-- --------------------------------------------------------------------------

	-- Clock(s) and Reset signal
	signal clocks, resets: std_logic_vector(0 downto 0);

	-- Network Input Port(s)
	signal ni_inPort_data : std_logic_vector(7 downto 0);
	signal ni_inPort_send : std_logic;
	signal ni_inPort_ack : std_logic;
	signal ni_inPort_rdy : std_logic;
	signal ni_inPort_count : std_logic_vector(15 downto 0);
	
	-- Network Input Port Fanout(s)
	signal nif_inPort_data : std_logic_vector(7 downto 0);
	signal nif_inPort_send : std_logic_vector(0 downto 0);
	signal nif_inPort_ack : std_logic_vector(0 downto 0);
	signal nif_inPort_rdy : std_logic_vector(0 downto 0);
	signal nif_inPort_count : std_logic_vector(15 downto 0);
	
	-- Network Output Port(s) 
	signal no_outPort_data : std_logic_vector(7 downto 0);
	signal no_outPort_send : std_logic;
	signal no_outPort_ack : std_logic;
	signal no_outPort_rdy : std_logic;
	signal no_outPort_count : std_logic_vector(15 downto 0);
	
	-- Actors Input/Output and Output fanout signals
	signal ai_split_in1_data : std_logic_vector(7 downto 0);
	signal ai_split_in1_send : std_logic;
	signal ai_split_in1_ack : std_logic;
	signal ai_split_in1_count : std_logic_vector(15 downto 0);
	
	signal ao_split_out1_data : std_logic_vector(7 downto 0);
	signal ao_split_out1_send : std_logic;
	signal ao_split_out1_ack : std_logic;
	signal ao_split_out1_rdy : std_logic;
	signal ao_split_out1_count : std_logic_vector(15 downto 0);
	
	signal aof_split_out1_data : std_logic_vector(7 downto 0);
	signal aof_split_out1_send : std_logic_vector(0 downto 0);
	signal aof_split_out1_ack : std_logic_vector(0 downto 0);
	signal aof_split_out1_rdy : std_logic_vector(0 downto 0);
	signal aof_split_out1_count : std_logic_vector(15 downto 0);
	
	signal ao_split_out2_data : std_logic_vector(7 downto 0);
	signal ao_split_out2_send : std_logic;
	signal ao_split_out2_ack : std_logic;
	signal ao_split_out2_rdy : std_logic;
	signal ao_split_out2_count : std_logic_vector(15 downto 0);
	
	signal aof_split_out2_data : std_logic_vector(7 downto 0);
	signal aof_split_out2_send : std_logic_vector(0 downto 0);
	signal aof_split_out2_ack : std_logic_vector(0 downto 0);
	signal aof_split_out2_rdy : std_logic_vector(0 downto 0);
	signal aof_split_out2_count : std_logic_vector(15 downto 0);
	
	signal ao_split_out3_data : std_logic_vector(7 downto 0);
	signal ao_split_out3_send : std_logic;
	signal ao_split_out3_ack : std_logic;
	signal ao_split_out3_rdy : std_logic;
	signal ao_split_out3_count : std_logic_vector(15 downto 0);
	
	signal aof_split_out3_data : std_logic_vector(7 downto 0);
	signal aof_split_out3_send : std_logic_vector(0 downto 0);
	signal aof_split_out3_ack : std_logic_vector(0 downto 0);
	signal aof_split_out3_rdy : std_logic_vector(0 downto 0);
	signal aof_split_out3_count : std_logic_vector(15 downto 0);
	
	signal ao_split_out4_data : std_logic_vector(7 downto 0);
	signal ao_split_out4_send : std_logic;
	signal ao_split_out4_ack : std_logic;
	signal ao_split_out4_rdy : std_logic;
	signal ao_split_out4_count : std_logic_vector(15 downto 0);
	
	signal aof_split_out4_data : std_logic_vector(7 downto 0);
	signal aof_split_out4_send : std_logic_vector(0 downto 0);
	signal aof_split_out4_ack : std_logic_vector(0 downto 0);
	signal aof_split_out4_rdy : std_logic_vector(0 downto 0);
	signal aof_split_out4_count : std_logic_vector(15 downto 0);
	
	signal ao_split_out5_data : std_logic_vector(7 downto 0);
	signal ao_split_out5_send : std_logic;
	signal ao_split_out5_ack : std_logic;
	signal ao_split_out5_rdy : std_logic;
	signal ao_split_out5_count : std_logic_vector(15 downto 0);
	
	signal aof_split_out5_data : std_logic_vector(7 downto 0);
	signal aof_split_out5_send : std_logic_vector(0 downto 0);
	signal aof_split_out5_ack : std_logic_vector(0 downto 0);
	signal aof_split_out5_rdy : std_logic_vector(0 downto 0);
	signal aof_split_out5_count : std_logic_vector(15 downto 0);
	
	signal ao_split_out6_data : std_logic_vector(7 downto 0);
	signal ao_split_out6_send : std_logic;
	signal ao_split_out6_ack : std_logic;
	signal ao_split_out6_rdy : std_logic;
	signal ao_split_out6_count : std_logic_vector(15 downto 0);
	
	signal aof_split_out6_data : std_logic_vector(7 downto 0);
	signal aof_split_out6_send : std_logic_vector(0 downto 0);
	signal aof_split_out6_ack : std_logic_vector(0 downto 0);
	signal aof_split_out6_rdy : std_logic_vector(0 downto 0);
	signal aof_split_out6_count : std_logic_vector(15 downto 0);
	
	signal ao_split_out7_data : std_logic_vector(7 downto 0);
	signal ao_split_out7_send : std_logic;
	signal ao_split_out7_ack : std_logic;
	signal ao_split_out7_rdy : std_logic;
	signal ao_split_out7_count : std_logic_vector(15 downto 0);
	
	signal aof_split_out7_data : std_logic_vector(7 downto 0);
	signal aof_split_out7_send : std_logic_vector(0 downto 0);
	signal aof_split_out7_ack : std_logic_vector(0 downto 0);
	signal aof_split_out7_rdy : std_logic_vector(0 downto 0);
	signal aof_split_out7_count : std_logic_vector(15 downto 0);
	
	signal ao_split_out8_data : std_logic_vector(7 downto 0);
	signal ao_split_out8_send : std_logic;
	signal ao_split_out8_ack : std_logic;
	signal ao_split_out8_rdy : std_logic;
	signal ao_split_out8_count : std_logic_vector(15 downto 0);
	
	signal aof_split_out8_data : std_logic_vector(7 downto 0);
	signal aof_split_out8_send : std_logic_vector(0 downto 0);
	signal aof_split_out8_ack : std_logic_vector(0 downto 0);
	signal aof_split_out8_rdy : std_logic_vector(0 downto 0);
	signal aof_split_out8_count : std_logic_vector(15 downto 0);
	
	signal ao_split_out9_data : std_logic_vector(7 downto 0);
	signal ao_split_out9_send : std_logic;
	signal ao_split_out9_ack : std_logic;
	signal ao_split_out9_rdy : std_logic;
	signal ao_split_out9_count : std_logic_vector(15 downto 0);
	
	signal aof_split_out9_data : std_logic_vector(7 downto 0);
	signal aof_split_out9_send : std_logic_vector(0 downto 0);
	signal aof_split_out9_ack : std_logic_vector(0 downto 0);
	signal aof_split_out9_rdy : std_logic_vector(0 downto 0);
	signal aof_split_out9_count : std_logic_vector(15 downto 0);
	
	signal ao_split_out10_data : std_logic_vector(7 downto 0);
	signal ao_split_out10_send : std_logic;
	signal ao_split_out10_ack : std_logic;
	signal ao_split_out10_rdy : std_logic;
	signal ao_split_out10_count : std_logic_vector(15 downto 0);
	
	signal aof_split_out10_data : std_logic_vector(7 downto 0);
	signal aof_split_out10_send : std_logic_vector(0 downto 0);
	signal aof_split_out10_ack : std_logic_vector(0 downto 0);
	signal aof_split_out10_rdy : std_logic_vector(0 downto 0);
	signal aof_split_out10_count : std_logic_vector(15 downto 0);
	
	signal ao_split_out11_data : std_logic_vector(7 downto 0);
	signal ao_split_out11_send : std_logic;
	signal ao_split_out11_ack : std_logic;
	signal ao_split_out11_rdy : std_logic;
	signal ao_split_out11_count : std_logic_vector(15 downto 0);
	
	signal aof_split_out11_data : std_logic_vector(7 downto 0);
	signal aof_split_out11_send : std_logic_vector(0 downto 0);
	signal aof_split_out11_ack : std_logic_vector(0 downto 0);
	signal aof_split_out11_rdy : std_logic_vector(0 downto 0);
	signal aof_split_out11_count : std_logic_vector(15 downto 0);
	
	signal ao_split_out12_data : std_logic_vector(7 downto 0);
	signal ao_split_out12_send : std_logic;
	signal ao_split_out12_ack : std_logic;
	signal ao_split_out12_rdy : std_logic;
	signal ao_split_out12_count : std_logic_vector(15 downto 0);
	
	signal aof_split_out12_data : std_logic_vector(7 downto 0);
	signal aof_split_out12_send : std_logic_vector(0 downto 0);
	signal aof_split_out12_ack : std_logic_vector(0 downto 0);
	signal aof_split_out12_rdy : std_logic_vector(0 downto 0);
	signal aof_split_out12_count : std_logic_vector(15 downto 0);
	
	signal ao_split_out13_data : std_logic_vector(7 downto 0);
	signal ao_split_out13_send : std_logic;
	signal ao_split_out13_ack : std_logic;
	signal ao_split_out13_rdy : std_logic;
	signal ao_split_out13_count : std_logic_vector(15 downto 0);
	
	signal aof_split_out13_data : std_logic_vector(7 downto 0);
	signal aof_split_out13_send : std_logic_vector(0 downto 0);
	signal aof_split_out13_ack : std_logic_vector(0 downto 0);
	signal aof_split_out13_rdy : std_logic_vector(0 downto 0);
	signal aof_split_out13_count : std_logic_vector(15 downto 0);
	
	signal ao_split_out14_data : std_logic_vector(7 downto 0);
	signal ao_split_out14_send : std_logic;
	signal ao_split_out14_ack : std_logic;
	signal ao_split_out14_rdy : std_logic;
	signal ao_split_out14_count : std_logic_vector(15 downto 0);
	
	signal aof_split_out14_data : std_logic_vector(7 downto 0);
	signal aof_split_out14_send : std_logic_vector(0 downto 0);
	signal aof_split_out14_ack : std_logic_vector(0 downto 0);
	signal aof_split_out14_rdy : std_logic_vector(0 downto 0);
	signal aof_split_out14_count : std_logic_vector(15 downto 0);
	
	signal ao_split_out15_data : std_logic_vector(7 downto 0);
	signal ao_split_out15_send : std_logic;
	signal ao_split_out15_ack : std_logic;
	signal ao_split_out15_rdy : std_logic;
	signal ao_split_out15_count : std_logic_vector(15 downto 0);
	
	signal aof_split_out15_data : std_logic_vector(7 downto 0);
	signal aof_split_out15_send : std_logic_vector(0 downto 0);
	signal aof_split_out15_ack : std_logic_vector(0 downto 0);
	signal aof_split_out15_rdy : std_logic_vector(0 downto 0);
	signal aof_split_out15_count : std_logic_vector(15 downto 0);
	
	signal ao_split_out16_data : std_logic_vector(7 downto 0);
	signal ao_split_out16_send : std_logic;
	signal ao_split_out16_ack : std_logic;
	signal ao_split_out16_rdy : std_logic;
	signal ao_split_out16_count : std_logic_vector(15 downto 0);
	
	signal aof_split_out16_data : std_logic_vector(7 downto 0);
	signal aof_split_out16_send : std_logic_vector(0 downto 0);
	signal aof_split_out16_ack : std_logic_vector(0 downto 0);
	signal aof_split_out16_rdy : std_logic_vector(0 downto 0);
	signal aof_split_out16_count : std_logic_vector(15 downto 0);
	
	signal ai_join_in1_data : std_logic_vector(7 downto 0);
	signal ai_join_in1_send : std_logic;
	signal ai_join_in1_ack : std_logic;
	signal ai_join_in1_count : std_logic_vector(15 downto 0);
	
	signal ai_join_in2_data : std_logic_vector(7 downto 0);
	signal ai_join_in2_send : std_logic;
	signal ai_join_in2_ack : std_logic;
	signal ai_join_in2_count : std_logic_vector(15 downto 0);
	
	signal ai_join_in3_data : std_logic_vector(7 downto 0);
	signal ai_join_in3_send : std_logic;
	signal ai_join_in3_ack : std_logic;
	signal ai_join_in3_count : std_logic_vector(15 downto 0);
	
	signal ai_join_in4_data : std_logic_vector(7 downto 0);
	signal ai_join_in4_send : std_logic;
	signal ai_join_in4_ack : std_logic;
	signal ai_join_in4_count : std_logic_vector(15 downto 0);
	
	signal ai_join_in5_data : std_logic_vector(7 downto 0);
	signal ai_join_in5_send : std_logic;
	signal ai_join_in5_ack : std_logic;
	signal ai_join_in5_count : std_logic_vector(15 downto 0);
	
	signal ai_join_in6_data : std_logic_vector(7 downto 0);
	signal ai_join_in6_send : std_logic;
	signal ai_join_in6_ack : std_logic;
	signal ai_join_in6_count : std_logic_vector(15 downto 0);
	
	signal ai_join_in7_data : std_logic_vector(7 downto 0);
	signal ai_join_in7_send : std_logic;
	signal ai_join_in7_ack : std_logic;
	signal ai_join_in7_count : std_logic_vector(15 downto 0);
	
	signal ai_join_in8_data : std_logic_vector(7 downto 0);
	signal ai_join_in8_send : std_logic;
	signal ai_join_in8_ack : std_logic;
	signal ai_join_in8_count : std_logic_vector(15 downto 0);
	
	signal ai_join_in9_data : std_logic_vector(7 downto 0);
	signal ai_join_in9_send : std_logic;
	signal ai_join_in9_ack : std_logic;
	signal ai_join_in9_count : std_logic_vector(15 downto 0);
	
	signal ai_join_in10_data : std_logic_vector(7 downto 0);
	signal ai_join_in10_send : std_logic;
	signal ai_join_in10_ack : std_logic;
	signal ai_join_in10_count : std_logic_vector(15 downto 0);
	
	signal ai_join_in11_data : std_logic_vector(7 downto 0);
	signal ai_join_in11_send : std_logic;
	signal ai_join_in11_ack : std_logic;
	signal ai_join_in11_count : std_logic_vector(15 downto 0);
	
	signal ai_join_in12_data : std_logic_vector(7 downto 0);
	signal ai_join_in12_send : std_logic;
	signal ai_join_in12_ack : std_logic;
	signal ai_join_in12_count : std_logic_vector(15 downto 0);
	
	signal ai_join_in13_data : std_logic_vector(7 downto 0);
	signal ai_join_in13_send : std_logic;
	signal ai_join_in13_ack : std_logic;
	signal ai_join_in13_count : std_logic_vector(15 downto 0);
	
	signal ai_join_in14_data : std_logic_vector(7 downto 0);
	signal ai_join_in14_send : std_logic;
	signal ai_join_in14_ack : std_logic;
	signal ai_join_in14_count : std_logic_vector(15 downto 0);
	
	signal ai_join_in15_data : std_logic_vector(7 downto 0);
	signal ai_join_in15_send : std_logic;
	signal ai_join_in15_ack : std_logic;
	signal ai_join_in15_count : std_logic_vector(15 downto 0);
	
	signal ai_join_in16_data : std_logic_vector(7 downto 0);
	signal ai_join_in16_send : std_logic;
	signal ai_join_in16_ack : std_logic;
	signal ai_join_in16_count : std_logic_vector(15 downto 0);
	
	signal ao_join_out1_data : std_logic_vector(7 downto 0);
	signal ao_join_out1_send : std_logic;
	signal ao_join_out1_ack : std_logic;
	signal ao_join_out1_rdy : std_logic;
	signal ao_join_out1_count : std_logic_vector(15 downto 0);
	
	signal aof_join_out1_data : std_logic_vector(7 downto 0);
	signal aof_join_out1_send : std_logic_vector(0 downto 0);
	signal aof_join_out1_ack : std_logic_vector(0 downto 0);
	signal aof_join_out1_rdy : std_logic_vector(0 downto 0);
	signal aof_join_out1_count : std_logic_vector(15 downto 0);
	
	signal ai_medianRow1_in1_data : std_logic_vector(7 downto 0);
	signal ai_medianRow1_in1_send : std_logic;
	signal ai_medianRow1_in1_ack : std_logic;
	signal ai_medianRow1_in1_count : std_logic_vector(15 downto 0);
	
	signal ao_medianRow1_median_data : std_logic_vector(7 downto 0);
	signal ao_medianRow1_median_send : std_logic;
	signal ao_medianRow1_median_ack : std_logic;
	signal ao_medianRow1_median_rdy : std_logic;
	signal ao_medianRow1_median_count : std_logic_vector(15 downto 0);
	
	signal aof_medianRow1_median_data : std_logic_vector(7 downto 0);
	signal aof_medianRow1_median_send : std_logic_vector(0 downto 0);
	signal aof_medianRow1_median_ack : std_logic_vector(0 downto 0);
	signal aof_medianRow1_median_rdy : std_logic_vector(0 downto 0);
	signal aof_medianRow1_median_count : std_logic_vector(15 downto 0);
	
	signal ai_medianRow2_in1_data : std_logic_vector(7 downto 0);
	signal ai_medianRow2_in1_send : std_logic;
	signal ai_medianRow2_in1_ack : std_logic;
	signal ai_medianRow2_in1_count : std_logic_vector(15 downto 0);
	
	signal ao_medianRow2_median_data : std_logic_vector(7 downto 0);
	signal ao_medianRow2_median_send : std_logic;
	signal ao_medianRow2_median_ack : std_logic;
	signal ao_medianRow2_median_rdy : std_logic;
	signal ao_medianRow2_median_count : std_logic_vector(15 downto 0);
	
	signal aof_medianRow2_median_data : std_logic_vector(7 downto 0);
	signal aof_medianRow2_median_send : std_logic_vector(0 downto 0);
	signal aof_medianRow2_median_ack : std_logic_vector(0 downto 0);
	signal aof_medianRow2_median_rdy : std_logic_vector(0 downto 0);
	signal aof_medianRow2_median_count : std_logic_vector(15 downto 0);
	
	signal ai_medianRow16_in1_data : std_logic_vector(7 downto 0);
	signal ai_medianRow16_in1_send : std_logic;
	signal ai_medianRow16_in1_ack : std_logic;
	signal ai_medianRow16_in1_count : std_logic_vector(15 downto 0);
	
	signal ao_medianRow16_median_data : std_logic_vector(7 downto 0);
	signal ao_medianRow16_median_send : std_logic;
	signal ao_medianRow16_median_ack : std_logic;
	signal ao_medianRow16_median_rdy : std_logic;
	signal ao_medianRow16_median_count : std_logic_vector(15 downto 0);
	
	signal aof_medianRow16_median_data : std_logic_vector(7 downto 0);
	signal aof_medianRow16_median_send : std_logic_vector(0 downto 0);
	signal aof_medianRow16_median_ack : std_logic_vector(0 downto 0);
	signal aof_medianRow16_median_rdy : std_logic_vector(0 downto 0);
	signal aof_medianRow16_median_count : std_logic_vector(15 downto 0);
	
	signal ai_medianRow15_in1_data : std_logic_vector(7 downto 0);
	signal ai_medianRow15_in1_send : std_logic;
	signal ai_medianRow15_in1_ack : std_logic;
	signal ai_medianRow15_in1_count : std_logic_vector(15 downto 0);
	
	signal ao_medianRow15_median_data : std_logic_vector(7 downto 0);
	signal ao_medianRow15_median_send : std_logic;
	signal ao_medianRow15_median_ack : std_logic;
	signal ao_medianRow15_median_rdy : std_logic;
	signal ao_medianRow15_median_count : std_logic_vector(15 downto 0);
	
	signal aof_medianRow15_median_data : std_logic_vector(7 downto 0);
	signal aof_medianRow15_median_send : std_logic_vector(0 downto 0);
	signal aof_medianRow15_median_ack : std_logic_vector(0 downto 0);
	signal aof_medianRow15_median_rdy : std_logic_vector(0 downto 0);
	signal aof_medianRow15_median_count : std_logic_vector(15 downto 0);
	
	signal ai_medianRow14_in1_data : std_logic_vector(7 downto 0);
	signal ai_medianRow14_in1_send : std_logic;
	signal ai_medianRow14_in1_ack : std_logic;
	signal ai_medianRow14_in1_count : std_logic_vector(15 downto 0);
	
	signal ao_medianRow14_median_data : std_logic_vector(7 downto 0);
	signal ao_medianRow14_median_send : std_logic;
	signal ao_medianRow14_median_ack : std_logic;
	signal ao_medianRow14_median_rdy : std_logic;
	signal ao_medianRow14_median_count : std_logic_vector(15 downto 0);
	
	signal aof_medianRow14_median_data : std_logic_vector(7 downto 0);
	signal aof_medianRow14_median_send : std_logic_vector(0 downto 0);
	signal aof_medianRow14_median_ack : std_logic_vector(0 downto 0);
	signal aof_medianRow14_median_rdy : std_logic_vector(0 downto 0);
	signal aof_medianRow14_median_count : std_logic_vector(15 downto 0);
	
	signal ai_medianRow13_in1_data : std_logic_vector(7 downto 0);
	signal ai_medianRow13_in1_send : std_logic;
	signal ai_medianRow13_in1_ack : std_logic;
	signal ai_medianRow13_in1_count : std_logic_vector(15 downto 0);
	
	signal ao_medianRow13_median_data : std_logic_vector(7 downto 0);
	signal ao_medianRow13_median_send : std_logic;
	signal ao_medianRow13_median_ack : std_logic;
	signal ao_medianRow13_median_rdy : std_logic;
	signal ao_medianRow13_median_count : std_logic_vector(15 downto 0);
	
	signal aof_medianRow13_median_data : std_logic_vector(7 downto 0);
	signal aof_medianRow13_median_send : std_logic_vector(0 downto 0);
	signal aof_medianRow13_median_ack : std_logic_vector(0 downto 0);
	signal aof_medianRow13_median_rdy : std_logic_vector(0 downto 0);
	signal aof_medianRow13_median_count : std_logic_vector(15 downto 0);
	
	signal ai_medianRow12_in1_data : std_logic_vector(7 downto 0);
	signal ai_medianRow12_in1_send : std_logic;
	signal ai_medianRow12_in1_ack : std_logic;
	signal ai_medianRow12_in1_count : std_logic_vector(15 downto 0);
	
	signal ao_medianRow12_median_data : std_logic_vector(7 downto 0);
	signal ao_medianRow12_median_send : std_logic;
	signal ao_medianRow12_median_ack : std_logic;
	signal ao_medianRow12_median_rdy : std_logic;
	signal ao_medianRow12_median_count : std_logic_vector(15 downto 0);
	
	signal aof_medianRow12_median_data : std_logic_vector(7 downto 0);
	signal aof_medianRow12_median_send : std_logic_vector(0 downto 0);
	signal aof_medianRow12_median_ack : std_logic_vector(0 downto 0);
	signal aof_medianRow12_median_rdy : std_logic_vector(0 downto 0);
	signal aof_medianRow12_median_count : std_logic_vector(15 downto 0);
	
	signal ai_medianRow11_in1_data : std_logic_vector(7 downto 0);
	signal ai_medianRow11_in1_send : std_logic;
	signal ai_medianRow11_in1_ack : std_logic;
	signal ai_medianRow11_in1_count : std_logic_vector(15 downto 0);
	
	signal ao_medianRow11_median_data : std_logic_vector(7 downto 0);
	signal ao_medianRow11_median_send : std_logic;
	signal ao_medianRow11_median_ack : std_logic;
	signal ao_medianRow11_median_rdy : std_logic;
	signal ao_medianRow11_median_count : std_logic_vector(15 downto 0);
	
	signal aof_medianRow11_median_data : std_logic_vector(7 downto 0);
	signal aof_medianRow11_median_send : std_logic_vector(0 downto 0);
	signal aof_medianRow11_median_ack : std_logic_vector(0 downto 0);
	signal aof_medianRow11_median_rdy : std_logic_vector(0 downto 0);
	signal aof_medianRow11_median_count : std_logic_vector(15 downto 0);
	
	signal ai_medianRow10_in1_data : std_logic_vector(7 downto 0);
	signal ai_medianRow10_in1_send : std_logic;
	signal ai_medianRow10_in1_ack : std_logic;
	signal ai_medianRow10_in1_count : std_logic_vector(15 downto 0);
	
	signal ao_medianRow10_median_data : std_logic_vector(7 downto 0);
	signal ao_medianRow10_median_send : std_logic;
	signal ao_medianRow10_median_ack : std_logic;
	signal ao_medianRow10_median_rdy : std_logic;
	signal ao_medianRow10_median_count : std_logic_vector(15 downto 0);
	
	signal aof_medianRow10_median_data : std_logic_vector(7 downto 0);
	signal aof_medianRow10_median_send : std_logic_vector(0 downto 0);
	signal aof_medianRow10_median_ack : std_logic_vector(0 downto 0);
	signal aof_medianRow10_median_rdy : std_logic_vector(0 downto 0);
	signal aof_medianRow10_median_count : std_logic_vector(15 downto 0);
	
	signal ai_medianRow9_in1_data : std_logic_vector(7 downto 0);
	signal ai_medianRow9_in1_send : std_logic;
	signal ai_medianRow9_in1_ack : std_logic;
	signal ai_medianRow9_in1_count : std_logic_vector(15 downto 0);
	
	signal ao_medianRow9_median_data : std_logic_vector(7 downto 0);
	signal ao_medianRow9_median_send : std_logic;
	signal ao_medianRow9_median_ack : std_logic;
	signal ao_medianRow9_median_rdy : std_logic;
	signal ao_medianRow9_median_count : std_logic_vector(15 downto 0);
	
	signal aof_medianRow9_median_data : std_logic_vector(7 downto 0);
	signal aof_medianRow9_median_send : std_logic_vector(0 downto 0);
	signal aof_medianRow9_median_ack : std_logic_vector(0 downto 0);
	signal aof_medianRow9_median_rdy : std_logic_vector(0 downto 0);
	signal aof_medianRow9_median_count : std_logic_vector(15 downto 0);
	
	signal ai_medianRow8_in1_data : std_logic_vector(7 downto 0);
	signal ai_medianRow8_in1_send : std_logic;
	signal ai_medianRow8_in1_ack : std_logic;
	signal ai_medianRow8_in1_count : std_logic_vector(15 downto 0);
	
	signal ao_medianRow8_median_data : std_logic_vector(7 downto 0);
	signal ao_medianRow8_median_send : std_logic;
	signal ao_medianRow8_median_ack : std_logic;
	signal ao_medianRow8_median_rdy : std_logic;
	signal ao_medianRow8_median_count : std_logic_vector(15 downto 0);
	
	signal aof_medianRow8_median_data : std_logic_vector(7 downto 0);
	signal aof_medianRow8_median_send : std_logic_vector(0 downto 0);
	signal aof_medianRow8_median_ack : std_logic_vector(0 downto 0);
	signal aof_medianRow8_median_rdy : std_logic_vector(0 downto 0);
	signal aof_medianRow8_median_count : std_logic_vector(15 downto 0);
	
	signal ai_medianRow7_in1_data : std_logic_vector(7 downto 0);
	signal ai_medianRow7_in1_send : std_logic;
	signal ai_medianRow7_in1_ack : std_logic;
	signal ai_medianRow7_in1_count : std_logic_vector(15 downto 0);
	
	signal ao_medianRow7_median_data : std_logic_vector(7 downto 0);
	signal ao_medianRow7_median_send : std_logic;
	signal ao_medianRow7_median_ack : std_logic;
	signal ao_medianRow7_median_rdy : std_logic;
	signal ao_medianRow7_median_count : std_logic_vector(15 downto 0);
	
	signal aof_medianRow7_median_data : std_logic_vector(7 downto 0);
	signal aof_medianRow7_median_send : std_logic_vector(0 downto 0);
	signal aof_medianRow7_median_ack : std_logic_vector(0 downto 0);
	signal aof_medianRow7_median_rdy : std_logic_vector(0 downto 0);
	signal aof_medianRow7_median_count : std_logic_vector(15 downto 0);
	
	signal ai_medianRow6_in1_data : std_logic_vector(7 downto 0);
	signal ai_medianRow6_in1_send : std_logic;
	signal ai_medianRow6_in1_ack : std_logic;
	signal ai_medianRow6_in1_count : std_logic_vector(15 downto 0);
	
	signal ao_medianRow6_median_data : std_logic_vector(7 downto 0);
	signal ao_medianRow6_median_send : std_logic;
	signal ao_medianRow6_median_ack : std_logic;
	signal ao_medianRow6_median_rdy : std_logic;
	signal ao_medianRow6_median_count : std_logic_vector(15 downto 0);
	
	signal aof_medianRow6_median_data : std_logic_vector(7 downto 0);
	signal aof_medianRow6_median_send : std_logic_vector(0 downto 0);
	signal aof_medianRow6_median_ack : std_logic_vector(0 downto 0);
	signal aof_medianRow6_median_rdy : std_logic_vector(0 downto 0);
	signal aof_medianRow6_median_count : std_logic_vector(15 downto 0);
	
	signal ai_medianRow5_in1_data : std_logic_vector(7 downto 0);
	signal ai_medianRow5_in1_send : std_logic;
	signal ai_medianRow5_in1_ack : std_logic;
	signal ai_medianRow5_in1_count : std_logic_vector(15 downto 0);
	
	signal ao_medianRow5_median_data : std_logic_vector(7 downto 0);
	signal ao_medianRow5_median_send : std_logic;
	signal ao_medianRow5_median_ack : std_logic;
	signal ao_medianRow5_median_rdy : std_logic;
	signal ao_medianRow5_median_count : std_logic_vector(15 downto 0);
	
	signal aof_medianRow5_median_data : std_logic_vector(7 downto 0);
	signal aof_medianRow5_median_send : std_logic_vector(0 downto 0);
	signal aof_medianRow5_median_ack : std_logic_vector(0 downto 0);
	signal aof_medianRow5_median_rdy : std_logic_vector(0 downto 0);
	signal aof_medianRow5_median_count : std_logic_vector(15 downto 0);
	
	signal ai_medianRow4_in1_data : std_logic_vector(7 downto 0);
	signal ai_medianRow4_in1_send : std_logic;
	signal ai_medianRow4_in1_ack : std_logic;
	signal ai_medianRow4_in1_count : std_logic_vector(15 downto 0);
	
	signal ao_medianRow4_median_data : std_logic_vector(7 downto 0);
	signal ao_medianRow4_median_send : std_logic;
	signal ao_medianRow4_median_ack : std_logic;
	signal ao_medianRow4_median_rdy : std_logic;
	signal ao_medianRow4_median_count : std_logic_vector(15 downto 0);
	
	signal aof_medianRow4_median_data : std_logic_vector(7 downto 0);
	signal aof_medianRow4_median_send : std_logic_vector(0 downto 0);
	signal aof_medianRow4_median_ack : std_logic_vector(0 downto 0);
	signal aof_medianRow4_median_rdy : std_logic_vector(0 downto 0);
	signal aof_medianRow4_median_count : std_logic_vector(15 downto 0);
	
	signal ai_medianRow3_in1_data : std_logic_vector(7 downto 0);
	signal ai_medianRow3_in1_send : std_logic;
	signal ai_medianRow3_in1_ack : std_logic;
	signal ai_medianRow3_in1_count : std_logic_vector(15 downto 0);
	
	signal ao_medianRow3_median_data : std_logic_vector(7 downto 0);
	signal ao_medianRow3_median_send : std_logic;
	signal ao_medianRow3_median_ack : std_logic;
	signal ao_medianRow3_median_rdy : std_logic;
	signal ao_medianRow3_median_count : std_logic_vector(15 downto 0);
	
	signal aof_medianRow3_median_data : std_logic_vector(7 downto 0);
	signal aof_medianRow3_median_send : std_logic_vector(0 downto 0);
	signal aof_medianRow3_median_ack : std_logic_vector(0 downto 0);
	signal aof_medianRow3_median_rdy : std_logic_vector(0 downto 0);
	signal aof_medianRow3_median_count : std_logic_vector(15 downto 0);

	-- --------------------------------------------------------------------------
	-- Network Instances
	-- --------------------------------------------------------------------------
	component split is
	port(
	     -- Instance split Input(s)
	     in1_data : in std_logic_vector(7 downto 0);
	     in1_send : in std_logic;
	     in1_ack : out std_logic;
	     in1_count : in std_logic_vector(15 downto 0);
	     -- Instance split Output(s)
	     out1_data : out std_logic_vector(7 downto 0);
	     out1_send : out std_logic;
	     out1_ack : in std_logic;
	     out1_rdy : in std_logic;
	     out1_count : out std_logic_vector(15 downto 0);
	     out2_data : out std_logic_vector(7 downto 0);
	     out2_send : out std_logic;
	     out2_ack : in std_logic;
	     out2_rdy : in std_logic;
	     out2_count : out std_logic_vector(15 downto 0);
	     out3_data : out std_logic_vector(7 downto 0);
	     out3_send : out std_logic;
	     out3_ack : in std_logic;
	     out3_rdy : in std_logic;
	     out3_count : out std_logic_vector(15 downto 0);
	     out4_data : out std_logic_vector(7 downto 0);
	     out4_send : out std_logic;
	     out4_ack : in std_logic;
	     out4_rdy : in std_logic;
	     out4_count : out std_logic_vector(15 downto 0);
	     out5_data : out std_logic_vector(7 downto 0);
	     out5_send : out std_logic;
	     out5_ack : in std_logic;
	     out5_rdy : in std_logic;
	     out5_count : out std_logic_vector(15 downto 0);
	     out6_data : out std_logic_vector(7 downto 0);
	     out6_send : out std_logic;
	     out6_ack : in std_logic;
	     out6_rdy : in std_logic;
	     out6_count : out std_logic_vector(15 downto 0);
	     out7_data : out std_logic_vector(7 downto 0);
	     out7_send : out std_logic;
	     out7_ack : in std_logic;
	     out7_rdy : in std_logic;
	     out7_count : out std_logic_vector(15 downto 0);
	     out8_data : out std_logic_vector(7 downto 0);
	     out8_send : out std_logic;
	     out8_ack : in std_logic;
	     out8_rdy : in std_logic;
	     out8_count : out std_logic_vector(15 downto 0);
	     out9_data : out std_logic_vector(7 downto 0);
	     out9_send : out std_logic;
	     out9_ack : in std_logic;
	     out9_rdy : in std_logic;
	     out9_count : out std_logic_vector(15 downto 0);
	     out10_data : out std_logic_vector(7 downto 0);
	     out10_send : out std_logic;
	     out10_ack : in std_logic;
	     out10_rdy : in std_logic;
	     out10_count : out std_logic_vector(15 downto 0);
	     out11_data : out std_logic_vector(7 downto 0);
	     out11_send : out std_logic;
	     out11_ack : in std_logic;
	     out11_rdy : in std_logic;
	     out11_count : out std_logic_vector(15 downto 0);
	     out12_data : out std_logic_vector(7 downto 0);
	     out12_send : out std_logic;
	     out12_ack : in std_logic;
	     out12_rdy : in std_logic;
	     out12_count : out std_logic_vector(15 downto 0);
	     out13_data : out std_logic_vector(7 downto 0);
	     out13_send : out std_logic;
	     out13_ack : in std_logic;
	     out13_rdy : in std_logic;
	     out13_count : out std_logic_vector(15 downto 0);
	     out14_data : out std_logic_vector(7 downto 0);
	     out14_send : out std_logic;
	     out14_ack : in std_logic;
	     out14_rdy : in std_logic;
	     out14_count : out std_logic_vector(15 downto 0);
	     out15_data : out std_logic_vector(7 downto 0);
	     out15_send : out std_logic;
	     out15_ack : in std_logic;
	     out15_rdy : in std_logic;
	     out15_count : out std_logic_vector(15 downto 0);
	     out16_data : out std_logic_vector(7 downto 0);
	     out16_send : out std_logic;
	     out16_ack : in std_logic;
	     out16_rdy : in std_logic;
	     out16_count : out std_logic_vector(15 downto 0);
	     clk: in std_logic;
	     reset: in std_logic);
	end component split;
	
	component join is
	port(
	     -- Instance join Input(s)
	     in1_data : in std_logic_vector(7 downto 0);
	     in1_send : in std_logic;
	     in1_ack : out std_logic;
	     in1_count : in std_logic_vector(15 downto 0);
	     in2_data : in std_logic_vector(7 downto 0);
	     in2_send : in std_logic;
	     in2_ack : out std_logic;
	     in2_count : in std_logic_vector(15 downto 0);
	     in3_data : in std_logic_vector(7 downto 0);
	     in3_send : in std_logic;
	     in3_ack : out std_logic;
	     in3_count : in std_logic_vector(15 downto 0);
	     in4_data : in std_logic_vector(7 downto 0);
	     in4_send : in std_logic;
	     in4_ack : out std_logic;
	     in4_count : in std_logic_vector(15 downto 0);
	     in5_data : in std_logic_vector(7 downto 0);
	     in5_send : in std_logic;
	     in5_ack : out std_logic;
	     in5_count : in std_logic_vector(15 downto 0);
	     in6_data : in std_logic_vector(7 downto 0);
	     in6_send : in std_logic;
	     in6_ack : out std_logic;
	     in6_count : in std_logic_vector(15 downto 0);
	     in7_data : in std_logic_vector(7 downto 0);
	     in7_send : in std_logic;
	     in7_ack : out std_logic;
	     in7_count : in std_logic_vector(15 downto 0);
	     in8_data : in std_logic_vector(7 downto 0);
	     in8_send : in std_logic;
	     in8_ack : out std_logic;
	     in8_count : in std_logic_vector(15 downto 0);
	     in9_data : in std_logic_vector(7 downto 0);
	     in9_send : in std_logic;
	     in9_ack : out std_logic;
	     in9_count : in std_logic_vector(15 downto 0);
	     in10_data : in std_logic_vector(7 downto 0);
	     in10_send : in std_logic;
	     in10_ack : out std_logic;
	     in10_count : in std_logic_vector(15 downto 0);
	     in11_data : in std_logic_vector(7 downto 0);
	     in11_send : in std_logic;
	     in11_ack : out std_logic;
	     in11_count : in std_logic_vector(15 downto 0);
	     in12_data : in std_logic_vector(7 downto 0);
	     in12_send : in std_logic;
	     in12_ack : out std_logic;
	     in12_count : in std_logic_vector(15 downto 0);
	     in13_data : in std_logic_vector(7 downto 0);
	     in13_send : in std_logic;
	     in13_ack : out std_logic;
	     in13_count : in std_logic_vector(15 downto 0);
	     in14_data : in std_logic_vector(7 downto 0);
	     in14_send : in std_logic;
	     in14_ack : out std_logic;
	     in14_count : in std_logic_vector(15 downto 0);
	     in15_data : in std_logic_vector(7 downto 0);
	     in15_send : in std_logic;
	     in15_ack : out std_logic;
	     in15_count : in std_logic_vector(15 downto 0);
	     in16_data : in std_logic_vector(7 downto 0);
	     in16_send : in std_logic;
	     in16_ack : out std_logic;
	     in16_count : in std_logic_vector(15 downto 0);
	     -- Instance join Output(s)
	     out1_data : out std_logic_vector(7 downto 0);
	     out1_send : out std_logic;
	     out1_ack : in std_logic;
	     out1_rdy : in std_logic;
	     out1_count : out std_logic_vector(15 downto 0);
	     clk: in std_logic;
	     reset: in std_logic);
	end component join;
	
	component medianRow1 is
	port(
	     -- Instance medianRow1 Input(s)
	     in1_data : in std_logic_vector(7 downto 0);
	     in1_send : in std_logic;
	     in1_ack : out std_logic;
	     in1_count : in std_logic_vector(15 downto 0);
	     -- Instance medianRow1 Output(s)
	     median_data : out std_logic_vector(7 downto 0);
	     median_send : out std_logic;
	     median_ack : in std_logic;
	     median_rdy : in std_logic;
	     median_count : out std_logic_vector(15 downto 0);
	     clk: in std_logic;
	     reset: in std_logic);
	end component medianRow1;
	
	component medianRow2 is
	port(
	     -- Instance medianRow2 Input(s)
	     in1_data : in std_logic_vector(7 downto 0);
	     in1_send : in std_logic;
	     in1_ack : out std_logic;
	     in1_count : in std_logic_vector(15 downto 0);
	     -- Instance medianRow2 Output(s)
	     median_data : out std_logic_vector(7 downto 0);
	     median_send : out std_logic;
	     median_ack : in std_logic;
	     median_rdy : in std_logic;
	     median_count : out std_logic_vector(15 downto 0);
	     clk: in std_logic;
	     reset: in std_logic);
	end component medianRow2;
	
	component medianRow16 is
	port(
	     -- Instance medianRow16 Input(s)
	     in1_data : in std_logic_vector(7 downto 0);
	     in1_send : in std_logic;
	     in1_ack : out std_logic;
	     in1_count : in std_logic_vector(15 downto 0);
	     -- Instance medianRow16 Output(s)
	     median_data : out std_logic_vector(7 downto 0);
	     median_send : out std_logic;
	     median_ack : in std_logic;
	     median_rdy : in std_logic;
	     median_count : out std_logic_vector(15 downto 0);
	     clk: in std_logic;
	     reset: in std_logic);
	end component medianRow16;
	
	component medianRow15 is
	port(
	     -- Instance medianRow15 Input(s)
	     in1_data : in std_logic_vector(7 downto 0);
	     in1_send : in std_logic;
	     in1_ack : out std_logic;
	     in1_count : in std_logic_vector(15 downto 0);
	     -- Instance medianRow15 Output(s)
	     median_data : out std_logic_vector(7 downto 0);
	     median_send : out std_logic;
	     median_ack : in std_logic;
	     median_rdy : in std_logic;
	     median_count : out std_logic_vector(15 downto 0);
	     clk: in std_logic;
	     reset: in std_logic);
	end component medianRow15;
	
	component medianRow14 is
	port(
	     -- Instance medianRow14 Input(s)
	     in1_data : in std_logic_vector(7 downto 0);
	     in1_send : in std_logic;
	     in1_ack : out std_logic;
	     in1_count : in std_logic_vector(15 downto 0);
	     -- Instance medianRow14 Output(s)
	     median_data : out std_logic_vector(7 downto 0);
	     median_send : out std_logic;
	     median_ack : in std_logic;
	     median_rdy : in std_logic;
	     median_count : out std_logic_vector(15 downto 0);
	     clk: in std_logic;
	     reset: in std_logic);
	end component medianRow14;
	
	component medianRow13 is
	port(
	     -- Instance medianRow13 Input(s)
	     in1_data : in std_logic_vector(7 downto 0);
	     in1_send : in std_logic;
	     in1_ack : out std_logic;
	     in1_count : in std_logic_vector(15 downto 0);
	     -- Instance medianRow13 Output(s)
	     median_data : out std_logic_vector(7 downto 0);
	     median_send : out std_logic;
	     median_ack : in std_logic;
	     median_rdy : in std_logic;
	     median_count : out std_logic_vector(15 downto 0);
	     clk: in std_logic;
	     reset: in std_logic);
	end component medianRow13;
	
	component medianRow12 is
	port(
	     -- Instance medianRow12 Input(s)
	     in1_data : in std_logic_vector(7 downto 0);
	     in1_send : in std_logic;
	     in1_ack : out std_logic;
	     in1_count : in std_logic_vector(15 downto 0);
	     -- Instance medianRow12 Output(s)
	     median_data : out std_logic_vector(7 downto 0);
	     median_send : out std_logic;
	     median_ack : in std_logic;
	     median_rdy : in std_logic;
	     median_count : out std_logic_vector(15 downto 0);
	     clk: in std_logic;
	     reset: in std_logic);
	end component medianRow12;
	
	component medianRow11 is
	port(
	     -- Instance medianRow11 Input(s)
	     in1_data : in std_logic_vector(7 downto 0);
	     in1_send : in std_logic;
	     in1_ack : out std_logic;
	     in1_count : in std_logic_vector(15 downto 0);
	     -- Instance medianRow11 Output(s)
	     median_data : out std_logic_vector(7 downto 0);
	     median_send : out std_logic;
	     median_ack : in std_logic;
	     median_rdy : in std_logic;
	     median_count : out std_logic_vector(15 downto 0);
	     clk: in std_logic;
	     reset: in std_logic);
	end component medianRow11;
	
	component medianRow10 is
	port(
	     -- Instance medianRow10 Input(s)
	     in1_data : in std_logic_vector(7 downto 0);
	     in1_send : in std_logic;
	     in1_ack : out std_logic;
	     in1_count : in std_logic_vector(15 downto 0);
	     -- Instance medianRow10 Output(s)
	     median_data : out std_logic_vector(7 downto 0);
	     median_send : out std_logic;
	     median_ack : in std_logic;
	     median_rdy : in std_logic;
	     median_count : out std_logic_vector(15 downto 0);
	     clk: in std_logic;
	     reset: in std_logic);
	end component medianRow10;
	
	component medianRow9 is
	port(
	     -- Instance medianRow9 Input(s)
	     in1_data : in std_logic_vector(7 downto 0);
	     in1_send : in std_logic;
	     in1_ack : out std_logic;
	     in1_count : in std_logic_vector(15 downto 0);
	     -- Instance medianRow9 Output(s)
	     median_data : out std_logic_vector(7 downto 0);
	     median_send : out std_logic;
	     median_ack : in std_logic;
	     median_rdy : in std_logic;
	     median_count : out std_logic_vector(15 downto 0);
	     clk: in std_logic;
	     reset: in std_logic);
	end component medianRow9;
	
	component medianRow8 is
	port(
	     -- Instance medianRow8 Input(s)
	     in1_data : in std_logic_vector(7 downto 0);
	     in1_send : in std_logic;
	     in1_ack : out std_logic;
	     in1_count : in std_logic_vector(15 downto 0);
	     -- Instance medianRow8 Output(s)
	     median_data : out std_logic_vector(7 downto 0);
	     median_send : out std_logic;
	     median_ack : in std_logic;
	     median_rdy : in std_logic;
	     median_count : out std_logic_vector(15 downto 0);
	     clk: in std_logic;
	     reset: in std_logic);
	end component medianRow8;
	
	component medianRow7 is
	port(
	     -- Instance medianRow7 Input(s)
	     in1_data : in std_logic_vector(7 downto 0);
	     in1_send : in std_logic;
	     in1_ack : out std_logic;
	     in1_count : in std_logic_vector(15 downto 0);
	     -- Instance medianRow7 Output(s)
	     median_data : out std_logic_vector(7 downto 0);
	     median_send : out std_logic;
	     median_ack : in std_logic;
	     median_rdy : in std_logic;
	     median_count : out std_logic_vector(15 downto 0);
	     clk: in std_logic;
	     reset: in std_logic);
	end component medianRow7;
	
	component medianRow6 is
	port(
	     -- Instance medianRow6 Input(s)
	     in1_data : in std_logic_vector(7 downto 0);
	     in1_send : in std_logic;
	     in1_ack : out std_logic;
	     in1_count : in std_logic_vector(15 downto 0);
	     -- Instance medianRow6 Output(s)
	     median_data : out std_logic_vector(7 downto 0);
	     median_send : out std_logic;
	     median_ack : in std_logic;
	     median_rdy : in std_logic;
	     median_count : out std_logic_vector(15 downto 0);
	     clk: in std_logic;
	     reset: in std_logic);
	end component medianRow6;
	
	component medianRow5 is
	port(
	     -- Instance medianRow5 Input(s)
	     in1_data : in std_logic_vector(7 downto 0);
	     in1_send : in std_logic;
	     in1_ack : out std_logic;
	     in1_count : in std_logic_vector(15 downto 0);
	     -- Instance medianRow5 Output(s)
	     median_data : out std_logic_vector(7 downto 0);
	     median_send : out std_logic;
	     median_ack : in std_logic;
	     median_rdy : in std_logic;
	     median_count : out std_logic_vector(15 downto 0);
	     clk: in std_logic;
	     reset: in std_logic);
	end component medianRow5;
	
	component medianRow4 is
	port(
	     -- Instance medianRow4 Input(s)
	     in1_data : in std_logic_vector(7 downto 0);
	     in1_send : in std_logic;
	     in1_ack : out std_logic;
	     in1_count : in std_logic_vector(15 downto 0);
	     -- Instance medianRow4 Output(s)
	     median_data : out std_logic_vector(7 downto 0);
	     median_send : out std_logic;
	     median_ack : in std_logic;
	     median_rdy : in std_logic;
	     median_count : out std_logic_vector(15 downto 0);
	     clk: in std_logic;
	     reset: in std_logic);
	end component medianRow4;
	
	component medianRow3 is
	port(
	     -- Instance medianRow3 Input(s)
	     in1_data : in std_logic_vector(7 downto 0);
	     in1_send : in std_logic;
	     in1_ack : out std_logic;
	     in1_count : in std_logic_vector(15 downto 0);
	     -- Instance medianRow3 Output(s)
	     median_data : out std_logic_vector(7 downto 0);
	     median_send : out std_logic;
	     median_ack : in std_logic;
	     median_rdy : in std_logic;
	     median_count : out std_logic_vector(15 downto 0);
	     clk: in std_logic;
	     reset: in std_logic);
	end component medianRow3;
	

begin
	-- Reset Controller
	rcon: entity SystemBuilder.resetController(behavioral)
	generic map(count => 1)
	port map( 
	         clocks => clocks, 
	         reset_in => reset, 
	         resets => resets);
	
	clocks(0) <= CLK;

	-- --------------------------------------------------------------------------
	-- Actor instances
	-- --------------------------------------------------------------------------
	i_split : component split
	port map(
		-- Instance split Input(s)
		in1_data => ai_split_in1_data,
		in1_send => ai_split_in1_send,
		in1_ack => ai_split_in1_ack,
		in1_count => ai_split_in1_count,
		-- Instance split Output(s)
		out1_data => ao_split_out1_data,
		out1_send => ao_split_out1_send,
		out1_ack => ao_split_out1_ack,
		out1_rdy => ao_split_out1_rdy,
		out1_count => ao_split_out1_count,
		
		out2_data => ao_split_out2_data,
		out2_send => ao_split_out2_send,
		out2_ack => ao_split_out2_ack,
		out2_rdy => ao_split_out2_rdy,
		out2_count => ao_split_out2_count,
		
		out3_data => ao_split_out3_data,
		out3_send => ao_split_out3_send,
		out3_ack => ao_split_out3_ack,
		out3_rdy => ao_split_out3_rdy,
		out3_count => ao_split_out3_count,
		
		out4_data => ao_split_out4_data,
		out4_send => ao_split_out4_send,
		out4_ack => ao_split_out4_ack,
		out4_rdy => ao_split_out4_rdy,
		out4_count => ao_split_out4_count,
		
		out5_data => ao_split_out5_data,
		out5_send => ao_split_out5_send,
		out5_ack => ao_split_out5_ack,
		out5_rdy => ao_split_out5_rdy,
		out5_count => ao_split_out5_count,
		
		out6_data => ao_split_out6_data,
		out6_send => ao_split_out6_send,
		out6_ack => ao_split_out6_ack,
		out6_rdy => ao_split_out6_rdy,
		out6_count => ao_split_out6_count,
		
		out7_data => ao_split_out7_data,
		out7_send => ao_split_out7_send,
		out7_ack => ao_split_out7_ack,
		out7_rdy => ao_split_out7_rdy,
		out7_count => ao_split_out7_count,
		
		out8_data => ao_split_out8_data,
		out8_send => ao_split_out8_send,
		out8_ack => ao_split_out8_ack,
		out8_rdy => ao_split_out8_rdy,
		out8_count => ao_split_out8_count,
		
		out9_data => ao_split_out9_data,
		out9_send => ao_split_out9_send,
		out9_ack => ao_split_out9_ack,
		out9_rdy => ao_split_out9_rdy,
		out9_count => ao_split_out9_count,
		
		out10_data => ao_split_out10_data,
		out10_send => ao_split_out10_send,
		out10_ack => ao_split_out10_ack,
		out10_rdy => ao_split_out10_rdy,
		out10_count => ao_split_out10_count,
		
		out11_data => ao_split_out11_data,
		out11_send => ao_split_out11_send,
		out11_ack => ao_split_out11_ack,
		out11_rdy => ao_split_out11_rdy,
		out11_count => ao_split_out11_count,
		
		out12_data => ao_split_out12_data,
		out12_send => ao_split_out12_send,
		out12_ack => ao_split_out12_ack,
		out12_rdy => ao_split_out12_rdy,
		out12_count => ao_split_out12_count,
		
		out13_data => ao_split_out13_data,
		out13_send => ao_split_out13_send,
		out13_ack => ao_split_out13_ack,
		out13_rdy => ao_split_out13_rdy,
		out13_count => ao_split_out13_count,
		
		out14_data => ao_split_out14_data,
		out14_send => ao_split_out14_send,
		out14_ack => ao_split_out14_ack,
		out14_rdy => ao_split_out14_rdy,
		out14_count => ao_split_out14_count,
		
		out15_data => ao_split_out15_data,
		out15_send => ao_split_out15_send,
		out15_ack => ao_split_out15_ack,
		out15_rdy => ao_split_out15_rdy,
		out15_count => ao_split_out15_count,
		
		out16_data => ao_split_out16_data,
		out16_send => ao_split_out16_send,
		out16_ack => ao_split_out16_ack,
		out16_rdy => ao_split_out16_rdy,
		out16_count => ao_split_out16_count,
		-- Clock and Reset
	clk => clocks(0),
	--clk => clocks(0),
	reset => resets(0));
	
	i_join : component join
	port map(
		-- Instance join Input(s)
		in1_data => ai_join_in1_data,
		in1_send => ai_join_in1_send,
		in1_ack => ai_join_in1_ack,
		in1_count => ai_join_in1_count,
		
		in2_data => ai_join_in2_data,
		in2_send => ai_join_in2_send,
		in2_ack => ai_join_in2_ack,
		in2_count => ai_join_in2_count,
		
		in3_data => ai_join_in3_data,
		in3_send => ai_join_in3_send,
		in3_ack => ai_join_in3_ack,
		in3_count => ai_join_in3_count,
		
		in4_data => ai_join_in4_data,
		in4_send => ai_join_in4_send,
		in4_ack => ai_join_in4_ack,
		in4_count => ai_join_in4_count,
		
		in5_data => ai_join_in5_data,
		in5_send => ai_join_in5_send,
		in5_ack => ai_join_in5_ack,
		in5_count => ai_join_in5_count,
		
		in6_data => ai_join_in6_data,
		in6_send => ai_join_in6_send,
		in6_ack => ai_join_in6_ack,
		in6_count => ai_join_in6_count,
		
		in7_data => ai_join_in7_data,
		in7_send => ai_join_in7_send,
		in7_ack => ai_join_in7_ack,
		in7_count => ai_join_in7_count,
		
		in8_data => ai_join_in8_data,
		in8_send => ai_join_in8_send,
		in8_ack => ai_join_in8_ack,
		in8_count => ai_join_in8_count,
		
		in9_data => ai_join_in9_data,
		in9_send => ai_join_in9_send,
		in9_ack => ai_join_in9_ack,
		in9_count => ai_join_in9_count,
		
		in10_data => ai_join_in10_data,
		in10_send => ai_join_in10_send,
		in10_ack => ai_join_in10_ack,
		in10_count => ai_join_in10_count,
		
		in11_data => ai_join_in11_data,
		in11_send => ai_join_in11_send,
		in11_ack => ai_join_in11_ack,
		in11_count => ai_join_in11_count,
		
		in12_data => ai_join_in12_data,
		in12_send => ai_join_in12_send,
		in12_ack => ai_join_in12_ack,
		in12_count => ai_join_in12_count,
		
		in13_data => ai_join_in13_data,
		in13_send => ai_join_in13_send,
		in13_ack => ai_join_in13_ack,
		in13_count => ai_join_in13_count,
		
		in14_data => ai_join_in14_data,
		in14_send => ai_join_in14_send,
		in14_ack => ai_join_in14_ack,
		in14_count => ai_join_in14_count,
		
		in15_data => ai_join_in15_data,
		in15_send => ai_join_in15_send,
		in15_ack => ai_join_in15_ack,
		in15_count => ai_join_in15_count,
		
		in16_data => ai_join_in16_data,
		in16_send => ai_join_in16_send,
		in16_ack => ai_join_in16_ack,
		in16_count => ai_join_in16_count,
		-- Instance join Output(s)
		out1_data => ao_join_out1_data,
		out1_send => ao_join_out1_send,
		out1_ack => ao_join_out1_ack,
		out1_rdy => ao_join_out1_rdy,
		out1_count => ao_join_out1_count,
		-- Clock and Reset
	clk => clocks(0),
	--clk => clocks(0),
	reset => resets(0));
	
	i_medianRow1 : component medianRow1
	port map(
		-- Instance medianRow1 Input(s)
		in1_data => ai_medianRow1_in1_data,
		in1_send => ai_medianRow1_in1_send,
		in1_ack => ai_medianRow1_in1_ack,
		in1_count => ai_medianRow1_in1_count,
		-- Instance medianRow1 Output(s)
		median_data => ao_medianRow1_median_data,
		median_send => ao_medianRow1_median_send,
		median_ack => ao_medianRow1_median_ack,
		median_rdy => ao_medianRow1_median_rdy,
		median_count => ao_medianRow1_median_count,
		-- Clock and Reset
	clk => clocks(0),
	--clk => clocks(0),
	reset => resets(0));
	
	i_medianRow2 : component medianRow2
	port map(
		-- Instance medianRow2 Input(s)
		in1_data => ai_medianRow2_in1_data,
		in1_send => ai_medianRow2_in1_send,
		in1_ack => ai_medianRow2_in1_ack,
		in1_count => ai_medianRow2_in1_count,
		-- Instance medianRow2 Output(s)
		median_data => ao_medianRow2_median_data,
		median_send => ao_medianRow2_median_send,
		median_ack => ao_medianRow2_median_ack,
		median_rdy => ao_medianRow2_median_rdy,
		median_count => ao_medianRow2_median_count,
		-- Clock and Reset
	clk => clocks(0),
	--clk => clocks(0),
	reset => resets(0));
	
	i_medianRow16 : component medianRow16
	port map(
		-- Instance medianRow16 Input(s)
		in1_data => ai_medianRow16_in1_data,
		in1_send => ai_medianRow16_in1_send,
		in1_ack => ai_medianRow16_in1_ack,
		in1_count => ai_medianRow16_in1_count,
		-- Instance medianRow16 Output(s)
		median_data => ao_medianRow16_median_data,
		median_send => ao_medianRow16_median_send,
		median_ack => ao_medianRow16_median_ack,
		median_rdy => ao_medianRow16_median_rdy,
		median_count => ao_medianRow16_median_count,
		-- Clock and Reset
	clk => clocks(0),
	--clk => clocks(0),
	reset => resets(0));
	
	i_medianRow15 : component medianRow15
	port map(
		-- Instance medianRow15 Input(s)
		in1_data => ai_medianRow15_in1_data,
		in1_send => ai_medianRow15_in1_send,
		in1_ack => ai_medianRow15_in1_ack,
		in1_count => ai_medianRow15_in1_count,
		-- Instance medianRow15 Output(s)
		median_data => ao_medianRow15_median_data,
		median_send => ao_medianRow15_median_send,
		median_ack => ao_medianRow15_median_ack,
		median_rdy => ao_medianRow15_median_rdy,
		median_count => ao_medianRow15_median_count,
		-- Clock and Reset
	clk => clocks(0),
	--clk => clocks(0),
	reset => resets(0));
	
	i_medianRow14 : component medianRow14
	port map(
		-- Instance medianRow14 Input(s)
		in1_data => ai_medianRow14_in1_data,
		in1_send => ai_medianRow14_in1_send,
		in1_ack => ai_medianRow14_in1_ack,
		in1_count => ai_medianRow14_in1_count,
		-- Instance medianRow14 Output(s)
		median_data => ao_medianRow14_median_data,
		median_send => ao_medianRow14_median_send,
		median_ack => ao_medianRow14_median_ack,
		median_rdy => ao_medianRow14_median_rdy,
		median_count => ao_medianRow14_median_count,
		-- Clock and Reset
	clk => clocks(0),
	--clk => clocks(0),
	reset => resets(0));
	
	i_medianRow13 : component medianRow13
	port map(
		-- Instance medianRow13 Input(s)
		in1_data => ai_medianRow13_in1_data,
		in1_send => ai_medianRow13_in1_send,
		in1_ack => ai_medianRow13_in1_ack,
		in1_count => ai_medianRow13_in1_count,
		-- Instance medianRow13 Output(s)
		median_data => ao_medianRow13_median_data,
		median_send => ao_medianRow13_median_send,
		median_ack => ao_medianRow13_median_ack,
		median_rdy => ao_medianRow13_median_rdy,
		median_count => ao_medianRow13_median_count,
		-- Clock and Reset
	clk => clocks(0),
	--clk => clocks(0),
	reset => resets(0));
	
	i_medianRow12 : component medianRow12
	port map(
		-- Instance medianRow12 Input(s)
		in1_data => ai_medianRow12_in1_data,
		in1_send => ai_medianRow12_in1_send,
		in1_ack => ai_medianRow12_in1_ack,
		in1_count => ai_medianRow12_in1_count,
		-- Instance medianRow12 Output(s)
		median_data => ao_medianRow12_median_data,
		median_send => ao_medianRow12_median_send,
		median_ack => ao_medianRow12_median_ack,
		median_rdy => ao_medianRow12_median_rdy,
		median_count => ao_medianRow12_median_count,
		-- Clock and Reset
	clk => clocks(0),
	--clk => clocks(0),
	reset => resets(0));
	
	i_medianRow11 : component medianRow11
	port map(
		-- Instance medianRow11 Input(s)
		in1_data => ai_medianRow11_in1_data,
		in1_send => ai_medianRow11_in1_send,
		in1_ack => ai_medianRow11_in1_ack,
		in1_count => ai_medianRow11_in1_count,
		-- Instance medianRow11 Output(s)
		median_data => ao_medianRow11_median_data,
		median_send => ao_medianRow11_median_send,
		median_ack => ao_medianRow11_median_ack,
		median_rdy => ao_medianRow11_median_rdy,
		median_count => ao_medianRow11_median_count,
		-- Clock and Reset
	clk => clocks(0),
	--clk => clocks(0),
	reset => resets(0));
	
	i_medianRow10 : component medianRow10
	port map(
		-- Instance medianRow10 Input(s)
		in1_data => ai_medianRow10_in1_data,
		in1_send => ai_medianRow10_in1_send,
		in1_ack => ai_medianRow10_in1_ack,
		in1_count => ai_medianRow10_in1_count,
		-- Instance medianRow10 Output(s)
		median_data => ao_medianRow10_median_data,
		median_send => ao_medianRow10_median_send,
		median_ack => ao_medianRow10_median_ack,
		median_rdy => ao_medianRow10_median_rdy,
		median_count => ao_medianRow10_median_count,
		-- Clock and Reset
	clk => clocks(0),
	--clk => clocks(0),
	reset => resets(0));
	
	i_medianRow9 : component medianRow9
	port map(
		-- Instance medianRow9 Input(s)
		in1_data => ai_medianRow9_in1_data,
		in1_send => ai_medianRow9_in1_send,
		in1_ack => ai_medianRow9_in1_ack,
		in1_count => ai_medianRow9_in1_count,
		-- Instance medianRow9 Output(s)
		median_data => ao_medianRow9_median_data,
		median_send => ao_medianRow9_median_send,
		median_ack => ao_medianRow9_median_ack,
		median_rdy => ao_medianRow9_median_rdy,
		median_count => ao_medianRow9_median_count,
		-- Clock and Reset
	clk => clocks(0),
	--clk => clocks(0),
	reset => resets(0));
	
	i_medianRow8 : component medianRow8
	port map(
		-- Instance medianRow8 Input(s)
		in1_data => ai_medianRow8_in1_data,
		in1_send => ai_medianRow8_in1_send,
		in1_ack => ai_medianRow8_in1_ack,
		in1_count => ai_medianRow8_in1_count,
		-- Instance medianRow8 Output(s)
		median_data => ao_medianRow8_median_data,
		median_send => ao_medianRow8_median_send,
		median_ack => ao_medianRow8_median_ack,
		median_rdy => ao_medianRow8_median_rdy,
		median_count => ao_medianRow8_median_count,
		-- Clock and Reset
	clk => clocks(0),
	--clk => clocks(0),
	reset => resets(0));
	
	i_medianRow7 : component medianRow7
	port map(
		-- Instance medianRow7 Input(s)
		in1_data => ai_medianRow7_in1_data,
		in1_send => ai_medianRow7_in1_send,
		in1_ack => ai_medianRow7_in1_ack,
		in1_count => ai_medianRow7_in1_count,
		-- Instance medianRow7 Output(s)
		median_data => ao_medianRow7_median_data,
		median_send => ao_medianRow7_median_send,
		median_ack => ao_medianRow7_median_ack,
		median_rdy => ao_medianRow7_median_rdy,
		median_count => ao_medianRow7_median_count,
		-- Clock and Reset
	clk => clocks(0),
	--clk => clocks(0),
	reset => resets(0));
	
	i_medianRow6 : component medianRow6
	port map(
		-- Instance medianRow6 Input(s)
		in1_data => ai_medianRow6_in1_data,
		in1_send => ai_medianRow6_in1_send,
		in1_ack => ai_medianRow6_in1_ack,
		in1_count => ai_medianRow6_in1_count,
		-- Instance medianRow6 Output(s)
		median_data => ao_medianRow6_median_data,
		median_send => ao_medianRow6_median_send,
		median_ack => ao_medianRow6_median_ack,
		median_rdy => ao_medianRow6_median_rdy,
		median_count => ao_medianRow6_median_count,
		-- Clock and Reset
	clk => clocks(0),
	--clk => clocks(0),
	reset => resets(0));
	
	i_medianRow5 : component medianRow5
	port map(
		-- Instance medianRow5 Input(s)
		in1_data => ai_medianRow5_in1_data,
		in1_send => ai_medianRow5_in1_send,
		in1_ack => ai_medianRow5_in1_ack,
		in1_count => ai_medianRow5_in1_count,
		-- Instance medianRow5 Output(s)
		median_data => ao_medianRow5_median_data,
		median_send => ao_medianRow5_median_send,
		median_ack => ao_medianRow5_median_ack,
		median_rdy => ao_medianRow5_median_rdy,
		median_count => ao_medianRow5_median_count,
		-- Clock and Reset
	clk => clocks(0),
	--clk => clocks(0),
	reset => resets(0));
	
	i_medianRow4 : component medianRow4
	port map(
		-- Instance medianRow4 Input(s)
		in1_data => ai_medianRow4_in1_data,
		in1_send => ai_medianRow4_in1_send,
		in1_ack => ai_medianRow4_in1_ack,
		in1_count => ai_medianRow4_in1_count,
		-- Instance medianRow4 Output(s)
		median_data => ao_medianRow4_median_data,
		median_send => ao_medianRow4_median_send,
		median_ack => ao_medianRow4_median_ack,
		median_rdy => ao_medianRow4_median_rdy,
		median_count => ao_medianRow4_median_count,
		-- Clock and Reset
	clk => clocks(0),
	--clk => clocks(0),
	reset => resets(0));
	
	i_medianRow3 : component medianRow3
	port map(
		-- Instance medianRow3 Input(s)
		in1_data => ai_medianRow3_in1_data,
		in1_send => ai_medianRow3_in1_send,
		in1_ack => ai_medianRow3_in1_ack,
		in1_count => ai_medianRow3_in1_count,
		-- Instance medianRow3 Output(s)
		median_data => ao_medianRow3_median_data,
		median_send => ao_medianRow3_median_send,
		median_ack => ao_medianRow3_median_ack,
		median_rdy => ao_medianRow3_median_rdy,
		median_count => ao_medianRow3_median_count,
		-- Clock and Reset
	clk => clocks(0),
	--clk => clocks(0),
	reset => resets(0));
	
	-- --------------------------------------------------------------------------
	-- Nework Input Fanouts
	-- --------------------------------------------------------------------------
	f_ni_inPort : entity SystemBuilder.Fanout(behavioral)
	generic map (fanout => 1, width => 8)
	port map(
		-- Fanout In
		In_data => ni_inPort_data,
		In_send => ni_inPort_send,
		In_ack => ni_inPort_ack,
		In_rdy => ni_inPort_rdy,
		In_count => ni_inPort_count,
		-- Fanout Out
		Out_data => nif_inPort_data,
		Out_send => nif_inPort_send,
		Out_ack => nif_inPort_ack,
		Out_rdy => nif_inPort_rdy,
		Out_count => nif_inPort_count,
		-- Clock & Reset
		clk => clocks(0),
		reset => resets(0));

	-- --------------------------------------------------------------------------
	-- Actor Output Fanouts
	-- --------------------------------------------------------------------------
	f_ao_split_out1 : entity SystemBuilder.Fanout(behavioral)
	generic map (fanout => 1, width => 8)
	port map(
		-- Fanout In
		In_data => ao_split_out1_data,
		In_send => ao_split_out1_send,
		In_ack => ao_split_out1_ack,
		In_rdy => ao_split_out1_rdy,
		In_count => ao_split_out1_count,
		-- Fanout Out
		Out_data => aof_split_out1_data,
		Out_send => aof_split_out1_send,
		Out_ack => aof_split_out1_ack,
		Out_rdy => aof_split_out1_rdy,
		Out_count => aof_split_out1_count,
		-- Clock & Reset
		clk => clocks(0),
		reset => resets(0));
	f_ao_split_out2 : entity SystemBuilder.Fanout(behavioral)
	generic map (fanout => 1, width => 8)
	port map(
		-- Fanout In
		In_data => ao_split_out2_data,
		In_send => ao_split_out2_send,
		In_ack => ao_split_out2_ack,
		In_rdy => ao_split_out2_rdy,
		In_count => ao_split_out2_count,
		-- Fanout Out
		Out_data => aof_split_out2_data,
		Out_send => aof_split_out2_send,
		Out_ack => aof_split_out2_ack,
		Out_rdy => aof_split_out2_rdy,
		Out_count => aof_split_out2_count,
		-- Clock & Reset
		clk => clocks(0),
		reset => resets(0));
	f_ao_split_out3 : entity SystemBuilder.Fanout(behavioral)
	generic map (fanout => 1, width => 8)
	port map(
		-- Fanout In
		In_data => ao_split_out3_data,
		In_send => ao_split_out3_send,
		In_ack => ao_split_out3_ack,
		In_rdy => ao_split_out3_rdy,
		In_count => ao_split_out3_count,
		-- Fanout Out
		Out_data => aof_split_out3_data,
		Out_send => aof_split_out3_send,
		Out_ack => aof_split_out3_ack,
		Out_rdy => aof_split_out3_rdy,
		Out_count => aof_split_out3_count,
		-- Clock & Reset
		clk => clocks(0),
		reset => resets(0));
	f_ao_split_out4 : entity SystemBuilder.Fanout(behavioral)
	generic map (fanout => 1, width => 8)
	port map(
		-- Fanout In
		In_data => ao_split_out4_data,
		In_send => ao_split_out4_send,
		In_ack => ao_split_out4_ack,
		In_rdy => ao_split_out4_rdy,
		In_count => ao_split_out4_count,
		-- Fanout Out
		Out_data => aof_split_out4_data,
		Out_send => aof_split_out4_send,
		Out_ack => aof_split_out4_ack,
		Out_rdy => aof_split_out4_rdy,
		Out_count => aof_split_out4_count,
		-- Clock & Reset
		clk => clocks(0),
		reset => resets(0));
	f_ao_split_out5 : entity SystemBuilder.Fanout(behavioral)
	generic map (fanout => 1, width => 8)
	port map(
		-- Fanout In
		In_data => ao_split_out5_data,
		In_send => ao_split_out5_send,
		In_ack => ao_split_out5_ack,
		In_rdy => ao_split_out5_rdy,
		In_count => ao_split_out5_count,
		-- Fanout Out
		Out_data => aof_split_out5_data,
		Out_send => aof_split_out5_send,
		Out_ack => aof_split_out5_ack,
		Out_rdy => aof_split_out5_rdy,
		Out_count => aof_split_out5_count,
		-- Clock & Reset
		clk => clocks(0),
		reset => resets(0));
	f_ao_split_out6 : entity SystemBuilder.Fanout(behavioral)
	generic map (fanout => 1, width => 8)
	port map(
		-- Fanout In
		In_data => ao_split_out6_data,
		In_send => ao_split_out6_send,
		In_ack => ao_split_out6_ack,
		In_rdy => ao_split_out6_rdy,
		In_count => ao_split_out6_count,
		-- Fanout Out
		Out_data => aof_split_out6_data,
		Out_send => aof_split_out6_send,
		Out_ack => aof_split_out6_ack,
		Out_rdy => aof_split_out6_rdy,
		Out_count => aof_split_out6_count,
		-- Clock & Reset
		clk => clocks(0),
		reset => resets(0));
	f_ao_split_out7 : entity SystemBuilder.Fanout(behavioral)
	generic map (fanout => 1, width => 8)
	port map(
		-- Fanout In
		In_data => ao_split_out7_data,
		In_send => ao_split_out7_send,
		In_ack => ao_split_out7_ack,
		In_rdy => ao_split_out7_rdy,
		In_count => ao_split_out7_count,
		-- Fanout Out
		Out_data => aof_split_out7_data,
		Out_send => aof_split_out7_send,
		Out_ack => aof_split_out7_ack,
		Out_rdy => aof_split_out7_rdy,
		Out_count => aof_split_out7_count,
		-- Clock & Reset
		clk => clocks(0),
		reset => resets(0));
	f_ao_split_out8 : entity SystemBuilder.Fanout(behavioral)
	generic map (fanout => 1, width => 8)
	port map(
		-- Fanout In
		In_data => ao_split_out8_data,
		In_send => ao_split_out8_send,
		In_ack => ao_split_out8_ack,
		In_rdy => ao_split_out8_rdy,
		In_count => ao_split_out8_count,
		-- Fanout Out
		Out_data => aof_split_out8_data,
		Out_send => aof_split_out8_send,
		Out_ack => aof_split_out8_ack,
		Out_rdy => aof_split_out8_rdy,
		Out_count => aof_split_out8_count,
		-- Clock & Reset
		clk => clocks(0),
		reset => resets(0));
	f_ao_split_out9 : entity SystemBuilder.Fanout(behavioral)
	generic map (fanout => 1, width => 8)
	port map(
		-- Fanout In
		In_data => ao_split_out9_data,
		In_send => ao_split_out9_send,
		In_ack => ao_split_out9_ack,
		In_rdy => ao_split_out9_rdy,
		In_count => ao_split_out9_count,
		-- Fanout Out
		Out_data => aof_split_out9_data,
		Out_send => aof_split_out9_send,
		Out_ack => aof_split_out9_ack,
		Out_rdy => aof_split_out9_rdy,
		Out_count => aof_split_out9_count,
		-- Clock & Reset
		clk => clocks(0),
		reset => resets(0));
	f_ao_split_out10 : entity SystemBuilder.Fanout(behavioral)
	generic map (fanout => 1, width => 8)
	port map(
		-- Fanout In
		In_data => ao_split_out10_data,
		In_send => ao_split_out10_send,
		In_ack => ao_split_out10_ack,
		In_rdy => ao_split_out10_rdy,
		In_count => ao_split_out10_count,
		-- Fanout Out
		Out_data => aof_split_out10_data,
		Out_send => aof_split_out10_send,
		Out_ack => aof_split_out10_ack,
		Out_rdy => aof_split_out10_rdy,
		Out_count => aof_split_out10_count,
		-- Clock & Reset
		clk => clocks(0),
		reset => resets(0));
	f_ao_split_out11 : entity SystemBuilder.Fanout(behavioral)
	generic map (fanout => 1, width => 8)
	port map(
		-- Fanout In
		In_data => ao_split_out11_data,
		In_send => ao_split_out11_send,
		In_ack => ao_split_out11_ack,
		In_rdy => ao_split_out11_rdy,
		In_count => ao_split_out11_count,
		-- Fanout Out
		Out_data => aof_split_out11_data,
		Out_send => aof_split_out11_send,
		Out_ack => aof_split_out11_ack,
		Out_rdy => aof_split_out11_rdy,
		Out_count => aof_split_out11_count,
		-- Clock & Reset
		clk => clocks(0),
		reset => resets(0));
	f_ao_split_out12 : entity SystemBuilder.Fanout(behavioral)
	generic map (fanout => 1, width => 8)
	port map(
		-- Fanout In
		In_data => ao_split_out12_data,
		In_send => ao_split_out12_send,
		In_ack => ao_split_out12_ack,
		In_rdy => ao_split_out12_rdy,
		In_count => ao_split_out12_count,
		-- Fanout Out
		Out_data => aof_split_out12_data,
		Out_send => aof_split_out12_send,
		Out_ack => aof_split_out12_ack,
		Out_rdy => aof_split_out12_rdy,
		Out_count => aof_split_out12_count,
		-- Clock & Reset
		clk => clocks(0),
		reset => resets(0));
	f_ao_split_out13 : entity SystemBuilder.Fanout(behavioral)
	generic map (fanout => 1, width => 8)
	port map(
		-- Fanout In
		In_data => ao_split_out13_data,
		In_send => ao_split_out13_send,
		In_ack => ao_split_out13_ack,
		In_rdy => ao_split_out13_rdy,
		In_count => ao_split_out13_count,
		-- Fanout Out
		Out_data => aof_split_out13_data,
		Out_send => aof_split_out13_send,
		Out_ack => aof_split_out13_ack,
		Out_rdy => aof_split_out13_rdy,
		Out_count => aof_split_out13_count,
		-- Clock & Reset
		clk => clocks(0),
		reset => resets(0));
	f_ao_split_out14 : entity SystemBuilder.Fanout(behavioral)
	generic map (fanout => 1, width => 8)
	port map(
		-- Fanout In
		In_data => ao_split_out14_data,
		In_send => ao_split_out14_send,
		In_ack => ao_split_out14_ack,
		In_rdy => ao_split_out14_rdy,
		In_count => ao_split_out14_count,
		-- Fanout Out
		Out_data => aof_split_out14_data,
		Out_send => aof_split_out14_send,
		Out_ack => aof_split_out14_ack,
		Out_rdy => aof_split_out14_rdy,
		Out_count => aof_split_out14_count,
		-- Clock & Reset
		clk => clocks(0),
		reset => resets(0));
	f_ao_split_out15 : entity SystemBuilder.Fanout(behavioral)
	generic map (fanout => 1, width => 8)
	port map(
		-- Fanout In
		In_data => ao_split_out15_data,
		In_send => ao_split_out15_send,
		In_ack => ao_split_out15_ack,
		In_rdy => ao_split_out15_rdy,
		In_count => ao_split_out15_count,
		-- Fanout Out
		Out_data => aof_split_out15_data,
		Out_send => aof_split_out15_send,
		Out_ack => aof_split_out15_ack,
		Out_rdy => aof_split_out15_rdy,
		Out_count => aof_split_out15_count,
		-- Clock & Reset
		clk => clocks(0),
		reset => resets(0));
	f_ao_split_out16 : entity SystemBuilder.Fanout(behavioral)
	generic map (fanout => 1, width => 8)
	port map(
		-- Fanout In
		In_data => ao_split_out16_data,
		In_send => ao_split_out16_send,
		In_ack => ao_split_out16_ack,
		In_rdy => ao_split_out16_rdy,
		In_count => ao_split_out16_count,
		-- Fanout Out
		Out_data => aof_split_out16_data,
		Out_send => aof_split_out16_send,
		Out_ack => aof_split_out16_ack,
		Out_rdy => aof_split_out16_rdy,
		Out_count => aof_split_out16_count,
		-- Clock & Reset
		clk => clocks(0),
		reset => resets(0));
	
	f_ao_join_out1 : entity SystemBuilder.Fanout(behavioral)
	generic map (fanout => 1, width => 8)
	port map(
		-- Fanout In
		In_data => ao_join_out1_data,
		In_send => ao_join_out1_send,
		In_ack => ao_join_out1_ack,
		In_rdy => ao_join_out1_rdy,
		In_count => ao_join_out1_count,
		-- Fanout Out
		Out_data => aof_join_out1_data,
		Out_send => aof_join_out1_send,
		Out_ack => aof_join_out1_ack,
		Out_rdy => aof_join_out1_rdy,
		Out_count => aof_join_out1_count,
		-- Clock & Reset
		clk => clocks(0),
		reset => resets(0));
	
	f_ao_medianRow1_median : entity SystemBuilder.Fanout(behavioral)
	generic map (fanout => 1, width => 8)
	port map(
		-- Fanout In
		In_data => ao_medianRow1_median_data,
		In_send => ao_medianRow1_median_send,
		In_ack => ao_medianRow1_median_ack,
		In_rdy => ao_medianRow1_median_rdy,
		In_count => ao_medianRow1_median_count,
		-- Fanout Out
		Out_data => aof_medianRow1_median_data,
		Out_send => aof_medianRow1_median_send,
		Out_ack => aof_medianRow1_median_ack,
		Out_rdy => aof_medianRow1_median_rdy,
		Out_count => aof_medianRow1_median_count,
		-- Clock & Reset
		clk => clocks(0),
		reset => resets(0));
	
	f_ao_medianRow2_median : entity SystemBuilder.Fanout(behavioral)
	generic map (fanout => 1, width => 8)
	port map(
		-- Fanout In
		In_data => ao_medianRow2_median_data,
		In_send => ao_medianRow2_median_send,
		In_ack => ao_medianRow2_median_ack,
		In_rdy => ao_medianRow2_median_rdy,
		In_count => ao_medianRow2_median_count,
		-- Fanout Out
		Out_data => aof_medianRow2_median_data,
		Out_send => aof_medianRow2_median_send,
		Out_ack => aof_medianRow2_median_ack,
		Out_rdy => aof_medianRow2_median_rdy,
		Out_count => aof_medianRow2_median_count,
		-- Clock & Reset
		clk => clocks(0),
		reset => resets(0));
	
	f_ao_medianRow16_median : entity SystemBuilder.Fanout(behavioral)
	generic map (fanout => 1, width => 8)
	port map(
		-- Fanout In
		In_data => ao_medianRow16_median_data,
		In_send => ao_medianRow16_median_send,
		In_ack => ao_medianRow16_median_ack,
		In_rdy => ao_medianRow16_median_rdy,
		In_count => ao_medianRow16_median_count,
		-- Fanout Out
		Out_data => aof_medianRow16_median_data,
		Out_send => aof_medianRow16_median_send,
		Out_ack => aof_medianRow16_median_ack,
		Out_rdy => aof_medianRow16_median_rdy,
		Out_count => aof_medianRow16_median_count,
		-- Clock & Reset
		clk => clocks(0),
		reset => resets(0));
	
	f_ao_medianRow15_median : entity SystemBuilder.Fanout(behavioral)
	generic map (fanout => 1, width => 8)
	port map(
		-- Fanout In
		In_data => ao_medianRow15_median_data,
		In_send => ao_medianRow15_median_send,
		In_ack => ao_medianRow15_median_ack,
		In_rdy => ao_medianRow15_median_rdy,
		In_count => ao_medianRow15_median_count,
		-- Fanout Out
		Out_data => aof_medianRow15_median_data,
		Out_send => aof_medianRow15_median_send,
		Out_ack => aof_medianRow15_median_ack,
		Out_rdy => aof_medianRow15_median_rdy,
		Out_count => aof_medianRow15_median_count,
		-- Clock & Reset
		clk => clocks(0),
		reset => resets(0));
	
	f_ao_medianRow14_median : entity SystemBuilder.Fanout(behavioral)
	generic map (fanout => 1, width => 8)
	port map(
		-- Fanout In
		In_data => ao_medianRow14_median_data,
		In_send => ao_medianRow14_median_send,
		In_ack => ao_medianRow14_median_ack,
		In_rdy => ao_medianRow14_median_rdy,
		In_count => ao_medianRow14_median_count,
		-- Fanout Out
		Out_data => aof_medianRow14_median_data,
		Out_send => aof_medianRow14_median_send,
		Out_ack => aof_medianRow14_median_ack,
		Out_rdy => aof_medianRow14_median_rdy,
		Out_count => aof_medianRow14_median_count,
		-- Clock & Reset
		clk => clocks(0),
		reset => resets(0));
	
	f_ao_medianRow13_median : entity SystemBuilder.Fanout(behavioral)
	generic map (fanout => 1, width => 8)
	port map(
		-- Fanout In
		In_data => ao_medianRow13_median_data,
		In_send => ao_medianRow13_median_send,
		In_ack => ao_medianRow13_median_ack,
		In_rdy => ao_medianRow13_median_rdy,
		In_count => ao_medianRow13_median_count,
		-- Fanout Out
		Out_data => aof_medianRow13_median_data,
		Out_send => aof_medianRow13_median_send,
		Out_ack => aof_medianRow13_median_ack,
		Out_rdy => aof_medianRow13_median_rdy,
		Out_count => aof_medianRow13_median_count,
		-- Clock & Reset
		clk => clocks(0),
		reset => resets(0));
	
	f_ao_medianRow12_median : entity SystemBuilder.Fanout(behavioral)
	generic map (fanout => 1, width => 8)
	port map(
		-- Fanout In
		In_data => ao_medianRow12_median_data,
		In_send => ao_medianRow12_median_send,
		In_ack => ao_medianRow12_median_ack,
		In_rdy => ao_medianRow12_median_rdy,
		In_count => ao_medianRow12_median_count,
		-- Fanout Out
		Out_data => aof_medianRow12_median_data,
		Out_send => aof_medianRow12_median_send,
		Out_ack => aof_medianRow12_median_ack,
		Out_rdy => aof_medianRow12_median_rdy,
		Out_count => aof_medianRow12_median_count,
		-- Clock & Reset
		clk => clocks(0),
		reset => resets(0));
	
	f_ao_medianRow11_median : entity SystemBuilder.Fanout(behavioral)
	generic map (fanout => 1, width => 8)
	port map(
		-- Fanout In
		In_data => ao_medianRow11_median_data,
		In_send => ao_medianRow11_median_send,
		In_ack => ao_medianRow11_median_ack,
		In_rdy => ao_medianRow11_median_rdy,
		In_count => ao_medianRow11_median_count,
		-- Fanout Out
		Out_data => aof_medianRow11_median_data,
		Out_send => aof_medianRow11_median_send,
		Out_ack => aof_medianRow11_median_ack,
		Out_rdy => aof_medianRow11_median_rdy,
		Out_count => aof_medianRow11_median_count,
		-- Clock & Reset
		clk => clocks(0),
		reset => resets(0));
	
	f_ao_medianRow10_median : entity SystemBuilder.Fanout(behavioral)
	generic map (fanout => 1, width => 8)
	port map(
		-- Fanout In
		In_data => ao_medianRow10_median_data,
		In_send => ao_medianRow10_median_send,
		In_ack => ao_medianRow10_median_ack,
		In_rdy => ao_medianRow10_median_rdy,
		In_count => ao_medianRow10_median_count,
		-- Fanout Out
		Out_data => aof_medianRow10_median_data,
		Out_send => aof_medianRow10_median_send,
		Out_ack => aof_medianRow10_median_ack,
		Out_rdy => aof_medianRow10_median_rdy,
		Out_count => aof_medianRow10_median_count,
		-- Clock & Reset
		clk => clocks(0),
		reset => resets(0));
	
	f_ao_medianRow9_median : entity SystemBuilder.Fanout(behavioral)
	generic map (fanout => 1, width => 8)
	port map(
		-- Fanout In
		In_data => ao_medianRow9_median_data,
		In_send => ao_medianRow9_median_send,
		In_ack => ao_medianRow9_median_ack,
		In_rdy => ao_medianRow9_median_rdy,
		In_count => ao_medianRow9_median_count,
		-- Fanout Out
		Out_data => aof_medianRow9_median_data,
		Out_send => aof_medianRow9_median_send,
		Out_ack => aof_medianRow9_median_ack,
		Out_rdy => aof_medianRow9_median_rdy,
		Out_count => aof_medianRow9_median_count,
		-- Clock & Reset
		clk => clocks(0),
		reset => resets(0));
	
	f_ao_medianRow8_median : entity SystemBuilder.Fanout(behavioral)
	generic map (fanout => 1, width => 8)
	port map(
		-- Fanout In
		In_data => ao_medianRow8_median_data,
		In_send => ao_medianRow8_median_send,
		In_ack => ao_medianRow8_median_ack,
		In_rdy => ao_medianRow8_median_rdy,
		In_count => ao_medianRow8_median_count,
		-- Fanout Out
		Out_data => aof_medianRow8_median_data,
		Out_send => aof_medianRow8_median_send,
		Out_ack => aof_medianRow8_median_ack,
		Out_rdy => aof_medianRow8_median_rdy,
		Out_count => aof_medianRow8_median_count,
		-- Clock & Reset
		clk => clocks(0),
		reset => resets(0));
	
	f_ao_medianRow7_median : entity SystemBuilder.Fanout(behavioral)
	generic map (fanout => 1, width => 8)
	port map(
		-- Fanout In
		In_data => ao_medianRow7_median_data,
		In_send => ao_medianRow7_median_send,
		In_ack => ao_medianRow7_median_ack,
		In_rdy => ao_medianRow7_median_rdy,
		In_count => ao_medianRow7_median_count,
		-- Fanout Out
		Out_data => aof_medianRow7_median_data,
		Out_send => aof_medianRow7_median_send,
		Out_ack => aof_medianRow7_median_ack,
		Out_rdy => aof_medianRow7_median_rdy,
		Out_count => aof_medianRow7_median_count,
		-- Clock & Reset
		clk => clocks(0),
		reset => resets(0));
	
	f_ao_medianRow6_median : entity SystemBuilder.Fanout(behavioral)
	generic map (fanout => 1, width => 8)
	port map(
		-- Fanout In
		In_data => ao_medianRow6_median_data,
		In_send => ao_medianRow6_median_send,
		In_ack => ao_medianRow6_median_ack,
		In_rdy => ao_medianRow6_median_rdy,
		In_count => ao_medianRow6_median_count,
		-- Fanout Out
		Out_data => aof_medianRow6_median_data,
		Out_send => aof_medianRow6_median_send,
		Out_ack => aof_medianRow6_median_ack,
		Out_rdy => aof_medianRow6_median_rdy,
		Out_count => aof_medianRow6_median_count,
		-- Clock & Reset
		clk => clocks(0),
		reset => resets(0));
	
	f_ao_medianRow5_median : entity SystemBuilder.Fanout(behavioral)
	generic map (fanout => 1, width => 8)
	port map(
		-- Fanout In
		In_data => ao_medianRow5_median_data,
		In_send => ao_medianRow5_median_send,
		In_ack => ao_medianRow5_median_ack,
		In_rdy => ao_medianRow5_median_rdy,
		In_count => ao_medianRow5_median_count,
		-- Fanout Out
		Out_data => aof_medianRow5_median_data,
		Out_send => aof_medianRow5_median_send,
		Out_ack => aof_medianRow5_median_ack,
		Out_rdy => aof_medianRow5_median_rdy,
		Out_count => aof_medianRow5_median_count,
		-- Clock & Reset
		clk => clocks(0),
		reset => resets(0));
	
	f_ao_medianRow4_median : entity SystemBuilder.Fanout(behavioral)
	generic map (fanout => 1, width => 8)
	port map(
		-- Fanout In
		In_data => ao_medianRow4_median_data,
		In_send => ao_medianRow4_median_send,
		In_ack => ao_medianRow4_median_ack,
		In_rdy => ao_medianRow4_median_rdy,
		In_count => ao_medianRow4_median_count,
		-- Fanout Out
		Out_data => aof_medianRow4_median_data,
		Out_send => aof_medianRow4_median_send,
		Out_ack => aof_medianRow4_median_ack,
		Out_rdy => aof_medianRow4_median_rdy,
		Out_count => aof_medianRow4_median_count,
		-- Clock & Reset
		clk => clocks(0),
		reset => resets(0));
	
	f_ao_medianRow3_median : entity SystemBuilder.Fanout(behavioral)
	generic map (fanout => 1, width => 8)
	port map(
		-- Fanout In
		In_data => ao_medianRow3_median_data,
		In_send => ao_medianRow3_median_send,
		In_ack => ao_medianRow3_median_ack,
		In_rdy => ao_medianRow3_median_rdy,
		In_count => ao_medianRow3_median_count,
		-- Fanout Out
		Out_data => aof_medianRow3_median_data,
		Out_send => aof_medianRow3_median_send,
		Out_ack => aof_medianRow3_median_ack,
		Out_rdy => aof_medianRow3_median_rdy,
		Out_count => aof_medianRow3_median_count,
		-- Clock & Reset
		clk => clocks(0),
		reset => resets(0));

	-- --------------------------------------------------------------------------
	-- Queues
	-- --------------------------------------------------------------------------
	q_ai_split_in1 : entity SystemBuilder.Queue(behavioral)
	generic map (length => 4096, width => 8)
	port map(
		-- Queue Out
		Out_data => ai_split_in1_data,
		Out_send => ai_split_in1_send,
		Out_ack => ai_split_in1_ack,
		Out_count => ai_split_in1_count,
		-- Queue In
		In_data => nif_inPort_data,
		In_send => nif_inPort_send(0),
		In_ack => nif_inPort_ack(0),
		In_rdy => nif_inPort_rdy(0),
		In_count => nif_inPort_count,
		-- Clock & Reset
		clk => clocks(0),
		reset => resets(0)
	);
	
	q_no_outPort : entity SystemBuilder.Queue(behavioral)
	generic map (length => 64, width => 8)
	port map(
		-- Queue Out
		Out_data => no_outPort_data,
		Out_send => no_outPort_send,
		Out_ack => no_outPort_ack,
		Out_count => no_outPort_count,
		-- Queue In
		In_data => aof_join_out1_data,
		In_send => aof_join_out1_send(0),
		In_ack => aof_join_out1_ack(0),
		In_rdy => aof_join_out1_rdy(0),
		In_count => aof_join_out1_count,
		-- Clock & Reset
		clk => clocks(0),
		reset => resets(0)
	);
	
	q_ai_medianRow6_in1 : entity SystemBuilder.Queue(behavioral)
	generic map (length => 4096, width => 8)
	port map(
		-- Queue Out
		Out_data => ai_medianRow6_in1_data,
		Out_send => ai_medianRow6_in1_send,
		Out_ack => ai_medianRow6_in1_ack,
		Out_count => ai_medianRow6_in1_count,
		-- Queue In
		In_data => aof_split_out6_data,
		In_send => aof_split_out6_send(0),
		In_ack => aof_split_out6_ack(0),
		In_rdy => aof_split_out6_rdy(0),
		In_count => aof_split_out6_count,
		-- Clock & Reset
		clk => clocks(0),
		reset => resets(0)
	);
	
	q_ai_join_in6 : entity SystemBuilder.Queue(behavioral)
	generic map (length => 4096, width => 8)
	port map(
		-- Queue Out
		Out_data => ai_join_in6_data,
		Out_send => ai_join_in6_send,
		Out_ack => ai_join_in6_ack,
		Out_count => ai_join_in6_count,
		-- Queue In
		In_data => aof_medianRow6_median_data,
		In_send => aof_medianRow6_median_send(0),
		In_ack => aof_medianRow6_median_ack(0),
		In_rdy => aof_medianRow6_median_rdy(0),
		In_count => aof_medianRow6_median_count,
		-- Clock & Reset
		clk => clocks(0),
		reset => resets(0)
	);
	
	q_ai_medianRow5_in1 : entity SystemBuilder.Queue(behavioral)
	generic map (length => 4096, width => 8)
	port map(
		-- Queue Out
		Out_data => ai_medianRow5_in1_data,
		Out_send => ai_medianRow5_in1_send,
		Out_ack => ai_medianRow5_in1_ack,
		Out_count => ai_medianRow5_in1_count,
		-- Queue In
		In_data => aof_split_out5_data,
		In_send => aof_split_out5_send(0),
		In_ack => aof_split_out5_ack(0),
		In_rdy => aof_split_out5_rdy(0),
		In_count => aof_split_out5_count,
		-- Clock & Reset
		clk => clocks(0),
		reset => resets(0)
	);
	
	q_ai_join_in5 : entity SystemBuilder.Queue(behavioral)
	generic map (length => 4096, width => 8)
	port map(
		-- Queue Out
		Out_data => ai_join_in5_data,
		Out_send => ai_join_in5_send,
		Out_ack => ai_join_in5_ack,
		Out_count => ai_join_in5_count,
		-- Queue In
		In_data => aof_medianRow5_median_data,
		In_send => aof_medianRow5_median_send(0),
		In_ack => aof_medianRow5_median_ack(0),
		In_rdy => aof_medianRow5_median_rdy(0),
		In_count => aof_medianRow5_median_count,
		-- Clock & Reset
		clk => clocks(0),
		reset => resets(0)
	);
	
	q_ai_medianRow4_in1 : entity SystemBuilder.Queue(behavioral)
	generic map (length => 4096, width => 8)
	port map(
		-- Queue Out
		Out_data => ai_medianRow4_in1_data,
		Out_send => ai_medianRow4_in1_send,
		Out_ack => ai_medianRow4_in1_ack,
		Out_count => ai_medianRow4_in1_count,
		-- Queue In
		In_data => aof_split_out4_data,
		In_send => aof_split_out4_send(0),
		In_ack => aof_split_out4_ack(0),
		In_rdy => aof_split_out4_rdy(0),
		In_count => aof_split_out4_count,
		-- Clock & Reset
		clk => clocks(0),
		reset => resets(0)
	);
	
	q_ai_join_in4 : entity SystemBuilder.Queue(behavioral)
	generic map (length => 4096, width => 8)
	port map(
		-- Queue Out
		Out_data => ai_join_in4_data,
		Out_send => ai_join_in4_send,
		Out_ack => ai_join_in4_ack,
		Out_count => ai_join_in4_count,
		-- Queue In
		In_data => aof_medianRow4_median_data,
		In_send => aof_medianRow4_median_send(0),
		In_ack => aof_medianRow4_median_ack(0),
		In_rdy => aof_medianRow4_median_rdy(0),
		In_count => aof_medianRow4_median_count,
		-- Clock & Reset
		clk => clocks(0),
		reset => resets(0)
	);
	
	q_ai_medianRow3_in1 : entity SystemBuilder.Queue(behavioral)
	generic map (length => 4096, width => 8)
	port map(
		-- Queue Out
		Out_data => ai_medianRow3_in1_data,
		Out_send => ai_medianRow3_in1_send,
		Out_ack => ai_medianRow3_in1_ack,
		Out_count => ai_medianRow3_in1_count,
		-- Queue In
		In_data => aof_split_out3_data,
		In_send => aof_split_out3_send(0),
		In_ack => aof_split_out3_ack(0),
		In_rdy => aof_split_out3_rdy(0),
		In_count => aof_split_out3_count,
		-- Clock & Reset
		clk => clocks(0),
		reset => resets(0)
	);
	
	q_ai_join_in3 : entity SystemBuilder.Queue(behavioral)
	generic map (length => 4096, width => 8)
	port map(
		-- Queue Out
		Out_data => ai_join_in3_data,
		Out_send => ai_join_in3_send,
		Out_ack => ai_join_in3_ack,
		Out_count => ai_join_in3_count,
		-- Queue In
		In_data => aof_medianRow3_median_data,
		In_send => aof_medianRow3_median_send(0),
		In_ack => aof_medianRow3_median_ack(0),
		In_rdy => aof_medianRow3_median_rdy(0),
		In_count => aof_medianRow3_median_count,
		-- Clock & Reset
		clk => clocks(0),
		reset => resets(0)
	);
	
	q_ai_medianRow7_in1 : entity SystemBuilder.Queue(behavioral)
	generic map (length => 4096, width => 8)
	port map(
		-- Queue Out
		Out_data => ai_medianRow7_in1_data,
		Out_send => ai_medianRow7_in1_send,
		Out_ack => ai_medianRow7_in1_ack,
		Out_count => ai_medianRow7_in1_count,
		-- Queue In
		In_data => aof_split_out7_data,
		In_send => aof_split_out7_send(0),
		In_ack => aof_split_out7_ack(0),
		In_rdy => aof_split_out7_rdy(0),
		In_count => aof_split_out7_count,
		-- Clock & Reset
		clk => clocks(0),
		reset => resets(0)
	);
	
	q_ai_join_in7 : entity SystemBuilder.Queue(behavioral)
	generic map (length => 4096, width => 8)
	port map(
		-- Queue Out
		Out_data => ai_join_in7_data,
		Out_send => ai_join_in7_send,
		Out_ack => ai_join_in7_ack,
		Out_count => ai_join_in7_count,
		-- Queue In
		In_data => aof_medianRow7_median_data,
		In_send => aof_medianRow7_median_send(0),
		In_ack => aof_medianRow7_median_ack(0),
		In_rdy => aof_medianRow7_median_rdy(0),
		In_count => aof_medianRow7_median_count,
		-- Clock & Reset
		clk => clocks(0),
		reset => resets(0)
	);
	
	q_ai_medianRow1_in1 : entity SystemBuilder.Queue(behavioral)
	generic map (length => 4096, width => 8)
	port map(
		-- Queue Out
		Out_data => ai_medianRow1_in1_data,
		Out_send => ai_medianRow1_in1_send,
		Out_ack => ai_medianRow1_in1_ack,
		Out_count => ai_medianRow1_in1_count,
		-- Queue In
		In_data => aof_split_out1_data,
		In_send => aof_split_out1_send(0),
		In_ack => aof_split_out1_ack(0),
		In_rdy => aof_split_out1_rdy(0),
		In_count => aof_split_out1_count,
		-- Clock & Reset
		clk => clocks(0),
		reset => resets(0)
	);
	
	q_ai_join_in1 : entity SystemBuilder.Queue(behavioral)
	generic map (length => 4096, width => 8)
	port map(
		-- Queue Out
		Out_data => ai_join_in1_data,
		Out_send => ai_join_in1_send,
		Out_ack => ai_join_in1_ack,
		Out_count => ai_join_in1_count,
		-- Queue In
		In_data => aof_medianRow1_median_data,
		In_send => aof_medianRow1_median_send(0),
		In_ack => aof_medianRow1_median_ack(0),
		In_rdy => aof_medianRow1_median_rdy(0),
		In_count => aof_medianRow1_median_count,
		-- Clock & Reset
		clk => clocks(0),
		reset => resets(0)
	);
	
	q_ai_medianRow10_in1 : entity SystemBuilder.Queue(behavioral)
	generic map (length => 4096, width => 8)
	port map(
		-- Queue Out
		Out_data => ai_medianRow10_in1_data,
		Out_send => ai_medianRow10_in1_send,
		Out_ack => ai_medianRow10_in1_ack,
		Out_count => ai_medianRow10_in1_count,
		-- Queue In
		In_data => aof_split_out10_data,
		In_send => aof_split_out10_send(0),
		In_ack => aof_split_out10_ack(0),
		In_rdy => aof_split_out10_rdy(0),
		In_count => aof_split_out10_count,
		-- Clock & Reset
		clk => clocks(0),
		reset => resets(0)
	);
	
	q_ai_join_in10 : entity SystemBuilder.Queue(behavioral)
	generic map (length => 4096, width => 8)
	port map(
		-- Queue Out
		Out_data => ai_join_in10_data,
		Out_send => ai_join_in10_send,
		Out_ack => ai_join_in10_ack,
		Out_count => ai_join_in10_count,
		-- Queue In
		In_data => aof_medianRow10_median_data,
		In_send => aof_medianRow10_median_send(0),
		In_ack => aof_medianRow10_median_ack(0),
		In_rdy => aof_medianRow10_median_rdy(0),
		In_count => aof_medianRow10_median_count,
		-- Clock & Reset
		clk => clocks(0),
		reset => resets(0)
	);
	
	q_ai_medianRow9_in1 : entity SystemBuilder.Queue(behavioral)
	generic map (length => 4096, width => 8)
	port map(
		-- Queue Out
		Out_data => ai_medianRow9_in1_data,
		Out_send => ai_medianRow9_in1_send,
		Out_ack => ai_medianRow9_in1_ack,
		Out_count => ai_medianRow9_in1_count,
		-- Queue In
		In_data => aof_split_out9_data,
		In_send => aof_split_out9_send(0),
		In_ack => aof_split_out9_ack(0),
		In_rdy => aof_split_out9_rdy(0),
		In_count => aof_split_out9_count,
		-- Clock & Reset
		clk => clocks(0),
		reset => resets(0)
	);
	
	q_ai_join_in9 : entity SystemBuilder.Queue(behavioral)
	generic map (length => 4096, width => 8)
	port map(
		-- Queue Out
		Out_data => ai_join_in9_data,
		Out_send => ai_join_in9_send,
		Out_ack => ai_join_in9_ack,
		Out_count => ai_join_in9_count,
		-- Queue In
		In_data => aof_medianRow9_median_data,
		In_send => aof_medianRow9_median_send(0),
		In_ack => aof_medianRow9_median_ack(0),
		In_rdy => aof_medianRow9_median_rdy(0),
		In_count => aof_medianRow9_median_count,
		-- Clock & Reset
		clk => clocks(0),
		reset => resets(0)
	);
	
	q_ai_medianRow8_in1 : entity SystemBuilder.Queue(behavioral)
	generic map (length => 4096, width => 8)
	port map(
		-- Queue Out
		Out_data => ai_medianRow8_in1_data,
		Out_send => ai_medianRow8_in1_send,
		Out_ack => ai_medianRow8_in1_ack,
		Out_count => ai_medianRow8_in1_count,
		-- Queue In
		In_data => aof_split_out8_data,
		In_send => aof_split_out8_send(0),
		In_ack => aof_split_out8_ack(0),
		In_rdy => aof_split_out8_rdy(0),
		In_count => aof_split_out8_count,
		-- Clock & Reset
		clk => clocks(0),
		reset => resets(0)
	);
	
	q_ai_join_in8 : entity SystemBuilder.Queue(behavioral)
	generic map (length => 4096, width => 8)
	port map(
		-- Queue Out
		Out_data => ai_join_in8_data,
		Out_send => ai_join_in8_send,
		Out_ack => ai_join_in8_ack,
		Out_count => ai_join_in8_count,
		-- Queue In
		In_data => aof_medianRow8_median_data,
		In_send => aof_medianRow8_median_send(0),
		In_ack => aof_medianRow8_median_ack(0),
		In_rdy => aof_medianRow8_median_rdy(0),
		In_count => aof_medianRow8_median_count,
		-- Clock & Reset
		clk => clocks(0),
		reset => resets(0)
	);
	
	q_ai_medianRow16_in1 : entity SystemBuilder.Queue(behavioral)
	generic map (length => 4096, width => 8)
	port map(
		-- Queue Out
		Out_data => ai_medianRow16_in1_data,
		Out_send => ai_medianRow16_in1_send,
		Out_ack => ai_medianRow16_in1_ack,
		Out_count => ai_medianRow16_in1_count,
		-- Queue In
		In_data => aof_split_out16_data,
		In_send => aof_split_out16_send(0),
		In_ack => aof_split_out16_ack(0),
		In_rdy => aof_split_out16_rdy(0),
		In_count => aof_split_out16_count,
		-- Clock & Reset
		clk => clocks(0),
		reset => resets(0)
	);
	
	q_ai_join_in16 : entity SystemBuilder.Queue(behavioral)
	generic map (length => 4096, width => 8)
	port map(
		-- Queue Out
		Out_data => ai_join_in16_data,
		Out_send => ai_join_in16_send,
		Out_ack => ai_join_in16_ack,
		Out_count => ai_join_in16_count,
		-- Queue In
		In_data => aof_medianRow16_median_data,
		In_send => aof_medianRow16_median_send(0),
		In_ack => aof_medianRow16_median_ack(0),
		In_rdy => aof_medianRow16_median_rdy(0),
		In_count => aof_medianRow16_median_count,
		-- Clock & Reset
		clk => clocks(0),
		reset => resets(0)
	);
	
	q_ai_medianRow15_in1 : entity SystemBuilder.Queue(behavioral)
	generic map (length => 4096, width => 8)
	port map(
		-- Queue Out
		Out_data => ai_medianRow15_in1_data,
		Out_send => ai_medianRow15_in1_send,
		Out_ack => ai_medianRow15_in1_ack,
		Out_count => ai_medianRow15_in1_count,
		-- Queue In
		In_data => aof_split_out15_data,
		In_send => aof_split_out15_send(0),
		In_ack => aof_split_out15_ack(0),
		In_rdy => aof_split_out15_rdy(0),
		In_count => aof_split_out15_count,
		-- Clock & Reset
		clk => clocks(0),
		reset => resets(0)
	);
	
	q_ai_join_in15 : entity SystemBuilder.Queue(behavioral)
	generic map (length => 4096, width => 8)
	port map(
		-- Queue Out
		Out_data => ai_join_in15_data,
		Out_send => ai_join_in15_send,
		Out_ack => ai_join_in15_ack,
		Out_count => ai_join_in15_count,
		-- Queue In
		In_data => aof_medianRow15_median_data,
		In_send => aof_medianRow15_median_send(0),
		In_ack => aof_medianRow15_median_ack(0),
		In_rdy => aof_medianRow15_median_rdy(0),
		In_count => aof_medianRow15_median_count,
		-- Clock & Reset
		clk => clocks(0),
		reset => resets(0)
	);
	
	q_ai_medianRow2_in1 : entity SystemBuilder.Queue(behavioral)
	generic map (length => 4096, width => 8)
	port map(
		-- Queue Out
		Out_data => ai_medianRow2_in1_data,
		Out_send => ai_medianRow2_in1_send,
		Out_ack => ai_medianRow2_in1_ack,
		Out_count => ai_medianRow2_in1_count,
		-- Queue In
		In_data => aof_split_out2_data,
		In_send => aof_split_out2_send(0),
		In_ack => aof_split_out2_ack(0),
		In_rdy => aof_split_out2_rdy(0),
		In_count => aof_split_out2_count,
		-- Clock & Reset
		clk => clocks(0),
		reset => resets(0)
	);
	
	q_ai_join_in2 : entity SystemBuilder.Queue(behavioral)
	generic map (length => 4096, width => 8)
	port map(
		-- Queue Out
		Out_data => ai_join_in2_data,
		Out_send => ai_join_in2_send,
		Out_ack => ai_join_in2_ack,
		Out_count => ai_join_in2_count,
		-- Queue In
		In_data => aof_medianRow2_median_data,
		In_send => aof_medianRow2_median_send(0),
		In_ack => aof_medianRow2_median_ack(0),
		In_rdy => aof_medianRow2_median_rdy(0),
		In_count => aof_medianRow2_median_count,
		-- Clock & Reset
		clk => clocks(0),
		reset => resets(0)
	);
	
	q_ai_medianRow12_in1 : entity SystemBuilder.Queue(behavioral)
	generic map (length => 4096, width => 8)
	port map(
		-- Queue Out
		Out_data => ai_medianRow12_in1_data,
		Out_send => ai_medianRow12_in1_send,
		Out_ack => ai_medianRow12_in1_ack,
		Out_count => ai_medianRow12_in1_count,
		-- Queue In
		In_data => aof_split_out12_data,
		In_send => aof_split_out12_send(0),
		In_ack => aof_split_out12_ack(0),
		In_rdy => aof_split_out12_rdy(0),
		In_count => aof_split_out12_count,
		-- Clock & Reset
		clk => clocks(0),
		reset => resets(0)
	);
	
	q_ai_join_in12 : entity SystemBuilder.Queue(behavioral)
	generic map (length => 4096, width => 8)
	port map(
		-- Queue Out
		Out_data => ai_join_in12_data,
		Out_send => ai_join_in12_send,
		Out_ack => ai_join_in12_ack,
		Out_count => ai_join_in12_count,
		-- Queue In
		In_data => aof_medianRow12_median_data,
		In_send => aof_medianRow12_median_send(0),
		In_ack => aof_medianRow12_median_ack(0),
		In_rdy => aof_medianRow12_median_rdy(0),
		In_count => aof_medianRow12_median_count,
		-- Clock & Reset
		clk => clocks(0),
		reset => resets(0)
	);
	
	q_ai_medianRow14_in1 : entity SystemBuilder.Queue(behavioral)
	generic map (length => 4096, width => 8)
	port map(
		-- Queue Out
		Out_data => ai_medianRow14_in1_data,
		Out_send => ai_medianRow14_in1_send,
		Out_ack => ai_medianRow14_in1_ack,
		Out_count => ai_medianRow14_in1_count,
		-- Queue In
		In_data => aof_split_out14_data,
		In_send => aof_split_out14_send(0),
		In_ack => aof_split_out14_ack(0),
		In_rdy => aof_split_out14_rdy(0),
		In_count => aof_split_out14_count,
		-- Clock & Reset
		clk => clocks(0),
		reset => resets(0)
	);
	
	q_ai_join_in14 : entity SystemBuilder.Queue(behavioral)
	generic map (length => 4096, width => 8)
	port map(
		-- Queue Out
		Out_data => ai_join_in14_data,
		Out_send => ai_join_in14_send,
		Out_ack => ai_join_in14_ack,
		Out_count => ai_join_in14_count,
		-- Queue In
		In_data => aof_medianRow14_median_data,
		In_send => aof_medianRow14_median_send(0),
		In_ack => aof_medianRow14_median_ack(0),
		In_rdy => aof_medianRow14_median_rdy(0),
		In_count => aof_medianRow14_median_count,
		-- Clock & Reset
		clk => clocks(0),
		reset => resets(0)
	);
	
	q_ai_medianRow13_in1 : entity SystemBuilder.Queue(behavioral)
	generic map (length => 4096, width => 8)
	port map(
		-- Queue Out
		Out_data => ai_medianRow13_in1_data,
		Out_send => ai_medianRow13_in1_send,
		Out_ack => ai_medianRow13_in1_ack,
		Out_count => ai_medianRow13_in1_count,
		-- Queue In
		In_data => aof_split_out13_data,
		In_send => aof_split_out13_send(0),
		In_ack => aof_split_out13_ack(0),
		In_rdy => aof_split_out13_rdy(0),
		In_count => aof_split_out13_count,
		-- Clock & Reset
		clk => clocks(0),
		reset => resets(0)
	);
	
	q_ai_join_in13 : entity SystemBuilder.Queue(behavioral)
	generic map (length => 4096, width => 8)
	port map(
		-- Queue Out
		Out_data => ai_join_in13_data,
		Out_send => ai_join_in13_send,
		Out_ack => ai_join_in13_ack,
		Out_count => ai_join_in13_count,
		-- Queue In
		In_data => aof_medianRow13_median_data,
		In_send => aof_medianRow13_median_send(0),
		In_ack => aof_medianRow13_median_ack(0),
		In_rdy => aof_medianRow13_median_rdy(0),
		In_count => aof_medianRow13_median_count,
		-- Clock & Reset
		clk => clocks(0),
		reset => resets(0)
	);
	
	q_ai_medianRow11_in1 : entity SystemBuilder.Queue(behavioral)
	generic map (length => 4096, width => 8)
	port map(
		-- Queue Out
		Out_data => ai_medianRow11_in1_data,
		Out_send => ai_medianRow11_in1_send,
		Out_ack => ai_medianRow11_in1_ack,
		Out_count => ai_medianRow11_in1_count,
		-- Queue In
		In_data => aof_split_out11_data,
		In_send => aof_split_out11_send(0),
		In_ack => aof_split_out11_ack(0),
		In_rdy => aof_split_out11_rdy(0),
		In_count => aof_split_out11_count,
		-- Clock & Reset
		clk => clocks(0),
		reset => resets(0)
	);
	
	q_ai_join_in11 : entity SystemBuilder.Queue(behavioral)
	generic map (length => 4096, width => 8)
	port map(
		-- Queue Out
		Out_data => ai_join_in11_data,
		Out_send => ai_join_in11_send,
		Out_ack => ai_join_in11_ack,
		Out_count => ai_join_in11_count,
		-- Queue In
		In_data => aof_medianRow11_median_data,
		In_send => aof_medianRow11_median_send(0),
		In_ack => aof_medianRow11_median_ack(0),
		In_rdy => aof_medianRow11_median_rdy(0),
		In_count => aof_medianRow11_median_count,
		-- Clock & Reset
		clk => clocks(0),
		reset => resets(0)
	);

	-- --------------------------------------------------------------------------
	-- Network port(s) instantiation
	-- --------------------------------------------------------------------------
	
	-- Output Port(s) Instantiation
	outPort_data <= no_outPort_data;
	outPort_send <= no_outPort_send;
	no_outPort_ack <= outPort_ack;
	no_outPort_rdy <= outPort_rdy;
	outPort_count <= no_outPort_count;
	
	-- Input Port(s) Instantiation
	ni_inPort_data <= inPort_data;
	ni_inPort_send <= inPort_send;
	inPort_ack <= ni_inPort_ack;
	inPort_rdy <= ni_inPort_rdy;
	ni_inPort_count <= inPort_count;
end architecture rtl;
-- ----------------------------------------------------------------------------
-- ----------------------------------------------------------------------------
-- ----------------------------------------------------------------------------
