// __  ___ __ ___  _ __   ___  ___ 
// \ \/ / '__/ _ \| '_ \ / _ \/ __|
//  >  <| | | (_) | | | | (_) \__ \
// /_/\_\_|  \___/|_| |_|\___/|___/
// 
// Xronos synthesizer version
// Run date: Fri 23 Feb 2018 14:28:39 +0000
// 

module medianRow1(median_COUNT, in1_ACK, median_RDY, median_SEND, in1_COUNT, in1_DATA, RESET, median_ACK, median_DATA, in1_SEND, CLK);
output	[15:0]	median_COUNT;
wire		compute_median_done;
wire		receive_go;
output		in1_ACK;
input		median_RDY;
output		median_SEND;
input	[15:0]	in1_COUNT;
wire		receive_done;
input	[7:0]	in1_DATA;
input		RESET;
input		median_ACK;
wire		compute_median_go;
output	[7:0]	median_DATA;
input		in1_SEND;
input		CLK;
wire		bus_0b300bb7_;
wire		bus_7d7be10a_;
wire	[31:0]	bus_5cd82129_;
wire		bus_647e31fc_;
wire		bus_42e6ea21_;
wire		bus_6a6bb0d8_;
wire	[2:0]	bus_1c861142_;
wire		bus_41c59c4f_;
wire	[31:0]	bus_2730c3ce_;
wire	[31:0]	bus_7b6cdbb6_;
wire		bus_5be161b9_;
wire	[31:0]	bus_76a31fa8_;
wire	[31:0]	bus_3d8e6b99_;
wire	[31:0]	bus_4a7bef6c_;
wire		bus_350f8dff_;
wire	[2:0]	bus_3064fede_;
wire		bus_1da7b235_;
wire	[31:0]	bus_75e254d1_;
wire		bus_338d584e_;
wire	[31:0]	compute_median_u616;
wire	[2:0]	compute_median_u622;
wire	[31:0]	compute_median_u624;
wire	[2:0]	compute_median_u619;
wire	[31:0]	compute_median_u618;
wire	[7:0]	compute_median_u628;
wire		compute_median;
wire		compute_median_u629;
wire	[31:0]	compute_median_u621;
wire	[31:0]	compute_median_u625;
wire	[15:0]	compute_median_u627;
wire		compute_median_u617;
wire		compute_median_u623;
wire	[2:0]	compute_median_u626;
wire		compute_median_u620;
wire		medianRow1_compute_median_instance_DONE;
wire		bus_55628fa6_;
wire	[31:0]	receive_u264;
wire	[31:0]	receive_u266;
wire		medianRow1_receive_instance_DONE;
wire		receive;
wire		receive_u265;
wire		receive_u269;
wire	[31:0]	receive_u267;
wire	[2:0]	receive_u268;
wire		bus_4d3e7ed8_;
wire	[31:0]	bus_634d3d5d_;
wire	[31:0]	bus_1e3e6f74_;
wire		bus_3da8fea0_;
wire		bus_1f08b399_;
wire		medianRow1_scheduler_instance_DONE;
wire		scheduler_u810;
wire		scheduler;
wire		scheduler_u809;
wire		scheduler_u808;
assign median_COUNT=compute_median_u627;
assign compute_median_done=bus_647e31fc_;
assign receive_go=scheduler_u810;
assign in1_ACK=receive_u269;
assign median_SEND=compute_median_u629;
assign receive_done=bus_1f08b399_;
assign compute_median_go=scheduler_u809;
assign median_DATA=compute_median_u628;
medianRow1_Kicker_56 medianRow1_Kicker_56_1(.CLK(CLK), .RESET(bus_7d7be10a_), .bus_0b300bb7_(bus_0b300bb7_));
medianRow1_globalreset_physical_4f74a827_ medianRow1_globalreset_physical_4f74a827__1(.bus_5da5a43a_(CLK), 
  .bus_40d9a120_(RESET), .bus_7d7be10a_(bus_7d7be10a_));
medianRow1_stateVar_i medianRow1_stateVar_i_1(.bus_6044264d_(CLK), .bus_10b24faa_(bus_7d7be10a_), 
  .bus_6d2f8f39_(receive), .bus_2e556fa9_(receive_u264), .bus_0fd6fc65_(compute_median), 
  .bus_63de6012_(32'h0), .bus_5cd82129_(bus_5cd82129_));
assign bus_647e31fc_=medianRow1_compute_median_instance_DONE&{1{medianRow1_compute_median_instance_DONE}};
medianRow1_simplememoryreferee_7a6c9948_ medianRow1_simplememoryreferee_7a6c9948__1(.bus_0ea46a09_(CLK), 
  .bus_45ab0853_(bus_7d7be10a_), .bus_2dad8218_(bus_4d3e7ed8_), .bus_252494e6_(bus_634d3d5d_), 
  .bus_7911494a_(receive_u265), .bus_52100183_({24'b0, receive_u267[7:0]}), .bus_1d7fc335_(receive_u266), 
  .bus_1006b671_(3'h1), .bus_03e14a17_(compute_median_u617), .bus_69ca0230_(compute_median_u623), 
  .bus_70efc095_(compute_median_u625), .bus_3be30e34_(compute_median_u624), .bus_62dc7422_(3'h1), 
  .bus_2730c3ce_(bus_2730c3ce_), .bus_7b6cdbb6_(bus_7b6cdbb6_), .bus_5be161b9_(bus_5be161b9_), 
  .bus_41c59c4f_(bus_41c59c4f_), .bus_1c861142_(bus_1c861142_), .bus_42e6ea21_(bus_42e6ea21_), 
  .bus_76a31fa8_(bus_76a31fa8_), .bus_6a6bb0d8_(bus_6a6bb0d8_));
medianRow1_simplememoryreferee_089d4c3a_ medianRow1_simplememoryreferee_089d4c3a__1(.bus_607a4016_(CLK), 
  .bus_1f9765c5_(bus_7d7be10a_), .bus_2c995c78_(bus_3da8fea0_), .bus_53cba69c_(bus_1e3e6f74_), 
  .bus_5dfe6803_(compute_median_u620), .bus_7566f228_(compute_median_u621), .bus_59289341_(3'h1), 
  .bus_3d8e6b99_(bus_3d8e6b99_), .bus_75e254d1_(bus_75e254d1_), .bus_350f8dff_(bus_350f8dff_), 
  .bus_1da7b235_(bus_1da7b235_), .bus_3064fede_(bus_3064fede_), .bus_4a7bef6c_(bus_4a7bef6c_), 
  .bus_338d584e_(bus_338d584e_));
medianRow1_compute_median medianRow1_compute_median_instance(.CLK(CLK), .RESET(bus_7d7be10a_), 
  .GO(compute_median_go), .port_63b18012_(bus_6a6bb0d8_), .port_35dc6a20_(bus_76a31fa8_), 
  .port_4f02bb9a_(bus_338d584e_), .port_64e75cb8_(bus_4a7bef6c_), .port_5c43e8fd_(bus_6a6bb0d8_), 
  .DONE(medianRow1_compute_median_instance_DONE), .RESULT(compute_median), .RESULT_u2176(compute_median_u616), 
  .RESULT_u2180(compute_median_u617), .RESULT_u2181(compute_median_u618), .RESULT_u2182(compute_median_u619), 
  .RESULT_u2177(compute_median_u620), .RESULT_u2178(compute_median_u621), .RESULT_u2179(compute_median_u622), 
  .RESULT_u2183(compute_median_u623), .RESULT_u2184(compute_median_u624), .RESULT_u2185(compute_median_u625), 
  .RESULT_u2186(compute_median_u626), .RESULT_u2187(compute_median_u627), .RESULT_u2188(compute_median_u628), 
  .RESULT_u2189(compute_median_u629));
medianRow1_stateVar_fsmState_medianRow1 medianRow1_stateVar_fsmState_medianRow1_1(.bus_49e0ee58_(CLK), 
  .bus_658bfc0d_(bus_7d7be10a_), .bus_3c83d0f9_(scheduler), .bus_488e9acd_(scheduler_u808), 
  .bus_55628fa6_(bus_55628fa6_));
medianRow1_receive medianRow1_receive_instance(.CLK(CLK), .RESET(bus_7d7be10a_), 
  .GO(receive_go), .port_7ef70fa1_(bus_5cd82129_), .port_3eb0594a_(bus_42e6ea21_), 
  .port_03204e73_(in1_DATA), .DONE(medianRow1_receive_instance_DONE), .RESULT(receive), 
  .RESULT_u2190(receive_u264), .RESULT_u2191(receive_u265), .RESULT_u2192(receive_u266), 
  .RESULT_u2193(receive_u267), .RESULT_u2194(receive_u268), .RESULT_u2195(receive_u269));
medianRow1_structuralmemory_5454fca3_ medianRow1_structuralmemory_5454fca3__1(.CLK_u90(CLK), 
  .bus_3077d8d0_(bus_7d7be10a_), .bus_59fc7e31_(bus_75e254d1_), .bus_5d043583_(3'h1), 
  .bus_341dcce4_(bus_1da7b235_), .bus_4b69a9ab_(bus_7b6cdbb6_), .bus_4795d5ee_(3'h1), 
  .bus_07aeae51_(bus_41c59c4f_), .bus_1a07a6df_(bus_5be161b9_), .bus_6f386e2b_(bus_2730c3ce_), 
  .bus_1e3e6f74_(bus_1e3e6f74_), .bus_3da8fea0_(bus_3da8fea0_), .bus_634d3d5d_(bus_634d3d5d_), 
  .bus_4d3e7ed8_(bus_4d3e7ed8_));
assign bus_1f08b399_=medianRow1_receive_instance_DONE&{1{medianRow1_receive_instance_DONE}};
medianRow1_scheduler medianRow1_scheduler_instance(.CLK(CLK), .RESET(bus_7d7be10a_), 
  .GO(bus_0b300bb7_), .port_0ae92c70_(bus_55628fa6_), .port_40b7289e_(bus_5cd82129_), 
  .port_1be92021_(receive_done), .port_059c8ae6_(compute_median_done), .port_30a7d09c_(in1_SEND), 
  .port_511d1433_(median_RDY), .DONE(medianRow1_scheduler_instance_DONE), .RESULT(scheduler), 
  .RESULT_u2196(scheduler_u808), .RESULT_u2197(scheduler_u809), .RESULT_u2198(scheduler_u810));
endmodule



module medianRow1_Kicker_56(CLK, RESET, bus_0b300bb7_);
input		CLK;
input		RESET;
output		bus_0b300bb7_;
wire		bus_44097282_;
reg		kicker_res=1'h0;
wire		bus_5025bcaf_;
reg		kicker_2=1'h0;
reg		kicker_1=1'h0;
wire		bus_563f3320_;
wire		bus_42ab521e_;
assign bus_44097282_=kicker_1&bus_5025bcaf_&bus_42ab521e_;
always @(posedge CLK)
begin
kicker_res<=bus_44097282_;
end
assign bus_5025bcaf_=~RESET;
always @(posedge CLK)
begin
kicker_2<=bus_563f3320_;
end
always @(posedge CLK)
begin
kicker_1<=bus_5025bcaf_;
end
assign bus_563f3320_=bus_5025bcaf_&kicker_1;
assign bus_42ab521e_=~kicker_2;
assign bus_0b300bb7_=kicker_res;
endmodule



module medianRow1_globalreset_physical_4f74a827_(bus_5da5a43a_, bus_40d9a120_, bus_7d7be10a_);
input		bus_5da5a43a_;
input		bus_40d9a120_;
output		bus_7d7be10a_;
wire		not_55066956_u0;
reg		final_u56=1'h1;
wire		and_68f56876_u0;
reg		sample_u56=1'h0;
reg		glitch_u56=1'h0;
wire		or_4db206f5_u0;
reg		cross_u56=1'h0;
assign not_55066956_u0=~and_68f56876_u0;
always @(posedge bus_5da5a43a_)
begin
final_u56<=not_55066956_u0;
end
assign and_68f56876_u0=cross_u56&glitch_u56;
always @(posedge bus_5da5a43a_)
begin
sample_u56<=1'h1;
end
always @(posedge bus_5da5a43a_)
begin
glitch_u56<=cross_u56;
end
assign or_4db206f5_u0=bus_40d9a120_|final_u56;
assign bus_7d7be10a_=or_4db206f5_u0;
always @(posedge bus_5da5a43a_)
begin
cross_u56<=sample_u56;
end
endmodule



module medianRow1_endianswapper_74f9dc87_(endianswapper_74f9dc87_in, endianswapper_74f9dc87_out);
input	[31:0]	endianswapper_74f9dc87_in;
output	[31:0]	endianswapper_74f9dc87_out;
assign endianswapper_74f9dc87_out=endianswapper_74f9dc87_in;
endmodule



module medianRow1_endianswapper_1496cef8_(endianswapper_1496cef8_in, endianswapper_1496cef8_out);
input	[31:0]	endianswapper_1496cef8_in;
output	[31:0]	endianswapper_1496cef8_out;
assign endianswapper_1496cef8_out=endianswapper_1496cef8_in;
endmodule



module medianRow1_stateVar_i(bus_6044264d_, bus_10b24faa_, bus_6d2f8f39_, bus_2e556fa9_, bus_0fd6fc65_, bus_63de6012_, bus_5cd82129_);
input		bus_6044264d_;
input		bus_10b24faa_;
input		bus_6d2f8f39_;
input	[31:0]	bus_2e556fa9_;
input		bus_0fd6fc65_;
input	[31:0]	bus_63de6012_;
output	[31:0]	bus_5cd82129_;
wire	[31:0]	mux_492a5b0c_u0;
wire		or_1a642e4e_u0;
wire	[31:0]	endianswapper_74f9dc87_out;
wire	[31:0]	endianswapper_1496cef8_out;
reg	[31:0]	stateVar_i_u46=32'h0;
assign mux_492a5b0c_u0=(bus_6d2f8f39_)?bus_2e556fa9_:32'h0;
assign or_1a642e4e_u0=bus_6d2f8f39_|bus_0fd6fc65_;
assign bus_5cd82129_=endianswapper_1496cef8_out;
medianRow1_endianswapper_74f9dc87_ medianRow1_endianswapper_74f9dc87__1(.endianswapper_74f9dc87_in(mux_492a5b0c_u0), 
  .endianswapper_74f9dc87_out(endianswapper_74f9dc87_out));
medianRow1_endianswapper_1496cef8_ medianRow1_endianswapper_1496cef8__1(.endianswapper_1496cef8_in(stateVar_i_u46), 
  .endianswapper_1496cef8_out(endianswapper_1496cef8_out));
always @(posedge bus_6044264d_ or posedge bus_10b24faa_)
begin
if (bus_10b24faa_)
stateVar_i_u46<=32'h0;
else if (or_1a642e4e_u0)
stateVar_i_u46<=endianswapper_74f9dc87_out;
end
endmodule



module medianRow1_simplememoryreferee_7a6c9948_(bus_0ea46a09_, bus_45ab0853_, bus_2dad8218_, bus_252494e6_, bus_7911494a_, bus_52100183_, bus_1d7fc335_, bus_1006b671_, bus_03e14a17_, bus_69ca0230_, bus_70efc095_, bus_3be30e34_, bus_62dc7422_, bus_2730c3ce_, bus_7b6cdbb6_, bus_5be161b9_, bus_41c59c4f_, bus_1c861142_, bus_42e6ea21_, bus_76a31fa8_, bus_6a6bb0d8_);
input		bus_0ea46a09_;
input		bus_45ab0853_;
input		bus_2dad8218_;
input	[31:0]	bus_252494e6_;
input		bus_7911494a_;
input	[31:0]	bus_52100183_;
input	[31:0]	bus_1d7fc335_;
input	[2:0]	bus_1006b671_;
input		bus_03e14a17_;
input		bus_69ca0230_;
input	[31:0]	bus_70efc095_;
input	[31:0]	bus_3be30e34_;
input	[2:0]	bus_62dc7422_;
output	[31:0]	bus_2730c3ce_;
output	[31:0]	bus_7b6cdbb6_;
output		bus_5be161b9_;
output		bus_41c59c4f_;
output	[2:0]	bus_1c861142_;
output		bus_42e6ea21_;
output	[31:0]	bus_76a31fa8_;
output		bus_6a6bb0d8_;
wire		or_15a5b261_u0;
wire		or_1417271f_u0;
wire		or_7c70ac5c_u0;
wire		and_41291405_u0;
wire	[31:0]	mux_6a1259c6_u0;
wire		not_6d5d8ee5_u0;
wire		not_7360cdf8_u0;
wire		or_5f401bda_u0;
wire	[31:0]	mux_53cc5a96_u0;
reg		done_qual_u216=1'h0;
wire		or_16425a3f_u0;
wire		and_08e5925b_u0;
reg		done_qual_u217_u0=1'h0;
assign or_15a5b261_u0=bus_7911494a_|done_qual_u216;
assign or_1417271f_u0=bus_7911494a_|or_16425a3f_u0;
assign or_7c70ac5c_u0=or_16425a3f_u0|done_qual_u217_u0;
assign and_41291405_u0=or_7c70ac5c_u0&bus_2dad8218_;
assign mux_6a1259c6_u0=(bus_7911494a_)?{24'b0, bus_52100183_[7:0]}:bus_70efc095_;
assign not_6d5d8ee5_u0=~bus_2dad8218_;
assign not_7360cdf8_u0=~bus_2dad8218_;
assign bus_2730c3ce_=mux_6a1259c6_u0;
assign bus_7b6cdbb6_=mux_53cc5a96_u0;
assign bus_5be161b9_=or_5f401bda_u0;
assign bus_41c59c4f_=or_1417271f_u0;
assign bus_1c861142_=3'h1;
assign bus_42e6ea21_=and_08e5925b_u0;
assign bus_76a31fa8_=bus_252494e6_;
assign bus_6a6bb0d8_=and_41291405_u0;
assign or_5f401bda_u0=bus_7911494a_|bus_69ca0230_;
assign mux_53cc5a96_u0=(bus_7911494a_)?bus_1d7fc335_:bus_3be30e34_;
always @(posedge bus_0ea46a09_)
begin
if (bus_45ab0853_)
done_qual_u216<=1'h0;
else done_qual_u216<=bus_7911494a_;
end
assign or_16425a3f_u0=bus_03e14a17_|bus_69ca0230_;
assign and_08e5925b_u0=or_15a5b261_u0&bus_2dad8218_;
always @(posedge bus_0ea46a09_)
begin
if (bus_45ab0853_)
done_qual_u217_u0<=1'h0;
else done_qual_u217_u0<=or_16425a3f_u0;
end
endmodule



module medianRow1_simplememoryreferee_089d4c3a_(bus_607a4016_, bus_1f9765c5_, bus_2c995c78_, bus_53cba69c_, bus_5dfe6803_, bus_7566f228_, bus_59289341_, bus_3d8e6b99_, bus_75e254d1_, bus_350f8dff_, bus_1da7b235_, bus_3064fede_, bus_4a7bef6c_, bus_338d584e_);
input		bus_607a4016_;
input		bus_1f9765c5_;
input		bus_2c995c78_;
input	[31:0]	bus_53cba69c_;
input		bus_5dfe6803_;
input	[31:0]	bus_7566f228_;
input	[2:0]	bus_59289341_;
output	[31:0]	bus_3d8e6b99_;
output	[31:0]	bus_75e254d1_;
output		bus_350f8dff_;
output		bus_1da7b235_;
output	[2:0]	bus_3064fede_;
output	[31:0]	bus_4a7bef6c_;
output		bus_338d584e_;
assign bus_3d8e6b99_=32'h0;
assign bus_75e254d1_=bus_7566f228_;
assign bus_350f8dff_=1'h0;
assign bus_1da7b235_=bus_5dfe6803_;
assign bus_3064fede_=3'h1;
assign bus_4a7bef6c_=bus_53cba69c_;
assign bus_338d584e_=bus_2c995c78_;
endmodule



module medianRow1_compute_median(CLK, RESET, GO, port_4f02bb9a_, port_64e75cb8_, port_63b18012_, port_35dc6a20_, port_5c43e8fd_, RESULT, RESULT_u2176, RESULT_u2177, RESULT_u2178, RESULT_u2179, RESULT_u2180, RESULT_u2181, RESULT_u2182, RESULT_u2183, RESULT_u2184, RESULT_u2185, RESULT_u2186, RESULT_u2187, RESULT_u2188, RESULT_u2189, DONE);
input		CLK;
input		RESET;
input		GO;
input		port_4f02bb9a_;
input	[31:0]	port_64e75cb8_;
input		port_63b18012_;
input	[31:0]	port_35dc6a20_;
input		port_5c43e8fd_;
output		RESULT;
output	[31:0]	RESULT_u2176;
output		RESULT_u2177;
output	[31:0]	RESULT_u2178;
output	[2:0]	RESULT_u2179;
output		RESULT_u2180;
output	[31:0]	RESULT_u2181;
output	[2:0]	RESULT_u2182;
output		RESULT_u2183;
output	[31:0]	RESULT_u2184;
output	[31:0]	RESULT_u2185;
output	[2:0]	RESULT_u2186;
output	[15:0]	RESULT_u2187;
output	[7:0]	RESULT_u2188;
output		RESULT_u2189;
output		DONE;
wire		and_u3698_u0;
wire	[15:0]	lessThan_b_unsigned;
wire	[15:0]	lessThan_a_unsigned;
wire		lessThan;
wire		andOp;
wire		not_u864_u0;
wire		and_u3699_u0;
wire		and_u3700_u0;
reg		syncEnable_u1383=1'h0;
reg	[31:0]	syncEnable_u1384_u0=32'h0;
wire signed	[32:0]	lessThan_u102_b_signed;
wire		lessThan_u102;
wire signed	[32:0]	lessThan_u102_a_signed;
wire		and_u3701_u0;
wire		and_u3702_u0;
wire		not_u865_u0;
wire		and_u3703_u0;
reg	[31:0]	syncEnable_u1385_u0=32'h0;
wire	[31:0]	add;
wire		and_u3704_u0;
wire	[31:0]	add_u650;
wire	[31:0]	add_u651;
wire		and_u3705_u0;
wire signed	[31:0]	greaterThan_b_signed;
wire		greaterThan;
wire signed	[31:0]	greaterThan_a_signed;
wire		not_u866_u0;
wire		and_u3706_u0;
wire		and_u3707_u0;
wire	[31:0]	add_u652;
wire		and_u3708_u0;
wire	[31:0]	add_u653;
wire	[31:0]	add_u654;
wire		and_u3709_u0;
wire	[31:0]	add_u655;
wire		or_u1408_u0;
reg		reg_70e636da_u0=1'h0;
wire		and_u3710_u0;
wire	[31:0]	add_u656;
wire	[31:0]	add_u657;
wire		and_u3711_u0;
reg		reg_5bd43bad_u0=1'h0;
wire		or_u1409_u0;
reg	[31:0]	syncEnable_u1386_u0=32'h0;
reg	[31:0]	syncEnable_u1387_u0=32'h0;
wire	[31:0]	mux_u1866;
wire	[31:0]	mux_u1867_u0;
wire		or_u1410_u0;
reg	[31:0]	syncEnable_u1388_u0=32'h0;
reg		reg_16e48c03_u0=1'h0;
reg	[31:0]	syncEnable_u1389_u0=32'h0;
reg	[31:0]	syncEnable_u1390_u0=32'h0;
reg	[31:0]	syncEnable_u1391_u0=32'h0;
reg		block_GO_delayed_u94=1'h0;
reg	[31:0]	syncEnable_u1392_u0=32'h0;
reg		reg_56c261e0_u0=1'h0;
reg	[31:0]	syncEnable_u1393_u0=32'h0;
wire		or_u1411_u0;
reg		reg_68a28b44_u0=1'h0;
wire	[31:0]	mux_u1868_u0;
wire		mux_u1869_u0;
wire		and_u3712_u0;
reg		reg_63a503d3_u0=1'h0;
reg		reg_56c261e0_result_delayed_u0=1'h0;
wire	[31:0]	mux_u1870_u0;
reg		syncEnable_u1394_u0=1'h0;
wire		and_u3713_u0;
reg		and_delayed_u384=1'h0;
wire	[31:0]	add_u658;
wire	[31:0]	mux_u1871_u0;
wire		or_u1412_u0;
wire		or_u1413_u0;
wire	[31:0]	mux_u1872_u0;
reg	[31:0]	syncEnable_u1395_u0=32'h0;
reg	[31:0]	syncEnable_u1396_u0=32'h0;
reg	[31:0]	syncEnable_u1397_u0=32'h0;
reg	[31:0]	syncEnable_u1398_u0=32'h0;
reg		block_GO_delayed_u95_u0=1'h0;
reg	[31:0]	syncEnable_u1399_u0=32'h0;
reg		syncEnable_u1400_u0=1'h0;
reg	[31:0]	syncEnable_u1401_u0=32'h0;
wire	[31:0]	mux_u1873_u0;
wire	[31:0]	mux_u1874_u0;
wire		mux_u1875_u0;
wire		or_u1414_u0;
wire	[31:0]	mux_u1876_u0;
wire	[31:0]	mux_u1877_u0;
wire	[31:0]	mux_u1878_u0;
wire	[15:0]	add_u659;
reg	[31:0]	latch_612240e0_reg=32'h0;
wire	[31:0]	latch_612240e0_out;
reg	[31:0]	latch_385e5778_reg=32'h0;
wire	[31:0]	latch_385e5778_out;
reg	[15:0]	syncEnable_u1402_u0=16'h0;
wire		latch_4d319ae4_out;
reg		latch_4d319ae4_reg=1'h0;
reg		reg_02a05716_u0=1'h0;
reg	[31:0]	latch_6c9252e4_reg=32'h0;
wire	[31:0]	latch_6c9252e4_out;
wire	[31:0]	latch_1aec40f6_out;
reg	[31:0]	latch_1aec40f6_reg=32'h0;
wire		scoreboard_0364fa9a_and;
wire		scoreboard_0364fa9a_resOr1;
wire		scoreboard_0364fa9a_resOr0;
wire		bus_198be270_;
reg		scoreboard_0364fa9a_reg0=1'h0;
reg		scoreboard_0364fa9a_reg1=1'h0;
wire		and_u3714_u0;
wire	[31:0]	mux_u1879_u0;
wire	[31:0]	mux_u1880_u0;
wire	[15:0]	mux_u1881_u0;
reg		loopControl_u92=1'h0;
reg	[31:0]	fbReg_tmp_row1_u44=32'h0;
wire		or_u1415_u0;
reg	[31:0]	fbReg_tmp_row0_u44=32'h0;
reg	[15:0]	fbReg_idx_u44=16'h0;
wire	[31:0]	mux_u1882_u0;
reg	[31:0]	fbReg_tmp_row_u44=32'h0;
reg	[31:0]	fbReg_tmp_u44=32'h0;
wire	[31:0]	mux_u1883_u0;
reg		syncEnable_u1403_u0=1'h0;
wire		mux_u1884_u0;
reg		fbReg_swapped_u44=1'h0;
wire		and_u3715_u0;
wire	[7:0]	simplePinWrite;
wire		simplePinWrite_u614;
wire	[15:0]	simplePinWrite_u615;
reg		reg_5e94c49f_u0=1'h0;
wire		or_u1416_u0;
wire	[31:0]	mux_u1885_u0;
reg		reg_5e94c49f_result_delayed_u0=1'h0;
assign and_u3698_u0=and_u3700_u0&or_u1415_u0;
assign lessThan_a_unsigned=mux_u1881_u0;
assign lessThan_b_unsigned=16'h65;
assign lessThan=lessThan_a_unsigned<lessThan_b_unsigned;
assign andOp=lessThan&mux_u1884_u0;
assign not_u864_u0=~andOp;
assign and_u3699_u0=or_u1415_u0&not_u864_u0;
assign and_u3700_u0=or_u1415_u0&andOp;
always @(posedge CLK)
begin
if (or_u1414_u0)
syncEnable_u1383<=mux_u1875_u0;
end
always @(posedge CLK)
begin
if (or_u1414_u0)
syncEnable_u1384_u0<=mux_u1878_u0;
end
assign lessThan_u102_a_signed={1'b0, mux_u1876_u0};
assign lessThan_u102_b_signed=33'h64;
assign lessThan_u102=lessThan_u102_a_signed<lessThan_u102_b_signed;
assign and_u3701_u0=or_u1414_u0&not_u865_u0;
assign and_u3702_u0=or_u1414_u0&lessThan_u102;
assign not_u865_u0=~lessThan_u102;
assign and_u3703_u0=and_u3702_u0&or_u1414_u0;
always @(posedge CLK)
begin
if (or_u1414_u0)
syncEnable_u1385_u0<=mux_u1873_u0;
end
assign add=mux_u1876_u0+32'h0;
assign and_u3704_u0=and_u3703_u0&port_4f02bb9a_;
assign add_u650=mux_u1876_u0+32'h1;
assign add_u651=add_u650+32'h0;
assign and_u3705_u0=and_u3703_u0&port_5c43e8fd_;
assign greaterThan_a_signed=syncEnable_u1399_u0;
assign greaterThan_b_signed=syncEnable_u1396_u0;
assign greaterThan=greaterThan_a_signed>greaterThan_b_signed;
assign not_u866_u0=~greaterThan;
assign and_u3706_u0=block_GO_delayed_u95_u0&greaterThan;
assign and_u3707_u0=block_GO_delayed_u95_u0&not_u866_u0;
assign add_u652=syncEnable_u1395_u0+32'h0;
assign and_u3708_u0=and_u3712_u0&port_4f02bb9a_;
assign add_u653=syncEnable_u1395_u0+32'h1;
assign add_u654=add_u653+32'h0;
assign and_u3709_u0=and_u3712_u0&port_5c43e8fd_;
assign add_u655=syncEnable_u1395_u0+32'h0;
assign or_u1408_u0=and_u3710_u0|RESET;
always @(posedge CLK or posedge block_GO_delayed_u94 or posedge or_u1408_u0)
begin
if (or_u1408_u0)
reg_70e636da_u0<=1'h0;
else if (block_GO_delayed_u94)
reg_70e636da_u0<=1'h1;
else reg_70e636da_u0<=reg_70e636da_u0;
end
assign and_u3710_u0=reg_70e636da_u0&port_5c43e8fd_;
assign add_u656=syncEnable_u1395_u0+32'h1;
assign add_u657=add_u656+32'h0;
assign and_u3711_u0=reg_5bd43bad_u0&port_5c43e8fd_;
always @(posedge CLK or posedge reg_16e48c03_u0 or posedge or_u1409_u0)
begin
if (or_u1409_u0)
reg_5bd43bad_u0<=1'h0;
else if (reg_16e48c03_u0)
reg_5bd43bad_u0<=1'h1;
else reg_5bd43bad_u0<=reg_5bd43bad_u0;
end
assign or_u1409_u0=and_u3711_u0|RESET;
always @(posedge CLK)
begin
if (and_u3712_u0)
syncEnable_u1386_u0<=port_35dc6a20_;
end
always @(posedge CLK)
begin
if (and_u3712_u0)
syncEnable_u1387_u0<=port_64e75cb8_;
end
assign mux_u1866=({32{block_GO_delayed_u94}}&syncEnable_u1388_u0)|({32{reg_16e48c03_u0}}&syncEnable_u1391_u0)|({32{and_u3712_u0}}&add_u654);
assign mux_u1867_u0=(block_GO_delayed_u94)?syncEnable_u1386_u0:syncEnable_u1387_u0;
assign or_u1410_u0=block_GO_delayed_u94|reg_16e48c03_u0;
always @(posedge CLK)
begin
if (and_u3712_u0)
syncEnable_u1388_u0<=add_u655;
end
always @(posedge CLK or posedge RESET)
begin
if (RESET)
reg_16e48c03_u0<=1'h0;
else reg_16e48c03_u0<=block_GO_delayed_u94;
end
always @(posedge CLK)
begin
if (and_u3712_u0)
syncEnable_u1389_u0<=port_64e75cb8_;
end
always @(posedge CLK)
begin
if (and_u3712_u0)
syncEnable_u1390_u0<=port_35dc6a20_;
end
always @(posedge CLK)
begin
if (and_u3712_u0)
syncEnable_u1391_u0<=add_u657;
end
always @(posedge CLK or posedge RESET)
begin
if (RESET)
block_GO_delayed_u94<=1'h0;
else block_GO_delayed_u94<=and_u3712_u0;
end
always @(posedge CLK)
begin
if (block_GO_delayed_u95_u0)
syncEnable_u1392_u0<=syncEnable_u1397_u0;
end
always @(posedge CLK or posedge RESET)
begin
if (RESET)
reg_56c261e0_u0<=1'h0;
else reg_56c261e0_u0<=and_delayed_u384;
end
always @(posedge CLK)
begin
if (block_GO_delayed_u95_u0)
syncEnable_u1393_u0<=syncEnable_u1401_u0;
end
assign or_u1411_u0=reg_63a503d3_u0|reg_68a28b44_u0;
always @(posedge CLK or posedge RESET)
begin
if (RESET)
reg_68a28b44_u0<=1'h0;
else reg_68a28b44_u0<=and_u3713_u0;
end
assign mux_u1868_u0=(reg_63a503d3_u0)?syncEnable_u1390_u0:syncEnable_u1393_u0;
assign mux_u1869_u0=(reg_63a503d3_u0)?1'h1:syncEnable_u1394_u0;
assign and_u3712_u0=and_u3706_u0&block_GO_delayed_u95_u0;
always @(posedge CLK or posedge RESET)
begin
if (RESET)
reg_63a503d3_u0<=1'h0;
else reg_63a503d3_u0<=reg_56c261e0_result_delayed_u0;
end
always @(posedge CLK or posedge RESET)
begin
if (RESET)
reg_56c261e0_result_delayed_u0<=1'h0;
else reg_56c261e0_result_delayed_u0<=reg_56c261e0_u0;
end
assign mux_u1870_u0=(reg_63a503d3_u0)?syncEnable_u1389_u0:syncEnable_u1392_u0;
always @(posedge CLK)
begin
if (block_GO_delayed_u95_u0)
syncEnable_u1394_u0<=syncEnable_u1400_u0;
end
assign and_u3713_u0=and_u3707_u0&block_GO_delayed_u95_u0;
always @(posedge CLK or posedge RESET)
begin
if (RESET)
and_delayed_u384<=1'h0;
else and_delayed_u384<=and_u3712_u0;
end
assign add_u658=mux_u1876_u0+32'h1;
assign mux_u1871_u0=(and_u3703_u0)?add:add_u652;
assign or_u1412_u0=and_u3703_u0|and_u3712_u0;
assign or_u1413_u0=and_u3703_u0|and_u3712_u0;
assign mux_u1872_u0=({32{or_u1410_u0}}&mux_u1866)|({32{and_u3703_u0}}&add_u651)|({32{and_u3712_u0}}&mux_u1866);
always @(posedge CLK)
begin
if (and_u3703_u0)
syncEnable_u1395_u0<=mux_u1876_u0;
end
always @(posedge CLK)
begin
if (and_u3703_u0)
syncEnable_u1396_u0<=port_35dc6a20_;
end
always @(posedge CLK)
begin
if (and_u3703_u0)
syncEnable_u1397_u0<=mux_u1877_u0;
end
always @(posedge CLK)
begin
if (and_u3703_u0)
syncEnable_u1398_u0<=add_u658;
end
always @(posedge CLK or posedge RESET)
begin
if (RESET)
block_GO_delayed_u95_u0<=1'h0;
else block_GO_delayed_u95_u0<=and_u3703_u0;
end
always @(posedge CLK)
begin
if (and_u3703_u0)
syncEnable_u1399_u0<=port_64e75cb8_;
end
always @(posedge CLK)
begin
if (and_u3703_u0)
syncEnable_u1400_u0<=mux_u1875_u0;
end
always @(posedge CLK)
begin
if (and_u3703_u0)
syncEnable_u1401_u0<=mux_u1873_u0;
end
assign mux_u1873_u0=(and_u3698_u0)?mux_u1883_u0:mux_u1868_u0;
assign mux_u1874_u0=(and_u3698_u0)?mux_u1880_u0:syncEnable_u1399_u0;
assign mux_u1875_u0=(and_u3698_u0)?1'h0:mux_u1869_u0;
assign or_u1414_u0=and_u3698_u0|or_u1411_u0;
assign mux_u1876_u0=(and_u3698_u0)?32'h0:syncEnable_u1398_u0;
assign mux_u1877_u0=(and_u3698_u0)?mux_u1882_u0:mux_u1870_u0;
assign mux_u1878_u0=(and_u3698_u0)?mux_u1879_u0:syncEnable_u1396_u0;
assign add_u659=mux_u1881_u0+16'h1;
always @(posedge CLK)
begin
if (or_u1411_u0)
latch_612240e0_reg<=mux_u1870_u0;
end
assign latch_612240e0_out=(or_u1411_u0)?mux_u1870_u0:latch_612240e0_reg;
always @(posedge CLK)
begin
if (or_u1411_u0)
latch_385e5778_reg<=syncEnable_u1399_u0;
end
assign latch_385e5778_out=(or_u1411_u0)?syncEnable_u1399_u0:latch_385e5778_reg;
always @(posedge CLK)
begin
if (and_u3698_u0)
syncEnable_u1402_u0<=add_u659;
end
assign latch_4d319ae4_out=(or_u1411_u0)?syncEnable_u1383:latch_4d319ae4_reg;
always @(posedge CLK)
begin
if (or_u1411_u0)
latch_4d319ae4_reg<=syncEnable_u1383;
end
always @(posedge CLK or posedge RESET)
begin
if (RESET)
reg_02a05716_u0<=1'h0;
else reg_02a05716_u0<=or_u1411_u0;
end
always @(posedge CLK)
begin
if (or_u1411_u0)
latch_6c9252e4_reg<=syncEnable_u1384_u0;
end
assign latch_6c9252e4_out=(or_u1411_u0)?syncEnable_u1384_u0:latch_6c9252e4_reg;
assign latch_1aec40f6_out=(or_u1411_u0)?syncEnable_u1385_u0:latch_1aec40f6_reg;
always @(posedge CLK)
begin
if (or_u1411_u0)
latch_1aec40f6_reg<=syncEnable_u1385_u0;
end
assign scoreboard_0364fa9a_and=scoreboard_0364fa9a_resOr0&scoreboard_0364fa9a_resOr1;
assign scoreboard_0364fa9a_resOr1=reg_02a05716_u0|scoreboard_0364fa9a_reg1;
assign scoreboard_0364fa9a_resOr0=or_u1411_u0|scoreboard_0364fa9a_reg0;
assign bus_198be270_=scoreboard_0364fa9a_and|RESET;
always @(posedge CLK)
begin
if (bus_198be270_)
scoreboard_0364fa9a_reg0<=1'h0;
else if (or_u1411_u0)
scoreboard_0364fa9a_reg0<=1'h1;
else scoreboard_0364fa9a_reg0<=scoreboard_0364fa9a_reg0;
end
always @(posedge CLK)
begin
if (bus_198be270_)
scoreboard_0364fa9a_reg1<=1'h0;
else if (reg_02a05716_u0)
scoreboard_0364fa9a_reg1<=1'h1;
else scoreboard_0364fa9a_reg1<=scoreboard_0364fa9a_reg1;
end
assign and_u3714_u0=and_u3699_u0&or_u1415_u0;
assign mux_u1879_u0=(GO)?32'h0:fbReg_tmp_row0_u44;
assign mux_u1880_u0=(GO)?32'h0:fbReg_tmp_row_u44;
assign mux_u1881_u0=(GO)?16'h0:fbReg_idx_u44;
always @(posedge CLK or posedge syncEnable_u1403_u0)
begin
if (syncEnable_u1403_u0)
loopControl_u92<=1'h0;
else loopControl_u92<=scoreboard_0364fa9a_and;
end
always @(posedge CLK)
begin
if (scoreboard_0364fa9a_and)
fbReg_tmp_row1_u44<=latch_1aec40f6_out;
end
assign or_u1415_u0=GO|loopControl_u92;
always @(posedge CLK)
begin
if (scoreboard_0364fa9a_and)
fbReg_tmp_row0_u44<=latch_6c9252e4_out;
end
always @(posedge CLK)
begin
if (scoreboard_0364fa9a_and)
fbReg_idx_u44<=syncEnable_u1402_u0;
end
assign mux_u1882_u0=(GO)?32'h0:fbReg_tmp_u44;
always @(posedge CLK)
begin
if (scoreboard_0364fa9a_and)
fbReg_tmp_row_u44<=latch_385e5778_out;
end
always @(posedge CLK)
begin
if (scoreboard_0364fa9a_and)
fbReg_tmp_u44<=latch_612240e0_out;
end
assign mux_u1883_u0=(GO)?32'h0:fbReg_tmp_row1_u44;
always @(posedge CLK)
begin
if (GO)
syncEnable_u1403_u0<=RESET;
end
assign mux_u1884_u0=(GO)?1'h0:fbReg_swapped_u44;
always @(posedge CLK)
begin
if (scoreboard_0364fa9a_and)
fbReg_swapped_u44<=latch_4d319ae4_out;
end
assign and_u3715_u0=reg_5e94c49f_u0&port_4f02bb9a_;
assign simplePinWrite=port_64e75cb8_[7:0];
assign simplePinWrite_u614=reg_5e94c49f_u0&{1{reg_5e94c49f_u0}};
assign simplePinWrite_u615=16'h1&{16{1'h1}};
always @(posedge CLK or posedge RESET)
begin
if (RESET)
reg_5e94c49f_u0<=1'h0;
else reg_5e94c49f_u0<=and_u3714_u0;
end
assign or_u1416_u0=or_u1412_u0|reg_5e94c49f_u0;
assign mux_u1885_u0=(or_u1412_u0)?mux_u1871_u0:32'h32;
always @(posedge CLK or posedge RESET)
begin
if (RESET)
reg_5e94c49f_result_delayed_u0<=1'h0;
else reg_5e94c49f_result_delayed_u0<=reg_5e94c49f_u0;
end
assign RESULT=GO;
assign RESULT_u2176=32'h0;
assign RESULT_u2177=or_u1416_u0;
assign RESULT_u2178=mux_u1885_u0;
assign RESULT_u2179=3'h1;
assign RESULT_u2180=or_u1413_u0;
assign RESULT_u2181=mux_u1872_u0;
assign RESULT_u2182=3'h1;
assign RESULT_u2183=or_u1410_u0;
assign RESULT_u2184=mux_u1872_u0;
assign RESULT_u2185=mux_u1867_u0;
assign RESULT_u2186=3'h1;
assign RESULT_u2187=simplePinWrite_u615;
assign RESULT_u2188=simplePinWrite;
assign RESULT_u2189=simplePinWrite_u614;
assign DONE=reg_5e94c49f_result_delayed_u0;
endmodule



module medianRow1_endianswapper_50d04fb2_(endianswapper_50d04fb2_in, endianswapper_50d04fb2_out);
input		endianswapper_50d04fb2_in;
output		endianswapper_50d04fb2_out;
assign endianswapper_50d04fb2_out=endianswapper_50d04fb2_in;
endmodule



module medianRow1_endianswapper_590e1646_(endianswapper_590e1646_in, endianswapper_590e1646_out);
input		endianswapper_590e1646_in;
output		endianswapper_590e1646_out;
assign endianswapper_590e1646_out=endianswapper_590e1646_in;
endmodule



module medianRow1_stateVar_fsmState_medianRow1(bus_49e0ee58_, bus_658bfc0d_, bus_3c83d0f9_, bus_488e9acd_, bus_55628fa6_);
input		bus_49e0ee58_;
input		bus_658bfc0d_;
input		bus_3c83d0f9_;
input		bus_488e9acd_;
output		bus_55628fa6_;
reg		stateVar_fsmState_medianRow1_u5=1'h0;
wire		endianswapper_50d04fb2_out;
wire		endianswapper_590e1646_out;
always @(posedge bus_49e0ee58_ or posedge bus_658bfc0d_)
begin
if (bus_658bfc0d_)
stateVar_fsmState_medianRow1_u5<=1'h0;
else if (bus_3c83d0f9_)
stateVar_fsmState_medianRow1_u5<=endianswapper_50d04fb2_out;
end
medianRow1_endianswapper_50d04fb2_ medianRow1_endianswapper_50d04fb2__1(.endianswapper_50d04fb2_in(bus_488e9acd_), 
  .endianswapper_50d04fb2_out(endianswapper_50d04fb2_out));
assign bus_55628fa6_=endianswapper_590e1646_out;
medianRow1_endianswapper_590e1646_ medianRow1_endianswapper_590e1646__1(.endianswapper_590e1646_in(stateVar_fsmState_medianRow1_u5), 
  .endianswapper_590e1646_out(endianswapper_590e1646_out));
endmodule



module medianRow1_receive(CLK, RESET, GO, port_7ef70fa1_, port_3eb0594a_, port_03204e73_, RESULT, RESULT_u2190, RESULT_u2191, RESULT_u2192, RESULT_u2193, RESULT_u2194, RESULT_u2195, DONE);
input		CLK;
input		RESET;
input		GO;
input	[31:0]	port_7ef70fa1_;
input		port_3eb0594a_;
input	[7:0]	port_03204e73_;
output		RESULT;
output	[31:0]	RESULT_u2190;
output		RESULT_u2191;
output	[31:0]	RESULT_u2192;
output	[31:0]	RESULT_u2193;
output	[2:0]	RESULT_u2194;
output		RESULT_u2195;
output		DONE;
wire		simplePinWrite;
wire	[31:0]	add;
reg		reg_3de97b79_u0=1'h0;
wire		or_u1417_u0;
wire		and_u3716_u0;
wire	[31:0]	add_u660;
reg		reg_7a966b35_u0=1'h0;
assign simplePinWrite=GO&{1{GO}};
assign add=port_7ef70fa1_+32'h0;
always @(posedge CLK or posedge GO or posedge or_u1417_u0)
begin
if (or_u1417_u0)
reg_3de97b79_u0<=1'h0;
else if (GO)
reg_3de97b79_u0<=1'h1;
else reg_3de97b79_u0<=reg_3de97b79_u0;
end
assign or_u1417_u0=and_u3716_u0|RESET;
assign and_u3716_u0=reg_3de97b79_u0&port_3eb0594a_;
assign add_u660=port_7ef70fa1_+32'h1;
always @(posedge CLK or posedge RESET)
begin
if (RESET)
reg_7a966b35_u0<=1'h0;
else reg_7a966b35_u0<=GO;
end
assign RESULT=GO;
assign RESULT_u2190=add_u660;
assign RESULT_u2191=GO;
assign RESULT_u2192=add;
assign RESULT_u2193={24'b0, port_03204e73_};
assign RESULT_u2194=3'h1;
assign RESULT_u2195=simplePinWrite;
assign DONE=reg_7a966b35_u0;
endmodule



module medianRow1_forge_memory_101x32_141(CLK, ENA, WEA, DINA, ENB, ADDRA, ADDRB, DOUTA, DOUTB, DONEA, DONEB);
input		CLK;
input		ENA;
input		WEA;
input	[31:0]	DINA;
input		ENB;
input	[31:0]	ADDRA;
input	[31:0]	ADDRB;
output	[31:0]	DOUTA;
output	[31:0]	DOUTB;
output		DONEA;
output		DONEB;
wire		wea_0;
wire	[31:0]	pre_douta_0;
wire	[31:0]	pre_doutb_0;
wire		wea_1;
wire	[31:0]	pre_douta_1;
wire	[31:0]	pre_doutb_1;
reg		wea_done;
reg		rea_done;
reg	[31:0]	mux_outa;
reg	[31:0]	mux_outb;
assign wea_0=WEA&(ADDRA[31:6]==26'h0);
assign wea_1=WEA&(ADDRA[31:6]==26'h1);
always @(posedge CLK)
begin
wea_done<=WEA;
end
assign DONEA=rea_done|wea_done;
always @(posedge CLK)
begin
end
assign DONEB=ENB;
always @(pre_douta_0 or pre_douta_1 or ADDRA)
begin
case (ADDRA[31:6])26'd0:mux_outa=pre_douta_0;
26'd1:mux_outa=pre_douta_1;
default:mux_outa=32'h0;
endcase end
assign DOUTA=mux_outa;
always @(pre_doutb_0 or pre_doutb_1 or ADDRB)
begin
case (ADDRB[31:6])26'd0:mux_outb=pre_doutb_0;
26'd1:mux_outb=pre_doutb_1;
default:mux_outb=32'h0;
endcase end
assign DOUTB=mux_outb;
// Memory array element: COL: 0, ROW: 0
//  Initialization of Dual Port LUT ram now done through explicit
// parameter setting.
RAM64X1D#(.INIT(64'h0000000000000000))RAM64X1D_instance_2816(.WCLK(CLK), .WE(wea_0), 
  .A0(ADDRA[0]), .A1(ADDRA[1]), .A2(ADDRA[2]), .A3(ADDRA[3]), .A4(ADDRA[4]), .A5(ADDRA[5]), 
  .D(DINA[0]), .SPO(pre_douta_0[0]), .DPRA0(ADDRB[0]), .DPRA1(ADDRB[1]), .DPRA2(ADDRB[2]), 
  .DPRA3(ADDRB[3]), .DPRA4(ADDRB[4]), .DPRA5(ADDRB[5]), .DPO(pre_doutb_0[0]));
// Memory array element: COL: 0, ROW: 1
//  Initialization of Dual Port LUT ram now done through explicit
// parameter setting.
RAM64X1D#(.INIT(64'h0000000000000000))RAM64X1D_instance_2817(.WCLK(CLK), .WE(wea_0), 
  .A0(ADDRA[0]), .A1(ADDRA[1]), .A2(ADDRA[2]), .A3(ADDRA[3]), .A4(ADDRA[4]), .A5(ADDRA[5]), 
  .D(DINA[1]), .SPO(pre_douta_0[1]), .DPRA0(ADDRB[0]), .DPRA1(ADDRB[1]), .DPRA2(ADDRB[2]), 
  .DPRA3(ADDRB[3]), .DPRA4(ADDRB[4]), .DPRA5(ADDRB[5]), .DPO(pre_doutb_0[1]));
// Memory array element: COL: 0, ROW: 2
//  Initialization of Dual Port LUT ram now done through explicit
// parameter setting.
RAM64X1D#(.INIT(64'h0000000000000000))RAM64X1D_instance_2818(.WCLK(CLK), .WE(wea_0), 
  .A0(ADDRA[0]), .A1(ADDRA[1]), .A2(ADDRA[2]), .A3(ADDRA[3]), .A4(ADDRA[4]), .A5(ADDRA[5]), 
  .D(DINA[2]), .SPO(pre_douta_0[2]), .DPRA0(ADDRB[0]), .DPRA1(ADDRB[1]), .DPRA2(ADDRB[2]), 
  .DPRA3(ADDRB[3]), .DPRA4(ADDRB[4]), .DPRA5(ADDRB[5]), .DPO(pre_doutb_0[2]));
// Memory array element: COL: 0, ROW: 3
//  Initialization of Dual Port LUT ram now done through explicit
// parameter setting.
RAM64X1D#(.INIT(64'h0000000000000000))RAM64X1D_instance_2819(.WCLK(CLK), .WE(wea_0), 
  .A0(ADDRA[0]), .A1(ADDRA[1]), .A2(ADDRA[2]), .A3(ADDRA[3]), .A4(ADDRA[4]), .A5(ADDRA[5]), 
  .D(DINA[3]), .SPO(pre_douta_0[3]), .DPRA0(ADDRB[0]), .DPRA1(ADDRB[1]), .DPRA2(ADDRB[2]), 
  .DPRA3(ADDRB[3]), .DPRA4(ADDRB[4]), .DPRA5(ADDRB[5]), .DPO(pre_doutb_0[3]));
// Memory array element: COL: 0, ROW: 4
//  Initialization of Dual Port LUT ram now done through explicit
// parameter setting.
RAM64X1D#(.INIT(64'h0000000000000000))RAM64X1D_instance_2820(.WCLK(CLK), .WE(wea_0), 
  .A0(ADDRA[0]), .A1(ADDRA[1]), .A2(ADDRA[2]), .A3(ADDRA[3]), .A4(ADDRA[4]), .A5(ADDRA[5]), 
  .D(DINA[4]), .SPO(pre_douta_0[4]), .DPRA0(ADDRB[0]), .DPRA1(ADDRB[1]), .DPRA2(ADDRB[2]), 
  .DPRA3(ADDRB[3]), .DPRA4(ADDRB[4]), .DPRA5(ADDRB[5]), .DPO(pre_doutb_0[4]));
// Memory array element: COL: 0, ROW: 5
//  Initialization of Dual Port LUT ram now done through explicit
// parameter setting.
RAM64X1D#(.INIT(64'h0000000000000000))RAM64X1D_instance_2821(.WCLK(CLK), .WE(wea_0), 
  .A0(ADDRA[0]), .A1(ADDRA[1]), .A2(ADDRA[2]), .A3(ADDRA[3]), .A4(ADDRA[4]), .A5(ADDRA[5]), 
  .D(DINA[5]), .SPO(pre_douta_0[5]), .DPRA0(ADDRB[0]), .DPRA1(ADDRB[1]), .DPRA2(ADDRB[2]), 
  .DPRA3(ADDRB[3]), .DPRA4(ADDRB[4]), .DPRA5(ADDRB[5]), .DPO(pre_doutb_0[5]));
// Memory array element: COL: 0, ROW: 6
//  Initialization of Dual Port LUT ram now done through explicit
// parameter setting.
RAM64X1D#(.INIT(64'h0000000000000000))RAM64X1D_instance_2822(.WCLK(CLK), .WE(wea_0), 
  .A0(ADDRA[0]), .A1(ADDRA[1]), .A2(ADDRA[2]), .A3(ADDRA[3]), .A4(ADDRA[4]), .A5(ADDRA[5]), 
  .D(DINA[6]), .SPO(pre_douta_0[6]), .DPRA0(ADDRB[0]), .DPRA1(ADDRB[1]), .DPRA2(ADDRB[2]), 
  .DPRA3(ADDRB[3]), .DPRA4(ADDRB[4]), .DPRA5(ADDRB[5]), .DPO(pre_doutb_0[6]));
// Memory array element: COL: 0, ROW: 7
//  Initialization of Dual Port LUT ram now done through explicit
// parameter setting.
RAM64X1D#(.INIT(64'h0000000000000000))RAM64X1D_instance_2823(.WCLK(CLK), .WE(wea_0), 
  .A0(ADDRA[0]), .A1(ADDRA[1]), .A2(ADDRA[2]), .A3(ADDRA[3]), .A4(ADDRA[4]), .A5(ADDRA[5]), 
  .D(DINA[7]), .SPO(pre_douta_0[7]), .DPRA0(ADDRB[0]), .DPRA1(ADDRB[1]), .DPRA2(ADDRB[2]), 
  .DPRA3(ADDRB[3]), .DPRA4(ADDRB[4]), .DPRA5(ADDRB[5]), .DPO(pre_doutb_0[7]));
// Memory array element: COL: 0, ROW: 8
//  Initialization of Dual Port LUT ram now done through explicit
// parameter setting.
RAM64X1D#(.INIT(64'h0000000000000000))RAM64X1D_instance_2824(.WCLK(CLK), .WE(wea_0), 
  .A0(ADDRA[0]), .A1(ADDRA[1]), .A2(ADDRA[2]), .A3(ADDRA[3]), .A4(ADDRA[4]), .A5(ADDRA[5]), 
  .D(DINA[8]), .SPO(pre_douta_0[8]), .DPRA0(ADDRB[0]), .DPRA1(ADDRB[1]), .DPRA2(ADDRB[2]), 
  .DPRA3(ADDRB[3]), .DPRA4(ADDRB[4]), .DPRA5(ADDRB[5]), .DPO(pre_doutb_0[8]));
// Memory array element: COL: 0, ROW: 9
//  Initialization of Dual Port LUT ram now done through explicit
// parameter setting.
RAM64X1D#(.INIT(64'h0000000000000000))RAM64X1D_instance_2825(.WCLK(CLK), .WE(wea_0), 
  .A0(ADDRA[0]), .A1(ADDRA[1]), .A2(ADDRA[2]), .A3(ADDRA[3]), .A4(ADDRA[4]), .A5(ADDRA[5]), 
  .D(DINA[9]), .SPO(pre_douta_0[9]), .DPRA0(ADDRB[0]), .DPRA1(ADDRB[1]), .DPRA2(ADDRB[2]), 
  .DPRA3(ADDRB[3]), .DPRA4(ADDRB[4]), .DPRA5(ADDRB[5]), .DPO(pre_doutb_0[9]));
// Memory array element: COL: 0, ROW: 10
//  Initialization of Dual Port LUT ram now done through explicit
// parameter setting.
RAM64X1D#(.INIT(64'h0000000000000000))RAM64X1D_instance_2826(.WCLK(CLK), .WE(wea_0), 
  .A0(ADDRA[0]), .A1(ADDRA[1]), .A2(ADDRA[2]), .A3(ADDRA[3]), .A4(ADDRA[4]), .A5(ADDRA[5]), 
  .D(DINA[10]), .SPO(pre_douta_0[10]), .DPRA0(ADDRB[0]), .DPRA1(ADDRB[1]), .DPRA2(ADDRB[2]), 
  .DPRA3(ADDRB[3]), .DPRA4(ADDRB[4]), .DPRA5(ADDRB[5]), .DPO(pre_doutb_0[10]));
// Memory array element: COL: 0, ROW: 11
//  Initialization of Dual Port LUT ram now done through explicit
// parameter setting.
RAM64X1D#(.INIT(64'h0000000000000000))RAM64X1D_instance_2827(.WCLK(CLK), .WE(wea_0), 
  .A0(ADDRA[0]), .A1(ADDRA[1]), .A2(ADDRA[2]), .A3(ADDRA[3]), .A4(ADDRA[4]), .A5(ADDRA[5]), 
  .D(DINA[11]), .SPO(pre_douta_0[11]), .DPRA0(ADDRB[0]), .DPRA1(ADDRB[1]), .DPRA2(ADDRB[2]), 
  .DPRA3(ADDRB[3]), .DPRA4(ADDRB[4]), .DPRA5(ADDRB[5]), .DPO(pre_doutb_0[11]));
// Memory array element: COL: 0, ROW: 12
//  Initialization of Dual Port LUT ram now done through explicit
// parameter setting.
RAM64X1D#(.INIT(64'h0000000000000000))RAM64X1D_instance_2828(.WCLK(CLK), .WE(wea_0), 
  .A0(ADDRA[0]), .A1(ADDRA[1]), .A2(ADDRA[2]), .A3(ADDRA[3]), .A4(ADDRA[4]), .A5(ADDRA[5]), 
  .D(DINA[12]), .SPO(pre_douta_0[12]), .DPRA0(ADDRB[0]), .DPRA1(ADDRB[1]), .DPRA2(ADDRB[2]), 
  .DPRA3(ADDRB[3]), .DPRA4(ADDRB[4]), .DPRA5(ADDRB[5]), .DPO(pre_doutb_0[12]));
// Memory array element: COL: 0, ROW: 13
//  Initialization of Dual Port LUT ram now done through explicit
// parameter setting.
RAM64X1D#(.INIT(64'h0000000000000000))RAM64X1D_instance_2829(.WCLK(CLK), .WE(wea_0), 
  .A0(ADDRA[0]), .A1(ADDRA[1]), .A2(ADDRA[2]), .A3(ADDRA[3]), .A4(ADDRA[4]), .A5(ADDRA[5]), 
  .D(DINA[13]), .SPO(pre_douta_0[13]), .DPRA0(ADDRB[0]), .DPRA1(ADDRB[1]), .DPRA2(ADDRB[2]), 
  .DPRA3(ADDRB[3]), .DPRA4(ADDRB[4]), .DPRA5(ADDRB[5]), .DPO(pre_doutb_0[13]));
// Memory array element: COL: 0, ROW: 14
//  Initialization of Dual Port LUT ram now done through explicit
// parameter setting.
RAM64X1D#(.INIT(64'h0000000000000000))RAM64X1D_instance_2830(.WCLK(CLK), .WE(wea_0), 
  .A0(ADDRA[0]), .A1(ADDRA[1]), .A2(ADDRA[2]), .A3(ADDRA[3]), .A4(ADDRA[4]), .A5(ADDRA[5]), 
  .D(DINA[14]), .SPO(pre_douta_0[14]), .DPRA0(ADDRB[0]), .DPRA1(ADDRB[1]), .DPRA2(ADDRB[2]), 
  .DPRA3(ADDRB[3]), .DPRA4(ADDRB[4]), .DPRA5(ADDRB[5]), .DPO(pre_doutb_0[14]));
// Memory array element: COL: 0, ROW: 15
//  Initialization of Dual Port LUT ram now done through explicit
// parameter setting.
RAM64X1D#(.INIT(64'h0000000000000000))RAM64X1D_instance_2831(.WCLK(CLK), .WE(wea_0), 
  .A0(ADDRA[0]), .A1(ADDRA[1]), .A2(ADDRA[2]), .A3(ADDRA[3]), .A4(ADDRA[4]), .A5(ADDRA[5]), 
  .D(DINA[15]), .SPO(pre_douta_0[15]), .DPRA0(ADDRB[0]), .DPRA1(ADDRB[1]), .DPRA2(ADDRB[2]), 
  .DPRA3(ADDRB[3]), .DPRA4(ADDRB[4]), .DPRA5(ADDRB[5]), .DPO(pre_doutb_0[15]));
// Memory array element: COL: 0, ROW: 16
//  Initialization of Dual Port LUT ram now done through explicit
// parameter setting.
RAM64X1D#(.INIT(64'h0000000000000000))RAM64X1D_instance_2832(.WCLK(CLK), .WE(wea_0), 
  .A0(ADDRA[0]), .A1(ADDRA[1]), .A2(ADDRA[2]), .A3(ADDRA[3]), .A4(ADDRA[4]), .A5(ADDRA[5]), 
  .D(DINA[16]), .SPO(pre_douta_0[16]), .DPRA0(ADDRB[0]), .DPRA1(ADDRB[1]), .DPRA2(ADDRB[2]), 
  .DPRA3(ADDRB[3]), .DPRA4(ADDRB[4]), .DPRA5(ADDRB[5]), .DPO(pre_doutb_0[16]));
// Memory array element: COL: 0, ROW: 17
//  Initialization of Dual Port LUT ram now done through explicit
// parameter setting.
RAM64X1D#(.INIT(64'h0000000000000000))RAM64X1D_instance_2833(.WCLK(CLK), .WE(wea_0), 
  .A0(ADDRA[0]), .A1(ADDRA[1]), .A2(ADDRA[2]), .A3(ADDRA[3]), .A4(ADDRA[4]), .A5(ADDRA[5]), 
  .D(DINA[17]), .SPO(pre_douta_0[17]), .DPRA0(ADDRB[0]), .DPRA1(ADDRB[1]), .DPRA2(ADDRB[2]), 
  .DPRA3(ADDRB[3]), .DPRA4(ADDRB[4]), .DPRA5(ADDRB[5]), .DPO(pre_doutb_0[17]));
// Memory array element: COL: 0, ROW: 18
//  Initialization of Dual Port LUT ram now done through explicit
// parameter setting.
RAM64X1D#(.INIT(64'h0000000000000000))RAM64X1D_instance_2834(.WCLK(CLK), .WE(wea_0), 
  .A0(ADDRA[0]), .A1(ADDRA[1]), .A2(ADDRA[2]), .A3(ADDRA[3]), .A4(ADDRA[4]), .A5(ADDRA[5]), 
  .D(DINA[18]), .SPO(pre_douta_0[18]), .DPRA0(ADDRB[0]), .DPRA1(ADDRB[1]), .DPRA2(ADDRB[2]), 
  .DPRA3(ADDRB[3]), .DPRA4(ADDRB[4]), .DPRA5(ADDRB[5]), .DPO(pre_doutb_0[18]));
// Memory array element: COL: 0, ROW: 19
//  Initialization of Dual Port LUT ram now done through explicit
// parameter setting.
RAM64X1D#(.INIT(64'h0000000000000000))RAM64X1D_instance_2835(.WCLK(CLK), .WE(wea_0), 
  .A0(ADDRA[0]), .A1(ADDRA[1]), .A2(ADDRA[2]), .A3(ADDRA[3]), .A4(ADDRA[4]), .A5(ADDRA[5]), 
  .D(DINA[19]), .SPO(pre_douta_0[19]), .DPRA0(ADDRB[0]), .DPRA1(ADDRB[1]), .DPRA2(ADDRB[2]), 
  .DPRA3(ADDRB[3]), .DPRA4(ADDRB[4]), .DPRA5(ADDRB[5]), .DPO(pre_doutb_0[19]));
// Memory array element: COL: 0, ROW: 20
//  Initialization of Dual Port LUT ram now done through explicit
// parameter setting.
RAM64X1D#(.INIT(64'h0000000000000000))RAM64X1D_instance_2836(.WCLK(CLK), .WE(wea_0), 
  .A0(ADDRA[0]), .A1(ADDRA[1]), .A2(ADDRA[2]), .A3(ADDRA[3]), .A4(ADDRA[4]), .A5(ADDRA[5]), 
  .D(DINA[20]), .SPO(pre_douta_0[20]), .DPRA0(ADDRB[0]), .DPRA1(ADDRB[1]), .DPRA2(ADDRB[2]), 
  .DPRA3(ADDRB[3]), .DPRA4(ADDRB[4]), .DPRA5(ADDRB[5]), .DPO(pre_doutb_0[20]));
// Memory array element: COL: 0, ROW: 21
//  Initialization of Dual Port LUT ram now done through explicit
// parameter setting.
RAM64X1D#(.INIT(64'h0000000000000000))RAM64X1D_instance_2837(.WCLK(CLK), .WE(wea_0), 
  .A0(ADDRA[0]), .A1(ADDRA[1]), .A2(ADDRA[2]), .A3(ADDRA[3]), .A4(ADDRA[4]), .A5(ADDRA[5]), 
  .D(DINA[21]), .SPO(pre_douta_0[21]), .DPRA0(ADDRB[0]), .DPRA1(ADDRB[1]), .DPRA2(ADDRB[2]), 
  .DPRA3(ADDRB[3]), .DPRA4(ADDRB[4]), .DPRA5(ADDRB[5]), .DPO(pre_doutb_0[21]));
// Memory array element: COL: 0, ROW: 22
//  Initialization of Dual Port LUT ram now done through explicit
// parameter setting.
RAM64X1D#(.INIT(64'h0000000000000000))RAM64X1D_instance_2838(.WCLK(CLK), .WE(wea_0), 
  .A0(ADDRA[0]), .A1(ADDRA[1]), .A2(ADDRA[2]), .A3(ADDRA[3]), .A4(ADDRA[4]), .A5(ADDRA[5]), 
  .D(DINA[22]), .SPO(pre_douta_0[22]), .DPRA0(ADDRB[0]), .DPRA1(ADDRB[1]), .DPRA2(ADDRB[2]), 
  .DPRA3(ADDRB[3]), .DPRA4(ADDRB[4]), .DPRA5(ADDRB[5]), .DPO(pre_doutb_0[22]));
// Memory array element: COL: 0, ROW: 23
//  Initialization of Dual Port LUT ram now done through explicit
// parameter setting.
RAM64X1D#(.INIT(64'h0000000000000000))RAM64X1D_instance_2839(.WCLK(CLK), .WE(wea_0), 
  .A0(ADDRA[0]), .A1(ADDRA[1]), .A2(ADDRA[2]), .A3(ADDRA[3]), .A4(ADDRA[4]), .A5(ADDRA[5]), 
  .D(DINA[23]), .SPO(pre_douta_0[23]), .DPRA0(ADDRB[0]), .DPRA1(ADDRB[1]), .DPRA2(ADDRB[2]), 
  .DPRA3(ADDRB[3]), .DPRA4(ADDRB[4]), .DPRA5(ADDRB[5]), .DPO(pre_doutb_0[23]));
// Memory array element: COL: 0, ROW: 24
//  Initialization of Dual Port LUT ram now done through explicit
// parameter setting.
RAM64X1D#(.INIT(64'h0000000000000000))RAM64X1D_instance_2840(.WCLK(CLK), .WE(wea_0), 
  .A0(ADDRA[0]), .A1(ADDRA[1]), .A2(ADDRA[2]), .A3(ADDRA[3]), .A4(ADDRA[4]), .A5(ADDRA[5]), 
  .D(DINA[24]), .SPO(pre_douta_0[24]), .DPRA0(ADDRB[0]), .DPRA1(ADDRB[1]), .DPRA2(ADDRB[2]), 
  .DPRA3(ADDRB[3]), .DPRA4(ADDRB[4]), .DPRA5(ADDRB[5]), .DPO(pre_doutb_0[24]));
// Memory array element: COL: 0, ROW: 25
//  Initialization of Dual Port LUT ram now done through explicit
// parameter setting.
RAM64X1D#(.INIT(64'h0000000000000000))RAM64X1D_instance_2841(.WCLK(CLK), .WE(wea_0), 
  .A0(ADDRA[0]), .A1(ADDRA[1]), .A2(ADDRA[2]), .A3(ADDRA[3]), .A4(ADDRA[4]), .A5(ADDRA[5]), 
  .D(DINA[25]), .SPO(pre_douta_0[25]), .DPRA0(ADDRB[0]), .DPRA1(ADDRB[1]), .DPRA2(ADDRB[2]), 
  .DPRA3(ADDRB[3]), .DPRA4(ADDRB[4]), .DPRA5(ADDRB[5]), .DPO(pre_doutb_0[25]));
// Memory array element: COL: 0, ROW: 26
//  Initialization of Dual Port LUT ram now done through explicit
// parameter setting.
RAM64X1D#(.INIT(64'h0000000000000000))RAM64X1D_instance_2842(.WCLK(CLK), .WE(wea_0), 
  .A0(ADDRA[0]), .A1(ADDRA[1]), .A2(ADDRA[2]), .A3(ADDRA[3]), .A4(ADDRA[4]), .A5(ADDRA[5]), 
  .D(DINA[26]), .SPO(pre_douta_0[26]), .DPRA0(ADDRB[0]), .DPRA1(ADDRB[1]), .DPRA2(ADDRB[2]), 
  .DPRA3(ADDRB[3]), .DPRA4(ADDRB[4]), .DPRA5(ADDRB[5]), .DPO(pre_doutb_0[26]));
// Memory array element: COL: 0, ROW: 27
//  Initialization of Dual Port LUT ram now done through explicit
// parameter setting.
RAM64X1D#(.INIT(64'h0000000000000000))RAM64X1D_instance_2843(.WCLK(CLK), .WE(wea_0), 
  .A0(ADDRA[0]), .A1(ADDRA[1]), .A2(ADDRA[2]), .A3(ADDRA[3]), .A4(ADDRA[4]), .A5(ADDRA[5]), 
  .D(DINA[27]), .SPO(pre_douta_0[27]), .DPRA0(ADDRB[0]), .DPRA1(ADDRB[1]), .DPRA2(ADDRB[2]), 
  .DPRA3(ADDRB[3]), .DPRA4(ADDRB[4]), .DPRA5(ADDRB[5]), .DPO(pre_doutb_0[27]));
// Memory array element: COL: 0, ROW: 28
//  Initialization of Dual Port LUT ram now done through explicit
// parameter setting.
RAM64X1D#(.INIT(64'h0000000000000000))RAM64X1D_instance_2844(.WCLK(CLK), .WE(wea_0), 
  .A0(ADDRA[0]), .A1(ADDRA[1]), .A2(ADDRA[2]), .A3(ADDRA[3]), .A4(ADDRA[4]), .A5(ADDRA[5]), 
  .D(DINA[28]), .SPO(pre_douta_0[28]), .DPRA0(ADDRB[0]), .DPRA1(ADDRB[1]), .DPRA2(ADDRB[2]), 
  .DPRA3(ADDRB[3]), .DPRA4(ADDRB[4]), .DPRA5(ADDRB[5]), .DPO(pre_doutb_0[28]));
// Memory array element: COL: 0, ROW: 29
//  Initialization of Dual Port LUT ram now done through explicit
// parameter setting.
RAM64X1D#(.INIT(64'h0000000000000000))RAM64X1D_instance_2845(.WCLK(CLK), .WE(wea_0), 
  .A0(ADDRA[0]), .A1(ADDRA[1]), .A2(ADDRA[2]), .A3(ADDRA[3]), .A4(ADDRA[4]), .A5(ADDRA[5]), 
  .D(DINA[29]), .SPO(pre_douta_0[29]), .DPRA0(ADDRB[0]), .DPRA1(ADDRB[1]), .DPRA2(ADDRB[2]), 
  .DPRA3(ADDRB[3]), .DPRA4(ADDRB[4]), .DPRA5(ADDRB[5]), .DPO(pre_doutb_0[29]));
// Memory array element: COL: 0, ROW: 30
//  Initialization of Dual Port LUT ram now done through explicit
// parameter setting.
RAM64X1D#(.INIT(64'h0000000000000000))RAM64X1D_instance_2846(.WCLK(CLK), .WE(wea_0), 
  .A0(ADDRA[0]), .A1(ADDRA[1]), .A2(ADDRA[2]), .A3(ADDRA[3]), .A4(ADDRA[4]), .A5(ADDRA[5]), 
  .D(DINA[30]), .SPO(pre_douta_0[30]), .DPRA0(ADDRB[0]), .DPRA1(ADDRB[1]), .DPRA2(ADDRB[2]), 
  .DPRA3(ADDRB[3]), .DPRA4(ADDRB[4]), .DPRA5(ADDRB[5]), .DPO(pre_doutb_0[30]));
// Memory array element: COL: 0, ROW: 31
//  Initialization of Dual Port LUT ram now done through explicit
// parameter setting.
RAM64X1D#(.INIT(64'h0000000000000000))RAM64X1D_instance_2847(.WCLK(CLK), .WE(wea_0), 
  .A0(ADDRA[0]), .A1(ADDRA[1]), .A2(ADDRA[2]), .A3(ADDRA[3]), .A4(ADDRA[4]), .A5(ADDRA[5]), 
  .D(DINA[31]), .SPO(pre_douta_0[31]), .DPRA0(ADDRB[0]), .DPRA1(ADDRB[1]), .DPRA2(ADDRB[2]), 
  .DPRA3(ADDRB[3]), .DPRA4(ADDRB[4]), .DPRA5(ADDRB[5]), .DPO(pre_doutb_0[31]));
// Memory array element: COL: 1, ROW: 0
//  Initialization of Dual Port LUT ram now done through explicit
// parameter setting.
RAM64X1D#(.INIT(64'h0000000000000000))RAM64X1D_instance_2848(.WCLK(CLK), .WE(wea_1), 
  .A0(ADDRA[0]), .A1(ADDRA[1]), .A2(ADDRA[2]), .A3(ADDRA[3]), .A4(ADDRA[4]), .A5(ADDRA[5]), 
  .D(DINA[0]), .SPO(pre_douta_1[0]), .DPRA0(ADDRB[0]), .DPRA1(ADDRB[1]), .DPRA2(ADDRB[2]), 
  .DPRA3(ADDRB[3]), .DPRA4(ADDRB[4]), .DPRA5(ADDRB[5]), .DPO(pre_doutb_1[0]));
// Memory array element: COL: 1, ROW: 1
//  Initialization of Dual Port LUT ram now done through explicit
// parameter setting.
RAM64X1D#(.INIT(64'h0000000000000000))RAM64X1D_instance_2849(.WCLK(CLK), .WE(wea_1), 
  .A0(ADDRA[0]), .A1(ADDRA[1]), .A2(ADDRA[2]), .A3(ADDRA[3]), .A4(ADDRA[4]), .A5(ADDRA[5]), 
  .D(DINA[1]), .SPO(pre_douta_1[1]), .DPRA0(ADDRB[0]), .DPRA1(ADDRB[1]), .DPRA2(ADDRB[2]), 
  .DPRA3(ADDRB[3]), .DPRA4(ADDRB[4]), .DPRA5(ADDRB[5]), .DPO(pre_doutb_1[1]));
// Memory array element: COL: 1, ROW: 2
//  Initialization of Dual Port LUT ram now done through explicit
// parameter setting.
RAM64X1D#(.INIT(64'h0000000000000000))RAM64X1D_instance_2850(.WCLK(CLK), .WE(wea_1), 
  .A0(ADDRA[0]), .A1(ADDRA[1]), .A2(ADDRA[2]), .A3(ADDRA[3]), .A4(ADDRA[4]), .A5(ADDRA[5]), 
  .D(DINA[2]), .SPO(pre_douta_1[2]), .DPRA0(ADDRB[0]), .DPRA1(ADDRB[1]), .DPRA2(ADDRB[2]), 
  .DPRA3(ADDRB[3]), .DPRA4(ADDRB[4]), .DPRA5(ADDRB[5]), .DPO(pre_doutb_1[2]));
// Memory array element: COL: 1, ROW: 3
//  Initialization of Dual Port LUT ram now done through explicit
// parameter setting.
RAM64X1D#(.INIT(64'h0000000000000000))RAM64X1D_instance_2851(.WCLK(CLK), .WE(wea_1), 
  .A0(ADDRA[0]), .A1(ADDRA[1]), .A2(ADDRA[2]), .A3(ADDRA[3]), .A4(ADDRA[4]), .A5(ADDRA[5]), 
  .D(DINA[3]), .SPO(pre_douta_1[3]), .DPRA0(ADDRB[0]), .DPRA1(ADDRB[1]), .DPRA2(ADDRB[2]), 
  .DPRA3(ADDRB[3]), .DPRA4(ADDRB[4]), .DPRA5(ADDRB[5]), .DPO(pre_doutb_1[3]));
// Memory array element: COL: 1, ROW: 4
//  Initialization of Dual Port LUT ram now done through explicit
// parameter setting.
RAM64X1D#(.INIT(64'h0000000000000000))RAM64X1D_instance_2852(.WCLK(CLK), .WE(wea_1), 
  .A0(ADDRA[0]), .A1(ADDRA[1]), .A2(ADDRA[2]), .A3(ADDRA[3]), .A4(ADDRA[4]), .A5(ADDRA[5]), 
  .D(DINA[4]), .SPO(pre_douta_1[4]), .DPRA0(ADDRB[0]), .DPRA1(ADDRB[1]), .DPRA2(ADDRB[2]), 
  .DPRA3(ADDRB[3]), .DPRA4(ADDRB[4]), .DPRA5(ADDRB[5]), .DPO(pre_doutb_1[4]));
// Memory array element: COL: 1, ROW: 5
//  Initialization of Dual Port LUT ram now done through explicit
// parameter setting.
RAM64X1D#(.INIT(64'h0000000000000000))RAM64X1D_instance_2853(.WCLK(CLK), .WE(wea_1), 
  .A0(ADDRA[0]), .A1(ADDRA[1]), .A2(ADDRA[2]), .A3(ADDRA[3]), .A4(ADDRA[4]), .A5(ADDRA[5]), 
  .D(DINA[5]), .SPO(pre_douta_1[5]), .DPRA0(ADDRB[0]), .DPRA1(ADDRB[1]), .DPRA2(ADDRB[2]), 
  .DPRA3(ADDRB[3]), .DPRA4(ADDRB[4]), .DPRA5(ADDRB[5]), .DPO(pre_doutb_1[5]));
// Memory array element: COL: 1, ROW: 6
//  Initialization of Dual Port LUT ram now done through explicit
// parameter setting.
RAM64X1D#(.INIT(64'h0000000000000000))RAM64X1D_instance_2854(.WCLK(CLK), .WE(wea_1), 
  .A0(ADDRA[0]), .A1(ADDRA[1]), .A2(ADDRA[2]), .A3(ADDRA[3]), .A4(ADDRA[4]), .A5(ADDRA[5]), 
  .D(DINA[6]), .SPO(pre_douta_1[6]), .DPRA0(ADDRB[0]), .DPRA1(ADDRB[1]), .DPRA2(ADDRB[2]), 
  .DPRA3(ADDRB[3]), .DPRA4(ADDRB[4]), .DPRA5(ADDRB[5]), .DPO(pre_doutb_1[6]));
// Memory array element: COL: 1, ROW: 7
//  Initialization of Dual Port LUT ram now done through explicit
// parameter setting.
RAM64X1D#(.INIT(64'h0000000000000000))RAM64X1D_instance_2855(.WCLK(CLK), .WE(wea_1), 
  .A0(ADDRA[0]), .A1(ADDRA[1]), .A2(ADDRA[2]), .A3(ADDRA[3]), .A4(ADDRA[4]), .A5(ADDRA[5]), 
  .D(DINA[7]), .SPO(pre_douta_1[7]), .DPRA0(ADDRB[0]), .DPRA1(ADDRB[1]), .DPRA2(ADDRB[2]), 
  .DPRA3(ADDRB[3]), .DPRA4(ADDRB[4]), .DPRA5(ADDRB[5]), .DPO(pre_doutb_1[7]));
// Memory array element: COL: 1, ROW: 8
//  Initialization of Dual Port LUT ram now done through explicit
// parameter setting.
RAM64X1D#(.INIT(64'h0000000000000000))RAM64X1D_instance_2856(.WCLK(CLK), .WE(wea_1), 
  .A0(ADDRA[0]), .A1(ADDRA[1]), .A2(ADDRA[2]), .A3(ADDRA[3]), .A4(ADDRA[4]), .A5(ADDRA[5]), 
  .D(DINA[8]), .SPO(pre_douta_1[8]), .DPRA0(ADDRB[0]), .DPRA1(ADDRB[1]), .DPRA2(ADDRB[2]), 
  .DPRA3(ADDRB[3]), .DPRA4(ADDRB[4]), .DPRA5(ADDRB[5]), .DPO(pre_doutb_1[8]));
// Memory array element: COL: 1, ROW: 9
//  Initialization of Dual Port LUT ram now done through explicit
// parameter setting.
RAM64X1D#(.INIT(64'h0000000000000000))RAM64X1D_instance_2857(.WCLK(CLK), .WE(wea_1), 
  .A0(ADDRA[0]), .A1(ADDRA[1]), .A2(ADDRA[2]), .A3(ADDRA[3]), .A4(ADDRA[4]), .A5(ADDRA[5]), 
  .D(DINA[9]), .SPO(pre_douta_1[9]), .DPRA0(ADDRB[0]), .DPRA1(ADDRB[1]), .DPRA2(ADDRB[2]), 
  .DPRA3(ADDRB[3]), .DPRA4(ADDRB[4]), .DPRA5(ADDRB[5]), .DPO(pre_doutb_1[9]));
// Memory array element: COL: 1, ROW: 10
//  Initialization of Dual Port LUT ram now done through explicit
// parameter setting.
RAM64X1D#(.INIT(64'h0000000000000000))RAM64X1D_instance_2858(.WCLK(CLK), .WE(wea_1), 
  .A0(ADDRA[0]), .A1(ADDRA[1]), .A2(ADDRA[2]), .A3(ADDRA[3]), .A4(ADDRA[4]), .A5(ADDRA[5]), 
  .D(DINA[10]), .SPO(pre_douta_1[10]), .DPRA0(ADDRB[0]), .DPRA1(ADDRB[1]), .DPRA2(ADDRB[2]), 
  .DPRA3(ADDRB[3]), .DPRA4(ADDRB[4]), .DPRA5(ADDRB[5]), .DPO(pre_doutb_1[10]));
// Memory array element: COL: 1, ROW: 11
//  Initialization of Dual Port LUT ram now done through explicit
// parameter setting.
RAM64X1D#(.INIT(64'h0000000000000000))RAM64X1D_instance_2859(.WCLK(CLK), .WE(wea_1), 
  .A0(ADDRA[0]), .A1(ADDRA[1]), .A2(ADDRA[2]), .A3(ADDRA[3]), .A4(ADDRA[4]), .A5(ADDRA[5]), 
  .D(DINA[11]), .SPO(pre_douta_1[11]), .DPRA0(ADDRB[0]), .DPRA1(ADDRB[1]), .DPRA2(ADDRB[2]), 
  .DPRA3(ADDRB[3]), .DPRA4(ADDRB[4]), .DPRA5(ADDRB[5]), .DPO(pre_doutb_1[11]));
// Memory array element: COL: 1, ROW: 12
//  Initialization of Dual Port LUT ram now done through explicit
// parameter setting.
RAM64X1D#(.INIT(64'h0000000000000000))RAM64X1D_instance_2860(.WCLK(CLK), .WE(wea_1), 
  .A0(ADDRA[0]), .A1(ADDRA[1]), .A2(ADDRA[2]), .A3(ADDRA[3]), .A4(ADDRA[4]), .A5(ADDRA[5]), 
  .D(DINA[12]), .SPO(pre_douta_1[12]), .DPRA0(ADDRB[0]), .DPRA1(ADDRB[1]), .DPRA2(ADDRB[2]), 
  .DPRA3(ADDRB[3]), .DPRA4(ADDRB[4]), .DPRA5(ADDRB[5]), .DPO(pre_doutb_1[12]));
// Memory array element: COL: 1, ROW: 13
//  Initialization of Dual Port LUT ram now done through explicit
// parameter setting.
RAM64X1D#(.INIT(64'h0000000000000000))RAM64X1D_instance_2861(.WCLK(CLK), .WE(wea_1), 
  .A0(ADDRA[0]), .A1(ADDRA[1]), .A2(ADDRA[2]), .A3(ADDRA[3]), .A4(ADDRA[4]), .A5(ADDRA[5]), 
  .D(DINA[13]), .SPO(pre_douta_1[13]), .DPRA0(ADDRB[0]), .DPRA1(ADDRB[1]), .DPRA2(ADDRB[2]), 
  .DPRA3(ADDRB[3]), .DPRA4(ADDRB[4]), .DPRA5(ADDRB[5]), .DPO(pre_doutb_1[13]));
// Memory array element: COL: 1, ROW: 14
//  Initialization of Dual Port LUT ram now done through explicit
// parameter setting.
RAM64X1D#(.INIT(64'h0000000000000000))RAM64X1D_instance_2862(.WCLK(CLK), .WE(wea_1), 
  .A0(ADDRA[0]), .A1(ADDRA[1]), .A2(ADDRA[2]), .A3(ADDRA[3]), .A4(ADDRA[4]), .A5(ADDRA[5]), 
  .D(DINA[14]), .SPO(pre_douta_1[14]), .DPRA0(ADDRB[0]), .DPRA1(ADDRB[1]), .DPRA2(ADDRB[2]), 
  .DPRA3(ADDRB[3]), .DPRA4(ADDRB[4]), .DPRA5(ADDRB[5]), .DPO(pre_doutb_1[14]));
// Memory array element: COL: 1, ROW: 15
//  Initialization of Dual Port LUT ram now done through explicit
// parameter setting.
RAM64X1D#(.INIT(64'h0000000000000000))RAM64X1D_instance_2863(.WCLK(CLK), .WE(wea_1), 
  .A0(ADDRA[0]), .A1(ADDRA[1]), .A2(ADDRA[2]), .A3(ADDRA[3]), .A4(ADDRA[4]), .A5(ADDRA[5]), 
  .D(DINA[15]), .SPO(pre_douta_1[15]), .DPRA0(ADDRB[0]), .DPRA1(ADDRB[1]), .DPRA2(ADDRB[2]), 
  .DPRA3(ADDRB[3]), .DPRA4(ADDRB[4]), .DPRA5(ADDRB[5]), .DPO(pre_doutb_1[15]));
// Memory array element: COL: 1, ROW: 16
//  Initialization of Dual Port LUT ram now done through explicit
// parameter setting.
RAM64X1D#(.INIT(64'h0000000000000000))RAM64X1D_instance_2864(.WCLK(CLK), .WE(wea_1), 
  .A0(ADDRA[0]), .A1(ADDRA[1]), .A2(ADDRA[2]), .A3(ADDRA[3]), .A4(ADDRA[4]), .A5(ADDRA[5]), 
  .D(DINA[16]), .SPO(pre_douta_1[16]), .DPRA0(ADDRB[0]), .DPRA1(ADDRB[1]), .DPRA2(ADDRB[2]), 
  .DPRA3(ADDRB[3]), .DPRA4(ADDRB[4]), .DPRA5(ADDRB[5]), .DPO(pre_doutb_1[16]));
// Memory array element: COL: 1, ROW: 17
//  Initialization of Dual Port LUT ram now done through explicit
// parameter setting.
RAM64X1D#(.INIT(64'h0000000000000000))RAM64X1D_instance_2865(.WCLK(CLK), .WE(wea_1), 
  .A0(ADDRA[0]), .A1(ADDRA[1]), .A2(ADDRA[2]), .A3(ADDRA[3]), .A4(ADDRA[4]), .A5(ADDRA[5]), 
  .D(DINA[17]), .SPO(pre_douta_1[17]), .DPRA0(ADDRB[0]), .DPRA1(ADDRB[1]), .DPRA2(ADDRB[2]), 
  .DPRA3(ADDRB[3]), .DPRA4(ADDRB[4]), .DPRA5(ADDRB[5]), .DPO(pre_doutb_1[17]));
// Memory array element: COL: 1, ROW: 18
//  Initialization of Dual Port LUT ram now done through explicit
// parameter setting.
RAM64X1D#(.INIT(64'h0000000000000000))RAM64X1D_instance_2866(.WCLK(CLK), .WE(wea_1), 
  .A0(ADDRA[0]), .A1(ADDRA[1]), .A2(ADDRA[2]), .A3(ADDRA[3]), .A4(ADDRA[4]), .A5(ADDRA[5]), 
  .D(DINA[18]), .SPO(pre_douta_1[18]), .DPRA0(ADDRB[0]), .DPRA1(ADDRB[1]), .DPRA2(ADDRB[2]), 
  .DPRA3(ADDRB[3]), .DPRA4(ADDRB[4]), .DPRA5(ADDRB[5]), .DPO(pre_doutb_1[18]));
// Memory array element: COL: 1, ROW: 19
//  Initialization of Dual Port LUT ram now done through explicit
// parameter setting.
RAM64X1D#(.INIT(64'h0000000000000000))RAM64X1D_instance_2867(.WCLK(CLK), .WE(wea_1), 
  .A0(ADDRA[0]), .A1(ADDRA[1]), .A2(ADDRA[2]), .A3(ADDRA[3]), .A4(ADDRA[4]), .A5(ADDRA[5]), 
  .D(DINA[19]), .SPO(pre_douta_1[19]), .DPRA0(ADDRB[0]), .DPRA1(ADDRB[1]), .DPRA2(ADDRB[2]), 
  .DPRA3(ADDRB[3]), .DPRA4(ADDRB[4]), .DPRA5(ADDRB[5]), .DPO(pre_doutb_1[19]));
// Memory array element: COL: 1, ROW: 20
//  Initialization of Dual Port LUT ram now done through explicit
// parameter setting.
RAM64X1D#(.INIT(64'h0000000000000000))RAM64X1D_instance_2868(.WCLK(CLK), .WE(wea_1), 
  .A0(ADDRA[0]), .A1(ADDRA[1]), .A2(ADDRA[2]), .A3(ADDRA[3]), .A4(ADDRA[4]), .A5(ADDRA[5]), 
  .D(DINA[20]), .SPO(pre_douta_1[20]), .DPRA0(ADDRB[0]), .DPRA1(ADDRB[1]), .DPRA2(ADDRB[2]), 
  .DPRA3(ADDRB[3]), .DPRA4(ADDRB[4]), .DPRA5(ADDRB[5]), .DPO(pre_doutb_1[20]));
// Memory array element: COL: 1, ROW: 21
//  Initialization of Dual Port LUT ram now done through explicit
// parameter setting.
RAM64X1D#(.INIT(64'h0000000000000000))RAM64X1D_instance_2869(.WCLK(CLK), .WE(wea_1), 
  .A0(ADDRA[0]), .A1(ADDRA[1]), .A2(ADDRA[2]), .A3(ADDRA[3]), .A4(ADDRA[4]), .A5(ADDRA[5]), 
  .D(DINA[21]), .SPO(pre_douta_1[21]), .DPRA0(ADDRB[0]), .DPRA1(ADDRB[1]), .DPRA2(ADDRB[2]), 
  .DPRA3(ADDRB[3]), .DPRA4(ADDRB[4]), .DPRA5(ADDRB[5]), .DPO(pre_doutb_1[21]));
// Memory array element: COL: 1, ROW: 22
//  Initialization of Dual Port LUT ram now done through explicit
// parameter setting.
RAM64X1D#(.INIT(64'h0000000000000000))RAM64X1D_instance_2870(.WCLK(CLK), .WE(wea_1), 
  .A0(ADDRA[0]), .A1(ADDRA[1]), .A2(ADDRA[2]), .A3(ADDRA[3]), .A4(ADDRA[4]), .A5(ADDRA[5]), 
  .D(DINA[22]), .SPO(pre_douta_1[22]), .DPRA0(ADDRB[0]), .DPRA1(ADDRB[1]), .DPRA2(ADDRB[2]), 
  .DPRA3(ADDRB[3]), .DPRA4(ADDRB[4]), .DPRA5(ADDRB[5]), .DPO(pre_doutb_1[22]));
// Memory array element: COL: 1, ROW: 23
//  Initialization of Dual Port LUT ram now done through explicit
// parameter setting.
RAM64X1D#(.INIT(64'h0000000000000000))RAM64X1D_instance_2871(.WCLK(CLK), .WE(wea_1), 
  .A0(ADDRA[0]), .A1(ADDRA[1]), .A2(ADDRA[2]), .A3(ADDRA[3]), .A4(ADDRA[4]), .A5(ADDRA[5]), 
  .D(DINA[23]), .SPO(pre_douta_1[23]), .DPRA0(ADDRB[0]), .DPRA1(ADDRB[1]), .DPRA2(ADDRB[2]), 
  .DPRA3(ADDRB[3]), .DPRA4(ADDRB[4]), .DPRA5(ADDRB[5]), .DPO(pre_doutb_1[23]));
// Memory array element: COL: 1, ROW: 24
//  Initialization of Dual Port LUT ram now done through explicit
// parameter setting.
RAM64X1D#(.INIT(64'h0000000000000000))RAM64X1D_instance_2872(.WCLK(CLK), .WE(wea_1), 
  .A0(ADDRA[0]), .A1(ADDRA[1]), .A2(ADDRA[2]), .A3(ADDRA[3]), .A4(ADDRA[4]), .A5(ADDRA[5]), 
  .D(DINA[24]), .SPO(pre_douta_1[24]), .DPRA0(ADDRB[0]), .DPRA1(ADDRB[1]), .DPRA2(ADDRB[2]), 
  .DPRA3(ADDRB[3]), .DPRA4(ADDRB[4]), .DPRA5(ADDRB[5]), .DPO(pre_doutb_1[24]));
// Memory array element: COL: 1, ROW: 25
//  Initialization of Dual Port LUT ram now done through explicit
// parameter setting.
RAM64X1D#(.INIT(64'h0000000000000000))RAM64X1D_instance_2873(.WCLK(CLK), .WE(wea_1), 
  .A0(ADDRA[0]), .A1(ADDRA[1]), .A2(ADDRA[2]), .A3(ADDRA[3]), .A4(ADDRA[4]), .A5(ADDRA[5]), 
  .D(DINA[25]), .SPO(pre_douta_1[25]), .DPRA0(ADDRB[0]), .DPRA1(ADDRB[1]), .DPRA2(ADDRB[2]), 
  .DPRA3(ADDRB[3]), .DPRA4(ADDRB[4]), .DPRA5(ADDRB[5]), .DPO(pre_doutb_1[25]));
// Memory array element: COL: 1, ROW: 26
//  Initialization of Dual Port LUT ram now done through explicit
// parameter setting.
RAM64X1D#(.INIT(64'h0000000000000000))RAM64X1D_instance_2874(.WCLK(CLK), .WE(wea_1), 
  .A0(ADDRA[0]), .A1(ADDRA[1]), .A2(ADDRA[2]), .A3(ADDRA[3]), .A4(ADDRA[4]), .A5(ADDRA[5]), 
  .D(DINA[26]), .SPO(pre_douta_1[26]), .DPRA0(ADDRB[0]), .DPRA1(ADDRB[1]), .DPRA2(ADDRB[2]), 
  .DPRA3(ADDRB[3]), .DPRA4(ADDRB[4]), .DPRA5(ADDRB[5]), .DPO(pre_doutb_1[26]));
// Memory array element: COL: 1, ROW: 27
//  Initialization of Dual Port LUT ram now done through explicit
// parameter setting.
RAM64X1D#(.INIT(64'h0000000000000000))RAM64X1D_instance_2875(.WCLK(CLK), .WE(wea_1), 
  .A0(ADDRA[0]), .A1(ADDRA[1]), .A2(ADDRA[2]), .A3(ADDRA[3]), .A4(ADDRA[4]), .A5(ADDRA[5]), 
  .D(DINA[27]), .SPO(pre_douta_1[27]), .DPRA0(ADDRB[0]), .DPRA1(ADDRB[1]), .DPRA2(ADDRB[2]), 
  .DPRA3(ADDRB[3]), .DPRA4(ADDRB[4]), .DPRA5(ADDRB[5]), .DPO(pre_doutb_1[27]));
// Memory array element: COL: 1, ROW: 28
//  Initialization of Dual Port LUT ram now done through explicit
// parameter setting.
RAM64X1D#(.INIT(64'h0000000000000000))RAM64X1D_instance_2876(.WCLK(CLK), .WE(wea_1), 
  .A0(ADDRA[0]), .A1(ADDRA[1]), .A2(ADDRA[2]), .A3(ADDRA[3]), .A4(ADDRA[4]), .A5(ADDRA[5]), 
  .D(DINA[28]), .SPO(pre_douta_1[28]), .DPRA0(ADDRB[0]), .DPRA1(ADDRB[1]), .DPRA2(ADDRB[2]), 
  .DPRA3(ADDRB[3]), .DPRA4(ADDRB[4]), .DPRA5(ADDRB[5]), .DPO(pre_doutb_1[28]));
// Memory array element: COL: 1, ROW: 29
//  Initialization of Dual Port LUT ram now done through explicit
// parameter setting.
RAM64X1D#(.INIT(64'h0000000000000000))RAM64X1D_instance_2877(.WCLK(CLK), .WE(wea_1), 
  .A0(ADDRA[0]), .A1(ADDRA[1]), .A2(ADDRA[2]), .A3(ADDRA[3]), .A4(ADDRA[4]), .A5(ADDRA[5]), 
  .D(DINA[29]), .SPO(pre_douta_1[29]), .DPRA0(ADDRB[0]), .DPRA1(ADDRB[1]), .DPRA2(ADDRB[2]), 
  .DPRA3(ADDRB[3]), .DPRA4(ADDRB[4]), .DPRA5(ADDRB[5]), .DPO(pre_doutb_1[29]));
// Memory array element: COL: 1, ROW: 30
//  Initialization of Dual Port LUT ram now done through explicit
// parameter setting.
RAM64X1D#(.INIT(64'h0000000000000000))RAM64X1D_instance_2878(.WCLK(CLK), .WE(wea_1), 
  .A0(ADDRA[0]), .A1(ADDRA[1]), .A2(ADDRA[2]), .A3(ADDRA[3]), .A4(ADDRA[4]), .A5(ADDRA[5]), 
  .D(DINA[30]), .SPO(pre_douta_1[30]), .DPRA0(ADDRB[0]), .DPRA1(ADDRB[1]), .DPRA2(ADDRB[2]), 
  .DPRA3(ADDRB[3]), .DPRA4(ADDRB[4]), .DPRA5(ADDRB[5]), .DPO(pre_doutb_1[30]));
// Memory array element: COL: 1, ROW: 31
//  Initialization of Dual Port LUT ram now done through explicit
// parameter setting.
RAM64X1D#(.INIT(64'h0000000000000000))RAM64X1D_instance_2879(.WCLK(CLK), .WE(wea_1), 
  .A0(ADDRA[0]), .A1(ADDRA[1]), .A2(ADDRA[2]), .A3(ADDRA[3]), .A4(ADDRA[4]), .A5(ADDRA[5]), 
  .D(DINA[31]), .SPO(pre_douta_1[31]), .DPRA0(ADDRB[0]), .DPRA1(ADDRB[1]), .DPRA2(ADDRB[2]), 
  .DPRA3(ADDRB[3]), .DPRA4(ADDRB[4]), .DPRA5(ADDRB[5]), .DPO(pre_doutb_1[31]));
endmodule



module medianRow1_structuralmemory_5454fca3_(CLK_u90, bus_3077d8d0_, bus_59fc7e31_, bus_5d043583_, bus_341dcce4_, bus_4b69a9ab_, bus_4795d5ee_, bus_07aeae51_, bus_1a07a6df_, bus_6f386e2b_, bus_1e3e6f74_, bus_3da8fea0_, bus_634d3d5d_, bus_4d3e7ed8_);
input		CLK_u90;
input		bus_3077d8d0_;
input	[31:0]	bus_59fc7e31_;
input	[2:0]	bus_5d043583_;
input		bus_341dcce4_;
input	[31:0]	bus_4b69a9ab_;
input	[2:0]	bus_4795d5ee_;
input		bus_07aeae51_;
input		bus_1a07a6df_;
input	[31:0]	bus_6f386e2b_;
output	[31:0]	bus_1e3e6f74_;
output		bus_3da8fea0_;
output	[31:0]	bus_634d3d5d_;
output		bus_4d3e7ed8_;
wire		or_409bceb8_u0;
wire		and_6be0c5e5_u0;
reg		logicalMem_2dd5467d_we_delay0_u0=1'h0;
wire		not_64ba1711_u0;
wire		or_3f2008ed_u0;
wire	[31:0]	bus_1b6dbcf8_;
wire	[31:0]	bus_3fb89a67_;
assign or_409bceb8_u0=bus_07aeae51_|bus_1a07a6df_;
assign and_6be0c5e5_u0=bus_07aeae51_&not_64ba1711_u0;
always @(posedge CLK_u90 or posedge bus_3077d8d0_)
begin
if (bus_3077d8d0_)
logicalMem_2dd5467d_we_delay0_u0<=1'h0;
else logicalMem_2dd5467d_we_delay0_u0<=bus_1a07a6df_;
end
assign not_64ba1711_u0=~bus_1a07a6df_;
assign or_3f2008ed_u0=and_6be0c5e5_u0|logicalMem_2dd5467d_we_delay0_u0;
medianRow1_forge_memory_101x32_141 medianRow1_forge_memory_101x32_141_instance0(.CLK(CLK_u90), 
  .ENA(or_409bceb8_u0), .WEA(bus_1a07a6df_), .DINA(bus_6f386e2b_), .ADDRA(bus_4b69a9ab_), 
  .DOUTA(bus_1b6dbcf8_), .DONEA(), .ENB(bus_341dcce4_), .ADDRB(bus_59fc7e31_), .DOUTB(bus_3fb89a67_), 
  .DONEB());
assign bus_1e3e6f74_=bus_3fb89a67_;
assign bus_3da8fea0_=bus_341dcce4_;
assign bus_634d3d5d_=bus_1b6dbcf8_;
assign bus_4d3e7ed8_=or_3f2008ed_u0;
endmodule



module medianRow1_scheduler(CLK, RESET, GO, port_0ae92c70_, port_40b7289e_, port_1be92021_, port_059c8ae6_, port_30a7d09c_, port_511d1433_, RESULT, RESULT_u2196, RESULT_u2197, RESULT_u2198, DONE);
input		CLK;
input		RESET;
input		GO;
input		port_0ae92c70_;
input	[31:0]	port_40b7289e_;
input		port_1be92021_;
input		port_059c8ae6_;
input		port_30a7d09c_;
input		port_511d1433_;
output		RESULT;
output		RESULT_u2196;
output		RESULT_u2197;
output		RESULT_u2198;
output		DONE;
wire		and_u3717_u0;
wire signed	[31:0]	lessThan_b_signed;
wire signed	[31:0]	lessThan_a_signed;
wire		lessThan;
wire signed	[31:0]	equals_b_signed;
wire		equals;
wire signed	[31:0]	equals_a_signed;
wire signed	[31:0]	equals_u230_b_signed;
wire signed	[31:0]	equals_u230_a_signed;
wire		equals_u230;
wire		and_u3718_u0;
wire		and_u3719_u0;
wire		not_u867_u0;
wire		andOp;
wire		not_u868_u0;
wire		and_u3720_u0;
wire		and_u3721_u0;
wire		simplePinWrite;
wire		and_u3722_u0;
wire		and_u3723_u0;
wire		equals_u231;
wire signed	[31:0]	equals_u231_a_signed;
wire signed	[31:0]	equals_u231_b_signed;
wire		not_u869_u0;
wire		and_u3724_u0;
wire		and_u3725_u0;
wire		andOp_u78;
wire		and_u3726_u0;
wire		and_u3727_u0;
wire		not_u870_u0;
wire		simplePinWrite_u616;
wire		and_u3728_u0;
wire		and_u3729_u0;
wire		not_u871_u0;
wire		and_u3730_u0;
wire		not_u872_u0;
wire		and_u3731_u0;
wire		simplePinWrite_u617;
reg		reg_419677f7_u0=1'h0;
wire		and_u3732_u0;
wire		and_u3733_u0;
wire		or_u1418_u0;
wire		and_u3734_u0;
reg		and_delayed_u385=1'h0;
wire		and_u3735_u0;
wire		or_u1419_u0;
reg		reg_666d4fa9_u0=1'h0;
wire		or_u1420_u0;
wire		or_u1421_u0;
wire		mux_u1886;
wire		and_u3736_u0;
wire		and_u3737_u0;
wire		and_u3738_u0;
reg		and_delayed_u386_u0=1'h0;
wire		or_u1422_u0;
wire		and_u3739_u0;
wire		or_u1423_u0;
wire		mux_u1887_u0;
wire		receive_go_merge;
reg		loopControl_u93=1'h0;
wire		or_u1424_u0;
reg		syncEnable_u1404=1'h0;
wire		or_u1425_u0;
wire		mux_u1888_u0;
reg		reg_125327c1_u0=1'h0;
reg		reg_125327c1_result_delayed_u0=1'h0;
assign and_u3717_u0=or_u1424_u0&or_u1424_u0;
assign lessThan_a_signed=port_40b7289e_;
assign lessThan_b_signed=32'h65;
assign lessThan=lessThan_a_signed<lessThan_b_signed;
assign equals_a_signed=port_40b7289e_;
assign equals_b_signed=32'h65;
assign equals=equals_a_signed==equals_b_signed;
assign equals_u230_a_signed={31'b0, port_0ae92c70_};
assign equals_u230_b_signed=32'h0;
assign equals_u230=equals_u230_a_signed==equals_u230_b_signed;
assign and_u3718_u0=and_u3717_u0&not_u867_u0;
assign and_u3719_u0=and_u3717_u0&equals_u230;
assign not_u867_u0=~equals_u230;
assign andOp=lessThan&port_30a7d09c_;
assign not_u868_u0=~andOp;
assign and_u3720_u0=and_u3723_u0&not_u868_u0;
assign and_u3721_u0=and_u3723_u0&andOp;
assign simplePinWrite=and_u3722_u0&{1{and_u3722_u0}};
assign and_u3722_u0=and_u3721_u0&and_u3723_u0;
assign and_u3723_u0=and_u3719_u0&and_u3717_u0;
assign equals_u231_a_signed={31'b0, port_0ae92c70_};
assign equals_u231_b_signed=32'h1;
assign equals_u231=equals_u231_a_signed==equals_u231_b_signed;
assign not_u869_u0=~equals_u231;
assign and_u3724_u0=and_u3717_u0&not_u869_u0;
assign and_u3725_u0=and_u3717_u0&equals_u231;
assign andOp_u78=lessThan&port_30a7d09c_;
assign and_u3726_u0=and_u3739_u0&not_u870_u0;
assign and_u3727_u0=and_u3739_u0&andOp_u78;
assign not_u870_u0=~andOp_u78;
assign simplePinWrite_u616=and_u3737_u0&{1{and_u3737_u0}};
assign and_u3728_u0=and_u3736_u0&not_u871_u0;
assign and_u3729_u0=and_u3736_u0&equals;
assign not_u871_u0=~equals;
assign and_u3730_u0=and_u3735_u0&port_511d1433_;
assign not_u872_u0=~port_511d1433_;
assign and_u3731_u0=and_u3735_u0&not_u872_u0;
assign simplePinWrite_u617=and_u3732_u0&{1{and_u3732_u0}};
always @(posedge CLK or posedge RESET)
begin
if (RESET)
reg_419677f7_u0<=1'h0;
else reg_419677f7_u0<=and_u3733_u0;
end
assign and_u3732_u0=and_u3730_u0&and_u3735_u0;
assign and_u3733_u0=and_u3731_u0&and_u3735_u0;
assign or_u1418_u0=port_059c8ae6_|reg_419677f7_u0;
assign and_u3734_u0=and_u3728_u0&and_u3736_u0;
always @(posedge CLK or posedge RESET)
begin
if (RESET)
and_delayed_u385<=1'h0;
else and_delayed_u385<=and_u3734_u0;
end
assign and_u3735_u0=and_u3729_u0&and_u3736_u0;
assign or_u1419_u0=and_delayed_u385|or_u1418_u0;
always @(posedge CLK or posedge RESET)
begin
if (RESET)
reg_666d4fa9_u0<=1'h0;
else reg_666d4fa9_u0<=and_u3737_u0;
end
assign or_u1420_u0=reg_666d4fa9_u0|or_u1419_u0;
assign or_u1421_u0=and_u3737_u0|and_u3732_u0;
assign mux_u1886=(and_u3737_u0)?1'h1:1'h0;
assign and_u3736_u0=and_u3726_u0&and_u3739_u0;
assign and_u3737_u0=and_u3727_u0&and_u3739_u0;
assign and_u3738_u0=and_u3724_u0&and_u3717_u0;
always @(posedge CLK or posedge RESET)
begin
if (RESET)
and_delayed_u386_u0<=1'h0;
else and_delayed_u386_u0<=and_u3738_u0;
end
assign or_u1422_u0=and_delayed_u386_u0|or_u1420_u0;
assign and_u3739_u0=and_u3725_u0&and_u3717_u0;
assign or_u1423_u0=and_u3722_u0|or_u1421_u0;
assign mux_u1887_u0=(and_u3722_u0)?1'h1:mux_u1886;
assign receive_go_merge=simplePinWrite|simplePinWrite_u616;
always @(posedge CLK or posedge syncEnable_u1404)
begin
if (syncEnable_u1404)
loopControl_u93<=1'h0;
else loopControl_u93<=or_u1422_u0;
end
assign or_u1424_u0=loopControl_u93|reg_125327c1_result_delayed_u0;
always @(posedge CLK)
begin
if (reg_125327c1_result_delayed_u0)
syncEnable_u1404<=RESET;
end
assign or_u1425_u0=GO|or_u1423_u0;
assign mux_u1888_u0=(GO)?1'h0:mux_u1887_u0;
always @(posedge CLK or posedge RESET)
begin
if (RESET)
reg_125327c1_u0<=1'h0;
else reg_125327c1_u0<=GO;
end
always @(posedge CLK or posedge RESET)
begin
if (RESET)
reg_125327c1_result_delayed_u0<=1'h0;
else reg_125327c1_result_delayed_u0<=reg_125327c1_u0;
end
assign RESULT=or_u1425_u0;
assign RESULT_u2196=mux_u1888_u0;
assign RESULT_u2197=simplePinWrite_u617;
assign RESULT_u2198=receive_go_merge;
assign DONE=1'h0;
endmodule


