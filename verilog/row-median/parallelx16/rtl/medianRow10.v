// __  ___ __ ___  _ __   ___  ___ 
// \ \/ / '__/ _ \| '_ \ / _ \/ __|
//  >  <| | | (_) | | | | (_) \__ \
// /_/\_\_|  \___/|_| |_|\___/|___/
// 
// Xronos synthesizer version
// Run date: Fri 23 Feb 2018 14:28:46 +0000
// 

module medianRow10(median_SEND, RESET, in1_DATA, in1_SEND, in1_COUNT, median_DATA, median_ACK, median_RDY, CLK, median_COUNT, in1_ACK);
output		median_SEND;
input		RESET;
input	[7:0]	in1_DATA;
input		in1_SEND;
wire		receive_done;
input	[15:0]	in1_COUNT;
output	[7:0]	median_DATA;
input		median_ACK;
input		median_RDY;
wire		receive_go;
input		CLK;
wire		compute_median_go;
output	[15:0]	median_COUNT;
wire		compute_median_done;
output		in1_ACK;
wire		bus_2f4436f2_;
wire		receive_u313;
wire		medianRow10_receive_instance_DONE;
wire	[31:0]	receive_u315;
wire		receive_u317;
wire	[31:0]	receive_u314;
wire	[31:0]	receive_u312;
wire		receive;
wire	[2:0]	receive_u316;
wire	[31:0]	bus_63567ac2_;
wire		bus_12e147f6_;
wire		bus_0ff2c3a2_;
wire		bus_32023f72_;
wire	[2:0]	bus_40678708_;
wire	[31:0]	bus_15282a42_;
wire		bus_0794ab73_;
wire	[31:0]	bus_0845192e_;
wire	[31:0]	compute_median_u737;
wire	[7:0]	compute_median_u739;
wire		compute_median_u740;
wire	[2:0]	compute_median_u738;
wire	[15:0]	compute_median_u741;
wire		compute_median;
wire	[31:0]	compute_median_u730;
wire		compute_median_u732;
wire	[2:0]	compute_median_u731;
wire		compute_median_u735;
wire		compute_median_u729;
wire	[2:0]	compute_median_u734;
wire	[31:0]	compute_median_u728;
wire		medianRow10_compute_median_instance_DONE;
wire	[31:0]	compute_median_u733;
wire	[31:0]	compute_median_u736;
wire	[31:0]	bus_464e2cdc_;
wire	[31:0]	bus_552f04d8_;
wire	[2:0]	bus_081e8684_;
wire	[31:0]	bus_3383ce82_;
wire		bus_404afc53_;
wire		bus_08189c58_;
wire		bus_1495d1d3_;
wire		bus_07d95708_;
wire		bus_676f3404_;
wire		bus_2bffdad4_;
wire		bus_57d24d46_;
wire		scheduler_u832;
wire		scheduler;
wire		scheduler_u834;
wire		scheduler_u833;
wire		medianRow10_scheduler_instance_DONE;
wire		bus_2c3c284a_;
wire	[31:0]	bus_71e9df0e_;
wire	[31:0]	bus_47a7c2cc_;
wire		bus_4edf7743_;
wire	[31:0]	bus_2cc05a50_;
assign median_SEND=compute_median_u740;
assign receive_done=bus_2bffdad4_;
assign median_DATA=compute_median_u739;
assign receive_go=scheduler_u834;
assign compute_median_go=scheduler_u833;
assign median_COUNT=compute_median_u741;
assign compute_median_done=bus_2f4436f2_;
assign in1_ACK=receive_u317;
assign bus_2f4436f2_=medianRow10_compute_median_instance_DONE&{1{medianRow10_compute_median_instance_DONE}};
medianRow10_receive medianRow10_receive_instance(.CLK(CLK), .RESET(bus_57d24d46_), 
  .GO(receive_go), .port_7d2b168c_(bus_2cc05a50_), .port_415cd5f7_(bus_0ff2c3a2_), 
  .port_21892323_(in1_DATA), .DONE(medianRow10_receive_instance_DONE), .RESULT(receive), 
  .RESULT_u2360(receive_u312), .RESULT_u2361(receive_u313), .RESULT_u2362(receive_u314), 
  .RESULT_u2363(receive_u315), .RESULT_u2364(receive_u316), .RESULT_u2365(receive_u317));
medianRow10_simplememoryreferee_004291ca_ medianRow10_simplememoryreferee_004291ca__1(.bus_64d5d7d4_(CLK), 
  .bus_6be760e2_(bus_57d24d46_), .bus_584fc826_(bus_2c3c284a_), .bus_057446d1_(bus_71e9df0e_), 
  .bus_33631334_(receive_u313), .bus_1e38caf1_({24'b0, receive_u315[7:0]}), .bus_2997a119_(receive_u314), 
  .bus_3ac70c64_(3'h1), .bus_7aa049e7_(compute_median_u732), .bus_07f63888_(compute_median_u735), 
  .bus_057ea880_(compute_median_u737), .bus_41a6fbf0_(compute_median_u736), .bus_50bd671e_(3'h1), 
  .bus_0845192e_(bus_0845192e_), .bus_15282a42_(bus_15282a42_), .bus_12e147f6_(bus_12e147f6_), 
  .bus_0794ab73_(bus_0794ab73_), .bus_40678708_(bus_40678708_), .bus_0ff2c3a2_(bus_0ff2c3a2_), 
  .bus_63567ac2_(bus_63567ac2_), .bus_32023f72_(bus_32023f72_));
medianRow10_compute_median medianRow10_compute_median_instance(.CLK(CLK), .RESET(bus_57d24d46_), 
  .GO(compute_median_go), .port_5943577f_(bus_08189c58_), .port_7cc448d8_(bus_3383ce82_), 
  .port_29331407_(bus_32023f72_), .port_67c67a4e_(bus_63567ac2_), .port_1149014c_(bus_32023f72_), 
  .DONE(medianRow10_compute_median_instance_DONE), .RESULT(compute_median), .RESULT_u2366(compute_median_u728), 
  .RESULT_u2367(compute_median_u729), .RESULT_u2368(compute_median_u730), .RESULT_u2369(compute_median_u731), 
  .RESULT_u2370(compute_median_u732), .RESULT_u2371(compute_median_u733), .RESULT_u2372(compute_median_u734), 
  .RESULT_u2373(compute_median_u735), .RESULT_u2374(compute_median_u736), .RESULT_u2375(compute_median_u737), 
  .RESULT_u2376(compute_median_u738), .RESULT_u2377(compute_median_u739), .RESULT_u2378(compute_median_u740), 
  .RESULT_u2379(compute_median_u741));
medianRow10_simplememoryreferee_76674ff9_ medianRow10_simplememoryreferee_76674ff9__1(.bus_7f797504_(CLK), 
  .bus_4691892e_(bus_57d24d46_), .bus_1a1cb8ab_(bus_4edf7743_), .bus_6172f423_(bus_47a7c2cc_), 
  .bus_4fd54b5a_(compute_median_u729), .bus_3fca7cf5_(compute_median_u730), .bus_51face74_(3'h1), 
  .bus_464e2cdc_(bus_464e2cdc_), .bus_552f04d8_(bus_552f04d8_), .bus_1495d1d3_(bus_1495d1d3_), 
  .bus_404afc53_(bus_404afc53_), .bus_081e8684_(bus_081e8684_), .bus_3383ce82_(bus_3383ce82_), 
  .bus_08189c58_(bus_08189c58_));
medianRow10_stateVar_fsmState_medianRow10 medianRow10_stateVar_fsmState_medianRow10_1(.bus_7c7159f5_(CLK), 
  .bus_45a9c290_(bus_57d24d46_), .bus_090ef929_(scheduler), .bus_74705736_(scheduler_u832), 
  .bus_07d95708_(bus_07d95708_));
medianRow10_Kicker_64 medianRow10_Kicker_64_1(.CLK(CLK), .RESET(bus_57d24d46_), 
  .bus_676f3404_(bus_676f3404_));
assign bus_2bffdad4_=medianRow10_receive_instance_DONE&{1{medianRow10_receive_instance_DONE}};
medianRow10_globalreset_physical_1cb566de_ medianRow10_globalreset_physical_1cb566de__1(.bus_78d8658c_(CLK), 
  .bus_76ebc647_(RESET), .bus_57d24d46_(bus_57d24d46_));
medianRow10_scheduler medianRow10_scheduler_instance(.CLK(CLK), .RESET(bus_57d24d46_), 
  .GO(bus_676f3404_), .port_1a957bf9_(bus_07d95708_), .port_1a4f064d_(bus_2cc05a50_), 
  .port_0de4af4d_(median_RDY), .port_3206ef84_(in1_SEND), .port_212e157d_(receive_done), 
  .port_6de852fe_(compute_median_done), .DONE(medianRow10_scheduler_instance_DONE), 
  .RESULT(scheduler), .RESULT_u2380(scheduler_u832), .RESULT_u2381(scheduler_u833), 
  .RESULT_u2382(scheduler_u834));
medianRow10_structuralmemory_1c422e2d_ medianRow10_structuralmemory_1c422e2d__1(.CLK_u98(CLK), 
  .bus_4be8d0e7_(bus_57d24d46_), .bus_666d62dd_(bus_552f04d8_), .bus_6752f545_(3'h1), 
  .bus_780770f8_(bus_404afc53_), .bus_5b65ca49_(bus_15282a42_), .bus_0cbb2674_(3'h1), 
  .bus_60c77d08_(bus_0794ab73_), .bus_442d5ea8_(bus_12e147f6_), .bus_79cbfafa_(bus_0845192e_), 
  .bus_47a7c2cc_(bus_47a7c2cc_), .bus_4edf7743_(bus_4edf7743_), .bus_71e9df0e_(bus_71e9df0e_), 
  .bus_2c3c284a_(bus_2c3c284a_));
medianRow10_stateVar_i medianRow10_stateVar_i_1(.bus_732945e8_(CLK), .bus_520aa855_(bus_57d24d46_), 
  .bus_43832041_(receive), .bus_7d736cde_(receive_u312), .bus_5e8ce25d_(compute_median), 
  .bus_2306d177_(32'h0), .bus_2cc05a50_(bus_2cc05a50_));
endmodule



module medianRow10_receive(CLK, RESET, GO, port_7d2b168c_, port_415cd5f7_, port_21892323_, RESULT, RESULT_u2360, RESULT_u2361, RESULT_u2362, RESULT_u2363, RESULT_u2364, RESULT_u2365, DONE);
input		CLK;
input		RESET;
input		GO;
input	[31:0]	port_7d2b168c_;
input		port_415cd5f7_;
input	[7:0]	port_21892323_;
output		RESULT;
output	[31:0]	RESULT_u2360;
output		RESULT_u2361;
output	[31:0]	RESULT_u2362;
output	[31:0]	RESULT_u2363;
output	[2:0]	RESULT_u2364;
output		RESULT_u2365;
output		DONE;
wire		simplePinWrite;
wire	[31:0]	add;
wire		and_u4034_u0;
reg		reg_11d58286_u0=1'h0;
wire		or_u1552_u0;
wire	[31:0]	add_u738;
reg		reg_587bc01a_u0=1'h0;
assign simplePinWrite=GO&{1{GO}};
assign add=port_7d2b168c_+32'h0;
assign and_u4034_u0=reg_11d58286_u0&port_415cd5f7_;
always @(posedge CLK or posedge GO or posedge or_u1552_u0)
begin
if (or_u1552_u0)
reg_11d58286_u0<=1'h0;
else if (GO)
reg_11d58286_u0<=1'h1;
else reg_11d58286_u0<=reg_11d58286_u0;
end
assign or_u1552_u0=and_u4034_u0|RESET;
assign add_u738=port_7d2b168c_+32'h1;
always @(posedge CLK or posedge RESET)
begin
if (RESET)
reg_587bc01a_u0<=1'h0;
else reg_587bc01a_u0<=GO;
end
assign RESULT=GO;
assign RESULT_u2360=add_u738;
assign RESULT_u2361=GO;
assign RESULT_u2362=add;
assign RESULT_u2363={24'b0, port_21892323_};
assign RESULT_u2364=3'h1;
assign RESULT_u2365=simplePinWrite;
assign DONE=reg_587bc01a_u0;
endmodule



module medianRow10_simplememoryreferee_004291ca_(bus_64d5d7d4_, bus_6be760e2_, bus_584fc826_, bus_057446d1_, bus_33631334_, bus_1e38caf1_, bus_2997a119_, bus_3ac70c64_, bus_7aa049e7_, bus_07f63888_, bus_057ea880_, bus_41a6fbf0_, bus_50bd671e_, bus_0845192e_, bus_15282a42_, bus_12e147f6_, bus_0794ab73_, bus_40678708_, bus_0ff2c3a2_, bus_63567ac2_, bus_32023f72_);
input		bus_64d5d7d4_;
input		bus_6be760e2_;
input		bus_584fc826_;
input	[31:0]	bus_057446d1_;
input		bus_33631334_;
input	[31:0]	bus_1e38caf1_;
input	[31:0]	bus_2997a119_;
input	[2:0]	bus_3ac70c64_;
input		bus_7aa049e7_;
input		bus_07f63888_;
input	[31:0]	bus_057ea880_;
input	[31:0]	bus_41a6fbf0_;
input	[2:0]	bus_50bd671e_;
output	[31:0]	bus_0845192e_;
output	[31:0]	bus_15282a42_;
output		bus_12e147f6_;
output		bus_0794ab73_;
output	[2:0]	bus_40678708_;
output		bus_0ff2c3a2_;
output	[31:0]	bus_63567ac2_;
output		bus_32023f72_;
wire		and_6bb8517a_u0;
wire		or_2b7685b5_u0;
reg		done_qual_u232=1'h0;
wire		or_6fd6cd5c_u0;
wire	[31:0]	mux_5becec21_u0;
wire		or_581a33ca_u0;
wire		and_6381eff1_u0;
reg		done_qual_u233_u0=1'h0;
wire		not_39c675b4_u0;
wire	[31:0]	mux_5c872cf4_u0;
wire		not_1960e07b_u0;
wire		or_3d508b4d_u0;
wire		or_36a47d3b_u0;
assign and_6bb8517a_u0=or_2b7685b5_u0&bus_584fc826_;
assign or_2b7685b5_u0=bus_33631334_|done_qual_u233_u0;
always @(posedge bus_64d5d7d4_)
begin
if (bus_6be760e2_)
done_qual_u232<=1'h0;
else done_qual_u232<=or_36a47d3b_u0;
end
assign or_6fd6cd5c_u0=bus_33631334_|or_36a47d3b_u0;
assign mux_5becec21_u0=(bus_33631334_)?{24'b0, bus_1e38caf1_[7:0]}:bus_057ea880_;
assign or_581a33ca_u0=or_36a47d3b_u0|done_qual_u232;
assign and_6381eff1_u0=or_581a33ca_u0&bus_584fc826_;
always @(posedge bus_64d5d7d4_)
begin
if (bus_6be760e2_)
done_qual_u233_u0<=1'h0;
else done_qual_u233_u0<=bus_33631334_;
end
assign not_39c675b4_u0=~bus_584fc826_;
assign mux_5c872cf4_u0=(bus_33631334_)?bus_2997a119_:bus_41a6fbf0_;
assign bus_0845192e_=mux_5becec21_u0;
assign bus_15282a42_=mux_5c872cf4_u0;
assign bus_12e147f6_=or_3d508b4d_u0;
assign bus_0794ab73_=or_6fd6cd5c_u0;
assign bus_40678708_=3'h1;
assign bus_0ff2c3a2_=and_6bb8517a_u0;
assign bus_63567ac2_=bus_057446d1_;
assign bus_32023f72_=and_6381eff1_u0;
assign not_1960e07b_u0=~bus_584fc826_;
assign or_3d508b4d_u0=bus_33631334_|bus_07f63888_;
assign or_36a47d3b_u0=bus_7aa049e7_|bus_07f63888_;
endmodule



module medianRow10_compute_median(CLK, RESET, GO, port_5943577f_, port_7cc448d8_, port_29331407_, port_67c67a4e_, port_1149014c_, RESULT, RESULT_u2366, RESULT_u2367, RESULT_u2368, RESULT_u2369, RESULT_u2370, RESULT_u2371, RESULT_u2372, RESULT_u2373, RESULT_u2374, RESULT_u2375, RESULT_u2376, RESULT_u2377, RESULT_u2378, RESULT_u2379, DONE);
input		CLK;
input		RESET;
input		GO;
input		port_5943577f_;
input	[31:0]	port_7cc448d8_;
input		port_29331407_;
input	[31:0]	port_67c67a4e_;
input		port_1149014c_;
output		RESULT;
output	[31:0]	RESULT_u2366;
output		RESULT_u2367;
output	[31:0]	RESULT_u2368;
output	[2:0]	RESULT_u2369;
output		RESULT_u2370;
output	[31:0]	RESULT_u2371;
output	[2:0]	RESULT_u2372;
output		RESULT_u2373;
output	[31:0]	RESULT_u2374;
output	[31:0]	RESULT_u2375;
output	[2:0]	RESULT_u2376;
output	[7:0]	RESULT_u2377;
output		RESULT_u2378;
output	[15:0]	RESULT_u2379;
output		DONE;
wire	[15:0]	lessThan_b_unsigned;
wire	[15:0]	lessThan_a_unsigned;
wire		lessThan;
wire		andOp;
wire		not_u936_u0;
wire		and_u4035_u0;
wire		and_u4036_u0;
wire		and_u4037_u0;
wire signed	[32:0]	lessThan_u110_b_signed;
wire signed	[32:0]	lessThan_u110_a_signed;
wire		lessThan_u110;
wire		and_u4038_u0;
wire		and_u4039_u0;
wire		not_u937_u0;
wire	[31:0]	add;
wire		and_u4040_u0;
wire	[31:0]	add_u739;
wire	[31:0]	add_u740;
wire		and_u4041_u0;
wire signed	[31:0]	greaterThan_b_signed;
wire		greaterThan;
wire signed	[31:0]	greaterThan_a_signed;
wire		not_u938_u0;
wire		and_u4042_u0;
wire		and_u4043_u0;
wire	[31:0]	add_u741;
wire		and_u4044_u0;
wire	[31:0]	add_u742;
wire	[31:0]	add_u743;
wire		and_u4045_u0;
wire	[31:0]	add_u744;
wire		or_u1553_u0;
wire		and_u4046_u0;
reg		reg_36cc7a41_u0=1'h0;
wire	[31:0]	add_u745;
wire	[31:0]	add_u746;
wire		or_u1554_u0;
reg		reg_6d0b33d3_u0=1'h0;
wire		and_u4047_u0;
reg	[31:0]	syncEnable_u1547=32'h0;
reg	[31:0]	syncEnable_u1548_u0=32'h0;
reg	[31:0]	syncEnable_u1549_u0=32'h0;
wire		or_u1555_u0;
wire	[31:0]	mux_u2050;
wire	[31:0]	mux_u2051_u0;
reg	[31:0]	syncEnable_u1550_u0=32'h0;
reg	[31:0]	syncEnable_u1551_u0=32'h0;
reg	[31:0]	syncEnable_u1552_u0=32'h0;
reg		reg_081086bf_u0=1'h0;
reg		block_GO_delayed_u110=1'h0;
reg		reg_17235ac3_u0=1'h0;
reg		reg_46cf27ae_u0=1'h0;
reg		reg_17235ac3_result_delayed_u0=1'h0;
wire		mux_u2052_u0;
wire		and_u4048_u0;
wire		or_u1556_u0;
wire	[31:0]	mux_u2053_u0;
reg		and_delayed_u409=1'h0;
reg	[31:0]	syncEnable_u1553_u0=32'h0;
reg		reg_17235ac3_result_delayed_result_delayed_u0=1'h0;
wire	[31:0]	mux_u2054_u0;
wire		and_u4049_u0;
reg		syncEnable_u1554_u0=1'h0;
reg	[31:0]	syncEnable_u1555_u0=32'h0;
wire	[31:0]	add_u747;
reg		syncEnable_u1556_u0=1'h0;
wire	[31:0]	mux_u2055_u0;
wire		or_u1557_u0;
reg	[31:0]	syncEnable_u1557_u0=32'h0;
reg	[31:0]	syncEnable_u1558_u0=32'h0;
reg	[31:0]	syncEnable_u1559_u0=32'h0;
reg		block_GO_delayed_u111_u0=1'h0;
reg	[31:0]	syncEnable_u1560_u0=32'h0;
reg	[31:0]	syncEnable_u1561_u0=32'h0;
wire		or_u1558_u0;
wire	[31:0]	mux_u2056_u0;
reg	[31:0]	syncEnable_u1562_u0=32'h0;
reg	[31:0]	syncEnable_u1563_u0=32'h0;
reg	[31:0]	syncEnable_u1564_u0=32'h0;
wire		and_u4050_u0;
reg	[31:0]	syncEnable_u1565_u0=32'h0;
wire	[31:0]	mux_u2057_u0;
wire	[31:0]	mux_u2058_u0;
wire		or_u1559_u0;
wire	[31:0]	mux_u2059_u0;
wire	[31:0]	mux_u2060_u0;
wire	[31:0]	mux_u2061_u0;
wire		mux_u2062_u0;
wire	[15:0]	add_u748;
reg		reg_211b7899_u0=1'h0;
wire		scoreboard_0267a043_resOr1;
wire		bus_180c08e1_;
reg		scoreboard_0267a043_reg0=1'h0;
reg		scoreboard_0267a043_reg1=1'h0;
wire		scoreboard_0267a043_resOr0;
wire		scoreboard_0267a043_and;
reg	[15:0]	syncEnable_u1566_u0=16'h0;
reg	[31:0]	latch_43daf934_reg=32'h0;
wire	[31:0]	latch_43daf934_out;
wire	[31:0]	latch_2dc4bb56_out;
reg	[31:0]	latch_2dc4bb56_reg=32'h0;
wire	[31:0]	latch_06f3cb41_out;
reg	[31:0]	latch_06f3cb41_reg=32'h0;
reg		latch_29c6fc7f_reg=1'h0;
wire		latch_29c6fc7f_out;
wire	[31:0]	latch_4d5b7ee1_out;
reg	[31:0]	latch_4d5b7ee1_reg=32'h0;
wire		and_u4051_u0;
reg		fbReg_swapped_u52=1'h0;
wire		mux_u2063_u0;
wire	[31:0]	mux_u2064_u0;
reg	[31:0]	fbReg_tmp_row_u52=32'h0;
reg	[31:0]	fbReg_tmp_row1_u52=32'h0;
wire	[31:0]	mux_u2065_u0;
reg	[15:0]	fbReg_idx_u52=16'h0;
wire	[31:0]	mux_u2066_u0;
wire	[15:0]	mux_u2067_u0;
reg	[31:0]	fbReg_tmp_u52=32'h0;
reg		loopControl_u108=1'h0;
reg		syncEnable_u1567_u0=1'h0;
wire		or_u1560_u0;
reg	[31:0]	fbReg_tmp_row0_u52=32'h0;
wire	[31:0]	mux_u2068_u0;
wire		and_u4052_u0;
wire	[15:0]	simplePinWrite;
wire		simplePinWrite_u646;
wire	[7:0]	simplePinWrite_u647;
wire	[31:0]	mux_u2069_u0;
wire		or_u1561_u0;
reg		reg_15e1cfae_u0=1'h0;
reg		reg_15e1cfae_result_delayed_u0=1'h0;
assign lessThan_a_unsigned=mux_u2067_u0;
assign lessThan_b_unsigned=16'h65;
assign lessThan=lessThan_a_unsigned<lessThan_b_unsigned;
assign andOp=lessThan&mux_u2063_u0;
assign not_u936_u0=~andOp;
assign and_u4035_u0=or_u1560_u0&andOp;
assign and_u4036_u0=or_u1560_u0&not_u936_u0;
assign and_u4037_u0=and_u4036_u0&or_u1560_u0;
assign lessThan_u110_a_signed={1'b0, mux_u2061_u0};
assign lessThan_u110_b_signed=33'h64;
assign lessThan_u110=lessThan_u110_a_signed<lessThan_u110_b_signed;
assign and_u4038_u0=or_u1559_u0&lessThan_u110;
assign and_u4039_u0=or_u1559_u0&not_u937_u0;
assign not_u937_u0=~lessThan_u110;
assign add=mux_u2061_u0+32'h0;
assign and_u4040_u0=and_u4050_u0&port_5943577f_;
assign add_u739=mux_u2061_u0+32'h1;
assign add_u740=add_u739+32'h0;
assign and_u4041_u0=and_u4050_u0&port_1149014c_;
assign greaterThan_a_signed=syncEnable_u1562_u0;
assign greaterThan_b_signed=syncEnable_u1560_u0;
assign greaterThan=greaterThan_a_signed>greaterThan_b_signed;
assign not_u938_u0=~greaterThan;
assign and_u4042_u0=block_GO_delayed_u111_u0&greaterThan;
assign and_u4043_u0=block_GO_delayed_u111_u0&not_u938_u0;
assign add_u741=syncEnable_u1559_u0+32'h0;
assign and_u4044_u0=and_u4049_u0&port_5943577f_;
assign add_u742=syncEnable_u1559_u0+32'h1;
assign add_u743=add_u742+32'h0;
assign and_u4045_u0=and_u4049_u0&port_1149014c_;
assign add_u744=syncEnable_u1559_u0+32'h0;
assign or_u1553_u0=and_u4046_u0|RESET;
assign and_u4046_u0=reg_36cc7a41_u0&port_1149014c_;
always @(posedge CLK or posedge block_GO_delayed_u110 or posedge or_u1553_u0)
begin
if (or_u1553_u0)
reg_36cc7a41_u0<=1'h0;
else if (block_GO_delayed_u110)
reg_36cc7a41_u0<=1'h1;
else reg_36cc7a41_u0<=reg_36cc7a41_u0;
end
assign add_u745=syncEnable_u1559_u0+32'h1;
assign add_u746=add_u745+32'h0;
assign or_u1554_u0=and_u4047_u0|RESET;
always @(posedge CLK or posedge reg_081086bf_u0 or posedge or_u1554_u0)
begin
if (or_u1554_u0)
reg_6d0b33d3_u0<=1'h0;
else if (reg_081086bf_u0)
reg_6d0b33d3_u0<=1'h1;
else reg_6d0b33d3_u0<=reg_6d0b33d3_u0;
end
assign and_u4047_u0=reg_6d0b33d3_u0&port_1149014c_;
always @(posedge CLK)
begin
if (and_u4049_u0)
syncEnable_u1547<=port_7cc448d8_;
end
always @(posedge CLK)
begin
if (and_u4049_u0)
syncEnable_u1548_u0<=add_u746;
end
always @(posedge CLK)
begin
if (and_u4049_u0)
syncEnable_u1549_u0<=port_67c67a4e_;
end
assign or_u1555_u0=block_GO_delayed_u110|reg_081086bf_u0;
assign mux_u2050=(block_GO_delayed_u110)?syncEnable_u1552_u0:syncEnable_u1547;
assign mux_u2051_u0=({32{block_GO_delayed_u110}}&syncEnable_u1550_u0)|({32{reg_081086bf_u0}}&syncEnable_u1548_u0)|({32{and_u4049_u0}}&add_u743);
always @(posedge CLK)
begin
if (and_u4049_u0)
syncEnable_u1550_u0<=add_u744;
end
always @(posedge CLK)
begin
if (and_u4049_u0)
syncEnable_u1551_u0<=port_7cc448d8_;
end
always @(posedge CLK)
begin
if (and_u4049_u0)
syncEnable_u1552_u0<=port_67c67a4e_;
end
always @(posedge CLK or posedge RESET)
begin
if (RESET)
reg_081086bf_u0<=1'h0;
else reg_081086bf_u0<=block_GO_delayed_u110;
end
always @(posedge CLK or posedge RESET)
begin
if (RESET)
block_GO_delayed_u110<=1'h0;
else block_GO_delayed_u110<=and_u4049_u0;
end
always @(posedge CLK or posedge RESET)
begin
if (RESET)
reg_17235ac3_u0<=1'h0;
else reg_17235ac3_u0<=reg_46cf27ae_u0;
end
always @(posedge CLK or posedge RESET)
begin
if (RESET)
reg_46cf27ae_u0<=1'h0;
else reg_46cf27ae_u0<=and_u4049_u0;
end
always @(posedge CLK or posedge RESET)
begin
if (RESET)
reg_17235ac3_result_delayed_u0<=1'h0;
else reg_17235ac3_result_delayed_u0<=reg_17235ac3_u0;
end
assign mux_u2052_u0=(and_delayed_u409)?syncEnable_u1554_u0:1'h1;
assign and_u4048_u0=and_u4043_u0&block_GO_delayed_u111_u0;
assign or_u1556_u0=and_delayed_u409|reg_17235ac3_result_delayed_result_delayed_u0;
assign mux_u2053_u0=(and_delayed_u409)?syncEnable_u1555_u0:syncEnable_u1549_u0;
always @(posedge CLK or posedge RESET)
begin
if (RESET)
and_delayed_u409<=1'h0;
else and_delayed_u409<=and_u4048_u0;
end
always @(posedge CLK)
begin
if (block_GO_delayed_u111_u0)
syncEnable_u1553_u0<=syncEnable_u1557_u0;
end
always @(posedge CLK or posedge RESET)
begin
if (RESET)
reg_17235ac3_result_delayed_result_delayed_u0<=1'h0;
else reg_17235ac3_result_delayed_result_delayed_u0<=reg_17235ac3_result_delayed_u0;
end
assign mux_u2054_u0=(and_delayed_u409)?syncEnable_u1553_u0:syncEnable_u1551_u0;
assign and_u4049_u0=and_u4042_u0&block_GO_delayed_u111_u0;
always @(posedge CLK)
begin
if (block_GO_delayed_u111_u0)
syncEnable_u1554_u0<=syncEnable_u1556_u0;
end
always @(posedge CLK)
begin
if (block_GO_delayed_u111_u0)
syncEnable_u1555_u0<=syncEnable_u1561_u0;
end
assign add_u747=mux_u2061_u0+32'h1;
always @(posedge CLK)
begin
if (and_u4050_u0)
syncEnable_u1556_u0<=mux_u2062_u0;
end
assign mux_u2055_u0=(and_u4050_u0)?add:add_u741;
assign or_u1557_u0=and_u4050_u0|and_u4049_u0;
always @(posedge CLK)
begin
if (and_u4050_u0)
syncEnable_u1557_u0<=mux_u2059_u0;
end
always @(posedge CLK)
begin
if (and_u4050_u0)
syncEnable_u1558_u0<=add_u747;
end
always @(posedge CLK)
begin
if (and_u4050_u0)
syncEnable_u1559_u0<=mux_u2061_u0;
end
always @(posedge CLK or posedge RESET)
begin
if (RESET)
block_GO_delayed_u111_u0<=1'h0;
else block_GO_delayed_u111_u0<=and_u4050_u0;
end
always @(posedge CLK)
begin
if (and_u4050_u0)
syncEnable_u1560_u0<=port_67c67a4e_;
end
always @(posedge CLK)
begin
if (and_u4050_u0)
syncEnable_u1561_u0<=mux_u2057_u0;
end
assign or_u1558_u0=and_u4050_u0|and_u4049_u0;
assign mux_u2056_u0=({32{or_u1555_u0}}&mux_u2051_u0)|({32{and_u4050_u0}}&add_u740)|({32{and_u4049_u0}}&mux_u2051_u0);
always @(posedge CLK)
begin
if (and_u4050_u0)
syncEnable_u1562_u0<=port_7cc448d8_;
end
always @(posedge CLK)
begin
if (or_u1559_u0)
syncEnable_u1563_u0<=mux_u2057_u0;
end
always @(posedge CLK)
begin
if (or_u1559_u0)
syncEnable_u1564_u0<=mux_u2058_u0;
end
assign and_u4050_u0=and_u4038_u0&or_u1559_u0;
always @(posedge CLK)
begin
if (or_u1559_u0)
syncEnable_u1565_u0<=mux_u2060_u0;
end
assign mux_u2057_u0=(or_u1556_u0)?mux_u2053_u0:mux_u2064_u0;
assign mux_u2058_u0=(or_u1556_u0)?syncEnable_u1562_u0:mux_u2065_u0;
assign or_u1559_u0=or_u1556_u0|and_u4051_u0;
assign mux_u2059_u0=(or_u1556_u0)?mux_u2054_u0:mux_u2068_u0;
assign mux_u2060_u0=(or_u1556_u0)?syncEnable_u1560_u0:mux_u2066_u0;
assign mux_u2061_u0=(or_u1556_u0)?syncEnable_u1558_u0:32'h0;
assign mux_u2062_u0=(or_u1556_u0)?mux_u2052_u0:1'h0;
assign add_u748=mux_u2067_u0+16'h1;
always @(posedge CLK or posedge RESET)
begin
if (RESET)
reg_211b7899_u0<=1'h0;
else reg_211b7899_u0<=or_u1556_u0;
end
assign scoreboard_0267a043_resOr1=reg_211b7899_u0|scoreboard_0267a043_reg1;
assign bus_180c08e1_=scoreboard_0267a043_and|RESET;
always @(posedge CLK)
begin
if (bus_180c08e1_)
scoreboard_0267a043_reg0<=1'h0;
else if (or_u1556_u0)
scoreboard_0267a043_reg0<=1'h1;
else scoreboard_0267a043_reg0<=scoreboard_0267a043_reg0;
end
always @(posedge CLK)
begin
if (bus_180c08e1_)
scoreboard_0267a043_reg1<=1'h0;
else if (reg_211b7899_u0)
scoreboard_0267a043_reg1<=1'h1;
else scoreboard_0267a043_reg1<=scoreboard_0267a043_reg1;
end
assign scoreboard_0267a043_resOr0=or_u1556_u0|scoreboard_0267a043_reg0;
assign scoreboard_0267a043_and=scoreboard_0267a043_resOr0&scoreboard_0267a043_resOr1;
always @(posedge CLK)
begin
if (and_u4051_u0)
syncEnable_u1566_u0<=add_u748;
end
always @(posedge CLK)
begin
if (or_u1556_u0)
latch_43daf934_reg<=syncEnable_u1563_u0;
end
assign latch_43daf934_out=(or_u1556_u0)?syncEnable_u1563_u0:latch_43daf934_reg;
assign latch_2dc4bb56_out=(or_u1556_u0)?syncEnable_u1564_u0:latch_2dc4bb56_reg;
always @(posedge CLK)
begin
if (or_u1556_u0)
latch_2dc4bb56_reg<=syncEnable_u1564_u0;
end
assign latch_06f3cb41_out=(or_u1556_u0)?mux_u2054_u0:latch_06f3cb41_reg;
always @(posedge CLK)
begin
if (or_u1556_u0)
latch_06f3cb41_reg<=mux_u2054_u0;
end
always @(posedge CLK)
begin
if (or_u1556_u0)
latch_29c6fc7f_reg<=mux_u2052_u0;
end
assign latch_29c6fc7f_out=(or_u1556_u0)?mux_u2052_u0:latch_29c6fc7f_reg;
assign latch_4d5b7ee1_out=(or_u1556_u0)?syncEnable_u1565_u0:latch_4d5b7ee1_reg;
always @(posedge CLK)
begin
if (or_u1556_u0)
latch_4d5b7ee1_reg<=syncEnable_u1565_u0;
end
assign and_u4051_u0=and_u4035_u0&or_u1560_u0;
always @(posedge CLK)
begin
if (scoreboard_0267a043_and)
fbReg_swapped_u52<=latch_29c6fc7f_out;
end
assign mux_u2063_u0=(loopControl_u108)?fbReg_swapped_u52:1'h0;
assign mux_u2064_u0=(loopControl_u108)?fbReg_tmp_row1_u52:32'h0;
always @(posedge CLK)
begin
if (scoreboard_0267a043_and)
fbReg_tmp_row_u52<=latch_2dc4bb56_out;
end
always @(posedge CLK)
begin
if (scoreboard_0267a043_and)
fbReg_tmp_row1_u52<=latch_43daf934_out;
end
assign mux_u2065_u0=(loopControl_u108)?fbReg_tmp_row_u52:32'h0;
always @(posedge CLK)
begin
if (scoreboard_0267a043_and)
fbReg_idx_u52<=syncEnable_u1566_u0;
end
assign mux_u2066_u0=(loopControl_u108)?fbReg_tmp_row0_u52:32'h0;
assign mux_u2067_u0=(loopControl_u108)?fbReg_idx_u52:16'h0;
always @(posedge CLK)
begin
if (scoreboard_0267a043_and)
fbReg_tmp_u52<=latch_06f3cb41_out;
end
always @(posedge CLK or posedge syncEnable_u1567_u0)
begin
if (syncEnable_u1567_u0)
loopControl_u108<=1'h0;
else loopControl_u108<=scoreboard_0267a043_and;
end
always @(posedge CLK)
begin
if (GO)
syncEnable_u1567_u0<=RESET;
end
assign or_u1560_u0=loopControl_u108|GO;
always @(posedge CLK)
begin
if (scoreboard_0267a043_and)
fbReg_tmp_row0_u52<=latch_4d5b7ee1_out;
end
assign mux_u2068_u0=(loopControl_u108)?fbReg_tmp_u52:32'h0;
assign and_u4052_u0=reg_15e1cfae_u0&port_5943577f_;
assign simplePinWrite=16'h1&{16{1'h1}};
assign simplePinWrite_u646=reg_15e1cfae_u0&{1{reg_15e1cfae_u0}};
assign simplePinWrite_u647=port_7cc448d8_[7:0];
assign mux_u2069_u0=(or_u1557_u0)?mux_u2055_u0:32'h32;
assign or_u1561_u0=or_u1557_u0|reg_15e1cfae_u0;
always @(posedge CLK or posedge RESET)
begin
if (RESET)
reg_15e1cfae_u0<=1'h0;
else reg_15e1cfae_u0<=and_u4037_u0;
end
always @(posedge CLK or posedge RESET)
begin
if (RESET)
reg_15e1cfae_result_delayed_u0<=1'h0;
else reg_15e1cfae_result_delayed_u0<=reg_15e1cfae_u0;
end
assign RESULT=GO;
assign RESULT_u2366=32'h0;
assign RESULT_u2367=or_u1561_u0;
assign RESULT_u2368=mux_u2069_u0;
assign RESULT_u2369=3'h1;
assign RESULT_u2370=or_u1558_u0;
assign RESULT_u2371=mux_u2056_u0;
assign RESULT_u2372=3'h1;
assign RESULT_u2373=or_u1555_u0;
assign RESULT_u2374=mux_u2056_u0;
assign RESULT_u2375=mux_u2050;
assign RESULT_u2376=3'h1;
assign RESULT_u2377=simplePinWrite_u647;
assign RESULT_u2378=simplePinWrite_u646;
assign RESULT_u2379=simplePinWrite;
assign DONE=reg_15e1cfae_result_delayed_u0;
endmodule



module medianRow10_simplememoryreferee_76674ff9_(bus_7f797504_, bus_4691892e_, bus_1a1cb8ab_, bus_6172f423_, bus_4fd54b5a_, bus_3fca7cf5_, bus_51face74_, bus_464e2cdc_, bus_552f04d8_, bus_1495d1d3_, bus_404afc53_, bus_081e8684_, bus_3383ce82_, bus_08189c58_);
input		bus_7f797504_;
input		bus_4691892e_;
input		bus_1a1cb8ab_;
input	[31:0]	bus_6172f423_;
input		bus_4fd54b5a_;
input	[31:0]	bus_3fca7cf5_;
input	[2:0]	bus_51face74_;
output	[31:0]	bus_464e2cdc_;
output	[31:0]	bus_552f04d8_;
output		bus_1495d1d3_;
output		bus_404afc53_;
output	[2:0]	bus_081e8684_;
output	[31:0]	bus_3383ce82_;
output		bus_08189c58_;
assign bus_464e2cdc_=32'h0;
assign bus_552f04d8_=bus_3fca7cf5_;
assign bus_1495d1d3_=1'h0;
assign bus_404afc53_=bus_4fd54b5a_;
assign bus_081e8684_=3'h1;
assign bus_3383ce82_=bus_6172f423_;
assign bus_08189c58_=bus_1a1cb8ab_;
endmodule



module medianRow10_endianswapper_31caa85c_(endianswapper_31caa85c_in, endianswapper_31caa85c_out);
input		endianswapper_31caa85c_in;
output		endianswapper_31caa85c_out;
assign endianswapper_31caa85c_out=endianswapper_31caa85c_in;
endmodule



module medianRow10_endianswapper_30bae045_(endianswapper_30bae045_in, endianswapper_30bae045_out);
input		endianswapper_30bae045_in;
output		endianswapper_30bae045_out;
assign endianswapper_30bae045_out=endianswapper_30bae045_in;
endmodule



module medianRow10_stateVar_fsmState_medianRow10(bus_7c7159f5_, bus_45a9c290_, bus_090ef929_, bus_74705736_, bus_07d95708_);
input		bus_7c7159f5_;
input		bus_45a9c290_;
input		bus_090ef929_;
input		bus_74705736_;
output		bus_07d95708_;
wire		endianswapper_31caa85c_out;
reg		stateVar_fsmState_medianRow10_u2=1'h0;
wire		endianswapper_30bae045_out;
assign bus_07d95708_=endianswapper_30bae045_out;
medianRow10_endianswapper_31caa85c_ medianRow10_endianswapper_31caa85c__1(.endianswapper_31caa85c_in(bus_74705736_), 
  .endianswapper_31caa85c_out(endianswapper_31caa85c_out));
always @(posedge bus_7c7159f5_ or posedge bus_45a9c290_)
begin
if (bus_45a9c290_)
stateVar_fsmState_medianRow10_u2<=1'h0;
else if (bus_090ef929_)
stateVar_fsmState_medianRow10_u2<=endianswapper_31caa85c_out;
end
medianRow10_endianswapper_30bae045_ medianRow10_endianswapper_30bae045__1(.endianswapper_30bae045_in(stateVar_fsmState_medianRow10_u2), 
  .endianswapper_30bae045_out(endianswapper_30bae045_out));
endmodule



module medianRow10_Kicker_64(CLK, RESET, bus_676f3404_);
input		CLK;
input		RESET;
output		bus_676f3404_;
reg		kicker_2=1'h0;
wire		bus_17d18850_;
wire		bus_1cefd96a_;
wire		bus_1e40529d_;
reg		kicker_res=1'h0;
reg		kicker_1=1'h0;
wire		bus_342d5463_;
assign bus_676f3404_=kicker_res;
always @(posedge CLK)
begin
kicker_2<=bus_17d18850_;
end
assign bus_17d18850_=bus_1cefd96a_&kicker_1;
assign bus_1cefd96a_=~RESET;
assign bus_1e40529d_=~kicker_2;
always @(posedge CLK)
begin
kicker_res<=bus_342d5463_;
end
always @(posedge CLK)
begin
kicker_1<=bus_1cefd96a_;
end
assign bus_342d5463_=kicker_1&bus_1cefd96a_&bus_1e40529d_;
endmodule



module medianRow10_globalreset_physical_1cb566de_(bus_78d8658c_, bus_76ebc647_, bus_57d24d46_);
input		bus_78d8658c_;
input		bus_76ebc647_;
output		bus_57d24d46_;
reg		final_u64=1'h1;
wire		or_7571ace6_u0;
wire		not_0b3f4d66_u0;
reg		sample_u64=1'h0;
reg		cross_u64=1'h0;
reg		glitch_u64=1'h0;
wire		and_2d42ecad_u0;
always @(posedge bus_78d8658c_)
begin
final_u64<=not_0b3f4d66_u0;
end
assign bus_57d24d46_=or_7571ace6_u0;
assign or_7571ace6_u0=bus_76ebc647_|final_u64;
assign not_0b3f4d66_u0=~and_2d42ecad_u0;
always @(posedge bus_78d8658c_)
begin
sample_u64<=1'h1;
end
always @(posedge bus_78d8658c_)
begin
cross_u64<=sample_u64;
end
always @(posedge bus_78d8658c_)
begin
glitch_u64<=cross_u64;
end
assign and_2d42ecad_u0=cross_u64&glitch_u64;
endmodule



module medianRow10_scheduler(CLK, RESET, GO, port_1a957bf9_, port_1a4f064d_, port_0de4af4d_, port_3206ef84_, port_212e157d_, port_6de852fe_, RESULT, RESULT_u2380, RESULT_u2381, RESULT_u2382, DONE);
input		CLK;
input		RESET;
input		GO;
input		port_1a957bf9_;
input	[31:0]	port_1a4f064d_;
input		port_0de4af4d_;
input		port_3206ef84_;
input		port_212e157d_;
input		port_6de852fe_;
output		RESULT;
output		RESULT_u2380;
output		RESULT_u2381;
output		RESULT_u2382;
output		DONE;
wire		and_u4053_u0;
wire		lessThan;
wire signed	[31:0]	lessThan_b_signed;
wire signed	[31:0]	lessThan_a_signed;
wire signed	[31:0]	equals_a_signed;
wire		equals;
wire signed	[31:0]	equals_b_signed;
wire		equals_u246;
wire signed	[31:0]	equals_u246_b_signed;
wire signed	[31:0]	equals_u246_a_signed;
wire		and_u4054_u0;
wire		and_u4055_u0;
wire		not_u939_u0;
wire		andOp;
wire		not_u940_u0;
wire		and_u4056_u0;
wire		and_u4057_u0;
wire		simplePinWrite;
wire		and_u4058_u0;
wire		and_u4059_u0;
wire signed	[31:0]	equals_u247_b_signed;
wire signed	[31:0]	equals_u247_a_signed;
wire		equals_u247;
wire		and_u4060_u0;
wire		not_u941_u0;
wire		and_u4061_u0;
wire		andOp_u86;
wire		and_u4062_u0;
wire		and_u4063_u0;
wire		not_u942_u0;
wire		simplePinWrite_u648;
wire		and_u4064_u0;
wire		and_u4065_u0;
wire		not_u943_u0;
wire		not_u944_u0;
wire		and_u4066_u0;
wire		and_u4067_u0;
wire		simplePinWrite_u649;
wire		and_u4068_u0;
reg		reg_1d837e90_u0=1'h0;
wire		or_u1562_u0;
wire		and_u4069_u0;
wire		and_u4070_u0;
wire		or_u1563_u0;
reg		reg_2d9d4a97_u0=1'h0;
wire		and_u4071_u0;
reg		reg_2ca562cb_u0=1'h0;
wire		or_u1564_u0;
wire		and_u4072_u0;
wire		and_u4073_u0;
wire		or_u1565_u0;
wire		mux_u2070;
wire		and_u4074_u0;
reg		and_delayed_u410=1'h0;
wire		or_u1566_u0;
wire		and_u4075_u0;
wire		or_u1567_u0;
wire		mux_u2071_u0;
wire		receive_go_merge;
reg		syncEnable_u1568=1'h0;
reg		loopControl_u109=1'h0;
wire		or_u1568_u0;
wire		mux_u2072_u0;
wire		or_u1569_u0;
reg		reg_654b4733_u0=1'h0;
reg		reg_0cd9f4e4_u0=1'h0;
assign and_u4053_u0=or_u1568_u0&or_u1568_u0;
assign lessThan_a_signed=port_1a4f064d_;
assign lessThan_b_signed=32'h65;
assign lessThan=lessThan_a_signed<lessThan_b_signed;
assign equals_a_signed=port_1a4f064d_;
assign equals_b_signed=32'h65;
assign equals=equals_a_signed==equals_b_signed;
assign equals_u246_a_signed={31'b0, port_1a957bf9_};
assign equals_u246_b_signed=32'h0;
assign equals_u246=equals_u246_a_signed==equals_u246_b_signed;
assign and_u4054_u0=and_u4053_u0&equals_u246;
assign and_u4055_u0=and_u4053_u0&not_u939_u0;
assign not_u939_u0=~equals_u246;
assign andOp=lessThan&port_3206ef84_;
assign not_u940_u0=~andOp;
assign and_u4056_u0=and_u4059_u0&not_u940_u0;
assign and_u4057_u0=and_u4059_u0&andOp;
assign simplePinWrite=and_u4058_u0&{1{and_u4058_u0}};
assign and_u4058_u0=and_u4057_u0&and_u4059_u0;
assign and_u4059_u0=and_u4054_u0&and_u4053_u0;
assign equals_u247_a_signed={31'b0, port_1a957bf9_};
assign equals_u247_b_signed=32'h1;
assign equals_u247=equals_u247_a_signed==equals_u247_b_signed;
assign and_u4060_u0=and_u4053_u0&not_u941_u0;
assign not_u941_u0=~equals_u247;
assign and_u4061_u0=and_u4053_u0&equals_u247;
assign andOp_u86=lessThan&port_3206ef84_;
assign and_u4062_u0=and_u4075_u0&andOp_u86;
assign and_u4063_u0=and_u4075_u0&not_u942_u0;
assign not_u942_u0=~andOp_u86;
assign simplePinWrite_u648=and_u4072_u0&{1{and_u4072_u0}};
assign and_u4064_u0=and_u4073_u0&not_u943_u0;
assign and_u4065_u0=and_u4073_u0&equals;
assign not_u943_u0=~equals;
assign not_u944_u0=~port_0de4af4d_;
assign and_u4066_u0=and_u4070_u0&port_0de4af4d_;
assign and_u4067_u0=and_u4070_u0&not_u944_u0;
assign simplePinWrite_u649=and_u4068_u0&{1{and_u4068_u0}};
assign and_u4068_u0=and_u4066_u0&and_u4070_u0;
always @(posedge CLK or posedge RESET)
begin
if (RESET)
reg_1d837e90_u0<=1'h0;
else reg_1d837e90_u0<=and_u4069_u0;
end
assign or_u1562_u0=reg_1d837e90_u0|port_6de852fe_;
assign and_u4069_u0=and_u4067_u0&and_u4070_u0;
assign and_u4070_u0=and_u4065_u0&and_u4073_u0;
assign or_u1563_u0=reg_2d9d4a97_u0|or_u1562_u0;
always @(posedge CLK or posedge RESET)
begin
if (RESET)
reg_2d9d4a97_u0<=1'h0;
else reg_2d9d4a97_u0<=and_u4071_u0;
end
assign and_u4071_u0=and_u4064_u0&and_u4073_u0;
always @(posedge CLK or posedge RESET)
begin
if (RESET)
reg_2ca562cb_u0<=1'h0;
else reg_2ca562cb_u0<=and_u4072_u0;
end
assign or_u1564_u0=reg_2ca562cb_u0|or_u1563_u0;
assign and_u4072_u0=and_u4062_u0&and_u4075_u0;
assign and_u4073_u0=and_u4063_u0&and_u4075_u0;
assign or_u1565_u0=and_u4072_u0|and_u4068_u0;
assign mux_u2070=(and_u4072_u0)?1'h1:1'h0;
assign and_u4074_u0=and_u4060_u0&and_u4053_u0;
always @(posedge CLK or posedge RESET)
begin
if (RESET)
and_delayed_u410<=1'h0;
else and_delayed_u410<=and_u4074_u0;
end
assign or_u1566_u0=and_delayed_u410|or_u1564_u0;
assign and_u4075_u0=and_u4061_u0&and_u4053_u0;
assign or_u1567_u0=and_u4058_u0|or_u1565_u0;
assign mux_u2071_u0=(and_u4058_u0)?1'h1:mux_u2070;
assign receive_go_merge=simplePinWrite|simplePinWrite_u648;
always @(posedge CLK)
begin
if (reg_654b4733_u0)
syncEnable_u1568<=RESET;
end
always @(posedge CLK or posedge syncEnable_u1568)
begin
if (syncEnable_u1568)
loopControl_u109<=1'h0;
else loopControl_u109<=or_u1566_u0;
end
assign or_u1568_u0=reg_654b4733_u0|loopControl_u109;
assign mux_u2072_u0=(GO)?1'h0:mux_u2071_u0;
assign or_u1569_u0=GO|or_u1567_u0;
always @(posedge CLK or posedge RESET)
begin
if (RESET)
reg_654b4733_u0<=1'h0;
else reg_654b4733_u0<=reg_0cd9f4e4_u0;
end
always @(posedge CLK or posedge RESET)
begin
if (RESET)
reg_0cd9f4e4_u0<=1'h0;
else reg_0cd9f4e4_u0<=GO;
end
assign RESULT=or_u1569_u0;
assign RESULT_u2380=mux_u2072_u0;
assign RESULT_u2381=simplePinWrite_u649;
assign RESULT_u2382=receive_go_merge;
assign DONE=1'h0;
endmodule



module medianRow10_forge_memory_101x32_157(CLK, ENA, WEA, DINA, ENB, ADDRA, ADDRB, DOUTA, DOUTB, DONEA, DONEB);
input		CLK;
input		ENA;
input		WEA;
input	[31:0]	DINA;
input		ENB;
input	[31:0]	ADDRA;
input	[31:0]	ADDRB;
output	[31:0]	DOUTA;
output	[31:0]	DOUTB;
output		DONEA;
output		DONEB;
wire		wea_0;
wire	[31:0]	pre_douta_0;
wire	[31:0]	pre_doutb_0;
wire		wea_1;
wire	[31:0]	pre_douta_1;
wire	[31:0]	pre_doutb_1;
reg		wea_done;
reg		rea_done;
reg	[31:0]	mux_outa;
reg	[31:0]	mux_outb;
assign wea_0=WEA&(ADDRA[31:6]==26'h0);
assign wea_1=WEA&(ADDRA[31:6]==26'h1);
always @(posedge CLK)
begin
wea_done<=WEA;
end
assign DONEA=rea_done|wea_done;
always @(posedge CLK)
begin
end
assign DONEB=ENB;
always @(pre_douta_0 or pre_douta_1 or ADDRA)
begin
case (ADDRA[31:6])26'd0:mux_outa=pre_douta_0;
26'd1:mux_outa=pre_douta_1;
default:mux_outa=32'h0;
endcase end
assign DOUTA=mux_outa;
always @(pre_doutb_0 or pre_doutb_1 or ADDRB)
begin
case (ADDRB[31:6])26'd0:mux_outb=pre_doutb_0;
26'd1:mux_outb=pre_doutb_1;
default:mux_outb=32'h0;
endcase end
assign DOUTB=mux_outb;
// Memory array element: COL: 0, ROW: 0
//  Initialization of Dual Port LUT ram now done through explicit
// parameter setting.
RAM64X1D#(.INIT(64'h0000000000000000))RAM64X1D_instance_3328(.WCLK(CLK), .WE(wea_0), 
  .A0(ADDRA[0]), .A1(ADDRA[1]), .A2(ADDRA[2]), .A3(ADDRA[3]), .A4(ADDRA[4]), .A5(ADDRA[5]), 
  .D(DINA[0]), .SPO(pre_douta_0[0]), .DPRA0(ADDRB[0]), .DPRA1(ADDRB[1]), .DPRA2(ADDRB[2]), 
  .DPRA3(ADDRB[3]), .DPRA4(ADDRB[4]), .DPRA5(ADDRB[5]), .DPO(pre_doutb_0[0]));
// Memory array element: COL: 0, ROW: 1
//  Initialization of Dual Port LUT ram now done through explicit
// parameter setting.
RAM64X1D#(.INIT(64'h0000000000000000))RAM64X1D_instance_3329(.WCLK(CLK), .WE(wea_0), 
  .A0(ADDRA[0]), .A1(ADDRA[1]), .A2(ADDRA[2]), .A3(ADDRA[3]), .A4(ADDRA[4]), .A5(ADDRA[5]), 
  .D(DINA[1]), .SPO(pre_douta_0[1]), .DPRA0(ADDRB[0]), .DPRA1(ADDRB[1]), .DPRA2(ADDRB[2]), 
  .DPRA3(ADDRB[3]), .DPRA4(ADDRB[4]), .DPRA5(ADDRB[5]), .DPO(pre_doutb_0[1]));
// Memory array element: COL: 0, ROW: 2
//  Initialization of Dual Port LUT ram now done through explicit
// parameter setting.
RAM64X1D#(.INIT(64'h0000000000000000))RAM64X1D_instance_3330(.WCLK(CLK), .WE(wea_0), 
  .A0(ADDRA[0]), .A1(ADDRA[1]), .A2(ADDRA[2]), .A3(ADDRA[3]), .A4(ADDRA[4]), .A5(ADDRA[5]), 
  .D(DINA[2]), .SPO(pre_douta_0[2]), .DPRA0(ADDRB[0]), .DPRA1(ADDRB[1]), .DPRA2(ADDRB[2]), 
  .DPRA3(ADDRB[3]), .DPRA4(ADDRB[4]), .DPRA5(ADDRB[5]), .DPO(pre_doutb_0[2]));
// Memory array element: COL: 0, ROW: 3
//  Initialization of Dual Port LUT ram now done through explicit
// parameter setting.
RAM64X1D#(.INIT(64'h0000000000000000))RAM64X1D_instance_3331(.WCLK(CLK), .WE(wea_0), 
  .A0(ADDRA[0]), .A1(ADDRA[1]), .A2(ADDRA[2]), .A3(ADDRA[3]), .A4(ADDRA[4]), .A5(ADDRA[5]), 
  .D(DINA[3]), .SPO(pre_douta_0[3]), .DPRA0(ADDRB[0]), .DPRA1(ADDRB[1]), .DPRA2(ADDRB[2]), 
  .DPRA3(ADDRB[3]), .DPRA4(ADDRB[4]), .DPRA5(ADDRB[5]), .DPO(pre_doutb_0[3]));
// Memory array element: COL: 0, ROW: 4
//  Initialization of Dual Port LUT ram now done through explicit
// parameter setting.
RAM64X1D#(.INIT(64'h0000000000000000))RAM64X1D_instance_3332(.WCLK(CLK), .WE(wea_0), 
  .A0(ADDRA[0]), .A1(ADDRA[1]), .A2(ADDRA[2]), .A3(ADDRA[3]), .A4(ADDRA[4]), .A5(ADDRA[5]), 
  .D(DINA[4]), .SPO(pre_douta_0[4]), .DPRA0(ADDRB[0]), .DPRA1(ADDRB[1]), .DPRA2(ADDRB[2]), 
  .DPRA3(ADDRB[3]), .DPRA4(ADDRB[4]), .DPRA5(ADDRB[5]), .DPO(pre_doutb_0[4]));
// Memory array element: COL: 0, ROW: 5
//  Initialization of Dual Port LUT ram now done through explicit
// parameter setting.
RAM64X1D#(.INIT(64'h0000000000000000))RAM64X1D_instance_3333(.WCLK(CLK), .WE(wea_0), 
  .A0(ADDRA[0]), .A1(ADDRA[1]), .A2(ADDRA[2]), .A3(ADDRA[3]), .A4(ADDRA[4]), .A5(ADDRA[5]), 
  .D(DINA[5]), .SPO(pre_douta_0[5]), .DPRA0(ADDRB[0]), .DPRA1(ADDRB[1]), .DPRA2(ADDRB[2]), 
  .DPRA3(ADDRB[3]), .DPRA4(ADDRB[4]), .DPRA5(ADDRB[5]), .DPO(pre_doutb_0[5]));
// Memory array element: COL: 0, ROW: 6
//  Initialization of Dual Port LUT ram now done through explicit
// parameter setting.
RAM64X1D#(.INIT(64'h0000000000000000))RAM64X1D_instance_3334(.WCLK(CLK), .WE(wea_0), 
  .A0(ADDRA[0]), .A1(ADDRA[1]), .A2(ADDRA[2]), .A3(ADDRA[3]), .A4(ADDRA[4]), .A5(ADDRA[5]), 
  .D(DINA[6]), .SPO(pre_douta_0[6]), .DPRA0(ADDRB[0]), .DPRA1(ADDRB[1]), .DPRA2(ADDRB[2]), 
  .DPRA3(ADDRB[3]), .DPRA4(ADDRB[4]), .DPRA5(ADDRB[5]), .DPO(pre_doutb_0[6]));
// Memory array element: COL: 0, ROW: 7
//  Initialization of Dual Port LUT ram now done through explicit
// parameter setting.
RAM64X1D#(.INIT(64'h0000000000000000))RAM64X1D_instance_3335(.WCLK(CLK), .WE(wea_0), 
  .A0(ADDRA[0]), .A1(ADDRA[1]), .A2(ADDRA[2]), .A3(ADDRA[3]), .A4(ADDRA[4]), .A5(ADDRA[5]), 
  .D(DINA[7]), .SPO(pre_douta_0[7]), .DPRA0(ADDRB[0]), .DPRA1(ADDRB[1]), .DPRA2(ADDRB[2]), 
  .DPRA3(ADDRB[3]), .DPRA4(ADDRB[4]), .DPRA5(ADDRB[5]), .DPO(pre_doutb_0[7]));
// Memory array element: COL: 0, ROW: 8
//  Initialization of Dual Port LUT ram now done through explicit
// parameter setting.
RAM64X1D#(.INIT(64'h0000000000000000))RAM64X1D_instance_3336(.WCLK(CLK), .WE(wea_0), 
  .A0(ADDRA[0]), .A1(ADDRA[1]), .A2(ADDRA[2]), .A3(ADDRA[3]), .A4(ADDRA[4]), .A5(ADDRA[5]), 
  .D(DINA[8]), .SPO(pre_douta_0[8]), .DPRA0(ADDRB[0]), .DPRA1(ADDRB[1]), .DPRA2(ADDRB[2]), 
  .DPRA3(ADDRB[3]), .DPRA4(ADDRB[4]), .DPRA5(ADDRB[5]), .DPO(pre_doutb_0[8]));
// Memory array element: COL: 0, ROW: 9
//  Initialization of Dual Port LUT ram now done through explicit
// parameter setting.
RAM64X1D#(.INIT(64'h0000000000000000))RAM64X1D_instance_3337(.WCLK(CLK), .WE(wea_0), 
  .A0(ADDRA[0]), .A1(ADDRA[1]), .A2(ADDRA[2]), .A3(ADDRA[3]), .A4(ADDRA[4]), .A5(ADDRA[5]), 
  .D(DINA[9]), .SPO(pre_douta_0[9]), .DPRA0(ADDRB[0]), .DPRA1(ADDRB[1]), .DPRA2(ADDRB[2]), 
  .DPRA3(ADDRB[3]), .DPRA4(ADDRB[4]), .DPRA5(ADDRB[5]), .DPO(pre_doutb_0[9]));
// Memory array element: COL: 0, ROW: 10
//  Initialization of Dual Port LUT ram now done through explicit
// parameter setting.
RAM64X1D#(.INIT(64'h0000000000000000))RAM64X1D_instance_3338(.WCLK(CLK), .WE(wea_0), 
  .A0(ADDRA[0]), .A1(ADDRA[1]), .A2(ADDRA[2]), .A3(ADDRA[3]), .A4(ADDRA[4]), .A5(ADDRA[5]), 
  .D(DINA[10]), .SPO(pre_douta_0[10]), .DPRA0(ADDRB[0]), .DPRA1(ADDRB[1]), .DPRA2(ADDRB[2]), 
  .DPRA3(ADDRB[3]), .DPRA4(ADDRB[4]), .DPRA5(ADDRB[5]), .DPO(pre_doutb_0[10]));
// Memory array element: COL: 0, ROW: 11
//  Initialization of Dual Port LUT ram now done through explicit
// parameter setting.
RAM64X1D#(.INIT(64'h0000000000000000))RAM64X1D_instance_3339(.WCLK(CLK), .WE(wea_0), 
  .A0(ADDRA[0]), .A1(ADDRA[1]), .A2(ADDRA[2]), .A3(ADDRA[3]), .A4(ADDRA[4]), .A5(ADDRA[5]), 
  .D(DINA[11]), .SPO(pre_douta_0[11]), .DPRA0(ADDRB[0]), .DPRA1(ADDRB[1]), .DPRA2(ADDRB[2]), 
  .DPRA3(ADDRB[3]), .DPRA4(ADDRB[4]), .DPRA5(ADDRB[5]), .DPO(pre_doutb_0[11]));
// Memory array element: COL: 0, ROW: 12
//  Initialization of Dual Port LUT ram now done through explicit
// parameter setting.
RAM64X1D#(.INIT(64'h0000000000000000))RAM64X1D_instance_3340(.WCLK(CLK), .WE(wea_0), 
  .A0(ADDRA[0]), .A1(ADDRA[1]), .A2(ADDRA[2]), .A3(ADDRA[3]), .A4(ADDRA[4]), .A5(ADDRA[5]), 
  .D(DINA[12]), .SPO(pre_douta_0[12]), .DPRA0(ADDRB[0]), .DPRA1(ADDRB[1]), .DPRA2(ADDRB[2]), 
  .DPRA3(ADDRB[3]), .DPRA4(ADDRB[4]), .DPRA5(ADDRB[5]), .DPO(pre_doutb_0[12]));
// Memory array element: COL: 0, ROW: 13
//  Initialization of Dual Port LUT ram now done through explicit
// parameter setting.
RAM64X1D#(.INIT(64'h0000000000000000))RAM64X1D_instance_3341(.WCLK(CLK), .WE(wea_0), 
  .A0(ADDRA[0]), .A1(ADDRA[1]), .A2(ADDRA[2]), .A3(ADDRA[3]), .A4(ADDRA[4]), .A5(ADDRA[5]), 
  .D(DINA[13]), .SPO(pre_douta_0[13]), .DPRA0(ADDRB[0]), .DPRA1(ADDRB[1]), .DPRA2(ADDRB[2]), 
  .DPRA3(ADDRB[3]), .DPRA4(ADDRB[4]), .DPRA5(ADDRB[5]), .DPO(pre_doutb_0[13]));
// Memory array element: COL: 0, ROW: 14
//  Initialization of Dual Port LUT ram now done through explicit
// parameter setting.
RAM64X1D#(.INIT(64'h0000000000000000))RAM64X1D_instance_3342(.WCLK(CLK), .WE(wea_0), 
  .A0(ADDRA[0]), .A1(ADDRA[1]), .A2(ADDRA[2]), .A3(ADDRA[3]), .A4(ADDRA[4]), .A5(ADDRA[5]), 
  .D(DINA[14]), .SPO(pre_douta_0[14]), .DPRA0(ADDRB[0]), .DPRA1(ADDRB[1]), .DPRA2(ADDRB[2]), 
  .DPRA3(ADDRB[3]), .DPRA4(ADDRB[4]), .DPRA5(ADDRB[5]), .DPO(pre_doutb_0[14]));
// Memory array element: COL: 0, ROW: 15
//  Initialization of Dual Port LUT ram now done through explicit
// parameter setting.
RAM64X1D#(.INIT(64'h0000000000000000))RAM64X1D_instance_3343(.WCLK(CLK), .WE(wea_0), 
  .A0(ADDRA[0]), .A1(ADDRA[1]), .A2(ADDRA[2]), .A3(ADDRA[3]), .A4(ADDRA[4]), .A5(ADDRA[5]), 
  .D(DINA[15]), .SPO(pre_douta_0[15]), .DPRA0(ADDRB[0]), .DPRA1(ADDRB[1]), .DPRA2(ADDRB[2]), 
  .DPRA3(ADDRB[3]), .DPRA4(ADDRB[4]), .DPRA5(ADDRB[5]), .DPO(pre_doutb_0[15]));
// Memory array element: COL: 0, ROW: 16
//  Initialization of Dual Port LUT ram now done through explicit
// parameter setting.
RAM64X1D#(.INIT(64'h0000000000000000))RAM64X1D_instance_3344(.WCLK(CLK), .WE(wea_0), 
  .A0(ADDRA[0]), .A1(ADDRA[1]), .A2(ADDRA[2]), .A3(ADDRA[3]), .A4(ADDRA[4]), .A5(ADDRA[5]), 
  .D(DINA[16]), .SPO(pre_douta_0[16]), .DPRA0(ADDRB[0]), .DPRA1(ADDRB[1]), .DPRA2(ADDRB[2]), 
  .DPRA3(ADDRB[3]), .DPRA4(ADDRB[4]), .DPRA5(ADDRB[5]), .DPO(pre_doutb_0[16]));
// Memory array element: COL: 0, ROW: 17
//  Initialization of Dual Port LUT ram now done through explicit
// parameter setting.
RAM64X1D#(.INIT(64'h0000000000000000))RAM64X1D_instance_3345(.WCLK(CLK), .WE(wea_0), 
  .A0(ADDRA[0]), .A1(ADDRA[1]), .A2(ADDRA[2]), .A3(ADDRA[3]), .A4(ADDRA[4]), .A5(ADDRA[5]), 
  .D(DINA[17]), .SPO(pre_douta_0[17]), .DPRA0(ADDRB[0]), .DPRA1(ADDRB[1]), .DPRA2(ADDRB[2]), 
  .DPRA3(ADDRB[3]), .DPRA4(ADDRB[4]), .DPRA5(ADDRB[5]), .DPO(pre_doutb_0[17]));
// Memory array element: COL: 0, ROW: 18
//  Initialization of Dual Port LUT ram now done through explicit
// parameter setting.
RAM64X1D#(.INIT(64'h0000000000000000))RAM64X1D_instance_3346(.WCLK(CLK), .WE(wea_0), 
  .A0(ADDRA[0]), .A1(ADDRA[1]), .A2(ADDRA[2]), .A3(ADDRA[3]), .A4(ADDRA[4]), .A5(ADDRA[5]), 
  .D(DINA[18]), .SPO(pre_douta_0[18]), .DPRA0(ADDRB[0]), .DPRA1(ADDRB[1]), .DPRA2(ADDRB[2]), 
  .DPRA3(ADDRB[3]), .DPRA4(ADDRB[4]), .DPRA5(ADDRB[5]), .DPO(pre_doutb_0[18]));
// Memory array element: COL: 0, ROW: 19
//  Initialization of Dual Port LUT ram now done through explicit
// parameter setting.
RAM64X1D#(.INIT(64'h0000000000000000))RAM64X1D_instance_3347(.WCLK(CLK), .WE(wea_0), 
  .A0(ADDRA[0]), .A1(ADDRA[1]), .A2(ADDRA[2]), .A3(ADDRA[3]), .A4(ADDRA[4]), .A5(ADDRA[5]), 
  .D(DINA[19]), .SPO(pre_douta_0[19]), .DPRA0(ADDRB[0]), .DPRA1(ADDRB[1]), .DPRA2(ADDRB[2]), 
  .DPRA3(ADDRB[3]), .DPRA4(ADDRB[4]), .DPRA5(ADDRB[5]), .DPO(pre_doutb_0[19]));
// Memory array element: COL: 0, ROW: 20
//  Initialization of Dual Port LUT ram now done through explicit
// parameter setting.
RAM64X1D#(.INIT(64'h0000000000000000))RAM64X1D_instance_3348(.WCLK(CLK), .WE(wea_0), 
  .A0(ADDRA[0]), .A1(ADDRA[1]), .A2(ADDRA[2]), .A3(ADDRA[3]), .A4(ADDRA[4]), .A5(ADDRA[5]), 
  .D(DINA[20]), .SPO(pre_douta_0[20]), .DPRA0(ADDRB[0]), .DPRA1(ADDRB[1]), .DPRA2(ADDRB[2]), 
  .DPRA3(ADDRB[3]), .DPRA4(ADDRB[4]), .DPRA5(ADDRB[5]), .DPO(pre_doutb_0[20]));
// Memory array element: COL: 0, ROW: 21
//  Initialization of Dual Port LUT ram now done through explicit
// parameter setting.
RAM64X1D#(.INIT(64'h0000000000000000))RAM64X1D_instance_3349(.WCLK(CLK), .WE(wea_0), 
  .A0(ADDRA[0]), .A1(ADDRA[1]), .A2(ADDRA[2]), .A3(ADDRA[3]), .A4(ADDRA[4]), .A5(ADDRA[5]), 
  .D(DINA[21]), .SPO(pre_douta_0[21]), .DPRA0(ADDRB[0]), .DPRA1(ADDRB[1]), .DPRA2(ADDRB[2]), 
  .DPRA3(ADDRB[3]), .DPRA4(ADDRB[4]), .DPRA5(ADDRB[5]), .DPO(pre_doutb_0[21]));
// Memory array element: COL: 0, ROW: 22
//  Initialization of Dual Port LUT ram now done through explicit
// parameter setting.
RAM64X1D#(.INIT(64'h0000000000000000))RAM64X1D_instance_3350(.WCLK(CLK), .WE(wea_0), 
  .A0(ADDRA[0]), .A1(ADDRA[1]), .A2(ADDRA[2]), .A3(ADDRA[3]), .A4(ADDRA[4]), .A5(ADDRA[5]), 
  .D(DINA[22]), .SPO(pre_douta_0[22]), .DPRA0(ADDRB[0]), .DPRA1(ADDRB[1]), .DPRA2(ADDRB[2]), 
  .DPRA3(ADDRB[3]), .DPRA4(ADDRB[4]), .DPRA5(ADDRB[5]), .DPO(pre_doutb_0[22]));
// Memory array element: COL: 0, ROW: 23
//  Initialization of Dual Port LUT ram now done through explicit
// parameter setting.
RAM64X1D#(.INIT(64'h0000000000000000))RAM64X1D_instance_3351(.WCLK(CLK), .WE(wea_0), 
  .A0(ADDRA[0]), .A1(ADDRA[1]), .A2(ADDRA[2]), .A3(ADDRA[3]), .A4(ADDRA[4]), .A5(ADDRA[5]), 
  .D(DINA[23]), .SPO(pre_douta_0[23]), .DPRA0(ADDRB[0]), .DPRA1(ADDRB[1]), .DPRA2(ADDRB[2]), 
  .DPRA3(ADDRB[3]), .DPRA4(ADDRB[4]), .DPRA5(ADDRB[5]), .DPO(pre_doutb_0[23]));
// Memory array element: COL: 0, ROW: 24
//  Initialization of Dual Port LUT ram now done through explicit
// parameter setting.
RAM64X1D#(.INIT(64'h0000000000000000))RAM64X1D_instance_3352(.WCLK(CLK), .WE(wea_0), 
  .A0(ADDRA[0]), .A1(ADDRA[1]), .A2(ADDRA[2]), .A3(ADDRA[3]), .A4(ADDRA[4]), .A5(ADDRA[5]), 
  .D(DINA[24]), .SPO(pre_douta_0[24]), .DPRA0(ADDRB[0]), .DPRA1(ADDRB[1]), .DPRA2(ADDRB[2]), 
  .DPRA3(ADDRB[3]), .DPRA4(ADDRB[4]), .DPRA5(ADDRB[5]), .DPO(pre_doutb_0[24]));
// Memory array element: COL: 0, ROW: 25
//  Initialization of Dual Port LUT ram now done through explicit
// parameter setting.
RAM64X1D#(.INIT(64'h0000000000000000))RAM64X1D_instance_3353(.WCLK(CLK), .WE(wea_0), 
  .A0(ADDRA[0]), .A1(ADDRA[1]), .A2(ADDRA[2]), .A3(ADDRA[3]), .A4(ADDRA[4]), .A5(ADDRA[5]), 
  .D(DINA[25]), .SPO(pre_douta_0[25]), .DPRA0(ADDRB[0]), .DPRA1(ADDRB[1]), .DPRA2(ADDRB[2]), 
  .DPRA3(ADDRB[3]), .DPRA4(ADDRB[4]), .DPRA5(ADDRB[5]), .DPO(pre_doutb_0[25]));
// Memory array element: COL: 0, ROW: 26
//  Initialization of Dual Port LUT ram now done through explicit
// parameter setting.
RAM64X1D#(.INIT(64'h0000000000000000))RAM64X1D_instance_3354(.WCLK(CLK), .WE(wea_0), 
  .A0(ADDRA[0]), .A1(ADDRA[1]), .A2(ADDRA[2]), .A3(ADDRA[3]), .A4(ADDRA[4]), .A5(ADDRA[5]), 
  .D(DINA[26]), .SPO(pre_douta_0[26]), .DPRA0(ADDRB[0]), .DPRA1(ADDRB[1]), .DPRA2(ADDRB[2]), 
  .DPRA3(ADDRB[3]), .DPRA4(ADDRB[4]), .DPRA5(ADDRB[5]), .DPO(pre_doutb_0[26]));
// Memory array element: COL: 0, ROW: 27
//  Initialization of Dual Port LUT ram now done through explicit
// parameter setting.
RAM64X1D#(.INIT(64'h0000000000000000))RAM64X1D_instance_3355(.WCLK(CLK), .WE(wea_0), 
  .A0(ADDRA[0]), .A1(ADDRA[1]), .A2(ADDRA[2]), .A3(ADDRA[3]), .A4(ADDRA[4]), .A5(ADDRA[5]), 
  .D(DINA[27]), .SPO(pre_douta_0[27]), .DPRA0(ADDRB[0]), .DPRA1(ADDRB[1]), .DPRA2(ADDRB[2]), 
  .DPRA3(ADDRB[3]), .DPRA4(ADDRB[4]), .DPRA5(ADDRB[5]), .DPO(pre_doutb_0[27]));
// Memory array element: COL: 0, ROW: 28
//  Initialization of Dual Port LUT ram now done through explicit
// parameter setting.
RAM64X1D#(.INIT(64'h0000000000000000))RAM64X1D_instance_3356(.WCLK(CLK), .WE(wea_0), 
  .A0(ADDRA[0]), .A1(ADDRA[1]), .A2(ADDRA[2]), .A3(ADDRA[3]), .A4(ADDRA[4]), .A5(ADDRA[5]), 
  .D(DINA[28]), .SPO(pre_douta_0[28]), .DPRA0(ADDRB[0]), .DPRA1(ADDRB[1]), .DPRA2(ADDRB[2]), 
  .DPRA3(ADDRB[3]), .DPRA4(ADDRB[4]), .DPRA5(ADDRB[5]), .DPO(pre_doutb_0[28]));
// Memory array element: COL: 0, ROW: 29
//  Initialization of Dual Port LUT ram now done through explicit
// parameter setting.
RAM64X1D#(.INIT(64'h0000000000000000))RAM64X1D_instance_3357(.WCLK(CLK), .WE(wea_0), 
  .A0(ADDRA[0]), .A1(ADDRA[1]), .A2(ADDRA[2]), .A3(ADDRA[3]), .A4(ADDRA[4]), .A5(ADDRA[5]), 
  .D(DINA[29]), .SPO(pre_douta_0[29]), .DPRA0(ADDRB[0]), .DPRA1(ADDRB[1]), .DPRA2(ADDRB[2]), 
  .DPRA3(ADDRB[3]), .DPRA4(ADDRB[4]), .DPRA5(ADDRB[5]), .DPO(pre_doutb_0[29]));
// Memory array element: COL: 0, ROW: 30
//  Initialization of Dual Port LUT ram now done through explicit
// parameter setting.
RAM64X1D#(.INIT(64'h0000000000000000))RAM64X1D_instance_3358(.WCLK(CLK), .WE(wea_0), 
  .A0(ADDRA[0]), .A1(ADDRA[1]), .A2(ADDRA[2]), .A3(ADDRA[3]), .A4(ADDRA[4]), .A5(ADDRA[5]), 
  .D(DINA[30]), .SPO(pre_douta_0[30]), .DPRA0(ADDRB[0]), .DPRA1(ADDRB[1]), .DPRA2(ADDRB[2]), 
  .DPRA3(ADDRB[3]), .DPRA4(ADDRB[4]), .DPRA5(ADDRB[5]), .DPO(pre_doutb_0[30]));
// Memory array element: COL: 0, ROW: 31
//  Initialization of Dual Port LUT ram now done through explicit
// parameter setting.
RAM64X1D#(.INIT(64'h0000000000000000))RAM64X1D_instance_3359(.WCLK(CLK), .WE(wea_0), 
  .A0(ADDRA[0]), .A1(ADDRA[1]), .A2(ADDRA[2]), .A3(ADDRA[3]), .A4(ADDRA[4]), .A5(ADDRA[5]), 
  .D(DINA[31]), .SPO(pre_douta_0[31]), .DPRA0(ADDRB[0]), .DPRA1(ADDRB[1]), .DPRA2(ADDRB[2]), 
  .DPRA3(ADDRB[3]), .DPRA4(ADDRB[4]), .DPRA5(ADDRB[5]), .DPO(pre_doutb_0[31]));
// Memory array element: COL: 1, ROW: 0
//  Initialization of Dual Port LUT ram now done through explicit
// parameter setting.
RAM64X1D#(.INIT(64'h0000000000000000))RAM64X1D_instance_3360(.WCLK(CLK), .WE(wea_1), 
  .A0(ADDRA[0]), .A1(ADDRA[1]), .A2(ADDRA[2]), .A3(ADDRA[3]), .A4(ADDRA[4]), .A5(ADDRA[5]), 
  .D(DINA[0]), .SPO(pre_douta_1[0]), .DPRA0(ADDRB[0]), .DPRA1(ADDRB[1]), .DPRA2(ADDRB[2]), 
  .DPRA3(ADDRB[3]), .DPRA4(ADDRB[4]), .DPRA5(ADDRB[5]), .DPO(pre_doutb_1[0]));
// Memory array element: COL: 1, ROW: 1
//  Initialization of Dual Port LUT ram now done through explicit
// parameter setting.
RAM64X1D#(.INIT(64'h0000000000000000))RAM64X1D_instance_3361(.WCLK(CLK), .WE(wea_1), 
  .A0(ADDRA[0]), .A1(ADDRA[1]), .A2(ADDRA[2]), .A3(ADDRA[3]), .A4(ADDRA[4]), .A5(ADDRA[5]), 
  .D(DINA[1]), .SPO(pre_douta_1[1]), .DPRA0(ADDRB[0]), .DPRA1(ADDRB[1]), .DPRA2(ADDRB[2]), 
  .DPRA3(ADDRB[3]), .DPRA4(ADDRB[4]), .DPRA5(ADDRB[5]), .DPO(pre_doutb_1[1]));
// Memory array element: COL: 1, ROW: 2
//  Initialization of Dual Port LUT ram now done through explicit
// parameter setting.
RAM64X1D#(.INIT(64'h0000000000000000))RAM64X1D_instance_3362(.WCLK(CLK), .WE(wea_1), 
  .A0(ADDRA[0]), .A1(ADDRA[1]), .A2(ADDRA[2]), .A3(ADDRA[3]), .A4(ADDRA[4]), .A5(ADDRA[5]), 
  .D(DINA[2]), .SPO(pre_douta_1[2]), .DPRA0(ADDRB[0]), .DPRA1(ADDRB[1]), .DPRA2(ADDRB[2]), 
  .DPRA3(ADDRB[3]), .DPRA4(ADDRB[4]), .DPRA5(ADDRB[5]), .DPO(pre_doutb_1[2]));
// Memory array element: COL: 1, ROW: 3
//  Initialization of Dual Port LUT ram now done through explicit
// parameter setting.
RAM64X1D#(.INIT(64'h0000000000000000))RAM64X1D_instance_3363(.WCLK(CLK), .WE(wea_1), 
  .A0(ADDRA[0]), .A1(ADDRA[1]), .A2(ADDRA[2]), .A3(ADDRA[3]), .A4(ADDRA[4]), .A5(ADDRA[5]), 
  .D(DINA[3]), .SPO(pre_douta_1[3]), .DPRA0(ADDRB[0]), .DPRA1(ADDRB[1]), .DPRA2(ADDRB[2]), 
  .DPRA3(ADDRB[3]), .DPRA4(ADDRB[4]), .DPRA5(ADDRB[5]), .DPO(pre_doutb_1[3]));
// Memory array element: COL: 1, ROW: 4
//  Initialization of Dual Port LUT ram now done through explicit
// parameter setting.
RAM64X1D#(.INIT(64'h0000000000000000))RAM64X1D_instance_3364(.WCLK(CLK), .WE(wea_1), 
  .A0(ADDRA[0]), .A1(ADDRA[1]), .A2(ADDRA[2]), .A3(ADDRA[3]), .A4(ADDRA[4]), .A5(ADDRA[5]), 
  .D(DINA[4]), .SPO(pre_douta_1[4]), .DPRA0(ADDRB[0]), .DPRA1(ADDRB[1]), .DPRA2(ADDRB[2]), 
  .DPRA3(ADDRB[3]), .DPRA4(ADDRB[4]), .DPRA5(ADDRB[5]), .DPO(pre_doutb_1[4]));
// Memory array element: COL: 1, ROW: 5
//  Initialization of Dual Port LUT ram now done through explicit
// parameter setting.
RAM64X1D#(.INIT(64'h0000000000000000))RAM64X1D_instance_3365(.WCLK(CLK), .WE(wea_1), 
  .A0(ADDRA[0]), .A1(ADDRA[1]), .A2(ADDRA[2]), .A3(ADDRA[3]), .A4(ADDRA[4]), .A5(ADDRA[5]), 
  .D(DINA[5]), .SPO(pre_douta_1[5]), .DPRA0(ADDRB[0]), .DPRA1(ADDRB[1]), .DPRA2(ADDRB[2]), 
  .DPRA3(ADDRB[3]), .DPRA4(ADDRB[4]), .DPRA5(ADDRB[5]), .DPO(pre_doutb_1[5]));
// Memory array element: COL: 1, ROW: 6
//  Initialization of Dual Port LUT ram now done through explicit
// parameter setting.
RAM64X1D#(.INIT(64'h0000000000000000))RAM64X1D_instance_3366(.WCLK(CLK), .WE(wea_1), 
  .A0(ADDRA[0]), .A1(ADDRA[1]), .A2(ADDRA[2]), .A3(ADDRA[3]), .A4(ADDRA[4]), .A5(ADDRA[5]), 
  .D(DINA[6]), .SPO(pre_douta_1[6]), .DPRA0(ADDRB[0]), .DPRA1(ADDRB[1]), .DPRA2(ADDRB[2]), 
  .DPRA3(ADDRB[3]), .DPRA4(ADDRB[4]), .DPRA5(ADDRB[5]), .DPO(pre_doutb_1[6]));
// Memory array element: COL: 1, ROW: 7
//  Initialization of Dual Port LUT ram now done through explicit
// parameter setting.
RAM64X1D#(.INIT(64'h0000000000000000))RAM64X1D_instance_3367(.WCLK(CLK), .WE(wea_1), 
  .A0(ADDRA[0]), .A1(ADDRA[1]), .A2(ADDRA[2]), .A3(ADDRA[3]), .A4(ADDRA[4]), .A5(ADDRA[5]), 
  .D(DINA[7]), .SPO(pre_douta_1[7]), .DPRA0(ADDRB[0]), .DPRA1(ADDRB[1]), .DPRA2(ADDRB[2]), 
  .DPRA3(ADDRB[3]), .DPRA4(ADDRB[4]), .DPRA5(ADDRB[5]), .DPO(pre_doutb_1[7]));
// Memory array element: COL: 1, ROW: 8
//  Initialization of Dual Port LUT ram now done through explicit
// parameter setting.
RAM64X1D#(.INIT(64'h0000000000000000))RAM64X1D_instance_3368(.WCLK(CLK), .WE(wea_1), 
  .A0(ADDRA[0]), .A1(ADDRA[1]), .A2(ADDRA[2]), .A3(ADDRA[3]), .A4(ADDRA[4]), .A5(ADDRA[5]), 
  .D(DINA[8]), .SPO(pre_douta_1[8]), .DPRA0(ADDRB[0]), .DPRA1(ADDRB[1]), .DPRA2(ADDRB[2]), 
  .DPRA3(ADDRB[3]), .DPRA4(ADDRB[4]), .DPRA5(ADDRB[5]), .DPO(pre_doutb_1[8]));
// Memory array element: COL: 1, ROW: 9
//  Initialization of Dual Port LUT ram now done through explicit
// parameter setting.
RAM64X1D#(.INIT(64'h0000000000000000))RAM64X1D_instance_3369(.WCLK(CLK), .WE(wea_1), 
  .A0(ADDRA[0]), .A1(ADDRA[1]), .A2(ADDRA[2]), .A3(ADDRA[3]), .A4(ADDRA[4]), .A5(ADDRA[5]), 
  .D(DINA[9]), .SPO(pre_douta_1[9]), .DPRA0(ADDRB[0]), .DPRA1(ADDRB[1]), .DPRA2(ADDRB[2]), 
  .DPRA3(ADDRB[3]), .DPRA4(ADDRB[4]), .DPRA5(ADDRB[5]), .DPO(pre_doutb_1[9]));
// Memory array element: COL: 1, ROW: 10
//  Initialization of Dual Port LUT ram now done through explicit
// parameter setting.
RAM64X1D#(.INIT(64'h0000000000000000))RAM64X1D_instance_3370(.WCLK(CLK), .WE(wea_1), 
  .A0(ADDRA[0]), .A1(ADDRA[1]), .A2(ADDRA[2]), .A3(ADDRA[3]), .A4(ADDRA[4]), .A5(ADDRA[5]), 
  .D(DINA[10]), .SPO(pre_douta_1[10]), .DPRA0(ADDRB[0]), .DPRA1(ADDRB[1]), .DPRA2(ADDRB[2]), 
  .DPRA3(ADDRB[3]), .DPRA4(ADDRB[4]), .DPRA5(ADDRB[5]), .DPO(pre_doutb_1[10]));
// Memory array element: COL: 1, ROW: 11
//  Initialization of Dual Port LUT ram now done through explicit
// parameter setting.
RAM64X1D#(.INIT(64'h0000000000000000))RAM64X1D_instance_3371(.WCLK(CLK), .WE(wea_1), 
  .A0(ADDRA[0]), .A1(ADDRA[1]), .A2(ADDRA[2]), .A3(ADDRA[3]), .A4(ADDRA[4]), .A5(ADDRA[5]), 
  .D(DINA[11]), .SPO(pre_douta_1[11]), .DPRA0(ADDRB[0]), .DPRA1(ADDRB[1]), .DPRA2(ADDRB[2]), 
  .DPRA3(ADDRB[3]), .DPRA4(ADDRB[4]), .DPRA5(ADDRB[5]), .DPO(pre_doutb_1[11]));
// Memory array element: COL: 1, ROW: 12
//  Initialization of Dual Port LUT ram now done through explicit
// parameter setting.
RAM64X1D#(.INIT(64'h0000000000000000))RAM64X1D_instance_3372(.WCLK(CLK), .WE(wea_1), 
  .A0(ADDRA[0]), .A1(ADDRA[1]), .A2(ADDRA[2]), .A3(ADDRA[3]), .A4(ADDRA[4]), .A5(ADDRA[5]), 
  .D(DINA[12]), .SPO(pre_douta_1[12]), .DPRA0(ADDRB[0]), .DPRA1(ADDRB[1]), .DPRA2(ADDRB[2]), 
  .DPRA3(ADDRB[3]), .DPRA4(ADDRB[4]), .DPRA5(ADDRB[5]), .DPO(pre_doutb_1[12]));
// Memory array element: COL: 1, ROW: 13
//  Initialization of Dual Port LUT ram now done through explicit
// parameter setting.
RAM64X1D#(.INIT(64'h0000000000000000))RAM64X1D_instance_3373(.WCLK(CLK), .WE(wea_1), 
  .A0(ADDRA[0]), .A1(ADDRA[1]), .A2(ADDRA[2]), .A3(ADDRA[3]), .A4(ADDRA[4]), .A5(ADDRA[5]), 
  .D(DINA[13]), .SPO(pre_douta_1[13]), .DPRA0(ADDRB[0]), .DPRA1(ADDRB[1]), .DPRA2(ADDRB[2]), 
  .DPRA3(ADDRB[3]), .DPRA4(ADDRB[4]), .DPRA5(ADDRB[5]), .DPO(pre_doutb_1[13]));
// Memory array element: COL: 1, ROW: 14
//  Initialization of Dual Port LUT ram now done through explicit
// parameter setting.
RAM64X1D#(.INIT(64'h0000000000000000))RAM64X1D_instance_3374(.WCLK(CLK), .WE(wea_1), 
  .A0(ADDRA[0]), .A1(ADDRA[1]), .A2(ADDRA[2]), .A3(ADDRA[3]), .A4(ADDRA[4]), .A5(ADDRA[5]), 
  .D(DINA[14]), .SPO(pre_douta_1[14]), .DPRA0(ADDRB[0]), .DPRA1(ADDRB[1]), .DPRA2(ADDRB[2]), 
  .DPRA3(ADDRB[3]), .DPRA4(ADDRB[4]), .DPRA5(ADDRB[5]), .DPO(pre_doutb_1[14]));
// Memory array element: COL: 1, ROW: 15
//  Initialization of Dual Port LUT ram now done through explicit
// parameter setting.
RAM64X1D#(.INIT(64'h0000000000000000))RAM64X1D_instance_3375(.WCLK(CLK), .WE(wea_1), 
  .A0(ADDRA[0]), .A1(ADDRA[1]), .A2(ADDRA[2]), .A3(ADDRA[3]), .A4(ADDRA[4]), .A5(ADDRA[5]), 
  .D(DINA[15]), .SPO(pre_douta_1[15]), .DPRA0(ADDRB[0]), .DPRA1(ADDRB[1]), .DPRA2(ADDRB[2]), 
  .DPRA3(ADDRB[3]), .DPRA4(ADDRB[4]), .DPRA5(ADDRB[5]), .DPO(pre_doutb_1[15]));
// Memory array element: COL: 1, ROW: 16
//  Initialization of Dual Port LUT ram now done through explicit
// parameter setting.
RAM64X1D#(.INIT(64'h0000000000000000))RAM64X1D_instance_3376(.WCLK(CLK), .WE(wea_1), 
  .A0(ADDRA[0]), .A1(ADDRA[1]), .A2(ADDRA[2]), .A3(ADDRA[3]), .A4(ADDRA[4]), .A5(ADDRA[5]), 
  .D(DINA[16]), .SPO(pre_douta_1[16]), .DPRA0(ADDRB[0]), .DPRA1(ADDRB[1]), .DPRA2(ADDRB[2]), 
  .DPRA3(ADDRB[3]), .DPRA4(ADDRB[4]), .DPRA5(ADDRB[5]), .DPO(pre_doutb_1[16]));
// Memory array element: COL: 1, ROW: 17
//  Initialization of Dual Port LUT ram now done through explicit
// parameter setting.
RAM64X1D#(.INIT(64'h0000000000000000))RAM64X1D_instance_3377(.WCLK(CLK), .WE(wea_1), 
  .A0(ADDRA[0]), .A1(ADDRA[1]), .A2(ADDRA[2]), .A3(ADDRA[3]), .A4(ADDRA[4]), .A5(ADDRA[5]), 
  .D(DINA[17]), .SPO(pre_douta_1[17]), .DPRA0(ADDRB[0]), .DPRA1(ADDRB[1]), .DPRA2(ADDRB[2]), 
  .DPRA3(ADDRB[3]), .DPRA4(ADDRB[4]), .DPRA5(ADDRB[5]), .DPO(pre_doutb_1[17]));
// Memory array element: COL: 1, ROW: 18
//  Initialization of Dual Port LUT ram now done through explicit
// parameter setting.
RAM64X1D#(.INIT(64'h0000000000000000))RAM64X1D_instance_3378(.WCLK(CLK), .WE(wea_1), 
  .A0(ADDRA[0]), .A1(ADDRA[1]), .A2(ADDRA[2]), .A3(ADDRA[3]), .A4(ADDRA[4]), .A5(ADDRA[5]), 
  .D(DINA[18]), .SPO(pre_douta_1[18]), .DPRA0(ADDRB[0]), .DPRA1(ADDRB[1]), .DPRA2(ADDRB[2]), 
  .DPRA3(ADDRB[3]), .DPRA4(ADDRB[4]), .DPRA5(ADDRB[5]), .DPO(pre_doutb_1[18]));
// Memory array element: COL: 1, ROW: 19
//  Initialization of Dual Port LUT ram now done through explicit
// parameter setting.
RAM64X1D#(.INIT(64'h0000000000000000))RAM64X1D_instance_3379(.WCLK(CLK), .WE(wea_1), 
  .A0(ADDRA[0]), .A1(ADDRA[1]), .A2(ADDRA[2]), .A3(ADDRA[3]), .A4(ADDRA[4]), .A5(ADDRA[5]), 
  .D(DINA[19]), .SPO(pre_douta_1[19]), .DPRA0(ADDRB[0]), .DPRA1(ADDRB[1]), .DPRA2(ADDRB[2]), 
  .DPRA3(ADDRB[3]), .DPRA4(ADDRB[4]), .DPRA5(ADDRB[5]), .DPO(pre_doutb_1[19]));
// Memory array element: COL: 1, ROW: 20
//  Initialization of Dual Port LUT ram now done through explicit
// parameter setting.
RAM64X1D#(.INIT(64'h0000000000000000))RAM64X1D_instance_3380(.WCLK(CLK), .WE(wea_1), 
  .A0(ADDRA[0]), .A1(ADDRA[1]), .A2(ADDRA[2]), .A3(ADDRA[3]), .A4(ADDRA[4]), .A5(ADDRA[5]), 
  .D(DINA[20]), .SPO(pre_douta_1[20]), .DPRA0(ADDRB[0]), .DPRA1(ADDRB[1]), .DPRA2(ADDRB[2]), 
  .DPRA3(ADDRB[3]), .DPRA4(ADDRB[4]), .DPRA5(ADDRB[5]), .DPO(pre_doutb_1[20]));
// Memory array element: COL: 1, ROW: 21
//  Initialization of Dual Port LUT ram now done through explicit
// parameter setting.
RAM64X1D#(.INIT(64'h0000000000000000))RAM64X1D_instance_3381(.WCLK(CLK), .WE(wea_1), 
  .A0(ADDRA[0]), .A1(ADDRA[1]), .A2(ADDRA[2]), .A3(ADDRA[3]), .A4(ADDRA[4]), .A5(ADDRA[5]), 
  .D(DINA[21]), .SPO(pre_douta_1[21]), .DPRA0(ADDRB[0]), .DPRA1(ADDRB[1]), .DPRA2(ADDRB[2]), 
  .DPRA3(ADDRB[3]), .DPRA4(ADDRB[4]), .DPRA5(ADDRB[5]), .DPO(pre_doutb_1[21]));
// Memory array element: COL: 1, ROW: 22
//  Initialization of Dual Port LUT ram now done through explicit
// parameter setting.
RAM64X1D#(.INIT(64'h0000000000000000))RAM64X1D_instance_3382(.WCLK(CLK), .WE(wea_1), 
  .A0(ADDRA[0]), .A1(ADDRA[1]), .A2(ADDRA[2]), .A3(ADDRA[3]), .A4(ADDRA[4]), .A5(ADDRA[5]), 
  .D(DINA[22]), .SPO(pre_douta_1[22]), .DPRA0(ADDRB[0]), .DPRA1(ADDRB[1]), .DPRA2(ADDRB[2]), 
  .DPRA3(ADDRB[3]), .DPRA4(ADDRB[4]), .DPRA5(ADDRB[5]), .DPO(pre_doutb_1[22]));
// Memory array element: COL: 1, ROW: 23
//  Initialization of Dual Port LUT ram now done through explicit
// parameter setting.
RAM64X1D#(.INIT(64'h0000000000000000))RAM64X1D_instance_3383(.WCLK(CLK), .WE(wea_1), 
  .A0(ADDRA[0]), .A1(ADDRA[1]), .A2(ADDRA[2]), .A3(ADDRA[3]), .A4(ADDRA[4]), .A5(ADDRA[5]), 
  .D(DINA[23]), .SPO(pre_douta_1[23]), .DPRA0(ADDRB[0]), .DPRA1(ADDRB[1]), .DPRA2(ADDRB[2]), 
  .DPRA3(ADDRB[3]), .DPRA4(ADDRB[4]), .DPRA5(ADDRB[5]), .DPO(pre_doutb_1[23]));
// Memory array element: COL: 1, ROW: 24
//  Initialization of Dual Port LUT ram now done through explicit
// parameter setting.
RAM64X1D#(.INIT(64'h0000000000000000))RAM64X1D_instance_3384(.WCLK(CLK), .WE(wea_1), 
  .A0(ADDRA[0]), .A1(ADDRA[1]), .A2(ADDRA[2]), .A3(ADDRA[3]), .A4(ADDRA[4]), .A5(ADDRA[5]), 
  .D(DINA[24]), .SPO(pre_douta_1[24]), .DPRA0(ADDRB[0]), .DPRA1(ADDRB[1]), .DPRA2(ADDRB[2]), 
  .DPRA3(ADDRB[3]), .DPRA4(ADDRB[4]), .DPRA5(ADDRB[5]), .DPO(pre_doutb_1[24]));
// Memory array element: COL: 1, ROW: 25
//  Initialization of Dual Port LUT ram now done through explicit
// parameter setting.
RAM64X1D#(.INIT(64'h0000000000000000))RAM64X1D_instance_3385(.WCLK(CLK), .WE(wea_1), 
  .A0(ADDRA[0]), .A1(ADDRA[1]), .A2(ADDRA[2]), .A3(ADDRA[3]), .A4(ADDRA[4]), .A5(ADDRA[5]), 
  .D(DINA[25]), .SPO(pre_douta_1[25]), .DPRA0(ADDRB[0]), .DPRA1(ADDRB[1]), .DPRA2(ADDRB[2]), 
  .DPRA3(ADDRB[3]), .DPRA4(ADDRB[4]), .DPRA5(ADDRB[5]), .DPO(pre_doutb_1[25]));
// Memory array element: COL: 1, ROW: 26
//  Initialization of Dual Port LUT ram now done through explicit
// parameter setting.
RAM64X1D#(.INIT(64'h0000000000000000))RAM64X1D_instance_3386(.WCLK(CLK), .WE(wea_1), 
  .A0(ADDRA[0]), .A1(ADDRA[1]), .A2(ADDRA[2]), .A3(ADDRA[3]), .A4(ADDRA[4]), .A5(ADDRA[5]), 
  .D(DINA[26]), .SPO(pre_douta_1[26]), .DPRA0(ADDRB[0]), .DPRA1(ADDRB[1]), .DPRA2(ADDRB[2]), 
  .DPRA3(ADDRB[3]), .DPRA4(ADDRB[4]), .DPRA5(ADDRB[5]), .DPO(pre_doutb_1[26]));
// Memory array element: COL: 1, ROW: 27
//  Initialization of Dual Port LUT ram now done through explicit
// parameter setting.
RAM64X1D#(.INIT(64'h0000000000000000))RAM64X1D_instance_3387(.WCLK(CLK), .WE(wea_1), 
  .A0(ADDRA[0]), .A1(ADDRA[1]), .A2(ADDRA[2]), .A3(ADDRA[3]), .A4(ADDRA[4]), .A5(ADDRA[5]), 
  .D(DINA[27]), .SPO(pre_douta_1[27]), .DPRA0(ADDRB[0]), .DPRA1(ADDRB[1]), .DPRA2(ADDRB[2]), 
  .DPRA3(ADDRB[3]), .DPRA4(ADDRB[4]), .DPRA5(ADDRB[5]), .DPO(pre_doutb_1[27]));
// Memory array element: COL: 1, ROW: 28
//  Initialization of Dual Port LUT ram now done through explicit
// parameter setting.
RAM64X1D#(.INIT(64'h0000000000000000))RAM64X1D_instance_3388(.WCLK(CLK), .WE(wea_1), 
  .A0(ADDRA[0]), .A1(ADDRA[1]), .A2(ADDRA[2]), .A3(ADDRA[3]), .A4(ADDRA[4]), .A5(ADDRA[5]), 
  .D(DINA[28]), .SPO(pre_douta_1[28]), .DPRA0(ADDRB[0]), .DPRA1(ADDRB[1]), .DPRA2(ADDRB[2]), 
  .DPRA3(ADDRB[3]), .DPRA4(ADDRB[4]), .DPRA5(ADDRB[5]), .DPO(pre_doutb_1[28]));
// Memory array element: COL: 1, ROW: 29
//  Initialization of Dual Port LUT ram now done through explicit
// parameter setting.
RAM64X1D#(.INIT(64'h0000000000000000))RAM64X1D_instance_3389(.WCLK(CLK), .WE(wea_1), 
  .A0(ADDRA[0]), .A1(ADDRA[1]), .A2(ADDRA[2]), .A3(ADDRA[3]), .A4(ADDRA[4]), .A5(ADDRA[5]), 
  .D(DINA[29]), .SPO(pre_douta_1[29]), .DPRA0(ADDRB[0]), .DPRA1(ADDRB[1]), .DPRA2(ADDRB[2]), 
  .DPRA3(ADDRB[3]), .DPRA4(ADDRB[4]), .DPRA5(ADDRB[5]), .DPO(pre_doutb_1[29]));
// Memory array element: COL: 1, ROW: 30
//  Initialization of Dual Port LUT ram now done through explicit
// parameter setting.
RAM64X1D#(.INIT(64'h0000000000000000))RAM64X1D_instance_3390(.WCLK(CLK), .WE(wea_1), 
  .A0(ADDRA[0]), .A1(ADDRA[1]), .A2(ADDRA[2]), .A3(ADDRA[3]), .A4(ADDRA[4]), .A5(ADDRA[5]), 
  .D(DINA[30]), .SPO(pre_douta_1[30]), .DPRA0(ADDRB[0]), .DPRA1(ADDRB[1]), .DPRA2(ADDRB[2]), 
  .DPRA3(ADDRB[3]), .DPRA4(ADDRB[4]), .DPRA5(ADDRB[5]), .DPO(pre_doutb_1[30]));
// Memory array element: COL: 1, ROW: 31
//  Initialization of Dual Port LUT ram now done through explicit
// parameter setting.
RAM64X1D#(.INIT(64'h0000000000000000))RAM64X1D_instance_3391(.WCLK(CLK), .WE(wea_1), 
  .A0(ADDRA[0]), .A1(ADDRA[1]), .A2(ADDRA[2]), .A3(ADDRA[3]), .A4(ADDRA[4]), .A5(ADDRA[5]), 
  .D(DINA[31]), .SPO(pre_douta_1[31]), .DPRA0(ADDRB[0]), .DPRA1(ADDRB[1]), .DPRA2(ADDRB[2]), 
  .DPRA3(ADDRB[3]), .DPRA4(ADDRB[4]), .DPRA5(ADDRB[5]), .DPO(pre_doutb_1[31]));
endmodule



module medianRow10_structuralmemory_1c422e2d_(CLK_u98, bus_4be8d0e7_, bus_666d62dd_, bus_6752f545_, bus_780770f8_, bus_5b65ca49_, bus_0cbb2674_, bus_60c77d08_, bus_442d5ea8_, bus_79cbfafa_, bus_47a7c2cc_, bus_4edf7743_, bus_71e9df0e_, bus_2c3c284a_);
input		CLK_u98;
input		bus_4be8d0e7_;
input	[31:0]	bus_666d62dd_;
input	[2:0]	bus_6752f545_;
input		bus_780770f8_;
input	[31:0]	bus_5b65ca49_;
input	[2:0]	bus_0cbb2674_;
input		bus_60c77d08_;
input		bus_442d5ea8_;
input	[31:0]	bus_79cbfafa_;
output	[31:0]	bus_47a7c2cc_;
output		bus_4edf7743_;
output	[31:0]	bus_71e9df0e_;
output		bus_2c3c284a_;
wire	[31:0]	bus_769a230e_;
wire	[31:0]	bus_40625d23_;
reg		logicalMem_6bff7a50_we_delay0_u0=1'h0;
wire		or_43ee9745_u0;
wire		or_4053a43e_u0;
wire		not_2a92548a_u0;
wire		and_691df7e4_u0;
medianRow10_forge_memory_101x32_157 medianRow10_forge_memory_101x32_157_instance0(.CLK(CLK_u98), 
  .ENA(or_43ee9745_u0), .WEA(bus_442d5ea8_), .DINA(bus_79cbfafa_), .ADDRA(bus_5b65ca49_), 
  .DOUTA(bus_40625d23_), .DONEA(), .ENB(bus_780770f8_), .ADDRB(bus_666d62dd_), .DOUTB(bus_769a230e_), 
  .DONEB());
always @(posedge CLK_u98 or posedge bus_4be8d0e7_)
begin
if (bus_4be8d0e7_)
logicalMem_6bff7a50_we_delay0_u0<=1'h0;
else logicalMem_6bff7a50_we_delay0_u0<=bus_442d5ea8_;
end
assign or_43ee9745_u0=bus_60c77d08_|bus_442d5ea8_;
assign or_4053a43e_u0=and_691df7e4_u0|logicalMem_6bff7a50_we_delay0_u0;
assign bus_47a7c2cc_=bus_769a230e_;
assign bus_4edf7743_=bus_780770f8_;
assign bus_71e9df0e_=bus_40625d23_;
assign bus_2c3c284a_=or_4053a43e_u0;
assign not_2a92548a_u0=~bus_442d5ea8_;
assign and_691df7e4_u0=bus_60c77d08_&not_2a92548a_u0;
endmodule



module medianRow10_endianswapper_0df5f01d_(endianswapper_0df5f01d_in, endianswapper_0df5f01d_out);
input	[31:0]	endianswapper_0df5f01d_in;
output	[31:0]	endianswapper_0df5f01d_out;
assign endianswapper_0df5f01d_out=endianswapper_0df5f01d_in;
endmodule



module medianRow10_endianswapper_0dc07fda_(endianswapper_0dc07fda_in, endianswapper_0dc07fda_out);
input	[31:0]	endianswapper_0dc07fda_in;
output	[31:0]	endianswapper_0dc07fda_out;
assign endianswapper_0dc07fda_out=endianswapper_0dc07fda_in;
endmodule



module medianRow10_stateVar_i(bus_732945e8_, bus_520aa855_, bus_43832041_, bus_7d736cde_, bus_5e8ce25d_, bus_2306d177_, bus_2cc05a50_);
input		bus_732945e8_;
input		bus_520aa855_;
input		bus_43832041_;
input	[31:0]	bus_7d736cde_;
input		bus_5e8ce25d_;
input	[31:0]	bus_2306d177_;
output	[31:0]	bus_2cc05a50_;
wire	[31:0]	endianswapper_0df5f01d_out;
reg	[31:0]	stateVar_i_u54=32'h0;
wire		or_58bc3415_u0;
wire	[31:0]	mux_3e61d81b_u0;
wire	[31:0]	endianswapper_0dc07fda_out;
assign bus_2cc05a50_=endianswapper_0dc07fda_out;
medianRow10_endianswapper_0df5f01d_ medianRow10_endianswapper_0df5f01d__1(.endianswapper_0df5f01d_in(mux_3e61d81b_u0), 
  .endianswapper_0df5f01d_out(endianswapper_0df5f01d_out));
always @(posedge bus_732945e8_ or posedge bus_520aa855_)
begin
if (bus_520aa855_)
stateVar_i_u54<=32'h0;
else if (or_58bc3415_u0)
stateVar_i_u54<=endianswapper_0df5f01d_out;
end
assign or_58bc3415_u0=bus_43832041_|bus_5e8ce25d_;
assign mux_3e61d81b_u0=(bus_43832041_)?bus_7d736cde_:32'h0;
medianRow10_endianswapper_0dc07fda_ medianRow10_endianswapper_0dc07fda__1(.endianswapper_0dc07fda_in(stateVar_i_u54), 
  .endianswapper_0dc07fda_out(endianswapper_0dc07fda_out));
endmodule


