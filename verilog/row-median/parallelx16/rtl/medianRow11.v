// __  ___ __ ___  _ __   ___  ___ 
// \ \/ / '__/ _ \| '_ \ / _ \/ __|
//  >  <| | | (_) | | | | (_) \__ \
// /_/\_\_|  \___/|_| |_|\___/|___/
// 
// Xronos synthesizer version
// Run date: Fri 23 Feb 2018 14:28:45 +0000
// 

module medianRow11(median_COUNT, in1_SEND, in1_DATA, median_RDY, median_SEND, RESET, in1_ACK, CLK, in1_COUNT, median_DATA, median_ACK);
output	[15:0]	median_COUNT;
wire		receive_go;
input		in1_SEND;
input	[7:0]	in1_DATA;
wire		compute_median_done;
input		median_RDY;
output		median_SEND;
input		RESET;
wire		compute_median_go;
output		in1_ACK;
input		CLK;
wire		receive_done;
input	[15:0]	in1_COUNT;
output	[7:0]	median_DATA;
input		median_ACK;
wire	[31:0]	receive_u309;
wire		receive;
wire		receive_u311;
wire		medianRow11_receive_instance_DONE;
wire		receive_u307;
wire	[2:0]	receive_u310;
wire	[31:0]	receive_u308;
wire	[31:0]	receive_u306;
wire		bus_520b7745_;
wire		bus_21c937d0_;
wire	[31:0]	bus_05263746_;
wire		bus_677fd684_;
wire		bus_39541933_;
wire		bus_6f21a65b_;
wire		bus_64bce8c6_;
wire	[2:0]	bus_5072c70b_;
wire	[31:0]	bus_092b75bf_;
wire	[31:0]	bus_5e0dbfd6_;
wire	[31:0]	bus_631e9f96_;
wire		medianRow11_scheduler_instance_DONE;
wire		scheduler;
wire		scheduler_u829;
wire		scheduler_u831;
wire		scheduler_u830;
wire		bus_3bb08e7a_;
wire	[2:0]	bus_29c0ac08_;
wire	[31:0]	bus_366874e5_;
wire		bus_7fe30663_;
wire		bus_20c3bf23_;
wire	[31:0]	bus_50c11f1f_;
wire	[31:0]	bus_0adee889_;
wire		bus_6b1ccdeb_;
wire	[2:0]	compute_median_u724;
wire	[7:0]	compute_median_u726;
wire	[31:0]	compute_median_u716;
wire		compute_median_u722;
wire	[15:0]	compute_median_u725;
wire	[2:0]	compute_median_u717;
wire	[31:0]	compute_median_u720;
wire		medianRow11_compute_median_instance_DONE;
wire		compute_median;
wire	[2:0]	compute_median_u721;
wire	[31:0]	compute_median_u714;
wire		compute_median_u727;
wire		compute_median_u715;
wire	[31:0]	compute_median_u723;
wire		compute_median_u718;
wire	[31:0]	compute_median_u719;
wire		bus_02b4a7bb_;
wire		bus_1a234e17_;
wire		bus_050e71bd_;
wire		bus_40bfd286_;
wire	[31:0]	bus_034f95dd_;
wire	[31:0]	bus_14b34018_;
assign median_COUNT=compute_median_u725;
assign receive_go=scheduler_u830;
assign compute_median_done=bus_21c937d0_;
assign median_SEND=compute_median_u727;
assign compute_median_go=scheduler_u831;
assign in1_ACK=receive_u311;
assign receive_done=bus_6b1ccdeb_;
assign median_DATA=compute_median_u726;
medianRow11_receive medianRow11_receive_instance(.CLK(CLK), .RESET(bus_1a234e17_), 
  .GO(receive_go), .port_4921ad66_(bus_05263746_), .port_4cb0b539_(bus_677fd684_), 
  .port_1459f759_(in1_DATA), .DONE(medianRow11_receive_instance_DONE), .RESULT(receive), 
  .RESULT_u2337(receive_u306), .RESULT_u2338(receive_u307), .RESULT_u2339(receive_u308), 
  .RESULT_u2340(receive_u309), .RESULT_u2341(receive_u310), .RESULT_u2342(receive_u311));
medianRow11_stateVar_fsmState_medianRow11 medianRow11_stateVar_fsmState_medianRow11_1(.bus_4b76e1bb_(CLK), 
  .bus_67a28b76_(bus_1a234e17_), .bus_415c1271_(scheduler), .bus_4e518d2e_(scheduler_u829), 
  .bus_520b7745_(bus_520b7745_));
assign bus_21c937d0_=medianRow11_compute_median_instance_DONE&{1{medianRow11_compute_median_instance_DONE}};
medianRow11_stateVar_i medianRow11_stateVar_i_1(.bus_76a4c6a9_(CLK), .bus_5fa179c3_(bus_1a234e17_), 
  .bus_066a2ab1_(receive), .bus_0210312c_(receive_u306), .bus_6950f3fc_(compute_median), 
  .bus_654456c6_(32'h0), .bus_05263746_(bus_05263746_));
medianRow11_simplememoryreferee_261462b7_ medianRow11_simplememoryreferee_261462b7__1(.bus_5fd3690f_(CLK), 
  .bus_633238ba_(bus_1a234e17_), .bus_7ea1891b_(bus_050e71bd_), .bus_25134a79_(bus_14b34018_), 
  .bus_466f495b_(receive_u307), .bus_20a66691_({24'b0, receive_u309[7:0]}), .bus_76333947_(receive_u308), 
  .bus_79ed8135_(3'h1), .bus_10f95ad2_(compute_median_u722), .bus_5315f75b_(compute_median_u718), 
  .bus_5fbaed60_(compute_median_u720), .bus_2a1bfc2e_(compute_median_u719), .bus_2cbd8ec9_(3'h1), 
  .bus_5e0dbfd6_(bus_5e0dbfd6_), .bus_631e9f96_(bus_631e9f96_), .bus_6f21a65b_(bus_6f21a65b_), 
  .bus_64bce8c6_(bus_64bce8c6_), .bus_5072c70b_(bus_5072c70b_), .bus_677fd684_(bus_677fd684_), 
  .bus_092b75bf_(bus_092b75bf_), .bus_39541933_(bus_39541933_));
medianRow11_scheduler medianRow11_scheduler_instance(.CLK(CLK), .RESET(bus_1a234e17_), 
  .GO(bus_02b4a7bb_), .port_0c9d176b_(bus_520b7745_), .port_0cac2f3e_(bus_05263746_), 
  .port_1f7f13fc_(receive_done), .port_4f8cb932_(compute_median_done), .port_4ce6add0_(in1_SEND), 
  .port_0949e719_(median_RDY), .DONE(medianRow11_scheduler_instance_DONE), .RESULT(scheduler), 
  .RESULT_u2343(scheduler_u829), .RESULT_u2344(scheduler_u830), .RESULT_u2345(scheduler_u831));
medianRow11_simplememoryreferee_5e5a9d3c_ medianRow11_simplememoryreferee_5e5a9d3c__1(.bus_1a719412_(CLK), 
  .bus_01f5e5c8_(bus_1a234e17_), .bus_607defe6_(bus_40bfd286_), .bus_21b91bf9_(bus_034f95dd_), 
  .bus_1a642b4d_(compute_median_u715), .bus_54fa6bf9_(compute_median_u716), .bus_37f3c51e_(3'h1), 
  .bus_50c11f1f_(bus_50c11f1f_), .bus_0adee889_(bus_0adee889_), .bus_3bb08e7a_(bus_3bb08e7a_), 
  .bus_7fe30663_(bus_7fe30663_), .bus_29c0ac08_(bus_29c0ac08_), .bus_366874e5_(bus_366874e5_), 
  .bus_20c3bf23_(bus_20c3bf23_));
assign bus_6b1ccdeb_=medianRow11_receive_instance_DONE&{1{medianRow11_receive_instance_DONE}};
medianRow11_compute_median medianRow11_compute_median_instance(.CLK(CLK), .RESET(bus_1a234e17_), 
  .GO(compute_median_go), .port_3c972910_(bus_20c3bf23_), .port_2edc1eec_(bus_366874e5_), 
  .port_7bb309a0_(bus_39541933_), .port_31b177c3_(bus_39541933_), .port_5ce3cc36_(bus_092b75bf_), 
  .DONE(medianRow11_compute_median_instance_DONE), .RESULT(compute_median), .RESULT_u2346(compute_median_u714), 
  .RESULT_u2347(compute_median_u715), .RESULT_u2348(compute_median_u716), .RESULT_u2349(compute_median_u717), 
  .RESULT_u2353(compute_median_u718), .RESULT_u2354(compute_median_u719), .RESULT_u2355(compute_median_u720), 
  .RESULT_u2356(compute_median_u721), .RESULT_u2350(compute_median_u722), .RESULT_u2351(compute_median_u723), 
  .RESULT_u2352(compute_median_u724), .RESULT_u2357(compute_median_u725), .RESULT_u2358(compute_median_u726), 
  .RESULT_u2359(compute_median_u727));
medianRow11_Kicker_63 medianRow11_Kicker_63_1(.CLK(CLK), .RESET(bus_1a234e17_), 
  .bus_02b4a7bb_(bus_02b4a7bb_));
medianRow11_globalreset_physical_7949483d_ medianRow11_globalreset_physical_7949483d__1(.bus_57ac5192_(CLK), 
  .bus_027dc1b9_(RESET), .bus_1a234e17_(bus_1a234e17_));
medianRow11_structuralmemory_6e11cb2c_ medianRow11_structuralmemory_6e11cb2c__1(.CLK_u97(CLK), 
  .bus_7bd0645e_(bus_1a234e17_), .bus_7e79f596_(bus_0adee889_), .bus_3fe6e2a6_(3'h1), 
  .bus_1ad88a23_(bus_7fe30663_), .bus_4a9f22ec_(bus_631e9f96_), .bus_5e672ab7_(3'h1), 
  .bus_338b88a8_(bus_64bce8c6_), .bus_1e6506cd_(bus_6f21a65b_), .bus_546fed83_(bus_5e0dbfd6_), 
  .bus_034f95dd_(bus_034f95dd_), .bus_40bfd286_(bus_40bfd286_), .bus_14b34018_(bus_14b34018_), 
  .bus_050e71bd_(bus_050e71bd_));
endmodule



module medianRow11_receive(CLK, RESET, GO, port_4921ad66_, port_4cb0b539_, port_1459f759_, RESULT, RESULT_u2337, RESULT_u2338, RESULT_u2339, RESULT_u2340, RESULT_u2341, RESULT_u2342, DONE);
input		CLK;
input		RESET;
input		GO;
input	[31:0]	port_4921ad66_;
input		port_4cb0b539_;
input	[7:0]	port_1459f759_;
output		RESULT;
output	[31:0]	RESULT_u2337;
output		RESULT_u2338;
output	[31:0]	RESULT_u2339;
output	[31:0]	RESULT_u2340;
output	[2:0]	RESULT_u2341;
output		RESULT_u2342;
output		DONE;
wire		simplePinWrite;
wire	[31:0]	add;
reg		reg_7353b501_u0=1'h0;
wire		and_u3992_u0;
wire		or_u1534_u0;
wire	[31:0]	add_u727;
reg		reg_573667d5_u0=1'h0;
assign simplePinWrite=GO&{1{GO}};
assign add=port_4921ad66_+32'h0;
always @(posedge CLK or posedge GO or posedge or_u1534_u0)
begin
if (or_u1534_u0)
reg_7353b501_u0<=1'h0;
else if (GO)
reg_7353b501_u0<=1'h1;
else reg_7353b501_u0<=reg_7353b501_u0;
end
assign and_u3992_u0=reg_7353b501_u0&port_4cb0b539_;
assign or_u1534_u0=and_u3992_u0|RESET;
assign add_u727=port_4921ad66_+32'h1;
always @(posedge CLK or posedge RESET)
begin
if (RESET)
reg_573667d5_u0<=1'h0;
else reg_573667d5_u0<=GO;
end
assign RESULT=GO;
assign RESULT_u2337=add_u727;
assign RESULT_u2338=GO;
assign RESULT_u2339=add;
assign RESULT_u2340={24'b0, port_1459f759_};
assign RESULT_u2341=3'h1;
assign RESULT_u2342=simplePinWrite;
assign DONE=reg_573667d5_u0;
endmodule



module medianRow11_endianswapper_7ee79650_(endianswapper_7ee79650_in, endianswapper_7ee79650_out);
input		endianswapper_7ee79650_in;
output		endianswapper_7ee79650_out;
assign endianswapper_7ee79650_out=endianswapper_7ee79650_in;
endmodule



module medianRow11_endianswapper_038a66bd_(endianswapper_038a66bd_in, endianswapper_038a66bd_out);
input		endianswapper_038a66bd_in;
output		endianswapper_038a66bd_out;
assign endianswapper_038a66bd_out=endianswapper_038a66bd_in;
endmodule



module medianRow11_stateVar_fsmState_medianRow11(bus_4b76e1bb_, bus_67a28b76_, bus_415c1271_, bus_4e518d2e_, bus_520b7745_);
input		bus_4b76e1bb_;
input		bus_67a28b76_;
input		bus_415c1271_;
input		bus_4e518d2e_;
output		bus_520b7745_;
reg		stateVar_fsmState_medianRow11_u2=1'h0;
wire		endianswapper_7ee79650_out;
wire		endianswapper_038a66bd_out;
always @(posedge bus_4b76e1bb_ or posedge bus_67a28b76_)
begin
if (bus_67a28b76_)
stateVar_fsmState_medianRow11_u2<=1'h0;
else if (bus_415c1271_)
stateVar_fsmState_medianRow11_u2<=endianswapper_7ee79650_out;
end
medianRow11_endianswapper_7ee79650_ medianRow11_endianswapper_7ee79650__1(.endianswapper_7ee79650_in(bus_4e518d2e_), 
  .endianswapper_7ee79650_out(endianswapper_7ee79650_out));
medianRow11_endianswapper_038a66bd_ medianRow11_endianswapper_038a66bd__1(.endianswapper_038a66bd_in(stateVar_fsmState_medianRow11_u2), 
  .endianswapper_038a66bd_out(endianswapper_038a66bd_out));
assign bus_520b7745_=endianswapper_038a66bd_out;
endmodule



module medianRow11_endianswapper_52d74fc4_(endianswapper_52d74fc4_in, endianswapper_52d74fc4_out);
input	[31:0]	endianswapper_52d74fc4_in;
output	[31:0]	endianswapper_52d74fc4_out;
assign endianswapper_52d74fc4_out=endianswapper_52d74fc4_in;
endmodule



module medianRow11_endianswapper_67fe545a_(endianswapper_67fe545a_in, endianswapper_67fe545a_out);
input	[31:0]	endianswapper_67fe545a_in;
output	[31:0]	endianswapper_67fe545a_out;
assign endianswapper_67fe545a_out=endianswapper_67fe545a_in;
endmodule



module medianRow11_stateVar_i(bus_76a4c6a9_, bus_5fa179c3_, bus_066a2ab1_, bus_0210312c_, bus_6950f3fc_, bus_654456c6_, bus_05263746_);
input		bus_76a4c6a9_;
input		bus_5fa179c3_;
input		bus_066a2ab1_;
input	[31:0]	bus_0210312c_;
input		bus_6950f3fc_;
input	[31:0]	bus_654456c6_;
output	[31:0]	bus_05263746_;
wire	[31:0]	mux_5883ae81_u0;
wire	[31:0]	endianswapper_52d74fc4_out;
wire	[31:0]	endianswapper_67fe545a_out;
reg	[31:0]	stateVar_i_u53=32'h0;
wire		or_72006d6e_u0;
assign mux_5883ae81_u0=(bus_066a2ab1_)?bus_0210312c_:32'h0;
medianRow11_endianswapper_52d74fc4_ medianRow11_endianswapper_52d74fc4__1(.endianswapper_52d74fc4_in(mux_5883ae81_u0), 
  .endianswapper_52d74fc4_out(endianswapper_52d74fc4_out));
medianRow11_endianswapper_67fe545a_ medianRow11_endianswapper_67fe545a__1(.endianswapper_67fe545a_in(stateVar_i_u53), 
  .endianswapper_67fe545a_out(endianswapper_67fe545a_out));
always @(posedge bus_76a4c6a9_ or posedge bus_5fa179c3_)
begin
if (bus_5fa179c3_)
stateVar_i_u53<=32'h0;
else if (or_72006d6e_u0)
stateVar_i_u53<=endianswapper_52d74fc4_out;
end
assign bus_05263746_=endianswapper_67fe545a_out;
assign or_72006d6e_u0=bus_066a2ab1_|bus_6950f3fc_;
endmodule



module medianRow11_simplememoryreferee_261462b7_(bus_5fd3690f_, bus_633238ba_, bus_7ea1891b_, bus_25134a79_, bus_466f495b_, bus_20a66691_, bus_76333947_, bus_79ed8135_, bus_10f95ad2_, bus_5315f75b_, bus_5fbaed60_, bus_2a1bfc2e_, bus_2cbd8ec9_, bus_5e0dbfd6_, bus_631e9f96_, bus_6f21a65b_, bus_64bce8c6_, bus_5072c70b_, bus_677fd684_, bus_092b75bf_, bus_39541933_);
input		bus_5fd3690f_;
input		bus_633238ba_;
input		bus_7ea1891b_;
input	[31:0]	bus_25134a79_;
input		bus_466f495b_;
input	[31:0]	bus_20a66691_;
input	[31:0]	bus_76333947_;
input	[2:0]	bus_79ed8135_;
input		bus_10f95ad2_;
input		bus_5315f75b_;
input	[31:0]	bus_5fbaed60_;
input	[31:0]	bus_2a1bfc2e_;
input	[2:0]	bus_2cbd8ec9_;
output	[31:0]	bus_5e0dbfd6_;
output	[31:0]	bus_631e9f96_;
output		bus_6f21a65b_;
output		bus_64bce8c6_;
output	[2:0]	bus_5072c70b_;
output		bus_677fd684_;
output	[31:0]	bus_092b75bf_;
output		bus_39541933_;
wire	[31:0]	mux_3ba42624_u0;
reg		done_qual_u230=1'h0;
wire		or_0f65d825_u0;
wire		or_746556a7_u0;
reg		done_qual_u231_u0=1'h0;
wire		and_5c39b070_u0;
wire		and_08bcbd96_u0;
wire		or_5cd9dd95_u0;
wire		not_051df0c8_u0;
wire	[31:0]	mux_4d88c51e_u0;
wire		not_6217500f_u0;
wire		or_730c86b5_u0;
wire		or_31e3961e_u0;
assign mux_3ba42624_u0=(bus_466f495b_)?bus_76333947_:bus_2a1bfc2e_;
always @(posedge bus_5fd3690f_)
begin
if (bus_633238ba_)
done_qual_u230<=1'h0;
else done_qual_u230<=bus_466f495b_;
end
assign or_0f65d825_u0=bus_10f95ad2_|bus_5315f75b_;
assign or_746556a7_u0=or_0f65d825_u0|done_qual_u231_u0;
always @(posedge bus_5fd3690f_)
begin
if (bus_633238ba_)
done_qual_u231_u0<=1'h0;
else done_qual_u231_u0<=or_0f65d825_u0;
end
assign and_5c39b070_u0=or_746556a7_u0&bus_7ea1891b_;
assign and_08bcbd96_u0=or_730c86b5_u0&bus_7ea1891b_;
assign or_5cd9dd95_u0=bus_466f495b_|or_0f65d825_u0;
assign not_051df0c8_u0=~bus_7ea1891b_;
assign mux_4d88c51e_u0=(bus_466f495b_)?{24'b0, bus_20a66691_[7:0]}:bus_5fbaed60_;
assign not_6217500f_u0=~bus_7ea1891b_;
assign or_730c86b5_u0=bus_466f495b_|done_qual_u230;
assign bus_5e0dbfd6_=mux_4d88c51e_u0;
assign bus_631e9f96_=mux_3ba42624_u0;
assign bus_6f21a65b_=or_31e3961e_u0;
assign bus_64bce8c6_=or_5cd9dd95_u0;
assign bus_5072c70b_=3'h1;
assign bus_677fd684_=and_08bcbd96_u0;
assign bus_092b75bf_=bus_25134a79_;
assign bus_39541933_=and_5c39b070_u0;
assign or_31e3961e_u0=bus_466f495b_|bus_5315f75b_;
endmodule



module medianRow11_scheduler(CLK, RESET, GO, port_0c9d176b_, port_0cac2f3e_, port_1f7f13fc_, port_4f8cb932_, port_4ce6add0_, port_0949e719_, RESULT, RESULT_u2343, RESULT_u2344, RESULT_u2345, DONE);
input		CLK;
input		RESET;
input		GO;
input		port_0c9d176b_;
input	[31:0]	port_0cac2f3e_;
input		port_1f7f13fc_;
input		port_4f8cb932_;
input		port_4ce6add0_;
input		port_0949e719_;
output		RESULT;
output		RESULT_u2343;
output		RESULT_u2344;
output		RESULT_u2345;
output		DONE;
wire signed	[31:0]	lessThan_a_signed;
wire signed	[31:0]	lessThan_b_signed;
wire		lessThan;
wire signed	[31:0]	equals_b_signed;
wire signed	[31:0]	equals_a_signed;
wire		equals;
wire signed	[31:0]	equals_u244_a_signed;
wire		equals_u244;
wire signed	[31:0]	equals_u244_b_signed;
wire		not_u927_u0;
wire		and_u3993_u0;
wire		and_u3994_u0;
wire		andOp;
wire		and_u3995_u0;
wire		not_u928_u0;
wire		and_u3996_u0;
wire		simplePinWrite;
wire		and_u3997_u0;
wire		and_u3998_u0;
wire		equals_u245;
wire signed	[31:0]	equals_u245_b_signed;
wire signed	[31:0]	equals_u245_a_signed;
wire		not_u929_u0;
wire		and_u3999_u0;
wire		and_u4000_u0;
wire		andOp_u85;
wire		not_u930_u0;
wire		and_u4001_u0;
wire		and_u4002_u0;
wire		simplePinWrite_u642;
wire		and_u4003_u0;
wire		and_u4004_u0;
wire		not_u931_u0;
wire		and_u4005_u0;
wire		not_u932_u0;
wire		and_u4006_u0;
wire		simplePinWrite_u643;
wire		or_u1535_u0;
reg		reg_73be0bdb_u0=1'h0;
wire		and_u4007_u0;
wire		and_u4008_u0;
wire		and_u4009_u0;
reg		and_delayed_u406=1'h0;
wire		and_u4010_u0;
wire		or_u1536_u0;
wire		or_u1537_u0;
wire		and_u4011_u0;
reg		reg_04dcdd2c_u0=1'h0;
wire		mux_u2027;
wire		or_u1538_u0;
wire		and_u4012_u0;
reg		reg_6d2aa3aa_u0=1'h0;
wire		and_u4013_u0;
wire		and_u4014_u0;
wire		or_u1539_u0;
wire		receive_go_merge;
wire		or_u1540_u0;
wire		mux_u2028_u0;
wire		and_u4015_u0;
wire		or_u1541_u0;
reg		syncEnable_u1526=1'h0;
reg		loopControl_u106=1'h0;
reg		reg_16fc1f3d_u0=1'h0;
wire		or_u1542_u0;
wire		mux_u2029_u0;
reg		reg_277edb17_u0=1'h0;
assign lessThan_a_signed=port_0cac2f3e_;
assign lessThan_b_signed=32'h65;
assign lessThan=lessThan_a_signed<lessThan_b_signed;
assign equals_a_signed=port_0cac2f3e_;
assign equals_b_signed=32'h65;
assign equals=equals_a_signed==equals_b_signed;
assign equals_u244_a_signed={31'b0, port_0c9d176b_};
assign equals_u244_b_signed=32'h0;
assign equals_u244=equals_u244_a_signed==equals_u244_b_signed;
assign not_u927_u0=~equals_u244;
assign and_u3993_u0=and_u4015_u0&equals_u244;
assign and_u3994_u0=and_u4015_u0&not_u927_u0;
assign andOp=lessThan&port_4ce6add0_;
assign and_u3995_u0=and_u3998_u0&not_u928_u0;
assign not_u928_u0=~andOp;
assign and_u3996_u0=and_u3998_u0&andOp;
assign simplePinWrite=and_u3997_u0&{1{and_u3997_u0}};
assign and_u3997_u0=and_u3996_u0&and_u3998_u0;
assign and_u3998_u0=and_u3993_u0&and_u4015_u0;
assign equals_u245_a_signed={31'b0, port_0c9d176b_};
assign equals_u245_b_signed=32'h1;
assign equals_u245=equals_u245_a_signed==equals_u245_b_signed;
assign not_u929_u0=~equals_u245;
assign and_u3999_u0=and_u4015_u0&equals_u245;
assign and_u4000_u0=and_u4015_u0&not_u929_u0;
assign andOp_u85=lessThan&port_4ce6add0_;
assign not_u930_u0=~andOp_u85;
assign and_u4001_u0=and_u4013_u0&not_u930_u0;
assign and_u4002_u0=and_u4013_u0&andOp_u85;
assign simplePinWrite_u642=and_u4012_u0&{1{and_u4012_u0}};
assign and_u4003_u0=and_u4011_u0&equals;
assign and_u4004_u0=and_u4011_u0&not_u931_u0;
assign not_u931_u0=~equals;
assign and_u4005_u0=and_u4010_u0&port_0949e719_;
assign not_u932_u0=~port_0949e719_;
assign and_u4006_u0=and_u4010_u0&not_u932_u0;
assign simplePinWrite_u643=and_u4007_u0&{1{and_u4007_u0}};
assign or_u1535_u0=port_4f8cb932_|reg_73be0bdb_u0;
always @(posedge CLK or posedge RESET)
begin
if (RESET)
reg_73be0bdb_u0<=1'h0;
else reg_73be0bdb_u0<=and_u4008_u0;
end
assign and_u4007_u0=and_u4005_u0&and_u4010_u0;
assign and_u4008_u0=and_u4006_u0&and_u4010_u0;
assign and_u4009_u0=and_u4004_u0&and_u4011_u0;
always @(posedge CLK or posedge RESET)
begin
if (RESET)
and_delayed_u406<=1'h0;
else and_delayed_u406<=and_u4009_u0;
end
assign and_u4010_u0=and_u4003_u0&and_u4011_u0;
assign or_u1536_u0=and_delayed_u406|or_u1535_u0;
assign or_u1537_u0=reg_04dcdd2c_u0|or_u1536_u0;
assign and_u4011_u0=and_u4001_u0&and_u4013_u0;
always @(posedge CLK or posedge RESET)
begin
if (RESET)
reg_04dcdd2c_u0<=1'h0;
else reg_04dcdd2c_u0<=and_u4012_u0;
end
assign mux_u2027=(and_u4012_u0)?1'h1:1'h0;
assign or_u1538_u0=and_u4012_u0|and_u4007_u0;
assign and_u4012_u0=and_u4002_u0&and_u4013_u0;
always @(posedge CLK or posedge RESET)
begin
if (RESET)
reg_6d2aa3aa_u0<=1'h0;
else reg_6d2aa3aa_u0<=and_u4014_u0;
end
assign and_u4013_u0=and_u3999_u0&and_u4015_u0;
assign and_u4014_u0=and_u4000_u0&and_u4015_u0;
assign or_u1539_u0=or_u1537_u0|reg_6d2aa3aa_u0;
assign receive_go_merge=simplePinWrite|simplePinWrite_u642;
assign or_u1540_u0=and_u3997_u0|or_u1538_u0;
assign mux_u2028_u0=(and_u3997_u0)?1'h1:mux_u2027;
assign and_u4015_u0=or_u1541_u0&or_u1541_u0;
assign or_u1541_u0=reg_16fc1f3d_u0|loopControl_u106;
always @(posedge CLK)
begin
if (reg_16fc1f3d_u0)
syncEnable_u1526<=RESET;
end
always @(posedge CLK or posedge syncEnable_u1526)
begin
if (syncEnable_u1526)
loopControl_u106<=1'h0;
else loopControl_u106<=or_u1539_u0;
end
always @(posedge CLK or posedge RESET)
begin
if (RESET)
reg_16fc1f3d_u0<=1'h0;
else reg_16fc1f3d_u0<=reg_277edb17_u0;
end
assign or_u1542_u0=GO|or_u1540_u0;
assign mux_u2029_u0=(GO)?1'h0:mux_u2028_u0;
always @(posedge CLK or posedge RESET)
begin
if (RESET)
reg_277edb17_u0<=1'h0;
else reg_277edb17_u0<=GO;
end
assign RESULT=or_u1542_u0;
assign RESULT_u2343=mux_u2029_u0;
assign RESULT_u2344=receive_go_merge;
assign RESULT_u2345=simplePinWrite_u643;
assign DONE=1'h0;
endmodule



module medianRow11_simplememoryreferee_5e5a9d3c_(bus_1a719412_, bus_01f5e5c8_, bus_607defe6_, bus_21b91bf9_, bus_1a642b4d_, bus_54fa6bf9_, bus_37f3c51e_, bus_50c11f1f_, bus_0adee889_, bus_3bb08e7a_, bus_7fe30663_, bus_29c0ac08_, bus_366874e5_, bus_20c3bf23_);
input		bus_1a719412_;
input		bus_01f5e5c8_;
input		bus_607defe6_;
input	[31:0]	bus_21b91bf9_;
input		bus_1a642b4d_;
input	[31:0]	bus_54fa6bf9_;
input	[2:0]	bus_37f3c51e_;
output	[31:0]	bus_50c11f1f_;
output	[31:0]	bus_0adee889_;
output		bus_3bb08e7a_;
output		bus_7fe30663_;
output	[2:0]	bus_29c0ac08_;
output	[31:0]	bus_366874e5_;
output		bus_20c3bf23_;
assign bus_50c11f1f_=32'h0;
assign bus_0adee889_=bus_54fa6bf9_;
assign bus_3bb08e7a_=1'h0;
assign bus_7fe30663_=bus_1a642b4d_;
assign bus_29c0ac08_=3'h1;
assign bus_366874e5_=bus_21b91bf9_;
assign bus_20c3bf23_=bus_607defe6_;
endmodule



module medianRow11_compute_median(CLK, RESET, GO, port_3c972910_, port_2edc1eec_, port_31b177c3_, port_5ce3cc36_, port_7bb309a0_, RESULT, RESULT_u2346, RESULT_u2347, RESULT_u2348, RESULT_u2349, RESULT_u2350, RESULT_u2351, RESULT_u2352, RESULT_u2353, RESULT_u2354, RESULT_u2355, RESULT_u2356, RESULT_u2357, RESULT_u2358, RESULT_u2359, DONE);
input		CLK;
input		RESET;
input		GO;
input		port_3c972910_;
input	[31:0]	port_2edc1eec_;
input		port_31b177c3_;
input	[31:0]	port_5ce3cc36_;
input		port_7bb309a0_;
output		RESULT;
output	[31:0]	RESULT_u2346;
output		RESULT_u2347;
output	[31:0]	RESULT_u2348;
output	[2:0]	RESULT_u2349;
output		RESULT_u2350;
output	[31:0]	RESULT_u2351;
output	[2:0]	RESULT_u2352;
output		RESULT_u2353;
output	[31:0]	RESULT_u2354;
output	[31:0]	RESULT_u2355;
output	[2:0]	RESULT_u2356;
output	[15:0]	RESULT_u2357;
output	[7:0]	RESULT_u2358;
output		RESULT_u2359;
output		DONE;
wire		and_u4016_u0;
wire	[15:0]	lessThan_a_unsigned;
wire		lessThan;
wire	[15:0]	lessThan_b_unsigned;
wire		andOp;
wire		not_u933_u0;
wire		and_u4017_u0;
wire		and_u4018_u0;
reg	[31:0]	syncEnable_u1527=32'h0;
wire		and_u4019_u0;
wire	[31:0]	add;
wire		and_u4020_u0;
wire	[31:0]	add_u728;
wire	[31:0]	add_u729;
wire		and_u4021_u0;
wire		greaterThan;
wire signed	[31:0]	greaterThan_b_signed;
wire signed	[31:0]	greaterThan_a_signed;
wire		not_u934_u0;
wire		and_u4022_u0;
wire		and_u4023_u0;
wire	[31:0]	add_u730;
wire		and_u4024_u0;
wire	[31:0]	add_u731;
wire	[31:0]	add_u732;
wire		and_u4025_u0;
wire	[31:0]	add_u733;
wire		and_u4026_u0;
reg		reg_21baed1c_u0=1'h0;
wire		or_u1543_u0;
wire	[31:0]	add_u734;
wire	[31:0]	add_u735;
wire		or_u1544_u0;
wire		and_u4027_u0;
reg		reg_116a5891_u0=1'h0;
reg	[31:0]	syncEnable_u1528_u0=32'h0;
reg	[31:0]	syncEnable_u1529_u0=32'h0;
reg		block_GO_delayed_u108=1'h0;
reg		block_GO_delayed_result_delayed_u21=1'h0;
wire	[31:0]	mux_u2030;
wire	[31:0]	mux_u2031_u0;
wire		or_u1545_u0;
reg	[31:0]	syncEnable_u1530_u0=32'h0;
reg	[31:0]	syncEnable_u1531_u0=32'h0;
reg	[31:0]	syncEnable_u1532_u0=32'h0;
reg	[31:0]	syncEnable_u1533_u0=32'h0;
wire		and_u4028_u0;
wire	[31:0]	mux_u2032_u0;
wire		and_u4029_u0;
reg		reg_4db6bf12_u0=1'h0;
reg	[31:0]	syncEnable_u1534_u0=32'h0;
reg		reg_288233cb_u0=1'h0;
reg	[31:0]	syncEnable_u1535_u0=32'h0;
wire	[31:0]	mux_u2033_u0;
reg		reg_4db6bf12_result_delayed_u0=1'h0;
wire		mux_u2034_u0;
reg		and_delayed_u407=1'h0;
wire		or_u1546_u0;
reg		syncEnable_u1536_u0=1'h0;
reg		and_delayed_u408_u0=1'h0;
wire	[31:0]	add_u736;
reg		syncEnable_u1537_u0=1'h0;
reg	[31:0]	syncEnable_u1538_u0=32'h0;
reg	[31:0]	syncEnable_u1539_u0=32'h0;
wire	[31:0]	mux_u2035_u0;
wire		or_u1547_u0;
reg		block_GO_delayed_u109_u0=1'h0;
reg	[31:0]	syncEnable_u1540_u0=32'h0;
reg	[31:0]	syncEnable_u1541_u0=32'h0;
reg	[31:0]	syncEnable_u1542_u0=32'h0;
wire	[31:0]	mux_u2036_u0;
wire		or_u1548_u0;
reg	[31:0]	syncEnable_u1543_u0=32'h0;
wire signed	[32:0]	lessThan_u109_b_signed;
wire signed	[32:0]	lessThan_u109_a_signed;
wire		lessThan_u109;
wire		and_u4030_u0;
wire		and_u4031_u0;
wire		not_u935_u0;
reg	[31:0]	syncEnable_u1544_u0=32'h0;
wire	[31:0]	mux_u2037_u0;
wire	[31:0]	mux_u2038_u0;
wire	[31:0]	mux_u2039_u0;
wire	[31:0]	mux_u2040_u0;
wire	[31:0]	mux_u2041_u0;
wire		or_u1549_u0;
wire		mux_u2042_u0;
wire	[15:0]	add_u737;
wire	[31:0]	latch_4fbeda9f_out;
reg	[31:0]	latch_4fbeda9f_reg=32'h0;
wire		bus_6349343a_;
wire		scoreboard_0ea3290a_resOr0;
wire		scoreboard_0ea3290a_resOr1;
wire		scoreboard_0ea3290a_and;
reg		scoreboard_0ea3290a_reg1=1'h0;
reg		scoreboard_0ea3290a_reg0=1'h0;
reg	[31:0]	latch_2aed62a1_reg=32'h0;
wire	[31:0]	latch_2aed62a1_out;
wire	[31:0]	latch_3d73777f_out;
reg	[31:0]	latch_3d73777f_reg=32'h0;
wire	[31:0]	latch_709bd236_out;
reg	[31:0]	latch_709bd236_reg=32'h0;
reg	[15:0]	syncEnable_u1545_u0=16'h0;
reg		reg_4a5a0207_u0=1'h0;
wire		latch_02004b3e_out;
reg		latch_02004b3e_reg=1'h0;
wire		and_u4032_u0;
reg	[31:0]	fbReg_tmp_row0_u51=32'h0;
reg		loopControl_u107=1'h0;
reg		fbReg_swapped_u51=1'h0;
reg		syncEnable_u1546_u0=1'h0;
wire	[31:0]	mux_u2043_u0;
wire		or_u1550_u0;
reg	[31:0]	fbReg_tmp_row1_u51=32'h0;
reg	[31:0]	fbReg_tmp_row_u51=32'h0;
wire	[31:0]	mux_u2044_u0;
reg	[15:0]	fbReg_idx_u51=16'h0;
wire		mux_u2045_u0;
wire	[31:0]	mux_u2046_u0;
reg	[31:0]	fbReg_tmp_u51=32'h0;
wire	[31:0]	mux_u2047_u0;
wire	[15:0]	mux_u2048_u0;
wire		and_u4033_u0;
wire		simplePinWrite;
wire	[15:0]	simplePinWrite_u644;
wire	[7:0]	simplePinWrite_u645;
wire		or_u1551_u0;
wire	[31:0]	mux_u2049_u0;
reg		reg_2076b695_u0=1'h0;
reg		reg_2076b695_result_delayed_u0=1'h0;
assign and_u4016_u0=and_u4018_u0&or_u1550_u0;
assign lessThan_a_unsigned=mux_u2048_u0;
assign lessThan_b_unsigned=16'h65;
assign lessThan=lessThan_a_unsigned<lessThan_b_unsigned;
assign andOp=lessThan&mux_u2045_u0;
assign not_u933_u0=~andOp;
assign and_u4017_u0=or_u1550_u0&andOp;
assign and_u4018_u0=or_u1550_u0&not_u933_u0;
always @(posedge CLK)
begin
if (or_u1549_u0)
syncEnable_u1527<=mux_u2039_u0;
end
assign and_u4019_u0=and_u4030_u0&or_u1549_u0;
assign add=mux_u2037_u0+32'h0;
assign and_u4020_u0=and_u4019_u0&port_3c972910_;
assign add_u728=mux_u2037_u0+32'h1;
assign add_u729=add_u728+32'h0;
assign and_u4021_u0=and_u4019_u0&port_7bb309a0_;
assign greaterThan_a_signed=syncEnable_u1539_u0;
assign greaterThan_b_signed=syncEnable_u1541_u0;
assign greaterThan=greaterThan_a_signed>greaterThan_b_signed;
assign not_u934_u0=~greaterThan;
assign and_u4022_u0=block_GO_delayed_u109_u0&not_u934_u0;
assign and_u4023_u0=block_GO_delayed_u109_u0&greaterThan;
assign add_u730=syncEnable_u1538_u0+32'h0;
assign and_u4024_u0=and_u4028_u0&port_3c972910_;
assign add_u731=syncEnable_u1538_u0+32'h1;
assign add_u732=add_u731+32'h0;
assign and_u4025_u0=and_u4028_u0&port_7bb309a0_;
assign add_u733=syncEnable_u1538_u0+32'h0;
assign and_u4026_u0=reg_21baed1c_u0&port_7bb309a0_;
always @(posedge CLK or posedge block_GO_delayed_u108 or posedge or_u1543_u0)
begin
if (or_u1543_u0)
reg_21baed1c_u0<=1'h0;
else if (block_GO_delayed_u108)
reg_21baed1c_u0<=1'h1;
else reg_21baed1c_u0<=reg_21baed1c_u0;
end
assign or_u1543_u0=and_u4026_u0|RESET;
assign add_u734=syncEnable_u1538_u0+32'h1;
assign add_u735=add_u734+32'h0;
assign or_u1544_u0=and_u4027_u0|RESET;
assign and_u4027_u0=reg_116a5891_u0&port_7bb309a0_;
always @(posedge CLK or posedge block_GO_delayed_result_delayed_u21 or posedge or_u1544_u0)
begin
if (or_u1544_u0)
reg_116a5891_u0<=1'h0;
else if (block_GO_delayed_result_delayed_u21)
reg_116a5891_u0<=1'h1;
else reg_116a5891_u0<=reg_116a5891_u0;
end
always @(posedge CLK)
begin
if (and_u4028_u0)
syncEnable_u1528_u0<=add_u735;
end
always @(posedge CLK)
begin
if (and_u4028_u0)
syncEnable_u1529_u0<=port_5ce3cc36_;
end
always @(posedge CLK or posedge RESET)
begin
if (RESET)
block_GO_delayed_u108<=1'h0;
else block_GO_delayed_u108<=and_u4028_u0;
end
always @(posedge CLK or posedge RESET)
begin
if (RESET)
block_GO_delayed_result_delayed_u21<=1'h0;
else block_GO_delayed_result_delayed_u21<=block_GO_delayed_u108;
end
assign mux_u2030=(block_GO_delayed_u108)?syncEnable_u1529_u0:syncEnable_u1531_u0;
assign mux_u2031_u0=({32{block_GO_delayed_u108}}&syncEnable_u1533_u0)|({32{block_GO_delayed_result_delayed_u21}}&syncEnable_u1528_u0)|({32{and_u4028_u0}}&add_u732);
assign or_u1545_u0=block_GO_delayed_u108|block_GO_delayed_result_delayed_u21;
always @(posedge CLK)
begin
if (and_u4028_u0)
syncEnable_u1530_u0<=port_5ce3cc36_;
end
always @(posedge CLK)
begin
if (and_u4028_u0)
syncEnable_u1531_u0<=port_2edc1eec_;
end
always @(posedge CLK)
begin
if (and_u4028_u0)
syncEnable_u1532_u0<=port_2edc1eec_;
end
always @(posedge CLK)
begin
if (and_u4028_u0)
syncEnable_u1533_u0<=add_u733;
end
assign and_u4028_u0=and_u4023_u0&block_GO_delayed_u109_u0;
assign mux_u2032_u0=(and_delayed_u408_u0)?syncEnable_u1534_u0:syncEnable_u1532_u0;
assign and_u4029_u0=and_u4022_u0&block_GO_delayed_u109_u0;
always @(posedge CLK or posedge RESET)
begin
if (RESET)
reg_4db6bf12_u0<=1'h0;
else reg_4db6bf12_u0<=reg_288233cb_u0;
end
always @(posedge CLK)
begin
if (block_GO_delayed_u109_u0)
syncEnable_u1534_u0<=syncEnable_u1542_u0;
end
always @(posedge CLK or posedge RESET)
begin
if (RESET)
reg_288233cb_u0<=1'h0;
else reg_288233cb_u0<=and_delayed_u407;
end
always @(posedge CLK)
begin
if (block_GO_delayed_u109_u0)
syncEnable_u1535_u0<=syncEnable_u1540_u0;
end
assign mux_u2033_u0=(and_delayed_u408_u0)?syncEnable_u1535_u0:syncEnable_u1530_u0;
always @(posedge CLK or posedge RESET)
begin
if (RESET)
reg_4db6bf12_result_delayed_u0<=1'h0;
else reg_4db6bf12_result_delayed_u0<=reg_4db6bf12_u0;
end
assign mux_u2034_u0=(and_delayed_u408_u0)?syncEnable_u1536_u0:1'h1;
always @(posedge CLK or posedge RESET)
begin
if (RESET)
and_delayed_u407<=1'h0;
else and_delayed_u407<=and_u4028_u0;
end
assign or_u1546_u0=and_delayed_u408_u0|reg_4db6bf12_result_delayed_u0;
always @(posedge CLK)
begin
if (block_GO_delayed_u109_u0)
syncEnable_u1536_u0<=syncEnable_u1537_u0;
end
always @(posedge CLK or posedge RESET)
begin
if (RESET)
and_delayed_u408_u0<=1'h0;
else and_delayed_u408_u0<=and_u4029_u0;
end
assign add_u736=mux_u2037_u0+32'h1;
always @(posedge CLK)
begin
if (and_u4019_u0)
syncEnable_u1537_u0<=mux_u2042_u0;
end
always @(posedge CLK)
begin
if (and_u4019_u0)
syncEnable_u1538_u0<=mux_u2037_u0;
end
always @(posedge CLK)
begin
if (and_u4019_u0)
syncEnable_u1539_u0<=port_2edc1eec_;
end
assign mux_u2035_u0=({32{or_u1545_u0}}&mux_u2031_u0)|({32{and_u4019_u0}}&add_u729)|({32{and_u4028_u0}}&mux_u2031_u0);
assign or_u1547_u0=and_u4019_u0|and_u4028_u0;
always @(posedge CLK or posedge RESET)
begin
if (RESET)
block_GO_delayed_u109_u0<=1'h0;
else block_GO_delayed_u109_u0<=and_u4019_u0;
end
always @(posedge CLK)
begin
if (and_u4019_u0)
syncEnable_u1540_u0<=mux_u2039_u0;
end
always @(posedge CLK)
begin
if (and_u4019_u0)
syncEnable_u1541_u0<=port_5ce3cc36_;
end
always @(posedge CLK)
begin
if (and_u4019_u0)
syncEnable_u1542_u0<=mux_u2041_u0;
end
assign mux_u2036_u0=(and_u4019_u0)?add:add_u730;
assign or_u1548_u0=and_u4019_u0|and_u4028_u0;
always @(posedge CLK)
begin
if (and_u4019_u0)
syncEnable_u1543_u0<=add_u736;
end
assign lessThan_u109_a_signed={1'b0, mux_u2037_u0};
assign lessThan_u109_b_signed=33'h64;
assign lessThan_u109=lessThan_u109_a_signed<lessThan_u109_b_signed;
assign and_u4030_u0=or_u1549_u0&lessThan_u109;
assign and_u4031_u0=or_u1549_u0&not_u935_u0;
assign not_u935_u0=~lessThan_u109;
always @(posedge CLK)
begin
if (or_u1549_u0)
syncEnable_u1544_u0<=mux_u2038_u0;
end
assign mux_u2037_u0=(and_u4032_u0)?32'h0:syncEnable_u1543_u0;
assign mux_u2038_u0=(and_u4032_u0)?mux_u2047_u0:syncEnable_u1539_u0;
assign mux_u2039_u0=(and_u4032_u0)?mux_u2044_u0:mux_u2033_u0;
assign mux_u2040_u0=(and_u4032_u0)?mux_u2046_u0:syncEnable_u1541_u0;
assign mux_u2041_u0=(and_u4032_u0)?mux_u2043_u0:mux_u2032_u0;
assign or_u1549_u0=and_u4032_u0|or_u1546_u0;
assign mux_u2042_u0=(and_u4032_u0)?1'h0:mux_u2034_u0;
assign add_u737=mux_u2048_u0+16'h1;
assign latch_4fbeda9f_out=(or_u1546_u0)?mux_u2032_u0:latch_4fbeda9f_reg;
always @(posedge CLK)
begin
if (or_u1546_u0)
latch_4fbeda9f_reg<=mux_u2032_u0;
end
assign bus_6349343a_=scoreboard_0ea3290a_and|RESET;
assign scoreboard_0ea3290a_resOr0=reg_4a5a0207_u0|scoreboard_0ea3290a_reg0;
assign scoreboard_0ea3290a_resOr1=or_u1546_u0|scoreboard_0ea3290a_reg1;
assign scoreboard_0ea3290a_and=scoreboard_0ea3290a_resOr0&scoreboard_0ea3290a_resOr1;
always @(posedge CLK)
begin
if (bus_6349343a_)
scoreboard_0ea3290a_reg1<=1'h0;
else if (or_u1546_u0)
scoreboard_0ea3290a_reg1<=1'h1;
else scoreboard_0ea3290a_reg1<=scoreboard_0ea3290a_reg1;
end
always @(posedge CLK)
begin
if (bus_6349343a_)
scoreboard_0ea3290a_reg0<=1'h0;
else if (reg_4a5a0207_u0)
scoreboard_0ea3290a_reg0<=1'h1;
else scoreboard_0ea3290a_reg0<=scoreboard_0ea3290a_reg0;
end
always @(posedge CLK)
begin
if (or_u1546_u0)
latch_2aed62a1_reg<=syncEnable_u1544_u0;
end
assign latch_2aed62a1_out=(or_u1546_u0)?syncEnable_u1544_u0:latch_2aed62a1_reg;
assign latch_3d73777f_out=(or_u1546_u0)?syncEnable_u1527:latch_3d73777f_reg;
always @(posedge CLK)
begin
if (or_u1546_u0)
latch_3d73777f_reg<=syncEnable_u1527;
end
assign latch_709bd236_out=(or_u1546_u0)?syncEnable_u1541_u0:latch_709bd236_reg;
always @(posedge CLK)
begin
if (or_u1546_u0)
latch_709bd236_reg<=syncEnable_u1541_u0;
end
always @(posedge CLK)
begin
if (and_u4032_u0)
syncEnable_u1545_u0<=add_u737;
end
always @(posedge CLK or posedge RESET)
begin
if (RESET)
reg_4a5a0207_u0<=1'h0;
else reg_4a5a0207_u0<=or_u1546_u0;
end
assign latch_02004b3e_out=(or_u1546_u0)?mux_u2034_u0:latch_02004b3e_reg;
always @(posedge CLK)
begin
if (or_u1546_u0)
latch_02004b3e_reg<=mux_u2034_u0;
end
assign and_u4032_u0=and_u4017_u0&or_u1550_u0;
always @(posedge CLK)
begin
if (scoreboard_0ea3290a_and)
fbReg_tmp_row0_u51<=latch_709bd236_out;
end
always @(posedge CLK or posedge syncEnable_u1546_u0)
begin
if (syncEnable_u1546_u0)
loopControl_u107<=1'h0;
else loopControl_u107<=scoreboard_0ea3290a_and;
end
always @(posedge CLK)
begin
if (scoreboard_0ea3290a_and)
fbReg_swapped_u51<=latch_02004b3e_out;
end
always @(posedge CLK)
begin
if (GO)
syncEnable_u1546_u0<=RESET;
end
assign mux_u2043_u0=(GO)?32'h0:fbReg_tmp_u51;
assign or_u1550_u0=GO|loopControl_u107;
always @(posedge CLK)
begin
if (scoreboard_0ea3290a_and)
fbReg_tmp_row1_u51<=latch_3d73777f_out;
end
always @(posedge CLK)
begin
if (scoreboard_0ea3290a_and)
fbReg_tmp_row_u51<=latch_2aed62a1_out;
end
assign mux_u2044_u0=(GO)?32'h0:fbReg_tmp_row1_u51;
always @(posedge CLK)
begin
if (scoreboard_0ea3290a_and)
fbReg_idx_u51<=syncEnable_u1545_u0;
end
assign mux_u2045_u0=(GO)?1'h0:fbReg_swapped_u51;
assign mux_u2046_u0=(GO)?32'h0:fbReg_tmp_row0_u51;
always @(posedge CLK)
begin
if (scoreboard_0ea3290a_and)
fbReg_tmp_u51<=latch_4fbeda9f_out;
end
assign mux_u2047_u0=(GO)?32'h0:fbReg_tmp_row_u51;
assign mux_u2048_u0=(GO)?16'h0:fbReg_idx_u51;
assign and_u4033_u0=reg_2076b695_u0&port_3c972910_;
assign simplePinWrite=reg_2076b695_u0&{1{reg_2076b695_u0}};
assign simplePinWrite_u644=16'h1&{16{1'h1}};
assign simplePinWrite_u645=port_2edc1eec_[7:0];
assign or_u1551_u0=or_u1548_u0|reg_2076b695_u0;
assign mux_u2049_u0=(or_u1548_u0)?mux_u2036_u0:32'h32;
always @(posedge CLK or posedge RESET)
begin
if (RESET)
reg_2076b695_u0<=1'h0;
else reg_2076b695_u0<=and_u4016_u0;
end
always @(posedge CLK or posedge RESET)
begin
if (RESET)
reg_2076b695_result_delayed_u0<=1'h0;
else reg_2076b695_result_delayed_u0<=reg_2076b695_u0;
end
assign RESULT=GO;
assign RESULT_u2346=32'h0;
assign RESULT_u2347=or_u1551_u0;
assign RESULT_u2348=mux_u2049_u0;
assign RESULT_u2349=3'h1;
assign RESULT_u2350=or_u1547_u0;
assign RESULT_u2351=mux_u2035_u0;
assign RESULT_u2352=3'h1;
assign RESULT_u2353=or_u1545_u0;
assign RESULT_u2354=mux_u2035_u0;
assign RESULT_u2355=mux_u2030;
assign RESULT_u2356=3'h1;
assign RESULT_u2357=simplePinWrite_u644;
assign RESULT_u2358=simplePinWrite_u645;
assign RESULT_u2359=simplePinWrite;
assign DONE=reg_2076b695_result_delayed_u0;
endmodule



module medianRow11_Kicker_63(CLK, RESET, bus_02b4a7bb_);
input		CLK;
input		RESET;
output		bus_02b4a7bb_;
wire		bus_1e8dd03a_;
wire		bus_4a4e0b91_;
reg		kicker_1=1'h0;
wire		bus_12984522_;
reg		kicker_res=1'h0;
wire		bus_3382892d_;
reg		kicker_2=1'h0;
assign bus_1e8dd03a_=kicker_1&bus_12984522_&bus_4a4e0b91_;
assign bus_4a4e0b91_=~kicker_2;
always @(posedge CLK)
begin
kicker_1<=bus_12984522_;
end
assign bus_12984522_=~RESET;
assign bus_02b4a7bb_=kicker_res;
always @(posedge CLK)
begin
kicker_res<=bus_1e8dd03a_;
end
assign bus_3382892d_=bus_12984522_&kicker_1;
always @(posedge CLK)
begin
kicker_2<=bus_3382892d_;
end
endmodule



module medianRow11_globalreset_physical_7949483d_(bus_57ac5192_, bus_027dc1b9_, bus_1a234e17_);
input		bus_57ac5192_;
input		bus_027dc1b9_;
output		bus_1a234e17_;
wire		not_57dbcc2b_u0;
reg		final_u63=1'h1;
wire		or_65973cb7_u0;
reg		sample_u63=1'h0;
wire		and_267a3d60_u0;
reg		cross_u63=1'h0;
reg		glitch_u63=1'h0;
assign not_57dbcc2b_u0=~and_267a3d60_u0;
always @(posedge bus_57ac5192_)
begin
final_u63<=not_57dbcc2b_u0;
end
assign or_65973cb7_u0=bus_027dc1b9_|final_u63;
assign bus_1a234e17_=or_65973cb7_u0;
always @(posedge bus_57ac5192_)
begin
sample_u63<=1'h1;
end
assign and_267a3d60_u0=cross_u63&glitch_u63;
always @(posedge bus_57ac5192_)
begin
cross_u63<=sample_u63;
end
always @(posedge bus_57ac5192_)
begin
glitch_u63<=cross_u63;
end
endmodule



module medianRow11_forge_memory_101x32_155(CLK, ENA, WEA, DINA, ENB, ADDRA, ADDRB, DOUTA, DOUTB, DONEA, DONEB);
input		CLK;
input		ENA;
input		WEA;
input	[31:0]	DINA;
input		ENB;
input	[31:0]	ADDRA;
input	[31:0]	ADDRB;
output	[31:0]	DOUTA;
output	[31:0]	DOUTB;
output		DONEA;
output		DONEB;
wire		wea_0;
wire	[31:0]	pre_douta_0;
wire	[31:0]	pre_doutb_0;
wire		wea_1;
wire	[31:0]	pre_douta_1;
wire	[31:0]	pre_doutb_1;
reg		wea_done;
reg		rea_done;
reg	[31:0]	mux_outa;
reg	[31:0]	mux_outb;
assign wea_0=WEA&(ADDRA[31:6]==26'h0);
assign wea_1=WEA&(ADDRA[31:6]==26'h1);
always @(posedge CLK)
begin
wea_done<=WEA;
end
assign DONEA=rea_done|wea_done;
always @(posedge CLK)
begin
end
assign DONEB=ENB;
always @(pre_douta_0 or pre_douta_1 or ADDRA)
begin
case (ADDRA[31:6])26'd0:mux_outa=pre_douta_0;
26'd1:mux_outa=pre_douta_1;
default:mux_outa=32'h0;
endcase end
assign DOUTA=mux_outa;
always @(pre_doutb_0 or pre_doutb_1 or ADDRB)
begin
case (ADDRB[31:6])26'd0:mux_outb=pre_doutb_0;
26'd1:mux_outb=pre_doutb_1;
default:mux_outb=32'h0;
endcase end
assign DOUTB=mux_outb;
// Memory array element: COL: 0, ROW: 0
//  Initialization of Dual Port LUT ram now done through explicit
// parameter setting.
RAM64X1D#(.INIT(64'h0000000000000000))RAM64X1D_instance_3264(.WCLK(CLK), .WE(wea_0), 
  .A0(ADDRA[0]), .A1(ADDRA[1]), .A2(ADDRA[2]), .A3(ADDRA[3]), .A4(ADDRA[4]), .A5(ADDRA[5]), 
  .D(DINA[0]), .SPO(pre_douta_0[0]), .DPRA0(ADDRB[0]), .DPRA1(ADDRB[1]), .DPRA2(ADDRB[2]), 
  .DPRA3(ADDRB[3]), .DPRA4(ADDRB[4]), .DPRA5(ADDRB[5]), .DPO(pre_doutb_0[0]));
// Memory array element: COL: 0, ROW: 1
//  Initialization of Dual Port LUT ram now done through explicit
// parameter setting.
RAM64X1D#(.INIT(64'h0000000000000000))RAM64X1D_instance_3265(.WCLK(CLK), .WE(wea_0), 
  .A0(ADDRA[0]), .A1(ADDRA[1]), .A2(ADDRA[2]), .A3(ADDRA[3]), .A4(ADDRA[4]), .A5(ADDRA[5]), 
  .D(DINA[1]), .SPO(pre_douta_0[1]), .DPRA0(ADDRB[0]), .DPRA1(ADDRB[1]), .DPRA2(ADDRB[2]), 
  .DPRA3(ADDRB[3]), .DPRA4(ADDRB[4]), .DPRA5(ADDRB[5]), .DPO(pre_doutb_0[1]));
// Memory array element: COL: 0, ROW: 2
//  Initialization of Dual Port LUT ram now done through explicit
// parameter setting.
RAM64X1D#(.INIT(64'h0000000000000000))RAM64X1D_instance_3266(.WCLK(CLK), .WE(wea_0), 
  .A0(ADDRA[0]), .A1(ADDRA[1]), .A2(ADDRA[2]), .A3(ADDRA[3]), .A4(ADDRA[4]), .A5(ADDRA[5]), 
  .D(DINA[2]), .SPO(pre_douta_0[2]), .DPRA0(ADDRB[0]), .DPRA1(ADDRB[1]), .DPRA2(ADDRB[2]), 
  .DPRA3(ADDRB[3]), .DPRA4(ADDRB[4]), .DPRA5(ADDRB[5]), .DPO(pre_doutb_0[2]));
// Memory array element: COL: 0, ROW: 3
//  Initialization of Dual Port LUT ram now done through explicit
// parameter setting.
RAM64X1D#(.INIT(64'h0000000000000000))RAM64X1D_instance_3267(.WCLK(CLK), .WE(wea_0), 
  .A0(ADDRA[0]), .A1(ADDRA[1]), .A2(ADDRA[2]), .A3(ADDRA[3]), .A4(ADDRA[4]), .A5(ADDRA[5]), 
  .D(DINA[3]), .SPO(pre_douta_0[3]), .DPRA0(ADDRB[0]), .DPRA1(ADDRB[1]), .DPRA2(ADDRB[2]), 
  .DPRA3(ADDRB[3]), .DPRA4(ADDRB[4]), .DPRA5(ADDRB[5]), .DPO(pre_doutb_0[3]));
// Memory array element: COL: 0, ROW: 4
//  Initialization of Dual Port LUT ram now done through explicit
// parameter setting.
RAM64X1D#(.INIT(64'h0000000000000000))RAM64X1D_instance_3268(.WCLK(CLK), .WE(wea_0), 
  .A0(ADDRA[0]), .A1(ADDRA[1]), .A2(ADDRA[2]), .A3(ADDRA[3]), .A4(ADDRA[4]), .A5(ADDRA[5]), 
  .D(DINA[4]), .SPO(pre_douta_0[4]), .DPRA0(ADDRB[0]), .DPRA1(ADDRB[1]), .DPRA2(ADDRB[2]), 
  .DPRA3(ADDRB[3]), .DPRA4(ADDRB[4]), .DPRA5(ADDRB[5]), .DPO(pre_doutb_0[4]));
// Memory array element: COL: 0, ROW: 5
//  Initialization of Dual Port LUT ram now done through explicit
// parameter setting.
RAM64X1D#(.INIT(64'h0000000000000000))RAM64X1D_instance_3269(.WCLK(CLK), .WE(wea_0), 
  .A0(ADDRA[0]), .A1(ADDRA[1]), .A2(ADDRA[2]), .A3(ADDRA[3]), .A4(ADDRA[4]), .A5(ADDRA[5]), 
  .D(DINA[5]), .SPO(pre_douta_0[5]), .DPRA0(ADDRB[0]), .DPRA1(ADDRB[1]), .DPRA2(ADDRB[2]), 
  .DPRA3(ADDRB[3]), .DPRA4(ADDRB[4]), .DPRA5(ADDRB[5]), .DPO(pre_doutb_0[5]));
// Memory array element: COL: 0, ROW: 6
//  Initialization of Dual Port LUT ram now done through explicit
// parameter setting.
RAM64X1D#(.INIT(64'h0000000000000000))RAM64X1D_instance_3270(.WCLK(CLK), .WE(wea_0), 
  .A0(ADDRA[0]), .A1(ADDRA[1]), .A2(ADDRA[2]), .A3(ADDRA[3]), .A4(ADDRA[4]), .A5(ADDRA[5]), 
  .D(DINA[6]), .SPO(pre_douta_0[6]), .DPRA0(ADDRB[0]), .DPRA1(ADDRB[1]), .DPRA2(ADDRB[2]), 
  .DPRA3(ADDRB[3]), .DPRA4(ADDRB[4]), .DPRA5(ADDRB[5]), .DPO(pre_doutb_0[6]));
// Memory array element: COL: 0, ROW: 7
//  Initialization of Dual Port LUT ram now done through explicit
// parameter setting.
RAM64X1D#(.INIT(64'h0000000000000000))RAM64X1D_instance_3271(.WCLK(CLK), .WE(wea_0), 
  .A0(ADDRA[0]), .A1(ADDRA[1]), .A2(ADDRA[2]), .A3(ADDRA[3]), .A4(ADDRA[4]), .A5(ADDRA[5]), 
  .D(DINA[7]), .SPO(pre_douta_0[7]), .DPRA0(ADDRB[0]), .DPRA1(ADDRB[1]), .DPRA2(ADDRB[2]), 
  .DPRA3(ADDRB[3]), .DPRA4(ADDRB[4]), .DPRA5(ADDRB[5]), .DPO(pre_doutb_0[7]));
// Memory array element: COL: 0, ROW: 8
//  Initialization of Dual Port LUT ram now done through explicit
// parameter setting.
RAM64X1D#(.INIT(64'h0000000000000000))RAM64X1D_instance_3272(.WCLK(CLK), .WE(wea_0), 
  .A0(ADDRA[0]), .A1(ADDRA[1]), .A2(ADDRA[2]), .A3(ADDRA[3]), .A4(ADDRA[4]), .A5(ADDRA[5]), 
  .D(DINA[8]), .SPO(pre_douta_0[8]), .DPRA0(ADDRB[0]), .DPRA1(ADDRB[1]), .DPRA2(ADDRB[2]), 
  .DPRA3(ADDRB[3]), .DPRA4(ADDRB[4]), .DPRA5(ADDRB[5]), .DPO(pre_doutb_0[8]));
// Memory array element: COL: 0, ROW: 9
//  Initialization of Dual Port LUT ram now done through explicit
// parameter setting.
RAM64X1D#(.INIT(64'h0000000000000000))RAM64X1D_instance_3273(.WCLK(CLK), .WE(wea_0), 
  .A0(ADDRA[0]), .A1(ADDRA[1]), .A2(ADDRA[2]), .A3(ADDRA[3]), .A4(ADDRA[4]), .A5(ADDRA[5]), 
  .D(DINA[9]), .SPO(pre_douta_0[9]), .DPRA0(ADDRB[0]), .DPRA1(ADDRB[1]), .DPRA2(ADDRB[2]), 
  .DPRA3(ADDRB[3]), .DPRA4(ADDRB[4]), .DPRA5(ADDRB[5]), .DPO(pre_doutb_0[9]));
// Memory array element: COL: 0, ROW: 10
//  Initialization of Dual Port LUT ram now done through explicit
// parameter setting.
RAM64X1D#(.INIT(64'h0000000000000000))RAM64X1D_instance_3274(.WCLK(CLK), .WE(wea_0), 
  .A0(ADDRA[0]), .A1(ADDRA[1]), .A2(ADDRA[2]), .A3(ADDRA[3]), .A4(ADDRA[4]), .A5(ADDRA[5]), 
  .D(DINA[10]), .SPO(pre_douta_0[10]), .DPRA0(ADDRB[0]), .DPRA1(ADDRB[1]), .DPRA2(ADDRB[2]), 
  .DPRA3(ADDRB[3]), .DPRA4(ADDRB[4]), .DPRA5(ADDRB[5]), .DPO(pre_doutb_0[10]));
// Memory array element: COL: 0, ROW: 11
//  Initialization of Dual Port LUT ram now done through explicit
// parameter setting.
RAM64X1D#(.INIT(64'h0000000000000000))RAM64X1D_instance_3275(.WCLK(CLK), .WE(wea_0), 
  .A0(ADDRA[0]), .A1(ADDRA[1]), .A2(ADDRA[2]), .A3(ADDRA[3]), .A4(ADDRA[4]), .A5(ADDRA[5]), 
  .D(DINA[11]), .SPO(pre_douta_0[11]), .DPRA0(ADDRB[0]), .DPRA1(ADDRB[1]), .DPRA2(ADDRB[2]), 
  .DPRA3(ADDRB[3]), .DPRA4(ADDRB[4]), .DPRA5(ADDRB[5]), .DPO(pre_doutb_0[11]));
// Memory array element: COL: 0, ROW: 12
//  Initialization of Dual Port LUT ram now done through explicit
// parameter setting.
RAM64X1D#(.INIT(64'h0000000000000000))RAM64X1D_instance_3276(.WCLK(CLK), .WE(wea_0), 
  .A0(ADDRA[0]), .A1(ADDRA[1]), .A2(ADDRA[2]), .A3(ADDRA[3]), .A4(ADDRA[4]), .A5(ADDRA[5]), 
  .D(DINA[12]), .SPO(pre_douta_0[12]), .DPRA0(ADDRB[0]), .DPRA1(ADDRB[1]), .DPRA2(ADDRB[2]), 
  .DPRA3(ADDRB[3]), .DPRA4(ADDRB[4]), .DPRA5(ADDRB[5]), .DPO(pre_doutb_0[12]));
// Memory array element: COL: 0, ROW: 13
//  Initialization of Dual Port LUT ram now done through explicit
// parameter setting.
RAM64X1D#(.INIT(64'h0000000000000000))RAM64X1D_instance_3277(.WCLK(CLK), .WE(wea_0), 
  .A0(ADDRA[0]), .A1(ADDRA[1]), .A2(ADDRA[2]), .A3(ADDRA[3]), .A4(ADDRA[4]), .A5(ADDRA[5]), 
  .D(DINA[13]), .SPO(pre_douta_0[13]), .DPRA0(ADDRB[0]), .DPRA1(ADDRB[1]), .DPRA2(ADDRB[2]), 
  .DPRA3(ADDRB[3]), .DPRA4(ADDRB[4]), .DPRA5(ADDRB[5]), .DPO(pre_doutb_0[13]));
// Memory array element: COL: 0, ROW: 14
//  Initialization of Dual Port LUT ram now done through explicit
// parameter setting.
RAM64X1D#(.INIT(64'h0000000000000000))RAM64X1D_instance_3278(.WCLK(CLK), .WE(wea_0), 
  .A0(ADDRA[0]), .A1(ADDRA[1]), .A2(ADDRA[2]), .A3(ADDRA[3]), .A4(ADDRA[4]), .A5(ADDRA[5]), 
  .D(DINA[14]), .SPO(pre_douta_0[14]), .DPRA0(ADDRB[0]), .DPRA1(ADDRB[1]), .DPRA2(ADDRB[2]), 
  .DPRA3(ADDRB[3]), .DPRA4(ADDRB[4]), .DPRA5(ADDRB[5]), .DPO(pre_doutb_0[14]));
// Memory array element: COL: 0, ROW: 15
//  Initialization of Dual Port LUT ram now done through explicit
// parameter setting.
RAM64X1D#(.INIT(64'h0000000000000000))RAM64X1D_instance_3279(.WCLK(CLK), .WE(wea_0), 
  .A0(ADDRA[0]), .A1(ADDRA[1]), .A2(ADDRA[2]), .A3(ADDRA[3]), .A4(ADDRA[4]), .A5(ADDRA[5]), 
  .D(DINA[15]), .SPO(pre_douta_0[15]), .DPRA0(ADDRB[0]), .DPRA1(ADDRB[1]), .DPRA2(ADDRB[2]), 
  .DPRA3(ADDRB[3]), .DPRA4(ADDRB[4]), .DPRA5(ADDRB[5]), .DPO(pre_doutb_0[15]));
// Memory array element: COL: 0, ROW: 16
//  Initialization of Dual Port LUT ram now done through explicit
// parameter setting.
RAM64X1D#(.INIT(64'h0000000000000000))RAM64X1D_instance_3280(.WCLK(CLK), .WE(wea_0), 
  .A0(ADDRA[0]), .A1(ADDRA[1]), .A2(ADDRA[2]), .A3(ADDRA[3]), .A4(ADDRA[4]), .A5(ADDRA[5]), 
  .D(DINA[16]), .SPO(pre_douta_0[16]), .DPRA0(ADDRB[0]), .DPRA1(ADDRB[1]), .DPRA2(ADDRB[2]), 
  .DPRA3(ADDRB[3]), .DPRA4(ADDRB[4]), .DPRA5(ADDRB[5]), .DPO(pre_doutb_0[16]));
// Memory array element: COL: 0, ROW: 17
//  Initialization of Dual Port LUT ram now done through explicit
// parameter setting.
RAM64X1D#(.INIT(64'h0000000000000000))RAM64X1D_instance_3281(.WCLK(CLK), .WE(wea_0), 
  .A0(ADDRA[0]), .A1(ADDRA[1]), .A2(ADDRA[2]), .A3(ADDRA[3]), .A4(ADDRA[4]), .A5(ADDRA[5]), 
  .D(DINA[17]), .SPO(pre_douta_0[17]), .DPRA0(ADDRB[0]), .DPRA1(ADDRB[1]), .DPRA2(ADDRB[2]), 
  .DPRA3(ADDRB[3]), .DPRA4(ADDRB[4]), .DPRA5(ADDRB[5]), .DPO(pre_doutb_0[17]));
// Memory array element: COL: 0, ROW: 18
//  Initialization of Dual Port LUT ram now done through explicit
// parameter setting.
RAM64X1D#(.INIT(64'h0000000000000000))RAM64X1D_instance_3282(.WCLK(CLK), .WE(wea_0), 
  .A0(ADDRA[0]), .A1(ADDRA[1]), .A2(ADDRA[2]), .A3(ADDRA[3]), .A4(ADDRA[4]), .A5(ADDRA[5]), 
  .D(DINA[18]), .SPO(pre_douta_0[18]), .DPRA0(ADDRB[0]), .DPRA1(ADDRB[1]), .DPRA2(ADDRB[2]), 
  .DPRA3(ADDRB[3]), .DPRA4(ADDRB[4]), .DPRA5(ADDRB[5]), .DPO(pre_doutb_0[18]));
// Memory array element: COL: 0, ROW: 19
//  Initialization of Dual Port LUT ram now done through explicit
// parameter setting.
RAM64X1D#(.INIT(64'h0000000000000000))RAM64X1D_instance_3283(.WCLK(CLK), .WE(wea_0), 
  .A0(ADDRA[0]), .A1(ADDRA[1]), .A2(ADDRA[2]), .A3(ADDRA[3]), .A4(ADDRA[4]), .A5(ADDRA[5]), 
  .D(DINA[19]), .SPO(pre_douta_0[19]), .DPRA0(ADDRB[0]), .DPRA1(ADDRB[1]), .DPRA2(ADDRB[2]), 
  .DPRA3(ADDRB[3]), .DPRA4(ADDRB[4]), .DPRA5(ADDRB[5]), .DPO(pre_doutb_0[19]));
// Memory array element: COL: 0, ROW: 20
//  Initialization of Dual Port LUT ram now done through explicit
// parameter setting.
RAM64X1D#(.INIT(64'h0000000000000000))RAM64X1D_instance_3284(.WCLK(CLK), .WE(wea_0), 
  .A0(ADDRA[0]), .A1(ADDRA[1]), .A2(ADDRA[2]), .A3(ADDRA[3]), .A4(ADDRA[4]), .A5(ADDRA[5]), 
  .D(DINA[20]), .SPO(pre_douta_0[20]), .DPRA0(ADDRB[0]), .DPRA1(ADDRB[1]), .DPRA2(ADDRB[2]), 
  .DPRA3(ADDRB[3]), .DPRA4(ADDRB[4]), .DPRA5(ADDRB[5]), .DPO(pre_doutb_0[20]));
// Memory array element: COL: 0, ROW: 21
//  Initialization of Dual Port LUT ram now done through explicit
// parameter setting.
RAM64X1D#(.INIT(64'h0000000000000000))RAM64X1D_instance_3285(.WCLK(CLK), .WE(wea_0), 
  .A0(ADDRA[0]), .A1(ADDRA[1]), .A2(ADDRA[2]), .A3(ADDRA[3]), .A4(ADDRA[4]), .A5(ADDRA[5]), 
  .D(DINA[21]), .SPO(pre_douta_0[21]), .DPRA0(ADDRB[0]), .DPRA1(ADDRB[1]), .DPRA2(ADDRB[2]), 
  .DPRA3(ADDRB[3]), .DPRA4(ADDRB[4]), .DPRA5(ADDRB[5]), .DPO(pre_doutb_0[21]));
// Memory array element: COL: 0, ROW: 22
//  Initialization of Dual Port LUT ram now done through explicit
// parameter setting.
RAM64X1D#(.INIT(64'h0000000000000000))RAM64X1D_instance_3286(.WCLK(CLK), .WE(wea_0), 
  .A0(ADDRA[0]), .A1(ADDRA[1]), .A2(ADDRA[2]), .A3(ADDRA[3]), .A4(ADDRA[4]), .A5(ADDRA[5]), 
  .D(DINA[22]), .SPO(pre_douta_0[22]), .DPRA0(ADDRB[0]), .DPRA1(ADDRB[1]), .DPRA2(ADDRB[2]), 
  .DPRA3(ADDRB[3]), .DPRA4(ADDRB[4]), .DPRA5(ADDRB[5]), .DPO(pre_doutb_0[22]));
// Memory array element: COL: 0, ROW: 23
//  Initialization of Dual Port LUT ram now done through explicit
// parameter setting.
RAM64X1D#(.INIT(64'h0000000000000000))RAM64X1D_instance_3287(.WCLK(CLK), .WE(wea_0), 
  .A0(ADDRA[0]), .A1(ADDRA[1]), .A2(ADDRA[2]), .A3(ADDRA[3]), .A4(ADDRA[4]), .A5(ADDRA[5]), 
  .D(DINA[23]), .SPO(pre_douta_0[23]), .DPRA0(ADDRB[0]), .DPRA1(ADDRB[1]), .DPRA2(ADDRB[2]), 
  .DPRA3(ADDRB[3]), .DPRA4(ADDRB[4]), .DPRA5(ADDRB[5]), .DPO(pre_doutb_0[23]));
// Memory array element: COL: 0, ROW: 24
//  Initialization of Dual Port LUT ram now done through explicit
// parameter setting.
RAM64X1D#(.INIT(64'h0000000000000000))RAM64X1D_instance_3288(.WCLK(CLK), .WE(wea_0), 
  .A0(ADDRA[0]), .A1(ADDRA[1]), .A2(ADDRA[2]), .A3(ADDRA[3]), .A4(ADDRA[4]), .A5(ADDRA[5]), 
  .D(DINA[24]), .SPO(pre_douta_0[24]), .DPRA0(ADDRB[0]), .DPRA1(ADDRB[1]), .DPRA2(ADDRB[2]), 
  .DPRA3(ADDRB[3]), .DPRA4(ADDRB[4]), .DPRA5(ADDRB[5]), .DPO(pre_doutb_0[24]));
// Memory array element: COL: 0, ROW: 25
//  Initialization of Dual Port LUT ram now done through explicit
// parameter setting.
RAM64X1D#(.INIT(64'h0000000000000000))RAM64X1D_instance_3289(.WCLK(CLK), .WE(wea_0), 
  .A0(ADDRA[0]), .A1(ADDRA[1]), .A2(ADDRA[2]), .A3(ADDRA[3]), .A4(ADDRA[4]), .A5(ADDRA[5]), 
  .D(DINA[25]), .SPO(pre_douta_0[25]), .DPRA0(ADDRB[0]), .DPRA1(ADDRB[1]), .DPRA2(ADDRB[2]), 
  .DPRA3(ADDRB[3]), .DPRA4(ADDRB[4]), .DPRA5(ADDRB[5]), .DPO(pre_doutb_0[25]));
// Memory array element: COL: 0, ROW: 26
//  Initialization of Dual Port LUT ram now done through explicit
// parameter setting.
RAM64X1D#(.INIT(64'h0000000000000000))RAM64X1D_instance_3290(.WCLK(CLK), .WE(wea_0), 
  .A0(ADDRA[0]), .A1(ADDRA[1]), .A2(ADDRA[2]), .A3(ADDRA[3]), .A4(ADDRA[4]), .A5(ADDRA[5]), 
  .D(DINA[26]), .SPO(pre_douta_0[26]), .DPRA0(ADDRB[0]), .DPRA1(ADDRB[1]), .DPRA2(ADDRB[2]), 
  .DPRA3(ADDRB[3]), .DPRA4(ADDRB[4]), .DPRA5(ADDRB[5]), .DPO(pre_doutb_0[26]));
// Memory array element: COL: 0, ROW: 27
//  Initialization of Dual Port LUT ram now done through explicit
// parameter setting.
RAM64X1D#(.INIT(64'h0000000000000000))RAM64X1D_instance_3291(.WCLK(CLK), .WE(wea_0), 
  .A0(ADDRA[0]), .A1(ADDRA[1]), .A2(ADDRA[2]), .A3(ADDRA[3]), .A4(ADDRA[4]), .A5(ADDRA[5]), 
  .D(DINA[27]), .SPO(pre_douta_0[27]), .DPRA0(ADDRB[0]), .DPRA1(ADDRB[1]), .DPRA2(ADDRB[2]), 
  .DPRA3(ADDRB[3]), .DPRA4(ADDRB[4]), .DPRA5(ADDRB[5]), .DPO(pre_doutb_0[27]));
// Memory array element: COL: 0, ROW: 28
//  Initialization of Dual Port LUT ram now done through explicit
// parameter setting.
RAM64X1D#(.INIT(64'h0000000000000000))RAM64X1D_instance_3292(.WCLK(CLK), .WE(wea_0), 
  .A0(ADDRA[0]), .A1(ADDRA[1]), .A2(ADDRA[2]), .A3(ADDRA[3]), .A4(ADDRA[4]), .A5(ADDRA[5]), 
  .D(DINA[28]), .SPO(pre_douta_0[28]), .DPRA0(ADDRB[0]), .DPRA1(ADDRB[1]), .DPRA2(ADDRB[2]), 
  .DPRA3(ADDRB[3]), .DPRA4(ADDRB[4]), .DPRA5(ADDRB[5]), .DPO(pre_doutb_0[28]));
// Memory array element: COL: 0, ROW: 29
//  Initialization of Dual Port LUT ram now done through explicit
// parameter setting.
RAM64X1D#(.INIT(64'h0000000000000000))RAM64X1D_instance_3293(.WCLK(CLK), .WE(wea_0), 
  .A0(ADDRA[0]), .A1(ADDRA[1]), .A2(ADDRA[2]), .A3(ADDRA[3]), .A4(ADDRA[4]), .A5(ADDRA[5]), 
  .D(DINA[29]), .SPO(pre_douta_0[29]), .DPRA0(ADDRB[0]), .DPRA1(ADDRB[1]), .DPRA2(ADDRB[2]), 
  .DPRA3(ADDRB[3]), .DPRA4(ADDRB[4]), .DPRA5(ADDRB[5]), .DPO(pre_doutb_0[29]));
// Memory array element: COL: 0, ROW: 30
//  Initialization of Dual Port LUT ram now done through explicit
// parameter setting.
RAM64X1D#(.INIT(64'h0000000000000000))RAM64X1D_instance_3294(.WCLK(CLK), .WE(wea_0), 
  .A0(ADDRA[0]), .A1(ADDRA[1]), .A2(ADDRA[2]), .A3(ADDRA[3]), .A4(ADDRA[4]), .A5(ADDRA[5]), 
  .D(DINA[30]), .SPO(pre_douta_0[30]), .DPRA0(ADDRB[0]), .DPRA1(ADDRB[1]), .DPRA2(ADDRB[2]), 
  .DPRA3(ADDRB[3]), .DPRA4(ADDRB[4]), .DPRA5(ADDRB[5]), .DPO(pre_doutb_0[30]));
// Memory array element: COL: 0, ROW: 31
//  Initialization of Dual Port LUT ram now done through explicit
// parameter setting.
RAM64X1D#(.INIT(64'h0000000000000000))RAM64X1D_instance_3295(.WCLK(CLK), .WE(wea_0), 
  .A0(ADDRA[0]), .A1(ADDRA[1]), .A2(ADDRA[2]), .A3(ADDRA[3]), .A4(ADDRA[4]), .A5(ADDRA[5]), 
  .D(DINA[31]), .SPO(pre_douta_0[31]), .DPRA0(ADDRB[0]), .DPRA1(ADDRB[1]), .DPRA2(ADDRB[2]), 
  .DPRA3(ADDRB[3]), .DPRA4(ADDRB[4]), .DPRA5(ADDRB[5]), .DPO(pre_doutb_0[31]));
// Memory array element: COL: 1, ROW: 0
//  Initialization of Dual Port LUT ram now done through explicit
// parameter setting.
RAM64X1D#(.INIT(64'h0000000000000000))RAM64X1D_instance_3296(.WCLK(CLK), .WE(wea_1), 
  .A0(ADDRA[0]), .A1(ADDRA[1]), .A2(ADDRA[2]), .A3(ADDRA[3]), .A4(ADDRA[4]), .A5(ADDRA[5]), 
  .D(DINA[0]), .SPO(pre_douta_1[0]), .DPRA0(ADDRB[0]), .DPRA1(ADDRB[1]), .DPRA2(ADDRB[2]), 
  .DPRA3(ADDRB[3]), .DPRA4(ADDRB[4]), .DPRA5(ADDRB[5]), .DPO(pre_doutb_1[0]));
// Memory array element: COL: 1, ROW: 1
//  Initialization of Dual Port LUT ram now done through explicit
// parameter setting.
RAM64X1D#(.INIT(64'h0000000000000000))RAM64X1D_instance_3297(.WCLK(CLK), .WE(wea_1), 
  .A0(ADDRA[0]), .A1(ADDRA[1]), .A2(ADDRA[2]), .A3(ADDRA[3]), .A4(ADDRA[4]), .A5(ADDRA[5]), 
  .D(DINA[1]), .SPO(pre_douta_1[1]), .DPRA0(ADDRB[0]), .DPRA1(ADDRB[1]), .DPRA2(ADDRB[2]), 
  .DPRA3(ADDRB[3]), .DPRA4(ADDRB[4]), .DPRA5(ADDRB[5]), .DPO(pre_doutb_1[1]));
// Memory array element: COL: 1, ROW: 2
//  Initialization of Dual Port LUT ram now done through explicit
// parameter setting.
RAM64X1D#(.INIT(64'h0000000000000000))RAM64X1D_instance_3298(.WCLK(CLK), .WE(wea_1), 
  .A0(ADDRA[0]), .A1(ADDRA[1]), .A2(ADDRA[2]), .A3(ADDRA[3]), .A4(ADDRA[4]), .A5(ADDRA[5]), 
  .D(DINA[2]), .SPO(pre_douta_1[2]), .DPRA0(ADDRB[0]), .DPRA1(ADDRB[1]), .DPRA2(ADDRB[2]), 
  .DPRA3(ADDRB[3]), .DPRA4(ADDRB[4]), .DPRA5(ADDRB[5]), .DPO(pre_doutb_1[2]));
// Memory array element: COL: 1, ROW: 3
//  Initialization of Dual Port LUT ram now done through explicit
// parameter setting.
RAM64X1D#(.INIT(64'h0000000000000000))RAM64X1D_instance_3299(.WCLK(CLK), .WE(wea_1), 
  .A0(ADDRA[0]), .A1(ADDRA[1]), .A2(ADDRA[2]), .A3(ADDRA[3]), .A4(ADDRA[4]), .A5(ADDRA[5]), 
  .D(DINA[3]), .SPO(pre_douta_1[3]), .DPRA0(ADDRB[0]), .DPRA1(ADDRB[1]), .DPRA2(ADDRB[2]), 
  .DPRA3(ADDRB[3]), .DPRA4(ADDRB[4]), .DPRA5(ADDRB[5]), .DPO(pre_doutb_1[3]));
// Memory array element: COL: 1, ROW: 4
//  Initialization of Dual Port LUT ram now done through explicit
// parameter setting.
RAM64X1D#(.INIT(64'h0000000000000000))RAM64X1D_instance_3300(.WCLK(CLK), .WE(wea_1), 
  .A0(ADDRA[0]), .A1(ADDRA[1]), .A2(ADDRA[2]), .A3(ADDRA[3]), .A4(ADDRA[4]), .A5(ADDRA[5]), 
  .D(DINA[4]), .SPO(pre_douta_1[4]), .DPRA0(ADDRB[0]), .DPRA1(ADDRB[1]), .DPRA2(ADDRB[2]), 
  .DPRA3(ADDRB[3]), .DPRA4(ADDRB[4]), .DPRA5(ADDRB[5]), .DPO(pre_doutb_1[4]));
// Memory array element: COL: 1, ROW: 5
//  Initialization of Dual Port LUT ram now done through explicit
// parameter setting.
RAM64X1D#(.INIT(64'h0000000000000000))RAM64X1D_instance_3301(.WCLK(CLK), .WE(wea_1), 
  .A0(ADDRA[0]), .A1(ADDRA[1]), .A2(ADDRA[2]), .A3(ADDRA[3]), .A4(ADDRA[4]), .A5(ADDRA[5]), 
  .D(DINA[5]), .SPO(pre_douta_1[5]), .DPRA0(ADDRB[0]), .DPRA1(ADDRB[1]), .DPRA2(ADDRB[2]), 
  .DPRA3(ADDRB[3]), .DPRA4(ADDRB[4]), .DPRA5(ADDRB[5]), .DPO(pre_doutb_1[5]));
// Memory array element: COL: 1, ROW: 6
//  Initialization of Dual Port LUT ram now done through explicit
// parameter setting.
RAM64X1D#(.INIT(64'h0000000000000000))RAM64X1D_instance_3302(.WCLK(CLK), .WE(wea_1), 
  .A0(ADDRA[0]), .A1(ADDRA[1]), .A2(ADDRA[2]), .A3(ADDRA[3]), .A4(ADDRA[4]), .A5(ADDRA[5]), 
  .D(DINA[6]), .SPO(pre_douta_1[6]), .DPRA0(ADDRB[0]), .DPRA1(ADDRB[1]), .DPRA2(ADDRB[2]), 
  .DPRA3(ADDRB[3]), .DPRA4(ADDRB[4]), .DPRA5(ADDRB[5]), .DPO(pre_doutb_1[6]));
// Memory array element: COL: 1, ROW: 7
//  Initialization of Dual Port LUT ram now done through explicit
// parameter setting.
RAM64X1D#(.INIT(64'h0000000000000000))RAM64X1D_instance_3303(.WCLK(CLK), .WE(wea_1), 
  .A0(ADDRA[0]), .A1(ADDRA[1]), .A2(ADDRA[2]), .A3(ADDRA[3]), .A4(ADDRA[4]), .A5(ADDRA[5]), 
  .D(DINA[7]), .SPO(pre_douta_1[7]), .DPRA0(ADDRB[0]), .DPRA1(ADDRB[1]), .DPRA2(ADDRB[2]), 
  .DPRA3(ADDRB[3]), .DPRA4(ADDRB[4]), .DPRA5(ADDRB[5]), .DPO(pre_doutb_1[7]));
// Memory array element: COL: 1, ROW: 8
//  Initialization of Dual Port LUT ram now done through explicit
// parameter setting.
RAM64X1D#(.INIT(64'h0000000000000000))RAM64X1D_instance_3304(.WCLK(CLK), .WE(wea_1), 
  .A0(ADDRA[0]), .A1(ADDRA[1]), .A2(ADDRA[2]), .A3(ADDRA[3]), .A4(ADDRA[4]), .A5(ADDRA[5]), 
  .D(DINA[8]), .SPO(pre_douta_1[8]), .DPRA0(ADDRB[0]), .DPRA1(ADDRB[1]), .DPRA2(ADDRB[2]), 
  .DPRA3(ADDRB[3]), .DPRA4(ADDRB[4]), .DPRA5(ADDRB[5]), .DPO(pre_doutb_1[8]));
// Memory array element: COL: 1, ROW: 9
//  Initialization of Dual Port LUT ram now done through explicit
// parameter setting.
RAM64X1D#(.INIT(64'h0000000000000000))RAM64X1D_instance_3305(.WCLK(CLK), .WE(wea_1), 
  .A0(ADDRA[0]), .A1(ADDRA[1]), .A2(ADDRA[2]), .A3(ADDRA[3]), .A4(ADDRA[4]), .A5(ADDRA[5]), 
  .D(DINA[9]), .SPO(pre_douta_1[9]), .DPRA0(ADDRB[0]), .DPRA1(ADDRB[1]), .DPRA2(ADDRB[2]), 
  .DPRA3(ADDRB[3]), .DPRA4(ADDRB[4]), .DPRA5(ADDRB[5]), .DPO(pre_doutb_1[9]));
// Memory array element: COL: 1, ROW: 10
//  Initialization of Dual Port LUT ram now done through explicit
// parameter setting.
RAM64X1D#(.INIT(64'h0000000000000000))RAM64X1D_instance_3306(.WCLK(CLK), .WE(wea_1), 
  .A0(ADDRA[0]), .A1(ADDRA[1]), .A2(ADDRA[2]), .A3(ADDRA[3]), .A4(ADDRA[4]), .A5(ADDRA[5]), 
  .D(DINA[10]), .SPO(pre_douta_1[10]), .DPRA0(ADDRB[0]), .DPRA1(ADDRB[1]), .DPRA2(ADDRB[2]), 
  .DPRA3(ADDRB[3]), .DPRA4(ADDRB[4]), .DPRA5(ADDRB[5]), .DPO(pre_doutb_1[10]));
// Memory array element: COL: 1, ROW: 11
//  Initialization of Dual Port LUT ram now done through explicit
// parameter setting.
RAM64X1D#(.INIT(64'h0000000000000000))RAM64X1D_instance_3307(.WCLK(CLK), .WE(wea_1), 
  .A0(ADDRA[0]), .A1(ADDRA[1]), .A2(ADDRA[2]), .A3(ADDRA[3]), .A4(ADDRA[4]), .A5(ADDRA[5]), 
  .D(DINA[11]), .SPO(pre_douta_1[11]), .DPRA0(ADDRB[0]), .DPRA1(ADDRB[1]), .DPRA2(ADDRB[2]), 
  .DPRA3(ADDRB[3]), .DPRA4(ADDRB[4]), .DPRA5(ADDRB[5]), .DPO(pre_doutb_1[11]));
// Memory array element: COL: 1, ROW: 12
//  Initialization of Dual Port LUT ram now done through explicit
// parameter setting.
RAM64X1D#(.INIT(64'h0000000000000000))RAM64X1D_instance_3308(.WCLK(CLK), .WE(wea_1), 
  .A0(ADDRA[0]), .A1(ADDRA[1]), .A2(ADDRA[2]), .A3(ADDRA[3]), .A4(ADDRA[4]), .A5(ADDRA[5]), 
  .D(DINA[12]), .SPO(pre_douta_1[12]), .DPRA0(ADDRB[0]), .DPRA1(ADDRB[1]), .DPRA2(ADDRB[2]), 
  .DPRA3(ADDRB[3]), .DPRA4(ADDRB[4]), .DPRA5(ADDRB[5]), .DPO(pre_doutb_1[12]));
// Memory array element: COL: 1, ROW: 13
//  Initialization of Dual Port LUT ram now done through explicit
// parameter setting.
RAM64X1D#(.INIT(64'h0000000000000000))RAM64X1D_instance_3309(.WCLK(CLK), .WE(wea_1), 
  .A0(ADDRA[0]), .A1(ADDRA[1]), .A2(ADDRA[2]), .A3(ADDRA[3]), .A4(ADDRA[4]), .A5(ADDRA[5]), 
  .D(DINA[13]), .SPO(pre_douta_1[13]), .DPRA0(ADDRB[0]), .DPRA1(ADDRB[1]), .DPRA2(ADDRB[2]), 
  .DPRA3(ADDRB[3]), .DPRA4(ADDRB[4]), .DPRA5(ADDRB[5]), .DPO(pre_doutb_1[13]));
// Memory array element: COL: 1, ROW: 14
//  Initialization of Dual Port LUT ram now done through explicit
// parameter setting.
RAM64X1D#(.INIT(64'h0000000000000000))RAM64X1D_instance_3310(.WCLK(CLK), .WE(wea_1), 
  .A0(ADDRA[0]), .A1(ADDRA[1]), .A2(ADDRA[2]), .A3(ADDRA[3]), .A4(ADDRA[4]), .A5(ADDRA[5]), 
  .D(DINA[14]), .SPO(pre_douta_1[14]), .DPRA0(ADDRB[0]), .DPRA1(ADDRB[1]), .DPRA2(ADDRB[2]), 
  .DPRA3(ADDRB[3]), .DPRA4(ADDRB[4]), .DPRA5(ADDRB[5]), .DPO(pre_doutb_1[14]));
// Memory array element: COL: 1, ROW: 15
//  Initialization of Dual Port LUT ram now done through explicit
// parameter setting.
RAM64X1D#(.INIT(64'h0000000000000000))RAM64X1D_instance_3311(.WCLK(CLK), .WE(wea_1), 
  .A0(ADDRA[0]), .A1(ADDRA[1]), .A2(ADDRA[2]), .A3(ADDRA[3]), .A4(ADDRA[4]), .A5(ADDRA[5]), 
  .D(DINA[15]), .SPO(pre_douta_1[15]), .DPRA0(ADDRB[0]), .DPRA1(ADDRB[1]), .DPRA2(ADDRB[2]), 
  .DPRA3(ADDRB[3]), .DPRA4(ADDRB[4]), .DPRA5(ADDRB[5]), .DPO(pre_doutb_1[15]));
// Memory array element: COL: 1, ROW: 16
//  Initialization of Dual Port LUT ram now done through explicit
// parameter setting.
RAM64X1D#(.INIT(64'h0000000000000000))RAM64X1D_instance_3312(.WCLK(CLK), .WE(wea_1), 
  .A0(ADDRA[0]), .A1(ADDRA[1]), .A2(ADDRA[2]), .A3(ADDRA[3]), .A4(ADDRA[4]), .A5(ADDRA[5]), 
  .D(DINA[16]), .SPO(pre_douta_1[16]), .DPRA0(ADDRB[0]), .DPRA1(ADDRB[1]), .DPRA2(ADDRB[2]), 
  .DPRA3(ADDRB[3]), .DPRA4(ADDRB[4]), .DPRA5(ADDRB[5]), .DPO(pre_doutb_1[16]));
// Memory array element: COL: 1, ROW: 17
//  Initialization of Dual Port LUT ram now done through explicit
// parameter setting.
RAM64X1D#(.INIT(64'h0000000000000000))RAM64X1D_instance_3313(.WCLK(CLK), .WE(wea_1), 
  .A0(ADDRA[0]), .A1(ADDRA[1]), .A2(ADDRA[2]), .A3(ADDRA[3]), .A4(ADDRA[4]), .A5(ADDRA[5]), 
  .D(DINA[17]), .SPO(pre_douta_1[17]), .DPRA0(ADDRB[0]), .DPRA1(ADDRB[1]), .DPRA2(ADDRB[2]), 
  .DPRA3(ADDRB[3]), .DPRA4(ADDRB[4]), .DPRA5(ADDRB[5]), .DPO(pre_doutb_1[17]));
// Memory array element: COL: 1, ROW: 18
//  Initialization of Dual Port LUT ram now done through explicit
// parameter setting.
RAM64X1D#(.INIT(64'h0000000000000000))RAM64X1D_instance_3314(.WCLK(CLK), .WE(wea_1), 
  .A0(ADDRA[0]), .A1(ADDRA[1]), .A2(ADDRA[2]), .A3(ADDRA[3]), .A4(ADDRA[4]), .A5(ADDRA[5]), 
  .D(DINA[18]), .SPO(pre_douta_1[18]), .DPRA0(ADDRB[0]), .DPRA1(ADDRB[1]), .DPRA2(ADDRB[2]), 
  .DPRA3(ADDRB[3]), .DPRA4(ADDRB[4]), .DPRA5(ADDRB[5]), .DPO(pre_doutb_1[18]));
// Memory array element: COL: 1, ROW: 19
//  Initialization of Dual Port LUT ram now done through explicit
// parameter setting.
RAM64X1D#(.INIT(64'h0000000000000000))RAM64X1D_instance_3315(.WCLK(CLK), .WE(wea_1), 
  .A0(ADDRA[0]), .A1(ADDRA[1]), .A2(ADDRA[2]), .A3(ADDRA[3]), .A4(ADDRA[4]), .A5(ADDRA[5]), 
  .D(DINA[19]), .SPO(pre_douta_1[19]), .DPRA0(ADDRB[0]), .DPRA1(ADDRB[1]), .DPRA2(ADDRB[2]), 
  .DPRA3(ADDRB[3]), .DPRA4(ADDRB[4]), .DPRA5(ADDRB[5]), .DPO(pre_doutb_1[19]));
// Memory array element: COL: 1, ROW: 20
//  Initialization of Dual Port LUT ram now done through explicit
// parameter setting.
RAM64X1D#(.INIT(64'h0000000000000000))RAM64X1D_instance_3316(.WCLK(CLK), .WE(wea_1), 
  .A0(ADDRA[0]), .A1(ADDRA[1]), .A2(ADDRA[2]), .A3(ADDRA[3]), .A4(ADDRA[4]), .A5(ADDRA[5]), 
  .D(DINA[20]), .SPO(pre_douta_1[20]), .DPRA0(ADDRB[0]), .DPRA1(ADDRB[1]), .DPRA2(ADDRB[2]), 
  .DPRA3(ADDRB[3]), .DPRA4(ADDRB[4]), .DPRA5(ADDRB[5]), .DPO(pre_doutb_1[20]));
// Memory array element: COL: 1, ROW: 21
//  Initialization of Dual Port LUT ram now done through explicit
// parameter setting.
RAM64X1D#(.INIT(64'h0000000000000000))RAM64X1D_instance_3317(.WCLK(CLK), .WE(wea_1), 
  .A0(ADDRA[0]), .A1(ADDRA[1]), .A2(ADDRA[2]), .A3(ADDRA[3]), .A4(ADDRA[4]), .A5(ADDRA[5]), 
  .D(DINA[21]), .SPO(pre_douta_1[21]), .DPRA0(ADDRB[0]), .DPRA1(ADDRB[1]), .DPRA2(ADDRB[2]), 
  .DPRA3(ADDRB[3]), .DPRA4(ADDRB[4]), .DPRA5(ADDRB[5]), .DPO(pre_doutb_1[21]));
// Memory array element: COL: 1, ROW: 22
//  Initialization of Dual Port LUT ram now done through explicit
// parameter setting.
RAM64X1D#(.INIT(64'h0000000000000000))RAM64X1D_instance_3318(.WCLK(CLK), .WE(wea_1), 
  .A0(ADDRA[0]), .A1(ADDRA[1]), .A2(ADDRA[2]), .A3(ADDRA[3]), .A4(ADDRA[4]), .A5(ADDRA[5]), 
  .D(DINA[22]), .SPO(pre_douta_1[22]), .DPRA0(ADDRB[0]), .DPRA1(ADDRB[1]), .DPRA2(ADDRB[2]), 
  .DPRA3(ADDRB[3]), .DPRA4(ADDRB[4]), .DPRA5(ADDRB[5]), .DPO(pre_doutb_1[22]));
// Memory array element: COL: 1, ROW: 23
//  Initialization of Dual Port LUT ram now done through explicit
// parameter setting.
RAM64X1D#(.INIT(64'h0000000000000000))RAM64X1D_instance_3319(.WCLK(CLK), .WE(wea_1), 
  .A0(ADDRA[0]), .A1(ADDRA[1]), .A2(ADDRA[2]), .A3(ADDRA[3]), .A4(ADDRA[4]), .A5(ADDRA[5]), 
  .D(DINA[23]), .SPO(pre_douta_1[23]), .DPRA0(ADDRB[0]), .DPRA1(ADDRB[1]), .DPRA2(ADDRB[2]), 
  .DPRA3(ADDRB[3]), .DPRA4(ADDRB[4]), .DPRA5(ADDRB[5]), .DPO(pre_doutb_1[23]));
// Memory array element: COL: 1, ROW: 24
//  Initialization of Dual Port LUT ram now done through explicit
// parameter setting.
RAM64X1D#(.INIT(64'h0000000000000000))RAM64X1D_instance_3320(.WCLK(CLK), .WE(wea_1), 
  .A0(ADDRA[0]), .A1(ADDRA[1]), .A2(ADDRA[2]), .A3(ADDRA[3]), .A4(ADDRA[4]), .A5(ADDRA[5]), 
  .D(DINA[24]), .SPO(pre_douta_1[24]), .DPRA0(ADDRB[0]), .DPRA1(ADDRB[1]), .DPRA2(ADDRB[2]), 
  .DPRA3(ADDRB[3]), .DPRA4(ADDRB[4]), .DPRA5(ADDRB[5]), .DPO(pre_doutb_1[24]));
// Memory array element: COL: 1, ROW: 25
//  Initialization of Dual Port LUT ram now done through explicit
// parameter setting.
RAM64X1D#(.INIT(64'h0000000000000000))RAM64X1D_instance_3321(.WCLK(CLK), .WE(wea_1), 
  .A0(ADDRA[0]), .A1(ADDRA[1]), .A2(ADDRA[2]), .A3(ADDRA[3]), .A4(ADDRA[4]), .A5(ADDRA[5]), 
  .D(DINA[25]), .SPO(pre_douta_1[25]), .DPRA0(ADDRB[0]), .DPRA1(ADDRB[1]), .DPRA2(ADDRB[2]), 
  .DPRA3(ADDRB[3]), .DPRA4(ADDRB[4]), .DPRA5(ADDRB[5]), .DPO(pre_doutb_1[25]));
// Memory array element: COL: 1, ROW: 26
//  Initialization of Dual Port LUT ram now done through explicit
// parameter setting.
RAM64X1D#(.INIT(64'h0000000000000000))RAM64X1D_instance_3322(.WCLK(CLK), .WE(wea_1), 
  .A0(ADDRA[0]), .A1(ADDRA[1]), .A2(ADDRA[2]), .A3(ADDRA[3]), .A4(ADDRA[4]), .A5(ADDRA[5]), 
  .D(DINA[26]), .SPO(pre_douta_1[26]), .DPRA0(ADDRB[0]), .DPRA1(ADDRB[1]), .DPRA2(ADDRB[2]), 
  .DPRA3(ADDRB[3]), .DPRA4(ADDRB[4]), .DPRA5(ADDRB[5]), .DPO(pre_doutb_1[26]));
// Memory array element: COL: 1, ROW: 27
//  Initialization of Dual Port LUT ram now done through explicit
// parameter setting.
RAM64X1D#(.INIT(64'h0000000000000000))RAM64X1D_instance_3323(.WCLK(CLK), .WE(wea_1), 
  .A0(ADDRA[0]), .A1(ADDRA[1]), .A2(ADDRA[2]), .A3(ADDRA[3]), .A4(ADDRA[4]), .A5(ADDRA[5]), 
  .D(DINA[27]), .SPO(pre_douta_1[27]), .DPRA0(ADDRB[0]), .DPRA1(ADDRB[1]), .DPRA2(ADDRB[2]), 
  .DPRA3(ADDRB[3]), .DPRA4(ADDRB[4]), .DPRA5(ADDRB[5]), .DPO(pre_doutb_1[27]));
// Memory array element: COL: 1, ROW: 28
//  Initialization of Dual Port LUT ram now done through explicit
// parameter setting.
RAM64X1D#(.INIT(64'h0000000000000000))RAM64X1D_instance_3324(.WCLK(CLK), .WE(wea_1), 
  .A0(ADDRA[0]), .A1(ADDRA[1]), .A2(ADDRA[2]), .A3(ADDRA[3]), .A4(ADDRA[4]), .A5(ADDRA[5]), 
  .D(DINA[28]), .SPO(pre_douta_1[28]), .DPRA0(ADDRB[0]), .DPRA1(ADDRB[1]), .DPRA2(ADDRB[2]), 
  .DPRA3(ADDRB[3]), .DPRA4(ADDRB[4]), .DPRA5(ADDRB[5]), .DPO(pre_doutb_1[28]));
// Memory array element: COL: 1, ROW: 29
//  Initialization of Dual Port LUT ram now done through explicit
// parameter setting.
RAM64X1D#(.INIT(64'h0000000000000000))RAM64X1D_instance_3325(.WCLK(CLK), .WE(wea_1), 
  .A0(ADDRA[0]), .A1(ADDRA[1]), .A2(ADDRA[2]), .A3(ADDRA[3]), .A4(ADDRA[4]), .A5(ADDRA[5]), 
  .D(DINA[29]), .SPO(pre_douta_1[29]), .DPRA0(ADDRB[0]), .DPRA1(ADDRB[1]), .DPRA2(ADDRB[2]), 
  .DPRA3(ADDRB[3]), .DPRA4(ADDRB[4]), .DPRA5(ADDRB[5]), .DPO(pre_doutb_1[29]));
// Memory array element: COL: 1, ROW: 30
//  Initialization of Dual Port LUT ram now done through explicit
// parameter setting.
RAM64X1D#(.INIT(64'h0000000000000000))RAM64X1D_instance_3326(.WCLK(CLK), .WE(wea_1), 
  .A0(ADDRA[0]), .A1(ADDRA[1]), .A2(ADDRA[2]), .A3(ADDRA[3]), .A4(ADDRA[4]), .A5(ADDRA[5]), 
  .D(DINA[30]), .SPO(pre_douta_1[30]), .DPRA0(ADDRB[0]), .DPRA1(ADDRB[1]), .DPRA2(ADDRB[2]), 
  .DPRA3(ADDRB[3]), .DPRA4(ADDRB[4]), .DPRA5(ADDRB[5]), .DPO(pre_doutb_1[30]));
// Memory array element: COL: 1, ROW: 31
//  Initialization of Dual Port LUT ram now done through explicit
// parameter setting.
RAM64X1D#(.INIT(64'h0000000000000000))RAM64X1D_instance_3327(.WCLK(CLK), .WE(wea_1), 
  .A0(ADDRA[0]), .A1(ADDRA[1]), .A2(ADDRA[2]), .A3(ADDRA[3]), .A4(ADDRA[4]), .A5(ADDRA[5]), 
  .D(DINA[31]), .SPO(pre_douta_1[31]), .DPRA0(ADDRB[0]), .DPRA1(ADDRB[1]), .DPRA2(ADDRB[2]), 
  .DPRA3(ADDRB[3]), .DPRA4(ADDRB[4]), .DPRA5(ADDRB[5]), .DPO(pre_doutb_1[31]));
endmodule



module medianRow11_structuralmemory_6e11cb2c_(CLK_u97, bus_7bd0645e_, bus_7e79f596_, bus_3fe6e2a6_, bus_1ad88a23_, bus_4a9f22ec_, bus_5e672ab7_, bus_338b88a8_, bus_1e6506cd_, bus_546fed83_, bus_034f95dd_, bus_40bfd286_, bus_14b34018_, bus_050e71bd_);
input		CLK_u97;
input		bus_7bd0645e_;
input	[31:0]	bus_7e79f596_;
input	[2:0]	bus_3fe6e2a6_;
input		bus_1ad88a23_;
input	[31:0]	bus_4a9f22ec_;
input	[2:0]	bus_5e672ab7_;
input		bus_338b88a8_;
input		bus_1e6506cd_;
input	[31:0]	bus_546fed83_;
output	[31:0]	bus_034f95dd_;
output		bus_40bfd286_;
output	[31:0]	bus_14b34018_;
output		bus_050e71bd_;
reg		logicalMem_79991c1e_we_delay0_u0=1'h0;
wire	[31:0]	bus_37774309_;
wire	[31:0]	bus_2ea48110_;
wire		and_6ce0fdf6_u0;
wire		not_21662b10_u0;
wire		or_00782c4e_u0;
wire		or_50d6a20d_u0;
always @(posedge CLK_u97 or posedge bus_7bd0645e_)
begin
if (bus_7bd0645e_)
logicalMem_79991c1e_we_delay0_u0<=1'h0;
else logicalMem_79991c1e_we_delay0_u0<=bus_1e6506cd_;
end
medianRow11_forge_memory_101x32_155 medianRow11_forge_memory_101x32_155_instance0(.CLK(CLK_u97), 
  .ENA(or_00782c4e_u0), .WEA(bus_1e6506cd_), .DINA(bus_546fed83_), .ADDRA(bus_4a9f22ec_), 
  .DOUTA(bus_37774309_), .DONEA(), .ENB(bus_1ad88a23_), .ADDRB(bus_7e79f596_), .DOUTB(bus_2ea48110_), 
  .DONEB());
assign and_6ce0fdf6_u0=bus_338b88a8_&not_21662b10_u0;
assign not_21662b10_u0=~bus_1e6506cd_;
assign or_00782c4e_u0=bus_338b88a8_|bus_1e6506cd_;
assign bus_034f95dd_=bus_2ea48110_;
assign bus_40bfd286_=bus_1ad88a23_;
assign bus_14b34018_=bus_37774309_;
assign bus_050e71bd_=or_50d6a20d_u0;
assign or_50d6a20d_u0=and_6ce0fdf6_u0|logicalMem_79991c1e_we_delay0_u0;
endmodule


