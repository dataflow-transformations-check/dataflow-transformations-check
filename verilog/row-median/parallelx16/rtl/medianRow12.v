// __  ___ __ ___  _ __   ___  ___ 
// \ \/ / '__/ _ \| '_ \ / _ \/ __|
//  >  <| | | (_) | | | | (_) \__ \
// /_/\_\_|  \___/|_| |_|\___/|___/
// 
// Xronos synthesizer version
// Run date: Fri 23 Feb 2018 14:28:44 +0000
// 

module medianRow12(in1_SEND, median_DATA, RESET, CLK, in1_ACK, median_ACK, in1_COUNT, median_SEND, median_COUNT, in1_DATA, median_RDY);
input		in1_SEND;
wire		receive_done;
output	[7:0]	median_DATA;
input		RESET;
wire		compute_median_go;
input		CLK;
output		in1_ACK;
input		median_ACK;
input	[15:0]	in1_COUNT;
output		median_SEND;
output	[15:0]	median_COUNT;
wire		compute_median_done;
input	[7:0]	in1_DATA;
input		median_RDY;
wire		receive_go;
wire	[2:0]	bus_0f2a23b0_;
wire	[31:0]	bus_58581f6e_;
wire		bus_643c507e_;
wire		bus_30e5017a_;
wire	[31:0]	bus_079f70f4_;
wire		bus_7710aa2d_;
wire	[31:0]	bus_50d0a306_;
wire		bus_3b845d6f_;
wire	[31:0]	bus_4d4099b8_;
wire	[31:0]	bus_2d1d0872_;
wire		bus_4f2fd13f_;
wire	[31:0]	bus_5eca54c7_;
wire		bus_7e00a85a_;
wire		compute_median_u705;
wire		compute_median_u713;
wire	[2:0]	compute_median_u704;
wire	[2:0]	compute_median_u707;
wire	[2:0]	compute_median_u710;
wire	[7:0]	compute_median_u711;
wire		compute_median;
wire		compute_median_u701;
wire	[31:0]	compute_median_u702;
wire		compute_median_u708;
wire	[31:0]	compute_median_u706;
wire		medianRow12_compute_median_instance_DONE;
wire	[15:0]	compute_median_u712;
wire	[31:0]	compute_median_u709;
wire	[31:0]	compute_median_u700;
wire	[31:0]	compute_median_u703;
wire		bus_6bb6dea5_;
wire	[2:0]	bus_226ed96c_;
wire		bus_3f6ea69d_;
wire	[31:0]	bus_6da4356f_;
wire		bus_01a1fca9_;
wire		bus_0168bae8_;
wire	[31:0]	bus_5ac62c92_;
wire	[31:0]	bus_3ed4d8c9_;
wire		bus_20b887ae_;
wire		scheduler_u826;
wire		medianRow12_scheduler_instance_DONE;
wire		scheduler_u828;
wire		scheduler;
wire		scheduler_u827;
wire		bus_2f0aac67_;
wire		bus_29619bd6_;
wire		bus_6fa98c49_;
wire		receive_u305;
wire		receive_u301;
wire		receive;
wire		medianRow12_receive_instance_DONE;
wire	[31:0]	receive_u302;
wire	[31:0]	receive_u303;
wire	[2:0]	receive_u304;
wire	[31:0]	receive_u300;
assign receive_done=bus_6bb6dea5_;
assign median_DATA=compute_median_u711;
assign compute_median_go=scheduler_u828;
assign in1_ACK=receive_u305;
assign median_SEND=compute_median_u713;
assign median_COUNT=compute_median_u712;
assign compute_median_done=bus_29619bd6_;
assign receive_go=scheduler_u827;
medianRow12_simplememoryreferee_478b3cca_ medianRow12_simplememoryreferee_478b3cca__1(.bus_7447c2a9_(CLK), 
  .bus_255c7b03_(bus_7e00a85a_), .bus_7686751a_(bus_3b845d6f_), .bus_011d01a3_(bus_2d1d0872_), 
  .bus_59f6ffd9_(compute_median_u708), .bus_400a061d_(compute_median_u709), .bus_38d26cd5_(3'h1), 
  .bus_50d0a306_(bus_50d0a306_), .bus_58581f6e_(bus_58581f6e_), .bus_643c507e_(bus_643c507e_), 
  .bus_30e5017a_(bus_30e5017a_), .bus_0f2a23b0_(bus_0f2a23b0_), .bus_079f70f4_(bus_079f70f4_), 
  .bus_7710aa2d_(bus_7710aa2d_));
medianRow12_structuralmemory_7c35cfb6_ medianRow12_structuralmemory_7c35cfb6__1(.CLK_u96(CLK), 
  .bus_697a4aab_(bus_7e00a85a_), .bus_27583776_(bus_6da4356f_), .bus_5bf1e8c9_(3'h1), 
  .bus_4d69e99e_(bus_01a1fca9_), .bus_54670688_(bus_3f6ea69d_), .bus_183e0c67_(bus_5ac62c92_), 
  .bus_04c9d28b_(bus_58581f6e_), .bus_21a0a30e_(3'h1), .bus_334ea5e4_(bus_30e5017a_), 
  .bus_4d4099b8_(bus_4d4099b8_), .bus_4f2fd13f_(bus_4f2fd13f_), .bus_2d1d0872_(bus_2d1d0872_), 
  .bus_3b845d6f_(bus_3b845d6f_));
medianRow12_stateVar_i medianRow12_stateVar_i_1(.bus_17363ce6_(CLK), .bus_63f2e3c3_(bus_7e00a85a_), 
  .bus_5696c9fc_(receive), .bus_047907f0_(receive_u300), .bus_6dd21a1a_(compute_median), 
  .bus_59b7fd9a_(32'h0), .bus_5eca54c7_(bus_5eca54c7_));
medianRow12_globalreset_physical_0187300a_ medianRow12_globalreset_physical_0187300a__1(.bus_3fad3916_(CLK), 
  .bus_3104f221_(RESET), .bus_7e00a85a_(bus_7e00a85a_));
medianRow12_compute_median medianRow12_compute_median_instance(.CLK(CLK), .RESET(bus_7e00a85a_), 
  .GO(compute_median_go), .port_4116d66a_(bus_20b887ae_), .port_5b6460df_(bus_20b887ae_), 
  .port_65b500bd_(bus_3ed4d8c9_), .port_6972be56_(bus_7710aa2d_), .port_656fc6de_(bus_079f70f4_), 
  .DONE(medianRow12_compute_median_instance_DONE), .RESULT(compute_median), .RESULT_u2314(compute_median_u700), 
  .RESULT_u2318(compute_median_u701), .RESULT_u2319(compute_median_u702), .RESULT_u2320(compute_median_u703), 
  .RESULT_u2321(compute_median_u704), .RESULT_u2315(compute_median_u705), .RESULT_u2316(compute_median_u706), 
  .RESULT_u2317(compute_median_u707), .RESULT_u2322(compute_median_u708), .RESULT_u2323(compute_median_u709), 
  .RESULT_u2324(compute_median_u710), .RESULT_u2325(compute_median_u711), .RESULT_u2326(compute_median_u712), 
  .RESULT_u2327(compute_median_u713));
assign bus_6bb6dea5_=medianRow12_receive_instance_DONE&{1{medianRow12_receive_instance_DONE}};
medianRow12_simplememoryreferee_09dd9f7f_ medianRow12_simplememoryreferee_09dd9f7f__1(.bus_298f49cd_(CLK), 
  .bus_70add411_(bus_7e00a85a_), .bus_317ca22f_(bus_4f2fd13f_), .bus_7f38c04c_(bus_4d4099b8_), 
  .bus_6a42c33d_(receive_u301), .bus_2c84212e_({24'b0, receive_u303[7:0]}), .bus_1b7d7334_(receive_u302), 
  .bus_367bcf39_(3'h1), .bus_7a2f929e_(compute_median_u705), .bus_13bd36b7_(compute_median_u701), 
  .bus_065379e4_(compute_median_u703), .bus_6e5ca7dc_(compute_median_u702), .bus_45e81940_(3'h1), 
  .bus_5ac62c92_(bus_5ac62c92_), .bus_6da4356f_(bus_6da4356f_), .bus_3f6ea69d_(bus_3f6ea69d_), 
  .bus_01a1fca9_(bus_01a1fca9_), .bus_226ed96c_(bus_226ed96c_), .bus_0168bae8_(bus_0168bae8_), 
  .bus_3ed4d8c9_(bus_3ed4d8c9_), .bus_20b887ae_(bus_20b887ae_));
medianRow12_scheduler medianRow12_scheduler_instance(.CLK(CLK), .RESET(bus_7e00a85a_), 
  .GO(bus_6fa98c49_), .port_7b5325cc_(bus_2f0aac67_), .port_234a25c0_(bus_5eca54c7_), 
  .port_47791d8f_(compute_median_done), .port_13891123_(median_RDY), .port_694c86c1_(in1_SEND), 
  .port_14c718fd_(receive_done), .DONE(medianRow12_scheduler_instance_DONE), 
  .RESULT(scheduler), .RESULT_u2328(scheduler_u826), .RESULT_u2329(scheduler_u827), 
  .RESULT_u2330(scheduler_u828));
medianRow12_stateVar_fsmState_medianRow12 medianRow12_stateVar_fsmState_medianRow12_1(.bus_240a3b4c_(CLK), 
  .bus_352fb3be_(bus_7e00a85a_), .bus_690711b0_(scheduler), .bus_545875fe_(scheduler_u826), 
  .bus_2f0aac67_(bus_2f0aac67_));
assign bus_29619bd6_=medianRow12_compute_median_instance_DONE&{1{medianRow12_compute_median_instance_DONE}};
medianRow12_Kicker_62 medianRow12_Kicker_62_1(.CLK(CLK), .RESET(bus_7e00a85a_), 
  .bus_6fa98c49_(bus_6fa98c49_));
medianRow12_receive medianRow12_receive_instance(.CLK(CLK), .RESET(bus_7e00a85a_), 
  .GO(receive_go), .port_2a85fe33_(bus_5eca54c7_), .port_47e5f0d9_(bus_0168bae8_), 
  .port_525f2c3d_(in1_DATA), .DONE(medianRow12_receive_instance_DONE), .RESULT(receive), 
  .RESULT_u2331(receive_u300), .RESULT_u2332(receive_u301), .RESULT_u2333(receive_u302), 
  .RESULT_u2334(receive_u303), .RESULT_u2335(receive_u304), .RESULT_u2336(receive_u305));
endmodule



module medianRow12_simplememoryreferee_478b3cca_(bus_7447c2a9_, bus_255c7b03_, bus_7686751a_, bus_011d01a3_, bus_59f6ffd9_, bus_400a061d_, bus_38d26cd5_, bus_50d0a306_, bus_58581f6e_, bus_643c507e_, bus_30e5017a_, bus_0f2a23b0_, bus_079f70f4_, bus_7710aa2d_);
input		bus_7447c2a9_;
input		bus_255c7b03_;
input		bus_7686751a_;
input	[31:0]	bus_011d01a3_;
input		bus_59f6ffd9_;
input	[31:0]	bus_400a061d_;
input	[2:0]	bus_38d26cd5_;
output	[31:0]	bus_50d0a306_;
output	[31:0]	bus_58581f6e_;
output		bus_643c507e_;
output		bus_30e5017a_;
output	[2:0]	bus_0f2a23b0_;
output	[31:0]	bus_079f70f4_;
output		bus_7710aa2d_;
assign bus_50d0a306_=32'h0;
assign bus_58581f6e_=bus_400a061d_;
assign bus_643c507e_=1'h0;
assign bus_30e5017a_=bus_59f6ffd9_;
assign bus_0f2a23b0_=3'h1;
assign bus_079f70f4_=bus_011d01a3_;
assign bus_7710aa2d_=bus_7686751a_;
endmodule



module medianRow12_forge_memory_101x32_153(CLK, ENA, WEA, DINA, ENB, ADDRA, ADDRB, DOUTA, DOUTB, DONEA, DONEB);
input		CLK;
input		ENA;
input		WEA;
input	[31:0]	DINA;
input		ENB;
input	[31:0]	ADDRA;
input	[31:0]	ADDRB;
output	[31:0]	DOUTA;
output	[31:0]	DOUTB;
output		DONEA;
output		DONEB;
wire		wea_0;
wire	[31:0]	pre_douta_0;
wire	[31:0]	pre_doutb_0;
wire		wea_1;
wire	[31:0]	pre_douta_1;
wire	[31:0]	pre_doutb_1;
reg		wea_done;
reg		rea_done;
reg	[31:0]	mux_outa;
reg	[31:0]	mux_outb;
assign wea_0=WEA&(ADDRA[31:6]==26'h0);
assign wea_1=WEA&(ADDRA[31:6]==26'h1);
always @(posedge CLK)
begin
wea_done<=WEA;
end
assign DONEA=rea_done|wea_done;
always @(posedge CLK)
begin
end
assign DONEB=ENB;
always @(pre_douta_0 or pre_douta_1 or ADDRA)
begin
case (ADDRA[31:6])26'd0:mux_outa=pre_douta_0;
26'd1:mux_outa=pre_douta_1;
default:mux_outa=32'h0;
endcase end
assign DOUTA=mux_outa;
always @(pre_doutb_0 or pre_doutb_1 or ADDRB)
begin
case (ADDRB[31:6])26'd0:mux_outb=pre_doutb_0;
26'd1:mux_outb=pre_doutb_1;
default:mux_outb=32'h0;
endcase end
assign DOUTB=mux_outb;
// Memory array element: COL: 0, ROW: 0
//  Initialization of Dual Port LUT ram now done through explicit
// parameter setting.
RAM64X1D#(.INIT(64'h0000000000000000))RAM64X1D_instance_3200(.WCLK(CLK), .WE(wea_0), 
  .A0(ADDRA[0]), .A1(ADDRA[1]), .A2(ADDRA[2]), .A3(ADDRA[3]), .A4(ADDRA[4]), .A5(ADDRA[5]), 
  .D(DINA[0]), .SPO(pre_douta_0[0]), .DPRA0(ADDRB[0]), .DPRA1(ADDRB[1]), .DPRA2(ADDRB[2]), 
  .DPRA3(ADDRB[3]), .DPRA4(ADDRB[4]), .DPRA5(ADDRB[5]), .DPO(pre_doutb_0[0]));
// Memory array element: COL: 0, ROW: 1
//  Initialization of Dual Port LUT ram now done through explicit
// parameter setting.
RAM64X1D#(.INIT(64'h0000000000000000))RAM64X1D_instance_3201(.WCLK(CLK), .WE(wea_0), 
  .A0(ADDRA[0]), .A1(ADDRA[1]), .A2(ADDRA[2]), .A3(ADDRA[3]), .A4(ADDRA[4]), .A5(ADDRA[5]), 
  .D(DINA[1]), .SPO(pre_douta_0[1]), .DPRA0(ADDRB[0]), .DPRA1(ADDRB[1]), .DPRA2(ADDRB[2]), 
  .DPRA3(ADDRB[3]), .DPRA4(ADDRB[4]), .DPRA5(ADDRB[5]), .DPO(pre_doutb_0[1]));
// Memory array element: COL: 0, ROW: 2
//  Initialization of Dual Port LUT ram now done through explicit
// parameter setting.
RAM64X1D#(.INIT(64'h0000000000000000))RAM64X1D_instance_3202(.WCLK(CLK), .WE(wea_0), 
  .A0(ADDRA[0]), .A1(ADDRA[1]), .A2(ADDRA[2]), .A3(ADDRA[3]), .A4(ADDRA[4]), .A5(ADDRA[5]), 
  .D(DINA[2]), .SPO(pre_douta_0[2]), .DPRA0(ADDRB[0]), .DPRA1(ADDRB[1]), .DPRA2(ADDRB[2]), 
  .DPRA3(ADDRB[3]), .DPRA4(ADDRB[4]), .DPRA5(ADDRB[5]), .DPO(pre_doutb_0[2]));
// Memory array element: COL: 0, ROW: 3
//  Initialization of Dual Port LUT ram now done through explicit
// parameter setting.
RAM64X1D#(.INIT(64'h0000000000000000))RAM64X1D_instance_3203(.WCLK(CLK), .WE(wea_0), 
  .A0(ADDRA[0]), .A1(ADDRA[1]), .A2(ADDRA[2]), .A3(ADDRA[3]), .A4(ADDRA[4]), .A5(ADDRA[5]), 
  .D(DINA[3]), .SPO(pre_douta_0[3]), .DPRA0(ADDRB[0]), .DPRA1(ADDRB[1]), .DPRA2(ADDRB[2]), 
  .DPRA3(ADDRB[3]), .DPRA4(ADDRB[4]), .DPRA5(ADDRB[5]), .DPO(pre_doutb_0[3]));
// Memory array element: COL: 0, ROW: 4
//  Initialization of Dual Port LUT ram now done through explicit
// parameter setting.
RAM64X1D#(.INIT(64'h0000000000000000))RAM64X1D_instance_3204(.WCLK(CLK), .WE(wea_0), 
  .A0(ADDRA[0]), .A1(ADDRA[1]), .A2(ADDRA[2]), .A3(ADDRA[3]), .A4(ADDRA[4]), .A5(ADDRA[5]), 
  .D(DINA[4]), .SPO(pre_douta_0[4]), .DPRA0(ADDRB[0]), .DPRA1(ADDRB[1]), .DPRA2(ADDRB[2]), 
  .DPRA3(ADDRB[3]), .DPRA4(ADDRB[4]), .DPRA5(ADDRB[5]), .DPO(pre_doutb_0[4]));
// Memory array element: COL: 0, ROW: 5
//  Initialization of Dual Port LUT ram now done through explicit
// parameter setting.
RAM64X1D#(.INIT(64'h0000000000000000))RAM64X1D_instance_3205(.WCLK(CLK), .WE(wea_0), 
  .A0(ADDRA[0]), .A1(ADDRA[1]), .A2(ADDRA[2]), .A3(ADDRA[3]), .A4(ADDRA[4]), .A5(ADDRA[5]), 
  .D(DINA[5]), .SPO(pre_douta_0[5]), .DPRA0(ADDRB[0]), .DPRA1(ADDRB[1]), .DPRA2(ADDRB[2]), 
  .DPRA3(ADDRB[3]), .DPRA4(ADDRB[4]), .DPRA5(ADDRB[5]), .DPO(pre_doutb_0[5]));
// Memory array element: COL: 0, ROW: 6
//  Initialization of Dual Port LUT ram now done through explicit
// parameter setting.
RAM64X1D#(.INIT(64'h0000000000000000))RAM64X1D_instance_3206(.WCLK(CLK), .WE(wea_0), 
  .A0(ADDRA[0]), .A1(ADDRA[1]), .A2(ADDRA[2]), .A3(ADDRA[3]), .A4(ADDRA[4]), .A5(ADDRA[5]), 
  .D(DINA[6]), .SPO(pre_douta_0[6]), .DPRA0(ADDRB[0]), .DPRA1(ADDRB[1]), .DPRA2(ADDRB[2]), 
  .DPRA3(ADDRB[3]), .DPRA4(ADDRB[4]), .DPRA5(ADDRB[5]), .DPO(pre_doutb_0[6]));
// Memory array element: COL: 0, ROW: 7
//  Initialization of Dual Port LUT ram now done through explicit
// parameter setting.
RAM64X1D#(.INIT(64'h0000000000000000))RAM64X1D_instance_3207(.WCLK(CLK), .WE(wea_0), 
  .A0(ADDRA[0]), .A1(ADDRA[1]), .A2(ADDRA[2]), .A3(ADDRA[3]), .A4(ADDRA[4]), .A5(ADDRA[5]), 
  .D(DINA[7]), .SPO(pre_douta_0[7]), .DPRA0(ADDRB[0]), .DPRA1(ADDRB[1]), .DPRA2(ADDRB[2]), 
  .DPRA3(ADDRB[3]), .DPRA4(ADDRB[4]), .DPRA5(ADDRB[5]), .DPO(pre_doutb_0[7]));
// Memory array element: COL: 0, ROW: 8
//  Initialization of Dual Port LUT ram now done through explicit
// parameter setting.
RAM64X1D#(.INIT(64'h0000000000000000))RAM64X1D_instance_3208(.WCLK(CLK), .WE(wea_0), 
  .A0(ADDRA[0]), .A1(ADDRA[1]), .A2(ADDRA[2]), .A3(ADDRA[3]), .A4(ADDRA[4]), .A5(ADDRA[5]), 
  .D(DINA[8]), .SPO(pre_douta_0[8]), .DPRA0(ADDRB[0]), .DPRA1(ADDRB[1]), .DPRA2(ADDRB[2]), 
  .DPRA3(ADDRB[3]), .DPRA4(ADDRB[4]), .DPRA5(ADDRB[5]), .DPO(pre_doutb_0[8]));
// Memory array element: COL: 0, ROW: 9
//  Initialization of Dual Port LUT ram now done through explicit
// parameter setting.
RAM64X1D#(.INIT(64'h0000000000000000))RAM64X1D_instance_3209(.WCLK(CLK), .WE(wea_0), 
  .A0(ADDRA[0]), .A1(ADDRA[1]), .A2(ADDRA[2]), .A3(ADDRA[3]), .A4(ADDRA[4]), .A5(ADDRA[5]), 
  .D(DINA[9]), .SPO(pre_douta_0[9]), .DPRA0(ADDRB[0]), .DPRA1(ADDRB[1]), .DPRA2(ADDRB[2]), 
  .DPRA3(ADDRB[3]), .DPRA4(ADDRB[4]), .DPRA5(ADDRB[5]), .DPO(pre_doutb_0[9]));
// Memory array element: COL: 0, ROW: 10
//  Initialization of Dual Port LUT ram now done through explicit
// parameter setting.
RAM64X1D#(.INIT(64'h0000000000000000))RAM64X1D_instance_3210(.WCLK(CLK), .WE(wea_0), 
  .A0(ADDRA[0]), .A1(ADDRA[1]), .A2(ADDRA[2]), .A3(ADDRA[3]), .A4(ADDRA[4]), .A5(ADDRA[5]), 
  .D(DINA[10]), .SPO(pre_douta_0[10]), .DPRA0(ADDRB[0]), .DPRA1(ADDRB[1]), .DPRA2(ADDRB[2]), 
  .DPRA3(ADDRB[3]), .DPRA4(ADDRB[4]), .DPRA5(ADDRB[5]), .DPO(pre_doutb_0[10]));
// Memory array element: COL: 0, ROW: 11
//  Initialization of Dual Port LUT ram now done through explicit
// parameter setting.
RAM64X1D#(.INIT(64'h0000000000000000))RAM64X1D_instance_3211(.WCLK(CLK), .WE(wea_0), 
  .A0(ADDRA[0]), .A1(ADDRA[1]), .A2(ADDRA[2]), .A3(ADDRA[3]), .A4(ADDRA[4]), .A5(ADDRA[5]), 
  .D(DINA[11]), .SPO(pre_douta_0[11]), .DPRA0(ADDRB[0]), .DPRA1(ADDRB[1]), .DPRA2(ADDRB[2]), 
  .DPRA3(ADDRB[3]), .DPRA4(ADDRB[4]), .DPRA5(ADDRB[5]), .DPO(pre_doutb_0[11]));
// Memory array element: COL: 0, ROW: 12
//  Initialization of Dual Port LUT ram now done through explicit
// parameter setting.
RAM64X1D#(.INIT(64'h0000000000000000))RAM64X1D_instance_3212(.WCLK(CLK), .WE(wea_0), 
  .A0(ADDRA[0]), .A1(ADDRA[1]), .A2(ADDRA[2]), .A3(ADDRA[3]), .A4(ADDRA[4]), .A5(ADDRA[5]), 
  .D(DINA[12]), .SPO(pre_douta_0[12]), .DPRA0(ADDRB[0]), .DPRA1(ADDRB[1]), .DPRA2(ADDRB[2]), 
  .DPRA3(ADDRB[3]), .DPRA4(ADDRB[4]), .DPRA5(ADDRB[5]), .DPO(pre_doutb_0[12]));
// Memory array element: COL: 0, ROW: 13
//  Initialization of Dual Port LUT ram now done through explicit
// parameter setting.
RAM64X1D#(.INIT(64'h0000000000000000))RAM64X1D_instance_3213(.WCLK(CLK), .WE(wea_0), 
  .A0(ADDRA[0]), .A1(ADDRA[1]), .A2(ADDRA[2]), .A3(ADDRA[3]), .A4(ADDRA[4]), .A5(ADDRA[5]), 
  .D(DINA[13]), .SPO(pre_douta_0[13]), .DPRA0(ADDRB[0]), .DPRA1(ADDRB[1]), .DPRA2(ADDRB[2]), 
  .DPRA3(ADDRB[3]), .DPRA4(ADDRB[4]), .DPRA5(ADDRB[5]), .DPO(pre_doutb_0[13]));
// Memory array element: COL: 0, ROW: 14
//  Initialization of Dual Port LUT ram now done through explicit
// parameter setting.
RAM64X1D#(.INIT(64'h0000000000000000))RAM64X1D_instance_3214(.WCLK(CLK), .WE(wea_0), 
  .A0(ADDRA[0]), .A1(ADDRA[1]), .A2(ADDRA[2]), .A3(ADDRA[3]), .A4(ADDRA[4]), .A5(ADDRA[5]), 
  .D(DINA[14]), .SPO(pre_douta_0[14]), .DPRA0(ADDRB[0]), .DPRA1(ADDRB[1]), .DPRA2(ADDRB[2]), 
  .DPRA3(ADDRB[3]), .DPRA4(ADDRB[4]), .DPRA5(ADDRB[5]), .DPO(pre_doutb_0[14]));
// Memory array element: COL: 0, ROW: 15
//  Initialization of Dual Port LUT ram now done through explicit
// parameter setting.
RAM64X1D#(.INIT(64'h0000000000000000))RAM64X1D_instance_3215(.WCLK(CLK), .WE(wea_0), 
  .A0(ADDRA[0]), .A1(ADDRA[1]), .A2(ADDRA[2]), .A3(ADDRA[3]), .A4(ADDRA[4]), .A5(ADDRA[5]), 
  .D(DINA[15]), .SPO(pre_douta_0[15]), .DPRA0(ADDRB[0]), .DPRA1(ADDRB[1]), .DPRA2(ADDRB[2]), 
  .DPRA3(ADDRB[3]), .DPRA4(ADDRB[4]), .DPRA5(ADDRB[5]), .DPO(pre_doutb_0[15]));
// Memory array element: COL: 0, ROW: 16
//  Initialization of Dual Port LUT ram now done through explicit
// parameter setting.
RAM64X1D#(.INIT(64'h0000000000000000))RAM64X1D_instance_3216(.WCLK(CLK), .WE(wea_0), 
  .A0(ADDRA[0]), .A1(ADDRA[1]), .A2(ADDRA[2]), .A3(ADDRA[3]), .A4(ADDRA[4]), .A5(ADDRA[5]), 
  .D(DINA[16]), .SPO(pre_douta_0[16]), .DPRA0(ADDRB[0]), .DPRA1(ADDRB[1]), .DPRA2(ADDRB[2]), 
  .DPRA3(ADDRB[3]), .DPRA4(ADDRB[4]), .DPRA5(ADDRB[5]), .DPO(pre_doutb_0[16]));
// Memory array element: COL: 0, ROW: 17
//  Initialization of Dual Port LUT ram now done through explicit
// parameter setting.
RAM64X1D#(.INIT(64'h0000000000000000))RAM64X1D_instance_3217(.WCLK(CLK), .WE(wea_0), 
  .A0(ADDRA[0]), .A1(ADDRA[1]), .A2(ADDRA[2]), .A3(ADDRA[3]), .A4(ADDRA[4]), .A5(ADDRA[5]), 
  .D(DINA[17]), .SPO(pre_douta_0[17]), .DPRA0(ADDRB[0]), .DPRA1(ADDRB[1]), .DPRA2(ADDRB[2]), 
  .DPRA3(ADDRB[3]), .DPRA4(ADDRB[4]), .DPRA5(ADDRB[5]), .DPO(pre_doutb_0[17]));
// Memory array element: COL: 0, ROW: 18
//  Initialization of Dual Port LUT ram now done through explicit
// parameter setting.
RAM64X1D#(.INIT(64'h0000000000000000))RAM64X1D_instance_3218(.WCLK(CLK), .WE(wea_0), 
  .A0(ADDRA[0]), .A1(ADDRA[1]), .A2(ADDRA[2]), .A3(ADDRA[3]), .A4(ADDRA[4]), .A5(ADDRA[5]), 
  .D(DINA[18]), .SPO(pre_douta_0[18]), .DPRA0(ADDRB[0]), .DPRA1(ADDRB[1]), .DPRA2(ADDRB[2]), 
  .DPRA3(ADDRB[3]), .DPRA4(ADDRB[4]), .DPRA5(ADDRB[5]), .DPO(pre_doutb_0[18]));
// Memory array element: COL: 0, ROW: 19
//  Initialization of Dual Port LUT ram now done through explicit
// parameter setting.
RAM64X1D#(.INIT(64'h0000000000000000))RAM64X1D_instance_3219(.WCLK(CLK), .WE(wea_0), 
  .A0(ADDRA[0]), .A1(ADDRA[1]), .A2(ADDRA[2]), .A3(ADDRA[3]), .A4(ADDRA[4]), .A5(ADDRA[5]), 
  .D(DINA[19]), .SPO(pre_douta_0[19]), .DPRA0(ADDRB[0]), .DPRA1(ADDRB[1]), .DPRA2(ADDRB[2]), 
  .DPRA3(ADDRB[3]), .DPRA4(ADDRB[4]), .DPRA5(ADDRB[5]), .DPO(pre_doutb_0[19]));
// Memory array element: COL: 0, ROW: 20
//  Initialization of Dual Port LUT ram now done through explicit
// parameter setting.
RAM64X1D#(.INIT(64'h0000000000000000))RAM64X1D_instance_3220(.WCLK(CLK), .WE(wea_0), 
  .A0(ADDRA[0]), .A1(ADDRA[1]), .A2(ADDRA[2]), .A3(ADDRA[3]), .A4(ADDRA[4]), .A5(ADDRA[5]), 
  .D(DINA[20]), .SPO(pre_douta_0[20]), .DPRA0(ADDRB[0]), .DPRA1(ADDRB[1]), .DPRA2(ADDRB[2]), 
  .DPRA3(ADDRB[3]), .DPRA4(ADDRB[4]), .DPRA5(ADDRB[5]), .DPO(pre_doutb_0[20]));
// Memory array element: COL: 0, ROW: 21
//  Initialization of Dual Port LUT ram now done through explicit
// parameter setting.
RAM64X1D#(.INIT(64'h0000000000000000))RAM64X1D_instance_3221(.WCLK(CLK), .WE(wea_0), 
  .A0(ADDRA[0]), .A1(ADDRA[1]), .A2(ADDRA[2]), .A3(ADDRA[3]), .A4(ADDRA[4]), .A5(ADDRA[5]), 
  .D(DINA[21]), .SPO(pre_douta_0[21]), .DPRA0(ADDRB[0]), .DPRA1(ADDRB[1]), .DPRA2(ADDRB[2]), 
  .DPRA3(ADDRB[3]), .DPRA4(ADDRB[4]), .DPRA5(ADDRB[5]), .DPO(pre_doutb_0[21]));
// Memory array element: COL: 0, ROW: 22
//  Initialization of Dual Port LUT ram now done through explicit
// parameter setting.
RAM64X1D#(.INIT(64'h0000000000000000))RAM64X1D_instance_3222(.WCLK(CLK), .WE(wea_0), 
  .A0(ADDRA[0]), .A1(ADDRA[1]), .A2(ADDRA[2]), .A3(ADDRA[3]), .A4(ADDRA[4]), .A5(ADDRA[5]), 
  .D(DINA[22]), .SPO(pre_douta_0[22]), .DPRA0(ADDRB[0]), .DPRA1(ADDRB[1]), .DPRA2(ADDRB[2]), 
  .DPRA3(ADDRB[3]), .DPRA4(ADDRB[4]), .DPRA5(ADDRB[5]), .DPO(pre_doutb_0[22]));
// Memory array element: COL: 0, ROW: 23
//  Initialization of Dual Port LUT ram now done through explicit
// parameter setting.
RAM64X1D#(.INIT(64'h0000000000000000))RAM64X1D_instance_3223(.WCLK(CLK), .WE(wea_0), 
  .A0(ADDRA[0]), .A1(ADDRA[1]), .A2(ADDRA[2]), .A3(ADDRA[3]), .A4(ADDRA[4]), .A5(ADDRA[5]), 
  .D(DINA[23]), .SPO(pre_douta_0[23]), .DPRA0(ADDRB[0]), .DPRA1(ADDRB[1]), .DPRA2(ADDRB[2]), 
  .DPRA3(ADDRB[3]), .DPRA4(ADDRB[4]), .DPRA5(ADDRB[5]), .DPO(pre_doutb_0[23]));
// Memory array element: COL: 0, ROW: 24
//  Initialization of Dual Port LUT ram now done through explicit
// parameter setting.
RAM64X1D#(.INIT(64'h0000000000000000))RAM64X1D_instance_3224(.WCLK(CLK), .WE(wea_0), 
  .A0(ADDRA[0]), .A1(ADDRA[1]), .A2(ADDRA[2]), .A3(ADDRA[3]), .A4(ADDRA[4]), .A5(ADDRA[5]), 
  .D(DINA[24]), .SPO(pre_douta_0[24]), .DPRA0(ADDRB[0]), .DPRA1(ADDRB[1]), .DPRA2(ADDRB[2]), 
  .DPRA3(ADDRB[3]), .DPRA4(ADDRB[4]), .DPRA5(ADDRB[5]), .DPO(pre_doutb_0[24]));
// Memory array element: COL: 0, ROW: 25
//  Initialization of Dual Port LUT ram now done through explicit
// parameter setting.
RAM64X1D#(.INIT(64'h0000000000000000))RAM64X1D_instance_3225(.WCLK(CLK), .WE(wea_0), 
  .A0(ADDRA[0]), .A1(ADDRA[1]), .A2(ADDRA[2]), .A3(ADDRA[3]), .A4(ADDRA[4]), .A5(ADDRA[5]), 
  .D(DINA[25]), .SPO(pre_douta_0[25]), .DPRA0(ADDRB[0]), .DPRA1(ADDRB[1]), .DPRA2(ADDRB[2]), 
  .DPRA3(ADDRB[3]), .DPRA4(ADDRB[4]), .DPRA5(ADDRB[5]), .DPO(pre_doutb_0[25]));
// Memory array element: COL: 0, ROW: 26
//  Initialization of Dual Port LUT ram now done through explicit
// parameter setting.
RAM64X1D#(.INIT(64'h0000000000000000))RAM64X1D_instance_3226(.WCLK(CLK), .WE(wea_0), 
  .A0(ADDRA[0]), .A1(ADDRA[1]), .A2(ADDRA[2]), .A3(ADDRA[3]), .A4(ADDRA[4]), .A5(ADDRA[5]), 
  .D(DINA[26]), .SPO(pre_douta_0[26]), .DPRA0(ADDRB[0]), .DPRA1(ADDRB[1]), .DPRA2(ADDRB[2]), 
  .DPRA3(ADDRB[3]), .DPRA4(ADDRB[4]), .DPRA5(ADDRB[5]), .DPO(pre_doutb_0[26]));
// Memory array element: COL: 0, ROW: 27
//  Initialization of Dual Port LUT ram now done through explicit
// parameter setting.
RAM64X1D#(.INIT(64'h0000000000000000))RAM64X1D_instance_3227(.WCLK(CLK), .WE(wea_0), 
  .A0(ADDRA[0]), .A1(ADDRA[1]), .A2(ADDRA[2]), .A3(ADDRA[3]), .A4(ADDRA[4]), .A5(ADDRA[5]), 
  .D(DINA[27]), .SPO(pre_douta_0[27]), .DPRA0(ADDRB[0]), .DPRA1(ADDRB[1]), .DPRA2(ADDRB[2]), 
  .DPRA3(ADDRB[3]), .DPRA4(ADDRB[4]), .DPRA5(ADDRB[5]), .DPO(pre_doutb_0[27]));
// Memory array element: COL: 0, ROW: 28
//  Initialization of Dual Port LUT ram now done through explicit
// parameter setting.
RAM64X1D#(.INIT(64'h0000000000000000))RAM64X1D_instance_3228(.WCLK(CLK), .WE(wea_0), 
  .A0(ADDRA[0]), .A1(ADDRA[1]), .A2(ADDRA[2]), .A3(ADDRA[3]), .A4(ADDRA[4]), .A5(ADDRA[5]), 
  .D(DINA[28]), .SPO(pre_douta_0[28]), .DPRA0(ADDRB[0]), .DPRA1(ADDRB[1]), .DPRA2(ADDRB[2]), 
  .DPRA3(ADDRB[3]), .DPRA4(ADDRB[4]), .DPRA5(ADDRB[5]), .DPO(pre_doutb_0[28]));
// Memory array element: COL: 0, ROW: 29
//  Initialization of Dual Port LUT ram now done through explicit
// parameter setting.
RAM64X1D#(.INIT(64'h0000000000000000))RAM64X1D_instance_3229(.WCLK(CLK), .WE(wea_0), 
  .A0(ADDRA[0]), .A1(ADDRA[1]), .A2(ADDRA[2]), .A3(ADDRA[3]), .A4(ADDRA[4]), .A5(ADDRA[5]), 
  .D(DINA[29]), .SPO(pre_douta_0[29]), .DPRA0(ADDRB[0]), .DPRA1(ADDRB[1]), .DPRA2(ADDRB[2]), 
  .DPRA3(ADDRB[3]), .DPRA4(ADDRB[4]), .DPRA5(ADDRB[5]), .DPO(pre_doutb_0[29]));
// Memory array element: COL: 0, ROW: 30
//  Initialization of Dual Port LUT ram now done through explicit
// parameter setting.
RAM64X1D#(.INIT(64'h0000000000000000))RAM64X1D_instance_3230(.WCLK(CLK), .WE(wea_0), 
  .A0(ADDRA[0]), .A1(ADDRA[1]), .A2(ADDRA[2]), .A3(ADDRA[3]), .A4(ADDRA[4]), .A5(ADDRA[5]), 
  .D(DINA[30]), .SPO(pre_douta_0[30]), .DPRA0(ADDRB[0]), .DPRA1(ADDRB[1]), .DPRA2(ADDRB[2]), 
  .DPRA3(ADDRB[3]), .DPRA4(ADDRB[4]), .DPRA5(ADDRB[5]), .DPO(pre_doutb_0[30]));
// Memory array element: COL: 0, ROW: 31
//  Initialization of Dual Port LUT ram now done through explicit
// parameter setting.
RAM64X1D#(.INIT(64'h0000000000000000))RAM64X1D_instance_3231(.WCLK(CLK), .WE(wea_0), 
  .A0(ADDRA[0]), .A1(ADDRA[1]), .A2(ADDRA[2]), .A3(ADDRA[3]), .A4(ADDRA[4]), .A5(ADDRA[5]), 
  .D(DINA[31]), .SPO(pre_douta_0[31]), .DPRA0(ADDRB[0]), .DPRA1(ADDRB[1]), .DPRA2(ADDRB[2]), 
  .DPRA3(ADDRB[3]), .DPRA4(ADDRB[4]), .DPRA5(ADDRB[5]), .DPO(pre_doutb_0[31]));
// Memory array element: COL: 1, ROW: 0
//  Initialization of Dual Port LUT ram now done through explicit
// parameter setting.
RAM64X1D#(.INIT(64'h0000000000000000))RAM64X1D_instance_3232(.WCLK(CLK), .WE(wea_1), 
  .A0(ADDRA[0]), .A1(ADDRA[1]), .A2(ADDRA[2]), .A3(ADDRA[3]), .A4(ADDRA[4]), .A5(ADDRA[5]), 
  .D(DINA[0]), .SPO(pre_douta_1[0]), .DPRA0(ADDRB[0]), .DPRA1(ADDRB[1]), .DPRA2(ADDRB[2]), 
  .DPRA3(ADDRB[3]), .DPRA4(ADDRB[4]), .DPRA5(ADDRB[5]), .DPO(pre_doutb_1[0]));
// Memory array element: COL: 1, ROW: 1
//  Initialization of Dual Port LUT ram now done through explicit
// parameter setting.
RAM64X1D#(.INIT(64'h0000000000000000))RAM64X1D_instance_3233(.WCLK(CLK), .WE(wea_1), 
  .A0(ADDRA[0]), .A1(ADDRA[1]), .A2(ADDRA[2]), .A3(ADDRA[3]), .A4(ADDRA[4]), .A5(ADDRA[5]), 
  .D(DINA[1]), .SPO(pre_douta_1[1]), .DPRA0(ADDRB[0]), .DPRA1(ADDRB[1]), .DPRA2(ADDRB[2]), 
  .DPRA3(ADDRB[3]), .DPRA4(ADDRB[4]), .DPRA5(ADDRB[5]), .DPO(pre_doutb_1[1]));
// Memory array element: COL: 1, ROW: 2
//  Initialization of Dual Port LUT ram now done through explicit
// parameter setting.
RAM64X1D#(.INIT(64'h0000000000000000))RAM64X1D_instance_3234(.WCLK(CLK), .WE(wea_1), 
  .A0(ADDRA[0]), .A1(ADDRA[1]), .A2(ADDRA[2]), .A3(ADDRA[3]), .A4(ADDRA[4]), .A5(ADDRA[5]), 
  .D(DINA[2]), .SPO(pre_douta_1[2]), .DPRA0(ADDRB[0]), .DPRA1(ADDRB[1]), .DPRA2(ADDRB[2]), 
  .DPRA3(ADDRB[3]), .DPRA4(ADDRB[4]), .DPRA5(ADDRB[5]), .DPO(pre_doutb_1[2]));
// Memory array element: COL: 1, ROW: 3
//  Initialization of Dual Port LUT ram now done through explicit
// parameter setting.
RAM64X1D#(.INIT(64'h0000000000000000))RAM64X1D_instance_3235(.WCLK(CLK), .WE(wea_1), 
  .A0(ADDRA[0]), .A1(ADDRA[1]), .A2(ADDRA[2]), .A3(ADDRA[3]), .A4(ADDRA[4]), .A5(ADDRA[5]), 
  .D(DINA[3]), .SPO(pre_douta_1[3]), .DPRA0(ADDRB[0]), .DPRA1(ADDRB[1]), .DPRA2(ADDRB[2]), 
  .DPRA3(ADDRB[3]), .DPRA4(ADDRB[4]), .DPRA5(ADDRB[5]), .DPO(pre_doutb_1[3]));
// Memory array element: COL: 1, ROW: 4
//  Initialization of Dual Port LUT ram now done through explicit
// parameter setting.
RAM64X1D#(.INIT(64'h0000000000000000))RAM64X1D_instance_3236(.WCLK(CLK), .WE(wea_1), 
  .A0(ADDRA[0]), .A1(ADDRA[1]), .A2(ADDRA[2]), .A3(ADDRA[3]), .A4(ADDRA[4]), .A5(ADDRA[5]), 
  .D(DINA[4]), .SPO(pre_douta_1[4]), .DPRA0(ADDRB[0]), .DPRA1(ADDRB[1]), .DPRA2(ADDRB[2]), 
  .DPRA3(ADDRB[3]), .DPRA4(ADDRB[4]), .DPRA5(ADDRB[5]), .DPO(pre_doutb_1[4]));
// Memory array element: COL: 1, ROW: 5
//  Initialization of Dual Port LUT ram now done through explicit
// parameter setting.
RAM64X1D#(.INIT(64'h0000000000000000))RAM64X1D_instance_3237(.WCLK(CLK), .WE(wea_1), 
  .A0(ADDRA[0]), .A1(ADDRA[1]), .A2(ADDRA[2]), .A3(ADDRA[3]), .A4(ADDRA[4]), .A5(ADDRA[5]), 
  .D(DINA[5]), .SPO(pre_douta_1[5]), .DPRA0(ADDRB[0]), .DPRA1(ADDRB[1]), .DPRA2(ADDRB[2]), 
  .DPRA3(ADDRB[3]), .DPRA4(ADDRB[4]), .DPRA5(ADDRB[5]), .DPO(pre_doutb_1[5]));
// Memory array element: COL: 1, ROW: 6
//  Initialization of Dual Port LUT ram now done through explicit
// parameter setting.
RAM64X1D#(.INIT(64'h0000000000000000))RAM64X1D_instance_3238(.WCLK(CLK), .WE(wea_1), 
  .A0(ADDRA[0]), .A1(ADDRA[1]), .A2(ADDRA[2]), .A3(ADDRA[3]), .A4(ADDRA[4]), .A5(ADDRA[5]), 
  .D(DINA[6]), .SPO(pre_douta_1[6]), .DPRA0(ADDRB[0]), .DPRA1(ADDRB[1]), .DPRA2(ADDRB[2]), 
  .DPRA3(ADDRB[3]), .DPRA4(ADDRB[4]), .DPRA5(ADDRB[5]), .DPO(pre_doutb_1[6]));
// Memory array element: COL: 1, ROW: 7
//  Initialization of Dual Port LUT ram now done through explicit
// parameter setting.
RAM64X1D#(.INIT(64'h0000000000000000))RAM64X1D_instance_3239(.WCLK(CLK), .WE(wea_1), 
  .A0(ADDRA[0]), .A1(ADDRA[1]), .A2(ADDRA[2]), .A3(ADDRA[3]), .A4(ADDRA[4]), .A5(ADDRA[5]), 
  .D(DINA[7]), .SPO(pre_douta_1[7]), .DPRA0(ADDRB[0]), .DPRA1(ADDRB[1]), .DPRA2(ADDRB[2]), 
  .DPRA3(ADDRB[3]), .DPRA4(ADDRB[4]), .DPRA5(ADDRB[5]), .DPO(pre_doutb_1[7]));
// Memory array element: COL: 1, ROW: 8
//  Initialization of Dual Port LUT ram now done through explicit
// parameter setting.
RAM64X1D#(.INIT(64'h0000000000000000))RAM64X1D_instance_3240(.WCLK(CLK), .WE(wea_1), 
  .A0(ADDRA[0]), .A1(ADDRA[1]), .A2(ADDRA[2]), .A3(ADDRA[3]), .A4(ADDRA[4]), .A5(ADDRA[5]), 
  .D(DINA[8]), .SPO(pre_douta_1[8]), .DPRA0(ADDRB[0]), .DPRA1(ADDRB[1]), .DPRA2(ADDRB[2]), 
  .DPRA3(ADDRB[3]), .DPRA4(ADDRB[4]), .DPRA5(ADDRB[5]), .DPO(pre_doutb_1[8]));
// Memory array element: COL: 1, ROW: 9
//  Initialization of Dual Port LUT ram now done through explicit
// parameter setting.
RAM64X1D#(.INIT(64'h0000000000000000))RAM64X1D_instance_3241(.WCLK(CLK), .WE(wea_1), 
  .A0(ADDRA[0]), .A1(ADDRA[1]), .A2(ADDRA[2]), .A3(ADDRA[3]), .A4(ADDRA[4]), .A5(ADDRA[5]), 
  .D(DINA[9]), .SPO(pre_douta_1[9]), .DPRA0(ADDRB[0]), .DPRA1(ADDRB[1]), .DPRA2(ADDRB[2]), 
  .DPRA3(ADDRB[3]), .DPRA4(ADDRB[4]), .DPRA5(ADDRB[5]), .DPO(pre_doutb_1[9]));
// Memory array element: COL: 1, ROW: 10
//  Initialization of Dual Port LUT ram now done through explicit
// parameter setting.
RAM64X1D#(.INIT(64'h0000000000000000))RAM64X1D_instance_3242(.WCLK(CLK), .WE(wea_1), 
  .A0(ADDRA[0]), .A1(ADDRA[1]), .A2(ADDRA[2]), .A3(ADDRA[3]), .A4(ADDRA[4]), .A5(ADDRA[5]), 
  .D(DINA[10]), .SPO(pre_douta_1[10]), .DPRA0(ADDRB[0]), .DPRA1(ADDRB[1]), .DPRA2(ADDRB[2]), 
  .DPRA3(ADDRB[3]), .DPRA4(ADDRB[4]), .DPRA5(ADDRB[5]), .DPO(pre_doutb_1[10]));
// Memory array element: COL: 1, ROW: 11
//  Initialization of Dual Port LUT ram now done through explicit
// parameter setting.
RAM64X1D#(.INIT(64'h0000000000000000))RAM64X1D_instance_3243(.WCLK(CLK), .WE(wea_1), 
  .A0(ADDRA[0]), .A1(ADDRA[1]), .A2(ADDRA[2]), .A3(ADDRA[3]), .A4(ADDRA[4]), .A5(ADDRA[5]), 
  .D(DINA[11]), .SPO(pre_douta_1[11]), .DPRA0(ADDRB[0]), .DPRA1(ADDRB[1]), .DPRA2(ADDRB[2]), 
  .DPRA3(ADDRB[3]), .DPRA4(ADDRB[4]), .DPRA5(ADDRB[5]), .DPO(pre_doutb_1[11]));
// Memory array element: COL: 1, ROW: 12
//  Initialization of Dual Port LUT ram now done through explicit
// parameter setting.
RAM64X1D#(.INIT(64'h0000000000000000))RAM64X1D_instance_3244(.WCLK(CLK), .WE(wea_1), 
  .A0(ADDRA[0]), .A1(ADDRA[1]), .A2(ADDRA[2]), .A3(ADDRA[3]), .A4(ADDRA[4]), .A5(ADDRA[5]), 
  .D(DINA[12]), .SPO(pre_douta_1[12]), .DPRA0(ADDRB[0]), .DPRA1(ADDRB[1]), .DPRA2(ADDRB[2]), 
  .DPRA3(ADDRB[3]), .DPRA4(ADDRB[4]), .DPRA5(ADDRB[5]), .DPO(pre_doutb_1[12]));
// Memory array element: COL: 1, ROW: 13
//  Initialization of Dual Port LUT ram now done through explicit
// parameter setting.
RAM64X1D#(.INIT(64'h0000000000000000))RAM64X1D_instance_3245(.WCLK(CLK), .WE(wea_1), 
  .A0(ADDRA[0]), .A1(ADDRA[1]), .A2(ADDRA[2]), .A3(ADDRA[3]), .A4(ADDRA[4]), .A5(ADDRA[5]), 
  .D(DINA[13]), .SPO(pre_douta_1[13]), .DPRA0(ADDRB[0]), .DPRA1(ADDRB[1]), .DPRA2(ADDRB[2]), 
  .DPRA3(ADDRB[3]), .DPRA4(ADDRB[4]), .DPRA5(ADDRB[5]), .DPO(pre_doutb_1[13]));
// Memory array element: COL: 1, ROW: 14
//  Initialization of Dual Port LUT ram now done through explicit
// parameter setting.
RAM64X1D#(.INIT(64'h0000000000000000))RAM64X1D_instance_3246(.WCLK(CLK), .WE(wea_1), 
  .A0(ADDRA[0]), .A1(ADDRA[1]), .A2(ADDRA[2]), .A3(ADDRA[3]), .A4(ADDRA[4]), .A5(ADDRA[5]), 
  .D(DINA[14]), .SPO(pre_douta_1[14]), .DPRA0(ADDRB[0]), .DPRA1(ADDRB[1]), .DPRA2(ADDRB[2]), 
  .DPRA3(ADDRB[3]), .DPRA4(ADDRB[4]), .DPRA5(ADDRB[5]), .DPO(pre_doutb_1[14]));
// Memory array element: COL: 1, ROW: 15
//  Initialization of Dual Port LUT ram now done through explicit
// parameter setting.
RAM64X1D#(.INIT(64'h0000000000000000))RAM64X1D_instance_3247(.WCLK(CLK), .WE(wea_1), 
  .A0(ADDRA[0]), .A1(ADDRA[1]), .A2(ADDRA[2]), .A3(ADDRA[3]), .A4(ADDRA[4]), .A5(ADDRA[5]), 
  .D(DINA[15]), .SPO(pre_douta_1[15]), .DPRA0(ADDRB[0]), .DPRA1(ADDRB[1]), .DPRA2(ADDRB[2]), 
  .DPRA3(ADDRB[3]), .DPRA4(ADDRB[4]), .DPRA5(ADDRB[5]), .DPO(pre_doutb_1[15]));
// Memory array element: COL: 1, ROW: 16
//  Initialization of Dual Port LUT ram now done through explicit
// parameter setting.
RAM64X1D#(.INIT(64'h0000000000000000))RAM64X1D_instance_3248(.WCLK(CLK), .WE(wea_1), 
  .A0(ADDRA[0]), .A1(ADDRA[1]), .A2(ADDRA[2]), .A3(ADDRA[3]), .A4(ADDRA[4]), .A5(ADDRA[5]), 
  .D(DINA[16]), .SPO(pre_douta_1[16]), .DPRA0(ADDRB[0]), .DPRA1(ADDRB[1]), .DPRA2(ADDRB[2]), 
  .DPRA3(ADDRB[3]), .DPRA4(ADDRB[4]), .DPRA5(ADDRB[5]), .DPO(pre_doutb_1[16]));
// Memory array element: COL: 1, ROW: 17
//  Initialization of Dual Port LUT ram now done through explicit
// parameter setting.
RAM64X1D#(.INIT(64'h0000000000000000))RAM64X1D_instance_3249(.WCLK(CLK), .WE(wea_1), 
  .A0(ADDRA[0]), .A1(ADDRA[1]), .A2(ADDRA[2]), .A3(ADDRA[3]), .A4(ADDRA[4]), .A5(ADDRA[5]), 
  .D(DINA[17]), .SPO(pre_douta_1[17]), .DPRA0(ADDRB[0]), .DPRA1(ADDRB[1]), .DPRA2(ADDRB[2]), 
  .DPRA3(ADDRB[3]), .DPRA4(ADDRB[4]), .DPRA5(ADDRB[5]), .DPO(pre_doutb_1[17]));
// Memory array element: COL: 1, ROW: 18
//  Initialization of Dual Port LUT ram now done through explicit
// parameter setting.
RAM64X1D#(.INIT(64'h0000000000000000))RAM64X1D_instance_3250(.WCLK(CLK), .WE(wea_1), 
  .A0(ADDRA[0]), .A1(ADDRA[1]), .A2(ADDRA[2]), .A3(ADDRA[3]), .A4(ADDRA[4]), .A5(ADDRA[5]), 
  .D(DINA[18]), .SPO(pre_douta_1[18]), .DPRA0(ADDRB[0]), .DPRA1(ADDRB[1]), .DPRA2(ADDRB[2]), 
  .DPRA3(ADDRB[3]), .DPRA4(ADDRB[4]), .DPRA5(ADDRB[5]), .DPO(pre_doutb_1[18]));
// Memory array element: COL: 1, ROW: 19
//  Initialization of Dual Port LUT ram now done through explicit
// parameter setting.
RAM64X1D#(.INIT(64'h0000000000000000))RAM64X1D_instance_3251(.WCLK(CLK), .WE(wea_1), 
  .A0(ADDRA[0]), .A1(ADDRA[1]), .A2(ADDRA[2]), .A3(ADDRA[3]), .A4(ADDRA[4]), .A5(ADDRA[5]), 
  .D(DINA[19]), .SPO(pre_douta_1[19]), .DPRA0(ADDRB[0]), .DPRA1(ADDRB[1]), .DPRA2(ADDRB[2]), 
  .DPRA3(ADDRB[3]), .DPRA4(ADDRB[4]), .DPRA5(ADDRB[5]), .DPO(pre_doutb_1[19]));
// Memory array element: COL: 1, ROW: 20
//  Initialization of Dual Port LUT ram now done through explicit
// parameter setting.
RAM64X1D#(.INIT(64'h0000000000000000))RAM64X1D_instance_3252(.WCLK(CLK), .WE(wea_1), 
  .A0(ADDRA[0]), .A1(ADDRA[1]), .A2(ADDRA[2]), .A3(ADDRA[3]), .A4(ADDRA[4]), .A5(ADDRA[5]), 
  .D(DINA[20]), .SPO(pre_douta_1[20]), .DPRA0(ADDRB[0]), .DPRA1(ADDRB[1]), .DPRA2(ADDRB[2]), 
  .DPRA3(ADDRB[3]), .DPRA4(ADDRB[4]), .DPRA5(ADDRB[5]), .DPO(pre_doutb_1[20]));
// Memory array element: COL: 1, ROW: 21
//  Initialization of Dual Port LUT ram now done through explicit
// parameter setting.
RAM64X1D#(.INIT(64'h0000000000000000))RAM64X1D_instance_3253(.WCLK(CLK), .WE(wea_1), 
  .A0(ADDRA[0]), .A1(ADDRA[1]), .A2(ADDRA[2]), .A3(ADDRA[3]), .A4(ADDRA[4]), .A5(ADDRA[5]), 
  .D(DINA[21]), .SPO(pre_douta_1[21]), .DPRA0(ADDRB[0]), .DPRA1(ADDRB[1]), .DPRA2(ADDRB[2]), 
  .DPRA3(ADDRB[3]), .DPRA4(ADDRB[4]), .DPRA5(ADDRB[5]), .DPO(pre_doutb_1[21]));
// Memory array element: COL: 1, ROW: 22
//  Initialization of Dual Port LUT ram now done through explicit
// parameter setting.
RAM64X1D#(.INIT(64'h0000000000000000))RAM64X1D_instance_3254(.WCLK(CLK), .WE(wea_1), 
  .A0(ADDRA[0]), .A1(ADDRA[1]), .A2(ADDRA[2]), .A3(ADDRA[3]), .A4(ADDRA[4]), .A5(ADDRA[5]), 
  .D(DINA[22]), .SPO(pre_douta_1[22]), .DPRA0(ADDRB[0]), .DPRA1(ADDRB[1]), .DPRA2(ADDRB[2]), 
  .DPRA3(ADDRB[3]), .DPRA4(ADDRB[4]), .DPRA5(ADDRB[5]), .DPO(pre_doutb_1[22]));
// Memory array element: COL: 1, ROW: 23
//  Initialization of Dual Port LUT ram now done through explicit
// parameter setting.
RAM64X1D#(.INIT(64'h0000000000000000))RAM64X1D_instance_3255(.WCLK(CLK), .WE(wea_1), 
  .A0(ADDRA[0]), .A1(ADDRA[1]), .A2(ADDRA[2]), .A3(ADDRA[3]), .A4(ADDRA[4]), .A5(ADDRA[5]), 
  .D(DINA[23]), .SPO(pre_douta_1[23]), .DPRA0(ADDRB[0]), .DPRA1(ADDRB[1]), .DPRA2(ADDRB[2]), 
  .DPRA3(ADDRB[3]), .DPRA4(ADDRB[4]), .DPRA5(ADDRB[5]), .DPO(pre_doutb_1[23]));
// Memory array element: COL: 1, ROW: 24
//  Initialization of Dual Port LUT ram now done through explicit
// parameter setting.
RAM64X1D#(.INIT(64'h0000000000000000))RAM64X1D_instance_3256(.WCLK(CLK), .WE(wea_1), 
  .A0(ADDRA[0]), .A1(ADDRA[1]), .A2(ADDRA[2]), .A3(ADDRA[3]), .A4(ADDRA[4]), .A5(ADDRA[5]), 
  .D(DINA[24]), .SPO(pre_douta_1[24]), .DPRA0(ADDRB[0]), .DPRA1(ADDRB[1]), .DPRA2(ADDRB[2]), 
  .DPRA3(ADDRB[3]), .DPRA4(ADDRB[4]), .DPRA5(ADDRB[5]), .DPO(pre_doutb_1[24]));
// Memory array element: COL: 1, ROW: 25
//  Initialization of Dual Port LUT ram now done through explicit
// parameter setting.
RAM64X1D#(.INIT(64'h0000000000000000))RAM64X1D_instance_3257(.WCLK(CLK), .WE(wea_1), 
  .A0(ADDRA[0]), .A1(ADDRA[1]), .A2(ADDRA[2]), .A3(ADDRA[3]), .A4(ADDRA[4]), .A5(ADDRA[5]), 
  .D(DINA[25]), .SPO(pre_douta_1[25]), .DPRA0(ADDRB[0]), .DPRA1(ADDRB[1]), .DPRA2(ADDRB[2]), 
  .DPRA3(ADDRB[3]), .DPRA4(ADDRB[4]), .DPRA5(ADDRB[5]), .DPO(pre_doutb_1[25]));
// Memory array element: COL: 1, ROW: 26
//  Initialization of Dual Port LUT ram now done through explicit
// parameter setting.
RAM64X1D#(.INIT(64'h0000000000000000))RAM64X1D_instance_3258(.WCLK(CLK), .WE(wea_1), 
  .A0(ADDRA[0]), .A1(ADDRA[1]), .A2(ADDRA[2]), .A3(ADDRA[3]), .A4(ADDRA[4]), .A5(ADDRA[5]), 
  .D(DINA[26]), .SPO(pre_douta_1[26]), .DPRA0(ADDRB[0]), .DPRA1(ADDRB[1]), .DPRA2(ADDRB[2]), 
  .DPRA3(ADDRB[3]), .DPRA4(ADDRB[4]), .DPRA5(ADDRB[5]), .DPO(pre_doutb_1[26]));
// Memory array element: COL: 1, ROW: 27
//  Initialization of Dual Port LUT ram now done through explicit
// parameter setting.
RAM64X1D#(.INIT(64'h0000000000000000))RAM64X1D_instance_3259(.WCLK(CLK), .WE(wea_1), 
  .A0(ADDRA[0]), .A1(ADDRA[1]), .A2(ADDRA[2]), .A3(ADDRA[3]), .A4(ADDRA[4]), .A5(ADDRA[5]), 
  .D(DINA[27]), .SPO(pre_douta_1[27]), .DPRA0(ADDRB[0]), .DPRA1(ADDRB[1]), .DPRA2(ADDRB[2]), 
  .DPRA3(ADDRB[3]), .DPRA4(ADDRB[4]), .DPRA5(ADDRB[5]), .DPO(pre_doutb_1[27]));
// Memory array element: COL: 1, ROW: 28
//  Initialization of Dual Port LUT ram now done through explicit
// parameter setting.
RAM64X1D#(.INIT(64'h0000000000000000))RAM64X1D_instance_3260(.WCLK(CLK), .WE(wea_1), 
  .A0(ADDRA[0]), .A1(ADDRA[1]), .A2(ADDRA[2]), .A3(ADDRA[3]), .A4(ADDRA[4]), .A5(ADDRA[5]), 
  .D(DINA[28]), .SPO(pre_douta_1[28]), .DPRA0(ADDRB[0]), .DPRA1(ADDRB[1]), .DPRA2(ADDRB[2]), 
  .DPRA3(ADDRB[3]), .DPRA4(ADDRB[4]), .DPRA5(ADDRB[5]), .DPO(pre_doutb_1[28]));
// Memory array element: COL: 1, ROW: 29
//  Initialization of Dual Port LUT ram now done through explicit
// parameter setting.
RAM64X1D#(.INIT(64'h0000000000000000))RAM64X1D_instance_3261(.WCLK(CLK), .WE(wea_1), 
  .A0(ADDRA[0]), .A1(ADDRA[1]), .A2(ADDRA[2]), .A3(ADDRA[3]), .A4(ADDRA[4]), .A5(ADDRA[5]), 
  .D(DINA[29]), .SPO(pre_douta_1[29]), .DPRA0(ADDRB[0]), .DPRA1(ADDRB[1]), .DPRA2(ADDRB[2]), 
  .DPRA3(ADDRB[3]), .DPRA4(ADDRB[4]), .DPRA5(ADDRB[5]), .DPO(pre_doutb_1[29]));
// Memory array element: COL: 1, ROW: 30
//  Initialization of Dual Port LUT ram now done through explicit
// parameter setting.
RAM64X1D#(.INIT(64'h0000000000000000))RAM64X1D_instance_3262(.WCLK(CLK), .WE(wea_1), 
  .A0(ADDRA[0]), .A1(ADDRA[1]), .A2(ADDRA[2]), .A3(ADDRA[3]), .A4(ADDRA[4]), .A5(ADDRA[5]), 
  .D(DINA[30]), .SPO(pre_douta_1[30]), .DPRA0(ADDRB[0]), .DPRA1(ADDRB[1]), .DPRA2(ADDRB[2]), 
  .DPRA3(ADDRB[3]), .DPRA4(ADDRB[4]), .DPRA5(ADDRB[5]), .DPO(pre_doutb_1[30]));
// Memory array element: COL: 1, ROW: 31
//  Initialization of Dual Port LUT ram now done through explicit
// parameter setting.
RAM64X1D#(.INIT(64'h0000000000000000))RAM64X1D_instance_3263(.WCLK(CLK), .WE(wea_1), 
  .A0(ADDRA[0]), .A1(ADDRA[1]), .A2(ADDRA[2]), .A3(ADDRA[3]), .A4(ADDRA[4]), .A5(ADDRA[5]), 
  .D(DINA[31]), .SPO(pre_douta_1[31]), .DPRA0(ADDRB[0]), .DPRA1(ADDRB[1]), .DPRA2(ADDRB[2]), 
  .DPRA3(ADDRB[3]), .DPRA4(ADDRB[4]), .DPRA5(ADDRB[5]), .DPO(pre_doutb_1[31]));
endmodule



module medianRow12_structuralmemory_7c35cfb6_(CLK_u96, bus_697a4aab_, bus_27583776_, bus_5bf1e8c9_, bus_4d69e99e_, bus_54670688_, bus_183e0c67_, bus_04c9d28b_, bus_21a0a30e_, bus_334ea5e4_, bus_4d4099b8_, bus_4f2fd13f_, bus_2d1d0872_, bus_3b845d6f_);
input		CLK_u96;
input		bus_697a4aab_;
input	[31:0]	bus_27583776_;
input	[2:0]	bus_5bf1e8c9_;
input		bus_4d69e99e_;
input		bus_54670688_;
input	[31:0]	bus_183e0c67_;
input	[31:0]	bus_04c9d28b_;
input	[2:0]	bus_21a0a30e_;
input		bus_334ea5e4_;
output	[31:0]	bus_4d4099b8_;
output		bus_4f2fd13f_;
output	[31:0]	bus_2d1d0872_;
output		bus_3b845d6f_;
wire		and_65479ba3_u0;
wire		not_7f224c46_u0;
reg		logicalMem_3c832673_we_delay0_u0=1'h0;
wire		or_69a9a262_u0;
wire	[31:0]	bus_2e65fe38_;
wire	[31:0]	bus_146542d3_;
wire		or_47c2dfec_u0;
assign and_65479ba3_u0=bus_4d69e99e_&not_7f224c46_u0;
assign not_7f224c46_u0=~bus_54670688_;
always @(posedge CLK_u96 or posedge bus_697a4aab_)
begin
if (bus_697a4aab_)
logicalMem_3c832673_we_delay0_u0<=1'h0;
else logicalMem_3c832673_we_delay0_u0<=bus_54670688_;
end
assign or_69a9a262_u0=and_65479ba3_u0|logicalMem_3c832673_we_delay0_u0;
assign bus_4d4099b8_=bus_146542d3_;
assign bus_4f2fd13f_=or_69a9a262_u0;
assign bus_2d1d0872_=bus_2e65fe38_;
assign bus_3b845d6f_=bus_334ea5e4_;
medianRow12_forge_memory_101x32_153 medianRow12_forge_memory_101x32_153_instance0(.CLK(CLK_u96), 
  .ENA(or_47c2dfec_u0), .WEA(bus_54670688_), .DINA(bus_183e0c67_), .ADDRA(bus_27583776_), 
  .DOUTA(bus_146542d3_), .DONEA(), .ENB(bus_334ea5e4_), .ADDRB(bus_04c9d28b_), .DOUTB(bus_2e65fe38_), 
  .DONEB());
assign or_47c2dfec_u0=bus_4d69e99e_|bus_54670688_;
endmodule



module medianRow12_endianswapper_35f568d1_(endianswapper_35f568d1_in, endianswapper_35f568d1_out);
input	[31:0]	endianswapper_35f568d1_in;
output	[31:0]	endianswapper_35f568d1_out;
assign endianswapper_35f568d1_out=endianswapper_35f568d1_in;
endmodule



module medianRow12_endianswapper_1b6f50d2_(endianswapper_1b6f50d2_in, endianswapper_1b6f50d2_out);
input	[31:0]	endianswapper_1b6f50d2_in;
output	[31:0]	endianswapper_1b6f50d2_out;
assign endianswapper_1b6f50d2_out=endianswapper_1b6f50d2_in;
endmodule



module medianRow12_stateVar_i(bus_17363ce6_, bus_63f2e3c3_, bus_5696c9fc_, bus_047907f0_, bus_6dd21a1a_, bus_59b7fd9a_, bus_5eca54c7_);
input		bus_17363ce6_;
input		bus_63f2e3c3_;
input		bus_5696c9fc_;
input	[31:0]	bus_047907f0_;
input		bus_6dd21a1a_;
input	[31:0]	bus_59b7fd9a_;
output	[31:0]	bus_5eca54c7_;
reg	[31:0]	stateVar_i_u52=32'h0;
wire	[31:0]	endianswapper_35f568d1_out;
wire		or_2e84c49d_u0;
wire	[31:0]	mux_5013860f_u0;
wire	[31:0]	endianswapper_1b6f50d2_out;
assign bus_5eca54c7_=endianswapper_1b6f50d2_out;
always @(posedge bus_17363ce6_ or posedge bus_63f2e3c3_)
begin
if (bus_63f2e3c3_)
stateVar_i_u52<=32'h0;
else if (or_2e84c49d_u0)
stateVar_i_u52<=endianswapper_35f568d1_out;
end
medianRow12_endianswapper_35f568d1_ medianRow12_endianswapper_35f568d1__1(.endianswapper_35f568d1_in(mux_5013860f_u0), 
  .endianswapper_35f568d1_out(endianswapper_35f568d1_out));
assign or_2e84c49d_u0=bus_5696c9fc_|bus_6dd21a1a_;
assign mux_5013860f_u0=(bus_5696c9fc_)?bus_047907f0_:32'h0;
medianRow12_endianswapper_1b6f50d2_ medianRow12_endianswapper_1b6f50d2__1(.endianswapper_1b6f50d2_in(stateVar_i_u52), 
  .endianswapper_1b6f50d2_out(endianswapper_1b6f50d2_out));
endmodule



module medianRow12_globalreset_physical_0187300a_(bus_3fad3916_, bus_3104f221_, bus_7e00a85a_);
input		bus_3fad3916_;
input		bus_3104f221_;
output		bus_7e00a85a_;
reg		glitch_u62=1'h0;
reg		cross_u62=1'h0;
reg		sample_u62=1'h0;
reg		final_u62=1'h1;
wire		or_35c7be5d_u0;
wire		and_334380d8_u0;
wire		not_640e6c25_u0;
assign bus_7e00a85a_=or_35c7be5d_u0;
always @(posedge bus_3fad3916_)
begin
glitch_u62<=cross_u62;
end
always @(posedge bus_3fad3916_)
begin
cross_u62<=sample_u62;
end
always @(posedge bus_3fad3916_)
begin
sample_u62<=1'h1;
end
always @(posedge bus_3fad3916_)
begin
final_u62<=not_640e6c25_u0;
end
assign or_35c7be5d_u0=bus_3104f221_|final_u62;
assign and_334380d8_u0=cross_u62&glitch_u62;
assign not_640e6c25_u0=~and_334380d8_u0;
endmodule



module medianRow12_compute_median(CLK, RESET, GO, port_5b6460df_, port_65b500bd_, port_4116d66a_, port_6972be56_, port_656fc6de_, RESULT, RESULT_u2314, RESULT_u2315, RESULT_u2316, RESULT_u2317, RESULT_u2318, RESULT_u2319, RESULT_u2320, RESULT_u2321, RESULT_u2322, RESULT_u2323, RESULT_u2324, RESULT_u2325, RESULT_u2326, RESULT_u2327, DONE);
input		CLK;
input		RESET;
input		GO;
input		port_5b6460df_;
input	[31:0]	port_65b500bd_;
input		port_4116d66a_;
input		port_6972be56_;
input	[31:0]	port_656fc6de_;
output		RESULT;
output	[31:0]	RESULT_u2314;
output		RESULT_u2315;
output	[31:0]	RESULT_u2316;
output	[2:0]	RESULT_u2317;
output		RESULT_u2318;
output	[31:0]	RESULT_u2319;
output	[31:0]	RESULT_u2320;
output	[2:0]	RESULT_u2321;
output		RESULT_u2322;
output	[31:0]	RESULT_u2323;
output	[2:0]	RESULT_u2324;
output	[7:0]	RESULT_u2325;
output	[15:0]	RESULT_u2326;
output		RESULT_u2327;
output		DONE;
wire		and_u3950_u0;
wire		lessThan;
wire signed	[32:0]	lessThan_b_signed;
wire signed	[32:0]	lessThan_a_signed;
wire		not_u918_u0;
wire		and_u3951_u0;
wire		and_u3952_u0;
wire	[31:0]	add;
wire		and_u3953_u0;
wire	[31:0]	add_u716;
wire	[31:0]	add_u717;
wire		and_u3954_u0;
wire signed	[31:0]	greaterThan_a_signed;
wire signed	[31:0]	greaterThan_b_signed;
wire		greaterThan;
wire		not_u919_u0;
wire		and_u3955_u0;
wire		and_u3956_u0;
wire	[31:0]	add_u718;
wire		and_u3957_u0;
wire	[31:0]	add_u719;
wire	[31:0]	add_u720;
wire		and_u3958_u0;
wire	[31:0]	add_u721;
wire		or_u1516_u0;
reg		reg_3c99eb5d_u0=1'h0;
wire		and_u3959_u0;
wire	[31:0]	add_u722;
wire	[31:0]	add_u723;
reg		reg_253d3653_u0=1'h0;
wire		and_u3960_u0;
wire		or_u1517_u0;
reg	[31:0]	syncEnable_u1507=32'h0;
reg	[31:0]	syncEnable_u1508_u0=32'h0;
reg	[31:0]	syncEnable_u1509_u0=32'h0;
reg		reg_6c182ebb_u0=1'h0;
reg	[31:0]	syncEnable_u1510_u0=32'h0;
wire	[31:0]	mux_u2004;
wire	[31:0]	mux_u2005_u0;
wire		or_u1518_u0;
reg	[31:0]	syncEnable_u1511_u0=32'h0;
reg		block_GO_delayed_u106=1'h0;
reg	[31:0]	syncEnable_u1512_u0=32'h0;
wire		and_u3961_u0;
wire	[31:0]	mux_u2006_u0;
reg		reg_12e54c61_u0=1'h0;
wire		mux_u2007_u0;
reg		syncEnable_u1513_u0=1'h0;
reg		reg_4d8f12a7_u0=1'h0;
reg		reg_0d8a7ac6_u0=1'h0;
reg		and_delayed_u402=1'h0;
reg		and_delayed_result_delayed_u15=1'h0;
wire	[31:0]	mux_u2008_u0;
wire		and_u3962_u0;
reg	[31:0]	syncEnable_u1514_u0=32'h0;
wire		or_u1519_u0;
reg	[31:0]	syncEnable_u1515_u0=32'h0;
wire	[31:0]	add_u724;
reg	[31:0]	syncEnable_u1516_u0=32'h0;
wire	[31:0]	mux_u2009_u0;
wire		or_u1520_u0;
reg	[31:0]	syncEnable_u1517_u0=32'h0;
reg	[31:0]	syncEnable_u1518_u0=32'h0;
reg	[31:0]	syncEnable_u1519_u0=32'h0;
reg	[31:0]	syncEnable_u1520_u0=32'h0;
wire	[31:0]	mux_u2010_u0;
wire		or_u1521_u0;
reg		syncEnable_u1521_u0=1'h0;
reg	[31:0]	syncEnable_u1522_u0=32'h0;
reg		block_GO_delayed_u107_u0=1'h0;
wire		and_u3963_u0;
wire		mux_u2011_u0;
wire	[31:0]	mux_u2012_u0;
wire		or_u1522_u0;
wire	[31:0]	mux_u2013_u0;
wire	[31:0]	mux_u2014_u0;
wire	[31:0]	mux_u2015_u0;
wire	[31:0]	mux_u2016_u0;
wire	[15:0]	add_u725;
reg	[31:0]	latch_126f882e_reg=32'h0;
wire	[31:0]	latch_126f882e_out;
wire	[31:0]	latch_5239dc5a_out;
reg	[31:0]	latch_5239dc5a_reg=32'h0;
reg		scoreboard_5babb9cc_reg0=1'h0;
wire		bus_0fe6260a_;
wire		scoreboard_5babb9cc_resOr0;
wire		scoreboard_5babb9cc_and;
wire		scoreboard_5babb9cc_resOr1;
reg		scoreboard_5babb9cc_reg1=1'h0;
reg		latch_02e67715_reg=1'h0;
wire		latch_02e67715_out;
reg		reg_37816834_u0=1'h0;
reg	[15:0]	syncEnable_u1523_u0=16'h0;
wire	[31:0]	latch_6ba9fb3f_out;
reg	[31:0]	latch_6ba9fb3f_reg=32'h0;
wire	[31:0]	latch_39f9a8ee_out;
reg	[31:0]	latch_39f9a8ee_reg=32'h0;
wire		and_u3964_u0;
wire	[15:0]	lessThan_u108_a_unsigned;
wire		lessThan_u108;
wire	[15:0]	lessThan_u108_b_unsigned;
wire		andOp;
wire		not_u920_u0;
wire		and_u3965_u0;
wire		and_u3966_u0;
reg	[31:0]	fbReg_tmp_row0_u50=32'h0;
wire		mux_u2017_u0;
wire	[31:0]	mux_u2018_u0;
reg		fbReg_swapped_u50=1'h0;
wire	[15:0]	mux_u2019_u0;
reg		loopControl_u104=1'h0;
reg	[31:0]	fbReg_tmp_u50=32'h0;
reg	[31:0]	fbReg_tmp_row1_u50=32'h0;
reg	[31:0]	fbReg_tmp_row_u50=32'h0;
wire	[31:0]	mux_u2020_u0;
wire	[31:0]	mux_u2021_u0;
reg	[15:0]	fbReg_idx_u50=16'h0;
wire	[31:0]	mux_u2022_u0;
wire		or_u1523_u0;
reg		syncEnable_u1524_u0=1'h0;
wire		and_u3967_u0;
wire		simplePinWrite;
wire	[7:0]	simplePinWrite_u638;
wire	[15:0]	simplePinWrite_u639;
reg		reg_445bafc0_u0=1'h0;
reg		reg_445bafc0_result_delayed_u0=1'h0;
wire	[31:0]	mux_u2023_u0;
wire		or_u1524_u0;
assign and_u3950_u0=and_u3965_u0&or_u1523_u0;
assign lessThan_a_signed={1'b0, mux_u2013_u0};
assign lessThan_b_signed=33'h64;
assign lessThan=lessThan_a_signed<lessThan_b_signed;
assign not_u918_u0=~lessThan;
assign and_u3951_u0=or_u1522_u0&lessThan;
assign and_u3952_u0=or_u1522_u0&not_u918_u0;
assign add=mux_u2013_u0+32'h0;
assign and_u3953_u0=and_u3963_u0&port_6972be56_;
assign add_u716=mux_u2013_u0+32'h1;
assign add_u717=add_u716+32'h0;
assign and_u3954_u0=and_u3963_u0&port_4116d66a_;
assign greaterThan_a_signed=syncEnable_u1520_u0;
assign greaterThan_b_signed=syncEnable_u1518_u0;
assign greaterThan=greaterThan_a_signed>greaterThan_b_signed;
assign not_u919_u0=~greaterThan;
assign and_u3955_u0=block_GO_delayed_u107_u0&greaterThan;
assign and_u3956_u0=block_GO_delayed_u107_u0&not_u919_u0;
assign add_u718=syncEnable_u1516_u0+32'h0;
assign and_u3957_u0=and_u3961_u0&port_6972be56_;
assign add_u719=syncEnable_u1516_u0+32'h1;
assign add_u720=add_u719+32'h0;
assign and_u3958_u0=and_u3961_u0&port_4116d66a_;
assign add_u721=syncEnable_u1516_u0+32'h0;
assign or_u1516_u0=and_u3959_u0|RESET;
always @(posedge CLK or posedge block_GO_delayed_u106 or posedge or_u1516_u0)
begin
if (or_u1516_u0)
reg_3c99eb5d_u0<=1'h0;
else if (block_GO_delayed_u106)
reg_3c99eb5d_u0<=1'h1;
else reg_3c99eb5d_u0<=reg_3c99eb5d_u0;
end
assign and_u3959_u0=reg_3c99eb5d_u0&port_4116d66a_;
assign add_u722=syncEnable_u1516_u0+32'h1;
assign add_u723=add_u722+32'h0;
always @(posedge CLK or posedge reg_6c182ebb_u0 or posedge or_u1517_u0)
begin
if (or_u1517_u0)
reg_253d3653_u0<=1'h0;
else if (reg_6c182ebb_u0)
reg_253d3653_u0<=1'h1;
else reg_253d3653_u0<=reg_253d3653_u0;
end
assign and_u3960_u0=reg_253d3653_u0&port_4116d66a_;
assign or_u1517_u0=and_u3960_u0|RESET;
always @(posedge CLK)
begin
if (and_u3961_u0)
syncEnable_u1507<=port_65b500bd_;
end
always @(posedge CLK)
begin
if (and_u3961_u0)
syncEnable_u1508_u0<=port_656fc6de_;
end
always @(posedge CLK)
begin
if (and_u3961_u0)
syncEnable_u1509_u0<=port_65b500bd_;
end
always @(posedge CLK or posedge RESET)
begin
if (RESET)
reg_6c182ebb_u0<=1'h0;
else reg_6c182ebb_u0<=block_GO_delayed_u106;
end
always @(posedge CLK)
begin
if (and_u3961_u0)
syncEnable_u1510_u0<=add_u723;
end
assign mux_u2004=({32{block_GO_delayed_u106}}&syncEnable_u1512_u0)|({32{reg_6c182ebb_u0}}&syncEnable_u1510_u0)|({32{and_u3961_u0}}&add_u720);
assign mux_u2005_u0=(block_GO_delayed_u106)?syncEnable_u1507:syncEnable_u1508_u0;
assign or_u1518_u0=block_GO_delayed_u106|reg_6c182ebb_u0;
always @(posedge CLK)
begin
if (and_u3961_u0)
syncEnable_u1511_u0<=port_656fc6de_;
end
always @(posedge CLK or posedge RESET)
begin
if (RESET)
block_GO_delayed_u106<=1'h0;
else block_GO_delayed_u106<=and_u3961_u0;
end
always @(posedge CLK)
begin
if (and_u3961_u0)
syncEnable_u1512_u0<=add_u721;
end
assign and_u3961_u0=and_u3955_u0&block_GO_delayed_u107_u0;
assign mux_u2006_u0=(reg_0d8a7ac6_u0)?syncEnable_u1514_u0:syncEnable_u1511_u0;
always @(posedge CLK or posedge RESET)
begin
if (RESET)
reg_12e54c61_u0<=1'h0;
else reg_12e54c61_u0<=reg_4d8f12a7_u0;
end
assign mux_u2007_u0=(reg_0d8a7ac6_u0)?syncEnable_u1513_u0:1'h1;
always @(posedge CLK)
begin
if (block_GO_delayed_u107_u0)
syncEnable_u1513_u0<=syncEnable_u1521_u0;
end
always @(posedge CLK or posedge RESET)
begin
if (RESET)
reg_4d8f12a7_u0<=1'h0;
else reg_4d8f12a7_u0<=and_delayed_result_delayed_u15;
end
always @(posedge CLK or posedge RESET)
begin
if (RESET)
reg_0d8a7ac6_u0<=1'h0;
else reg_0d8a7ac6_u0<=and_u3962_u0;
end
always @(posedge CLK or posedge RESET)
begin
if (RESET)
and_delayed_u402<=1'h0;
else and_delayed_u402<=and_u3961_u0;
end
always @(posedge CLK or posedge RESET)
begin
if (RESET)
and_delayed_result_delayed_u15<=1'h0;
else and_delayed_result_delayed_u15<=and_delayed_u402;
end
assign mux_u2008_u0=(reg_0d8a7ac6_u0)?syncEnable_u1515_u0:syncEnable_u1509_u0;
assign and_u3962_u0=and_u3956_u0&block_GO_delayed_u107_u0;
always @(posedge CLK)
begin
if (block_GO_delayed_u107_u0)
syncEnable_u1514_u0<=syncEnable_u1522_u0;
end
assign or_u1519_u0=reg_0d8a7ac6_u0|reg_12e54c61_u0;
always @(posedge CLK)
begin
if (block_GO_delayed_u107_u0)
syncEnable_u1515_u0<=syncEnable_u1519_u0;
end
assign add_u724=mux_u2013_u0+32'h1;
always @(posedge CLK)
begin
if (and_u3963_u0)
syncEnable_u1516_u0<=mux_u2013_u0;
end
assign mux_u2009_u0=({32{or_u1518_u0}}&mux_u2004)|({32{and_u3963_u0}}&add_u717)|({32{and_u3961_u0}}&mux_u2004);
assign or_u1520_u0=and_u3963_u0|and_u3961_u0;
always @(posedge CLK)
begin
if (and_u3963_u0)
syncEnable_u1517_u0<=add_u724;
end
always @(posedge CLK)
begin
if (and_u3963_u0)
syncEnable_u1518_u0<=port_65b500bd_;
end
always @(posedge CLK)
begin
if (and_u3963_u0)
syncEnable_u1519_u0<=mux_u2015_u0;
end
always @(posedge CLK)
begin
if (and_u3963_u0)
syncEnable_u1520_u0<=port_656fc6de_;
end
assign mux_u2010_u0=(and_u3963_u0)?add:add_u718;
assign or_u1521_u0=and_u3963_u0|and_u3961_u0;
always @(posedge CLK)
begin
if (and_u3963_u0)
syncEnable_u1521_u0<=mux_u2011_u0;
end
always @(posedge CLK)
begin
if (and_u3963_u0)
syncEnable_u1522_u0<=mux_u2012_u0;
end
always @(posedge CLK or posedge RESET)
begin
if (RESET)
block_GO_delayed_u107_u0<=1'h0;
else block_GO_delayed_u107_u0<=and_u3963_u0;
end
assign and_u3963_u0=and_u3951_u0&or_u1522_u0;
assign mux_u2011_u0=(and_u3964_u0)?1'h0:mux_u2007_u0;
assign mux_u2012_u0=(and_u3964_u0)?mux_u2021_u0:mux_u2006_u0;
assign or_u1522_u0=and_u3964_u0|or_u1519_u0;
assign mux_u2013_u0=(and_u3964_u0)?32'h0:syncEnable_u1517_u0;
assign mux_u2014_u0=(and_u3964_u0)?mux_u2022_u0:syncEnable_u1520_u0;
assign mux_u2015_u0=(and_u3964_u0)?mux_u2020_u0:mux_u2008_u0;
assign mux_u2016_u0=(and_u3964_u0)?mux_u2018_u0:syncEnable_u1518_u0;
assign add_u725=mux_u2019_u0+16'h1;
always @(posedge CLK)
begin
if (or_u1519_u0)
latch_126f882e_reg<=syncEnable_u1520_u0;
end
assign latch_126f882e_out=(or_u1519_u0)?syncEnable_u1520_u0:latch_126f882e_reg;
assign latch_5239dc5a_out=(or_u1519_u0)?mux_u2008_u0:latch_5239dc5a_reg;
always @(posedge CLK)
begin
if (or_u1519_u0)
latch_5239dc5a_reg<=mux_u2008_u0;
end
always @(posedge CLK)
begin
if (bus_0fe6260a_)
scoreboard_5babb9cc_reg0<=1'h0;
else if (or_u1519_u0)
scoreboard_5babb9cc_reg0<=1'h1;
else scoreboard_5babb9cc_reg0<=scoreboard_5babb9cc_reg0;
end
assign bus_0fe6260a_=scoreboard_5babb9cc_and|RESET;
assign scoreboard_5babb9cc_resOr0=or_u1519_u0|scoreboard_5babb9cc_reg0;
assign scoreboard_5babb9cc_and=scoreboard_5babb9cc_resOr0&scoreboard_5babb9cc_resOr1;
assign scoreboard_5babb9cc_resOr1=reg_37816834_u0|scoreboard_5babb9cc_reg1;
always @(posedge CLK)
begin
if (bus_0fe6260a_)
scoreboard_5babb9cc_reg1<=1'h0;
else if (reg_37816834_u0)
scoreboard_5babb9cc_reg1<=1'h1;
else scoreboard_5babb9cc_reg1<=scoreboard_5babb9cc_reg1;
end
always @(posedge CLK)
begin
if (or_u1519_u0)
latch_02e67715_reg<=mux_u2007_u0;
end
assign latch_02e67715_out=(or_u1519_u0)?mux_u2007_u0:latch_02e67715_reg;
always @(posedge CLK or posedge RESET)
begin
if (RESET)
reg_37816834_u0<=1'h0;
else reg_37816834_u0<=or_u1519_u0;
end
always @(posedge CLK)
begin
if (and_u3964_u0)
syncEnable_u1523_u0<=add_u725;
end
assign latch_6ba9fb3f_out=(or_u1519_u0)?mux_u2006_u0:latch_6ba9fb3f_reg;
always @(posedge CLK)
begin
if (or_u1519_u0)
latch_6ba9fb3f_reg<=mux_u2006_u0;
end
assign latch_39f9a8ee_out=(or_u1519_u0)?syncEnable_u1518_u0:latch_39f9a8ee_reg;
always @(posedge CLK)
begin
if (or_u1519_u0)
latch_39f9a8ee_reg<=syncEnable_u1518_u0;
end
assign and_u3964_u0=and_u3966_u0&or_u1523_u0;
assign lessThan_u108_a_unsigned=mux_u2019_u0;
assign lessThan_u108_b_unsigned=16'h65;
assign lessThan_u108=lessThan_u108_a_unsigned<lessThan_u108_b_unsigned;
assign andOp=lessThan_u108&mux_u2017_u0;
assign not_u920_u0=~andOp;
assign and_u3965_u0=or_u1523_u0&not_u920_u0;
assign and_u3966_u0=or_u1523_u0&andOp;
always @(posedge CLK)
begin
if (scoreboard_5babb9cc_and)
fbReg_tmp_row0_u50<=latch_39f9a8ee_out;
end
assign mux_u2017_u0=(loopControl_u104)?fbReg_swapped_u50:1'h0;
assign mux_u2018_u0=(loopControl_u104)?fbReg_tmp_row0_u50:32'h0;
always @(posedge CLK)
begin
if (scoreboard_5babb9cc_and)
fbReg_swapped_u50<=latch_02e67715_out;
end
assign mux_u2019_u0=(loopControl_u104)?fbReg_idx_u50:16'h0;
always @(posedge CLK or posedge syncEnable_u1524_u0)
begin
if (syncEnable_u1524_u0)
loopControl_u104<=1'h0;
else loopControl_u104<=scoreboard_5babb9cc_and;
end
always @(posedge CLK)
begin
if (scoreboard_5babb9cc_and)
fbReg_tmp_u50<=latch_6ba9fb3f_out;
end
always @(posedge CLK)
begin
if (scoreboard_5babb9cc_and)
fbReg_tmp_row1_u50<=latch_5239dc5a_out;
end
always @(posedge CLK)
begin
if (scoreboard_5babb9cc_and)
fbReg_tmp_row_u50<=latch_126f882e_out;
end
assign mux_u2020_u0=(loopControl_u104)?fbReg_tmp_row1_u50:32'h0;
assign mux_u2021_u0=(loopControl_u104)?fbReg_tmp_u50:32'h0;
always @(posedge CLK)
begin
if (scoreboard_5babb9cc_and)
fbReg_idx_u50<=syncEnable_u1523_u0;
end
assign mux_u2022_u0=(loopControl_u104)?fbReg_tmp_row_u50:32'h0;
assign or_u1523_u0=loopControl_u104|GO;
always @(posedge CLK)
begin
if (GO)
syncEnable_u1524_u0<=RESET;
end
assign and_u3967_u0=reg_445bafc0_u0&port_6972be56_;
assign simplePinWrite=reg_445bafc0_u0&{1{reg_445bafc0_u0}};
assign simplePinWrite_u638=port_656fc6de_[7:0];
assign simplePinWrite_u639=16'h1&{16{1'h1}};
always @(posedge CLK or posedge RESET)
begin
if (RESET)
reg_445bafc0_u0<=1'h0;
else reg_445bafc0_u0<=and_u3950_u0;
end
always @(posedge CLK or posedge RESET)
begin
if (RESET)
reg_445bafc0_result_delayed_u0<=1'h0;
else reg_445bafc0_result_delayed_u0<=reg_445bafc0_u0;
end
assign mux_u2023_u0=(or_u1521_u0)?mux_u2010_u0:32'h32;
assign or_u1524_u0=or_u1521_u0|reg_445bafc0_u0;
assign RESULT=GO;
assign RESULT_u2314=32'h0;
assign RESULT_u2315=or_u1520_u0;
assign RESULT_u2316=mux_u2009_u0;
assign RESULT_u2317=3'h1;
assign RESULT_u2318=or_u1518_u0;
assign RESULT_u2319=mux_u2009_u0;
assign RESULT_u2320=mux_u2005_u0;
assign RESULT_u2321=3'h1;
assign RESULT_u2322=or_u1524_u0;
assign RESULT_u2323=mux_u2023_u0;
assign RESULT_u2324=3'h1;
assign RESULT_u2325=simplePinWrite_u638;
assign RESULT_u2326=simplePinWrite_u639;
assign RESULT_u2327=simplePinWrite;
assign DONE=reg_445bafc0_result_delayed_u0;
endmodule



module medianRow12_simplememoryreferee_09dd9f7f_(bus_298f49cd_, bus_70add411_, bus_317ca22f_, bus_7f38c04c_, bus_6a42c33d_, bus_2c84212e_, bus_1b7d7334_, bus_367bcf39_, bus_7a2f929e_, bus_13bd36b7_, bus_065379e4_, bus_6e5ca7dc_, bus_45e81940_, bus_5ac62c92_, bus_6da4356f_, bus_3f6ea69d_, bus_01a1fca9_, bus_226ed96c_, bus_0168bae8_, bus_3ed4d8c9_, bus_20b887ae_);
input		bus_298f49cd_;
input		bus_70add411_;
input		bus_317ca22f_;
input	[31:0]	bus_7f38c04c_;
input		bus_6a42c33d_;
input	[31:0]	bus_2c84212e_;
input	[31:0]	bus_1b7d7334_;
input	[2:0]	bus_367bcf39_;
input		bus_7a2f929e_;
input		bus_13bd36b7_;
input	[31:0]	bus_065379e4_;
input	[31:0]	bus_6e5ca7dc_;
input	[2:0]	bus_45e81940_;
output	[31:0]	bus_5ac62c92_;
output	[31:0]	bus_6da4356f_;
output		bus_3f6ea69d_;
output		bus_01a1fca9_;
output	[2:0]	bus_226ed96c_;
output		bus_0168bae8_;
output	[31:0]	bus_3ed4d8c9_;
output		bus_20b887ae_;
wire		not_4bde93dd_u0;
wire		or_195dba39_u0;
wire		and_2a65d66d_u0;
reg		done_qual_u228=1'h0;
wire		not_0e08c3c4_u0;
wire		or_5f8028cd_u0;
wire		and_6b94e35a_u0;
wire		or_01a58956_u0;
reg		done_qual_u229_u0=1'h0;
wire	[31:0]	mux_66006936_u0;
wire		or_5a5d5500_u0;
wire	[31:0]	mux_3d91226f_u0;
wire		or_2c7c2202_u0;
assign bus_5ac62c92_=mux_3d91226f_u0;
assign bus_6da4356f_=mux_66006936_u0;
assign bus_3f6ea69d_=or_5f8028cd_u0;
assign bus_01a1fca9_=or_5a5d5500_u0;
assign bus_226ed96c_=3'h1;
assign bus_0168bae8_=and_6b94e35a_u0;
assign bus_3ed4d8c9_=bus_7f38c04c_;
assign bus_20b887ae_=and_2a65d66d_u0;
assign not_4bde93dd_u0=~bus_317ca22f_;
assign or_195dba39_u0=or_01a58956_u0|done_qual_u229_u0;
assign and_2a65d66d_u0=or_195dba39_u0&bus_317ca22f_;
always @(posedge bus_298f49cd_)
begin
if (bus_70add411_)
done_qual_u228<=1'h0;
else done_qual_u228<=bus_6a42c33d_;
end
assign not_0e08c3c4_u0=~bus_317ca22f_;
assign or_5f8028cd_u0=bus_6a42c33d_|bus_13bd36b7_;
assign and_6b94e35a_u0=or_2c7c2202_u0&bus_317ca22f_;
assign or_01a58956_u0=bus_7a2f929e_|bus_13bd36b7_;
always @(posedge bus_298f49cd_)
begin
if (bus_70add411_)
done_qual_u229_u0<=1'h0;
else done_qual_u229_u0<=or_01a58956_u0;
end
assign mux_66006936_u0=(bus_6a42c33d_)?bus_1b7d7334_:bus_6e5ca7dc_;
assign or_5a5d5500_u0=bus_6a42c33d_|or_01a58956_u0;
assign mux_3d91226f_u0=(bus_6a42c33d_)?{24'b0, bus_2c84212e_[7:0]}:bus_065379e4_;
assign or_2c7c2202_u0=bus_6a42c33d_|done_qual_u228;
endmodule



module medianRow12_scheduler(CLK, RESET, GO, port_7b5325cc_, port_234a25c0_, port_47791d8f_, port_13891123_, port_694c86c1_, port_14c718fd_, RESULT, RESULT_u2328, RESULT_u2329, RESULT_u2330, DONE);
input		CLK;
input		RESET;
input		GO;
input		port_7b5325cc_;
input	[31:0]	port_234a25c0_;
input		port_47791d8f_;
input		port_13891123_;
input		port_694c86c1_;
input		port_14c718fd_;
output		RESULT;
output		RESULT_u2328;
output		RESULT_u2329;
output		RESULT_u2330;
output		DONE;
wire signed	[31:0]	lessThan_a_signed;
wire signed	[31:0]	lessThan_b_signed;
wire		lessThan;
wire signed	[31:0]	equals_a_signed;
wire signed	[31:0]	equals_b_signed;
wire		equals;
wire signed	[31:0]	equals_u242_a_signed;
wire		equals_u242;
wire signed	[31:0]	equals_u242_b_signed;
wire		and_u3968_u0;
wire		and_u3969_u0;
wire		not_u921_u0;
wire		andOp;
wire		and_u3970_u0;
wire		and_u3971_u0;
wire		not_u922_u0;
wire		simplePinWrite;
wire		and_u3972_u0;
wire		and_u3973_u0;
wire signed	[31:0]	equals_u243_a_signed;
wire signed	[31:0]	equals_u243_b_signed;
wire		equals_u243;
wire		and_u3974_u0;
wire		and_u3975_u0;
wire		not_u923_u0;
wire		andOp_u84;
wire		not_u924_u0;
wire		and_u3976_u0;
wire		and_u3977_u0;
wire		simplePinWrite_u640;
wire		and_u3978_u0;
wire		and_u3979_u0;
wire		not_u925_u0;
wire		and_u3980_u0;
wire		not_u926_u0;
wire		and_u3981_u0;
wire		simplePinWrite_u641;
wire		or_u1525_u0;
wire		and_u3982_u0;
wire		and_u3983_u0;
reg		and_delayed_u403=1'h0;
reg		reg_79b21d02_u0=1'h0;
wire		or_u1526_u0;
wire		and_u3984_u0;
wire		and_u3985_u0;
wire		and_u3986_u0;
wire		and_u3987_u0;
reg		and_delayed_u404_u0=1'h0;
wire		or_u1527_u0;
wire		mux_u2024;
wire		or_u1528_u0;
wire		and_u3988_u0;
reg		and_delayed_u405_u0=1'h0;
wire		or_u1529_u0;
wire		and_u3989_u0;
wire		or_u1530_u0;
wire		mux_u2025_u0;
wire		receive_go_merge;
wire		and_u3990_u0;
reg		syncEnable_u1525=1'h0;
reg		loopControl_u105=1'h0;
wire		or_u1531_u0;
wire		or_u1532_u0;
wire		mux_u2026_u0;
reg		reg_7e2f96cb_u0=1'h0;
reg		reg_3b7498da_u0=1'h0;
assign lessThan_a_signed=port_234a25c0_;
assign lessThan_b_signed=32'h65;
assign lessThan=lessThan_a_signed<lessThan_b_signed;
assign equals_a_signed=port_234a25c0_;
assign equals_b_signed=32'h65;
assign equals=equals_a_signed==equals_b_signed;
assign equals_u242_a_signed={31'b0, port_7b5325cc_};
assign equals_u242_b_signed=32'h0;
assign equals_u242=equals_u242_a_signed==equals_u242_b_signed;
assign and_u3968_u0=and_u3990_u0&not_u921_u0;
assign and_u3969_u0=and_u3990_u0&equals_u242;
assign not_u921_u0=~equals_u242;
assign andOp=lessThan&port_694c86c1_;
assign and_u3970_u0=and_u3973_u0&andOp;
assign and_u3971_u0=and_u3973_u0&not_u922_u0;
assign not_u922_u0=~andOp;
assign simplePinWrite=and_u3972_u0&{1{and_u3972_u0}};
assign and_u3972_u0=and_u3970_u0&and_u3973_u0;
assign and_u3973_u0=and_u3969_u0&and_u3990_u0;
assign equals_u243_a_signed={31'b0, port_7b5325cc_};
assign equals_u243_b_signed=32'h1;
assign equals_u243=equals_u243_a_signed==equals_u243_b_signed;
assign and_u3974_u0=and_u3990_u0&not_u923_u0;
assign and_u3975_u0=and_u3990_u0&equals_u243;
assign not_u923_u0=~equals_u243;
assign andOp_u84=lessThan&port_694c86c1_;
assign not_u924_u0=~andOp_u84;
assign and_u3976_u0=and_u3989_u0&not_u924_u0;
assign and_u3977_u0=and_u3989_u0&andOp_u84;
assign simplePinWrite_u640=and_u3987_u0&{1{and_u3987_u0}};
assign and_u3978_u0=and_u3986_u0&equals;
assign and_u3979_u0=and_u3986_u0&not_u925_u0;
assign not_u925_u0=~equals;
assign and_u3980_u0=and_u3984_u0&port_13891123_;
assign not_u926_u0=~port_13891123_;
assign and_u3981_u0=and_u3984_u0&not_u926_u0;
assign simplePinWrite_u641=and_u3982_u0&{1{and_u3982_u0}};
assign or_u1525_u0=port_47791d8f_|and_delayed_u403;
assign and_u3982_u0=and_u3980_u0&and_u3984_u0;
assign and_u3983_u0=and_u3981_u0&and_u3984_u0;
always @(posedge CLK or posedge RESET)
begin
if (RESET)
and_delayed_u403<=1'h0;
else and_delayed_u403<=and_u3983_u0;
end
always @(posedge CLK or posedge RESET)
begin
if (RESET)
reg_79b21d02_u0<=1'h0;
else reg_79b21d02_u0<=and_u3985_u0;
end
assign or_u1526_u0=or_u1525_u0|reg_79b21d02_u0;
assign and_u3984_u0=and_u3978_u0&and_u3986_u0;
assign and_u3985_u0=and_u3979_u0&and_u3986_u0;
assign and_u3986_u0=and_u3976_u0&and_u3989_u0;
assign and_u3987_u0=and_u3977_u0&and_u3989_u0;
always @(posedge CLK or posedge RESET)
begin
if (RESET)
and_delayed_u404_u0<=1'h0;
else and_delayed_u404_u0<=and_u3987_u0;
end
assign or_u1527_u0=and_delayed_u404_u0|or_u1526_u0;
assign mux_u2024=(and_u3987_u0)?1'h1:1'h0;
assign or_u1528_u0=and_u3987_u0|and_u3982_u0;
assign and_u3988_u0=and_u3974_u0&and_u3990_u0;
always @(posedge CLK or posedge RESET)
begin
if (RESET)
and_delayed_u405_u0<=1'h0;
else and_delayed_u405_u0<=and_u3988_u0;
end
assign or_u1529_u0=or_u1527_u0|and_delayed_u405_u0;
assign and_u3989_u0=and_u3975_u0&and_u3990_u0;
assign or_u1530_u0=and_u3972_u0|or_u1528_u0;
assign mux_u2025_u0=(and_u3972_u0)?1'h1:mux_u2024;
assign receive_go_merge=simplePinWrite|simplePinWrite_u640;
assign and_u3990_u0=or_u1531_u0&or_u1531_u0;
always @(posedge CLK)
begin
if (reg_7e2f96cb_u0)
syncEnable_u1525<=RESET;
end
always @(posedge CLK or posedge syncEnable_u1525)
begin
if (syncEnable_u1525)
loopControl_u105<=1'h0;
else loopControl_u105<=or_u1529_u0;
end
assign or_u1531_u0=loopControl_u105|reg_7e2f96cb_u0;
assign or_u1532_u0=GO|or_u1530_u0;
assign mux_u2026_u0=(GO)?1'h0:mux_u2025_u0;
always @(posedge CLK or posedge RESET)
begin
if (RESET)
reg_7e2f96cb_u0<=1'h0;
else reg_7e2f96cb_u0<=reg_3b7498da_u0;
end
always @(posedge CLK or posedge RESET)
begin
if (RESET)
reg_3b7498da_u0<=1'h0;
else reg_3b7498da_u0<=GO;
end
assign RESULT=or_u1532_u0;
assign RESULT_u2328=mux_u2026_u0;
assign RESULT_u2329=receive_go_merge;
assign RESULT_u2330=simplePinWrite_u641;
assign DONE=1'h0;
endmodule



module medianRow12_endianswapper_59bdf2a8_(endianswapper_59bdf2a8_in, endianswapper_59bdf2a8_out);
input		endianswapper_59bdf2a8_in;
output		endianswapper_59bdf2a8_out;
assign endianswapper_59bdf2a8_out=endianswapper_59bdf2a8_in;
endmodule



module medianRow12_endianswapper_7c48f8f4_(endianswapper_7c48f8f4_in, endianswapper_7c48f8f4_out);
input		endianswapper_7c48f8f4_in;
output		endianswapper_7c48f8f4_out;
assign endianswapper_7c48f8f4_out=endianswapper_7c48f8f4_in;
endmodule



module medianRow12_stateVar_fsmState_medianRow12(bus_240a3b4c_, bus_352fb3be_, bus_690711b0_, bus_545875fe_, bus_2f0aac67_);
input		bus_240a3b4c_;
input		bus_352fb3be_;
input		bus_690711b0_;
input		bus_545875fe_;
output		bus_2f0aac67_;
wire		endianswapper_59bdf2a8_out;
wire		endianswapper_7c48f8f4_out;
reg		stateVar_fsmState_medianRow12_u2=1'h0;
medianRow12_endianswapper_59bdf2a8_ medianRow12_endianswapper_59bdf2a8__1(.endianswapper_59bdf2a8_in(stateVar_fsmState_medianRow12_u2), 
  .endianswapper_59bdf2a8_out(endianswapper_59bdf2a8_out));
assign bus_2f0aac67_=endianswapper_59bdf2a8_out;
medianRow12_endianswapper_7c48f8f4_ medianRow12_endianswapper_7c48f8f4__1(.endianswapper_7c48f8f4_in(bus_545875fe_), 
  .endianswapper_7c48f8f4_out(endianswapper_7c48f8f4_out));
always @(posedge bus_240a3b4c_ or posedge bus_352fb3be_)
begin
if (bus_352fb3be_)
stateVar_fsmState_medianRow12_u2<=1'h0;
else if (bus_690711b0_)
stateVar_fsmState_medianRow12_u2<=endianswapper_7c48f8f4_out;
end
endmodule



module medianRow12_Kicker_62(CLK, RESET, bus_6fa98c49_);
input		CLK;
input		RESET;
output		bus_6fa98c49_;
wire		bus_0dcd36fd_;
wire		bus_645ec91d_;
reg		kicker_1=1'h0;
reg		kicker_2=1'h0;
wire		bus_021809d7_;
wire		bus_52d36de4_;
reg		kicker_res=1'h0;
assign bus_6fa98c49_=kicker_res;
assign bus_0dcd36fd_=bus_52d36de4_&kicker_1;
assign bus_645ec91d_=kicker_1&bus_52d36de4_&bus_021809d7_;
always @(posedge CLK)
begin
kicker_1<=bus_52d36de4_;
end
always @(posedge CLK)
begin
kicker_2<=bus_0dcd36fd_;
end
assign bus_021809d7_=~kicker_2;
assign bus_52d36de4_=~RESET;
always @(posedge CLK)
begin
kicker_res<=bus_645ec91d_;
end
endmodule



module medianRow12_receive(CLK, RESET, GO, port_2a85fe33_, port_47e5f0d9_, port_525f2c3d_, RESULT, RESULT_u2331, RESULT_u2332, RESULT_u2333, RESULT_u2334, RESULT_u2335, RESULT_u2336, DONE);
input		CLK;
input		RESET;
input		GO;
input	[31:0]	port_2a85fe33_;
input		port_47e5f0d9_;
input	[7:0]	port_525f2c3d_;
output		RESULT;
output	[31:0]	RESULT_u2331;
output		RESULT_u2332;
output	[31:0]	RESULT_u2333;
output	[31:0]	RESULT_u2334;
output	[2:0]	RESULT_u2335;
output		RESULT_u2336;
output		DONE;
wire		simplePinWrite;
wire	[31:0]	add;
reg		reg_4ee27909_u0=1'h0;
wire		and_u3991_u0;
wire		or_u1533_u0;
wire	[31:0]	add_u726;
reg		reg_7780d253_u0=1'h0;
assign simplePinWrite=GO&{1{GO}};
assign add=port_2a85fe33_+32'h0;
always @(posedge CLK or posedge GO or posedge or_u1533_u0)
begin
if (or_u1533_u0)
reg_4ee27909_u0<=1'h0;
else if (GO)
reg_4ee27909_u0<=1'h1;
else reg_4ee27909_u0<=reg_4ee27909_u0;
end
assign and_u3991_u0=reg_4ee27909_u0&port_47e5f0d9_;
assign or_u1533_u0=and_u3991_u0|RESET;
assign add_u726=port_2a85fe33_+32'h1;
always @(posedge CLK or posedge RESET)
begin
if (RESET)
reg_7780d253_u0<=1'h0;
else reg_7780d253_u0<=GO;
end
assign RESULT=GO;
assign RESULT_u2331=add_u726;
assign RESULT_u2332=GO;
assign RESULT_u2333=add;
assign RESULT_u2334={24'b0, port_525f2c3d_};
assign RESULT_u2335=3'h1;
assign RESULT_u2336=simplePinWrite;
assign DONE=reg_7780d253_u0;
endmodule


