// __  ___ __ ___  _ __   ___  ___ 
// \ \/ / '__/ _ \| '_ \ / _ \/ __|
//  >  <| | | (_) | | | | (_) \__ \
// /_/\_\_|  \___/|_| |_|\___/|___/
// 
// Xronos synthesizer version
// Run date: Fri 23 Feb 2018 14:28:43 +0000
// 

module medianRow13(median_SEND, median_DATA, in1_ACK, RESET, median_RDY, CLK, median_ACK, in1_DATA, median_COUNT, in1_COUNT, in1_SEND);
output		median_SEND;
wire		receive_go;
wire		compute_median_done;
output	[7:0]	median_DATA;
output		in1_ACK;
wire		receive_done;
wire		compute_median_go;
input		RESET;
input		median_RDY;
input		CLK;
input		median_ACK;
input	[7:0]	in1_DATA;
output	[15:0]	median_COUNT;
input	[15:0]	in1_COUNT;
input		in1_SEND;
wire	[31:0]	bus_5c23c6dc_;
wire		bus_66881bff_;
wire		medianRow13_scheduler_instance_DONE;
wire		scheduler_u823;
wire		scheduler_u825;
wire		scheduler;
wire		scheduler_u824;
wire		bus_70cf810e_;
wire		bus_54176535_;
wire	[31:0]	bus_70d38c83_;
wire		bus_0bbf1d7b_;
wire	[2:0]	bus_3e71dc68_;
wire	[31:0]	bus_5a7462db_;
wire		bus_76271ce4_;
wire		bus_54e28a5d_;
wire	[31:0]	bus_15f53daf_;
wire	[31:0]	compute_median_u688;
wire	[2:0]	compute_median_u693;
wire		compute_median_u698;
wire	[15:0]	compute_median_u697;
wire	[31:0]	compute_median_u692;
wire		compute_median;
wire	[2:0]	compute_median_u690;
wire	[31:0]	compute_median_u689;
wire	[31:0]	compute_median_u695;
wire	[2:0]	compute_median_u696;
wire		compute_median_u691;
wire	[7:0]	compute_median_u699;
wire		compute_median_u694;
wire	[31:0]	compute_median_u686;
wire		medianRow13_compute_median_instance_DONE;
wire		compute_median_u687;
wire		bus_4cc1a91e_;
wire		bus_039ae53e_;
wire	[31:0]	bus_3f884be2_;
wire	[31:0]	bus_79467be5_;
wire		bus_4d9fd1fe_;
wire	[2:0]	bus_64070445_;
wire	[31:0]	bus_5565d9f1_;
wire		bus_73218e0e_;
wire		bus_2fe1579d_;
wire		receive_u299;
wire	[31:0]	receive_u296;
wire	[31:0]	receive_u294;
wire	[2:0]	receive_u298;
wire		medianRow13_receive_instance_DONE;
wire	[31:0]	receive_u297;
wire		receive;
wire		receive_u295;
wire		bus_0050164c_;
wire		bus_560c2387_;
wire	[31:0]	bus_7fade1e4_;
wire		bus_3ae5bab1_;
wire	[31:0]	bus_65ef3584_;
assign median_SEND=compute_median_u698;
assign receive_go=scheduler_u825;
assign compute_median_done=bus_0050164c_;
assign median_DATA=compute_median_u699;
assign in1_ACK=receive_u299;
assign receive_done=bus_73218e0e_;
assign compute_median_go=scheduler_u824;
assign median_COUNT=compute_median_u697;
medianRow13_stateVar_i medianRow13_stateVar_i_1(.bus_67535e14_(CLK), .bus_6966df10_(bus_66881bff_), 
  .bus_28317430_(receive), .bus_0a2ee72e_(receive_u294), .bus_5d6e1582_(compute_median), 
  .bus_0c2155af_(32'h0), .bus_5c23c6dc_(bus_5c23c6dc_));
medianRow13_globalreset_physical_3c277d63_ medianRow13_globalreset_physical_3c277d63__1(.bus_1b31f810_(CLK), 
  .bus_48bdb164_(RESET), .bus_66881bff_(bus_66881bff_));
medianRow13_scheduler medianRow13_scheduler_instance(.CLK(CLK), .RESET(bus_66881bff_), 
  .GO(bus_2fe1579d_), .port_6db0638b_(bus_70cf810e_), .port_01cb707e_(bus_5c23c6dc_), 
  .port_30d32211_(receive_done), .port_378f0696_(in1_SEND), .port_525c72b7_(median_RDY), 
  .port_3a33183a_(compute_median_done), .DONE(medianRow13_scheduler_instance_DONE), 
  .RESULT(scheduler), .RESULT_u2291(scheduler_u823), .RESULT_u2292(scheduler_u824), 
  .RESULT_u2293(scheduler_u825));
medianRow13_stateVar_fsmState_medianRow13 medianRow13_stateVar_fsmState_medianRow13_1(.bus_1d3c6a38_(CLK), 
  .bus_602b6445_(bus_66881bff_), .bus_7c42b9f2_(scheduler), .bus_396324bb_(scheduler_u823), 
  .bus_70cf810e_(bus_70cf810e_));
medianRow13_simplememoryreferee_49ed623a_ medianRow13_simplememoryreferee_49ed623a__1(.bus_045deef4_(CLK), 
  .bus_6989ef5f_(bus_66881bff_), .bus_6a0202d4_(bus_560c2387_), .bus_0d6400bc_(bus_65ef3584_), 
  .bus_56689ef6_(receive_u295), .bus_10203f31_({24'b0, receive_u297[7:0]}), .bus_3b7102f6_(receive_u296), 
  .bus_18979418_(3'h1), .bus_35258787_(compute_median_u694), .bus_17bceaca_(compute_median_u687), 
  .bus_1c74e322_(compute_median_u689), .bus_00571e15_(compute_median_u688), .bus_6cf3ffb6_(3'h1), 
  .bus_70d38c83_(bus_70d38c83_), .bus_15f53daf_(bus_15f53daf_), .bus_54e28a5d_(bus_54e28a5d_), 
  .bus_0bbf1d7b_(bus_0bbf1d7b_), .bus_3e71dc68_(bus_3e71dc68_), .bus_54176535_(bus_54176535_), 
  .bus_5a7462db_(bus_5a7462db_), .bus_76271ce4_(bus_76271ce4_));
medianRow13_compute_median medianRow13_compute_median_instance(.CLK(CLK), .RESET(bus_66881bff_), 
  .GO(compute_median_go), .port_4549bd5c_(bus_76271ce4_), .port_334c6c69_(bus_4cc1a91e_), 
  .port_5fe55fd0_(bus_79467be5_), .port_4e6a8129_(bus_76271ce4_), .port_7bad8a3d_(bus_5a7462db_), 
  .DONE(medianRow13_compute_median_instance_DONE), .RESULT(compute_median), .RESULT_u2294(compute_median_u686), 
  .RESULT_u2301(compute_median_u687), .RESULT_u2302(compute_median_u688), .RESULT_u2303(compute_median_u689), 
  .RESULT_u2304(compute_median_u690), .RESULT_u2295(compute_median_u691), .RESULT_u2296(compute_median_u692), 
  .RESULT_u2297(compute_median_u693), .RESULT_u2298(compute_median_u694), .RESULT_u2299(compute_median_u695), 
  .RESULT_u2300(compute_median_u696), .RESULT_u2305(compute_median_u697), .RESULT_u2306(compute_median_u698), 
  .RESULT_u2307(compute_median_u699));
medianRow13_simplememoryreferee_581534fa_ medianRow13_simplememoryreferee_581534fa__1(.bus_05af811f_(CLK), 
  .bus_03e6381b_(bus_66881bff_), .bus_5d3841b0_(bus_3ae5bab1_), .bus_08e46347_(bus_7fade1e4_), 
  .bus_25bf663b_(compute_median_u691), .bus_0db2645e_(compute_median_u692), .bus_0d17c9ed_(3'h1), 
  .bus_3f884be2_(bus_3f884be2_), .bus_5565d9f1_(bus_5565d9f1_), .bus_039ae53e_(bus_039ae53e_), 
  .bus_4d9fd1fe_(bus_4d9fd1fe_), .bus_64070445_(bus_64070445_), .bus_79467be5_(bus_79467be5_), 
  .bus_4cc1a91e_(bus_4cc1a91e_));
assign bus_73218e0e_=medianRow13_receive_instance_DONE&{1{medianRow13_receive_instance_DONE}};
medianRow13_Kicker_61 medianRow13_Kicker_61_1(.CLK(CLK), .RESET(bus_66881bff_), 
  .bus_2fe1579d_(bus_2fe1579d_));
medianRow13_receive medianRow13_receive_instance(.CLK(CLK), .RESET(bus_66881bff_), 
  .GO(receive_go), .port_7e463996_(bus_5c23c6dc_), .port_049c315f_(bus_54176535_), 
  .port_47edc46e_(in1_DATA), .DONE(medianRow13_receive_instance_DONE), .RESULT(receive), 
  .RESULT_u2308(receive_u294), .RESULT_u2309(receive_u295), .RESULT_u2310(receive_u296), 
  .RESULT_u2311(receive_u297), .RESULT_u2312(receive_u298), .RESULT_u2313(receive_u299));
assign bus_0050164c_=medianRow13_compute_median_instance_DONE&{1{medianRow13_compute_median_instance_DONE}};
medianRow13_structuralmemory_2c76e809_ medianRow13_structuralmemory_2c76e809__1(.CLK_u95(CLK), 
  .bus_2f85e247_(bus_66881bff_), .bus_3ca45016_(bus_5565d9f1_), .bus_568a65b9_(3'h1), 
  .bus_4ac85576_(bus_4d9fd1fe_), .bus_641f6cf0_(bus_15f53daf_), .bus_50d77e1e_(3'h1), 
  .bus_4ed55c07_(bus_0bbf1d7b_), .bus_2d16d021_(bus_54e28a5d_), .bus_237d7b4a_(bus_70d38c83_), 
  .bus_7fade1e4_(bus_7fade1e4_), .bus_3ae5bab1_(bus_3ae5bab1_), .bus_65ef3584_(bus_65ef3584_), 
  .bus_560c2387_(bus_560c2387_));
endmodule



module medianRow13_endianswapper_697dfc5c_(endianswapper_697dfc5c_in, endianswapper_697dfc5c_out);
input	[31:0]	endianswapper_697dfc5c_in;
output	[31:0]	endianswapper_697dfc5c_out;
assign endianswapper_697dfc5c_out=endianswapper_697dfc5c_in;
endmodule



module medianRow13_endianswapper_2e24dfce_(endianswapper_2e24dfce_in, endianswapper_2e24dfce_out);
input	[31:0]	endianswapper_2e24dfce_in;
output	[31:0]	endianswapper_2e24dfce_out;
assign endianswapper_2e24dfce_out=endianswapper_2e24dfce_in;
endmodule



module medianRow13_stateVar_i(bus_67535e14_, bus_6966df10_, bus_28317430_, bus_0a2ee72e_, bus_5d6e1582_, bus_0c2155af_, bus_5c23c6dc_);
input		bus_67535e14_;
input		bus_6966df10_;
input		bus_28317430_;
input	[31:0]	bus_0a2ee72e_;
input		bus_5d6e1582_;
input	[31:0]	bus_0c2155af_;
output	[31:0]	bus_5c23c6dc_;
wire	[31:0]	endianswapper_697dfc5c_out;
wire		or_303625c3_u0;
reg	[31:0]	stateVar_i_u51=32'h0;
wire	[31:0]	mux_01f9628e_u0;
wire	[31:0]	endianswapper_2e24dfce_out;
medianRow13_endianswapper_697dfc5c_ medianRow13_endianswapper_697dfc5c__1(.endianswapper_697dfc5c_in(mux_01f9628e_u0), 
  .endianswapper_697dfc5c_out(endianswapper_697dfc5c_out));
assign or_303625c3_u0=bus_28317430_|bus_5d6e1582_;
always @(posedge bus_67535e14_ or posedge bus_6966df10_)
begin
if (bus_6966df10_)
stateVar_i_u51<=32'h0;
else if (or_303625c3_u0)
stateVar_i_u51<=endianswapper_697dfc5c_out;
end
assign mux_01f9628e_u0=(bus_28317430_)?bus_0a2ee72e_:32'h0;
medianRow13_endianswapper_2e24dfce_ medianRow13_endianswapper_2e24dfce__1(.endianswapper_2e24dfce_in(stateVar_i_u51), 
  .endianswapper_2e24dfce_out(endianswapper_2e24dfce_out));
assign bus_5c23c6dc_=endianswapper_2e24dfce_out;
endmodule



module medianRow13_globalreset_physical_3c277d63_(bus_1b31f810_, bus_48bdb164_, bus_66881bff_);
input		bus_1b31f810_;
input		bus_48bdb164_;
output		bus_66881bff_;
reg		final_u61=1'h1;
wire		and_394194e4_u0;
wire		not_0d88517e_u0;
reg		glitch_u61=1'h0;
reg		sample_u61=1'h0;
reg		cross_u61=1'h0;
wire		or_0bcd7592_u0;
always @(posedge bus_1b31f810_)
begin
final_u61<=not_0d88517e_u0;
end
assign bus_66881bff_=or_0bcd7592_u0;
assign and_394194e4_u0=cross_u61&glitch_u61;
assign not_0d88517e_u0=~and_394194e4_u0;
always @(posedge bus_1b31f810_)
begin
glitch_u61<=cross_u61;
end
always @(posedge bus_1b31f810_)
begin
sample_u61<=1'h1;
end
always @(posedge bus_1b31f810_)
begin
cross_u61<=sample_u61;
end
assign or_0bcd7592_u0=bus_48bdb164_|final_u61;
endmodule



module medianRow13_scheduler(CLK, RESET, GO, port_6db0638b_, port_01cb707e_, port_30d32211_, port_378f0696_, port_525c72b7_, port_3a33183a_, RESULT, RESULT_u2291, RESULT_u2292, RESULT_u2293, DONE);
input		CLK;
input		RESET;
input		GO;
input		port_6db0638b_;
input	[31:0]	port_01cb707e_;
input		port_30d32211_;
input		port_378f0696_;
input		port_525c72b7_;
input		port_3a33183a_;
output		RESULT;
output		RESULT_u2291;
output		RESULT_u2292;
output		RESULT_u2293;
output		DONE;
wire		lessThan;
wire signed	[31:0]	lessThan_a_signed;
wire signed	[31:0]	lessThan_b_signed;
wire signed	[31:0]	equals_a_signed;
wire signed	[31:0]	equals_b_signed;
wire		equals;
wire signed	[31:0]	equals_u240_b_signed;
wire signed	[31:0]	equals_u240_a_signed;
wire		equals_u240;
wire		not_u909_u0;
wire		and_u3908_u0;
wire		and_u3909_u0;
wire		andOp;
wire		and_u3910_u0;
wire		not_u910_u0;
wire		and_u3911_u0;
wire		simplePinWrite;
wire		and_u3912_u0;
wire		and_u3913_u0;
wire signed	[31:0]	equals_u241_b_signed;
wire signed	[31:0]	equals_u241_a_signed;
wire		equals_u241;
wire		not_u911_u0;
wire		and_u3914_u0;
wire		and_u3915_u0;
wire		andOp_u83;
wire		not_u912_u0;
wire		and_u3916_u0;
wire		and_u3917_u0;
wire		simplePinWrite_u634;
wire		and_u3918_u0;
wire		and_u3919_u0;
wire		not_u913_u0;
wire		and_u3920_u0;
wire		not_u914_u0;
wire		and_u3921_u0;
wire		simplePinWrite_u635;
wire		and_u3922_u0;
wire		and_u3923_u0;
wire		or_u1498_u0;
reg		and_delayed_u397=1'h0;
wire		or_u1499_u0;
wire		and_u3924_u0;
wire		and_u3925_u0;
reg		and_delayed_u398_u0=1'h0;
wire		and_u3926_u0;
reg		and_delayed_u399_u0=1'h0;
wire		and_u3927_u0;
wire		or_u1500_u0;
wire		mux_u1981;
wire		or_u1501_u0;
wire		and_u3928_u0;
reg		and_delayed_u400_u0=1'h0;
wire		or_u1502_u0;
wire		and_u3929_u0;
wire		receive_go_merge;
wire		or_u1503_u0;
wire		mux_u1982_u0;
wire		and_u3930_u0;
reg		loopControl_u102=1'h0;
wire		or_u1504_u0;
reg		syncEnable_u1487=1'h0;
reg		reg_3937b747_u0=1'h0;
wire		or_u1505_u0;
wire		mux_u1983_u0;
reg		reg_3937b747_result_delayed_u0=1'h0;
assign lessThan_a_signed=port_01cb707e_;
assign lessThan_b_signed=32'h65;
assign lessThan=lessThan_a_signed<lessThan_b_signed;
assign equals_a_signed=port_01cb707e_;
assign equals_b_signed=32'h65;
assign equals=equals_a_signed==equals_b_signed;
assign equals_u240_a_signed={31'b0, port_6db0638b_};
assign equals_u240_b_signed=32'h0;
assign equals_u240=equals_u240_a_signed==equals_u240_b_signed;
assign not_u909_u0=~equals_u240;
assign and_u3908_u0=and_u3930_u0&equals_u240;
assign and_u3909_u0=and_u3930_u0&not_u909_u0;
assign andOp=lessThan&port_378f0696_;
assign and_u3910_u0=and_u3913_u0&andOp;
assign not_u910_u0=~andOp;
assign and_u3911_u0=and_u3913_u0&not_u910_u0;
assign simplePinWrite=and_u3912_u0&{1{and_u3912_u0}};
assign and_u3912_u0=and_u3910_u0&and_u3913_u0;
assign and_u3913_u0=and_u3908_u0&and_u3930_u0;
assign equals_u241_a_signed={31'b0, port_6db0638b_};
assign equals_u241_b_signed=32'h1;
assign equals_u241=equals_u241_a_signed==equals_u241_b_signed;
assign not_u911_u0=~equals_u241;
assign and_u3914_u0=and_u3930_u0&not_u911_u0;
assign and_u3915_u0=and_u3930_u0&equals_u241;
assign andOp_u83=lessThan&port_378f0696_;
assign not_u912_u0=~andOp_u83;
assign and_u3916_u0=and_u3929_u0&not_u912_u0;
assign and_u3917_u0=and_u3929_u0&andOp_u83;
assign simplePinWrite_u634=and_u3926_u0&{1{and_u3926_u0}};
assign and_u3918_u0=and_u3927_u0&not_u913_u0;
assign and_u3919_u0=and_u3927_u0&equals;
assign not_u913_u0=~equals;
assign and_u3920_u0=and_u3925_u0&not_u914_u0;
assign not_u914_u0=~port_525c72b7_;
assign and_u3921_u0=and_u3925_u0&port_525c72b7_;
assign simplePinWrite_u635=and_u3922_u0&{1{and_u3922_u0}};
assign and_u3922_u0=and_u3921_u0&and_u3925_u0;
assign and_u3923_u0=and_u3920_u0&and_u3925_u0;
assign or_u1498_u0=and_delayed_u397|port_3a33183a_;
always @(posedge CLK or posedge RESET)
begin
if (RESET)
and_delayed_u397<=1'h0;
else and_delayed_u397<=and_u3923_u0;
end
assign or_u1499_u0=and_delayed_u398_u0|or_u1498_u0;
assign and_u3924_u0=and_u3918_u0&and_u3927_u0;
assign and_u3925_u0=and_u3919_u0&and_u3927_u0;
always @(posedge CLK or posedge RESET)
begin
if (RESET)
and_delayed_u398_u0<=1'h0;
else and_delayed_u398_u0<=and_u3924_u0;
end
assign and_u3926_u0=and_u3917_u0&and_u3929_u0;
always @(posedge CLK or posedge RESET)
begin
if (RESET)
and_delayed_u399_u0<=1'h0;
else and_delayed_u399_u0<=and_u3926_u0;
end
assign and_u3927_u0=and_u3916_u0&and_u3929_u0;
assign or_u1500_u0=or_u1499_u0|and_delayed_u399_u0;
assign mux_u1981=(and_u3926_u0)?1'h1:1'h0;
assign or_u1501_u0=and_u3926_u0|and_u3922_u0;
assign and_u3928_u0=and_u3914_u0&and_u3930_u0;
always @(posedge CLK or posedge RESET)
begin
if (RESET)
and_delayed_u400_u0<=1'h0;
else and_delayed_u400_u0<=and_u3928_u0;
end
assign or_u1502_u0=or_u1500_u0|and_delayed_u400_u0;
assign and_u3929_u0=and_u3915_u0&and_u3930_u0;
assign receive_go_merge=simplePinWrite|simplePinWrite_u634;
assign or_u1503_u0=and_u3912_u0|or_u1501_u0;
assign mux_u1982_u0=(and_u3912_u0)?1'h1:mux_u1981;
assign and_u3930_u0=or_u1504_u0&or_u1504_u0;
always @(posedge CLK or posedge syncEnable_u1487)
begin
if (syncEnable_u1487)
loopControl_u102<=1'h0;
else loopControl_u102<=or_u1502_u0;
end
assign or_u1504_u0=reg_3937b747_result_delayed_u0|loopControl_u102;
always @(posedge CLK)
begin
if (reg_3937b747_result_delayed_u0)
syncEnable_u1487<=RESET;
end
always @(posedge CLK or posedge RESET)
begin
if (RESET)
reg_3937b747_u0<=1'h0;
else reg_3937b747_u0<=GO;
end
assign or_u1505_u0=GO|or_u1503_u0;
assign mux_u1983_u0=(GO)?1'h0:mux_u1982_u0;
always @(posedge CLK or posedge RESET)
begin
if (RESET)
reg_3937b747_result_delayed_u0<=1'h0;
else reg_3937b747_result_delayed_u0<=reg_3937b747_u0;
end
assign RESULT=or_u1505_u0;
assign RESULT_u2291=mux_u1983_u0;
assign RESULT_u2292=simplePinWrite_u635;
assign RESULT_u2293=receive_go_merge;
assign DONE=1'h0;
endmodule



module medianRow13_endianswapper_1ed47400_(endianswapper_1ed47400_in, endianswapper_1ed47400_out);
input		endianswapper_1ed47400_in;
output		endianswapper_1ed47400_out;
assign endianswapper_1ed47400_out=endianswapper_1ed47400_in;
endmodule



module medianRow13_endianswapper_2be8af31_(endianswapper_2be8af31_in, endianswapper_2be8af31_out);
input		endianswapper_2be8af31_in;
output		endianswapper_2be8af31_out;
assign endianswapper_2be8af31_out=endianswapper_2be8af31_in;
endmodule



module medianRow13_stateVar_fsmState_medianRow13(bus_1d3c6a38_, bus_602b6445_, bus_7c42b9f2_, bus_396324bb_, bus_70cf810e_);
input		bus_1d3c6a38_;
input		bus_602b6445_;
input		bus_7c42b9f2_;
input		bus_396324bb_;
output		bus_70cf810e_;
wire		endianswapper_1ed47400_out;
reg		stateVar_fsmState_medianRow13_u2=1'h0;
wire		endianswapper_2be8af31_out;
medianRow13_endianswapper_1ed47400_ medianRow13_endianswapper_1ed47400__1(.endianswapper_1ed47400_in(bus_396324bb_), 
  .endianswapper_1ed47400_out(endianswapper_1ed47400_out));
always @(posedge bus_1d3c6a38_ or posedge bus_602b6445_)
begin
if (bus_602b6445_)
stateVar_fsmState_medianRow13_u2<=1'h0;
else if (bus_7c42b9f2_)
stateVar_fsmState_medianRow13_u2<=endianswapper_1ed47400_out;
end
assign bus_70cf810e_=endianswapper_2be8af31_out;
medianRow13_endianswapper_2be8af31_ medianRow13_endianswapper_2be8af31__1(.endianswapper_2be8af31_in(stateVar_fsmState_medianRow13_u2), 
  .endianswapper_2be8af31_out(endianswapper_2be8af31_out));
endmodule



module medianRow13_simplememoryreferee_49ed623a_(bus_045deef4_, bus_6989ef5f_, bus_6a0202d4_, bus_0d6400bc_, bus_56689ef6_, bus_10203f31_, bus_3b7102f6_, bus_18979418_, bus_35258787_, bus_17bceaca_, bus_1c74e322_, bus_00571e15_, bus_6cf3ffb6_, bus_70d38c83_, bus_15f53daf_, bus_54e28a5d_, bus_0bbf1d7b_, bus_3e71dc68_, bus_54176535_, bus_5a7462db_, bus_76271ce4_);
input		bus_045deef4_;
input		bus_6989ef5f_;
input		bus_6a0202d4_;
input	[31:0]	bus_0d6400bc_;
input		bus_56689ef6_;
input	[31:0]	bus_10203f31_;
input	[31:0]	bus_3b7102f6_;
input	[2:0]	bus_18979418_;
input		bus_35258787_;
input		bus_17bceaca_;
input	[31:0]	bus_1c74e322_;
input	[31:0]	bus_00571e15_;
input	[2:0]	bus_6cf3ffb6_;
output	[31:0]	bus_70d38c83_;
output	[31:0]	bus_15f53daf_;
output		bus_54e28a5d_;
output		bus_0bbf1d7b_;
output	[2:0]	bus_3e71dc68_;
output		bus_54176535_;
output	[31:0]	bus_5a7462db_;
output		bus_76271ce4_;
reg		done_qual_u226=1'h0;
wire		and_447813b8_u0;
wire		and_0d258fc6_u0;
wire		or_39c531e1_u0;
wire		or_30759270_u0;
wire		or_68cf78c8_u0;
wire	[31:0]	mux_657c3cd5_u0;
wire		not_782b0907_u0;
wire		or_793dee2e_u0;
wire		not_0452a564_u0;
wire	[31:0]	mux_32bd00a4_u0;
reg		done_qual_u227_u0=1'h0;
wire		or_7532c88c_u0;
assign bus_70d38c83_=mux_32bd00a4_u0;
assign bus_15f53daf_=mux_657c3cd5_u0;
assign bus_54e28a5d_=or_30759270_u0;
assign bus_0bbf1d7b_=or_68cf78c8_u0;
assign bus_3e71dc68_=3'h1;
assign bus_54176535_=and_447813b8_u0;
assign bus_5a7462db_=bus_0d6400bc_;
assign bus_76271ce4_=and_0d258fc6_u0;
always @(posedge bus_045deef4_)
begin
if (bus_6989ef5f_)
done_qual_u226<=1'h0;
else done_qual_u226<=bus_56689ef6_;
end
assign and_447813b8_u0=or_39c531e1_u0&bus_6a0202d4_;
assign and_0d258fc6_u0=or_7532c88c_u0&bus_6a0202d4_;
assign or_39c531e1_u0=bus_56689ef6_|done_qual_u226;
assign or_30759270_u0=bus_56689ef6_|bus_17bceaca_;
assign or_68cf78c8_u0=bus_56689ef6_|or_793dee2e_u0;
assign mux_657c3cd5_u0=(bus_56689ef6_)?bus_3b7102f6_:bus_00571e15_;
assign not_782b0907_u0=~bus_6a0202d4_;
assign or_793dee2e_u0=bus_35258787_|bus_17bceaca_;
assign not_0452a564_u0=~bus_6a0202d4_;
assign mux_32bd00a4_u0=(bus_56689ef6_)?{24'b0, bus_10203f31_[7:0]}:bus_1c74e322_;
always @(posedge bus_045deef4_)
begin
if (bus_6989ef5f_)
done_qual_u227_u0<=1'h0;
else done_qual_u227_u0<=or_793dee2e_u0;
end
assign or_7532c88c_u0=or_793dee2e_u0|done_qual_u227_u0;
endmodule



module medianRow13_compute_median(CLK, RESET, GO, port_334c6c69_, port_5fe55fd0_, port_4e6a8129_, port_7bad8a3d_, port_4549bd5c_, RESULT, RESULT_u2294, RESULT_u2295, RESULT_u2296, RESULT_u2297, RESULT_u2298, RESULT_u2299, RESULT_u2300, RESULT_u2301, RESULT_u2302, RESULT_u2303, RESULT_u2304, RESULT_u2305, RESULT_u2306, RESULT_u2307, DONE);
input		CLK;
input		RESET;
input		GO;
input		port_334c6c69_;
input	[31:0]	port_5fe55fd0_;
input		port_4e6a8129_;
input	[31:0]	port_7bad8a3d_;
input		port_4549bd5c_;
output		RESULT;
output	[31:0]	RESULT_u2294;
output		RESULT_u2295;
output	[31:0]	RESULT_u2296;
output	[2:0]	RESULT_u2297;
output		RESULT_u2298;
output	[31:0]	RESULT_u2299;
output	[2:0]	RESULT_u2300;
output		RESULT_u2301;
output	[31:0]	RESULT_u2302;
output	[31:0]	RESULT_u2303;
output	[2:0]	RESULT_u2304;
output	[15:0]	RESULT_u2305;
output		RESULT_u2306;
output	[7:0]	RESULT_u2307;
output		DONE;
wire		lessThan;
wire signed	[32:0]	lessThan_b_signed;
wire signed	[32:0]	lessThan_a_signed;
wire		not_u915_u0;
wire		and_u3931_u0;
wire		and_u3932_u0;
wire	[31:0]	add;
wire		and_u3933_u0;
wire	[31:0]	add_u705;
wire	[31:0]	add_u706;
wire		and_u3934_u0;
wire signed	[31:0]	greaterThan_a_signed;
wire signed	[31:0]	greaterThan_b_signed;
wire		greaterThan;
wire		and_u3935_u0;
wire		and_u3936_u0;
wire		not_u916_u0;
wire	[31:0]	add_u707;
wire		and_u3937_u0;
wire	[31:0]	add_u708;
wire	[31:0]	add_u709;
wire		and_u3938_u0;
wire	[31:0]	add_u710;
wire		or_u1506_u0;
reg		reg_1ea29ca7_u0=1'h0;
wire		and_u3939_u0;
wire	[31:0]	add_u711;
wire	[31:0]	add_u712;
wire		and_u3940_u0;
wire		or_u1507_u0;
reg		reg_6b35b1ce_u0=1'h0;
reg	[31:0]	syncEnable_u1488=32'h0;
reg	[31:0]	syncEnable_u1489_u0=32'h0;
reg	[31:0]	syncEnable_u1490_u0=32'h0;
reg	[31:0]	syncEnable_u1491_u0=32'h0;
reg	[31:0]	syncEnable_u1492_u0=32'h0;
reg		reg_6c47629d_u0=1'h0;
reg		block_GO_delayed_u104=1'h0;
wire	[31:0]	mux_u1984;
wire		or_u1508_u0;
wire	[31:0]	mux_u1985_u0;
reg	[31:0]	syncEnable_u1493_u0=32'h0;
reg	[31:0]	syncEnable_u1494_u0=32'h0;
reg		reg_2eb9055a_u0=1'h0;
reg		syncEnable_u1495_u0=1'h0;
reg	[31:0]	syncEnable_u1496_u0=32'h0;
wire	[31:0]	mux_u1986_u0;
wire		and_u3941_u0;
wire		and_u3942_u0;
reg		reg_125fb26d_u0=1'h0;
reg		and_delayed_u401=1'h0;
reg		reg_2eb9055a_result_delayed_u0=1'h0;
wire		mux_u1987_u0;
reg		reg_2eb9055a_result_delayed_result_delayed_u0=1'h0;
wire		or_u1509_u0;
wire	[31:0]	mux_u1988_u0;
wire	[31:0]	add_u713;
reg	[31:0]	syncEnable_u1497_u0=32'h0;
reg		block_GO_delayed_u105_u0=1'h0;
reg	[31:0]	syncEnable_u1498_u0=32'h0;
reg	[31:0]	syncEnable_u1499_u0=32'h0;
reg	[31:0]	syncEnable_u1500_u0=32'h0;
reg	[31:0]	syncEnable_u1501_u0=32'h0;
wire	[31:0]	mux_u1989_u0;
wire		or_u1510_u0;
wire	[31:0]	mux_u1990_u0;
wire		or_u1511_u0;
reg		syncEnable_u1502_u0=1'h0;
reg	[31:0]	syncEnable_u1503_u0=32'h0;
reg	[31:0]	syncEnable_u1504_u0=32'h0;
wire		and_u3943_u0;
wire		mux_u1991_u0;
wire	[31:0]	mux_u1992_u0;
wire	[31:0]	mux_u1993_u0;
wire		or_u1512_u0;
wire	[31:0]	mux_u1994_u0;
wire	[31:0]	mux_u1995_u0;
wire	[31:0]	mux_u1996_u0;
wire	[15:0]	add_u714;
wire	[31:0]	latch_0a36a935_out;
reg	[31:0]	latch_0a36a935_reg=32'h0;
reg		reg_0d68cfc3_u0=1'h0;
reg	[15:0]	syncEnable_u1505_u0=16'h0;
reg		latch_6f5edcf2_reg=1'h0;
wire		latch_6f5edcf2_out;
wire	[31:0]	latch_6d4cf300_out;
reg	[31:0]	latch_6d4cf300_reg=32'h0;
reg		scoreboard_7492bf02_reg0=1'h0;
wire		scoreboard_7492bf02_resOr0;
wire		scoreboard_7492bf02_resOr1;
reg		scoreboard_7492bf02_reg1=1'h0;
wire		bus_34c85801_;
wire		scoreboard_7492bf02_and;
wire	[31:0]	latch_206f6035_out;
reg	[31:0]	latch_206f6035_reg=32'h0;
reg	[31:0]	latch_7d809bfa_reg=32'h0;
wire	[31:0]	latch_7d809bfa_out;
wire		and_u3944_u0;
wire		lessThan_u107;
wire	[15:0]	lessThan_u107_b_unsigned;
wire	[15:0]	lessThan_u107_a_unsigned;
wire		andOp;
wire		not_u917_u0;
wire		and_u3945_u0;
wire		and_u3946_u0;
wire		and_u3947_u0;
reg	[31:0]	fbReg_tmp_u49=32'h0;
reg	[31:0]	fbReg_tmp_row_u49=32'h0;
reg	[31:0]	fbReg_tmp_row0_u49=32'h0;
reg		loopControl_u103=1'h0;
reg		syncEnable_u1506_u0=1'h0;
wire	[31:0]	mux_u1997_u0;
reg	[15:0]	fbReg_idx_u49=16'h0;
wire	[31:0]	mux_u1998_u0;
wire		mux_u1999_u0;
reg	[31:0]	fbReg_tmp_row1_u49=32'h0;
wire	[31:0]	mux_u2000_u0;
reg		fbReg_swapped_u49=1'h0;
wire	[31:0]	mux_u2001_u0;
wire		or_u1513_u0;
wire	[15:0]	mux_u2002_u0;
wire		and_u3948_u0;
wire	[15:0]	simplePinWrite;
wire	[7:0]	simplePinWrite_u636;
wire		simplePinWrite_u637;
reg		reg_56508e58_u0=1'h0;
wire		or_u1514_u0;
wire	[31:0]	mux_u2003_u0;
reg		reg_43638b85_u0=1'h0;
assign lessThan_a_signed={1'b0, mux_u1992_u0};
assign lessThan_b_signed=33'h64;
assign lessThan=lessThan_a_signed<lessThan_b_signed;
assign not_u915_u0=~lessThan;
assign and_u3931_u0=or_u1512_u0&lessThan;
assign and_u3932_u0=or_u1512_u0&not_u915_u0;
assign add=mux_u1992_u0+32'h0;
assign and_u3933_u0=and_u3943_u0&port_334c6c69_;
assign add_u705=mux_u1992_u0+32'h1;
assign add_u706=add_u705+32'h0;
assign and_u3934_u0=and_u3943_u0&port_4549bd5c_;
assign greaterThan_a_signed=syncEnable_u1501_u0;
assign greaterThan_b_signed=syncEnable_u1497_u0;
assign greaterThan=greaterThan_a_signed>greaterThan_b_signed;
assign and_u3935_u0=block_GO_delayed_u105_u0&not_u916_u0;
assign and_u3936_u0=block_GO_delayed_u105_u0&greaterThan;
assign not_u916_u0=~greaterThan;
assign add_u707=syncEnable_u1498_u0+32'h0;
assign and_u3937_u0=and_u3941_u0&port_334c6c69_;
assign add_u708=syncEnable_u1498_u0+32'h1;
assign add_u709=add_u708+32'h0;
assign and_u3938_u0=and_u3941_u0&port_4549bd5c_;
assign add_u710=syncEnable_u1498_u0+32'h0;
assign or_u1506_u0=and_u3939_u0|RESET;
always @(posedge CLK or posedge block_GO_delayed_u104 or posedge or_u1506_u0)
begin
if (or_u1506_u0)
reg_1ea29ca7_u0<=1'h0;
else if (block_GO_delayed_u104)
reg_1ea29ca7_u0<=1'h1;
else reg_1ea29ca7_u0<=reg_1ea29ca7_u0;
end
assign and_u3939_u0=reg_1ea29ca7_u0&port_4549bd5c_;
assign add_u711=syncEnable_u1498_u0+32'h1;
assign add_u712=add_u711+32'h0;
assign and_u3940_u0=reg_6b35b1ce_u0&port_4549bd5c_;
assign or_u1507_u0=and_u3940_u0|RESET;
always @(posedge CLK or posedge reg_6c47629d_u0 or posedge or_u1507_u0)
begin
if (or_u1507_u0)
reg_6b35b1ce_u0<=1'h0;
else if (reg_6c47629d_u0)
reg_6b35b1ce_u0<=1'h1;
else reg_6b35b1ce_u0<=reg_6b35b1ce_u0;
end
always @(posedge CLK)
begin
if (and_u3941_u0)
syncEnable_u1488<=port_5fe55fd0_;
end
always @(posedge CLK)
begin
if (and_u3941_u0)
syncEnable_u1489_u0<=add_u712;
end
always @(posedge CLK)
begin
if (and_u3941_u0)
syncEnable_u1490_u0<=port_5fe55fd0_;
end
always @(posedge CLK)
begin
if (and_u3941_u0)
syncEnable_u1491_u0<=add_u710;
end
always @(posedge CLK)
begin
if (and_u3941_u0)
syncEnable_u1492_u0<=port_7bad8a3d_;
end
always @(posedge CLK or posedge RESET)
begin
if (RESET)
reg_6c47629d_u0<=1'h0;
else reg_6c47629d_u0<=block_GO_delayed_u104;
end
always @(posedge CLK or posedge RESET)
begin
if (RESET)
block_GO_delayed_u104<=1'h0;
else block_GO_delayed_u104<=and_u3941_u0;
end
assign mux_u1984=(block_GO_delayed_u104)?syncEnable_u1492_u0:syncEnable_u1490_u0;
assign or_u1508_u0=block_GO_delayed_u104|reg_6c47629d_u0;
assign mux_u1985_u0=({32{block_GO_delayed_u104}}&syncEnable_u1491_u0)|({32{reg_6c47629d_u0}}&syncEnable_u1489_u0)|({32{and_u3941_u0}}&add_u709);
always @(posedge CLK)
begin
if (and_u3941_u0)
syncEnable_u1493_u0<=port_7bad8a3d_;
end
always @(posedge CLK)
begin
if (block_GO_delayed_u105_u0)
syncEnable_u1494_u0<=syncEnable_u1499_u0;
end
always @(posedge CLK or posedge RESET)
begin
if (RESET)
reg_2eb9055a_u0<=1'h0;
else reg_2eb9055a_u0<=and_u3941_u0;
end
always @(posedge CLK)
begin
if (block_GO_delayed_u105_u0)
syncEnable_u1495_u0<=syncEnable_u1502_u0;
end
always @(posedge CLK)
begin
if (block_GO_delayed_u105_u0)
syncEnable_u1496_u0<=syncEnable_u1503_u0;
end
assign mux_u1986_u0=(and_delayed_u401)?syncEnable_u1494_u0:syncEnable_u1493_u0;
assign and_u3941_u0=and_u3936_u0&block_GO_delayed_u105_u0;
assign and_u3942_u0=and_u3935_u0&block_GO_delayed_u105_u0;
always @(posedge CLK or posedge RESET)
begin
if (RESET)
reg_125fb26d_u0<=1'h0;
else reg_125fb26d_u0<=reg_2eb9055a_result_delayed_result_delayed_u0;
end
always @(posedge CLK or posedge RESET)
begin
if (RESET)
and_delayed_u401<=1'h0;
else and_delayed_u401<=and_u3942_u0;
end
always @(posedge CLK or posedge RESET)
begin
if (RESET)
reg_2eb9055a_result_delayed_u0<=1'h0;
else reg_2eb9055a_result_delayed_u0<=reg_2eb9055a_u0;
end
assign mux_u1987_u0=(and_delayed_u401)?syncEnable_u1495_u0:1'h1;
always @(posedge CLK or posedge RESET)
begin
if (RESET)
reg_2eb9055a_result_delayed_result_delayed_u0<=1'h0;
else reg_2eb9055a_result_delayed_result_delayed_u0<=reg_2eb9055a_result_delayed_u0;
end
assign or_u1509_u0=and_delayed_u401|reg_125fb26d_u0;
assign mux_u1988_u0=(and_delayed_u401)?syncEnable_u1496_u0:syncEnable_u1488;
assign add_u713=mux_u1992_u0+32'h1;
always @(posedge CLK)
begin
if (and_u3943_u0)
syncEnable_u1497_u0<=port_7bad8a3d_;
end
always @(posedge CLK or posedge RESET)
begin
if (RESET)
block_GO_delayed_u105_u0<=1'h0;
else block_GO_delayed_u105_u0<=and_u3943_u0;
end
always @(posedge CLK)
begin
if (and_u3943_u0)
syncEnable_u1498_u0<=mux_u1992_u0;
end
always @(posedge CLK)
begin
if (and_u3943_u0)
syncEnable_u1499_u0<=mux_u1993_u0;
end
always @(posedge CLK)
begin
if (and_u3943_u0)
syncEnable_u1500_u0<=add_u713;
end
always @(posedge CLK)
begin
if (and_u3943_u0)
syncEnable_u1501_u0<=port_5fe55fd0_;
end
assign mux_u1989_u0=(and_u3943_u0)?add:add_u707;
assign or_u1510_u0=and_u3943_u0|and_u3941_u0;
assign mux_u1990_u0=({32{or_u1508_u0}}&mux_u1985_u0)|({32{and_u3943_u0}}&add_u706)|({32{and_u3941_u0}}&mux_u1985_u0);
assign or_u1511_u0=and_u3943_u0|and_u3941_u0;
always @(posedge CLK)
begin
if (and_u3943_u0)
syncEnable_u1502_u0<=mux_u1991_u0;
end
always @(posedge CLK)
begin
if (and_u3943_u0)
syncEnable_u1503_u0<=mux_u1994_u0;
end
always @(posedge CLK)
begin
if (or_u1512_u0)
syncEnable_u1504_u0<=mux_u1995_u0;
end
assign and_u3943_u0=and_u3931_u0&or_u1512_u0;
assign mux_u1991_u0=(and_u3944_u0)?1'h0:mux_u1987_u0;
assign mux_u1992_u0=(and_u3944_u0)?32'h0:syncEnable_u1500_u0;
assign mux_u1993_u0=(and_u3944_u0)?mux_u1997_u0:mux_u1986_u0;
assign or_u1512_u0=and_u3944_u0|or_u1509_u0;
assign mux_u1994_u0=(and_u3944_u0)?mux_u2001_u0:mux_u1988_u0;
assign mux_u1995_u0=(and_u3944_u0)?mux_u2000_u0:syncEnable_u1501_u0;
assign mux_u1996_u0=(and_u3944_u0)?mux_u1998_u0:syncEnable_u1497_u0;
assign add_u714=mux_u2002_u0+16'h1;
assign latch_0a36a935_out=(or_u1509_u0)?mux_u1988_u0:latch_0a36a935_reg;
always @(posedge CLK)
begin
if (or_u1509_u0)
latch_0a36a935_reg<=mux_u1988_u0;
end
always @(posedge CLK or posedge RESET)
begin
if (RESET)
reg_0d68cfc3_u0<=1'h0;
else reg_0d68cfc3_u0<=or_u1509_u0;
end
always @(posedge CLK)
begin
if (and_u3944_u0)
syncEnable_u1505_u0<=add_u714;
end
always @(posedge CLK)
begin
if (or_u1509_u0)
latch_6f5edcf2_reg<=mux_u1987_u0;
end
assign latch_6f5edcf2_out=(or_u1509_u0)?mux_u1987_u0:latch_6f5edcf2_reg;
assign latch_6d4cf300_out=(or_u1509_u0)?syncEnable_u1497_u0:latch_6d4cf300_reg;
always @(posedge CLK)
begin
if (or_u1509_u0)
latch_6d4cf300_reg<=syncEnable_u1497_u0;
end
always @(posedge CLK)
begin
if (bus_34c85801_)
scoreboard_7492bf02_reg0<=1'h0;
else if (reg_0d68cfc3_u0)
scoreboard_7492bf02_reg0<=1'h1;
else scoreboard_7492bf02_reg0<=scoreboard_7492bf02_reg0;
end
assign scoreboard_7492bf02_resOr0=reg_0d68cfc3_u0|scoreboard_7492bf02_reg0;
assign scoreboard_7492bf02_resOr1=or_u1509_u0|scoreboard_7492bf02_reg1;
always @(posedge CLK)
begin
if (bus_34c85801_)
scoreboard_7492bf02_reg1<=1'h0;
else if (or_u1509_u0)
scoreboard_7492bf02_reg1<=1'h1;
else scoreboard_7492bf02_reg1<=scoreboard_7492bf02_reg1;
end
assign bus_34c85801_=scoreboard_7492bf02_and|RESET;
assign scoreboard_7492bf02_and=scoreboard_7492bf02_resOr0&scoreboard_7492bf02_resOr1;
assign latch_206f6035_out=(or_u1509_u0)?syncEnable_u1504_u0:latch_206f6035_reg;
always @(posedge CLK)
begin
if (or_u1509_u0)
latch_206f6035_reg<=syncEnable_u1504_u0;
end
always @(posedge CLK)
begin
if (or_u1509_u0)
latch_7d809bfa_reg<=mux_u1986_u0;
end
assign latch_7d809bfa_out=(or_u1509_u0)?mux_u1986_u0:latch_7d809bfa_reg;
assign and_u3944_u0=and_u3946_u0&or_u1513_u0;
assign lessThan_u107_a_unsigned=mux_u2002_u0;
assign lessThan_u107_b_unsigned=16'h65;
assign lessThan_u107=lessThan_u107_a_unsigned<lessThan_u107_b_unsigned;
assign andOp=lessThan_u107&mux_u1999_u0;
assign not_u917_u0=~andOp;
assign and_u3945_u0=or_u1513_u0&not_u917_u0;
assign and_u3946_u0=or_u1513_u0&andOp;
assign and_u3947_u0=and_u3945_u0&or_u1513_u0;
always @(posedge CLK)
begin
if (scoreboard_7492bf02_and)
fbReg_tmp_u49<=latch_0a36a935_out;
end
always @(posedge CLK)
begin
if (scoreboard_7492bf02_and)
fbReg_tmp_row_u49<=latch_206f6035_out;
end
always @(posedge CLK)
begin
if (scoreboard_7492bf02_and)
fbReg_tmp_row0_u49<=latch_6d4cf300_out;
end
always @(posedge CLK or posedge syncEnable_u1506_u0)
begin
if (syncEnable_u1506_u0)
loopControl_u103<=1'h0;
else loopControl_u103<=scoreboard_7492bf02_and;
end
always @(posedge CLK)
begin
if (GO)
syncEnable_u1506_u0<=RESET;
end
assign mux_u1997_u0=(loopControl_u103)?fbReg_tmp_row1_u49:32'h0;
always @(posedge CLK)
begin
if (scoreboard_7492bf02_and)
fbReg_idx_u49<=syncEnable_u1505_u0;
end
assign mux_u1998_u0=(loopControl_u103)?fbReg_tmp_row0_u49:32'h0;
assign mux_u1999_u0=(loopControl_u103)?fbReg_swapped_u49:1'h0;
always @(posedge CLK)
begin
if (scoreboard_7492bf02_and)
fbReg_tmp_row1_u49<=latch_7d809bfa_out;
end
assign mux_u2000_u0=(loopControl_u103)?fbReg_tmp_row_u49:32'h0;
always @(posedge CLK)
begin
if (scoreboard_7492bf02_and)
fbReg_swapped_u49<=latch_6f5edcf2_out;
end
assign mux_u2001_u0=(loopControl_u103)?fbReg_tmp_u49:32'h0;
assign or_u1513_u0=loopControl_u103|GO;
assign mux_u2002_u0=(loopControl_u103)?fbReg_idx_u49:16'h0;
assign and_u3948_u0=reg_43638b85_u0&port_334c6c69_;
assign simplePinWrite=16'h1&{16{1'h1}};
assign simplePinWrite_u636=port_5fe55fd0_[7:0];
assign simplePinWrite_u637=reg_43638b85_u0&{1{reg_43638b85_u0}};
always @(posedge CLK or posedge RESET)
begin
if (RESET)
reg_56508e58_u0<=1'h0;
else reg_56508e58_u0<=reg_43638b85_u0;
end
assign or_u1514_u0=or_u1510_u0|reg_43638b85_u0;
assign mux_u2003_u0=(or_u1510_u0)?mux_u1989_u0:32'h32;
always @(posedge CLK or posedge RESET)
begin
if (RESET)
reg_43638b85_u0<=1'h0;
else reg_43638b85_u0<=and_u3947_u0;
end
assign RESULT=GO;
assign RESULT_u2294=32'h0;
assign RESULT_u2295=or_u1514_u0;
assign RESULT_u2296=mux_u2003_u0;
assign RESULT_u2297=3'h1;
assign RESULT_u2298=or_u1511_u0;
assign RESULT_u2299=mux_u1990_u0;
assign RESULT_u2300=3'h1;
assign RESULT_u2301=or_u1508_u0;
assign RESULT_u2302=mux_u1990_u0;
assign RESULT_u2303=mux_u1984;
assign RESULT_u2304=3'h1;
assign RESULT_u2305=simplePinWrite;
assign RESULT_u2306=simplePinWrite_u637;
assign RESULT_u2307=simplePinWrite_u636;
assign DONE=reg_56508e58_u0;
endmodule



module medianRow13_simplememoryreferee_581534fa_(bus_05af811f_, bus_03e6381b_, bus_5d3841b0_, bus_08e46347_, bus_25bf663b_, bus_0db2645e_, bus_0d17c9ed_, bus_3f884be2_, bus_5565d9f1_, bus_039ae53e_, bus_4d9fd1fe_, bus_64070445_, bus_79467be5_, bus_4cc1a91e_);
input		bus_05af811f_;
input		bus_03e6381b_;
input		bus_5d3841b0_;
input	[31:0]	bus_08e46347_;
input		bus_25bf663b_;
input	[31:0]	bus_0db2645e_;
input	[2:0]	bus_0d17c9ed_;
output	[31:0]	bus_3f884be2_;
output	[31:0]	bus_5565d9f1_;
output		bus_039ae53e_;
output		bus_4d9fd1fe_;
output	[2:0]	bus_64070445_;
output	[31:0]	bus_79467be5_;
output		bus_4cc1a91e_;
assign bus_3f884be2_=32'h0;
assign bus_5565d9f1_=bus_0db2645e_;
assign bus_039ae53e_=1'h0;
assign bus_4d9fd1fe_=bus_25bf663b_;
assign bus_64070445_=3'h1;
assign bus_79467be5_=bus_08e46347_;
assign bus_4cc1a91e_=bus_5d3841b0_;
endmodule



module medianRow13_Kicker_61(CLK, RESET, bus_2fe1579d_);
input		CLK;
input		RESET;
output		bus_2fe1579d_;
wire		bus_679df9ad_;
wire		bus_1df08b35_;
reg		kicker_2=1'h0;
wire		bus_7c107d71_;
reg		kicker_1=1'h0;
reg		kicker_res=1'h0;
wire		bus_414f4e7a_;
assign bus_679df9ad_=bus_414f4e7a_&kicker_1;
assign bus_1df08b35_=kicker_1&bus_414f4e7a_&bus_7c107d71_;
always @(posedge CLK)
begin
kicker_2<=bus_679df9ad_;
end
assign bus_7c107d71_=~kicker_2;
always @(posedge CLK)
begin
kicker_1<=bus_414f4e7a_;
end
assign bus_2fe1579d_=kicker_res;
always @(posedge CLK)
begin
kicker_res<=bus_1df08b35_;
end
assign bus_414f4e7a_=~RESET;
endmodule



module medianRow13_receive(CLK, RESET, GO, port_7e463996_, port_049c315f_, port_47edc46e_, RESULT, RESULT_u2308, RESULT_u2309, RESULT_u2310, RESULT_u2311, RESULT_u2312, RESULT_u2313, DONE);
input		CLK;
input		RESET;
input		GO;
input	[31:0]	port_7e463996_;
input		port_049c315f_;
input	[7:0]	port_47edc46e_;
output		RESULT;
output	[31:0]	RESULT_u2308;
output		RESULT_u2309;
output	[31:0]	RESULT_u2310;
output	[31:0]	RESULT_u2311;
output	[2:0]	RESULT_u2312;
output		RESULT_u2313;
output		DONE;
wire		simplePinWrite;
wire	[31:0]	add;
wire		and_u3949_u0;
reg		reg_5263ad9d_u0=1'h0;
wire		or_u1515_u0;
wire	[31:0]	add_u715;
reg		reg_14359d25_u0=1'h0;
assign simplePinWrite=GO&{1{GO}};
assign add=port_7e463996_+32'h0;
assign and_u3949_u0=reg_5263ad9d_u0&port_049c315f_;
always @(posedge CLK or posedge GO or posedge or_u1515_u0)
begin
if (or_u1515_u0)
reg_5263ad9d_u0<=1'h0;
else if (GO)
reg_5263ad9d_u0<=1'h1;
else reg_5263ad9d_u0<=reg_5263ad9d_u0;
end
assign or_u1515_u0=and_u3949_u0|RESET;
assign add_u715=port_7e463996_+32'h1;
always @(posedge CLK or posedge RESET)
begin
if (RESET)
reg_14359d25_u0<=1'h0;
else reg_14359d25_u0<=GO;
end
assign RESULT=GO;
assign RESULT_u2308=add_u715;
assign RESULT_u2309=GO;
assign RESULT_u2310=add;
assign RESULT_u2311={24'b0, port_47edc46e_};
assign RESULT_u2312=3'h1;
assign RESULT_u2313=simplePinWrite;
assign DONE=reg_14359d25_u0;
endmodule



module medianRow13_forge_memory_101x32_151(CLK, ENA, WEA, DINA, ENB, ADDRA, ADDRB, DOUTA, DOUTB, DONEA, DONEB);
input		CLK;
input		ENA;
input		WEA;
input	[31:0]	DINA;
input		ENB;
input	[31:0]	ADDRA;
input	[31:0]	ADDRB;
output	[31:0]	DOUTA;
output	[31:0]	DOUTB;
output		DONEA;
output		DONEB;
wire		wea_0;
wire	[31:0]	pre_douta_0;
wire	[31:0]	pre_doutb_0;
wire		wea_1;
wire	[31:0]	pre_douta_1;
wire	[31:0]	pre_doutb_1;
reg		wea_done;
reg		rea_done;
reg	[31:0]	mux_outa;
reg	[31:0]	mux_outb;
assign wea_0=WEA&(ADDRA[31:6]==26'h0);
assign wea_1=WEA&(ADDRA[31:6]==26'h1);
always @(posedge CLK)
begin
wea_done<=WEA;
end
assign DONEA=rea_done|wea_done;
always @(posedge CLK)
begin
end
assign DONEB=ENB;
always @(pre_douta_0 or pre_douta_1 or ADDRA)
begin
case (ADDRA[31:6])26'd0:mux_outa=pre_douta_0;
26'd1:mux_outa=pre_douta_1;
default:mux_outa=32'h0;
endcase end
assign DOUTA=mux_outa;
always @(pre_doutb_0 or pre_doutb_1 or ADDRB)
begin
case (ADDRB[31:6])26'd0:mux_outb=pre_doutb_0;
26'd1:mux_outb=pre_doutb_1;
default:mux_outb=32'h0;
endcase end
assign DOUTB=mux_outb;
// Memory array element: COL: 0, ROW: 0
//  Initialization of Dual Port LUT ram now done through explicit
// parameter setting.
RAM64X1D#(.INIT(64'h0000000000000000))RAM64X1D_instance_3136(.WCLK(CLK), .WE(wea_0), 
  .A0(ADDRA[0]), .A1(ADDRA[1]), .A2(ADDRA[2]), .A3(ADDRA[3]), .A4(ADDRA[4]), .A5(ADDRA[5]), 
  .D(DINA[0]), .SPO(pre_douta_0[0]), .DPRA0(ADDRB[0]), .DPRA1(ADDRB[1]), .DPRA2(ADDRB[2]), 
  .DPRA3(ADDRB[3]), .DPRA4(ADDRB[4]), .DPRA5(ADDRB[5]), .DPO(pre_doutb_0[0]));
// Memory array element: COL: 0, ROW: 1
//  Initialization of Dual Port LUT ram now done through explicit
// parameter setting.
RAM64X1D#(.INIT(64'h0000000000000000))RAM64X1D_instance_3137(.WCLK(CLK), .WE(wea_0), 
  .A0(ADDRA[0]), .A1(ADDRA[1]), .A2(ADDRA[2]), .A3(ADDRA[3]), .A4(ADDRA[4]), .A5(ADDRA[5]), 
  .D(DINA[1]), .SPO(pre_douta_0[1]), .DPRA0(ADDRB[0]), .DPRA1(ADDRB[1]), .DPRA2(ADDRB[2]), 
  .DPRA3(ADDRB[3]), .DPRA4(ADDRB[4]), .DPRA5(ADDRB[5]), .DPO(pre_doutb_0[1]));
// Memory array element: COL: 0, ROW: 2
//  Initialization of Dual Port LUT ram now done through explicit
// parameter setting.
RAM64X1D#(.INIT(64'h0000000000000000))RAM64X1D_instance_3138(.WCLK(CLK), .WE(wea_0), 
  .A0(ADDRA[0]), .A1(ADDRA[1]), .A2(ADDRA[2]), .A3(ADDRA[3]), .A4(ADDRA[4]), .A5(ADDRA[5]), 
  .D(DINA[2]), .SPO(pre_douta_0[2]), .DPRA0(ADDRB[0]), .DPRA1(ADDRB[1]), .DPRA2(ADDRB[2]), 
  .DPRA3(ADDRB[3]), .DPRA4(ADDRB[4]), .DPRA5(ADDRB[5]), .DPO(pre_doutb_0[2]));
// Memory array element: COL: 0, ROW: 3
//  Initialization of Dual Port LUT ram now done through explicit
// parameter setting.
RAM64X1D#(.INIT(64'h0000000000000000))RAM64X1D_instance_3139(.WCLK(CLK), .WE(wea_0), 
  .A0(ADDRA[0]), .A1(ADDRA[1]), .A2(ADDRA[2]), .A3(ADDRA[3]), .A4(ADDRA[4]), .A5(ADDRA[5]), 
  .D(DINA[3]), .SPO(pre_douta_0[3]), .DPRA0(ADDRB[0]), .DPRA1(ADDRB[1]), .DPRA2(ADDRB[2]), 
  .DPRA3(ADDRB[3]), .DPRA4(ADDRB[4]), .DPRA5(ADDRB[5]), .DPO(pre_doutb_0[3]));
// Memory array element: COL: 0, ROW: 4
//  Initialization of Dual Port LUT ram now done through explicit
// parameter setting.
RAM64X1D#(.INIT(64'h0000000000000000))RAM64X1D_instance_3140(.WCLK(CLK), .WE(wea_0), 
  .A0(ADDRA[0]), .A1(ADDRA[1]), .A2(ADDRA[2]), .A3(ADDRA[3]), .A4(ADDRA[4]), .A5(ADDRA[5]), 
  .D(DINA[4]), .SPO(pre_douta_0[4]), .DPRA0(ADDRB[0]), .DPRA1(ADDRB[1]), .DPRA2(ADDRB[2]), 
  .DPRA3(ADDRB[3]), .DPRA4(ADDRB[4]), .DPRA5(ADDRB[5]), .DPO(pre_doutb_0[4]));
// Memory array element: COL: 0, ROW: 5
//  Initialization of Dual Port LUT ram now done through explicit
// parameter setting.
RAM64X1D#(.INIT(64'h0000000000000000))RAM64X1D_instance_3141(.WCLK(CLK), .WE(wea_0), 
  .A0(ADDRA[0]), .A1(ADDRA[1]), .A2(ADDRA[2]), .A3(ADDRA[3]), .A4(ADDRA[4]), .A5(ADDRA[5]), 
  .D(DINA[5]), .SPO(pre_douta_0[5]), .DPRA0(ADDRB[0]), .DPRA1(ADDRB[1]), .DPRA2(ADDRB[2]), 
  .DPRA3(ADDRB[3]), .DPRA4(ADDRB[4]), .DPRA5(ADDRB[5]), .DPO(pre_doutb_0[5]));
// Memory array element: COL: 0, ROW: 6
//  Initialization of Dual Port LUT ram now done through explicit
// parameter setting.
RAM64X1D#(.INIT(64'h0000000000000000))RAM64X1D_instance_3142(.WCLK(CLK), .WE(wea_0), 
  .A0(ADDRA[0]), .A1(ADDRA[1]), .A2(ADDRA[2]), .A3(ADDRA[3]), .A4(ADDRA[4]), .A5(ADDRA[5]), 
  .D(DINA[6]), .SPO(pre_douta_0[6]), .DPRA0(ADDRB[0]), .DPRA1(ADDRB[1]), .DPRA2(ADDRB[2]), 
  .DPRA3(ADDRB[3]), .DPRA4(ADDRB[4]), .DPRA5(ADDRB[5]), .DPO(pre_doutb_0[6]));
// Memory array element: COL: 0, ROW: 7
//  Initialization of Dual Port LUT ram now done through explicit
// parameter setting.
RAM64X1D#(.INIT(64'h0000000000000000))RAM64X1D_instance_3143(.WCLK(CLK), .WE(wea_0), 
  .A0(ADDRA[0]), .A1(ADDRA[1]), .A2(ADDRA[2]), .A3(ADDRA[3]), .A4(ADDRA[4]), .A5(ADDRA[5]), 
  .D(DINA[7]), .SPO(pre_douta_0[7]), .DPRA0(ADDRB[0]), .DPRA1(ADDRB[1]), .DPRA2(ADDRB[2]), 
  .DPRA3(ADDRB[3]), .DPRA4(ADDRB[4]), .DPRA5(ADDRB[5]), .DPO(pre_doutb_0[7]));
// Memory array element: COL: 0, ROW: 8
//  Initialization of Dual Port LUT ram now done through explicit
// parameter setting.
RAM64X1D#(.INIT(64'h0000000000000000))RAM64X1D_instance_3144(.WCLK(CLK), .WE(wea_0), 
  .A0(ADDRA[0]), .A1(ADDRA[1]), .A2(ADDRA[2]), .A3(ADDRA[3]), .A4(ADDRA[4]), .A5(ADDRA[5]), 
  .D(DINA[8]), .SPO(pre_douta_0[8]), .DPRA0(ADDRB[0]), .DPRA1(ADDRB[1]), .DPRA2(ADDRB[2]), 
  .DPRA3(ADDRB[3]), .DPRA4(ADDRB[4]), .DPRA5(ADDRB[5]), .DPO(pre_doutb_0[8]));
// Memory array element: COL: 0, ROW: 9
//  Initialization of Dual Port LUT ram now done through explicit
// parameter setting.
RAM64X1D#(.INIT(64'h0000000000000000))RAM64X1D_instance_3145(.WCLK(CLK), .WE(wea_0), 
  .A0(ADDRA[0]), .A1(ADDRA[1]), .A2(ADDRA[2]), .A3(ADDRA[3]), .A4(ADDRA[4]), .A5(ADDRA[5]), 
  .D(DINA[9]), .SPO(pre_douta_0[9]), .DPRA0(ADDRB[0]), .DPRA1(ADDRB[1]), .DPRA2(ADDRB[2]), 
  .DPRA3(ADDRB[3]), .DPRA4(ADDRB[4]), .DPRA5(ADDRB[5]), .DPO(pre_doutb_0[9]));
// Memory array element: COL: 0, ROW: 10
//  Initialization of Dual Port LUT ram now done through explicit
// parameter setting.
RAM64X1D#(.INIT(64'h0000000000000000))RAM64X1D_instance_3146(.WCLK(CLK), .WE(wea_0), 
  .A0(ADDRA[0]), .A1(ADDRA[1]), .A2(ADDRA[2]), .A3(ADDRA[3]), .A4(ADDRA[4]), .A5(ADDRA[5]), 
  .D(DINA[10]), .SPO(pre_douta_0[10]), .DPRA0(ADDRB[0]), .DPRA1(ADDRB[1]), .DPRA2(ADDRB[2]), 
  .DPRA3(ADDRB[3]), .DPRA4(ADDRB[4]), .DPRA5(ADDRB[5]), .DPO(pre_doutb_0[10]));
// Memory array element: COL: 0, ROW: 11
//  Initialization of Dual Port LUT ram now done through explicit
// parameter setting.
RAM64X1D#(.INIT(64'h0000000000000000))RAM64X1D_instance_3147(.WCLK(CLK), .WE(wea_0), 
  .A0(ADDRA[0]), .A1(ADDRA[1]), .A2(ADDRA[2]), .A3(ADDRA[3]), .A4(ADDRA[4]), .A5(ADDRA[5]), 
  .D(DINA[11]), .SPO(pre_douta_0[11]), .DPRA0(ADDRB[0]), .DPRA1(ADDRB[1]), .DPRA2(ADDRB[2]), 
  .DPRA3(ADDRB[3]), .DPRA4(ADDRB[4]), .DPRA5(ADDRB[5]), .DPO(pre_doutb_0[11]));
// Memory array element: COL: 0, ROW: 12
//  Initialization of Dual Port LUT ram now done through explicit
// parameter setting.
RAM64X1D#(.INIT(64'h0000000000000000))RAM64X1D_instance_3148(.WCLK(CLK), .WE(wea_0), 
  .A0(ADDRA[0]), .A1(ADDRA[1]), .A2(ADDRA[2]), .A3(ADDRA[3]), .A4(ADDRA[4]), .A5(ADDRA[5]), 
  .D(DINA[12]), .SPO(pre_douta_0[12]), .DPRA0(ADDRB[0]), .DPRA1(ADDRB[1]), .DPRA2(ADDRB[2]), 
  .DPRA3(ADDRB[3]), .DPRA4(ADDRB[4]), .DPRA5(ADDRB[5]), .DPO(pre_doutb_0[12]));
// Memory array element: COL: 0, ROW: 13
//  Initialization of Dual Port LUT ram now done through explicit
// parameter setting.
RAM64X1D#(.INIT(64'h0000000000000000))RAM64X1D_instance_3149(.WCLK(CLK), .WE(wea_0), 
  .A0(ADDRA[0]), .A1(ADDRA[1]), .A2(ADDRA[2]), .A3(ADDRA[3]), .A4(ADDRA[4]), .A5(ADDRA[5]), 
  .D(DINA[13]), .SPO(pre_douta_0[13]), .DPRA0(ADDRB[0]), .DPRA1(ADDRB[1]), .DPRA2(ADDRB[2]), 
  .DPRA3(ADDRB[3]), .DPRA4(ADDRB[4]), .DPRA5(ADDRB[5]), .DPO(pre_doutb_0[13]));
// Memory array element: COL: 0, ROW: 14
//  Initialization of Dual Port LUT ram now done through explicit
// parameter setting.
RAM64X1D#(.INIT(64'h0000000000000000))RAM64X1D_instance_3150(.WCLK(CLK), .WE(wea_0), 
  .A0(ADDRA[0]), .A1(ADDRA[1]), .A2(ADDRA[2]), .A3(ADDRA[3]), .A4(ADDRA[4]), .A5(ADDRA[5]), 
  .D(DINA[14]), .SPO(pre_douta_0[14]), .DPRA0(ADDRB[0]), .DPRA1(ADDRB[1]), .DPRA2(ADDRB[2]), 
  .DPRA3(ADDRB[3]), .DPRA4(ADDRB[4]), .DPRA5(ADDRB[5]), .DPO(pre_doutb_0[14]));
// Memory array element: COL: 0, ROW: 15
//  Initialization of Dual Port LUT ram now done through explicit
// parameter setting.
RAM64X1D#(.INIT(64'h0000000000000000))RAM64X1D_instance_3151(.WCLK(CLK), .WE(wea_0), 
  .A0(ADDRA[0]), .A1(ADDRA[1]), .A2(ADDRA[2]), .A3(ADDRA[3]), .A4(ADDRA[4]), .A5(ADDRA[5]), 
  .D(DINA[15]), .SPO(pre_douta_0[15]), .DPRA0(ADDRB[0]), .DPRA1(ADDRB[1]), .DPRA2(ADDRB[2]), 
  .DPRA3(ADDRB[3]), .DPRA4(ADDRB[4]), .DPRA5(ADDRB[5]), .DPO(pre_doutb_0[15]));
// Memory array element: COL: 0, ROW: 16
//  Initialization of Dual Port LUT ram now done through explicit
// parameter setting.
RAM64X1D#(.INIT(64'h0000000000000000))RAM64X1D_instance_3152(.WCLK(CLK), .WE(wea_0), 
  .A0(ADDRA[0]), .A1(ADDRA[1]), .A2(ADDRA[2]), .A3(ADDRA[3]), .A4(ADDRA[4]), .A5(ADDRA[5]), 
  .D(DINA[16]), .SPO(pre_douta_0[16]), .DPRA0(ADDRB[0]), .DPRA1(ADDRB[1]), .DPRA2(ADDRB[2]), 
  .DPRA3(ADDRB[3]), .DPRA4(ADDRB[4]), .DPRA5(ADDRB[5]), .DPO(pre_doutb_0[16]));
// Memory array element: COL: 0, ROW: 17
//  Initialization of Dual Port LUT ram now done through explicit
// parameter setting.
RAM64X1D#(.INIT(64'h0000000000000000))RAM64X1D_instance_3153(.WCLK(CLK), .WE(wea_0), 
  .A0(ADDRA[0]), .A1(ADDRA[1]), .A2(ADDRA[2]), .A3(ADDRA[3]), .A4(ADDRA[4]), .A5(ADDRA[5]), 
  .D(DINA[17]), .SPO(pre_douta_0[17]), .DPRA0(ADDRB[0]), .DPRA1(ADDRB[1]), .DPRA2(ADDRB[2]), 
  .DPRA3(ADDRB[3]), .DPRA4(ADDRB[4]), .DPRA5(ADDRB[5]), .DPO(pre_doutb_0[17]));
// Memory array element: COL: 0, ROW: 18
//  Initialization of Dual Port LUT ram now done through explicit
// parameter setting.
RAM64X1D#(.INIT(64'h0000000000000000))RAM64X1D_instance_3154(.WCLK(CLK), .WE(wea_0), 
  .A0(ADDRA[0]), .A1(ADDRA[1]), .A2(ADDRA[2]), .A3(ADDRA[3]), .A4(ADDRA[4]), .A5(ADDRA[5]), 
  .D(DINA[18]), .SPO(pre_douta_0[18]), .DPRA0(ADDRB[0]), .DPRA1(ADDRB[1]), .DPRA2(ADDRB[2]), 
  .DPRA3(ADDRB[3]), .DPRA4(ADDRB[4]), .DPRA5(ADDRB[5]), .DPO(pre_doutb_0[18]));
// Memory array element: COL: 0, ROW: 19
//  Initialization of Dual Port LUT ram now done through explicit
// parameter setting.
RAM64X1D#(.INIT(64'h0000000000000000))RAM64X1D_instance_3155(.WCLK(CLK), .WE(wea_0), 
  .A0(ADDRA[0]), .A1(ADDRA[1]), .A2(ADDRA[2]), .A3(ADDRA[3]), .A4(ADDRA[4]), .A5(ADDRA[5]), 
  .D(DINA[19]), .SPO(pre_douta_0[19]), .DPRA0(ADDRB[0]), .DPRA1(ADDRB[1]), .DPRA2(ADDRB[2]), 
  .DPRA3(ADDRB[3]), .DPRA4(ADDRB[4]), .DPRA5(ADDRB[5]), .DPO(pre_doutb_0[19]));
// Memory array element: COL: 0, ROW: 20
//  Initialization of Dual Port LUT ram now done through explicit
// parameter setting.
RAM64X1D#(.INIT(64'h0000000000000000))RAM64X1D_instance_3156(.WCLK(CLK), .WE(wea_0), 
  .A0(ADDRA[0]), .A1(ADDRA[1]), .A2(ADDRA[2]), .A3(ADDRA[3]), .A4(ADDRA[4]), .A5(ADDRA[5]), 
  .D(DINA[20]), .SPO(pre_douta_0[20]), .DPRA0(ADDRB[0]), .DPRA1(ADDRB[1]), .DPRA2(ADDRB[2]), 
  .DPRA3(ADDRB[3]), .DPRA4(ADDRB[4]), .DPRA5(ADDRB[5]), .DPO(pre_doutb_0[20]));
// Memory array element: COL: 0, ROW: 21
//  Initialization of Dual Port LUT ram now done through explicit
// parameter setting.
RAM64X1D#(.INIT(64'h0000000000000000))RAM64X1D_instance_3157(.WCLK(CLK), .WE(wea_0), 
  .A0(ADDRA[0]), .A1(ADDRA[1]), .A2(ADDRA[2]), .A3(ADDRA[3]), .A4(ADDRA[4]), .A5(ADDRA[5]), 
  .D(DINA[21]), .SPO(pre_douta_0[21]), .DPRA0(ADDRB[0]), .DPRA1(ADDRB[1]), .DPRA2(ADDRB[2]), 
  .DPRA3(ADDRB[3]), .DPRA4(ADDRB[4]), .DPRA5(ADDRB[5]), .DPO(pre_doutb_0[21]));
// Memory array element: COL: 0, ROW: 22
//  Initialization of Dual Port LUT ram now done through explicit
// parameter setting.
RAM64X1D#(.INIT(64'h0000000000000000))RAM64X1D_instance_3158(.WCLK(CLK), .WE(wea_0), 
  .A0(ADDRA[0]), .A1(ADDRA[1]), .A2(ADDRA[2]), .A3(ADDRA[3]), .A4(ADDRA[4]), .A5(ADDRA[5]), 
  .D(DINA[22]), .SPO(pre_douta_0[22]), .DPRA0(ADDRB[0]), .DPRA1(ADDRB[1]), .DPRA2(ADDRB[2]), 
  .DPRA3(ADDRB[3]), .DPRA4(ADDRB[4]), .DPRA5(ADDRB[5]), .DPO(pre_doutb_0[22]));
// Memory array element: COL: 0, ROW: 23
//  Initialization of Dual Port LUT ram now done through explicit
// parameter setting.
RAM64X1D#(.INIT(64'h0000000000000000))RAM64X1D_instance_3159(.WCLK(CLK), .WE(wea_0), 
  .A0(ADDRA[0]), .A1(ADDRA[1]), .A2(ADDRA[2]), .A3(ADDRA[3]), .A4(ADDRA[4]), .A5(ADDRA[5]), 
  .D(DINA[23]), .SPO(pre_douta_0[23]), .DPRA0(ADDRB[0]), .DPRA1(ADDRB[1]), .DPRA2(ADDRB[2]), 
  .DPRA3(ADDRB[3]), .DPRA4(ADDRB[4]), .DPRA5(ADDRB[5]), .DPO(pre_doutb_0[23]));
// Memory array element: COL: 0, ROW: 24
//  Initialization of Dual Port LUT ram now done through explicit
// parameter setting.
RAM64X1D#(.INIT(64'h0000000000000000))RAM64X1D_instance_3160(.WCLK(CLK), .WE(wea_0), 
  .A0(ADDRA[0]), .A1(ADDRA[1]), .A2(ADDRA[2]), .A3(ADDRA[3]), .A4(ADDRA[4]), .A5(ADDRA[5]), 
  .D(DINA[24]), .SPO(pre_douta_0[24]), .DPRA0(ADDRB[0]), .DPRA1(ADDRB[1]), .DPRA2(ADDRB[2]), 
  .DPRA3(ADDRB[3]), .DPRA4(ADDRB[4]), .DPRA5(ADDRB[5]), .DPO(pre_doutb_0[24]));
// Memory array element: COL: 0, ROW: 25
//  Initialization of Dual Port LUT ram now done through explicit
// parameter setting.
RAM64X1D#(.INIT(64'h0000000000000000))RAM64X1D_instance_3161(.WCLK(CLK), .WE(wea_0), 
  .A0(ADDRA[0]), .A1(ADDRA[1]), .A2(ADDRA[2]), .A3(ADDRA[3]), .A4(ADDRA[4]), .A5(ADDRA[5]), 
  .D(DINA[25]), .SPO(pre_douta_0[25]), .DPRA0(ADDRB[0]), .DPRA1(ADDRB[1]), .DPRA2(ADDRB[2]), 
  .DPRA3(ADDRB[3]), .DPRA4(ADDRB[4]), .DPRA5(ADDRB[5]), .DPO(pre_doutb_0[25]));
// Memory array element: COL: 0, ROW: 26
//  Initialization of Dual Port LUT ram now done through explicit
// parameter setting.
RAM64X1D#(.INIT(64'h0000000000000000))RAM64X1D_instance_3162(.WCLK(CLK), .WE(wea_0), 
  .A0(ADDRA[0]), .A1(ADDRA[1]), .A2(ADDRA[2]), .A3(ADDRA[3]), .A4(ADDRA[4]), .A5(ADDRA[5]), 
  .D(DINA[26]), .SPO(pre_douta_0[26]), .DPRA0(ADDRB[0]), .DPRA1(ADDRB[1]), .DPRA2(ADDRB[2]), 
  .DPRA3(ADDRB[3]), .DPRA4(ADDRB[4]), .DPRA5(ADDRB[5]), .DPO(pre_doutb_0[26]));
// Memory array element: COL: 0, ROW: 27
//  Initialization of Dual Port LUT ram now done through explicit
// parameter setting.
RAM64X1D#(.INIT(64'h0000000000000000))RAM64X1D_instance_3163(.WCLK(CLK), .WE(wea_0), 
  .A0(ADDRA[0]), .A1(ADDRA[1]), .A2(ADDRA[2]), .A3(ADDRA[3]), .A4(ADDRA[4]), .A5(ADDRA[5]), 
  .D(DINA[27]), .SPO(pre_douta_0[27]), .DPRA0(ADDRB[0]), .DPRA1(ADDRB[1]), .DPRA2(ADDRB[2]), 
  .DPRA3(ADDRB[3]), .DPRA4(ADDRB[4]), .DPRA5(ADDRB[5]), .DPO(pre_doutb_0[27]));
// Memory array element: COL: 0, ROW: 28
//  Initialization of Dual Port LUT ram now done through explicit
// parameter setting.
RAM64X1D#(.INIT(64'h0000000000000000))RAM64X1D_instance_3164(.WCLK(CLK), .WE(wea_0), 
  .A0(ADDRA[0]), .A1(ADDRA[1]), .A2(ADDRA[2]), .A3(ADDRA[3]), .A4(ADDRA[4]), .A5(ADDRA[5]), 
  .D(DINA[28]), .SPO(pre_douta_0[28]), .DPRA0(ADDRB[0]), .DPRA1(ADDRB[1]), .DPRA2(ADDRB[2]), 
  .DPRA3(ADDRB[3]), .DPRA4(ADDRB[4]), .DPRA5(ADDRB[5]), .DPO(pre_doutb_0[28]));
// Memory array element: COL: 0, ROW: 29
//  Initialization of Dual Port LUT ram now done through explicit
// parameter setting.
RAM64X1D#(.INIT(64'h0000000000000000))RAM64X1D_instance_3165(.WCLK(CLK), .WE(wea_0), 
  .A0(ADDRA[0]), .A1(ADDRA[1]), .A2(ADDRA[2]), .A3(ADDRA[3]), .A4(ADDRA[4]), .A5(ADDRA[5]), 
  .D(DINA[29]), .SPO(pre_douta_0[29]), .DPRA0(ADDRB[0]), .DPRA1(ADDRB[1]), .DPRA2(ADDRB[2]), 
  .DPRA3(ADDRB[3]), .DPRA4(ADDRB[4]), .DPRA5(ADDRB[5]), .DPO(pre_doutb_0[29]));
// Memory array element: COL: 0, ROW: 30
//  Initialization of Dual Port LUT ram now done through explicit
// parameter setting.
RAM64X1D#(.INIT(64'h0000000000000000))RAM64X1D_instance_3166(.WCLK(CLK), .WE(wea_0), 
  .A0(ADDRA[0]), .A1(ADDRA[1]), .A2(ADDRA[2]), .A3(ADDRA[3]), .A4(ADDRA[4]), .A5(ADDRA[5]), 
  .D(DINA[30]), .SPO(pre_douta_0[30]), .DPRA0(ADDRB[0]), .DPRA1(ADDRB[1]), .DPRA2(ADDRB[2]), 
  .DPRA3(ADDRB[3]), .DPRA4(ADDRB[4]), .DPRA5(ADDRB[5]), .DPO(pre_doutb_0[30]));
// Memory array element: COL: 0, ROW: 31
//  Initialization of Dual Port LUT ram now done through explicit
// parameter setting.
RAM64X1D#(.INIT(64'h0000000000000000))RAM64X1D_instance_3167(.WCLK(CLK), .WE(wea_0), 
  .A0(ADDRA[0]), .A1(ADDRA[1]), .A2(ADDRA[2]), .A3(ADDRA[3]), .A4(ADDRA[4]), .A5(ADDRA[5]), 
  .D(DINA[31]), .SPO(pre_douta_0[31]), .DPRA0(ADDRB[0]), .DPRA1(ADDRB[1]), .DPRA2(ADDRB[2]), 
  .DPRA3(ADDRB[3]), .DPRA4(ADDRB[4]), .DPRA5(ADDRB[5]), .DPO(pre_doutb_0[31]));
// Memory array element: COL: 1, ROW: 0
//  Initialization of Dual Port LUT ram now done through explicit
// parameter setting.
RAM64X1D#(.INIT(64'h0000000000000000))RAM64X1D_instance_3168(.WCLK(CLK), .WE(wea_1), 
  .A0(ADDRA[0]), .A1(ADDRA[1]), .A2(ADDRA[2]), .A3(ADDRA[3]), .A4(ADDRA[4]), .A5(ADDRA[5]), 
  .D(DINA[0]), .SPO(pre_douta_1[0]), .DPRA0(ADDRB[0]), .DPRA1(ADDRB[1]), .DPRA2(ADDRB[2]), 
  .DPRA3(ADDRB[3]), .DPRA4(ADDRB[4]), .DPRA5(ADDRB[5]), .DPO(pre_doutb_1[0]));
// Memory array element: COL: 1, ROW: 1
//  Initialization of Dual Port LUT ram now done through explicit
// parameter setting.
RAM64X1D#(.INIT(64'h0000000000000000))RAM64X1D_instance_3169(.WCLK(CLK), .WE(wea_1), 
  .A0(ADDRA[0]), .A1(ADDRA[1]), .A2(ADDRA[2]), .A3(ADDRA[3]), .A4(ADDRA[4]), .A5(ADDRA[5]), 
  .D(DINA[1]), .SPO(pre_douta_1[1]), .DPRA0(ADDRB[0]), .DPRA1(ADDRB[1]), .DPRA2(ADDRB[2]), 
  .DPRA3(ADDRB[3]), .DPRA4(ADDRB[4]), .DPRA5(ADDRB[5]), .DPO(pre_doutb_1[1]));
// Memory array element: COL: 1, ROW: 2
//  Initialization of Dual Port LUT ram now done through explicit
// parameter setting.
RAM64X1D#(.INIT(64'h0000000000000000))RAM64X1D_instance_3170(.WCLK(CLK), .WE(wea_1), 
  .A0(ADDRA[0]), .A1(ADDRA[1]), .A2(ADDRA[2]), .A3(ADDRA[3]), .A4(ADDRA[4]), .A5(ADDRA[5]), 
  .D(DINA[2]), .SPO(pre_douta_1[2]), .DPRA0(ADDRB[0]), .DPRA1(ADDRB[1]), .DPRA2(ADDRB[2]), 
  .DPRA3(ADDRB[3]), .DPRA4(ADDRB[4]), .DPRA5(ADDRB[5]), .DPO(pre_doutb_1[2]));
// Memory array element: COL: 1, ROW: 3
//  Initialization of Dual Port LUT ram now done through explicit
// parameter setting.
RAM64X1D#(.INIT(64'h0000000000000000))RAM64X1D_instance_3171(.WCLK(CLK), .WE(wea_1), 
  .A0(ADDRA[0]), .A1(ADDRA[1]), .A2(ADDRA[2]), .A3(ADDRA[3]), .A4(ADDRA[4]), .A5(ADDRA[5]), 
  .D(DINA[3]), .SPO(pre_douta_1[3]), .DPRA0(ADDRB[0]), .DPRA1(ADDRB[1]), .DPRA2(ADDRB[2]), 
  .DPRA3(ADDRB[3]), .DPRA4(ADDRB[4]), .DPRA5(ADDRB[5]), .DPO(pre_doutb_1[3]));
// Memory array element: COL: 1, ROW: 4
//  Initialization of Dual Port LUT ram now done through explicit
// parameter setting.
RAM64X1D#(.INIT(64'h0000000000000000))RAM64X1D_instance_3172(.WCLK(CLK), .WE(wea_1), 
  .A0(ADDRA[0]), .A1(ADDRA[1]), .A2(ADDRA[2]), .A3(ADDRA[3]), .A4(ADDRA[4]), .A5(ADDRA[5]), 
  .D(DINA[4]), .SPO(pre_douta_1[4]), .DPRA0(ADDRB[0]), .DPRA1(ADDRB[1]), .DPRA2(ADDRB[2]), 
  .DPRA3(ADDRB[3]), .DPRA4(ADDRB[4]), .DPRA5(ADDRB[5]), .DPO(pre_doutb_1[4]));
// Memory array element: COL: 1, ROW: 5
//  Initialization of Dual Port LUT ram now done through explicit
// parameter setting.
RAM64X1D#(.INIT(64'h0000000000000000))RAM64X1D_instance_3173(.WCLK(CLK), .WE(wea_1), 
  .A0(ADDRA[0]), .A1(ADDRA[1]), .A2(ADDRA[2]), .A3(ADDRA[3]), .A4(ADDRA[4]), .A5(ADDRA[5]), 
  .D(DINA[5]), .SPO(pre_douta_1[5]), .DPRA0(ADDRB[0]), .DPRA1(ADDRB[1]), .DPRA2(ADDRB[2]), 
  .DPRA3(ADDRB[3]), .DPRA4(ADDRB[4]), .DPRA5(ADDRB[5]), .DPO(pre_doutb_1[5]));
// Memory array element: COL: 1, ROW: 6
//  Initialization of Dual Port LUT ram now done through explicit
// parameter setting.
RAM64X1D#(.INIT(64'h0000000000000000))RAM64X1D_instance_3174(.WCLK(CLK), .WE(wea_1), 
  .A0(ADDRA[0]), .A1(ADDRA[1]), .A2(ADDRA[2]), .A3(ADDRA[3]), .A4(ADDRA[4]), .A5(ADDRA[5]), 
  .D(DINA[6]), .SPO(pre_douta_1[6]), .DPRA0(ADDRB[0]), .DPRA1(ADDRB[1]), .DPRA2(ADDRB[2]), 
  .DPRA3(ADDRB[3]), .DPRA4(ADDRB[4]), .DPRA5(ADDRB[5]), .DPO(pre_doutb_1[6]));
// Memory array element: COL: 1, ROW: 7
//  Initialization of Dual Port LUT ram now done through explicit
// parameter setting.
RAM64X1D#(.INIT(64'h0000000000000000))RAM64X1D_instance_3175(.WCLK(CLK), .WE(wea_1), 
  .A0(ADDRA[0]), .A1(ADDRA[1]), .A2(ADDRA[2]), .A3(ADDRA[3]), .A4(ADDRA[4]), .A5(ADDRA[5]), 
  .D(DINA[7]), .SPO(pre_douta_1[7]), .DPRA0(ADDRB[0]), .DPRA1(ADDRB[1]), .DPRA2(ADDRB[2]), 
  .DPRA3(ADDRB[3]), .DPRA4(ADDRB[4]), .DPRA5(ADDRB[5]), .DPO(pre_doutb_1[7]));
// Memory array element: COL: 1, ROW: 8
//  Initialization of Dual Port LUT ram now done through explicit
// parameter setting.
RAM64X1D#(.INIT(64'h0000000000000000))RAM64X1D_instance_3176(.WCLK(CLK), .WE(wea_1), 
  .A0(ADDRA[0]), .A1(ADDRA[1]), .A2(ADDRA[2]), .A3(ADDRA[3]), .A4(ADDRA[4]), .A5(ADDRA[5]), 
  .D(DINA[8]), .SPO(pre_douta_1[8]), .DPRA0(ADDRB[0]), .DPRA1(ADDRB[1]), .DPRA2(ADDRB[2]), 
  .DPRA3(ADDRB[3]), .DPRA4(ADDRB[4]), .DPRA5(ADDRB[5]), .DPO(pre_doutb_1[8]));
// Memory array element: COL: 1, ROW: 9
//  Initialization of Dual Port LUT ram now done through explicit
// parameter setting.
RAM64X1D#(.INIT(64'h0000000000000000))RAM64X1D_instance_3177(.WCLK(CLK), .WE(wea_1), 
  .A0(ADDRA[0]), .A1(ADDRA[1]), .A2(ADDRA[2]), .A3(ADDRA[3]), .A4(ADDRA[4]), .A5(ADDRA[5]), 
  .D(DINA[9]), .SPO(pre_douta_1[9]), .DPRA0(ADDRB[0]), .DPRA1(ADDRB[1]), .DPRA2(ADDRB[2]), 
  .DPRA3(ADDRB[3]), .DPRA4(ADDRB[4]), .DPRA5(ADDRB[5]), .DPO(pre_doutb_1[9]));
// Memory array element: COL: 1, ROW: 10
//  Initialization of Dual Port LUT ram now done through explicit
// parameter setting.
RAM64X1D#(.INIT(64'h0000000000000000))RAM64X1D_instance_3178(.WCLK(CLK), .WE(wea_1), 
  .A0(ADDRA[0]), .A1(ADDRA[1]), .A2(ADDRA[2]), .A3(ADDRA[3]), .A4(ADDRA[4]), .A5(ADDRA[5]), 
  .D(DINA[10]), .SPO(pre_douta_1[10]), .DPRA0(ADDRB[0]), .DPRA1(ADDRB[1]), .DPRA2(ADDRB[2]), 
  .DPRA3(ADDRB[3]), .DPRA4(ADDRB[4]), .DPRA5(ADDRB[5]), .DPO(pre_doutb_1[10]));
// Memory array element: COL: 1, ROW: 11
//  Initialization of Dual Port LUT ram now done through explicit
// parameter setting.
RAM64X1D#(.INIT(64'h0000000000000000))RAM64X1D_instance_3179(.WCLK(CLK), .WE(wea_1), 
  .A0(ADDRA[0]), .A1(ADDRA[1]), .A2(ADDRA[2]), .A3(ADDRA[3]), .A4(ADDRA[4]), .A5(ADDRA[5]), 
  .D(DINA[11]), .SPO(pre_douta_1[11]), .DPRA0(ADDRB[0]), .DPRA1(ADDRB[1]), .DPRA2(ADDRB[2]), 
  .DPRA3(ADDRB[3]), .DPRA4(ADDRB[4]), .DPRA5(ADDRB[5]), .DPO(pre_doutb_1[11]));
// Memory array element: COL: 1, ROW: 12
//  Initialization of Dual Port LUT ram now done through explicit
// parameter setting.
RAM64X1D#(.INIT(64'h0000000000000000))RAM64X1D_instance_3180(.WCLK(CLK), .WE(wea_1), 
  .A0(ADDRA[0]), .A1(ADDRA[1]), .A2(ADDRA[2]), .A3(ADDRA[3]), .A4(ADDRA[4]), .A5(ADDRA[5]), 
  .D(DINA[12]), .SPO(pre_douta_1[12]), .DPRA0(ADDRB[0]), .DPRA1(ADDRB[1]), .DPRA2(ADDRB[2]), 
  .DPRA3(ADDRB[3]), .DPRA4(ADDRB[4]), .DPRA5(ADDRB[5]), .DPO(pre_doutb_1[12]));
// Memory array element: COL: 1, ROW: 13
//  Initialization of Dual Port LUT ram now done through explicit
// parameter setting.
RAM64X1D#(.INIT(64'h0000000000000000))RAM64X1D_instance_3181(.WCLK(CLK), .WE(wea_1), 
  .A0(ADDRA[0]), .A1(ADDRA[1]), .A2(ADDRA[2]), .A3(ADDRA[3]), .A4(ADDRA[4]), .A5(ADDRA[5]), 
  .D(DINA[13]), .SPO(pre_douta_1[13]), .DPRA0(ADDRB[0]), .DPRA1(ADDRB[1]), .DPRA2(ADDRB[2]), 
  .DPRA3(ADDRB[3]), .DPRA4(ADDRB[4]), .DPRA5(ADDRB[5]), .DPO(pre_doutb_1[13]));
// Memory array element: COL: 1, ROW: 14
//  Initialization of Dual Port LUT ram now done through explicit
// parameter setting.
RAM64X1D#(.INIT(64'h0000000000000000))RAM64X1D_instance_3182(.WCLK(CLK), .WE(wea_1), 
  .A0(ADDRA[0]), .A1(ADDRA[1]), .A2(ADDRA[2]), .A3(ADDRA[3]), .A4(ADDRA[4]), .A5(ADDRA[5]), 
  .D(DINA[14]), .SPO(pre_douta_1[14]), .DPRA0(ADDRB[0]), .DPRA1(ADDRB[1]), .DPRA2(ADDRB[2]), 
  .DPRA3(ADDRB[3]), .DPRA4(ADDRB[4]), .DPRA5(ADDRB[5]), .DPO(pre_doutb_1[14]));
// Memory array element: COL: 1, ROW: 15
//  Initialization of Dual Port LUT ram now done through explicit
// parameter setting.
RAM64X1D#(.INIT(64'h0000000000000000))RAM64X1D_instance_3183(.WCLK(CLK), .WE(wea_1), 
  .A0(ADDRA[0]), .A1(ADDRA[1]), .A2(ADDRA[2]), .A3(ADDRA[3]), .A4(ADDRA[4]), .A5(ADDRA[5]), 
  .D(DINA[15]), .SPO(pre_douta_1[15]), .DPRA0(ADDRB[0]), .DPRA1(ADDRB[1]), .DPRA2(ADDRB[2]), 
  .DPRA3(ADDRB[3]), .DPRA4(ADDRB[4]), .DPRA5(ADDRB[5]), .DPO(pre_doutb_1[15]));
// Memory array element: COL: 1, ROW: 16
//  Initialization of Dual Port LUT ram now done through explicit
// parameter setting.
RAM64X1D#(.INIT(64'h0000000000000000))RAM64X1D_instance_3184(.WCLK(CLK), .WE(wea_1), 
  .A0(ADDRA[0]), .A1(ADDRA[1]), .A2(ADDRA[2]), .A3(ADDRA[3]), .A4(ADDRA[4]), .A5(ADDRA[5]), 
  .D(DINA[16]), .SPO(pre_douta_1[16]), .DPRA0(ADDRB[0]), .DPRA1(ADDRB[1]), .DPRA2(ADDRB[2]), 
  .DPRA3(ADDRB[3]), .DPRA4(ADDRB[4]), .DPRA5(ADDRB[5]), .DPO(pre_doutb_1[16]));
// Memory array element: COL: 1, ROW: 17
//  Initialization of Dual Port LUT ram now done through explicit
// parameter setting.
RAM64X1D#(.INIT(64'h0000000000000000))RAM64X1D_instance_3185(.WCLK(CLK), .WE(wea_1), 
  .A0(ADDRA[0]), .A1(ADDRA[1]), .A2(ADDRA[2]), .A3(ADDRA[3]), .A4(ADDRA[4]), .A5(ADDRA[5]), 
  .D(DINA[17]), .SPO(pre_douta_1[17]), .DPRA0(ADDRB[0]), .DPRA1(ADDRB[1]), .DPRA2(ADDRB[2]), 
  .DPRA3(ADDRB[3]), .DPRA4(ADDRB[4]), .DPRA5(ADDRB[5]), .DPO(pre_doutb_1[17]));
// Memory array element: COL: 1, ROW: 18
//  Initialization of Dual Port LUT ram now done through explicit
// parameter setting.
RAM64X1D#(.INIT(64'h0000000000000000))RAM64X1D_instance_3186(.WCLK(CLK), .WE(wea_1), 
  .A0(ADDRA[0]), .A1(ADDRA[1]), .A2(ADDRA[2]), .A3(ADDRA[3]), .A4(ADDRA[4]), .A5(ADDRA[5]), 
  .D(DINA[18]), .SPO(pre_douta_1[18]), .DPRA0(ADDRB[0]), .DPRA1(ADDRB[1]), .DPRA2(ADDRB[2]), 
  .DPRA3(ADDRB[3]), .DPRA4(ADDRB[4]), .DPRA5(ADDRB[5]), .DPO(pre_doutb_1[18]));
// Memory array element: COL: 1, ROW: 19
//  Initialization of Dual Port LUT ram now done through explicit
// parameter setting.
RAM64X1D#(.INIT(64'h0000000000000000))RAM64X1D_instance_3187(.WCLK(CLK), .WE(wea_1), 
  .A0(ADDRA[0]), .A1(ADDRA[1]), .A2(ADDRA[2]), .A3(ADDRA[3]), .A4(ADDRA[4]), .A5(ADDRA[5]), 
  .D(DINA[19]), .SPO(pre_douta_1[19]), .DPRA0(ADDRB[0]), .DPRA1(ADDRB[1]), .DPRA2(ADDRB[2]), 
  .DPRA3(ADDRB[3]), .DPRA4(ADDRB[4]), .DPRA5(ADDRB[5]), .DPO(pre_doutb_1[19]));
// Memory array element: COL: 1, ROW: 20
//  Initialization of Dual Port LUT ram now done through explicit
// parameter setting.
RAM64X1D#(.INIT(64'h0000000000000000))RAM64X1D_instance_3188(.WCLK(CLK), .WE(wea_1), 
  .A0(ADDRA[0]), .A1(ADDRA[1]), .A2(ADDRA[2]), .A3(ADDRA[3]), .A4(ADDRA[4]), .A5(ADDRA[5]), 
  .D(DINA[20]), .SPO(pre_douta_1[20]), .DPRA0(ADDRB[0]), .DPRA1(ADDRB[1]), .DPRA2(ADDRB[2]), 
  .DPRA3(ADDRB[3]), .DPRA4(ADDRB[4]), .DPRA5(ADDRB[5]), .DPO(pre_doutb_1[20]));
// Memory array element: COL: 1, ROW: 21
//  Initialization of Dual Port LUT ram now done through explicit
// parameter setting.
RAM64X1D#(.INIT(64'h0000000000000000))RAM64X1D_instance_3189(.WCLK(CLK), .WE(wea_1), 
  .A0(ADDRA[0]), .A1(ADDRA[1]), .A2(ADDRA[2]), .A3(ADDRA[3]), .A4(ADDRA[4]), .A5(ADDRA[5]), 
  .D(DINA[21]), .SPO(pre_douta_1[21]), .DPRA0(ADDRB[0]), .DPRA1(ADDRB[1]), .DPRA2(ADDRB[2]), 
  .DPRA3(ADDRB[3]), .DPRA4(ADDRB[4]), .DPRA5(ADDRB[5]), .DPO(pre_doutb_1[21]));
// Memory array element: COL: 1, ROW: 22
//  Initialization of Dual Port LUT ram now done through explicit
// parameter setting.
RAM64X1D#(.INIT(64'h0000000000000000))RAM64X1D_instance_3190(.WCLK(CLK), .WE(wea_1), 
  .A0(ADDRA[0]), .A1(ADDRA[1]), .A2(ADDRA[2]), .A3(ADDRA[3]), .A4(ADDRA[4]), .A5(ADDRA[5]), 
  .D(DINA[22]), .SPO(pre_douta_1[22]), .DPRA0(ADDRB[0]), .DPRA1(ADDRB[1]), .DPRA2(ADDRB[2]), 
  .DPRA3(ADDRB[3]), .DPRA4(ADDRB[4]), .DPRA5(ADDRB[5]), .DPO(pre_doutb_1[22]));
// Memory array element: COL: 1, ROW: 23
//  Initialization of Dual Port LUT ram now done through explicit
// parameter setting.
RAM64X1D#(.INIT(64'h0000000000000000))RAM64X1D_instance_3191(.WCLK(CLK), .WE(wea_1), 
  .A0(ADDRA[0]), .A1(ADDRA[1]), .A2(ADDRA[2]), .A3(ADDRA[3]), .A4(ADDRA[4]), .A5(ADDRA[5]), 
  .D(DINA[23]), .SPO(pre_douta_1[23]), .DPRA0(ADDRB[0]), .DPRA1(ADDRB[1]), .DPRA2(ADDRB[2]), 
  .DPRA3(ADDRB[3]), .DPRA4(ADDRB[4]), .DPRA5(ADDRB[5]), .DPO(pre_doutb_1[23]));
// Memory array element: COL: 1, ROW: 24
//  Initialization of Dual Port LUT ram now done through explicit
// parameter setting.
RAM64X1D#(.INIT(64'h0000000000000000))RAM64X1D_instance_3192(.WCLK(CLK), .WE(wea_1), 
  .A0(ADDRA[0]), .A1(ADDRA[1]), .A2(ADDRA[2]), .A3(ADDRA[3]), .A4(ADDRA[4]), .A5(ADDRA[5]), 
  .D(DINA[24]), .SPO(pre_douta_1[24]), .DPRA0(ADDRB[0]), .DPRA1(ADDRB[1]), .DPRA2(ADDRB[2]), 
  .DPRA3(ADDRB[3]), .DPRA4(ADDRB[4]), .DPRA5(ADDRB[5]), .DPO(pre_doutb_1[24]));
// Memory array element: COL: 1, ROW: 25
//  Initialization of Dual Port LUT ram now done through explicit
// parameter setting.
RAM64X1D#(.INIT(64'h0000000000000000))RAM64X1D_instance_3193(.WCLK(CLK), .WE(wea_1), 
  .A0(ADDRA[0]), .A1(ADDRA[1]), .A2(ADDRA[2]), .A3(ADDRA[3]), .A4(ADDRA[4]), .A5(ADDRA[5]), 
  .D(DINA[25]), .SPO(pre_douta_1[25]), .DPRA0(ADDRB[0]), .DPRA1(ADDRB[1]), .DPRA2(ADDRB[2]), 
  .DPRA3(ADDRB[3]), .DPRA4(ADDRB[4]), .DPRA5(ADDRB[5]), .DPO(pre_doutb_1[25]));
// Memory array element: COL: 1, ROW: 26
//  Initialization of Dual Port LUT ram now done through explicit
// parameter setting.
RAM64X1D#(.INIT(64'h0000000000000000))RAM64X1D_instance_3194(.WCLK(CLK), .WE(wea_1), 
  .A0(ADDRA[0]), .A1(ADDRA[1]), .A2(ADDRA[2]), .A3(ADDRA[3]), .A4(ADDRA[4]), .A5(ADDRA[5]), 
  .D(DINA[26]), .SPO(pre_douta_1[26]), .DPRA0(ADDRB[0]), .DPRA1(ADDRB[1]), .DPRA2(ADDRB[2]), 
  .DPRA3(ADDRB[3]), .DPRA4(ADDRB[4]), .DPRA5(ADDRB[5]), .DPO(pre_doutb_1[26]));
// Memory array element: COL: 1, ROW: 27
//  Initialization of Dual Port LUT ram now done through explicit
// parameter setting.
RAM64X1D#(.INIT(64'h0000000000000000))RAM64X1D_instance_3195(.WCLK(CLK), .WE(wea_1), 
  .A0(ADDRA[0]), .A1(ADDRA[1]), .A2(ADDRA[2]), .A3(ADDRA[3]), .A4(ADDRA[4]), .A5(ADDRA[5]), 
  .D(DINA[27]), .SPO(pre_douta_1[27]), .DPRA0(ADDRB[0]), .DPRA1(ADDRB[1]), .DPRA2(ADDRB[2]), 
  .DPRA3(ADDRB[3]), .DPRA4(ADDRB[4]), .DPRA5(ADDRB[5]), .DPO(pre_doutb_1[27]));
// Memory array element: COL: 1, ROW: 28
//  Initialization of Dual Port LUT ram now done through explicit
// parameter setting.
RAM64X1D#(.INIT(64'h0000000000000000))RAM64X1D_instance_3196(.WCLK(CLK), .WE(wea_1), 
  .A0(ADDRA[0]), .A1(ADDRA[1]), .A2(ADDRA[2]), .A3(ADDRA[3]), .A4(ADDRA[4]), .A5(ADDRA[5]), 
  .D(DINA[28]), .SPO(pre_douta_1[28]), .DPRA0(ADDRB[0]), .DPRA1(ADDRB[1]), .DPRA2(ADDRB[2]), 
  .DPRA3(ADDRB[3]), .DPRA4(ADDRB[4]), .DPRA5(ADDRB[5]), .DPO(pre_doutb_1[28]));
// Memory array element: COL: 1, ROW: 29
//  Initialization of Dual Port LUT ram now done through explicit
// parameter setting.
RAM64X1D#(.INIT(64'h0000000000000000))RAM64X1D_instance_3197(.WCLK(CLK), .WE(wea_1), 
  .A0(ADDRA[0]), .A1(ADDRA[1]), .A2(ADDRA[2]), .A3(ADDRA[3]), .A4(ADDRA[4]), .A5(ADDRA[5]), 
  .D(DINA[29]), .SPO(pre_douta_1[29]), .DPRA0(ADDRB[0]), .DPRA1(ADDRB[1]), .DPRA2(ADDRB[2]), 
  .DPRA3(ADDRB[3]), .DPRA4(ADDRB[4]), .DPRA5(ADDRB[5]), .DPO(pre_doutb_1[29]));
// Memory array element: COL: 1, ROW: 30
//  Initialization of Dual Port LUT ram now done through explicit
// parameter setting.
RAM64X1D#(.INIT(64'h0000000000000000))RAM64X1D_instance_3198(.WCLK(CLK), .WE(wea_1), 
  .A0(ADDRA[0]), .A1(ADDRA[1]), .A2(ADDRA[2]), .A3(ADDRA[3]), .A4(ADDRA[4]), .A5(ADDRA[5]), 
  .D(DINA[30]), .SPO(pre_douta_1[30]), .DPRA0(ADDRB[0]), .DPRA1(ADDRB[1]), .DPRA2(ADDRB[2]), 
  .DPRA3(ADDRB[3]), .DPRA4(ADDRB[4]), .DPRA5(ADDRB[5]), .DPO(pre_doutb_1[30]));
// Memory array element: COL: 1, ROW: 31
//  Initialization of Dual Port LUT ram now done through explicit
// parameter setting.
RAM64X1D#(.INIT(64'h0000000000000000))RAM64X1D_instance_3199(.WCLK(CLK), .WE(wea_1), 
  .A0(ADDRA[0]), .A1(ADDRA[1]), .A2(ADDRA[2]), .A3(ADDRA[3]), .A4(ADDRA[4]), .A5(ADDRA[5]), 
  .D(DINA[31]), .SPO(pre_douta_1[31]), .DPRA0(ADDRB[0]), .DPRA1(ADDRB[1]), .DPRA2(ADDRB[2]), 
  .DPRA3(ADDRB[3]), .DPRA4(ADDRB[4]), .DPRA5(ADDRB[5]), .DPO(pre_doutb_1[31]));
endmodule



module medianRow13_structuralmemory_2c76e809_(CLK_u95, bus_2f85e247_, bus_3ca45016_, bus_568a65b9_, bus_4ac85576_, bus_641f6cf0_, bus_50d77e1e_, bus_4ed55c07_, bus_2d16d021_, bus_237d7b4a_, bus_7fade1e4_, bus_3ae5bab1_, bus_65ef3584_, bus_560c2387_);
input		CLK_u95;
input		bus_2f85e247_;
input	[31:0]	bus_3ca45016_;
input	[2:0]	bus_568a65b9_;
input		bus_4ac85576_;
input	[31:0]	bus_641f6cf0_;
input	[2:0]	bus_50d77e1e_;
input		bus_4ed55c07_;
input		bus_2d16d021_;
input	[31:0]	bus_237d7b4a_;
output	[31:0]	bus_7fade1e4_;
output		bus_3ae5bab1_;
output	[31:0]	bus_65ef3584_;
output		bus_560c2387_;
wire		or_5ab68ff7_u0;
wire		and_52f5c311_u0;
wire		not_2518cefd_u0;
reg		logicalMem_3641f37f_we_delay0_u0=1'h0;
wire	[31:0]	bus_7f0e6984_;
wire	[31:0]	bus_5fac5809_;
wire		or_7713d70b_u0;
assign or_5ab68ff7_u0=bus_4ed55c07_|bus_2d16d021_;
assign and_52f5c311_u0=bus_4ed55c07_&not_2518cefd_u0;
assign not_2518cefd_u0=~bus_2d16d021_;
assign bus_7fade1e4_=bus_7f0e6984_;
assign bus_3ae5bab1_=bus_4ac85576_;
assign bus_65ef3584_=bus_5fac5809_;
assign bus_560c2387_=or_7713d70b_u0;
always @(posedge CLK_u95 or posedge bus_2f85e247_)
begin
if (bus_2f85e247_)
logicalMem_3641f37f_we_delay0_u0<=1'h0;
else logicalMem_3641f37f_we_delay0_u0<=bus_2d16d021_;
end
medianRow13_forge_memory_101x32_151 medianRow13_forge_memory_101x32_151_instance0(.CLK(CLK_u95), 
  .ENA(or_5ab68ff7_u0), .WEA(bus_2d16d021_), .DINA(bus_237d7b4a_), .ADDRA(bus_641f6cf0_), 
  .DOUTA(bus_5fac5809_), .DONEA(), .ENB(bus_4ac85576_), .ADDRB(bus_3ca45016_), .DOUTB(bus_7f0e6984_), 
  .DONEB());
assign or_7713d70b_u0=and_52f5c311_u0|logicalMem_3641f37f_we_delay0_u0;
endmodule


