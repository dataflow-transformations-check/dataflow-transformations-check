// __  ___ __ ___  _ __   ___  ___ 
// \ \/ / '__/ _ \| '_ \ / _ \/ __|
//  >  <| | | (_) | | | | (_) \__ \
// /_/\_\_|  \___/|_| |_|\___/|___/
// 
// Xronos synthesizer version
// Run date: Fri 23 Feb 2018 14:28:42 +0000
// 

module medianRow14(in1_COUNT, median_SEND, in1_SEND, in1_ACK, median_COUNT, in1_DATA, median_ACK, RESET, CLK, median_RDY, median_DATA);
input	[15:0]	in1_COUNT;
wire		compute_median_done;
output		median_SEND;
wire		receive_done;
input		in1_SEND;
output		in1_ACK;
output	[15:0]	median_COUNT;
input	[7:0]	in1_DATA;
input		median_ACK;
input		RESET;
wire		compute_median_go;
input		CLK;
input		median_RDY;
output	[7:0]	median_DATA;
wire		receive_go;
wire		bus_30b13cf3_;
wire		bus_7242a94f_;
wire		bus_40a54195_;
wire	[31:0]	bus_2005807b_;
wire		bus_5ac2bee3_;
wire	[2:0]	bus_04185c5a_;
wire	[31:0]	bus_53fc1a5b_;
wire		bus_732a2d8d_;
wire		bus_52fe0204_;
wire	[31:0]	bus_6b85f47c_;
wire		scheduler;
wire		scheduler_u822;
wire		scheduler_u820;
wire		medianRow14_scheduler_instance_DONE;
wire		scheduler_u821;
wire		compute_median_u684;
wire	[2:0]	compute_median_u682;
wire	[31:0]	compute_median_u675;
wire	[2:0]	compute_median_u676;
wire	[31:0]	compute_median_u681;
wire	[7:0]	compute_median_u683;
wire		medianRow14_compute_median_instance_DONE;
wire	[31:0]	compute_median_u672;
wire	[31:0]	compute_median_u674;
wire		compute_median_u680;
wire	[15:0]	compute_median_u685;
wire		compute_median;
wire		compute_median_u673;
wire	[2:0]	compute_median_u679;
wire	[31:0]	compute_median_u678;
wire		compute_median_u677;
wire		bus_3d7b8987_;
wire		bus_0df84855_;
wire	[31:0]	bus_52928377_;
wire		bus_713854a6_;
wire		bus_0419815e_;
wire	[31:0]	bus_7f631c10_;
wire		bus_637acd77_;
wire	[2:0]	bus_32a0e263_;
wire		bus_06d4ea92_;
wire	[31:0]	bus_43bd9345_;
wire		bus_64a4f33b_;
wire	[31:0]	bus_5f38f938_;
wire	[31:0]	bus_3988adda_;
wire		bus_48f2414d_;
wire	[31:0]	receive_u288;
wire	[31:0]	receive_u290;
wire		receive_u293;
wire		medianRow14_receive_instance_DONE;
wire	[31:0]	receive_u291;
wire		receive;
wire		receive_u289;
wire	[2:0]	receive_u292;
wire	[31:0]	bus_5a91f7de_;
assign compute_median_done=bus_30b13cf3_;
assign median_SEND=compute_median_u684;
assign receive_done=bus_40a54195_;
assign in1_ACK=receive_u293;
assign median_COUNT=compute_median_u685;
assign compute_median_go=scheduler_u821;
assign median_DATA=compute_median_u683;
assign receive_go=scheduler_u822;
assign bus_30b13cf3_=medianRow14_compute_median_instance_DONE&{1{medianRow14_compute_median_instance_DONE}};
medianRow14_Kicker_60 medianRow14_Kicker_60_1(.CLK(CLK), .RESET(bus_0df84855_), 
  .bus_7242a94f_(bus_7242a94f_));
assign bus_40a54195_=medianRow14_receive_instance_DONE&{1{medianRow14_receive_instance_DONE}};
medianRow14_simplememoryreferee_656082b3_ medianRow14_simplememoryreferee_656082b3__1(.bus_7a2b448b_(CLK), 
  .bus_21b58019_(bus_0df84855_), .bus_27a5e20d_(bus_713854a6_), .bus_2116a4e2_(bus_52928377_), 
  .bus_15c06e35_(compute_median_u677), .bus_5ebef1f4_(compute_median_u678), .bus_561b467c_(3'h1), 
  .bus_2005807b_(bus_2005807b_), .bus_6b85f47c_(bus_6b85f47c_), .bus_732a2d8d_(bus_732a2d8d_), 
  .bus_5ac2bee3_(bus_5ac2bee3_), .bus_04185c5a_(bus_04185c5a_), .bus_53fc1a5b_(bus_53fc1a5b_), 
  .bus_52fe0204_(bus_52fe0204_));
medianRow14_scheduler medianRow14_scheduler_instance(.CLK(CLK), .RESET(bus_0df84855_), 
  .GO(bus_7242a94f_), .port_3197827b_(bus_3d7b8987_), .port_55cac33a_(bus_5a91f7de_), 
  .port_3c38b220_(median_RDY), .port_59940304_(in1_SEND), .port_0c2d1aba_(compute_median_done), 
  .port_7d5b6775_(receive_done), .DONE(medianRow14_scheduler_instance_DONE), 
  .RESULT(scheduler), .RESULT_u2268(scheduler_u820), .RESULT_u2269(scheduler_u821), 
  .RESULT_u2270(scheduler_u822));
medianRow14_compute_median medianRow14_compute_median_instance(.CLK(CLK), .RESET(bus_0df84855_), 
  .GO(compute_median_go), .port_76f7aacb_(bus_48f2414d_), .port_2e1fbae3_(bus_52fe0204_), 
  .port_76e4dd2c_(bus_53fc1a5b_), .port_3ee33f3d_(bus_48f2414d_), .port_6e601dbc_(bus_3988adda_), 
  .DONE(medianRow14_compute_median_instance_DONE), .RESULT(compute_median), .RESULT_u2271(compute_median_u672), 
  .RESULT_u2275(compute_median_u673), .RESULT_u2276(compute_median_u674), .RESULT_u2277(compute_median_u675), 
  .RESULT_u2278(compute_median_u676), .RESULT_u2279(compute_median_u677), .RESULT_u2280(compute_median_u678), 
  .RESULT_u2281(compute_median_u679), .RESULT_u2272(compute_median_u680), .RESULT_u2273(compute_median_u681), 
  .RESULT_u2274(compute_median_u682), .RESULT_u2282(compute_median_u683), .RESULT_u2283(compute_median_u684), 
  .RESULT_u2284(compute_median_u685));
medianRow14_stateVar_fsmState_medianRow14 medianRow14_stateVar_fsmState_medianRow14_1(.bus_45dd65fc_(CLK), 
  .bus_57fccf7b_(bus_0df84855_), .bus_51356527_(scheduler), .bus_4e5f87e9_(scheduler_u820), 
  .bus_3d7b8987_(bus_3d7b8987_));
medianRow14_globalreset_physical_5d88c254_ medianRow14_globalreset_physical_5d88c254__1(.bus_6ba46f68_(CLK), 
  .bus_70bc7440_(RESET), .bus_0df84855_(bus_0df84855_));
medianRow14_structuralmemory_618ad16b_ medianRow14_structuralmemory_618ad16b__1(.CLK_u94(CLK), 
  .bus_05bd45bf_(bus_0df84855_), .bus_58767192_(bus_43bd9345_), .bus_1d9f7981_(3'h1), 
  .bus_52c4fa14_(bus_64a4f33b_), .bus_7ac684df_(bus_637acd77_), .bus_355418f2_(bus_5f38f938_), 
  .bus_79890934_(bus_6b85f47c_), .bus_68daf440_(3'h1), .bus_4c6d4b8e_(bus_5ac2bee3_), 
  .bus_7f631c10_(bus_7f631c10_), .bus_0419815e_(bus_0419815e_), .bus_52928377_(bus_52928377_), 
  .bus_713854a6_(bus_713854a6_));
medianRow14_simplememoryreferee_61093e6a_ medianRow14_simplememoryreferee_61093e6a__1(.bus_06546d6b_(CLK), 
  .bus_13c2f4dd_(bus_0df84855_), .bus_3aec6f19_(bus_0419815e_), .bus_406f1f21_(bus_7f631c10_), 
  .bus_7a7cab01_(receive_u289), .bus_5d2629ca_({24'b0, receive_u291[7:0]}), .bus_632636fa_(receive_u290), 
  .bus_17ae75da_(3'h1), .bus_18a94427_(compute_median_u680), .bus_10efc10b_(compute_median_u673), 
  .bus_5686e6c7_(compute_median_u675), .bus_53b95538_(compute_median_u674), .bus_741e438e_(3'h1), 
  .bus_5f38f938_(bus_5f38f938_), .bus_43bd9345_(bus_43bd9345_), .bus_637acd77_(bus_637acd77_), 
  .bus_64a4f33b_(bus_64a4f33b_), .bus_32a0e263_(bus_32a0e263_), .bus_06d4ea92_(bus_06d4ea92_), 
  .bus_3988adda_(bus_3988adda_), .bus_48f2414d_(bus_48f2414d_));
medianRow14_receive medianRow14_receive_instance(.CLK(CLK), .RESET(bus_0df84855_), 
  .GO(receive_go), .port_14d71502_(bus_5a91f7de_), .port_49444547_(bus_06d4ea92_), 
  .port_6bf44be4_(in1_DATA), .DONE(medianRow14_receive_instance_DONE), .RESULT(receive), 
  .RESULT_u2285(receive_u288), .RESULT_u2286(receive_u289), .RESULT_u2287(receive_u290), 
  .RESULT_u2288(receive_u291), .RESULT_u2289(receive_u292), .RESULT_u2290(receive_u293));
medianRow14_stateVar_i medianRow14_stateVar_i_1(.bus_1e078b49_(CLK), .bus_3b0b26c0_(bus_0df84855_), 
  .bus_49371d5b_(receive), .bus_09179c78_(receive_u288), .bus_232555cd_(compute_median), 
  .bus_3954d5ca_(32'h0), .bus_5a91f7de_(bus_5a91f7de_));
endmodule



module medianRow14_Kicker_60(CLK, RESET, bus_7242a94f_);
input		CLK;
input		RESET;
output		bus_7242a94f_;
reg		kicker_1=1'h0;
wire		bus_5d5e907f_;
reg		kicker_2=1'h0;
wire		bus_51c13128_;
wire		bus_05175bfe_;
wire		bus_24cd34d7_;
reg		kicker_res=1'h0;
always @(posedge CLK)
begin
kicker_1<=bus_24cd34d7_;
end
assign bus_5d5e907f_=~kicker_2;
assign bus_7242a94f_=kicker_res;
always @(posedge CLK)
begin
kicker_2<=bus_51c13128_;
end
assign bus_51c13128_=bus_24cd34d7_&kicker_1;
assign bus_05175bfe_=kicker_1&bus_24cd34d7_&bus_5d5e907f_;
assign bus_24cd34d7_=~RESET;
always @(posedge CLK)
begin
kicker_res<=bus_05175bfe_;
end
endmodule



module medianRow14_simplememoryreferee_656082b3_(bus_7a2b448b_, bus_21b58019_, bus_27a5e20d_, bus_2116a4e2_, bus_15c06e35_, bus_5ebef1f4_, bus_561b467c_, bus_2005807b_, bus_6b85f47c_, bus_732a2d8d_, bus_5ac2bee3_, bus_04185c5a_, bus_53fc1a5b_, bus_52fe0204_);
input		bus_7a2b448b_;
input		bus_21b58019_;
input		bus_27a5e20d_;
input	[31:0]	bus_2116a4e2_;
input		bus_15c06e35_;
input	[31:0]	bus_5ebef1f4_;
input	[2:0]	bus_561b467c_;
output	[31:0]	bus_2005807b_;
output	[31:0]	bus_6b85f47c_;
output		bus_732a2d8d_;
output		bus_5ac2bee3_;
output	[2:0]	bus_04185c5a_;
output	[31:0]	bus_53fc1a5b_;
output		bus_52fe0204_;
assign bus_2005807b_=32'h0;
assign bus_6b85f47c_=bus_5ebef1f4_;
assign bus_732a2d8d_=1'h0;
assign bus_5ac2bee3_=bus_15c06e35_;
assign bus_04185c5a_=3'h1;
assign bus_53fc1a5b_=bus_2116a4e2_;
assign bus_52fe0204_=bus_27a5e20d_;
endmodule



module medianRow14_scheduler(CLK, RESET, GO, port_3197827b_, port_55cac33a_, port_3c38b220_, port_59940304_, port_0c2d1aba_, port_7d5b6775_, RESULT, RESULT_u2268, RESULT_u2269, RESULT_u2270, DONE);
input		CLK;
input		RESET;
input		GO;
input		port_3197827b_;
input	[31:0]	port_55cac33a_;
input		port_3c38b220_;
input		port_59940304_;
input		port_0c2d1aba_;
input		port_7d5b6775_;
output		RESULT;
output		RESULT_u2268;
output		RESULT_u2269;
output		RESULT_u2270;
output		DONE;
wire		and_u3866_u0;
wire		lessThan;
wire signed	[31:0]	lessThan_a_signed;
wire signed	[31:0]	lessThan_b_signed;
wire signed	[31:0]	equals_b_signed;
wire		equals;
wire signed	[31:0]	equals_a_signed;
wire signed	[31:0]	equals_u238_a_signed;
wire		equals_u238;
wire signed	[31:0]	equals_u238_b_signed;
wire		not_u900_u0;
wire		and_u3867_u0;
wire		and_u3868_u0;
wire		andOp;
wire		and_u3869_u0;
wire		not_u901_u0;
wire		and_u3870_u0;
wire		simplePinWrite;
wire		and_u3871_u0;
wire		and_u3872_u0;
wire		equals_u239;
wire signed	[31:0]	equals_u239_b_signed;
wire signed	[31:0]	equals_u239_a_signed;
wire		and_u3873_u0;
wire		not_u902_u0;
wire		and_u3874_u0;
wire		andOp_u82;
wire		and_u3875_u0;
wire		not_u903_u0;
wire		and_u3876_u0;
wire		simplePinWrite_u630;
wire		and_u3877_u0;
wire		not_u904_u0;
wire		and_u3878_u0;
wire		and_u3879_u0;
wire		not_u905_u0;
wire		and_u3880_u0;
wire		simplePinWrite_u631;
wire		or_u1480_u0;
reg		reg_709a49fe_u0=1'h0;
wire		and_u3881_u0;
wire		and_u3882_u0;
wire		and_u3883_u0;
wire		and_u3884_u0;
reg		and_delayed_u395=1'h0;
wire		or_u1481_u0;
wire		and_u3885_u0;
wire		or_u1482_u0;
wire		mux_u1958;
wire		or_u1483_u0;
reg		reg_4a4c6590_u0=1'h0;
wire		and_u3886_u0;
wire		or_u1484_u0;
reg		reg_2f57b5b3_u0=1'h0;
wire		and_u3887_u0;
wire		and_u3888_u0;
wire		receive_go_merge;
wire		or_u1485_u0;
wire		mux_u1959_u0;
wire		or_u1486_u0;
reg		syncEnable_u1467=1'h0;
reg		loopControl_u100=1'h0;
reg		reg_0c125244_u0=1'h0;
wire		mux_u1960_u0;
wire		or_u1487_u0;
reg		reg_00bd944d_u0=1'h0;
assign and_u3866_u0=or_u1486_u0&or_u1486_u0;
assign lessThan_a_signed=port_55cac33a_;
assign lessThan_b_signed=32'h65;
assign lessThan=lessThan_a_signed<lessThan_b_signed;
assign equals_a_signed=port_55cac33a_;
assign equals_b_signed=32'h65;
assign equals=equals_a_signed==equals_b_signed;
assign equals_u238_a_signed={31'b0, port_3197827b_};
assign equals_u238_b_signed=32'h0;
assign equals_u238=equals_u238_a_signed==equals_u238_b_signed;
assign not_u900_u0=~equals_u238;
assign and_u3867_u0=and_u3866_u0&not_u900_u0;
assign and_u3868_u0=and_u3866_u0&equals_u238;
assign andOp=lessThan&port_59940304_;
assign and_u3869_u0=and_u3872_u0&andOp;
assign not_u901_u0=~andOp;
assign and_u3870_u0=and_u3872_u0&not_u901_u0;
assign simplePinWrite=and_u3871_u0&{1{and_u3871_u0}};
assign and_u3871_u0=and_u3869_u0&and_u3872_u0;
assign and_u3872_u0=and_u3868_u0&and_u3866_u0;
assign equals_u239_a_signed={31'b0, port_3197827b_};
assign equals_u239_b_signed=32'h1;
assign equals_u239=equals_u239_a_signed==equals_u239_b_signed;
assign and_u3873_u0=and_u3866_u0&not_u902_u0;
assign not_u902_u0=~equals_u239;
assign and_u3874_u0=and_u3866_u0&equals_u239;
assign andOp_u82=lessThan&port_59940304_;
assign and_u3875_u0=and_u3887_u0&not_u903_u0;
assign not_u903_u0=~andOp_u82;
assign and_u3876_u0=and_u3887_u0&andOp_u82;
assign simplePinWrite_u630=and_u3886_u0&{1{and_u3886_u0}};
assign and_u3877_u0=and_u3885_u0&not_u904_u0;
assign not_u904_u0=~equals;
assign and_u3878_u0=and_u3885_u0&equals;
assign and_u3879_u0=and_u3883_u0&not_u905_u0;
assign not_u905_u0=~port_3c38b220_;
assign and_u3880_u0=and_u3883_u0&port_3c38b220_;
assign simplePinWrite_u631=and_u3881_u0&{1{and_u3881_u0}};
assign or_u1480_u0=reg_709a49fe_u0|port_0c2d1aba_;
always @(posedge CLK or posedge RESET)
begin
if (RESET)
reg_709a49fe_u0<=1'h0;
else reg_709a49fe_u0<=and_u3882_u0;
end
assign and_u3881_u0=and_u3880_u0&and_u3883_u0;
assign and_u3882_u0=and_u3879_u0&and_u3883_u0;
assign and_u3883_u0=and_u3878_u0&and_u3885_u0;
assign and_u3884_u0=and_u3877_u0&and_u3885_u0;
always @(posedge CLK or posedge RESET)
begin
if (RESET)
and_delayed_u395<=1'h0;
else and_delayed_u395<=and_u3884_u0;
end
assign or_u1481_u0=or_u1480_u0|and_delayed_u395;
assign and_u3885_u0=and_u3875_u0&and_u3887_u0;
assign or_u1482_u0=reg_4a4c6590_u0|or_u1481_u0;
assign mux_u1958=(and_u3886_u0)?1'h1:1'h0;
assign or_u1483_u0=and_u3886_u0|and_u3881_u0;
always @(posedge CLK or posedge RESET)
begin
if (RESET)
reg_4a4c6590_u0<=1'h0;
else reg_4a4c6590_u0<=and_u3886_u0;
end
assign and_u3886_u0=and_u3876_u0&and_u3887_u0;
assign or_u1484_u0=reg_2f57b5b3_u0|or_u1482_u0;
always @(posedge CLK or posedge RESET)
begin
if (RESET)
reg_2f57b5b3_u0<=1'h0;
else reg_2f57b5b3_u0<=and_u3888_u0;
end
assign and_u3887_u0=and_u3874_u0&and_u3866_u0;
assign and_u3888_u0=and_u3873_u0&and_u3866_u0;
assign receive_go_merge=simplePinWrite|simplePinWrite_u630;
assign or_u1485_u0=and_u3871_u0|or_u1483_u0;
assign mux_u1959_u0=(and_u3871_u0)?1'h1:mux_u1958;
assign or_u1486_u0=reg_0c125244_u0|loopControl_u100;
always @(posedge CLK)
begin
if (reg_0c125244_u0)
syncEnable_u1467<=RESET;
end
always @(posedge CLK or posedge syncEnable_u1467)
begin
if (syncEnable_u1467)
loopControl_u100<=1'h0;
else loopControl_u100<=or_u1484_u0;
end
always @(posedge CLK or posedge RESET)
begin
if (RESET)
reg_0c125244_u0<=1'h0;
else reg_0c125244_u0<=reg_00bd944d_u0;
end
assign mux_u1960_u0=(GO)?1'h0:mux_u1959_u0;
assign or_u1487_u0=GO|or_u1485_u0;
always @(posedge CLK or posedge RESET)
begin
if (RESET)
reg_00bd944d_u0<=1'h0;
else reg_00bd944d_u0<=GO;
end
assign RESULT=or_u1487_u0;
assign RESULT_u2268=mux_u1960_u0;
assign RESULT_u2269=simplePinWrite_u631;
assign RESULT_u2270=receive_go_merge;
assign DONE=1'h0;
endmodule



module medianRow14_compute_median(CLK, RESET, GO, port_3ee33f3d_, port_6e601dbc_, port_76f7aacb_, port_2e1fbae3_, port_76e4dd2c_, RESULT, RESULT_u2271, RESULT_u2272, RESULT_u2273, RESULT_u2274, RESULT_u2275, RESULT_u2276, RESULT_u2277, RESULT_u2278, RESULT_u2279, RESULT_u2280, RESULT_u2281, RESULT_u2282, RESULT_u2283, RESULT_u2284, DONE);
input		CLK;
input		RESET;
input		GO;
input		port_3ee33f3d_;
input	[31:0]	port_6e601dbc_;
input		port_76f7aacb_;
input		port_2e1fbae3_;
input	[31:0]	port_76e4dd2c_;
output		RESULT;
output	[31:0]	RESULT_u2271;
output		RESULT_u2272;
output	[31:0]	RESULT_u2273;
output	[2:0]	RESULT_u2274;
output		RESULT_u2275;
output	[31:0]	RESULT_u2276;
output	[31:0]	RESULT_u2277;
output	[2:0]	RESULT_u2278;
output		RESULT_u2279;
output	[31:0]	RESULT_u2280;
output	[2:0]	RESULT_u2281;
output	[7:0]	RESULT_u2282;
output		RESULT_u2283;
output	[15:0]	RESULT_u2284;
output		DONE;
wire		and_u3889_u0;
reg	[31:0]	syncEnable_u1468=32'h0;
wire	[31:0]	add;
wire		and_u3890_u0;
wire	[31:0]	add_u694;
wire	[31:0]	add_u695;
wire		and_u3891_u0;
wire		greaterThan;
wire signed	[31:0]	greaterThan_a_signed;
wire signed	[31:0]	greaterThan_b_signed;
wire		not_u906_u0;
wire		and_u3892_u0;
wire		and_u3893_u0;
wire	[31:0]	add_u696;
wire		and_u3894_u0;
wire	[31:0]	add_u697;
wire	[31:0]	add_u698;
wire		and_u3895_u0;
wire	[31:0]	add_u699;
wire		and_u3896_u0;
wire		or_u1488_u0;
reg		reg_57db87c4_u0=1'h0;
wire	[31:0]	add_u700;
wire	[31:0]	add_u701;
reg		reg_000f981d_u0=1'h0;
wire		or_u1489_u0;
wire		and_u3897_u0;
reg	[31:0]	syncEnable_u1469_u0=32'h0;
reg		block_GO_delayed_u102=1'h0;
reg	[31:0]	syncEnable_u1470_u0=32'h0;
reg	[31:0]	syncEnable_u1471_u0=32'h0;
reg		block_GO_delayed_result_delayed_u20=1'h0;
wire		or_u1490_u0;
wire	[31:0]	mux_u1961;
wire	[31:0]	mux_u1962_u0;
reg	[31:0]	syncEnable_u1472_u0=32'h0;
reg	[31:0]	syncEnable_u1473_u0=32'h0;
reg	[31:0]	syncEnable_u1474_u0=32'h0;
reg		reg_1d072682_u0=1'h0;
wire	[31:0]	mux_u1963_u0;
reg		reg_63486d2e_u0=1'h0;
wire		mux_u1964_u0;
reg		reg_1015bbbb_u0=1'h0;
wire		and_u3898_u0;
wire		or_u1491_u0;
wire		and_u3899_u0;
wire	[31:0]	mux_u1965_u0;
reg		reg_1015bbbb_result_delayed_u0=1'h0;
reg		syncEnable_u1475_u0=1'h0;
reg		and_delayed_u396=1'h0;
reg	[31:0]	syncEnable_u1476_u0=32'h0;
reg	[31:0]	syncEnable_u1477_u0=32'h0;
wire	[31:0]	add_u702;
reg	[31:0]	syncEnable_u1478_u0=32'h0;
reg		syncEnable_u1479_u0=1'h0;
wire		or_u1492_u0;
wire	[31:0]	mux_u1966_u0;
reg	[31:0]	syncEnable_u1480_u0=32'h0;
reg	[31:0]	syncEnable_u1481_u0=32'h0;
reg		block_GO_delayed_u103_u0=1'h0;
reg	[31:0]	syncEnable_u1482_u0=32'h0;
wire	[31:0]	mux_u1967_u0;
wire		or_u1493_u0;
reg	[31:0]	syncEnable_u1483_u0=32'h0;
reg	[31:0]	syncEnable_u1484_u0=32'h0;
wire signed	[32:0]	lessThan_a_signed;
wire signed	[32:0]	lessThan_b_signed;
wire		lessThan;
wire		not_u907_u0;
wire		and_u3900_u0;
wire		and_u3901_u0;
wire		or_u1494_u0;
wire	[31:0]	mux_u1968_u0;
wire	[31:0]	mux_u1969_u0;
wire	[31:0]	mux_u1970_u0;
wire	[31:0]	mux_u1971_u0;
wire	[31:0]	mux_u1972_u0;
wire		mux_u1973_u0;
wire	[15:0]	add_u703;
reg	[31:0]	latch_4b25ade7_reg=32'h0;
wire	[31:0]	latch_4b25ade7_out;
wire		scoreboard_027e257a_resOr0;
reg		scoreboard_027e257a_reg1=1'h0;
wire		bus_4a9fc472_;
wire		scoreboard_027e257a_and;
wire		scoreboard_027e257a_resOr1;
reg		scoreboard_027e257a_reg0=1'h0;
wire	[31:0]	latch_6a896160_out;
reg	[31:0]	latch_6a896160_reg=32'h0;
wire	[31:0]	latch_1fe9e3d9_out;
reg	[31:0]	latch_1fe9e3d9_reg=32'h0;
reg		latch_3133f10a_reg=1'h0;
wire		latch_3133f10a_out;
reg		reg_46adda37_u0=1'h0;
reg	[31:0]	latch_76c2b81e_reg=32'h0;
wire	[31:0]	latch_76c2b81e_out;
reg	[15:0]	syncEnable_u1485_u0=16'h0;
wire	[15:0]	lessThan_u106_a_unsigned;
wire	[15:0]	lessThan_u106_b_unsigned;
wire		lessThan_u106;
wire		andOp;
wire		not_u908_u0;
wire		and_u3902_u0;
wire		and_u3903_u0;
wire		and_u3904_u0;
wire		and_u3905_u0;
wire		or_u1495_u0;
wire	[31:0]	mux_u1974_u0;
reg	[31:0]	fbReg_tmp_row_u48=32'h0;
reg		loopControl_u101=1'h0;
reg	[31:0]	fbReg_tmp_u48=32'h0;
wire	[31:0]	mux_u1975_u0;
reg	[31:0]	fbReg_tmp_row1_u48=32'h0;
wire		mux_u1976_u0;
reg		fbReg_swapped_u48=1'h0;
wire	[31:0]	mux_u1977_u0;
wire	[15:0]	mux_u1978_u0;
wire	[31:0]	mux_u1979_u0;
reg	[15:0]	fbReg_idx_u48=16'h0;
reg	[31:0]	fbReg_tmp_row0_u48=32'h0;
reg		syncEnable_u1486_u0=1'h0;
wire		and_u3906_u0;
wire	[7:0]	simplePinWrite;
wire	[15:0]	simplePinWrite_u632;
wire		simplePinWrite_u633;
reg		reg_5fb682e0_u0=1'h0;
reg		reg_070c6ae9_u0=1'h0;
wire	[31:0]	mux_u1980_u0;
wire		or_u1496_u0;
assign and_u3889_u0=and_u3900_u0&or_u1494_u0;
always @(posedge CLK)
begin
if (or_u1494_u0)
syncEnable_u1468<=mux_u1972_u0;
end
assign add=mux_u1970_u0+32'h0;
assign and_u3890_u0=and_u3889_u0&port_2e1fbae3_;
assign add_u694=mux_u1970_u0+32'h1;
assign add_u695=add_u694+32'h0;
assign and_u3891_u0=and_u3889_u0&port_76f7aacb_;
assign greaterThan_a_signed=syncEnable_u1480_u0;
assign greaterThan_b_signed=syncEnable_u1478_u0;
assign greaterThan=greaterThan_a_signed>greaterThan_b_signed;
assign not_u906_u0=~greaterThan;
assign and_u3892_u0=block_GO_delayed_u103_u0&not_u906_u0;
assign and_u3893_u0=block_GO_delayed_u103_u0&greaterThan;
assign add_u696=syncEnable_u1482_u0+32'h0;
assign and_u3894_u0=and_u3898_u0&port_2e1fbae3_;
assign add_u697=syncEnable_u1482_u0+32'h1;
assign add_u698=add_u697+32'h0;
assign and_u3895_u0=and_u3898_u0&port_76f7aacb_;
assign add_u699=syncEnable_u1482_u0+32'h0;
assign and_u3896_u0=reg_57db87c4_u0&port_76f7aacb_;
assign or_u1488_u0=and_u3896_u0|RESET;
always @(posedge CLK or posedge block_GO_delayed_u102 or posedge or_u1488_u0)
begin
if (or_u1488_u0)
reg_57db87c4_u0<=1'h0;
else if (block_GO_delayed_u102)
reg_57db87c4_u0<=1'h1;
else reg_57db87c4_u0<=reg_57db87c4_u0;
end
assign add_u700=syncEnable_u1482_u0+32'h1;
assign add_u701=add_u700+32'h0;
always @(posedge CLK or posedge block_GO_delayed_result_delayed_u20 or posedge or_u1489_u0)
begin
if (or_u1489_u0)
reg_000f981d_u0<=1'h0;
else if (block_GO_delayed_result_delayed_u20)
reg_000f981d_u0<=1'h1;
else reg_000f981d_u0<=reg_000f981d_u0;
end
assign or_u1489_u0=and_u3897_u0|RESET;
assign and_u3897_u0=reg_000f981d_u0&port_76f7aacb_;
always @(posedge CLK)
begin
if (and_u3898_u0)
syncEnable_u1469_u0<=port_6e601dbc_;
end
always @(posedge CLK or posedge RESET)
begin
if (RESET)
block_GO_delayed_u102<=1'h0;
else block_GO_delayed_u102<=and_u3898_u0;
end
always @(posedge CLK)
begin
if (and_u3898_u0)
syncEnable_u1470_u0<=port_6e601dbc_;
end
always @(posedge CLK)
begin
if (and_u3898_u0)
syncEnable_u1471_u0<=port_76e4dd2c_;
end
always @(posedge CLK or posedge RESET)
begin
if (RESET)
block_GO_delayed_result_delayed_u20<=1'h0;
else block_GO_delayed_result_delayed_u20<=block_GO_delayed_u102;
end
assign or_u1490_u0=block_GO_delayed_u102|block_GO_delayed_result_delayed_u20;
assign mux_u1961=(block_GO_delayed_u102)?syncEnable_u1470_u0:syncEnable_u1473_u0;
assign mux_u1962_u0=({32{block_GO_delayed_u102}}&syncEnable_u1474_u0)|({32{block_GO_delayed_result_delayed_u20}}&syncEnable_u1472_u0)|({32{and_u3898_u0}}&add_u698);
always @(posedge CLK)
begin
if (and_u3898_u0)
syncEnable_u1472_u0<=add_u701;
end
always @(posedge CLK)
begin
if (and_u3898_u0)
syncEnable_u1473_u0<=port_76e4dd2c_;
end
always @(posedge CLK)
begin
if (and_u3898_u0)
syncEnable_u1474_u0<=add_u699;
end
always @(posedge CLK or posedge RESET)
begin
if (RESET)
reg_1d072682_u0<=1'h0;
else reg_1d072682_u0<=reg_63486d2e_u0;
end
assign mux_u1963_u0=(and_delayed_u396)?syncEnable_u1476_u0:syncEnable_u1469_u0;
always @(posedge CLK or posedge RESET)
begin
if (RESET)
reg_63486d2e_u0<=1'h0;
else reg_63486d2e_u0<=reg_1015bbbb_result_delayed_u0;
end
assign mux_u1964_u0=(and_delayed_u396)?syncEnable_u1475_u0:1'h1;
always @(posedge CLK or posedge RESET)
begin
if (RESET)
reg_1015bbbb_u0<=1'h0;
else reg_1015bbbb_u0<=and_u3898_u0;
end
assign and_u3898_u0=and_u3893_u0&block_GO_delayed_u103_u0;
assign or_u1491_u0=and_delayed_u396|reg_1d072682_u0;
assign and_u3899_u0=and_u3892_u0&block_GO_delayed_u103_u0;
assign mux_u1965_u0=(and_delayed_u396)?syncEnable_u1477_u0:syncEnable_u1471_u0;
always @(posedge CLK or posedge RESET)
begin
if (RESET)
reg_1015bbbb_result_delayed_u0<=1'h0;
else reg_1015bbbb_result_delayed_u0<=reg_1015bbbb_u0;
end
always @(posedge CLK)
begin
if (block_GO_delayed_u103_u0)
syncEnable_u1475_u0<=syncEnable_u1479_u0;
end
always @(posedge CLK or posedge RESET)
begin
if (RESET)
and_delayed_u396<=1'h0;
else and_delayed_u396<=and_u3899_u0;
end
always @(posedge CLK)
begin
if (block_GO_delayed_u103_u0)
syncEnable_u1476_u0<=syncEnable_u1483_u0;
end
always @(posedge CLK)
begin
if (block_GO_delayed_u103_u0)
syncEnable_u1477_u0<=syncEnable_u1481_u0;
end
assign add_u702=mux_u1970_u0+32'h1;
always @(posedge CLK)
begin
if (and_u3889_u0)
syncEnable_u1478_u0<=port_6e601dbc_;
end
always @(posedge CLK)
begin
if (and_u3889_u0)
syncEnable_u1479_u0<=mux_u1973_u0;
end
assign or_u1492_u0=and_u3889_u0|and_u3898_u0;
assign mux_u1966_u0=(and_u3889_u0)?add:add_u696;
always @(posedge CLK)
begin
if (and_u3889_u0)
syncEnable_u1480_u0<=port_76e4dd2c_;
end
always @(posedge CLK)
begin
if (and_u3889_u0)
syncEnable_u1481_u0<=mux_u1972_u0;
end
always @(posedge CLK or posedge RESET)
begin
if (RESET)
block_GO_delayed_u103_u0<=1'h0;
else block_GO_delayed_u103_u0<=and_u3889_u0;
end
always @(posedge CLK)
begin
if (and_u3889_u0)
syncEnable_u1482_u0<=mux_u1970_u0;
end
assign mux_u1967_u0=({32{or_u1490_u0}}&mux_u1962_u0)|({32{and_u3889_u0}}&add_u695)|({32{and_u3898_u0}}&mux_u1962_u0);
assign or_u1493_u0=and_u3889_u0|and_u3898_u0;
always @(posedge CLK)
begin
if (and_u3889_u0)
syncEnable_u1483_u0<=mux_u1969_u0;
end
always @(posedge CLK)
begin
if (and_u3889_u0)
syncEnable_u1484_u0<=add_u702;
end
assign lessThan_a_signed={1'b0, mux_u1970_u0};
assign lessThan_b_signed=33'h64;
assign lessThan=lessThan_a_signed<lessThan_b_signed;
assign not_u907_u0=~lessThan;
assign and_u3900_u0=or_u1494_u0&lessThan;
assign and_u3901_u0=or_u1494_u0&not_u907_u0;
assign or_u1494_u0=and_u3905_u0|or_u1491_u0;
assign mux_u1968_u0=(and_u3905_u0)?mux_u1975_u0:syncEnable_u1480_u0;
assign mux_u1969_u0=(and_u3905_u0)?mux_u1974_u0:mux_u1963_u0;
assign mux_u1970_u0=(and_u3905_u0)?32'h0:syncEnable_u1484_u0;
assign mux_u1971_u0=(and_u3905_u0)?mux_u1977_u0:syncEnable_u1478_u0;
assign mux_u1972_u0=(and_u3905_u0)?mux_u1979_u0:mux_u1965_u0;
assign mux_u1973_u0=(and_u3905_u0)?1'h0:mux_u1964_u0;
assign add_u703=mux_u1978_u0+16'h1;
always @(posedge CLK)
begin
if (or_u1491_u0)
latch_4b25ade7_reg<=syncEnable_u1478_u0;
end
assign latch_4b25ade7_out=(or_u1491_u0)?syncEnable_u1478_u0:latch_4b25ade7_reg;
assign scoreboard_027e257a_resOr0=reg_46adda37_u0|scoreboard_027e257a_reg0;
always @(posedge CLK)
begin
if (bus_4a9fc472_)
scoreboard_027e257a_reg1<=1'h0;
else if (or_u1491_u0)
scoreboard_027e257a_reg1<=1'h1;
else scoreboard_027e257a_reg1<=scoreboard_027e257a_reg1;
end
assign bus_4a9fc472_=scoreboard_027e257a_and|RESET;
assign scoreboard_027e257a_and=scoreboard_027e257a_resOr0&scoreboard_027e257a_resOr1;
assign scoreboard_027e257a_resOr1=or_u1491_u0|scoreboard_027e257a_reg1;
always @(posedge CLK)
begin
if (bus_4a9fc472_)
scoreboard_027e257a_reg0<=1'h0;
else if (reg_46adda37_u0)
scoreboard_027e257a_reg0<=1'h1;
else scoreboard_027e257a_reg0<=scoreboard_027e257a_reg0;
end
assign latch_6a896160_out=(or_u1491_u0)?mux_u1963_u0:latch_6a896160_reg;
always @(posedge CLK)
begin
if (or_u1491_u0)
latch_6a896160_reg<=mux_u1963_u0;
end
assign latch_1fe9e3d9_out=(or_u1491_u0)?syncEnable_u1468:latch_1fe9e3d9_reg;
always @(posedge CLK)
begin
if (or_u1491_u0)
latch_1fe9e3d9_reg<=syncEnable_u1468;
end
always @(posedge CLK)
begin
if (or_u1491_u0)
latch_3133f10a_reg<=mux_u1964_u0;
end
assign latch_3133f10a_out=(or_u1491_u0)?mux_u1964_u0:latch_3133f10a_reg;
always @(posedge CLK or posedge RESET)
begin
if (RESET)
reg_46adda37_u0<=1'h0;
else reg_46adda37_u0<=or_u1491_u0;
end
always @(posedge CLK)
begin
if (or_u1491_u0)
latch_76c2b81e_reg<=syncEnable_u1480_u0;
end
assign latch_76c2b81e_out=(or_u1491_u0)?syncEnable_u1480_u0:latch_76c2b81e_reg;
always @(posedge CLK)
begin
if (and_u3905_u0)
syncEnable_u1485_u0<=add_u703;
end
assign lessThan_u106_a_unsigned=mux_u1978_u0;
assign lessThan_u106_b_unsigned=16'h65;
assign lessThan_u106=lessThan_u106_a_unsigned<lessThan_u106_b_unsigned;
assign andOp=lessThan_u106&mux_u1976_u0;
assign not_u908_u0=~andOp;
assign and_u3902_u0=or_u1495_u0&not_u908_u0;
assign and_u3903_u0=or_u1495_u0&andOp;
assign and_u3904_u0=and_u3902_u0&or_u1495_u0;
assign and_u3905_u0=and_u3903_u0&or_u1495_u0;
assign or_u1495_u0=GO|loopControl_u101;
assign mux_u1974_u0=(GO)?32'h0:fbReg_tmp_row1_u48;
always @(posedge CLK)
begin
if (scoreboard_027e257a_and)
fbReg_tmp_row_u48<=latch_76c2b81e_out;
end
always @(posedge CLK or posedge syncEnable_u1486_u0)
begin
if (syncEnable_u1486_u0)
loopControl_u101<=1'h0;
else loopControl_u101<=scoreboard_027e257a_and;
end
always @(posedge CLK)
begin
if (scoreboard_027e257a_and)
fbReg_tmp_u48<=latch_1fe9e3d9_out;
end
assign mux_u1975_u0=(GO)?32'h0:fbReg_tmp_row_u48;
always @(posedge CLK)
begin
if (scoreboard_027e257a_and)
fbReg_tmp_row1_u48<=latch_6a896160_out;
end
assign mux_u1976_u0=(GO)?1'h0:fbReg_swapped_u48;
always @(posedge CLK)
begin
if (scoreboard_027e257a_and)
fbReg_swapped_u48<=latch_3133f10a_out;
end
assign mux_u1977_u0=(GO)?32'h0:fbReg_tmp_row0_u48;
assign mux_u1978_u0=(GO)?16'h0:fbReg_idx_u48;
assign mux_u1979_u0=(GO)?32'h0:fbReg_tmp_u48;
always @(posedge CLK)
begin
if (scoreboard_027e257a_and)
fbReg_idx_u48<=syncEnable_u1485_u0;
end
always @(posedge CLK)
begin
if (scoreboard_027e257a_and)
fbReg_tmp_row0_u48<=latch_4b25ade7_out;
end
always @(posedge CLK)
begin
if (GO)
syncEnable_u1486_u0<=RESET;
end
assign and_u3906_u0=reg_070c6ae9_u0&port_2e1fbae3_;
assign simplePinWrite=port_76e4dd2c_[7:0];
assign simplePinWrite_u632=16'h1&{16{1'h1}};
assign simplePinWrite_u633=reg_070c6ae9_u0&{1{reg_070c6ae9_u0}};
always @(posedge CLK or posedge RESET)
begin
if (RESET)
reg_5fb682e0_u0<=1'h0;
else reg_5fb682e0_u0<=reg_070c6ae9_u0;
end
always @(posedge CLK or posedge RESET)
begin
if (RESET)
reg_070c6ae9_u0<=1'h0;
else reg_070c6ae9_u0<=and_u3904_u0;
end
assign mux_u1980_u0=(or_u1492_u0)?mux_u1966_u0:32'h32;
assign or_u1496_u0=or_u1492_u0|reg_070c6ae9_u0;
assign RESULT=GO;
assign RESULT_u2271=32'h0;
assign RESULT_u2272=or_u1493_u0;
assign RESULT_u2273=mux_u1967_u0;
assign RESULT_u2274=3'h1;
assign RESULT_u2275=or_u1490_u0;
assign RESULT_u2276=mux_u1967_u0;
assign RESULT_u2277=mux_u1961;
assign RESULT_u2278=3'h1;
assign RESULT_u2279=or_u1496_u0;
assign RESULT_u2280=mux_u1980_u0;
assign RESULT_u2281=3'h1;
assign RESULT_u2282=simplePinWrite;
assign RESULT_u2283=simplePinWrite_u633;
assign RESULT_u2284=simplePinWrite_u632;
assign DONE=reg_5fb682e0_u0;
endmodule



module medianRow14_endianswapper_2eb1ea45_(endianswapper_2eb1ea45_in, endianswapper_2eb1ea45_out);
input		endianswapper_2eb1ea45_in;
output		endianswapper_2eb1ea45_out;
assign endianswapper_2eb1ea45_out=endianswapper_2eb1ea45_in;
endmodule



module medianRow14_endianswapper_6562ff9c_(endianswapper_6562ff9c_in, endianswapper_6562ff9c_out);
input		endianswapper_6562ff9c_in;
output		endianswapper_6562ff9c_out;
assign endianswapper_6562ff9c_out=endianswapper_6562ff9c_in;
endmodule



module medianRow14_stateVar_fsmState_medianRow14(bus_45dd65fc_, bus_57fccf7b_, bus_51356527_, bus_4e5f87e9_, bus_3d7b8987_);
input		bus_45dd65fc_;
input		bus_57fccf7b_;
input		bus_51356527_;
input		bus_4e5f87e9_;
output		bus_3d7b8987_;
wire		endianswapper_2eb1ea45_out;
reg		stateVar_fsmState_medianRow14_u2=1'h0;
wire		endianswapper_6562ff9c_out;
medianRow14_endianswapper_2eb1ea45_ medianRow14_endianswapper_2eb1ea45__1(.endianswapper_2eb1ea45_in(bus_4e5f87e9_), 
  .endianswapper_2eb1ea45_out(endianswapper_2eb1ea45_out));
always @(posedge bus_45dd65fc_ or posedge bus_57fccf7b_)
begin
if (bus_57fccf7b_)
stateVar_fsmState_medianRow14_u2<=1'h0;
else if (bus_51356527_)
stateVar_fsmState_medianRow14_u2<=endianswapper_2eb1ea45_out;
end
medianRow14_endianswapper_6562ff9c_ medianRow14_endianswapper_6562ff9c__1(.endianswapper_6562ff9c_in(stateVar_fsmState_medianRow14_u2), 
  .endianswapper_6562ff9c_out(endianswapper_6562ff9c_out));
assign bus_3d7b8987_=endianswapper_6562ff9c_out;
endmodule



module medianRow14_globalreset_physical_5d88c254_(bus_6ba46f68_, bus_70bc7440_, bus_0df84855_);
input		bus_6ba46f68_;
input		bus_70bc7440_;
output		bus_0df84855_;
reg		sample_u60=1'h0;
wire		not_0caf63fa_u0;
reg		glitch_u60=1'h0;
wire		or_7d24a388_u0;
reg		cross_u60=1'h0;
wire		and_46a18c1c_u0;
reg		final_u60=1'h1;
assign bus_0df84855_=or_7d24a388_u0;
always @(posedge bus_6ba46f68_)
begin
sample_u60<=1'h1;
end
assign not_0caf63fa_u0=~and_46a18c1c_u0;
always @(posedge bus_6ba46f68_)
begin
glitch_u60<=cross_u60;
end
assign or_7d24a388_u0=bus_70bc7440_|final_u60;
always @(posedge bus_6ba46f68_)
begin
cross_u60<=sample_u60;
end
assign and_46a18c1c_u0=cross_u60&glitch_u60;
always @(posedge bus_6ba46f68_)
begin
final_u60<=not_0caf63fa_u0;
end
endmodule



module medianRow14_forge_memory_101x32_149(CLK, ENA, WEA, DINA, ENB, ADDRA, ADDRB, DOUTA, DOUTB, DONEA, DONEB);
input		CLK;
input		ENA;
input		WEA;
input	[31:0]	DINA;
input		ENB;
input	[31:0]	ADDRA;
input	[31:0]	ADDRB;
output	[31:0]	DOUTA;
output	[31:0]	DOUTB;
output		DONEA;
output		DONEB;
wire		wea_0;
wire	[31:0]	pre_douta_0;
wire	[31:0]	pre_doutb_0;
wire		wea_1;
wire	[31:0]	pre_douta_1;
wire	[31:0]	pre_doutb_1;
reg		wea_done;
reg		rea_done;
reg	[31:0]	mux_outa;
reg	[31:0]	mux_outb;
assign wea_0=WEA&(ADDRA[31:6]==26'h0);
assign wea_1=WEA&(ADDRA[31:6]==26'h1);
always @(posedge CLK)
begin
wea_done<=WEA;
end
assign DONEA=rea_done|wea_done;
always @(posedge CLK)
begin
end
assign DONEB=ENB;
always @(pre_douta_0 or pre_douta_1 or ADDRA)
begin
case (ADDRA[31:6])26'd0:mux_outa=pre_douta_0;
26'd1:mux_outa=pre_douta_1;
default:mux_outa=32'h0;
endcase end
assign DOUTA=mux_outa;
always @(pre_doutb_0 or pre_doutb_1 or ADDRB)
begin
case (ADDRB[31:6])26'd0:mux_outb=pre_doutb_0;
26'd1:mux_outb=pre_doutb_1;
default:mux_outb=32'h0;
endcase end
assign DOUTB=mux_outb;
// Memory array element: COL: 0, ROW: 0
//  Initialization of Dual Port LUT ram now done through explicit
// parameter setting.
RAM64X1D#(.INIT(64'h0000000000000000))RAM64X1D_instance_3072(.WCLK(CLK), .WE(wea_0), 
  .A0(ADDRA[0]), .A1(ADDRA[1]), .A2(ADDRA[2]), .A3(ADDRA[3]), .A4(ADDRA[4]), .A5(ADDRA[5]), 
  .D(DINA[0]), .SPO(pre_douta_0[0]), .DPRA0(ADDRB[0]), .DPRA1(ADDRB[1]), .DPRA2(ADDRB[2]), 
  .DPRA3(ADDRB[3]), .DPRA4(ADDRB[4]), .DPRA5(ADDRB[5]), .DPO(pre_doutb_0[0]));
// Memory array element: COL: 0, ROW: 1
//  Initialization of Dual Port LUT ram now done through explicit
// parameter setting.
RAM64X1D#(.INIT(64'h0000000000000000))RAM64X1D_instance_3073(.WCLK(CLK), .WE(wea_0), 
  .A0(ADDRA[0]), .A1(ADDRA[1]), .A2(ADDRA[2]), .A3(ADDRA[3]), .A4(ADDRA[4]), .A5(ADDRA[5]), 
  .D(DINA[1]), .SPO(pre_douta_0[1]), .DPRA0(ADDRB[0]), .DPRA1(ADDRB[1]), .DPRA2(ADDRB[2]), 
  .DPRA3(ADDRB[3]), .DPRA4(ADDRB[4]), .DPRA5(ADDRB[5]), .DPO(pre_doutb_0[1]));
// Memory array element: COL: 0, ROW: 2
//  Initialization of Dual Port LUT ram now done through explicit
// parameter setting.
RAM64X1D#(.INIT(64'h0000000000000000))RAM64X1D_instance_3074(.WCLK(CLK), .WE(wea_0), 
  .A0(ADDRA[0]), .A1(ADDRA[1]), .A2(ADDRA[2]), .A3(ADDRA[3]), .A4(ADDRA[4]), .A5(ADDRA[5]), 
  .D(DINA[2]), .SPO(pre_douta_0[2]), .DPRA0(ADDRB[0]), .DPRA1(ADDRB[1]), .DPRA2(ADDRB[2]), 
  .DPRA3(ADDRB[3]), .DPRA4(ADDRB[4]), .DPRA5(ADDRB[5]), .DPO(pre_doutb_0[2]));
// Memory array element: COL: 0, ROW: 3
//  Initialization of Dual Port LUT ram now done through explicit
// parameter setting.
RAM64X1D#(.INIT(64'h0000000000000000))RAM64X1D_instance_3075(.WCLK(CLK), .WE(wea_0), 
  .A0(ADDRA[0]), .A1(ADDRA[1]), .A2(ADDRA[2]), .A3(ADDRA[3]), .A4(ADDRA[4]), .A5(ADDRA[5]), 
  .D(DINA[3]), .SPO(pre_douta_0[3]), .DPRA0(ADDRB[0]), .DPRA1(ADDRB[1]), .DPRA2(ADDRB[2]), 
  .DPRA3(ADDRB[3]), .DPRA4(ADDRB[4]), .DPRA5(ADDRB[5]), .DPO(pre_doutb_0[3]));
// Memory array element: COL: 0, ROW: 4
//  Initialization of Dual Port LUT ram now done through explicit
// parameter setting.
RAM64X1D#(.INIT(64'h0000000000000000))RAM64X1D_instance_3076(.WCLK(CLK), .WE(wea_0), 
  .A0(ADDRA[0]), .A1(ADDRA[1]), .A2(ADDRA[2]), .A3(ADDRA[3]), .A4(ADDRA[4]), .A5(ADDRA[5]), 
  .D(DINA[4]), .SPO(pre_douta_0[4]), .DPRA0(ADDRB[0]), .DPRA1(ADDRB[1]), .DPRA2(ADDRB[2]), 
  .DPRA3(ADDRB[3]), .DPRA4(ADDRB[4]), .DPRA5(ADDRB[5]), .DPO(pre_doutb_0[4]));
// Memory array element: COL: 0, ROW: 5
//  Initialization of Dual Port LUT ram now done through explicit
// parameter setting.
RAM64X1D#(.INIT(64'h0000000000000000))RAM64X1D_instance_3077(.WCLK(CLK), .WE(wea_0), 
  .A0(ADDRA[0]), .A1(ADDRA[1]), .A2(ADDRA[2]), .A3(ADDRA[3]), .A4(ADDRA[4]), .A5(ADDRA[5]), 
  .D(DINA[5]), .SPO(pre_douta_0[5]), .DPRA0(ADDRB[0]), .DPRA1(ADDRB[1]), .DPRA2(ADDRB[2]), 
  .DPRA3(ADDRB[3]), .DPRA4(ADDRB[4]), .DPRA5(ADDRB[5]), .DPO(pre_doutb_0[5]));
// Memory array element: COL: 0, ROW: 6
//  Initialization of Dual Port LUT ram now done through explicit
// parameter setting.
RAM64X1D#(.INIT(64'h0000000000000000))RAM64X1D_instance_3078(.WCLK(CLK), .WE(wea_0), 
  .A0(ADDRA[0]), .A1(ADDRA[1]), .A2(ADDRA[2]), .A3(ADDRA[3]), .A4(ADDRA[4]), .A5(ADDRA[5]), 
  .D(DINA[6]), .SPO(pre_douta_0[6]), .DPRA0(ADDRB[0]), .DPRA1(ADDRB[1]), .DPRA2(ADDRB[2]), 
  .DPRA3(ADDRB[3]), .DPRA4(ADDRB[4]), .DPRA5(ADDRB[5]), .DPO(pre_doutb_0[6]));
// Memory array element: COL: 0, ROW: 7
//  Initialization of Dual Port LUT ram now done through explicit
// parameter setting.
RAM64X1D#(.INIT(64'h0000000000000000))RAM64X1D_instance_3079(.WCLK(CLK), .WE(wea_0), 
  .A0(ADDRA[0]), .A1(ADDRA[1]), .A2(ADDRA[2]), .A3(ADDRA[3]), .A4(ADDRA[4]), .A5(ADDRA[5]), 
  .D(DINA[7]), .SPO(pre_douta_0[7]), .DPRA0(ADDRB[0]), .DPRA1(ADDRB[1]), .DPRA2(ADDRB[2]), 
  .DPRA3(ADDRB[3]), .DPRA4(ADDRB[4]), .DPRA5(ADDRB[5]), .DPO(pre_doutb_0[7]));
// Memory array element: COL: 0, ROW: 8
//  Initialization of Dual Port LUT ram now done through explicit
// parameter setting.
RAM64X1D#(.INIT(64'h0000000000000000))RAM64X1D_instance_3080(.WCLK(CLK), .WE(wea_0), 
  .A0(ADDRA[0]), .A1(ADDRA[1]), .A2(ADDRA[2]), .A3(ADDRA[3]), .A4(ADDRA[4]), .A5(ADDRA[5]), 
  .D(DINA[8]), .SPO(pre_douta_0[8]), .DPRA0(ADDRB[0]), .DPRA1(ADDRB[1]), .DPRA2(ADDRB[2]), 
  .DPRA3(ADDRB[3]), .DPRA4(ADDRB[4]), .DPRA5(ADDRB[5]), .DPO(pre_doutb_0[8]));
// Memory array element: COL: 0, ROW: 9
//  Initialization of Dual Port LUT ram now done through explicit
// parameter setting.
RAM64X1D#(.INIT(64'h0000000000000000))RAM64X1D_instance_3081(.WCLK(CLK), .WE(wea_0), 
  .A0(ADDRA[0]), .A1(ADDRA[1]), .A2(ADDRA[2]), .A3(ADDRA[3]), .A4(ADDRA[4]), .A5(ADDRA[5]), 
  .D(DINA[9]), .SPO(pre_douta_0[9]), .DPRA0(ADDRB[0]), .DPRA1(ADDRB[1]), .DPRA2(ADDRB[2]), 
  .DPRA3(ADDRB[3]), .DPRA4(ADDRB[4]), .DPRA5(ADDRB[5]), .DPO(pre_doutb_0[9]));
// Memory array element: COL: 0, ROW: 10
//  Initialization of Dual Port LUT ram now done through explicit
// parameter setting.
RAM64X1D#(.INIT(64'h0000000000000000))RAM64X1D_instance_3082(.WCLK(CLK), .WE(wea_0), 
  .A0(ADDRA[0]), .A1(ADDRA[1]), .A2(ADDRA[2]), .A3(ADDRA[3]), .A4(ADDRA[4]), .A5(ADDRA[5]), 
  .D(DINA[10]), .SPO(pre_douta_0[10]), .DPRA0(ADDRB[0]), .DPRA1(ADDRB[1]), .DPRA2(ADDRB[2]), 
  .DPRA3(ADDRB[3]), .DPRA4(ADDRB[4]), .DPRA5(ADDRB[5]), .DPO(pre_doutb_0[10]));
// Memory array element: COL: 0, ROW: 11
//  Initialization of Dual Port LUT ram now done through explicit
// parameter setting.
RAM64X1D#(.INIT(64'h0000000000000000))RAM64X1D_instance_3083(.WCLK(CLK), .WE(wea_0), 
  .A0(ADDRA[0]), .A1(ADDRA[1]), .A2(ADDRA[2]), .A3(ADDRA[3]), .A4(ADDRA[4]), .A5(ADDRA[5]), 
  .D(DINA[11]), .SPO(pre_douta_0[11]), .DPRA0(ADDRB[0]), .DPRA1(ADDRB[1]), .DPRA2(ADDRB[2]), 
  .DPRA3(ADDRB[3]), .DPRA4(ADDRB[4]), .DPRA5(ADDRB[5]), .DPO(pre_doutb_0[11]));
// Memory array element: COL: 0, ROW: 12
//  Initialization of Dual Port LUT ram now done through explicit
// parameter setting.
RAM64X1D#(.INIT(64'h0000000000000000))RAM64X1D_instance_3084(.WCLK(CLK), .WE(wea_0), 
  .A0(ADDRA[0]), .A1(ADDRA[1]), .A2(ADDRA[2]), .A3(ADDRA[3]), .A4(ADDRA[4]), .A5(ADDRA[5]), 
  .D(DINA[12]), .SPO(pre_douta_0[12]), .DPRA0(ADDRB[0]), .DPRA1(ADDRB[1]), .DPRA2(ADDRB[2]), 
  .DPRA3(ADDRB[3]), .DPRA4(ADDRB[4]), .DPRA5(ADDRB[5]), .DPO(pre_doutb_0[12]));
// Memory array element: COL: 0, ROW: 13
//  Initialization of Dual Port LUT ram now done through explicit
// parameter setting.
RAM64X1D#(.INIT(64'h0000000000000000))RAM64X1D_instance_3085(.WCLK(CLK), .WE(wea_0), 
  .A0(ADDRA[0]), .A1(ADDRA[1]), .A2(ADDRA[2]), .A3(ADDRA[3]), .A4(ADDRA[4]), .A5(ADDRA[5]), 
  .D(DINA[13]), .SPO(pre_douta_0[13]), .DPRA0(ADDRB[0]), .DPRA1(ADDRB[1]), .DPRA2(ADDRB[2]), 
  .DPRA3(ADDRB[3]), .DPRA4(ADDRB[4]), .DPRA5(ADDRB[5]), .DPO(pre_doutb_0[13]));
// Memory array element: COL: 0, ROW: 14
//  Initialization of Dual Port LUT ram now done through explicit
// parameter setting.
RAM64X1D#(.INIT(64'h0000000000000000))RAM64X1D_instance_3086(.WCLK(CLK), .WE(wea_0), 
  .A0(ADDRA[0]), .A1(ADDRA[1]), .A2(ADDRA[2]), .A3(ADDRA[3]), .A4(ADDRA[4]), .A5(ADDRA[5]), 
  .D(DINA[14]), .SPO(pre_douta_0[14]), .DPRA0(ADDRB[0]), .DPRA1(ADDRB[1]), .DPRA2(ADDRB[2]), 
  .DPRA3(ADDRB[3]), .DPRA4(ADDRB[4]), .DPRA5(ADDRB[5]), .DPO(pre_doutb_0[14]));
// Memory array element: COL: 0, ROW: 15
//  Initialization of Dual Port LUT ram now done through explicit
// parameter setting.
RAM64X1D#(.INIT(64'h0000000000000000))RAM64X1D_instance_3087(.WCLK(CLK), .WE(wea_0), 
  .A0(ADDRA[0]), .A1(ADDRA[1]), .A2(ADDRA[2]), .A3(ADDRA[3]), .A4(ADDRA[4]), .A5(ADDRA[5]), 
  .D(DINA[15]), .SPO(pre_douta_0[15]), .DPRA0(ADDRB[0]), .DPRA1(ADDRB[1]), .DPRA2(ADDRB[2]), 
  .DPRA3(ADDRB[3]), .DPRA4(ADDRB[4]), .DPRA5(ADDRB[5]), .DPO(pre_doutb_0[15]));
// Memory array element: COL: 0, ROW: 16
//  Initialization of Dual Port LUT ram now done through explicit
// parameter setting.
RAM64X1D#(.INIT(64'h0000000000000000))RAM64X1D_instance_3088(.WCLK(CLK), .WE(wea_0), 
  .A0(ADDRA[0]), .A1(ADDRA[1]), .A2(ADDRA[2]), .A3(ADDRA[3]), .A4(ADDRA[4]), .A5(ADDRA[5]), 
  .D(DINA[16]), .SPO(pre_douta_0[16]), .DPRA0(ADDRB[0]), .DPRA1(ADDRB[1]), .DPRA2(ADDRB[2]), 
  .DPRA3(ADDRB[3]), .DPRA4(ADDRB[4]), .DPRA5(ADDRB[5]), .DPO(pre_doutb_0[16]));
// Memory array element: COL: 0, ROW: 17
//  Initialization of Dual Port LUT ram now done through explicit
// parameter setting.
RAM64X1D#(.INIT(64'h0000000000000000))RAM64X1D_instance_3089(.WCLK(CLK), .WE(wea_0), 
  .A0(ADDRA[0]), .A1(ADDRA[1]), .A2(ADDRA[2]), .A3(ADDRA[3]), .A4(ADDRA[4]), .A5(ADDRA[5]), 
  .D(DINA[17]), .SPO(pre_douta_0[17]), .DPRA0(ADDRB[0]), .DPRA1(ADDRB[1]), .DPRA2(ADDRB[2]), 
  .DPRA3(ADDRB[3]), .DPRA4(ADDRB[4]), .DPRA5(ADDRB[5]), .DPO(pre_doutb_0[17]));
// Memory array element: COL: 0, ROW: 18
//  Initialization of Dual Port LUT ram now done through explicit
// parameter setting.
RAM64X1D#(.INIT(64'h0000000000000000))RAM64X1D_instance_3090(.WCLK(CLK), .WE(wea_0), 
  .A0(ADDRA[0]), .A1(ADDRA[1]), .A2(ADDRA[2]), .A3(ADDRA[3]), .A4(ADDRA[4]), .A5(ADDRA[5]), 
  .D(DINA[18]), .SPO(pre_douta_0[18]), .DPRA0(ADDRB[0]), .DPRA1(ADDRB[1]), .DPRA2(ADDRB[2]), 
  .DPRA3(ADDRB[3]), .DPRA4(ADDRB[4]), .DPRA5(ADDRB[5]), .DPO(pre_doutb_0[18]));
// Memory array element: COL: 0, ROW: 19
//  Initialization of Dual Port LUT ram now done through explicit
// parameter setting.
RAM64X1D#(.INIT(64'h0000000000000000))RAM64X1D_instance_3091(.WCLK(CLK), .WE(wea_0), 
  .A0(ADDRA[0]), .A1(ADDRA[1]), .A2(ADDRA[2]), .A3(ADDRA[3]), .A4(ADDRA[4]), .A5(ADDRA[5]), 
  .D(DINA[19]), .SPO(pre_douta_0[19]), .DPRA0(ADDRB[0]), .DPRA1(ADDRB[1]), .DPRA2(ADDRB[2]), 
  .DPRA3(ADDRB[3]), .DPRA4(ADDRB[4]), .DPRA5(ADDRB[5]), .DPO(pre_doutb_0[19]));
// Memory array element: COL: 0, ROW: 20
//  Initialization of Dual Port LUT ram now done through explicit
// parameter setting.
RAM64X1D#(.INIT(64'h0000000000000000))RAM64X1D_instance_3092(.WCLK(CLK), .WE(wea_0), 
  .A0(ADDRA[0]), .A1(ADDRA[1]), .A2(ADDRA[2]), .A3(ADDRA[3]), .A4(ADDRA[4]), .A5(ADDRA[5]), 
  .D(DINA[20]), .SPO(pre_douta_0[20]), .DPRA0(ADDRB[0]), .DPRA1(ADDRB[1]), .DPRA2(ADDRB[2]), 
  .DPRA3(ADDRB[3]), .DPRA4(ADDRB[4]), .DPRA5(ADDRB[5]), .DPO(pre_doutb_0[20]));
// Memory array element: COL: 0, ROW: 21
//  Initialization of Dual Port LUT ram now done through explicit
// parameter setting.
RAM64X1D#(.INIT(64'h0000000000000000))RAM64X1D_instance_3093(.WCLK(CLK), .WE(wea_0), 
  .A0(ADDRA[0]), .A1(ADDRA[1]), .A2(ADDRA[2]), .A3(ADDRA[3]), .A4(ADDRA[4]), .A5(ADDRA[5]), 
  .D(DINA[21]), .SPO(pre_douta_0[21]), .DPRA0(ADDRB[0]), .DPRA1(ADDRB[1]), .DPRA2(ADDRB[2]), 
  .DPRA3(ADDRB[3]), .DPRA4(ADDRB[4]), .DPRA5(ADDRB[5]), .DPO(pre_doutb_0[21]));
// Memory array element: COL: 0, ROW: 22
//  Initialization of Dual Port LUT ram now done through explicit
// parameter setting.
RAM64X1D#(.INIT(64'h0000000000000000))RAM64X1D_instance_3094(.WCLK(CLK), .WE(wea_0), 
  .A0(ADDRA[0]), .A1(ADDRA[1]), .A2(ADDRA[2]), .A3(ADDRA[3]), .A4(ADDRA[4]), .A5(ADDRA[5]), 
  .D(DINA[22]), .SPO(pre_douta_0[22]), .DPRA0(ADDRB[0]), .DPRA1(ADDRB[1]), .DPRA2(ADDRB[2]), 
  .DPRA3(ADDRB[3]), .DPRA4(ADDRB[4]), .DPRA5(ADDRB[5]), .DPO(pre_doutb_0[22]));
// Memory array element: COL: 0, ROW: 23
//  Initialization of Dual Port LUT ram now done through explicit
// parameter setting.
RAM64X1D#(.INIT(64'h0000000000000000))RAM64X1D_instance_3095(.WCLK(CLK), .WE(wea_0), 
  .A0(ADDRA[0]), .A1(ADDRA[1]), .A2(ADDRA[2]), .A3(ADDRA[3]), .A4(ADDRA[4]), .A5(ADDRA[5]), 
  .D(DINA[23]), .SPO(pre_douta_0[23]), .DPRA0(ADDRB[0]), .DPRA1(ADDRB[1]), .DPRA2(ADDRB[2]), 
  .DPRA3(ADDRB[3]), .DPRA4(ADDRB[4]), .DPRA5(ADDRB[5]), .DPO(pre_doutb_0[23]));
// Memory array element: COL: 0, ROW: 24
//  Initialization of Dual Port LUT ram now done through explicit
// parameter setting.
RAM64X1D#(.INIT(64'h0000000000000000))RAM64X1D_instance_3096(.WCLK(CLK), .WE(wea_0), 
  .A0(ADDRA[0]), .A1(ADDRA[1]), .A2(ADDRA[2]), .A3(ADDRA[3]), .A4(ADDRA[4]), .A5(ADDRA[5]), 
  .D(DINA[24]), .SPO(pre_douta_0[24]), .DPRA0(ADDRB[0]), .DPRA1(ADDRB[1]), .DPRA2(ADDRB[2]), 
  .DPRA3(ADDRB[3]), .DPRA4(ADDRB[4]), .DPRA5(ADDRB[5]), .DPO(pre_doutb_0[24]));
// Memory array element: COL: 0, ROW: 25
//  Initialization of Dual Port LUT ram now done through explicit
// parameter setting.
RAM64X1D#(.INIT(64'h0000000000000000))RAM64X1D_instance_3097(.WCLK(CLK), .WE(wea_0), 
  .A0(ADDRA[0]), .A1(ADDRA[1]), .A2(ADDRA[2]), .A3(ADDRA[3]), .A4(ADDRA[4]), .A5(ADDRA[5]), 
  .D(DINA[25]), .SPO(pre_douta_0[25]), .DPRA0(ADDRB[0]), .DPRA1(ADDRB[1]), .DPRA2(ADDRB[2]), 
  .DPRA3(ADDRB[3]), .DPRA4(ADDRB[4]), .DPRA5(ADDRB[5]), .DPO(pre_doutb_0[25]));
// Memory array element: COL: 0, ROW: 26
//  Initialization of Dual Port LUT ram now done through explicit
// parameter setting.
RAM64X1D#(.INIT(64'h0000000000000000))RAM64X1D_instance_3098(.WCLK(CLK), .WE(wea_0), 
  .A0(ADDRA[0]), .A1(ADDRA[1]), .A2(ADDRA[2]), .A3(ADDRA[3]), .A4(ADDRA[4]), .A5(ADDRA[5]), 
  .D(DINA[26]), .SPO(pre_douta_0[26]), .DPRA0(ADDRB[0]), .DPRA1(ADDRB[1]), .DPRA2(ADDRB[2]), 
  .DPRA3(ADDRB[3]), .DPRA4(ADDRB[4]), .DPRA5(ADDRB[5]), .DPO(pre_doutb_0[26]));
// Memory array element: COL: 0, ROW: 27
//  Initialization of Dual Port LUT ram now done through explicit
// parameter setting.
RAM64X1D#(.INIT(64'h0000000000000000))RAM64X1D_instance_3099(.WCLK(CLK), .WE(wea_0), 
  .A0(ADDRA[0]), .A1(ADDRA[1]), .A2(ADDRA[2]), .A3(ADDRA[3]), .A4(ADDRA[4]), .A5(ADDRA[5]), 
  .D(DINA[27]), .SPO(pre_douta_0[27]), .DPRA0(ADDRB[0]), .DPRA1(ADDRB[1]), .DPRA2(ADDRB[2]), 
  .DPRA3(ADDRB[3]), .DPRA4(ADDRB[4]), .DPRA5(ADDRB[5]), .DPO(pre_doutb_0[27]));
// Memory array element: COL: 0, ROW: 28
//  Initialization of Dual Port LUT ram now done through explicit
// parameter setting.
RAM64X1D#(.INIT(64'h0000000000000000))RAM64X1D_instance_3100(.WCLK(CLK), .WE(wea_0), 
  .A0(ADDRA[0]), .A1(ADDRA[1]), .A2(ADDRA[2]), .A3(ADDRA[3]), .A4(ADDRA[4]), .A5(ADDRA[5]), 
  .D(DINA[28]), .SPO(pre_douta_0[28]), .DPRA0(ADDRB[0]), .DPRA1(ADDRB[1]), .DPRA2(ADDRB[2]), 
  .DPRA3(ADDRB[3]), .DPRA4(ADDRB[4]), .DPRA5(ADDRB[5]), .DPO(pre_doutb_0[28]));
// Memory array element: COL: 0, ROW: 29
//  Initialization of Dual Port LUT ram now done through explicit
// parameter setting.
RAM64X1D#(.INIT(64'h0000000000000000))RAM64X1D_instance_3101(.WCLK(CLK), .WE(wea_0), 
  .A0(ADDRA[0]), .A1(ADDRA[1]), .A2(ADDRA[2]), .A3(ADDRA[3]), .A4(ADDRA[4]), .A5(ADDRA[5]), 
  .D(DINA[29]), .SPO(pre_douta_0[29]), .DPRA0(ADDRB[0]), .DPRA1(ADDRB[1]), .DPRA2(ADDRB[2]), 
  .DPRA3(ADDRB[3]), .DPRA4(ADDRB[4]), .DPRA5(ADDRB[5]), .DPO(pre_doutb_0[29]));
// Memory array element: COL: 0, ROW: 30
//  Initialization of Dual Port LUT ram now done through explicit
// parameter setting.
RAM64X1D#(.INIT(64'h0000000000000000))RAM64X1D_instance_3102(.WCLK(CLK), .WE(wea_0), 
  .A0(ADDRA[0]), .A1(ADDRA[1]), .A2(ADDRA[2]), .A3(ADDRA[3]), .A4(ADDRA[4]), .A5(ADDRA[5]), 
  .D(DINA[30]), .SPO(pre_douta_0[30]), .DPRA0(ADDRB[0]), .DPRA1(ADDRB[1]), .DPRA2(ADDRB[2]), 
  .DPRA3(ADDRB[3]), .DPRA4(ADDRB[4]), .DPRA5(ADDRB[5]), .DPO(pre_doutb_0[30]));
// Memory array element: COL: 0, ROW: 31
//  Initialization of Dual Port LUT ram now done through explicit
// parameter setting.
RAM64X1D#(.INIT(64'h0000000000000000))RAM64X1D_instance_3103(.WCLK(CLK), .WE(wea_0), 
  .A0(ADDRA[0]), .A1(ADDRA[1]), .A2(ADDRA[2]), .A3(ADDRA[3]), .A4(ADDRA[4]), .A5(ADDRA[5]), 
  .D(DINA[31]), .SPO(pre_douta_0[31]), .DPRA0(ADDRB[0]), .DPRA1(ADDRB[1]), .DPRA2(ADDRB[2]), 
  .DPRA3(ADDRB[3]), .DPRA4(ADDRB[4]), .DPRA5(ADDRB[5]), .DPO(pre_doutb_0[31]));
// Memory array element: COL: 1, ROW: 0
//  Initialization of Dual Port LUT ram now done through explicit
// parameter setting.
RAM64X1D#(.INIT(64'h0000000000000000))RAM64X1D_instance_3104(.WCLK(CLK), .WE(wea_1), 
  .A0(ADDRA[0]), .A1(ADDRA[1]), .A2(ADDRA[2]), .A3(ADDRA[3]), .A4(ADDRA[4]), .A5(ADDRA[5]), 
  .D(DINA[0]), .SPO(pre_douta_1[0]), .DPRA0(ADDRB[0]), .DPRA1(ADDRB[1]), .DPRA2(ADDRB[2]), 
  .DPRA3(ADDRB[3]), .DPRA4(ADDRB[4]), .DPRA5(ADDRB[5]), .DPO(pre_doutb_1[0]));
// Memory array element: COL: 1, ROW: 1
//  Initialization of Dual Port LUT ram now done through explicit
// parameter setting.
RAM64X1D#(.INIT(64'h0000000000000000))RAM64X1D_instance_3105(.WCLK(CLK), .WE(wea_1), 
  .A0(ADDRA[0]), .A1(ADDRA[1]), .A2(ADDRA[2]), .A3(ADDRA[3]), .A4(ADDRA[4]), .A5(ADDRA[5]), 
  .D(DINA[1]), .SPO(pre_douta_1[1]), .DPRA0(ADDRB[0]), .DPRA1(ADDRB[1]), .DPRA2(ADDRB[2]), 
  .DPRA3(ADDRB[3]), .DPRA4(ADDRB[4]), .DPRA5(ADDRB[5]), .DPO(pre_doutb_1[1]));
// Memory array element: COL: 1, ROW: 2
//  Initialization of Dual Port LUT ram now done through explicit
// parameter setting.
RAM64X1D#(.INIT(64'h0000000000000000))RAM64X1D_instance_3106(.WCLK(CLK), .WE(wea_1), 
  .A0(ADDRA[0]), .A1(ADDRA[1]), .A2(ADDRA[2]), .A3(ADDRA[3]), .A4(ADDRA[4]), .A5(ADDRA[5]), 
  .D(DINA[2]), .SPO(pre_douta_1[2]), .DPRA0(ADDRB[0]), .DPRA1(ADDRB[1]), .DPRA2(ADDRB[2]), 
  .DPRA3(ADDRB[3]), .DPRA4(ADDRB[4]), .DPRA5(ADDRB[5]), .DPO(pre_doutb_1[2]));
// Memory array element: COL: 1, ROW: 3
//  Initialization of Dual Port LUT ram now done through explicit
// parameter setting.
RAM64X1D#(.INIT(64'h0000000000000000))RAM64X1D_instance_3107(.WCLK(CLK), .WE(wea_1), 
  .A0(ADDRA[0]), .A1(ADDRA[1]), .A2(ADDRA[2]), .A3(ADDRA[3]), .A4(ADDRA[4]), .A5(ADDRA[5]), 
  .D(DINA[3]), .SPO(pre_douta_1[3]), .DPRA0(ADDRB[0]), .DPRA1(ADDRB[1]), .DPRA2(ADDRB[2]), 
  .DPRA3(ADDRB[3]), .DPRA4(ADDRB[4]), .DPRA5(ADDRB[5]), .DPO(pre_doutb_1[3]));
// Memory array element: COL: 1, ROW: 4
//  Initialization of Dual Port LUT ram now done through explicit
// parameter setting.
RAM64X1D#(.INIT(64'h0000000000000000))RAM64X1D_instance_3108(.WCLK(CLK), .WE(wea_1), 
  .A0(ADDRA[0]), .A1(ADDRA[1]), .A2(ADDRA[2]), .A3(ADDRA[3]), .A4(ADDRA[4]), .A5(ADDRA[5]), 
  .D(DINA[4]), .SPO(pre_douta_1[4]), .DPRA0(ADDRB[0]), .DPRA1(ADDRB[1]), .DPRA2(ADDRB[2]), 
  .DPRA3(ADDRB[3]), .DPRA4(ADDRB[4]), .DPRA5(ADDRB[5]), .DPO(pre_doutb_1[4]));
// Memory array element: COL: 1, ROW: 5
//  Initialization of Dual Port LUT ram now done through explicit
// parameter setting.
RAM64X1D#(.INIT(64'h0000000000000000))RAM64X1D_instance_3109(.WCLK(CLK), .WE(wea_1), 
  .A0(ADDRA[0]), .A1(ADDRA[1]), .A2(ADDRA[2]), .A3(ADDRA[3]), .A4(ADDRA[4]), .A5(ADDRA[5]), 
  .D(DINA[5]), .SPO(pre_douta_1[5]), .DPRA0(ADDRB[0]), .DPRA1(ADDRB[1]), .DPRA2(ADDRB[2]), 
  .DPRA3(ADDRB[3]), .DPRA4(ADDRB[4]), .DPRA5(ADDRB[5]), .DPO(pre_doutb_1[5]));
// Memory array element: COL: 1, ROW: 6
//  Initialization of Dual Port LUT ram now done through explicit
// parameter setting.
RAM64X1D#(.INIT(64'h0000000000000000))RAM64X1D_instance_3110(.WCLK(CLK), .WE(wea_1), 
  .A0(ADDRA[0]), .A1(ADDRA[1]), .A2(ADDRA[2]), .A3(ADDRA[3]), .A4(ADDRA[4]), .A5(ADDRA[5]), 
  .D(DINA[6]), .SPO(pre_douta_1[6]), .DPRA0(ADDRB[0]), .DPRA1(ADDRB[1]), .DPRA2(ADDRB[2]), 
  .DPRA3(ADDRB[3]), .DPRA4(ADDRB[4]), .DPRA5(ADDRB[5]), .DPO(pre_doutb_1[6]));
// Memory array element: COL: 1, ROW: 7
//  Initialization of Dual Port LUT ram now done through explicit
// parameter setting.
RAM64X1D#(.INIT(64'h0000000000000000))RAM64X1D_instance_3111(.WCLK(CLK), .WE(wea_1), 
  .A0(ADDRA[0]), .A1(ADDRA[1]), .A2(ADDRA[2]), .A3(ADDRA[3]), .A4(ADDRA[4]), .A5(ADDRA[5]), 
  .D(DINA[7]), .SPO(pre_douta_1[7]), .DPRA0(ADDRB[0]), .DPRA1(ADDRB[1]), .DPRA2(ADDRB[2]), 
  .DPRA3(ADDRB[3]), .DPRA4(ADDRB[4]), .DPRA5(ADDRB[5]), .DPO(pre_doutb_1[7]));
// Memory array element: COL: 1, ROW: 8
//  Initialization of Dual Port LUT ram now done through explicit
// parameter setting.
RAM64X1D#(.INIT(64'h0000000000000000))RAM64X1D_instance_3112(.WCLK(CLK), .WE(wea_1), 
  .A0(ADDRA[0]), .A1(ADDRA[1]), .A2(ADDRA[2]), .A3(ADDRA[3]), .A4(ADDRA[4]), .A5(ADDRA[5]), 
  .D(DINA[8]), .SPO(pre_douta_1[8]), .DPRA0(ADDRB[0]), .DPRA1(ADDRB[1]), .DPRA2(ADDRB[2]), 
  .DPRA3(ADDRB[3]), .DPRA4(ADDRB[4]), .DPRA5(ADDRB[5]), .DPO(pre_doutb_1[8]));
// Memory array element: COL: 1, ROW: 9
//  Initialization of Dual Port LUT ram now done through explicit
// parameter setting.
RAM64X1D#(.INIT(64'h0000000000000000))RAM64X1D_instance_3113(.WCLK(CLK), .WE(wea_1), 
  .A0(ADDRA[0]), .A1(ADDRA[1]), .A2(ADDRA[2]), .A3(ADDRA[3]), .A4(ADDRA[4]), .A5(ADDRA[5]), 
  .D(DINA[9]), .SPO(pre_douta_1[9]), .DPRA0(ADDRB[0]), .DPRA1(ADDRB[1]), .DPRA2(ADDRB[2]), 
  .DPRA3(ADDRB[3]), .DPRA4(ADDRB[4]), .DPRA5(ADDRB[5]), .DPO(pre_doutb_1[9]));
// Memory array element: COL: 1, ROW: 10
//  Initialization of Dual Port LUT ram now done through explicit
// parameter setting.
RAM64X1D#(.INIT(64'h0000000000000000))RAM64X1D_instance_3114(.WCLK(CLK), .WE(wea_1), 
  .A0(ADDRA[0]), .A1(ADDRA[1]), .A2(ADDRA[2]), .A3(ADDRA[3]), .A4(ADDRA[4]), .A5(ADDRA[5]), 
  .D(DINA[10]), .SPO(pre_douta_1[10]), .DPRA0(ADDRB[0]), .DPRA1(ADDRB[1]), .DPRA2(ADDRB[2]), 
  .DPRA3(ADDRB[3]), .DPRA4(ADDRB[4]), .DPRA5(ADDRB[5]), .DPO(pre_doutb_1[10]));
// Memory array element: COL: 1, ROW: 11
//  Initialization of Dual Port LUT ram now done through explicit
// parameter setting.
RAM64X1D#(.INIT(64'h0000000000000000))RAM64X1D_instance_3115(.WCLK(CLK), .WE(wea_1), 
  .A0(ADDRA[0]), .A1(ADDRA[1]), .A2(ADDRA[2]), .A3(ADDRA[3]), .A4(ADDRA[4]), .A5(ADDRA[5]), 
  .D(DINA[11]), .SPO(pre_douta_1[11]), .DPRA0(ADDRB[0]), .DPRA1(ADDRB[1]), .DPRA2(ADDRB[2]), 
  .DPRA3(ADDRB[3]), .DPRA4(ADDRB[4]), .DPRA5(ADDRB[5]), .DPO(pre_doutb_1[11]));
// Memory array element: COL: 1, ROW: 12
//  Initialization of Dual Port LUT ram now done through explicit
// parameter setting.
RAM64X1D#(.INIT(64'h0000000000000000))RAM64X1D_instance_3116(.WCLK(CLK), .WE(wea_1), 
  .A0(ADDRA[0]), .A1(ADDRA[1]), .A2(ADDRA[2]), .A3(ADDRA[3]), .A4(ADDRA[4]), .A5(ADDRA[5]), 
  .D(DINA[12]), .SPO(pre_douta_1[12]), .DPRA0(ADDRB[0]), .DPRA1(ADDRB[1]), .DPRA2(ADDRB[2]), 
  .DPRA3(ADDRB[3]), .DPRA4(ADDRB[4]), .DPRA5(ADDRB[5]), .DPO(pre_doutb_1[12]));
// Memory array element: COL: 1, ROW: 13
//  Initialization of Dual Port LUT ram now done through explicit
// parameter setting.
RAM64X1D#(.INIT(64'h0000000000000000))RAM64X1D_instance_3117(.WCLK(CLK), .WE(wea_1), 
  .A0(ADDRA[0]), .A1(ADDRA[1]), .A2(ADDRA[2]), .A3(ADDRA[3]), .A4(ADDRA[4]), .A5(ADDRA[5]), 
  .D(DINA[13]), .SPO(pre_douta_1[13]), .DPRA0(ADDRB[0]), .DPRA1(ADDRB[1]), .DPRA2(ADDRB[2]), 
  .DPRA3(ADDRB[3]), .DPRA4(ADDRB[4]), .DPRA5(ADDRB[5]), .DPO(pre_doutb_1[13]));
// Memory array element: COL: 1, ROW: 14
//  Initialization of Dual Port LUT ram now done through explicit
// parameter setting.
RAM64X1D#(.INIT(64'h0000000000000000))RAM64X1D_instance_3118(.WCLK(CLK), .WE(wea_1), 
  .A0(ADDRA[0]), .A1(ADDRA[1]), .A2(ADDRA[2]), .A3(ADDRA[3]), .A4(ADDRA[4]), .A5(ADDRA[5]), 
  .D(DINA[14]), .SPO(pre_douta_1[14]), .DPRA0(ADDRB[0]), .DPRA1(ADDRB[1]), .DPRA2(ADDRB[2]), 
  .DPRA3(ADDRB[3]), .DPRA4(ADDRB[4]), .DPRA5(ADDRB[5]), .DPO(pre_doutb_1[14]));
// Memory array element: COL: 1, ROW: 15
//  Initialization of Dual Port LUT ram now done through explicit
// parameter setting.
RAM64X1D#(.INIT(64'h0000000000000000))RAM64X1D_instance_3119(.WCLK(CLK), .WE(wea_1), 
  .A0(ADDRA[0]), .A1(ADDRA[1]), .A2(ADDRA[2]), .A3(ADDRA[3]), .A4(ADDRA[4]), .A5(ADDRA[5]), 
  .D(DINA[15]), .SPO(pre_douta_1[15]), .DPRA0(ADDRB[0]), .DPRA1(ADDRB[1]), .DPRA2(ADDRB[2]), 
  .DPRA3(ADDRB[3]), .DPRA4(ADDRB[4]), .DPRA5(ADDRB[5]), .DPO(pre_doutb_1[15]));
// Memory array element: COL: 1, ROW: 16
//  Initialization of Dual Port LUT ram now done through explicit
// parameter setting.
RAM64X1D#(.INIT(64'h0000000000000000))RAM64X1D_instance_3120(.WCLK(CLK), .WE(wea_1), 
  .A0(ADDRA[0]), .A1(ADDRA[1]), .A2(ADDRA[2]), .A3(ADDRA[3]), .A4(ADDRA[4]), .A5(ADDRA[5]), 
  .D(DINA[16]), .SPO(pre_douta_1[16]), .DPRA0(ADDRB[0]), .DPRA1(ADDRB[1]), .DPRA2(ADDRB[2]), 
  .DPRA3(ADDRB[3]), .DPRA4(ADDRB[4]), .DPRA5(ADDRB[5]), .DPO(pre_doutb_1[16]));
// Memory array element: COL: 1, ROW: 17
//  Initialization of Dual Port LUT ram now done through explicit
// parameter setting.
RAM64X1D#(.INIT(64'h0000000000000000))RAM64X1D_instance_3121(.WCLK(CLK), .WE(wea_1), 
  .A0(ADDRA[0]), .A1(ADDRA[1]), .A2(ADDRA[2]), .A3(ADDRA[3]), .A4(ADDRA[4]), .A5(ADDRA[5]), 
  .D(DINA[17]), .SPO(pre_douta_1[17]), .DPRA0(ADDRB[0]), .DPRA1(ADDRB[1]), .DPRA2(ADDRB[2]), 
  .DPRA3(ADDRB[3]), .DPRA4(ADDRB[4]), .DPRA5(ADDRB[5]), .DPO(pre_doutb_1[17]));
// Memory array element: COL: 1, ROW: 18
//  Initialization of Dual Port LUT ram now done through explicit
// parameter setting.
RAM64X1D#(.INIT(64'h0000000000000000))RAM64X1D_instance_3122(.WCLK(CLK), .WE(wea_1), 
  .A0(ADDRA[0]), .A1(ADDRA[1]), .A2(ADDRA[2]), .A3(ADDRA[3]), .A4(ADDRA[4]), .A5(ADDRA[5]), 
  .D(DINA[18]), .SPO(pre_douta_1[18]), .DPRA0(ADDRB[0]), .DPRA1(ADDRB[1]), .DPRA2(ADDRB[2]), 
  .DPRA3(ADDRB[3]), .DPRA4(ADDRB[4]), .DPRA5(ADDRB[5]), .DPO(pre_doutb_1[18]));
// Memory array element: COL: 1, ROW: 19
//  Initialization of Dual Port LUT ram now done through explicit
// parameter setting.
RAM64X1D#(.INIT(64'h0000000000000000))RAM64X1D_instance_3123(.WCLK(CLK), .WE(wea_1), 
  .A0(ADDRA[0]), .A1(ADDRA[1]), .A2(ADDRA[2]), .A3(ADDRA[3]), .A4(ADDRA[4]), .A5(ADDRA[5]), 
  .D(DINA[19]), .SPO(pre_douta_1[19]), .DPRA0(ADDRB[0]), .DPRA1(ADDRB[1]), .DPRA2(ADDRB[2]), 
  .DPRA3(ADDRB[3]), .DPRA4(ADDRB[4]), .DPRA5(ADDRB[5]), .DPO(pre_doutb_1[19]));
// Memory array element: COL: 1, ROW: 20
//  Initialization of Dual Port LUT ram now done through explicit
// parameter setting.
RAM64X1D#(.INIT(64'h0000000000000000))RAM64X1D_instance_3124(.WCLK(CLK), .WE(wea_1), 
  .A0(ADDRA[0]), .A1(ADDRA[1]), .A2(ADDRA[2]), .A3(ADDRA[3]), .A4(ADDRA[4]), .A5(ADDRA[5]), 
  .D(DINA[20]), .SPO(pre_douta_1[20]), .DPRA0(ADDRB[0]), .DPRA1(ADDRB[1]), .DPRA2(ADDRB[2]), 
  .DPRA3(ADDRB[3]), .DPRA4(ADDRB[4]), .DPRA5(ADDRB[5]), .DPO(pre_doutb_1[20]));
// Memory array element: COL: 1, ROW: 21
//  Initialization of Dual Port LUT ram now done through explicit
// parameter setting.
RAM64X1D#(.INIT(64'h0000000000000000))RAM64X1D_instance_3125(.WCLK(CLK), .WE(wea_1), 
  .A0(ADDRA[0]), .A1(ADDRA[1]), .A2(ADDRA[2]), .A3(ADDRA[3]), .A4(ADDRA[4]), .A5(ADDRA[5]), 
  .D(DINA[21]), .SPO(pre_douta_1[21]), .DPRA0(ADDRB[0]), .DPRA1(ADDRB[1]), .DPRA2(ADDRB[2]), 
  .DPRA3(ADDRB[3]), .DPRA4(ADDRB[4]), .DPRA5(ADDRB[5]), .DPO(pre_doutb_1[21]));
// Memory array element: COL: 1, ROW: 22
//  Initialization of Dual Port LUT ram now done through explicit
// parameter setting.
RAM64X1D#(.INIT(64'h0000000000000000))RAM64X1D_instance_3126(.WCLK(CLK), .WE(wea_1), 
  .A0(ADDRA[0]), .A1(ADDRA[1]), .A2(ADDRA[2]), .A3(ADDRA[3]), .A4(ADDRA[4]), .A5(ADDRA[5]), 
  .D(DINA[22]), .SPO(pre_douta_1[22]), .DPRA0(ADDRB[0]), .DPRA1(ADDRB[1]), .DPRA2(ADDRB[2]), 
  .DPRA3(ADDRB[3]), .DPRA4(ADDRB[4]), .DPRA5(ADDRB[5]), .DPO(pre_doutb_1[22]));
// Memory array element: COL: 1, ROW: 23
//  Initialization of Dual Port LUT ram now done through explicit
// parameter setting.
RAM64X1D#(.INIT(64'h0000000000000000))RAM64X1D_instance_3127(.WCLK(CLK), .WE(wea_1), 
  .A0(ADDRA[0]), .A1(ADDRA[1]), .A2(ADDRA[2]), .A3(ADDRA[3]), .A4(ADDRA[4]), .A5(ADDRA[5]), 
  .D(DINA[23]), .SPO(pre_douta_1[23]), .DPRA0(ADDRB[0]), .DPRA1(ADDRB[1]), .DPRA2(ADDRB[2]), 
  .DPRA3(ADDRB[3]), .DPRA4(ADDRB[4]), .DPRA5(ADDRB[5]), .DPO(pre_doutb_1[23]));
// Memory array element: COL: 1, ROW: 24
//  Initialization of Dual Port LUT ram now done through explicit
// parameter setting.
RAM64X1D#(.INIT(64'h0000000000000000))RAM64X1D_instance_3128(.WCLK(CLK), .WE(wea_1), 
  .A0(ADDRA[0]), .A1(ADDRA[1]), .A2(ADDRA[2]), .A3(ADDRA[3]), .A4(ADDRA[4]), .A5(ADDRA[5]), 
  .D(DINA[24]), .SPO(pre_douta_1[24]), .DPRA0(ADDRB[0]), .DPRA1(ADDRB[1]), .DPRA2(ADDRB[2]), 
  .DPRA3(ADDRB[3]), .DPRA4(ADDRB[4]), .DPRA5(ADDRB[5]), .DPO(pre_doutb_1[24]));
// Memory array element: COL: 1, ROW: 25
//  Initialization of Dual Port LUT ram now done through explicit
// parameter setting.
RAM64X1D#(.INIT(64'h0000000000000000))RAM64X1D_instance_3129(.WCLK(CLK), .WE(wea_1), 
  .A0(ADDRA[0]), .A1(ADDRA[1]), .A2(ADDRA[2]), .A3(ADDRA[3]), .A4(ADDRA[4]), .A5(ADDRA[5]), 
  .D(DINA[25]), .SPO(pre_douta_1[25]), .DPRA0(ADDRB[0]), .DPRA1(ADDRB[1]), .DPRA2(ADDRB[2]), 
  .DPRA3(ADDRB[3]), .DPRA4(ADDRB[4]), .DPRA5(ADDRB[5]), .DPO(pre_doutb_1[25]));
// Memory array element: COL: 1, ROW: 26
//  Initialization of Dual Port LUT ram now done through explicit
// parameter setting.
RAM64X1D#(.INIT(64'h0000000000000000))RAM64X1D_instance_3130(.WCLK(CLK), .WE(wea_1), 
  .A0(ADDRA[0]), .A1(ADDRA[1]), .A2(ADDRA[2]), .A3(ADDRA[3]), .A4(ADDRA[4]), .A5(ADDRA[5]), 
  .D(DINA[26]), .SPO(pre_douta_1[26]), .DPRA0(ADDRB[0]), .DPRA1(ADDRB[1]), .DPRA2(ADDRB[2]), 
  .DPRA3(ADDRB[3]), .DPRA4(ADDRB[4]), .DPRA5(ADDRB[5]), .DPO(pre_doutb_1[26]));
// Memory array element: COL: 1, ROW: 27
//  Initialization of Dual Port LUT ram now done through explicit
// parameter setting.
RAM64X1D#(.INIT(64'h0000000000000000))RAM64X1D_instance_3131(.WCLK(CLK), .WE(wea_1), 
  .A0(ADDRA[0]), .A1(ADDRA[1]), .A2(ADDRA[2]), .A3(ADDRA[3]), .A4(ADDRA[4]), .A5(ADDRA[5]), 
  .D(DINA[27]), .SPO(pre_douta_1[27]), .DPRA0(ADDRB[0]), .DPRA1(ADDRB[1]), .DPRA2(ADDRB[2]), 
  .DPRA3(ADDRB[3]), .DPRA4(ADDRB[4]), .DPRA5(ADDRB[5]), .DPO(pre_doutb_1[27]));
// Memory array element: COL: 1, ROW: 28
//  Initialization of Dual Port LUT ram now done through explicit
// parameter setting.
RAM64X1D#(.INIT(64'h0000000000000000))RAM64X1D_instance_3132(.WCLK(CLK), .WE(wea_1), 
  .A0(ADDRA[0]), .A1(ADDRA[1]), .A2(ADDRA[2]), .A3(ADDRA[3]), .A4(ADDRA[4]), .A5(ADDRA[5]), 
  .D(DINA[28]), .SPO(pre_douta_1[28]), .DPRA0(ADDRB[0]), .DPRA1(ADDRB[1]), .DPRA2(ADDRB[2]), 
  .DPRA3(ADDRB[3]), .DPRA4(ADDRB[4]), .DPRA5(ADDRB[5]), .DPO(pre_doutb_1[28]));
// Memory array element: COL: 1, ROW: 29
//  Initialization of Dual Port LUT ram now done through explicit
// parameter setting.
RAM64X1D#(.INIT(64'h0000000000000000))RAM64X1D_instance_3133(.WCLK(CLK), .WE(wea_1), 
  .A0(ADDRA[0]), .A1(ADDRA[1]), .A2(ADDRA[2]), .A3(ADDRA[3]), .A4(ADDRA[4]), .A5(ADDRA[5]), 
  .D(DINA[29]), .SPO(pre_douta_1[29]), .DPRA0(ADDRB[0]), .DPRA1(ADDRB[1]), .DPRA2(ADDRB[2]), 
  .DPRA3(ADDRB[3]), .DPRA4(ADDRB[4]), .DPRA5(ADDRB[5]), .DPO(pre_doutb_1[29]));
// Memory array element: COL: 1, ROW: 30
//  Initialization of Dual Port LUT ram now done through explicit
// parameter setting.
RAM64X1D#(.INIT(64'h0000000000000000))RAM64X1D_instance_3134(.WCLK(CLK), .WE(wea_1), 
  .A0(ADDRA[0]), .A1(ADDRA[1]), .A2(ADDRA[2]), .A3(ADDRA[3]), .A4(ADDRA[4]), .A5(ADDRA[5]), 
  .D(DINA[30]), .SPO(pre_douta_1[30]), .DPRA0(ADDRB[0]), .DPRA1(ADDRB[1]), .DPRA2(ADDRB[2]), 
  .DPRA3(ADDRB[3]), .DPRA4(ADDRB[4]), .DPRA5(ADDRB[5]), .DPO(pre_doutb_1[30]));
// Memory array element: COL: 1, ROW: 31
//  Initialization of Dual Port LUT ram now done through explicit
// parameter setting.
RAM64X1D#(.INIT(64'h0000000000000000))RAM64X1D_instance_3135(.WCLK(CLK), .WE(wea_1), 
  .A0(ADDRA[0]), .A1(ADDRA[1]), .A2(ADDRA[2]), .A3(ADDRA[3]), .A4(ADDRA[4]), .A5(ADDRA[5]), 
  .D(DINA[31]), .SPO(pre_douta_1[31]), .DPRA0(ADDRB[0]), .DPRA1(ADDRB[1]), .DPRA2(ADDRB[2]), 
  .DPRA3(ADDRB[3]), .DPRA4(ADDRB[4]), .DPRA5(ADDRB[5]), .DPO(pre_doutb_1[31]));
endmodule



module medianRow14_structuralmemory_618ad16b_(CLK_u94, bus_05bd45bf_, bus_58767192_, bus_1d9f7981_, bus_52c4fa14_, bus_7ac684df_, bus_355418f2_, bus_79890934_, bus_68daf440_, bus_4c6d4b8e_, bus_7f631c10_, bus_0419815e_, bus_52928377_, bus_713854a6_);
input		CLK_u94;
input		bus_05bd45bf_;
input	[31:0]	bus_58767192_;
input	[2:0]	bus_1d9f7981_;
input		bus_52c4fa14_;
input		bus_7ac684df_;
input	[31:0]	bus_355418f2_;
input	[31:0]	bus_79890934_;
input	[2:0]	bus_68daf440_;
input		bus_4c6d4b8e_;
output	[31:0]	bus_7f631c10_;
output		bus_0419815e_;
output	[31:0]	bus_52928377_;
output		bus_713854a6_;
wire		and_3ec9a708_u0;
reg		logicalMem_6bc48ecf_we_delay0_u0=1'h0;
wire		or_45a1e4c3_u0;
wire		not_62626ba1_u0;
wire	[31:0]	bus_1282d1b9_;
wire	[31:0]	bus_0f7897da_;
wire		or_586b759b_u0;
assign and_3ec9a708_u0=bus_52c4fa14_&not_62626ba1_u0;
always @(posedge CLK_u94 or posedge bus_05bd45bf_)
begin
if (bus_05bd45bf_)
logicalMem_6bc48ecf_we_delay0_u0<=1'h0;
else logicalMem_6bc48ecf_we_delay0_u0<=bus_7ac684df_;
end
assign or_45a1e4c3_u0=bus_52c4fa14_|bus_7ac684df_;
assign not_62626ba1_u0=~bus_7ac684df_;
medianRow14_forge_memory_101x32_149 medianRow14_forge_memory_101x32_149_instance0(.CLK(CLK_u94), 
  .ENA(or_45a1e4c3_u0), .WEA(bus_7ac684df_), .DINA(bus_355418f2_), .ADDRA(bus_58767192_), 
  .DOUTA(bus_1282d1b9_), .DONEA(), .ENB(bus_4c6d4b8e_), .ADDRB(bus_79890934_), .DOUTB(bus_0f7897da_), 
  .DONEB());
assign or_586b759b_u0=and_3ec9a708_u0|logicalMem_6bc48ecf_we_delay0_u0;
assign bus_7f631c10_=bus_1282d1b9_;
assign bus_0419815e_=or_586b759b_u0;
assign bus_52928377_=bus_0f7897da_;
assign bus_713854a6_=bus_4c6d4b8e_;
endmodule



module medianRow14_simplememoryreferee_61093e6a_(bus_06546d6b_, bus_13c2f4dd_, bus_3aec6f19_, bus_406f1f21_, bus_7a7cab01_, bus_5d2629ca_, bus_632636fa_, bus_17ae75da_, bus_18a94427_, bus_10efc10b_, bus_5686e6c7_, bus_53b95538_, bus_741e438e_, bus_5f38f938_, bus_43bd9345_, bus_637acd77_, bus_64a4f33b_, bus_32a0e263_, bus_06d4ea92_, bus_3988adda_, bus_48f2414d_);
input		bus_06546d6b_;
input		bus_13c2f4dd_;
input		bus_3aec6f19_;
input	[31:0]	bus_406f1f21_;
input		bus_7a7cab01_;
input	[31:0]	bus_5d2629ca_;
input	[31:0]	bus_632636fa_;
input	[2:0]	bus_17ae75da_;
input		bus_18a94427_;
input		bus_10efc10b_;
input	[31:0]	bus_5686e6c7_;
input	[31:0]	bus_53b95538_;
input	[2:0]	bus_741e438e_;
output	[31:0]	bus_5f38f938_;
output	[31:0]	bus_43bd9345_;
output		bus_637acd77_;
output		bus_64a4f33b_;
output	[2:0]	bus_32a0e263_;
output		bus_06d4ea92_;
output	[31:0]	bus_3988adda_;
output		bus_48f2414d_;
wire		not_228c72ec_u0;
wire	[31:0]	mux_153235f3_u0;
wire		or_562320c2_u0;
wire		or_4fb7ca53_u0;
wire		not_137ef39a_u0;
wire		or_6389f68f_u0;
wire		and_62e74221_u0;
wire		or_29ea0ded_u0;
reg		done_qual_u224=1'h0;
wire		or_5748dea3_u0;
wire	[31:0]	mux_3da1aeb0_u0;
wire		and_232558d6_u0;
reg		done_qual_u225_u0=1'h0;
assign not_228c72ec_u0=~bus_3aec6f19_;
assign mux_153235f3_u0=(bus_7a7cab01_)?bus_632636fa_:bus_53b95538_;
assign or_562320c2_u0=bus_18a94427_|bus_10efc10b_;
assign or_4fb7ca53_u0=bus_7a7cab01_|done_qual_u224;
assign not_137ef39a_u0=~bus_3aec6f19_;
assign or_6389f68f_u0=bus_7a7cab01_|bus_10efc10b_;
assign and_62e74221_u0=or_4fb7ca53_u0&bus_3aec6f19_;
assign or_29ea0ded_u0=bus_7a7cab01_|or_562320c2_u0;
assign bus_5f38f938_=mux_3da1aeb0_u0;
assign bus_43bd9345_=mux_153235f3_u0;
assign bus_637acd77_=or_6389f68f_u0;
assign bus_64a4f33b_=or_29ea0ded_u0;
assign bus_32a0e263_=3'h1;
assign bus_06d4ea92_=and_62e74221_u0;
assign bus_3988adda_=bus_406f1f21_;
assign bus_48f2414d_=and_232558d6_u0;
always @(posedge bus_06546d6b_)
begin
if (bus_13c2f4dd_)
done_qual_u224<=1'h0;
else done_qual_u224<=bus_7a7cab01_;
end
assign or_5748dea3_u0=or_562320c2_u0|done_qual_u225_u0;
assign mux_3da1aeb0_u0=(bus_7a7cab01_)?{24'b0, bus_5d2629ca_[7:0]}:bus_5686e6c7_;
assign and_232558d6_u0=or_5748dea3_u0&bus_3aec6f19_;
always @(posedge bus_06546d6b_)
begin
if (bus_13c2f4dd_)
done_qual_u225_u0<=1'h0;
else done_qual_u225_u0<=or_562320c2_u0;
end
endmodule



module medianRow14_receive(CLK, RESET, GO, port_14d71502_, port_49444547_, port_6bf44be4_, RESULT, RESULT_u2285, RESULT_u2286, RESULT_u2287, RESULT_u2288, RESULT_u2289, RESULT_u2290, DONE);
input		CLK;
input		RESET;
input		GO;
input	[31:0]	port_14d71502_;
input		port_49444547_;
input	[7:0]	port_6bf44be4_;
output		RESULT;
output	[31:0]	RESULT_u2285;
output		RESULT_u2286;
output	[31:0]	RESULT_u2287;
output	[31:0]	RESULT_u2288;
output	[2:0]	RESULT_u2289;
output		RESULT_u2290;
output		DONE;
wire		simplePinWrite;
wire	[31:0]	add;
wire		and_u3907_u0;
reg		reg_4feb7b95_u0=1'h0;
wire		or_u1497_u0;
wire	[31:0]	add_u704;
reg		reg_338d492b_u0=1'h0;
assign simplePinWrite=GO&{1{GO}};
assign add=port_14d71502_+32'h0;
assign and_u3907_u0=reg_4feb7b95_u0&port_49444547_;
always @(posedge CLK or posedge GO or posedge or_u1497_u0)
begin
if (or_u1497_u0)
reg_4feb7b95_u0<=1'h0;
else if (GO)
reg_4feb7b95_u0<=1'h1;
else reg_4feb7b95_u0<=reg_4feb7b95_u0;
end
assign or_u1497_u0=and_u3907_u0|RESET;
assign add_u704=port_14d71502_+32'h1;
always @(posedge CLK or posedge RESET)
begin
if (RESET)
reg_338d492b_u0<=1'h0;
else reg_338d492b_u0<=GO;
end
assign RESULT=GO;
assign RESULT_u2285=add_u704;
assign RESULT_u2286=GO;
assign RESULT_u2287=add;
assign RESULT_u2288={24'b0, port_6bf44be4_};
assign RESULT_u2289=3'h1;
assign RESULT_u2290=simplePinWrite;
assign DONE=reg_338d492b_u0;
endmodule



module medianRow14_endianswapper_4daf60aa_(endianswapper_4daf60aa_in, endianswapper_4daf60aa_out);
input	[31:0]	endianswapper_4daf60aa_in;
output	[31:0]	endianswapper_4daf60aa_out;
assign endianswapper_4daf60aa_out=endianswapper_4daf60aa_in;
endmodule



module medianRow14_endianswapper_0f65bf63_(endianswapper_0f65bf63_in, endianswapper_0f65bf63_out);
input	[31:0]	endianswapper_0f65bf63_in;
output	[31:0]	endianswapper_0f65bf63_out;
assign endianswapper_0f65bf63_out=endianswapper_0f65bf63_in;
endmodule



module medianRow14_stateVar_i(bus_1e078b49_, bus_3b0b26c0_, bus_49371d5b_, bus_09179c78_, bus_232555cd_, bus_3954d5ca_, bus_5a91f7de_);
input		bus_1e078b49_;
input		bus_3b0b26c0_;
input		bus_49371d5b_;
input	[31:0]	bus_09179c78_;
input		bus_232555cd_;
input	[31:0]	bus_3954d5ca_;
output	[31:0]	bus_5a91f7de_;
wire	[31:0]	endianswapper_4daf60aa_out;
wire	[31:0]	mux_0e8792d1_u0;
wire	[31:0]	endianswapper_0f65bf63_out;
wire		or_61e7f68f_u0;
reg	[31:0]	stateVar_i_u50=32'h0;
medianRow14_endianswapper_4daf60aa_ medianRow14_endianswapper_4daf60aa__1(.endianswapper_4daf60aa_in(stateVar_i_u50), 
  .endianswapper_4daf60aa_out(endianswapper_4daf60aa_out));
assign mux_0e8792d1_u0=(bus_49371d5b_)?bus_09179c78_:32'h0;
medianRow14_endianswapper_0f65bf63_ medianRow14_endianswapper_0f65bf63__1(.endianswapper_0f65bf63_in(mux_0e8792d1_u0), 
  .endianswapper_0f65bf63_out(endianswapper_0f65bf63_out));
assign or_61e7f68f_u0=bus_49371d5b_|bus_232555cd_;
always @(posedge bus_1e078b49_ or posedge bus_3b0b26c0_)
begin
if (bus_3b0b26c0_)
stateVar_i_u50<=32'h0;
else if (or_61e7f68f_u0)
stateVar_i_u50<=endianswapper_0f65bf63_out;
end
assign bus_5a91f7de_=endianswapper_4daf60aa_out;
endmodule


