// __  ___ __ ___  _ __   ___  ___ 
// \ \/ / '__/ _ \| '_ \ / _ \/ __|
//  >  <| | | (_) | | | | (_) \__ \
// /_/\_\_|  \___/|_| |_|\___/|___/
// 
// Xronos synthesizer version
// Run date: Fri 23 Feb 2018 14:28:41 +0000
// 

module medianRow15(in1_SEND, in1_COUNT, median_COUNT, median_ACK, CLK, median_SEND, in1_DATA, median_DATA, RESET, median_RDY, in1_ACK);
wire		compute_median_done;
input		in1_SEND;
input	[15:0]	in1_COUNT;
wire		compute_median_go;
output	[15:0]	median_COUNT;
input		median_ACK;
input		CLK;
output		median_SEND;
input	[7:0]	in1_DATA;
output	[7:0]	median_DATA;
wire		receive_done;
input		RESET;
input		median_RDY;
output		in1_ACK;
wire		receive_go;
wire	[15:0]	compute_median_u670;
wire		compute_median;
wire	[31:0]	compute_median_u663;
wire	[31:0]	compute_median_u658;
wire	[31:0]	compute_median_u664;
wire	[31:0]	compute_median_u660;
wire		compute_median_u666;
wire	[7:0]	compute_median_u669;
wire	[2:0]	compute_median_u661;
wire		medianRow15_compute_median_instance_DONE;
wire	[2:0]	compute_median_u665;
wire		compute_median_u659;
wire		compute_median_u662;
wire	[31:0]	compute_median_u667;
wire	[2:0]	compute_median_u668;
wire		compute_median_u671;
wire	[31:0]	bus_50e4b31d_;
wire	[31:0]	bus_1f8aa73e_;
wire		bus_615fc111_;
wire	[31:0]	bus_57dab543_;
wire		bus_128cdc0c_;
wire	[2:0]	bus_1ae98ba1_;
wire	[31:0]	bus_63e5f82a_;
wire		bus_2dd6155f_;
wire		bus_6450e1f6_;
wire		bus_1ffa2e1c_;
wire	[31:0]	bus_2c0f475e_;
wire		bus_22d42b7f_;
wire		bus_0fb836ff_;
wire	[31:0]	bus_6a2aae02_;
wire		bus_44fc4bd9_;
wire		bus_13290e56_;
wire	[31:0]	bus_472c4243_;
wire	[31:0]	bus_43c94e3c_;
wire		bus_2aee950c_;
wire	[31:0]	bus_3b5a1da0_;
wire	[2:0]	bus_2446d1a7_;
wire		bus_398ab5ca_;
wire		bus_1e11ad99_;
wire		bus_70c54dad_;
wire	[2:0]	receive_u286;
wire		medianRow15_receive_instance_DONE;
wire		receive_u283;
wire	[31:0]	receive_u285;
wire		receive_u287;
wire		receive;
wire	[31:0]	receive_u284;
wire	[31:0]	receive_u282;
wire		scheduler;
wire		medianRow15_scheduler_instance_DONE;
wire		scheduler_u818;
wire		scheduler_u819;
wire		scheduler_u817;
wire		bus_087f002b_;
assign compute_median_done=bus_6450e1f6_;
assign compute_median_go=scheduler_u818;
assign median_COUNT=compute_median_u670;
assign median_SEND=compute_median_u671;
assign median_DATA=compute_median_u669;
assign receive_done=bus_1ffa2e1c_;
assign in1_ACK=receive_u287;
assign receive_go=scheduler_u819;
medianRow15_compute_median medianRow15_compute_median_instance(.CLK(CLK), .RESET(bus_1e11ad99_), 
  .GO(compute_median_go), .port_3395b64e_(bus_615fc111_), .port_3861adec_(bus_1f8aa73e_), 
  .port_116bd778_(bus_13290e56_), .port_4b49cd8b_(bus_13290e56_), .port_1c290d55_(bus_43c94e3c_), 
  .DONE(medianRow15_compute_median_instance_DONE), .RESULT(compute_median), .RESULT_u2245(compute_median_u658), 
  .RESULT_u2246(compute_median_u659), .RESULT_u2247(compute_median_u660), .RESULT_u2248(compute_median_u661), 
  .RESULT_u2252(compute_median_u662), .RESULT_u2253(compute_median_u663), .RESULT_u2254(compute_median_u664), 
  .RESULT_u2255(compute_median_u665), .RESULT_u2249(compute_median_u666), .RESULT_u2250(compute_median_u667), 
  .RESULT_u2251(compute_median_u668), .RESULT_u2256(compute_median_u669), .RESULT_u2257(compute_median_u670), 
  .RESULT_u2258(compute_median_u671));
medianRow15_stateVar_i medianRow15_stateVar_i_1(.bus_216c6464_(CLK), .bus_45fc9e40_(bus_1e11ad99_), 
  .bus_22e42cfc_(receive), .bus_41c6af53_(receive_u282), .bus_27b62153_(compute_median), 
  .bus_7a2269bf_(32'h0), .bus_50e4b31d_(bus_50e4b31d_));
medianRow15_simplememoryreferee_3cb498ed_ medianRow15_simplememoryreferee_3cb498ed__1(.bus_415400a4_(CLK), 
  .bus_450dd27a_(bus_1e11ad99_), .bus_066efbeb_(bus_0fb836ff_), .bus_2f93382a_(bus_6a2aae02_), 
  .bus_49688931_(compute_median_u659), .bus_207345a7_(compute_median_u660), .bus_375789c7_(3'h1), 
  .bus_63e5f82a_(bus_63e5f82a_), .bus_57dab543_(bus_57dab543_), .bus_2dd6155f_(bus_2dd6155f_), 
  .bus_128cdc0c_(bus_128cdc0c_), .bus_1ae98ba1_(bus_1ae98ba1_), .bus_1f8aa73e_(bus_1f8aa73e_), 
  .bus_615fc111_(bus_615fc111_));
assign bus_6450e1f6_=medianRow15_compute_median_instance_DONE&{1{medianRow15_compute_median_instance_DONE}};
assign bus_1ffa2e1c_=medianRow15_receive_instance_DONE&{1{medianRow15_receive_instance_DONE}};
medianRow15_structuralmemory_7b577e70_ medianRow15_structuralmemory_7b577e70__1(.CLK_u93(CLK), 
  .bus_2f085f50_(bus_1e11ad99_), .bus_0ed07fa1_(bus_57dab543_), .bus_410f28b2_(3'h1), 
  .bus_105841d0_(bus_128cdc0c_), .bus_4bdcc002_(bus_472c4243_), .bus_0aef563f_(3'h1), 
  .bus_247ac50a_(bus_2aee950c_), .bus_799a33cb_(bus_44fc4bd9_), .bus_7e1b4a62_(bus_3b5a1da0_), 
  .bus_6a2aae02_(bus_6a2aae02_), .bus_0fb836ff_(bus_0fb836ff_), .bus_2c0f475e_(bus_2c0f475e_), 
  .bus_22d42b7f_(bus_22d42b7f_));
medianRow15_simplememoryreferee_7469aa46_ medianRow15_simplememoryreferee_7469aa46__1(.bus_4dac9f68_(CLK), 
  .bus_6bc72ebe_(bus_1e11ad99_), .bus_6a6b593e_(bus_22d42b7f_), .bus_75b3baec_(bus_2c0f475e_), 
  .bus_4f5eb8ab_(receive_u283), .bus_794c18bd_({24'b0, receive_u285[7:0]}), .bus_7c3e0c8f_(receive_u284), 
  .bus_04d08500_(3'h1), .bus_2f818ae3_(compute_median_u666), .bus_515ce90a_(compute_median_u662), 
  .bus_69ce42d9_(compute_median_u664), .bus_60e38b66_(compute_median_u663), .bus_4c57f205_(3'h1), 
  .bus_3b5a1da0_(bus_3b5a1da0_), .bus_472c4243_(bus_472c4243_), .bus_44fc4bd9_(bus_44fc4bd9_), 
  .bus_2aee950c_(bus_2aee950c_), .bus_2446d1a7_(bus_2446d1a7_), .bus_398ab5ca_(bus_398ab5ca_), 
  .bus_43c94e3c_(bus_43c94e3c_), .bus_13290e56_(bus_13290e56_));
medianRow15_globalreset_physical_736798d6_ medianRow15_globalreset_physical_736798d6__1(.bus_20d7caed_(CLK), 
  .bus_6860427e_(RESET), .bus_1e11ad99_(bus_1e11ad99_));
medianRow15_Kicker_59 medianRow15_Kicker_59_1(.CLK(CLK), .RESET(bus_1e11ad99_), 
  .bus_70c54dad_(bus_70c54dad_));
medianRow15_receive medianRow15_receive_instance(.CLK(CLK), .RESET(bus_1e11ad99_), 
  .GO(receive_go), .port_72bf504d_(bus_50e4b31d_), .port_4ebb2248_(bus_398ab5ca_), 
  .port_44cf90eb_(in1_DATA), .DONE(medianRow15_receive_instance_DONE), .RESULT(receive), 
  .RESULT_u2259(receive_u282), .RESULT_u2260(receive_u283), .RESULT_u2261(receive_u284), 
  .RESULT_u2262(receive_u285), .RESULT_u2263(receive_u286), .RESULT_u2264(receive_u287));
medianRow15_scheduler medianRow15_scheduler_instance(.CLK(CLK), .RESET(bus_1e11ad99_), 
  .GO(bus_70c54dad_), .port_06e0c99e_(bus_087f002b_), .port_15d5fd88_(bus_50e4b31d_), 
  .port_15680b7f_(compute_median_done), .port_5e7b9726_(in1_SEND), .port_4c6274dc_(receive_done), 
  .port_780b87c2_(median_RDY), .DONE(medianRow15_scheduler_instance_DONE), .RESULT(scheduler), 
  .RESULT_u2265(scheduler_u817), .RESULT_u2266(scheduler_u818), .RESULT_u2267(scheduler_u819));
medianRow15_stateVar_fsmState_medianRow15 medianRow15_stateVar_fsmState_medianRow15_1(.bus_1071d90d_(CLK), 
  .bus_785d8595_(bus_1e11ad99_), .bus_06145738_(scheduler), .bus_0c5600e8_(scheduler_u817), 
  .bus_087f002b_(bus_087f002b_));
endmodule



module medianRow15_compute_median(CLK, RESET, GO, port_3395b64e_, port_3861adec_, port_4b49cd8b_, port_1c290d55_, port_116bd778_, RESULT, RESULT_u2245, RESULT_u2246, RESULT_u2247, RESULT_u2248, RESULT_u2249, RESULT_u2250, RESULT_u2251, RESULT_u2252, RESULT_u2253, RESULT_u2254, RESULT_u2255, RESULT_u2256, RESULT_u2257, RESULT_u2258, DONE);
input		CLK;
input		RESET;
input		GO;
input		port_3395b64e_;
input	[31:0]	port_3861adec_;
input		port_4b49cd8b_;
input	[31:0]	port_1c290d55_;
input		port_116bd778_;
output		RESULT;
output	[31:0]	RESULT_u2245;
output		RESULT_u2246;
output	[31:0]	RESULT_u2247;
output	[2:0]	RESULT_u2248;
output		RESULT_u2249;
output	[31:0]	RESULT_u2250;
output	[2:0]	RESULT_u2251;
output		RESULT_u2252;
output	[31:0]	RESULT_u2253;
output	[31:0]	RESULT_u2254;
output	[2:0]	RESULT_u2255;
output	[7:0]	RESULT_u2256;
output	[15:0]	RESULT_u2257;
output		RESULT_u2258;
output		DONE;
wire		and_u3824_u0;
wire	[15:0]	lessThan_a_unsigned;
wire	[15:0]	lessThan_b_unsigned;
wire		lessThan;
wire		andOp;
wire		and_u3825_u0;
wire		not_u891_u0;
wire		and_u3826_u0;
wire		and_u3827_u0;
wire	[31:0]	add;
wire		and_u3828_u0;
wire	[31:0]	add_u683;
wire	[31:0]	add_u684;
wire		and_u3829_u0;
wire signed	[31:0]	greaterThan_a_signed;
wire signed	[31:0]	greaterThan_b_signed;
wire		greaterThan;
wire		and_u3830_u0;
wire		not_u892_u0;
wire		and_u3831_u0;
wire	[31:0]	add_u685;
wire		and_u3832_u0;
wire	[31:0]	add_u686;
wire	[31:0]	add_u687;
wire		and_u3833_u0;
wire	[31:0]	add_u688;
wire		or_u1462_u0;
reg		reg_231a4ac3_u0=1'h0;
wire		and_u3834_u0;
wire	[31:0]	add_u689;
wire	[31:0]	add_u690;
wire		or_u1463_u0;
wire		and_u3835_u0;
reg		reg_4d65f538_u0=1'h0;
reg	[31:0]	syncEnable_u1446=32'h0;
reg	[31:0]	syncEnable_u1447_u0=32'h0;
reg		block_GO_delayed_u100=1'h0;
reg	[31:0]	syncEnable_u1448_u0=32'h0;
reg	[31:0]	syncEnable_u1449_u0=32'h0;
reg	[31:0]	syncEnable_u1450_u0=32'h0;
reg		block_GO_delayed_result_delayed_u19=1'h0;
reg	[31:0]	syncEnable_u1451_u0=32'h0;
wire		or_u1464_u0;
wire	[31:0]	mux_u1935;
wire	[31:0]	mux_u1936_u0;
reg		reg_7af24af3_u0=1'h0;
wire		mux_u1937_u0;
reg		reg_7af24af3_result_delayed_u0=1'h0;
reg		reg_50f2ade3_u0=1'h0;
reg		syncEnable_u1452_u0=1'h0;
wire		or_u1465_u0;
wire		and_u3836_u0;
wire	[31:0]	mux_u1938_u0;
reg		reg_147ad4a0_u0=1'h0;
wire		and_u3837_u0;
reg		reg_7af24af3_result_delayed_result_delayed_u0=1'h0;
reg	[31:0]	syncEnable_u1453_u0=32'h0;
wire	[31:0]	mux_u1939_u0;
reg	[31:0]	syncEnable_u1454_u0=32'h0;
wire	[31:0]	add_u691;
wire		or_u1466_u0;
wire	[31:0]	mux_u1940_u0;
reg	[31:0]	syncEnable_u1455_u0=32'h0;
reg	[31:0]	syncEnable_u1456_u0=32'h0;
wire	[31:0]	mux_u1941_u0;
wire		or_u1467_u0;
reg		syncEnable_u1457_u0=1'h0;
reg	[31:0]	syncEnable_u1458_u0=32'h0;
reg	[31:0]	syncEnable_u1459_u0=32'h0;
reg	[31:0]	syncEnable_u1460_u0=32'h0;
reg		block_GO_delayed_u101_u0=1'h0;
reg	[31:0]	syncEnable_u1461_u0=32'h0;
reg	[31:0]	syncEnable_u1462_u0=32'h0;
reg	[31:0]	syncEnable_u1463_u0=32'h0;
wire signed	[32:0]	lessThan_u105_b_signed;
wire signed	[32:0]	lessThan_u105_a_signed;
wire		lessThan_u105;
wire		not_u893_u0;
wire		and_u3838_u0;
wire		and_u3839_u0;
wire		or_u1468_u0;
wire	[31:0]	mux_u1942_u0;
wire	[31:0]	mux_u1943_u0;
wire	[31:0]	mux_u1944_u0;
wire	[31:0]	mux_u1945_u0;
wire	[31:0]	mux_u1946_u0;
wire		mux_u1947_u0;
wire	[15:0]	add_u692;
reg	[15:0]	syncEnable_u1464_u0=16'h0;
reg	[31:0]	latch_2d209920_reg=32'h0;
wire	[31:0]	latch_2d209920_out;
wire		scoreboard_4b3706a6_resOr0;
wire		scoreboard_4b3706a6_and;
reg		scoreboard_4b3706a6_reg0=1'h0;
wire		bus_6b3076c5_;
reg		scoreboard_4b3706a6_reg1=1'h0;
wire		scoreboard_4b3706a6_resOr1;
reg	[31:0]	latch_0d8d695a_reg=32'h0;
wire	[31:0]	latch_0d8d695a_out;
reg	[31:0]	latch_213112e6_reg=32'h0;
wire	[31:0]	latch_213112e6_out;
reg	[31:0]	latch_01f00ea9_reg=32'h0;
wire	[31:0]	latch_01f00ea9_out;
reg		reg_4de2c938_u0=1'h0;
wire		latch_0fc348ff_out;
reg		latch_0fc348ff_reg=1'h0;
wire		and_u3840_u0;
reg		fbReg_swapped_u47=1'h0;
reg	[31:0]	fbReg_tmp_row0_u47=32'h0;
wire	[15:0]	mux_u1948_u0;
wire		or_u1469_u0;
wire	[31:0]	mux_u1949_u0;
reg		loopControl_u98=1'h0;
reg	[15:0]	fbReg_idx_u47=16'h0;
reg		syncEnable_u1465_u0=1'h0;
reg	[31:0]	fbReg_tmp_row_u47=32'h0;
wire	[31:0]	mux_u1950_u0;
wire	[31:0]	mux_u1951_u0;
reg	[31:0]	fbReg_tmp_u47=32'h0;
reg	[31:0]	fbReg_tmp_row1_u47=32'h0;
wire		mux_u1952_u0;
wire	[31:0]	mux_u1953_u0;
wire		and_u3841_u0;
wire	[15:0]	simplePinWrite;
wire		simplePinWrite_u626;
wire	[7:0]	simplePinWrite_u627;
reg		reg_352b0be2_u0=1'h0;
wire	[31:0]	mux_u1954_u0;
wire		or_u1470_u0;
reg		reg_352b0be2_result_delayed_u0=1'h0;
assign and_u3824_u0=and_u3826_u0&or_u1469_u0;
assign lessThan_a_unsigned=mux_u1948_u0;
assign lessThan_b_unsigned=16'h65;
assign lessThan=lessThan_a_unsigned<lessThan_b_unsigned;
assign andOp=lessThan&mux_u1952_u0;
assign and_u3825_u0=or_u1469_u0&andOp;
assign not_u891_u0=~andOp;
assign and_u3826_u0=or_u1469_u0&not_u891_u0;
assign and_u3827_u0=and_u3839_u0&or_u1468_u0;
assign add=mux_u1944_u0+32'h0;
assign and_u3828_u0=and_u3827_u0&port_3395b64e_;
assign add_u683=mux_u1944_u0+32'h1;
assign add_u684=add_u683+32'h0;
assign and_u3829_u0=and_u3827_u0&port_116bd778_;
assign greaterThan_a_signed=syncEnable_u1458_u0;
assign greaterThan_b_signed=syncEnable_u1461_u0;
assign greaterThan=greaterThan_a_signed>greaterThan_b_signed;
assign and_u3830_u0=block_GO_delayed_u101_u0&not_u892_u0;
assign not_u892_u0=~greaterThan;
assign and_u3831_u0=block_GO_delayed_u101_u0&greaterThan;
assign add_u685=syncEnable_u1456_u0+32'h0;
assign and_u3832_u0=and_u3836_u0&port_3395b64e_;
assign add_u686=syncEnable_u1456_u0+32'h1;
assign add_u687=add_u686+32'h0;
assign and_u3833_u0=and_u3836_u0&port_116bd778_;
assign add_u688=syncEnable_u1456_u0+32'h0;
assign or_u1462_u0=and_u3834_u0|RESET;
always @(posedge CLK or posedge block_GO_delayed_u100 or posedge or_u1462_u0)
begin
if (or_u1462_u0)
reg_231a4ac3_u0<=1'h0;
else if (block_GO_delayed_u100)
reg_231a4ac3_u0<=1'h1;
else reg_231a4ac3_u0<=reg_231a4ac3_u0;
end
assign and_u3834_u0=reg_231a4ac3_u0&port_116bd778_;
assign add_u689=syncEnable_u1456_u0+32'h1;
assign add_u690=add_u689+32'h0;
assign or_u1463_u0=and_u3835_u0|RESET;
assign and_u3835_u0=reg_4d65f538_u0&port_116bd778_;
always @(posedge CLK or posedge block_GO_delayed_result_delayed_u19 or posedge or_u1463_u0)
begin
if (or_u1463_u0)
reg_4d65f538_u0<=1'h0;
else if (block_GO_delayed_result_delayed_u19)
reg_4d65f538_u0<=1'h1;
else reg_4d65f538_u0<=reg_4d65f538_u0;
end
always @(posedge CLK)
begin
if (and_u3836_u0)
syncEnable_u1446<=port_1c290d55_;
end
always @(posedge CLK)
begin
if (and_u3836_u0)
syncEnable_u1447_u0<=port_3861adec_;
end
always @(posedge CLK or posedge RESET)
begin
if (RESET)
block_GO_delayed_u100<=1'h0;
else block_GO_delayed_u100<=and_u3836_u0;
end
always @(posedge CLK)
begin
if (and_u3836_u0)
syncEnable_u1448_u0<=add_u688;
end
always @(posedge CLK)
begin
if (and_u3836_u0)
syncEnable_u1449_u0<=add_u690;
end
always @(posedge CLK)
begin
if (and_u3836_u0)
syncEnable_u1450_u0<=port_1c290d55_;
end
always @(posedge CLK or posedge RESET)
begin
if (RESET)
block_GO_delayed_result_delayed_u19<=1'h0;
else block_GO_delayed_result_delayed_u19<=block_GO_delayed_u100;
end
always @(posedge CLK)
begin
if (and_u3836_u0)
syncEnable_u1451_u0<=port_3861adec_;
end
assign or_u1464_u0=block_GO_delayed_u100|block_GO_delayed_result_delayed_u19;
assign mux_u1935=({32{block_GO_delayed_u100}}&syncEnable_u1448_u0)|({32{block_GO_delayed_result_delayed_u19}}&syncEnable_u1449_u0)|({32{and_u3836_u0}}&add_u687);
assign mux_u1936_u0=(block_GO_delayed_u100)?syncEnable_u1446:syncEnable_u1447_u0;
always @(posedge CLK or posedge RESET)
begin
if (RESET)
reg_7af24af3_u0<=1'h0;
else reg_7af24af3_u0<=and_u3836_u0;
end
assign mux_u1937_u0=(reg_50f2ade3_u0)?1'h1:syncEnable_u1452_u0;
always @(posedge CLK or posedge RESET)
begin
if (RESET)
reg_7af24af3_result_delayed_u0<=1'h0;
else reg_7af24af3_result_delayed_u0<=reg_7af24af3_u0;
end
always @(posedge CLK or posedge RESET)
begin
if (RESET)
reg_50f2ade3_u0<=1'h0;
else reg_50f2ade3_u0<=reg_7af24af3_result_delayed_result_delayed_u0;
end
always @(posedge CLK)
begin
if (block_GO_delayed_u101_u0)
syncEnable_u1452_u0<=syncEnable_u1457_u0;
end
assign or_u1465_u0=reg_50f2ade3_u0|reg_147ad4a0_u0;
assign and_u3836_u0=and_u3831_u0&block_GO_delayed_u101_u0;
assign mux_u1938_u0=(reg_50f2ade3_u0)?syncEnable_u1450_u0:syncEnable_u1454_u0;
always @(posedge CLK or posedge RESET)
begin
if (RESET)
reg_147ad4a0_u0<=1'h0;
else reg_147ad4a0_u0<=and_u3837_u0;
end
assign and_u3837_u0=and_u3830_u0&block_GO_delayed_u101_u0;
always @(posedge CLK or posedge RESET)
begin
if (RESET)
reg_7af24af3_result_delayed_result_delayed_u0<=1'h0;
else reg_7af24af3_result_delayed_result_delayed_u0<=reg_7af24af3_result_delayed_u0;
end
always @(posedge CLK)
begin
if (block_GO_delayed_u101_u0)
syncEnable_u1453_u0<=syncEnable_u1460_u0;
end
assign mux_u1939_u0=(reg_50f2ade3_u0)?syncEnable_u1451_u0:syncEnable_u1453_u0;
always @(posedge CLK)
begin
if (block_GO_delayed_u101_u0)
syncEnable_u1454_u0<=syncEnable_u1455_u0;
end
assign add_u691=mux_u1944_u0+32'h1;
assign or_u1466_u0=and_u3827_u0|and_u3836_u0;
assign mux_u1940_u0=({32{or_u1464_u0}}&mux_u1935)|({32{and_u3827_u0}}&add_u684)|({32{and_u3836_u0}}&mux_u1935);
always @(posedge CLK)
begin
if (and_u3827_u0)
syncEnable_u1455_u0<=mux_u1942_u0;
end
always @(posedge CLK)
begin
if (and_u3827_u0)
syncEnable_u1456_u0<=mux_u1944_u0;
end
assign mux_u1941_u0=(and_u3827_u0)?add:add_u685;
assign or_u1467_u0=and_u3827_u0|and_u3836_u0;
always @(posedge CLK)
begin
if (and_u3827_u0)
syncEnable_u1457_u0<=mux_u1947_u0;
end
always @(posedge CLK)
begin
if (and_u3827_u0)
syncEnable_u1458_u0<=port_3861adec_;
end
always @(posedge CLK)
begin
if (and_u3827_u0)
syncEnable_u1459_u0<=add_u691;
end
always @(posedge CLK)
begin
if (and_u3827_u0)
syncEnable_u1460_u0<=mux_u1945_u0;
end
always @(posedge CLK or posedge RESET)
begin
if (RESET)
block_GO_delayed_u101_u0<=1'h0;
else block_GO_delayed_u101_u0<=and_u3827_u0;
end
always @(posedge CLK)
begin
if (and_u3827_u0)
syncEnable_u1461_u0<=port_1c290d55_;
end
always @(posedge CLK)
begin
if (or_u1468_u0)
syncEnable_u1462_u0<=mux_u1943_u0;
end
always @(posedge CLK)
begin
if (or_u1468_u0)
syncEnable_u1463_u0<=mux_u1946_u0;
end
assign lessThan_u105_a_signed={1'b0, mux_u1944_u0};
assign lessThan_u105_b_signed=33'h64;
assign lessThan_u105=lessThan_u105_a_signed<lessThan_u105_b_signed;
assign not_u893_u0=~lessThan_u105;
assign and_u3838_u0=or_u1468_u0&not_u893_u0;
assign and_u3839_u0=or_u1468_u0&lessThan_u105;
assign or_u1468_u0=and_u3840_u0|or_u1465_u0;
assign mux_u1942_u0=(and_u3840_u0)?mux_u1950_u0:mux_u1938_u0;
assign mux_u1943_u0=(and_u3840_u0)?mux_u1949_u0:syncEnable_u1458_u0;
assign mux_u1944_u0=(and_u3840_u0)?32'h0:syncEnable_u1459_u0;
assign mux_u1945_u0=(and_u3840_u0)?mux_u1951_u0:mux_u1939_u0;
assign mux_u1946_u0=(and_u3840_u0)?mux_u1953_u0:syncEnable_u1461_u0;
assign mux_u1947_u0=(and_u3840_u0)?1'h0:mux_u1937_u0;
assign add_u692=mux_u1948_u0+16'h1;
always @(posedge CLK)
begin
if (and_u3840_u0)
syncEnable_u1464_u0<=add_u692;
end
always @(posedge CLK)
begin
if (or_u1465_u0)
latch_2d209920_reg<=syncEnable_u1462_u0;
end
assign latch_2d209920_out=(or_u1465_u0)?syncEnable_u1462_u0:latch_2d209920_reg;
assign scoreboard_4b3706a6_resOr0=reg_4de2c938_u0|scoreboard_4b3706a6_reg0;
assign scoreboard_4b3706a6_and=scoreboard_4b3706a6_resOr0&scoreboard_4b3706a6_resOr1;
always @(posedge CLK)
begin
if (bus_6b3076c5_)
scoreboard_4b3706a6_reg0<=1'h0;
else if (reg_4de2c938_u0)
scoreboard_4b3706a6_reg0<=1'h1;
else scoreboard_4b3706a6_reg0<=scoreboard_4b3706a6_reg0;
end
assign bus_6b3076c5_=scoreboard_4b3706a6_and|RESET;
always @(posedge CLK)
begin
if (bus_6b3076c5_)
scoreboard_4b3706a6_reg1<=1'h0;
else if (or_u1465_u0)
scoreboard_4b3706a6_reg1<=1'h1;
else scoreboard_4b3706a6_reg1<=scoreboard_4b3706a6_reg1;
end
assign scoreboard_4b3706a6_resOr1=or_u1465_u0|scoreboard_4b3706a6_reg1;
always @(posedge CLK)
begin
if (or_u1465_u0)
latch_0d8d695a_reg<=mux_u1939_u0;
end
assign latch_0d8d695a_out=(or_u1465_u0)?mux_u1939_u0:latch_0d8d695a_reg;
always @(posedge CLK)
begin
if (or_u1465_u0)
latch_213112e6_reg<=syncEnable_u1463_u0;
end
assign latch_213112e6_out=(or_u1465_u0)?syncEnable_u1463_u0:latch_213112e6_reg;
always @(posedge CLK)
begin
if (or_u1465_u0)
latch_01f00ea9_reg<=mux_u1938_u0;
end
assign latch_01f00ea9_out=(or_u1465_u0)?mux_u1938_u0:latch_01f00ea9_reg;
always @(posedge CLK or posedge RESET)
begin
if (RESET)
reg_4de2c938_u0<=1'h0;
else reg_4de2c938_u0<=or_u1465_u0;
end
assign latch_0fc348ff_out=(or_u1465_u0)?mux_u1937_u0:latch_0fc348ff_reg;
always @(posedge CLK)
begin
if (or_u1465_u0)
latch_0fc348ff_reg<=mux_u1937_u0;
end
assign and_u3840_u0=and_u3825_u0&or_u1469_u0;
always @(posedge CLK)
begin
if (scoreboard_4b3706a6_and)
fbReg_swapped_u47<=latch_0fc348ff_out;
end
always @(posedge CLK)
begin
if (scoreboard_4b3706a6_and)
fbReg_tmp_row0_u47<=latch_213112e6_out;
end
assign mux_u1948_u0=(GO)?16'h0:fbReg_idx_u47;
assign or_u1469_u0=GO|loopControl_u98;
assign mux_u1949_u0=(GO)?32'h0:fbReg_tmp_row_u47;
always @(posedge CLK or posedge syncEnable_u1465_u0)
begin
if (syncEnable_u1465_u0)
loopControl_u98<=1'h0;
else loopControl_u98<=scoreboard_4b3706a6_and;
end
always @(posedge CLK)
begin
if (scoreboard_4b3706a6_and)
fbReg_idx_u47<=syncEnable_u1464_u0;
end
always @(posedge CLK)
begin
if (GO)
syncEnable_u1465_u0<=RESET;
end
always @(posedge CLK)
begin
if (scoreboard_4b3706a6_and)
fbReg_tmp_row_u47<=latch_2d209920_out;
end
assign mux_u1950_u0=(GO)?32'h0:fbReg_tmp_row1_u47;
assign mux_u1951_u0=(GO)?32'h0:fbReg_tmp_u47;
always @(posedge CLK)
begin
if (scoreboard_4b3706a6_and)
fbReg_tmp_u47<=latch_0d8d695a_out;
end
always @(posedge CLK)
begin
if (scoreboard_4b3706a6_and)
fbReg_tmp_row1_u47<=latch_01f00ea9_out;
end
assign mux_u1952_u0=(GO)?1'h0:fbReg_swapped_u47;
assign mux_u1953_u0=(GO)?32'h0:fbReg_tmp_row0_u47;
assign and_u3841_u0=reg_352b0be2_u0&port_3395b64e_;
assign simplePinWrite=16'h1&{16{1'h1}};
assign simplePinWrite_u626=reg_352b0be2_u0&{1{reg_352b0be2_u0}};
assign simplePinWrite_u627=port_3861adec_[7:0];
always @(posedge CLK or posedge RESET)
begin
if (RESET)
reg_352b0be2_u0<=1'h0;
else reg_352b0be2_u0<=and_u3824_u0;
end
assign mux_u1954_u0=(or_u1467_u0)?mux_u1941_u0:32'h32;
assign or_u1470_u0=or_u1467_u0|reg_352b0be2_u0;
always @(posedge CLK or posedge RESET)
begin
if (RESET)
reg_352b0be2_result_delayed_u0<=1'h0;
else reg_352b0be2_result_delayed_u0<=reg_352b0be2_u0;
end
assign RESULT=GO;
assign RESULT_u2245=32'h0;
assign RESULT_u2246=or_u1470_u0;
assign RESULT_u2247=mux_u1954_u0;
assign RESULT_u2248=3'h1;
assign RESULT_u2249=or_u1466_u0;
assign RESULT_u2250=mux_u1940_u0;
assign RESULT_u2251=3'h1;
assign RESULT_u2252=or_u1464_u0;
assign RESULT_u2253=mux_u1940_u0;
assign RESULT_u2254=mux_u1936_u0;
assign RESULT_u2255=3'h1;
assign RESULT_u2256=simplePinWrite_u627;
assign RESULT_u2257=simplePinWrite;
assign RESULT_u2258=simplePinWrite_u626;
assign DONE=reg_352b0be2_result_delayed_u0;
endmodule



module medianRow15_endianswapper_64109b68_(endianswapper_64109b68_in, endianswapper_64109b68_out);
input	[31:0]	endianswapper_64109b68_in;
output	[31:0]	endianswapper_64109b68_out;
assign endianswapper_64109b68_out=endianswapper_64109b68_in;
endmodule



module medianRow15_endianswapper_5962e72c_(endianswapper_5962e72c_in, endianswapper_5962e72c_out);
input	[31:0]	endianswapper_5962e72c_in;
output	[31:0]	endianswapper_5962e72c_out;
assign endianswapper_5962e72c_out=endianswapper_5962e72c_in;
endmodule



module medianRow15_stateVar_i(bus_216c6464_, bus_45fc9e40_, bus_22e42cfc_, bus_41c6af53_, bus_27b62153_, bus_7a2269bf_, bus_50e4b31d_);
input		bus_216c6464_;
input		bus_45fc9e40_;
input		bus_22e42cfc_;
input	[31:0]	bus_41c6af53_;
input		bus_27b62153_;
input	[31:0]	bus_7a2269bf_;
output	[31:0]	bus_50e4b31d_;
wire	[31:0]	endianswapper_64109b68_out;
wire	[31:0]	mux_7a45c3de_u0;
reg	[31:0]	stateVar_i_u49=32'h0;
wire		or_031375be_u0;
wire	[31:0]	endianswapper_5962e72c_out;
assign bus_50e4b31d_=endianswapper_5962e72c_out;
medianRow15_endianswapper_64109b68_ medianRow15_endianswapper_64109b68__1(.endianswapper_64109b68_in(mux_7a45c3de_u0), 
  .endianswapper_64109b68_out(endianswapper_64109b68_out));
assign mux_7a45c3de_u0=(bus_22e42cfc_)?bus_41c6af53_:32'h0;
always @(posedge bus_216c6464_ or posedge bus_45fc9e40_)
begin
if (bus_45fc9e40_)
stateVar_i_u49<=32'h0;
else if (or_031375be_u0)
stateVar_i_u49<=endianswapper_64109b68_out;
end
assign or_031375be_u0=bus_22e42cfc_|bus_27b62153_;
medianRow15_endianswapper_5962e72c_ medianRow15_endianswapper_5962e72c__1(.endianswapper_5962e72c_in(stateVar_i_u49), 
  .endianswapper_5962e72c_out(endianswapper_5962e72c_out));
endmodule



module medianRow15_simplememoryreferee_3cb498ed_(bus_415400a4_, bus_450dd27a_, bus_066efbeb_, bus_2f93382a_, bus_49688931_, bus_207345a7_, bus_375789c7_, bus_63e5f82a_, bus_57dab543_, bus_2dd6155f_, bus_128cdc0c_, bus_1ae98ba1_, bus_1f8aa73e_, bus_615fc111_);
input		bus_415400a4_;
input		bus_450dd27a_;
input		bus_066efbeb_;
input	[31:0]	bus_2f93382a_;
input		bus_49688931_;
input	[31:0]	bus_207345a7_;
input	[2:0]	bus_375789c7_;
output	[31:0]	bus_63e5f82a_;
output	[31:0]	bus_57dab543_;
output		bus_2dd6155f_;
output		bus_128cdc0c_;
output	[2:0]	bus_1ae98ba1_;
output	[31:0]	bus_1f8aa73e_;
output		bus_615fc111_;
assign bus_63e5f82a_=32'h0;
assign bus_57dab543_=bus_207345a7_;
assign bus_2dd6155f_=1'h0;
assign bus_128cdc0c_=bus_49688931_;
assign bus_1ae98ba1_=3'h1;
assign bus_1f8aa73e_=bus_2f93382a_;
assign bus_615fc111_=bus_066efbeb_;
endmodule



module medianRow15_forge_memory_101x32_147(CLK, ENA, WEA, DINA, ENB, ADDRA, ADDRB, DOUTA, DOUTB, DONEA, DONEB);
input		CLK;
input		ENA;
input		WEA;
input	[31:0]	DINA;
input		ENB;
input	[31:0]	ADDRA;
input	[31:0]	ADDRB;
output	[31:0]	DOUTA;
output	[31:0]	DOUTB;
output		DONEA;
output		DONEB;
wire		wea_0;
wire	[31:0]	pre_douta_0;
wire	[31:0]	pre_doutb_0;
wire		wea_1;
wire	[31:0]	pre_douta_1;
wire	[31:0]	pre_doutb_1;
reg		wea_done;
reg		rea_done;
reg	[31:0]	mux_outa;
reg	[31:0]	mux_outb;
assign wea_0=WEA&(ADDRA[31:6]==26'h0);
assign wea_1=WEA&(ADDRA[31:6]==26'h1);
always @(posedge CLK)
begin
wea_done<=WEA;
end
assign DONEA=rea_done|wea_done;
always @(posedge CLK)
begin
end
assign DONEB=ENB;
always @(pre_douta_0 or pre_douta_1 or ADDRA)
begin
case (ADDRA[31:6])26'd0:mux_outa=pre_douta_0;
26'd1:mux_outa=pre_douta_1;
default:mux_outa=32'h0;
endcase end
assign DOUTA=mux_outa;
always @(pre_doutb_0 or pre_doutb_1 or ADDRB)
begin
case (ADDRB[31:6])26'd0:mux_outb=pre_doutb_0;
26'd1:mux_outb=pre_doutb_1;
default:mux_outb=32'h0;
endcase end
assign DOUTB=mux_outb;
// Memory array element: COL: 0, ROW: 0
//  Initialization of Dual Port LUT ram now done through explicit
// parameter setting.
RAM64X1D#(.INIT(64'h0000000000000000))RAM64X1D_instance_3008(.WCLK(CLK), .WE(wea_0), 
  .A0(ADDRA[0]), .A1(ADDRA[1]), .A2(ADDRA[2]), .A3(ADDRA[3]), .A4(ADDRA[4]), .A5(ADDRA[5]), 
  .D(DINA[0]), .SPO(pre_douta_0[0]), .DPRA0(ADDRB[0]), .DPRA1(ADDRB[1]), .DPRA2(ADDRB[2]), 
  .DPRA3(ADDRB[3]), .DPRA4(ADDRB[4]), .DPRA5(ADDRB[5]), .DPO(pre_doutb_0[0]));
// Memory array element: COL: 0, ROW: 1
//  Initialization of Dual Port LUT ram now done through explicit
// parameter setting.
RAM64X1D#(.INIT(64'h0000000000000000))RAM64X1D_instance_3009(.WCLK(CLK), .WE(wea_0), 
  .A0(ADDRA[0]), .A1(ADDRA[1]), .A2(ADDRA[2]), .A3(ADDRA[3]), .A4(ADDRA[4]), .A5(ADDRA[5]), 
  .D(DINA[1]), .SPO(pre_douta_0[1]), .DPRA0(ADDRB[0]), .DPRA1(ADDRB[1]), .DPRA2(ADDRB[2]), 
  .DPRA3(ADDRB[3]), .DPRA4(ADDRB[4]), .DPRA5(ADDRB[5]), .DPO(pre_doutb_0[1]));
// Memory array element: COL: 0, ROW: 2
//  Initialization of Dual Port LUT ram now done through explicit
// parameter setting.
RAM64X1D#(.INIT(64'h0000000000000000))RAM64X1D_instance_3010(.WCLK(CLK), .WE(wea_0), 
  .A0(ADDRA[0]), .A1(ADDRA[1]), .A2(ADDRA[2]), .A3(ADDRA[3]), .A4(ADDRA[4]), .A5(ADDRA[5]), 
  .D(DINA[2]), .SPO(pre_douta_0[2]), .DPRA0(ADDRB[0]), .DPRA1(ADDRB[1]), .DPRA2(ADDRB[2]), 
  .DPRA3(ADDRB[3]), .DPRA4(ADDRB[4]), .DPRA5(ADDRB[5]), .DPO(pre_doutb_0[2]));
// Memory array element: COL: 0, ROW: 3
//  Initialization of Dual Port LUT ram now done through explicit
// parameter setting.
RAM64X1D#(.INIT(64'h0000000000000000))RAM64X1D_instance_3011(.WCLK(CLK), .WE(wea_0), 
  .A0(ADDRA[0]), .A1(ADDRA[1]), .A2(ADDRA[2]), .A3(ADDRA[3]), .A4(ADDRA[4]), .A5(ADDRA[5]), 
  .D(DINA[3]), .SPO(pre_douta_0[3]), .DPRA0(ADDRB[0]), .DPRA1(ADDRB[1]), .DPRA2(ADDRB[2]), 
  .DPRA3(ADDRB[3]), .DPRA4(ADDRB[4]), .DPRA5(ADDRB[5]), .DPO(pre_doutb_0[3]));
// Memory array element: COL: 0, ROW: 4
//  Initialization of Dual Port LUT ram now done through explicit
// parameter setting.
RAM64X1D#(.INIT(64'h0000000000000000))RAM64X1D_instance_3012(.WCLK(CLK), .WE(wea_0), 
  .A0(ADDRA[0]), .A1(ADDRA[1]), .A2(ADDRA[2]), .A3(ADDRA[3]), .A4(ADDRA[4]), .A5(ADDRA[5]), 
  .D(DINA[4]), .SPO(pre_douta_0[4]), .DPRA0(ADDRB[0]), .DPRA1(ADDRB[1]), .DPRA2(ADDRB[2]), 
  .DPRA3(ADDRB[3]), .DPRA4(ADDRB[4]), .DPRA5(ADDRB[5]), .DPO(pre_doutb_0[4]));
// Memory array element: COL: 0, ROW: 5
//  Initialization of Dual Port LUT ram now done through explicit
// parameter setting.
RAM64X1D#(.INIT(64'h0000000000000000))RAM64X1D_instance_3013(.WCLK(CLK), .WE(wea_0), 
  .A0(ADDRA[0]), .A1(ADDRA[1]), .A2(ADDRA[2]), .A3(ADDRA[3]), .A4(ADDRA[4]), .A5(ADDRA[5]), 
  .D(DINA[5]), .SPO(pre_douta_0[5]), .DPRA0(ADDRB[0]), .DPRA1(ADDRB[1]), .DPRA2(ADDRB[2]), 
  .DPRA3(ADDRB[3]), .DPRA4(ADDRB[4]), .DPRA5(ADDRB[5]), .DPO(pre_doutb_0[5]));
// Memory array element: COL: 0, ROW: 6
//  Initialization of Dual Port LUT ram now done through explicit
// parameter setting.
RAM64X1D#(.INIT(64'h0000000000000000))RAM64X1D_instance_3014(.WCLK(CLK), .WE(wea_0), 
  .A0(ADDRA[0]), .A1(ADDRA[1]), .A2(ADDRA[2]), .A3(ADDRA[3]), .A4(ADDRA[4]), .A5(ADDRA[5]), 
  .D(DINA[6]), .SPO(pre_douta_0[6]), .DPRA0(ADDRB[0]), .DPRA1(ADDRB[1]), .DPRA2(ADDRB[2]), 
  .DPRA3(ADDRB[3]), .DPRA4(ADDRB[4]), .DPRA5(ADDRB[5]), .DPO(pre_doutb_0[6]));
// Memory array element: COL: 0, ROW: 7
//  Initialization of Dual Port LUT ram now done through explicit
// parameter setting.
RAM64X1D#(.INIT(64'h0000000000000000))RAM64X1D_instance_3015(.WCLK(CLK), .WE(wea_0), 
  .A0(ADDRA[0]), .A1(ADDRA[1]), .A2(ADDRA[2]), .A3(ADDRA[3]), .A4(ADDRA[4]), .A5(ADDRA[5]), 
  .D(DINA[7]), .SPO(pre_douta_0[7]), .DPRA0(ADDRB[0]), .DPRA1(ADDRB[1]), .DPRA2(ADDRB[2]), 
  .DPRA3(ADDRB[3]), .DPRA4(ADDRB[4]), .DPRA5(ADDRB[5]), .DPO(pre_doutb_0[7]));
// Memory array element: COL: 0, ROW: 8
//  Initialization of Dual Port LUT ram now done through explicit
// parameter setting.
RAM64X1D#(.INIT(64'h0000000000000000))RAM64X1D_instance_3016(.WCLK(CLK), .WE(wea_0), 
  .A0(ADDRA[0]), .A1(ADDRA[1]), .A2(ADDRA[2]), .A3(ADDRA[3]), .A4(ADDRA[4]), .A5(ADDRA[5]), 
  .D(DINA[8]), .SPO(pre_douta_0[8]), .DPRA0(ADDRB[0]), .DPRA1(ADDRB[1]), .DPRA2(ADDRB[2]), 
  .DPRA3(ADDRB[3]), .DPRA4(ADDRB[4]), .DPRA5(ADDRB[5]), .DPO(pre_doutb_0[8]));
// Memory array element: COL: 0, ROW: 9
//  Initialization of Dual Port LUT ram now done through explicit
// parameter setting.
RAM64X1D#(.INIT(64'h0000000000000000))RAM64X1D_instance_3017(.WCLK(CLK), .WE(wea_0), 
  .A0(ADDRA[0]), .A1(ADDRA[1]), .A2(ADDRA[2]), .A3(ADDRA[3]), .A4(ADDRA[4]), .A5(ADDRA[5]), 
  .D(DINA[9]), .SPO(pre_douta_0[9]), .DPRA0(ADDRB[0]), .DPRA1(ADDRB[1]), .DPRA2(ADDRB[2]), 
  .DPRA3(ADDRB[3]), .DPRA4(ADDRB[4]), .DPRA5(ADDRB[5]), .DPO(pre_doutb_0[9]));
// Memory array element: COL: 0, ROW: 10
//  Initialization of Dual Port LUT ram now done through explicit
// parameter setting.
RAM64X1D#(.INIT(64'h0000000000000000))RAM64X1D_instance_3018(.WCLK(CLK), .WE(wea_0), 
  .A0(ADDRA[0]), .A1(ADDRA[1]), .A2(ADDRA[2]), .A3(ADDRA[3]), .A4(ADDRA[4]), .A5(ADDRA[5]), 
  .D(DINA[10]), .SPO(pre_douta_0[10]), .DPRA0(ADDRB[0]), .DPRA1(ADDRB[1]), .DPRA2(ADDRB[2]), 
  .DPRA3(ADDRB[3]), .DPRA4(ADDRB[4]), .DPRA5(ADDRB[5]), .DPO(pre_doutb_0[10]));
// Memory array element: COL: 0, ROW: 11
//  Initialization of Dual Port LUT ram now done through explicit
// parameter setting.
RAM64X1D#(.INIT(64'h0000000000000000))RAM64X1D_instance_3019(.WCLK(CLK), .WE(wea_0), 
  .A0(ADDRA[0]), .A1(ADDRA[1]), .A2(ADDRA[2]), .A3(ADDRA[3]), .A4(ADDRA[4]), .A5(ADDRA[5]), 
  .D(DINA[11]), .SPO(pre_douta_0[11]), .DPRA0(ADDRB[0]), .DPRA1(ADDRB[1]), .DPRA2(ADDRB[2]), 
  .DPRA3(ADDRB[3]), .DPRA4(ADDRB[4]), .DPRA5(ADDRB[5]), .DPO(pre_doutb_0[11]));
// Memory array element: COL: 0, ROW: 12
//  Initialization of Dual Port LUT ram now done through explicit
// parameter setting.
RAM64X1D#(.INIT(64'h0000000000000000))RAM64X1D_instance_3020(.WCLK(CLK), .WE(wea_0), 
  .A0(ADDRA[0]), .A1(ADDRA[1]), .A2(ADDRA[2]), .A3(ADDRA[3]), .A4(ADDRA[4]), .A5(ADDRA[5]), 
  .D(DINA[12]), .SPO(pre_douta_0[12]), .DPRA0(ADDRB[0]), .DPRA1(ADDRB[1]), .DPRA2(ADDRB[2]), 
  .DPRA3(ADDRB[3]), .DPRA4(ADDRB[4]), .DPRA5(ADDRB[5]), .DPO(pre_doutb_0[12]));
// Memory array element: COL: 0, ROW: 13
//  Initialization of Dual Port LUT ram now done through explicit
// parameter setting.
RAM64X1D#(.INIT(64'h0000000000000000))RAM64X1D_instance_3021(.WCLK(CLK), .WE(wea_0), 
  .A0(ADDRA[0]), .A1(ADDRA[1]), .A2(ADDRA[2]), .A3(ADDRA[3]), .A4(ADDRA[4]), .A5(ADDRA[5]), 
  .D(DINA[13]), .SPO(pre_douta_0[13]), .DPRA0(ADDRB[0]), .DPRA1(ADDRB[1]), .DPRA2(ADDRB[2]), 
  .DPRA3(ADDRB[3]), .DPRA4(ADDRB[4]), .DPRA5(ADDRB[5]), .DPO(pre_doutb_0[13]));
// Memory array element: COL: 0, ROW: 14
//  Initialization of Dual Port LUT ram now done through explicit
// parameter setting.
RAM64X1D#(.INIT(64'h0000000000000000))RAM64X1D_instance_3022(.WCLK(CLK), .WE(wea_0), 
  .A0(ADDRA[0]), .A1(ADDRA[1]), .A2(ADDRA[2]), .A3(ADDRA[3]), .A4(ADDRA[4]), .A5(ADDRA[5]), 
  .D(DINA[14]), .SPO(pre_douta_0[14]), .DPRA0(ADDRB[0]), .DPRA1(ADDRB[1]), .DPRA2(ADDRB[2]), 
  .DPRA3(ADDRB[3]), .DPRA4(ADDRB[4]), .DPRA5(ADDRB[5]), .DPO(pre_doutb_0[14]));
// Memory array element: COL: 0, ROW: 15
//  Initialization of Dual Port LUT ram now done through explicit
// parameter setting.
RAM64X1D#(.INIT(64'h0000000000000000))RAM64X1D_instance_3023(.WCLK(CLK), .WE(wea_0), 
  .A0(ADDRA[0]), .A1(ADDRA[1]), .A2(ADDRA[2]), .A3(ADDRA[3]), .A4(ADDRA[4]), .A5(ADDRA[5]), 
  .D(DINA[15]), .SPO(pre_douta_0[15]), .DPRA0(ADDRB[0]), .DPRA1(ADDRB[1]), .DPRA2(ADDRB[2]), 
  .DPRA3(ADDRB[3]), .DPRA4(ADDRB[4]), .DPRA5(ADDRB[5]), .DPO(pre_doutb_0[15]));
// Memory array element: COL: 0, ROW: 16
//  Initialization of Dual Port LUT ram now done through explicit
// parameter setting.
RAM64X1D#(.INIT(64'h0000000000000000))RAM64X1D_instance_3024(.WCLK(CLK), .WE(wea_0), 
  .A0(ADDRA[0]), .A1(ADDRA[1]), .A2(ADDRA[2]), .A3(ADDRA[3]), .A4(ADDRA[4]), .A5(ADDRA[5]), 
  .D(DINA[16]), .SPO(pre_douta_0[16]), .DPRA0(ADDRB[0]), .DPRA1(ADDRB[1]), .DPRA2(ADDRB[2]), 
  .DPRA3(ADDRB[3]), .DPRA4(ADDRB[4]), .DPRA5(ADDRB[5]), .DPO(pre_doutb_0[16]));
// Memory array element: COL: 0, ROW: 17
//  Initialization of Dual Port LUT ram now done through explicit
// parameter setting.
RAM64X1D#(.INIT(64'h0000000000000000))RAM64X1D_instance_3025(.WCLK(CLK), .WE(wea_0), 
  .A0(ADDRA[0]), .A1(ADDRA[1]), .A2(ADDRA[2]), .A3(ADDRA[3]), .A4(ADDRA[4]), .A5(ADDRA[5]), 
  .D(DINA[17]), .SPO(pre_douta_0[17]), .DPRA0(ADDRB[0]), .DPRA1(ADDRB[1]), .DPRA2(ADDRB[2]), 
  .DPRA3(ADDRB[3]), .DPRA4(ADDRB[4]), .DPRA5(ADDRB[5]), .DPO(pre_doutb_0[17]));
// Memory array element: COL: 0, ROW: 18
//  Initialization of Dual Port LUT ram now done through explicit
// parameter setting.
RAM64X1D#(.INIT(64'h0000000000000000))RAM64X1D_instance_3026(.WCLK(CLK), .WE(wea_0), 
  .A0(ADDRA[0]), .A1(ADDRA[1]), .A2(ADDRA[2]), .A3(ADDRA[3]), .A4(ADDRA[4]), .A5(ADDRA[5]), 
  .D(DINA[18]), .SPO(pre_douta_0[18]), .DPRA0(ADDRB[0]), .DPRA1(ADDRB[1]), .DPRA2(ADDRB[2]), 
  .DPRA3(ADDRB[3]), .DPRA4(ADDRB[4]), .DPRA5(ADDRB[5]), .DPO(pre_doutb_0[18]));
// Memory array element: COL: 0, ROW: 19
//  Initialization of Dual Port LUT ram now done through explicit
// parameter setting.
RAM64X1D#(.INIT(64'h0000000000000000))RAM64X1D_instance_3027(.WCLK(CLK), .WE(wea_0), 
  .A0(ADDRA[0]), .A1(ADDRA[1]), .A2(ADDRA[2]), .A3(ADDRA[3]), .A4(ADDRA[4]), .A5(ADDRA[5]), 
  .D(DINA[19]), .SPO(pre_douta_0[19]), .DPRA0(ADDRB[0]), .DPRA1(ADDRB[1]), .DPRA2(ADDRB[2]), 
  .DPRA3(ADDRB[3]), .DPRA4(ADDRB[4]), .DPRA5(ADDRB[5]), .DPO(pre_doutb_0[19]));
// Memory array element: COL: 0, ROW: 20
//  Initialization of Dual Port LUT ram now done through explicit
// parameter setting.
RAM64X1D#(.INIT(64'h0000000000000000))RAM64X1D_instance_3028(.WCLK(CLK), .WE(wea_0), 
  .A0(ADDRA[0]), .A1(ADDRA[1]), .A2(ADDRA[2]), .A3(ADDRA[3]), .A4(ADDRA[4]), .A5(ADDRA[5]), 
  .D(DINA[20]), .SPO(pre_douta_0[20]), .DPRA0(ADDRB[0]), .DPRA1(ADDRB[1]), .DPRA2(ADDRB[2]), 
  .DPRA3(ADDRB[3]), .DPRA4(ADDRB[4]), .DPRA5(ADDRB[5]), .DPO(pre_doutb_0[20]));
// Memory array element: COL: 0, ROW: 21
//  Initialization of Dual Port LUT ram now done through explicit
// parameter setting.
RAM64X1D#(.INIT(64'h0000000000000000))RAM64X1D_instance_3029(.WCLK(CLK), .WE(wea_0), 
  .A0(ADDRA[0]), .A1(ADDRA[1]), .A2(ADDRA[2]), .A3(ADDRA[3]), .A4(ADDRA[4]), .A5(ADDRA[5]), 
  .D(DINA[21]), .SPO(pre_douta_0[21]), .DPRA0(ADDRB[0]), .DPRA1(ADDRB[1]), .DPRA2(ADDRB[2]), 
  .DPRA3(ADDRB[3]), .DPRA4(ADDRB[4]), .DPRA5(ADDRB[5]), .DPO(pre_doutb_0[21]));
// Memory array element: COL: 0, ROW: 22
//  Initialization of Dual Port LUT ram now done through explicit
// parameter setting.
RAM64X1D#(.INIT(64'h0000000000000000))RAM64X1D_instance_3030(.WCLK(CLK), .WE(wea_0), 
  .A0(ADDRA[0]), .A1(ADDRA[1]), .A2(ADDRA[2]), .A3(ADDRA[3]), .A4(ADDRA[4]), .A5(ADDRA[5]), 
  .D(DINA[22]), .SPO(pre_douta_0[22]), .DPRA0(ADDRB[0]), .DPRA1(ADDRB[1]), .DPRA2(ADDRB[2]), 
  .DPRA3(ADDRB[3]), .DPRA4(ADDRB[4]), .DPRA5(ADDRB[5]), .DPO(pre_doutb_0[22]));
// Memory array element: COL: 0, ROW: 23
//  Initialization of Dual Port LUT ram now done through explicit
// parameter setting.
RAM64X1D#(.INIT(64'h0000000000000000))RAM64X1D_instance_3031(.WCLK(CLK), .WE(wea_0), 
  .A0(ADDRA[0]), .A1(ADDRA[1]), .A2(ADDRA[2]), .A3(ADDRA[3]), .A4(ADDRA[4]), .A5(ADDRA[5]), 
  .D(DINA[23]), .SPO(pre_douta_0[23]), .DPRA0(ADDRB[0]), .DPRA1(ADDRB[1]), .DPRA2(ADDRB[2]), 
  .DPRA3(ADDRB[3]), .DPRA4(ADDRB[4]), .DPRA5(ADDRB[5]), .DPO(pre_doutb_0[23]));
// Memory array element: COL: 0, ROW: 24
//  Initialization of Dual Port LUT ram now done through explicit
// parameter setting.
RAM64X1D#(.INIT(64'h0000000000000000))RAM64X1D_instance_3032(.WCLK(CLK), .WE(wea_0), 
  .A0(ADDRA[0]), .A1(ADDRA[1]), .A2(ADDRA[2]), .A3(ADDRA[3]), .A4(ADDRA[4]), .A5(ADDRA[5]), 
  .D(DINA[24]), .SPO(pre_douta_0[24]), .DPRA0(ADDRB[0]), .DPRA1(ADDRB[1]), .DPRA2(ADDRB[2]), 
  .DPRA3(ADDRB[3]), .DPRA4(ADDRB[4]), .DPRA5(ADDRB[5]), .DPO(pre_doutb_0[24]));
// Memory array element: COL: 0, ROW: 25
//  Initialization of Dual Port LUT ram now done through explicit
// parameter setting.
RAM64X1D#(.INIT(64'h0000000000000000))RAM64X1D_instance_3033(.WCLK(CLK), .WE(wea_0), 
  .A0(ADDRA[0]), .A1(ADDRA[1]), .A2(ADDRA[2]), .A3(ADDRA[3]), .A4(ADDRA[4]), .A5(ADDRA[5]), 
  .D(DINA[25]), .SPO(pre_douta_0[25]), .DPRA0(ADDRB[0]), .DPRA1(ADDRB[1]), .DPRA2(ADDRB[2]), 
  .DPRA3(ADDRB[3]), .DPRA4(ADDRB[4]), .DPRA5(ADDRB[5]), .DPO(pre_doutb_0[25]));
// Memory array element: COL: 0, ROW: 26
//  Initialization of Dual Port LUT ram now done through explicit
// parameter setting.
RAM64X1D#(.INIT(64'h0000000000000000))RAM64X1D_instance_3034(.WCLK(CLK), .WE(wea_0), 
  .A0(ADDRA[0]), .A1(ADDRA[1]), .A2(ADDRA[2]), .A3(ADDRA[3]), .A4(ADDRA[4]), .A5(ADDRA[5]), 
  .D(DINA[26]), .SPO(pre_douta_0[26]), .DPRA0(ADDRB[0]), .DPRA1(ADDRB[1]), .DPRA2(ADDRB[2]), 
  .DPRA3(ADDRB[3]), .DPRA4(ADDRB[4]), .DPRA5(ADDRB[5]), .DPO(pre_doutb_0[26]));
// Memory array element: COL: 0, ROW: 27
//  Initialization of Dual Port LUT ram now done through explicit
// parameter setting.
RAM64X1D#(.INIT(64'h0000000000000000))RAM64X1D_instance_3035(.WCLK(CLK), .WE(wea_0), 
  .A0(ADDRA[0]), .A1(ADDRA[1]), .A2(ADDRA[2]), .A3(ADDRA[3]), .A4(ADDRA[4]), .A5(ADDRA[5]), 
  .D(DINA[27]), .SPO(pre_douta_0[27]), .DPRA0(ADDRB[0]), .DPRA1(ADDRB[1]), .DPRA2(ADDRB[2]), 
  .DPRA3(ADDRB[3]), .DPRA4(ADDRB[4]), .DPRA5(ADDRB[5]), .DPO(pre_doutb_0[27]));
// Memory array element: COL: 0, ROW: 28
//  Initialization of Dual Port LUT ram now done through explicit
// parameter setting.
RAM64X1D#(.INIT(64'h0000000000000000))RAM64X1D_instance_3036(.WCLK(CLK), .WE(wea_0), 
  .A0(ADDRA[0]), .A1(ADDRA[1]), .A2(ADDRA[2]), .A3(ADDRA[3]), .A4(ADDRA[4]), .A5(ADDRA[5]), 
  .D(DINA[28]), .SPO(pre_douta_0[28]), .DPRA0(ADDRB[0]), .DPRA1(ADDRB[1]), .DPRA2(ADDRB[2]), 
  .DPRA3(ADDRB[3]), .DPRA4(ADDRB[4]), .DPRA5(ADDRB[5]), .DPO(pre_doutb_0[28]));
// Memory array element: COL: 0, ROW: 29
//  Initialization of Dual Port LUT ram now done through explicit
// parameter setting.
RAM64X1D#(.INIT(64'h0000000000000000))RAM64X1D_instance_3037(.WCLK(CLK), .WE(wea_0), 
  .A0(ADDRA[0]), .A1(ADDRA[1]), .A2(ADDRA[2]), .A3(ADDRA[3]), .A4(ADDRA[4]), .A5(ADDRA[5]), 
  .D(DINA[29]), .SPO(pre_douta_0[29]), .DPRA0(ADDRB[0]), .DPRA1(ADDRB[1]), .DPRA2(ADDRB[2]), 
  .DPRA3(ADDRB[3]), .DPRA4(ADDRB[4]), .DPRA5(ADDRB[5]), .DPO(pre_doutb_0[29]));
// Memory array element: COL: 0, ROW: 30
//  Initialization of Dual Port LUT ram now done through explicit
// parameter setting.
RAM64X1D#(.INIT(64'h0000000000000000))RAM64X1D_instance_3038(.WCLK(CLK), .WE(wea_0), 
  .A0(ADDRA[0]), .A1(ADDRA[1]), .A2(ADDRA[2]), .A3(ADDRA[3]), .A4(ADDRA[4]), .A5(ADDRA[5]), 
  .D(DINA[30]), .SPO(pre_douta_0[30]), .DPRA0(ADDRB[0]), .DPRA1(ADDRB[1]), .DPRA2(ADDRB[2]), 
  .DPRA3(ADDRB[3]), .DPRA4(ADDRB[4]), .DPRA5(ADDRB[5]), .DPO(pre_doutb_0[30]));
// Memory array element: COL: 0, ROW: 31
//  Initialization of Dual Port LUT ram now done through explicit
// parameter setting.
RAM64X1D#(.INIT(64'h0000000000000000))RAM64X1D_instance_3039(.WCLK(CLK), .WE(wea_0), 
  .A0(ADDRA[0]), .A1(ADDRA[1]), .A2(ADDRA[2]), .A3(ADDRA[3]), .A4(ADDRA[4]), .A5(ADDRA[5]), 
  .D(DINA[31]), .SPO(pre_douta_0[31]), .DPRA0(ADDRB[0]), .DPRA1(ADDRB[1]), .DPRA2(ADDRB[2]), 
  .DPRA3(ADDRB[3]), .DPRA4(ADDRB[4]), .DPRA5(ADDRB[5]), .DPO(pre_doutb_0[31]));
// Memory array element: COL: 1, ROW: 0
//  Initialization of Dual Port LUT ram now done through explicit
// parameter setting.
RAM64X1D#(.INIT(64'h0000000000000000))RAM64X1D_instance_3040(.WCLK(CLK), .WE(wea_1), 
  .A0(ADDRA[0]), .A1(ADDRA[1]), .A2(ADDRA[2]), .A3(ADDRA[3]), .A4(ADDRA[4]), .A5(ADDRA[5]), 
  .D(DINA[0]), .SPO(pre_douta_1[0]), .DPRA0(ADDRB[0]), .DPRA1(ADDRB[1]), .DPRA2(ADDRB[2]), 
  .DPRA3(ADDRB[3]), .DPRA4(ADDRB[4]), .DPRA5(ADDRB[5]), .DPO(pre_doutb_1[0]));
// Memory array element: COL: 1, ROW: 1
//  Initialization of Dual Port LUT ram now done through explicit
// parameter setting.
RAM64X1D#(.INIT(64'h0000000000000000))RAM64X1D_instance_3041(.WCLK(CLK), .WE(wea_1), 
  .A0(ADDRA[0]), .A1(ADDRA[1]), .A2(ADDRA[2]), .A3(ADDRA[3]), .A4(ADDRA[4]), .A5(ADDRA[5]), 
  .D(DINA[1]), .SPO(pre_douta_1[1]), .DPRA0(ADDRB[0]), .DPRA1(ADDRB[1]), .DPRA2(ADDRB[2]), 
  .DPRA3(ADDRB[3]), .DPRA4(ADDRB[4]), .DPRA5(ADDRB[5]), .DPO(pre_doutb_1[1]));
// Memory array element: COL: 1, ROW: 2
//  Initialization of Dual Port LUT ram now done through explicit
// parameter setting.
RAM64X1D#(.INIT(64'h0000000000000000))RAM64X1D_instance_3042(.WCLK(CLK), .WE(wea_1), 
  .A0(ADDRA[0]), .A1(ADDRA[1]), .A2(ADDRA[2]), .A3(ADDRA[3]), .A4(ADDRA[4]), .A5(ADDRA[5]), 
  .D(DINA[2]), .SPO(pre_douta_1[2]), .DPRA0(ADDRB[0]), .DPRA1(ADDRB[1]), .DPRA2(ADDRB[2]), 
  .DPRA3(ADDRB[3]), .DPRA4(ADDRB[4]), .DPRA5(ADDRB[5]), .DPO(pre_doutb_1[2]));
// Memory array element: COL: 1, ROW: 3
//  Initialization of Dual Port LUT ram now done through explicit
// parameter setting.
RAM64X1D#(.INIT(64'h0000000000000000))RAM64X1D_instance_3043(.WCLK(CLK), .WE(wea_1), 
  .A0(ADDRA[0]), .A1(ADDRA[1]), .A2(ADDRA[2]), .A3(ADDRA[3]), .A4(ADDRA[4]), .A5(ADDRA[5]), 
  .D(DINA[3]), .SPO(pre_douta_1[3]), .DPRA0(ADDRB[0]), .DPRA1(ADDRB[1]), .DPRA2(ADDRB[2]), 
  .DPRA3(ADDRB[3]), .DPRA4(ADDRB[4]), .DPRA5(ADDRB[5]), .DPO(pre_doutb_1[3]));
// Memory array element: COL: 1, ROW: 4
//  Initialization of Dual Port LUT ram now done through explicit
// parameter setting.
RAM64X1D#(.INIT(64'h0000000000000000))RAM64X1D_instance_3044(.WCLK(CLK), .WE(wea_1), 
  .A0(ADDRA[0]), .A1(ADDRA[1]), .A2(ADDRA[2]), .A3(ADDRA[3]), .A4(ADDRA[4]), .A5(ADDRA[5]), 
  .D(DINA[4]), .SPO(pre_douta_1[4]), .DPRA0(ADDRB[0]), .DPRA1(ADDRB[1]), .DPRA2(ADDRB[2]), 
  .DPRA3(ADDRB[3]), .DPRA4(ADDRB[4]), .DPRA5(ADDRB[5]), .DPO(pre_doutb_1[4]));
// Memory array element: COL: 1, ROW: 5
//  Initialization of Dual Port LUT ram now done through explicit
// parameter setting.
RAM64X1D#(.INIT(64'h0000000000000000))RAM64X1D_instance_3045(.WCLK(CLK), .WE(wea_1), 
  .A0(ADDRA[0]), .A1(ADDRA[1]), .A2(ADDRA[2]), .A3(ADDRA[3]), .A4(ADDRA[4]), .A5(ADDRA[5]), 
  .D(DINA[5]), .SPO(pre_douta_1[5]), .DPRA0(ADDRB[0]), .DPRA1(ADDRB[1]), .DPRA2(ADDRB[2]), 
  .DPRA3(ADDRB[3]), .DPRA4(ADDRB[4]), .DPRA5(ADDRB[5]), .DPO(pre_doutb_1[5]));
// Memory array element: COL: 1, ROW: 6
//  Initialization of Dual Port LUT ram now done through explicit
// parameter setting.
RAM64X1D#(.INIT(64'h0000000000000000))RAM64X1D_instance_3046(.WCLK(CLK), .WE(wea_1), 
  .A0(ADDRA[0]), .A1(ADDRA[1]), .A2(ADDRA[2]), .A3(ADDRA[3]), .A4(ADDRA[4]), .A5(ADDRA[5]), 
  .D(DINA[6]), .SPO(pre_douta_1[6]), .DPRA0(ADDRB[0]), .DPRA1(ADDRB[1]), .DPRA2(ADDRB[2]), 
  .DPRA3(ADDRB[3]), .DPRA4(ADDRB[4]), .DPRA5(ADDRB[5]), .DPO(pre_doutb_1[6]));
// Memory array element: COL: 1, ROW: 7
//  Initialization of Dual Port LUT ram now done through explicit
// parameter setting.
RAM64X1D#(.INIT(64'h0000000000000000))RAM64X1D_instance_3047(.WCLK(CLK), .WE(wea_1), 
  .A0(ADDRA[0]), .A1(ADDRA[1]), .A2(ADDRA[2]), .A3(ADDRA[3]), .A4(ADDRA[4]), .A5(ADDRA[5]), 
  .D(DINA[7]), .SPO(pre_douta_1[7]), .DPRA0(ADDRB[0]), .DPRA1(ADDRB[1]), .DPRA2(ADDRB[2]), 
  .DPRA3(ADDRB[3]), .DPRA4(ADDRB[4]), .DPRA5(ADDRB[5]), .DPO(pre_doutb_1[7]));
// Memory array element: COL: 1, ROW: 8
//  Initialization of Dual Port LUT ram now done through explicit
// parameter setting.
RAM64X1D#(.INIT(64'h0000000000000000))RAM64X1D_instance_3048(.WCLK(CLK), .WE(wea_1), 
  .A0(ADDRA[0]), .A1(ADDRA[1]), .A2(ADDRA[2]), .A3(ADDRA[3]), .A4(ADDRA[4]), .A5(ADDRA[5]), 
  .D(DINA[8]), .SPO(pre_douta_1[8]), .DPRA0(ADDRB[0]), .DPRA1(ADDRB[1]), .DPRA2(ADDRB[2]), 
  .DPRA3(ADDRB[3]), .DPRA4(ADDRB[4]), .DPRA5(ADDRB[5]), .DPO(pre_doutb_1[8]));
// Memory array element: COL: 1, ROW: 9
//  Initialization of Dual Port LUT ram now done through explicit
// parameter setting.
RAM64X1D#(.INIT(64'h0000000000000000))RAM64X1D_instance_3049(.WCLK(CLK), .WE(wea_1), 
  .A0(ADDRA[0]), .A1(ADDRA[1]), .A2(ADDRA[2]), .A3(ADDRA[3]), .A4(ADDRA[4]), .A5(ADDRA[5]), 
  .D(DINA[9]), .SPO(pre_douta_1[9]), .DPRA0(ADDRB[0]), .DPRA1(ADDRB[1]), .DPRA2(ADDRB[2]), 
  .DPRA3(ADDRB[3]), .DPRA4(ADDRB[4]), .DPRA5(ADDRB[5]), .DPO(pre_doutb_1[9]));
// Memory array element: COL: 1, ROW: 10
//  Initialization of Dual Port LUT ram now done through explicit
// parameter setting.
RAM64X1D#(.INIT(64'h0000000000000000))RAM64X1D_instance_3050(.WCLK(CLK), .WE(wea_1), 
  .A0(ADDRA[0]), .A1(ADDRA[1]), .A2(ADDRA[2]), .A3(ADDRA[3]), .A4(ADDRA[4]), .A5(ADDRA[5]), 
  .D(DINA[10]), .SPO(pre_douta_1[10]), .DPRA0(ADDRB[0]), .DPRA1(ADDRB[1]), .DPRA2(ADDRB[2]), 
  .DPRA3(ADDRB[3]), .DPRA4(ADDRB[4]), .DPRA5(ADDRB[5]), .DPO(pre_doutb_1[10]));
// Memory array element: COL: 1, ROW: 11
//  Initialization of Dual Port LUT ram now done through explicit
// parameter setting.
RAM64X1D#(.INIT(64'h0000000000000000))RAM64X1D_instance_3051(.WCLK(CLK), .WE(wea_1), 
  .A0(ADDRA[0]), .A1(ADDRA[1]), .A2(ADDRA[2]), .A3(ADDRA[3]), .A4(ADDRA[4]), .A5(ADDRA[5]), 
  .D(DINA[11]), .SPO(pre_douta_1[11]), .DPRA0(ADDRB[0]), .DPRA1(ADDRB[1]), .DPRA2(ADDRB[2]), 
  .DPRA3(ADDRB[3]), .DPRA4(ADDRB[4]), .DPRA5(ADDRB[5]), .DPO(pre_doutb_1[11]));
// Memory array element: COL: 1, ROW: 12
//  Initialization of Dual Port LUT ram now done through explicit
// parameter setting.
RAM64X1D#(.INIT(64'h0000000000000000))RAM64X1D_instance_3052(.WCLK(CLK), .WE(wea_1), 
  .A0(ADDRA[0]), .A1(ADDRA[1]), .A2(ADDRA[2]), .A3(ADDRA[3]), .A4(ADDRA[4]), .A5(ADDRA[5]), 
  .D(DINA[12]), .SPO(pre_douta_1[12]), .DPRA0(ADDRB[0]), .DPRA1(ADDRB[1]), .DPRA2(ADDRB[2]), 
  .DPRA3(ADDRB[3]), .DPRA4(ADDRB[4]), .DPRA5(ADDRB[5]), .DPO(pre_doutb_1[12]));
// Memory array element: COL: 1, ROW: 13
//  Initialization of Dual Port LUT ram now done through explicit
// parameter setting.
RAM64X1D#(.INIT(64'h0000000000000000))RAM64X1D_instance_3053(.WCLK(CLK), .WE(wea_1), 
  .A0(ADDRA[0]), .A1(ADDRA[1]), .A2(ADDRA[2]), .A3(ADDRA[3]), .A4(ADDRA[4]), .A5(ADDRA[5]), 
  .D(DINA[13]), .SPO(pre_douta_1[13]), .DPRA0(ADDRB[0]), .DPRA1(ADDRB[1]), .DPRA2(ADDRB[2]), 
  .DPRA3(ADDRB[3]), .DPRA4(ADDRB[4]), .DPRA5(ADDRB[5]), .DPO(pre_doutb_1[13]));
// Memory array element: COL: 1, ROW: 14
//  Initialization of Dual Port LUT ram now done through explicit
// parameter setting.
RAM64X1D#(.INIT(64'h0000000000000000))RAM64X1D_instance_3054(.WCLK(CLK), .WE(wea_1), 
  .A0(ADDRA[0]), .A1(ADDRA[1]), .A2(ADDRA[2]), .A3(ADDRA[3]), .A4(ADDRA[4]), .A5(ADDRA[5]), 
  .D(DINA[14]), .SPO(pre_douta_1[14]), .DPRA0(ADDRB[0]), .DPRA1(ADDRB[1]), .DPRA2(ADDRB[2]), 
  .DPRA3(ADDRB[3]), .DPRA4(ADDRB[4]), .DPRA5(ADDRB[5]), .DPO(pre_doutb_1[14]));
// Memory array element: COL: 1, ROW: 15
//  Initialization of Dual Port LUT ram now done through explicit
// parameter setting.
RAM64X1D#(.INIT(64'h0000000000000000))RAM64X1D_instance_3055(.WCLK(CLK), .WE(wea_1), 
  .A0(ADDRA[0]), .A1(ADDRA[1]), .A2(ADDRA[2]), .A3(ADDRA[3]), .A4(ADDRA[4]), .A5(ADDRA[5]), 
  .D(DINA[15]), .SPO(pre_douta_1[15]), .DPRA0(ADDRB[0]), .DPRA1(ADDRB[1]), .DPRA2(ADDRB[2]), 
  .DPRA3(ADDRB[3]), .DPRA4(ADDRB[4]), .DPRA5(ADDRB[5]), .DPO(pre_doutb_1[15]));
// Memory array element: COL: 1, ROW: 16
//  Initialization of Dual Port LUT ram now done through explicit
// parameter setting.
RAM64X1D#(.INIT(64'h0000000000000000))RAM64X1D_instance_3056(.WCLK(CLK), .WE(wea_1), 
  .A0(ADDRA[0]), .A1(ADDRA[1]), .A2(ADDRA[2]), .A3(ADDRA[3]), .A4(ADDRA[4]), .A5(ADDRA[5]), 
  .D(DINA[16]), .SPO(pre_douta_1[16]), .DPRA0(ADDRB[0]), .DPRA1(ADDRB[1]), .DPRA2(ADDRB[2]), 
  .DPRA3(ADDRB[3]), .DPRA4(ADDRB[4]), .DPRA5(ADDRB[5]), .DPO(pre_doutb_1[16]));
// Memory array element: COL: 1, ROW: 17
//  Initialization of Dual Port LUT ram now done through explicit
// parameter setting.
RAM64X1D#(.INIT(64'h0000000000000000))RAM64X1D_instance_3057(.WCLK(CLK), .WE(wea_1), 
  .A0(ADDRA[0]), .A1(ADDRA[1]), .A2(ADDRA[2]), .A3(ADDRA[3]), .A4(ADDRA[4]), .A5(ADDRA[5]), 
  .D(DINA[17]), .SPO(pre_douta_1[17]), .DPRA0(ADDRB[0]), .DPRA1(ADDRB[1]), .DPRA2(ADDRB[2]), 
  .DPRA3(ADDRB[3]), .DPRA4(ADDRB[4]), .DPRA5(ADDRB[5]), .DPO(pre_doutb_1[17]));
// Memory array element: COL: 1, ROW: 18
//  Initialization of Dual Port LUT ram now done through explicit
// parameter setting.
RAM64X1D#(.INIT(64'h0000000000000000))RAM64X1D_instance_3058(.WCLK(CLK), .WE(wea_1), 
  .A0(ADDRA[0]), .A1(ADDRA[1]), .A2(ADDRA[2]), .A3(ADDRA[3]), .A4(ADDRA[4]), .A5(ADDRA[5]), 
  .D(DINA[18]), .SPO(pre_douta_1[18]), .DPRA0(ADDRB[0]), .DPRA1(ADDRB[1]), .DPRA2(ADDRB[2]), 
  .DPRA3(ADDRB[3]), .DPRA4(ADDRB[4]), .DPRA5(ADDRB[5]), .DPO(pre_doutb_1[18]));
// Memory array element: COL: 1, ROW: 19
//  Initialization of Dual Port LUT ram now done through explicit
// parameter setting.
RAM64X1D#(.INIT(64'h0000000000000000))RAM64X1D_instance_3059(.WCLK(CLK), .WE(wea_1), 
  .A0(ADDRA[0]), .A1(ADDRA[1]), .A2(ADDRA[2]), .A3(ADDRA[3]), .A4(ADDRA[4]), .A5(ADDRA[5]), 
  .D(DINA[19]), .SPO(pre_douta_1[19]), .DPRA0(ADDRB[0]), .DPRA1(ADDRB[1]), .DPRA2(ADDRB[2]), 
  .DPRA3(ADDRB[3]), .DPRA4(ADDRB[4]), .DPRA5(ADDRB[5]), .DPO(pre_doutb_1[19]));
// Memory array element: COL: 1, ROW: 20
//  Initialization of Dual Port LUT ram now done through explicit
// parameter setting.
RAM64X1D#(.INIT(64'h0000000000000000))RAM64X1D_instance_3060(.WCLK(CLK), .WE(wea_1), 
  .A0(ADDRA[0]), .A1(ADDRA[1]), .A2(ADDRA[2]), .A3(ADDRA[3]), .A4(ADDRA[4]), .A5(ADDRA[5]), 
  .D(DINA[20]), .SPO(pre_douta_1[20]), .DPRA0(ADDRB[0]), .DPRA1(ADDRB[1]), .DPRA2(ADDRB[2]), 
  .DPRA3(ADDRB[3]), .DPRA4(ADDRB[4]), .DPRA5(ADDRB[5]), .DPO(pre_doutb_1[20]));
// Memory array element: COL: 1, ROW: 21
//  Initialization of Dual Port LUT ram now done through explicit
// parameter setting.
RAM64X1D#(.INIT(64'h0000000000000000))RAM64X1D_instance_3061(.WCLK(CLK), .WE(wea_1), 
  .A0(ADDRA[0]), .A1(ADDRA[1]), .A2(ADDRA[2]), .A3(ADDRA[3]), .A4(ADDRA[4]), .A5(ADDRA[5]), 
  .D(DINA[21]), .SPO(pre_douta_1[21]), .DPRA0(ADDRB[0]), .DPRA1(ADDRB[1]), .DPRA2(ADDRB[2]), 
  .DPRA3(ADDRB[3]), .DPRA4(ADDRB[4]), .DPRA5(ADDRB[5]), .DPO(pre_doutb_1[21]));
// Memory array element: COL: 1, ROW: 22
//  Initialization of Dual Port LUT ram now done through explicit
// parameter setting.
RAM64X1D#(.INIT(64'h0000000000000000))RAM64X1D_instance_3062(.WCLK(CLK), .WE(wea_1), 
  .A0(ADDRA[0]), .A1(ADDRA[1]), .A2(ADDRA[2]), .A3(ADDRA[3]), .A4(ADDRA[4]), .A5(ADDRA[5]), 
  .D(DINA[22]), .SPO(pre_douta_1[22]), .DPRA0(ADDRB[0]), .DPRA1(ADDRB[1]), .DPRA2(ADDRB[2]), 
  .DPRA3(ADDRB[3]), .DPRA4(ADDRB[4]), .DPRA5(ADDRB[5]), .DPO(pre_doutb_1[22]));
// Memory array element: COL: 1, ROW: 23
//  Initialization of Dual Port LUT ram now done through explicit
// parameter setting.
RAM64X1D#(.INIT(64'h0000000000000000))RAM64X1D_instance_3063(.WCLK(CLK), .WE(wea_1), 
  .A0(ADDRA[0]), .A1(ADDRA[1]), .A2(ADDRA[2]), .A3(ADDRA[3]), .A4(ADDRA[4]), .A5(ADDRA[5]), 
  .D(DINA[23]), .SPO(pre_douta_1[23]), .DPRA0(ADDRB[0]), .DPRA1(ADDRB[1]), .DPRA2(ADDRB[2]), 
  .DPRA3(ADDRB[3]), .DPRA4(ADDRB[4]), .DPRA5(ADDRB[5]), .DPO(pre_doutb_1[23]));
// Memory array element: COL: 1, ROW: 24
//  Initialization of Dual Port LUT ram now done through explicit
// parameter setting.
RAM64X1D#(.INIT(64'h0000000000000000))RAM64X1D_instance_3064(.WCLK(CLK), .WE(wea_1), 
  .A0(ADDRA[0]), .A1(ADDRA[1]), .A2(ADDRA[2]), .A3(ADDRA[3]), .A4(ADDRA[4]), .A5(ADDRA[5]), 
  .D(DINA[24]), .SPO(pre_douta_1[24]), .DPRA0(ADDRB[0]), .DPRA1(ADDRB[1]), .DPRA2(ADDRB[2]), 
  .DPRA3(ADDRB[3]), .DPRA4(ADDRB[4]), .DPRA5(ADDRB[5]), .DPO(pre_doutb_1[24]));
// Memory array element: COL: 1, ROW: 25
//  Initialization of Dual Port LUT ram now done through explicit
// parameter setting.
RAM64X1D#(.INIT(64'h0000000000000000))RAM64X1D_instance_3065(.WCLK(CLK), .WE(wea_1), 
  .A0(ADDRA[0]), .A1(ADDRA[1]), .A2(ADDRA[2]), .A3(ADDRA[3]), .A4(ADDRA[4]), .A5(ADDRA[5]), 
  .D(DINA[25]), .SPO(pre_douta_1[25]), .DPRA0(ADDRB[0]), .DPRA1(ADDRB[1]), .DPRA2(ADDRB[2]), 
  .DPRA3(ADDRB[3]), .DPRA4(ADDRB[4]), .DPRA5(ADDRB[5]), .DPO(pre_doutb_1[25]));
// Memory array element: COL: 1, ROW: 26
//  Initialization of Dual Port LUT ram now done through explicit
// parameter setting.
RAM64X1D#(.INIT(64'h0000000000000000))RAM64X1D_instance_3066(.WCLK(CLK), .WE(wea_1), 
  .A0(ADDRA[0]), .A1(ADDRA[1]), .A2(ADDRA[2]), .A3(ADDRA[3]), .A4(ADDRA[4]), .A5(ADDRA[5]), 
  .D(DINA[26]), .SPO(pre_douta_1[26]), .DPRA0(ADDRB[0]), .DPRA1(ADDRB[1]), .DPRA2(ADDRB[2]), 
  .DPRA3(ADDRB[3]), .DPRA4(ADDRB[4]), .DPRA5(ADDRB[5]), .DPO(pre_doutb_1[26]));
// Memory array element: COL: 1, ROW: 27
//  Initialization of Dual Port LUT ram now done through explicit
// parameter setting.
RAM64X1D#(.INIT(64'h0000000000000000))RAM64X1D_instance_3067(.WCLK(CLK), .WE(wea_1), 
  .A0(ADDRA[0]), .A1(ADDRA[1]), .A2(ADDRA[2]), .A3(ADDRA[3]), .A4(ADDRA[4]), .A5(ADDRA[5]), 
  .D(DINA[27]), .SPO(pre_douta_1[27]), .DPRA0(ADDRB[0]), .DPRA1(ADDRB[1]), .DPRA2(ADDRB[2]), 
  .DPRA3(ADDRB[3]), .DPRA4(ADDRB[4]), .DPRA5(ADDRB[5]), .DPO(pre_doutb_1[27]));
// Memory array element: COL: 1, ROW: 28
//  Initialization of Dual Port LUT ram now done through explicit
// parameter setting.
RAM64X1D#(.INIT(64'h0000000000000000))RAM64X1D_instance_3068(.WCLK(CLK), .WE(wea_1), 
  .A0(ADDRA[0]), .A1(ADDRA[1]), .A2(ADDRA[2]), .A3(ADDRA[3]), .A4(ADDRA[4]), .A5(ADDRA[5]), 
  .D(DINA[28]), .SPO(pre_douta_1[28]), .DPRA0(ADDRB[0]), .DPRA1(ADDRB[1]), .DPRA2(ADDRB[2]), 
  .DPRA3(ADDRB[3]), .DPRA4(ADDRB[4]), .DPRA5(ADDRB[5]), .DPO(pre_doutb_1[28]));
// Memory array element: COL: 1, ROW: 29
//  Initialization of Dual Port LUT ram now done through explicit
// parameter setting.
RAM64X1D#(.INIT(64'h0000000000000000))RAM64X1D_instance_3069(.WCLK(CLK), .WE(wea_1), 
  .A0(ADDRA[0]), .A1(ADDRA[1]), .A2(ADDRA[2]), .A3(ADDRA[3]), .A4(ADDRA[4]), .A5(ADDRA[5]), 
  .D(DINA[29]), .SPO(pre_douta_1[29]), .DPRA0(ADDRB[0]), .DPRA1(ADDRB[1]), .DPRA2(ADDRB[2]), 
  .DPRA3(ADDRB[3]), .DPRA4(ADDRB[4]), .DPRA5(ADDRB[5]), .DPO(pre_doutb_1[29]));
// Memory array element: COL: 1, ROW: 30
//  Initialization of Dual Port LUT ram now done through explicit
// parameter setting.
RAM64X1D#(.INIT(64'h0000000000000000))RAM64X1D_instance_3070(.WCLK(CLK), .WE(wea_1), 
  .A0(ADDRA[0]), .A1(ADDRA[1]), .A2(ADDRA[2]), .A3(ADDRA[3]), .A4(ADDRA[4]), .A5(ADDRA[5]), 
  .D(DINA[30]), .SPO(pre_douta_1[30]), .DPRA0(ADDRB[0]), .DPRA1(ADDRB[1]), .DPRA2(ADDRB[2]), 
  .DPRA3(ADDRB[3]), .DPRA4(ADDRB[4]), .DPRA5(ADDRB[5]), .DPO(pre_doutb_1[30]));
// Memory array element: COL: 1, ROW: 31
//  Initialization of Dual Port LUT ram now done through explicit
// parameter setting.
RAM64X1D#(.INIT(64'h0000000000000000))RAM64X1D_instance_3071(.WCLK(CLK), .WE(wea_1), 
  .A0(ADDRA[0]), .A1(ADDRA[1]), .A2(ADDRA[2]), .A3(ADDRA[3]), .A4(ADDRA[4]), .A5(ADDRA[5]), 
  .D(DINA[31]), .SPO(pre_douta_1[31]), .DPRA0(ADDRB[0]), .DPRA1(ADDRB[1]), .DPRA2(ADDRB[2]), 
  .DPRA3(ADDRB[3]), .DPRA4(ADDRB[4]), .DPRA5(ADDRB[5]), .DPO(pre_doutb_1[31]));
endmodule



module medianRow15_structuralmemory_7b577e70_(CLK_u93, bus_2f085f50_, bus_0ed07fa1_, bus_410f28b2_, bus_105841d0_, bus_4bdcc002_, bus_0aef563f_, bus_247ac50a_, bus_799a33cb_, bus_7e1b4a62_, bus_6a2aae02_, bus_0fb836ff_, bus_2c0f475e_, bus_22d42b7f_);
input		CLK_u93;
input		bus_2f085f50_;
input	[31:0]	bus_0ed07fa1_;
input	[2:0]	bus_410f28b2_;
input		bus_105841d0_;
input	[31:0]	bus_4bdcc002_;
input	[2:0]	bus_0aef563f_;
input		bus_247ac50a_;
input		bus_799a33cb_;
input	[31:0]	bus_7e1b4a62_;
output	[31:0]	bus_6a2aae02_;
output		bus_0fb836ff_;
output	[31:0]	bus_2c0f475e_;
output		bus_22d42b7f_;
wire		or_5e11b8d9_u0;
wire		and_5145a1af_u0;
reg		logicalMem_1530ea9a_we_delay0_u0=1'h0;
wire		or_33c2c872_u0;
wire		not_22c299b3_u0;
wire	[31:0]	bus_7b000f84_;
wire	[31:0]	bus_1f2f0e09_;
assign or_5e11b8d9_u0=and_5145a1af_u0|logicalMem_1530ea9a_we_delay0_u0;
assign and_5145a1af_u0=bus_247ac50a_&not_22c299b3_u0;
always @(posedge CLK_u93 or posedge bus_2f085f50_)
begin
if (bus_2f085f50_)
logicalMem_1530ea9a_we_delay0_u0<=1'h0;
else logicalMem_1530ea9a_we_delay0_u0<=bus_799a33cb_;
end
assign or_33c2c872_u0=bus_247ac50a_|bus_799a33cb_;
assign not_22c299b3_u0=~bus_799a33cb_;
medianRow15_forge_memory_101x32_147 medianRow15_forge_memory_101x32_147_instance0(.CLK(CLK_u93), 
  .ENA(or_33c2c872_u0), .WEA(bus_799a33cb_), .DINA(bus_7e1b4a62_), .ADDRA(bus_4bdcc002_), 
  .DOUTA(bus_1f2f0e09_), .DONEA(), .ENB(bus_105841d0_), .ADDRB(bus_0ed07fa1_), .DOUTB(bus_7b000f84_), 
  .DONEB());
assign bus_6a2aae02_=bus_7b000f84_;
assign bus_0fb836ff_=bus_105841d0_;
assign bus_2c0f475e_=bus_1f2f0e09_;
assign bus_22d42b7f_=or_5e11b8d9_u0;
endmodule



module medianRow15_simplememoryreferee_7469aa46_(bus_4dac9f68_, bus_6bc72ebe_, bus_6a6b593e_, bus_75b3baec_, bus_4f5eb8ab_, bus_794c18bd_, bus_7c3e0c8f_, bus_04d08500_, bus_2f818ae3_, bus_515ce90a_, bus_69ce42d9_, bus_60e38b66_, bus_4c57f205_, bus_3b5a1da0_, bus_472c4243_, bus_44fc4bd9_, bus_2aee950c_, bus_2446d1a7_, bus_398ab5ca_, bus_43c94e3c_, bus_13290e56_);
input		bus_4dac9f68_;
input		bus_6bc72ebe_;
input		bus_6a6b593e_;
input	[31:0]	bus_75b3baec_;
input		bus_4f5eb8ab_;
input	[31:0]	bus_794c18bd_;
input	[31:0]	bus_7c3e0c8f_;
input	[2:0]	bus_04d08500_;
input		bus_2f818ae3_;
input		bus_515ce90a_;
input	[31:0]	bus_69ce42d9_;
input	[31:0]	bus_60e38b66_;
input	[2:0]	bus_4c57f205_;
output	[31:0]	bus_3b5a1da0_;
output	[31:0]	bus_472c4243_;
output		bus_44fc4bd9_;
output		bus_2aee950c_;
output	[2:0]	bus_2446d1a7_;
output		bus_398ab5ca_;
output	[31:0]	bus_43c94e3c_;
output		bus_13290e56_;
wire		and_0ce55dc5_u0;
wire	[31:0]	mux_1d5ec93d_u0;
wire		not_60c13f05_u0;
wire		or_25ec8d4a_u0;
wire		not_4a13c65b_u0;
wire		or_7527cbec_u0;
wire		or_7326abcb_u0;
wire		and_39bff630_u0;
wire	[31:0]	mux_6f4e789e_u0;
wire		or_508a591f_u0;
reg		done_qual_u222=1'h0;
reg		done_qual_u223_u0=1'h0;
wire		or_74b81d84_u0;
assign and_0ce55dc5_u0=or_7527cbec_u0&bus_6a6b593e_;
assign mux_1d5ec93d_u0=(bus_4f5eb8ab_)?{24'b0, bus_794c18bd_[7:0]}:bus_69ce42d9_;
assign not_60c13f05_u0=~bus_6a6b593e_;
assign or_25ec8d4a_u0=bus_2f818ae3_|bus_515ce90a_;
assign bus_3b5a1da0_=mux_1d5ec93d_u0;
assign bus_472c4243_=mux_6f4e789e_u0;
assign bus_44fc4bd9_=or_7326abcb_u0;
assign bus_2aee950c_=or_74b81d84_u0;
assign bus_2446d1a7_=3'h1;
assign bus_398ab5ca_=and_0ce55dc5_u0;
assign bus_43c94e3c_=bus_75b3baec_;
assign bus_13290e56_=and_39bff630_u0;
assign not_4a13c65b_u0=~bus_6a6b593e_;
assign or_7527cbec_u0=bus_4f5eb8ab_|done_qual_u222;
assign or_7326abcb_u0=bus_4f5eb8ab_|bus_515ce90a_;
assign and_39bff630_u0=or_508a591f_u0&bus_6a6b593e_;
assign mux_6f4e789e_u0=(bus_4f5eb8ab_)?bus_7c3e0c8f_:bus_60e38b66_;
assign or_508a591f_u0=or_25ec8d4a_u0|done_qual_u223_u0;
always @(posedge bus_4dac9f68_)
begin
if (bus_6bc72ebe_)
done_qual_u222<=1'h0;
else done_qual_u222<=bus_4f5eb8ab_;
end
always @(posedge bus_4dac9f68_)
begin
if (bus_6bc72ebe_)
done_qual_u223_u0<=1'h0;
else done_qual_u223_u0<=or_25ec8d4a_u0;
end
assign or_74b81d84_u0=bus_4f5eb8ab_|or_25ec8d4a_u0;
endmodule



module medianRow15_globalreset_physical_736798d6_(bus_20d7caed_, bus_6860427e_, bus_1e11ad99_);
input		bus_20d7caed_;
input		bus_6860427e_;
output		bus_1e11ad99_;
wire		or_6ba112c1_u0;
wire		and_637d4eef_u0;
reg		final_u59=1'h1;
reg		glitch_u59=1'h0;
reg		sample_u59=1'h0;
reg		cross_u59=1'h0;
wire		not_652880a6_u0;
assign or_6ba112c1_u0=bus_6860427e_|final_u59;
assign and_637d4eef_u0=cross_u59&glitch_u59;
always @(posedge bus_20d7caed_)
begin
final_u59<=not_652880a6_u0;
end
always @(posedge bus_20d7caed_)
begin
glitch_u59<=cross_u59;
end
always @(posedge bus_20d7caed_)
begin
sample_u59<=1'h1;
end
assign bus_1e11ad99_=or_6ba112c1_u0;
always @(posedge bus_20d7caed_)
begin
cross_u59<=sample_u59;
end
assign not_652880a6_u0=~and_637d4eef_u0;
endmodule



module medianRow15_Kicker_59(CLK, RESET, bus_70c54dad_);
input		CLK;
input		RESET;
output		bus_70c54dad_;
wire		bus_05fdfb99_;
wire		bus_0bf2f336_;
reg		kicker_1=1'h0;
reg		kicker_res=1'h0;
wire		bus_79447f9b_;
wire		bus_1500ebc3_;
reg		kicker_2=1'h0;
assign bus_05fdfb99_=~RESET;
assign bus_0bf2f336_=bus_05fdfb99_&kicker_1;
assign bus_70c54dad_=kicker_res;
always @(posedge CLK)
begin
kicker_1<=bus_05fdfb99_;
end
always @(posedge CLK)
begin
kicker_res<=bus_1500ebc3_;
end
assign bus_79447f9b_=~kicker_2;
assign bus_1500ebc3_=kicker_1&bus_05fdfb99_&bus_79447f9b_;
always @(posedge CLK)
begin
kicker_2<=bus_0bf2f336_;
end
endmodule



module medianRow15_receive(CLK, RESET, GO, port_72bf504d_, port_4ebb2248_, port_44cf90eb_, RESULT, RESULT_u2259, RESULT_u2260, RESULT_u2261, RESULT_u2262, RESULT_u2263, RESULT_u2264, DONE);
input		CLK;
input		RESET;
input		GO;
input	[31:0]	port_72bf504d_;
input		port_4ebb2248_;
input	[7:0]	port_44cf90eb_;
output		RESULT;
output	[31:0]	RESULT_u2259;
output		RESULT_u2260;
output	[31:0]	RESULT_u2261;
output	[31:0]	RESULT_u2262;
output	[2:0]	RESULT_u2263;
output		RESULT_u2264;
output		DONE;
wire		simplePinWrite;
wire	[31:0]	add;
wire		and_u3842_u0;
reg		reg_7a415c14_u0=1'h0;
wire		or_u1471_u0;
wire	[31:0]	add_u693;
reg		reg_28a8ab8b_u0=1'h0;
assign simplePinWrite=GO&{1{GO}};
assign add=port_72bf504d_+32'h0;
assign and_u3842_u0=reg_7a415c14_u0&port_4ebb2248_;
always @(posedge CLK or posedge GO or posedge or_u1471_u0)
begin
if (or_u1471_u0)
reg_7a415c14_u0<=1'h0;
else if (GO)
reg_7a415c14_u0<=1'h1;
else reg_7a415c14_u0<=reg_7a415c14_u0;
end
assign or_u1471_u0=and_u3842_u0|RESET;
assign add_u693=port_72bf504d_+32'h1;
always @(posedge CLK or posedge RESET)
begin
if (RESET)
reg_28a8ab8b_u0<=1'h0;
else reg_28a8ab8b_u0<=GO;
end
assign RESULT=GO;
assign RESULT_u2259=add_u693;
assign RESULT_u2260=GO;
assign RESULT_u2261=add;
assign RESULT_u2262={24'b0, port_44cf90eb_};
assign RESULT_u2263=3'h1;
assign RESULT_u2264=simplePinWrite;
assign DONE=reg_28a8ab8b_u0;
endmodule



module medianRow15_scheduler(CLK, RESET, GO, port_06e0c99e_, port_15d5fd88_, port_15680b7f_, port_5e7b9726_, port_4c6274dc_, port_780b87c2_, RESULT, RESULT_u2265, RESULT_u2266, RESULT_u2267, DONE);
input		CLK;
input		RESET;
input		GO;
input		port_06e0c99e_;
input	[31:0]	port_15d5fd88_;
input		port_15680b7f_;
input		port_5e7b9726_;
input		port_4c6274dc_;
input		port_780b87c2_;
output		RESULT;
output		RESULT_u2265;
output		RESULT_u2266;
output		RESULT_u2267;
output		DONE;
wire		and_u3843_u0;
wire signed	[31:0]	lessThan_b_signed;
wire		lessThan;
wire signed	[31:0]	lessThan_a_signed;
wire signed	[31:0]	equals_a_signed;
wire signed	[31:0]	equals_b_signed;
wire		equals;
wire		equals_u236;
wire signed	[31:0]	equals_u236_b_signed;
wire signed	[31:0]	equals_u236_a_signed;
wire		and_u3844_u0;
wire		and_u3845_u0;
wire		not_u894_u0;
wire		andOp;
wire		not_u895_u0;
wire		and_u3846_u0;
wire		and_u3847_u0;
wire		simplePinWrite;
wire		and_u3848_u0;
wire		and_u3849_u0;
wire		equals_u237;
wire signed	[31:0]	equals_u237_b_signed;
wire signed	[31:0]	equals_u237_a_signed;
wire		and_u3850_u0;
wire		and_u3851_u0;
wire		not_u896_u0;
wire		andOp_u81;
wire		and_u3852_u0;
wire		not_u897_u0;
wire		and_u3853_u0;
wire		simplePinWrite_u628;
wire		not_u898_u0;
wire		and_u3854_u0;
wire		and_u3855_u0;
wire		not_u899_u0;
wire		and_u3856_u0;
wire		and_u3857_u0;
wire		simplePinWrite_u629;
wire		or_u1472_u0;
reg		reg_011018b4_u0=1'h0;
wire		and_u3858_u0;
wire		and_u3859_u0;
wire		and_u3860_u0;
wire		or_u1473_u0;
reg		reg_1e6eadb5_u0=1'h0;
wire		and_u3861_u0;
wire		and_u3862_u0;
wire		mux_u1955;
wire		or_u1474_u0;
wire		and_u3863_u0;
reg		and_delayed_u393=1'h0;
wire		or_u1475_u0;
wire		or_u1476_u0;
wire		and_u3864_u0;
wire		and_u3865_u0;
reg		and_delayed_u394_u0=1'h0;
wire		receive_go_merge;
wire		mux_u1956_u0;
wire		or_u1477_u0;
reg		loopControl_u99=1'h0;
wire		or_u1478_u0;
reg		syncEnable_u1466=1'h0;
reg		reg_6917751d_u0=1'h0;
wire		mux_u1957_u0;
wire		or_u1479_u0;
reg		reg_636feedc_u0=1'h0;
assign and_u3843_u0=or_u1478_u0&or_u1478_u0;
assign lessThan_a_signed=port_15d5fd88_;
assign lessThan_b_signed=32'h65;
assign lessThan=lessThan_a_signed<lessThan_b_signed;
assign equals_a_signed=port_15d5fd88_;
assign equals_b_signed=32'h65;
assign equals=equals_a_signed==equals_b_signed;
assign equals_u236_a_signed={31'b0, port_06e0c99e_};
assign equals_u236_b_signed=32'h0;
assign equals_u236=equals_u236_a_signed==equals_u236_b_signed;
assign and_u3844_u0=and_u3843_u0&equals_u236;
assign and_u3845_u0=and_u3843_u0&not_u894_u0;
assign not_u894_u0=~equals_u236;
assign andOp=lessThan&port_5e7b9726_;
assign not_u895_u0=~andOp;
assign and_u3846_u0=and_u3849_u0&andOp;
assign and_u3847_u0=and_u3849_u0&not_u895_u0;
assign simplePinWrite=and_u3848_u0&{1{and_u3848_u0}};
assign and_u3848_u0=and_u3846_u0&and_u3849_u0;
assign and_u3849_u0=and_u3844_u0&and_u3843_u0;
assign equals_u237_a_signed={31'b0, port_06e0c99e_};
assign equals_u237_b_signed=32'h1;
assign equals_u237=equals_u237_a_signed==equals_u237_b_signed;
assign and_u3850_u0=and_u3843_u0&equals_u237;
assign and_u3851_u0=and_u3843_u0&not_u896_u0;
assign not_u896_u0=~equals_u237;
assign andOp_u81=lessThan&port_5e7b9726_;
assign and_u3852_u0=and_u3864_u0&not_u897_u0;
assign not_u897_u0=~andOp_u81;
assign and_u3853_u0=and_u3864_u0&andOp_u81;
assign simplePinWrite_u628=and_u3863_u0&{1{and_u3863_u0}};
assign not_u898_u0=~equals;
assign and_u3854_u0=and_u3862_u0&equals;
assign and_u3855_u0=and_u3862_u0&not_u898_u0;
assign not_u899_u0=~port_780b87c2_;
assign and_u3856_u0=and_u3860_u0&port_780b87c2_;
assign and_u3857_u0=and_u3860_u0&not_u899_u0;
assign simplePinWrite_u629=and_u3859_u0&{1{and_u3859_u0}};
assign or_u1472_u0=reg_011018b4_u0|port_15680b7f_;
always @(posedge CLK or posedge RESET)
begin
if (RESET)
reg_011018b4_u0<=1'h0;
else reg_011018b4_u0<=and_u3858_u0;
end
assign and_u3858_u0=and_u3857_u0&and_u3860_u0;
assign and_u3859_u0=and_u3856_u0&and_u3860_u0;
assign and_u3860_u0=and_u3854_u0&and_u3862_u0;
assign or_u1473_u0=or_u1472_u0|reg_1e6eadb5_u0;
always @(posedge CLK or posedge RESET)
begin
if (RESET)
reg_1e6eadb5_u0<=1'h0;
else reg_1e6eadb5_u0<=and_u3861_u0;
end
assign and_u3861_u0=and_u3855_u0&and_u3862_u0;
assign and_u3862_u0=and_u3852_u0&and_u3864_u0;
assign mux_u1955=(and_u3863_u0)?1'h1:1'h0;
assign or_u1474_u0=and_u3863_u0|and_u3859_u0;
assign and_u3863_u0=and_u3853_u0&and_u3864_u0;
always @(posedge CLK or posedge RESET)
begin
if (RESET)
and_delayed_u393<=1'h0;
else and_delayed_u393<=and_u3863_u0;
end
assign or_u1475_u0=and_delayed_u393|or_u1473_u0;
assign or_u1476_u0=or_u1475_u0|and_delayed_u394_u0;
assign and_u3864_u0=and_u3850_u0&and_u3843_u0;
assign and_u3865_u0=and_u3851_u0&and_u3843_u0;
always @(posedge CLK or posedge RESET)
begin
if (RESET)
and_delayed_u394_u0<=1'h0;
else and_delayed_u394_u0<=and_u3865_u0;
end
assign receive_go_merge=simplePinWrite|simplePinWrite_u628;
assign mux_u1956_u0=(and_u3848_u0)?1'h1:mux_u1955;
assign or_u1477_u0=and_u3848_u0|or_u1474_u0;
always @(posedge CLK or posedge syncEnable_u1466)
begin
if (syncEnable_u1466)
loopControl_u99<=1'h0;
else loopControl_u99<=or_u1476_u0;
end
assign or_u1478_u0=reg_6917751d_u0|loopControl_u99;
always @(posedge CLK)
begin
if (reg_6917751d_u0)
syncEnable_u1466<=RESET;
end
always @(posedge CLK or posedge RESET)
begin
if (RESET)
reg_6917751d_u0<=1'h0;
else reg_6917751d_u0<=reg_636feedc_u0;
end
assign mux_u1957_u0=(GO)?1'h0:mux_u1956_u0;
assign or_u1479_u0=GO|or_u1477_u0;
always @(posedge CLK or posedge RESET)
begin
if (RESET)
reg_636feedc_u0<=1'h0;
else reg_636feedc_u0<=GO;
end
assign RESULT=or_u1479_u0;
assign RESULT_u2265=mux_u1957_u0;
assign RESULT_u2266=simplePinWrite_u629;
assign RESULT_u2267=receive_go_merge;
assign DONE=1'h0;
endmodule



module medianRow15_endianswapper_09ac08c0_(endianswapper_09ac08c0_in, endianswapper_09ac08c0_out);
input		endianswapper_09ac08c0_in;
output		endianswapper_09ac08c0_out;
assign endianswapper_09ac08c0_out=endianswapper_09ac08c0_in;
endmodule



module medianRow15_endianswapper_060a1634_(endianswapper_060a1634_in, endianswapper_060a1634_out);
input		endianswapper_060a1634_in;
output		endianswapper_060a1634_out;
assign endianswapper_060a1634_out=endianswapper_060a1634_in;
endmodule



module medianRow15_stateVar_fsmState_medianRow15(bus_1071d90d_, bus_785d8595_, bus_06145738_, bus_0c5600e8_, bus_087f002b_);
input		bus_1071d90d_;
input		bus_785d8595_;
input		bus_06145738_;
input		bus_0c5600e8_;
output		bus_087f002b_;
reg		stateVar_fsmState_medianRow15_u2=1'h0;
wire		endianswapper_09ac08c0_out;
wire		endianswapper_060a1634_out;
always @(posedge bus_1071d90d_ or posedge bus_785d8595_)
begin
if (bus_785d8595_)
stateVar_fsmState_medianRow15_u2<=1'h0;
else if (bus_06145738_)
stateVar_fsmState_medianRow15_u2<=endianswapper_09ac08c0_out;
end
assign bus_087f002b_=endianswapper_060a1634_out;
medianRow15_endianswapper_09ac08c0_ medianRow15_endianswapper_09ac08c0__1(.endianswapper_09ac08c0_in(bus_0c5600e8_), 
  .endianswapper_09ac08c0_out(endianswapper_09ac08c0_out));
medianRow15_endianswapper_060a1634_ medianRow15_endianswapper_060a1634__1(.endianswapper_060a1634_in(stateVar_fsmState_medianRow15_u2), 
  .endianswapper_060a1634_out(endianswapper_060a1634_out));
endmodule


