// __  ___ __ ___  _ __   ___  ___ 
// \ \/ / '__/ _ \| '_ \ / _ \/ __|
//  >  <| | | (_) | | | | (_) \__ \
// /_/\_\_|  \___/|_| |_|\___/|___/
// 
// Xronos synthesizer version
// Run date: Fri 23 Feb 2018 14:28:40 +0000
// 

module medianRow16(median_ACK, in1_ACK, median_COUNT, median_DATA, in1_DATA, in1_SEND, median_RDY, in1_COUNT, median_SEND, RESET, CLK);
input		median_ACK;
wire		compute_median_done;
output		in1_ACK;
output	[15:0]	median_COUNT;
wire		receive_done;
wire		compute_median_go;
wire		receive_go;
output	[7:0]	median_DATA;
input	[7:0]	in1_DATA;
input		in1_SEND;
input		median_RDY;
input	[15:0]	in1_COUNT;
output		median_SEND;
input		RESET;
input		CLK;
wire	[31:0]	bus_7d9a83ad_;
wire	[31:0]	bus_50c61c9c_;
wire		bus_643a5b28_;
wire		bus_736b3a9d_;
wire	[2:0]	bus_12022a24_;
wire		bus_1fa39201_;
wire	[31:0]	bus_690f0626_;
wire	[31:0]	bus_39f9a3a4_;
wire	[31:0]	bus_69d2d349_;
wire		bus_4a01031d_;
wire		bus_049c99a8_;
wire		bus_6e13e060_;
wire	[31:0]	compute_median_u647;
wire		medianRow16_compute_median_instance_DONE;
wire	[31:0]	compute_median_u646;
wire	[2:0]	compute_median_u648;
wire		compute_median_u649;
wire		compute_median_u645;
wire	[2:0]	compute_median_u651;
wire	[2:0]	compute_median_u654;
wire	[31:0]	compute_median_u653;
wire	[7:0]	compute_median_u656;
wire		compute_median;
wire		compute_median_u655;
wire		compute_median_u652;
wire	[31:0]	compute_median_u650;
wire	[15:0]	compute_median_u657;
wire	[31:0]	compute_median_u644;
wire		bus_4bb433c0_;
wire	[31:0]	bus_283dd90f_;
wire		bus_455600cd_;
wire		bus_00e0d341_;
wire	[31:0]	bus_7b23c32c_;
wire	[31:0]	bus_6119244d_;
wire		bus_37b13109_;
wire		bus_4049f1ab_;
wire	[31:0]	bus_2bd281cf_;
wire		bus_374dfb27_;
wire	[2:0]	bus_59113781_;
wire		medianRow16_scheduler_instance_DONE;
wire		scheduler_u815;
wire		scheduler_u816;
wire		scheduler;
wire		scheduler_u814;
wire		bus_0de0a591_;
wire		bus_00eda733_;
wire		medianRow16_receive_instance_DONE;
wire		receive;
wire	[2:0]	receive_u280;
wire	[31:0]	receive_u278;
wire	[31:0]	receive_u276;
wire		receive_u277;
wire		receive_u281;
wire	[31:0]	receive_u279;
assign compute_median_done=bus_4bb433c0_;
assign in1_ACK=receive_u281;
assign median_COUNT=compute_median_u657;
assign receive_done=bus_455600cd_;
assign compute_median_go=scheduler_u815;
assign receive_go=scheduler_u816;
assign median_DATA=compute_median_u656;
assign median_SEND=compute_median_u655;
medianRow16_simplememoryreferee_4e5820db_ medianRow16_simplememoryreferee_4e5820db__1(.bus_0b05c28c_(CLK), 
  .bus_1eddb586_(bus_6e13e060_), .bus_04483a89_(bus_4a01031d_), .bus_59f9e02a_(bus_39f9a3a4_), 
  .bus_7e4ac54a_(compute_median_u652), .bus_4c92f742_(compute_median_u653), .bus_47f536db_(3'h1), 
  .bus_50c61c9c_(bus_50c61c9c_), .bus_690f0626_(bus_690f0626_), .bus_1fa39201_(bus_1fa39201_), 
  .bus_643a5b28_(bus_643a5b28_), .bus_12022a24_(bus_12022a24_), .bus_7d9a83ad_(bus_7d9a83ad_), 
  .bus_736b3a9d_(bus_736b3a9d_));
medianRow16_structuralmemory_0d8bbb88_ medianRow16_structuralmemory_0d8bbb88__1(.CLK_u92(CLK), 
  .bus_55ea36cd_(bus_6e13e060_), .bus_16c1ae6c_(bus_690f0626_), .bus_614f8980_(3'h1), 
  .bus_7828d1f0_(bus_643a5b28_), .bus_1b905b34_(bus_6119244d_), .bus_0495362c_(3'h1), 
  .bus_230ec4d9_(bus_4049f1ab_), .bus_58bbdcbb_(bus_37b13109_), .bus_3e2e4f72_(bus_7b23c32c_), 
  .bus_39f9a3a4_(bus_39f9a3a4_), .bus_4a01031d_(bus_4a01031d_), .bus_69d2d349_(bus_69d2d349_), 
  .bus_049c99a8_(bus_049c99a8_));
medianRow16_globalreset_physical_38352fa5_ medianRow16_globalreset_physical_38352fa5__1(.bus_03f935a5_(CLK), 
  .bus_23c2cb23_(RESET), .bus_6e13e060_(bus_6e13e060_));
medianRow16_compute_median medianRow16_compute_median_instance(.CLK(CLK), .RESET(bus_6e13e060_), 
  .GO(compute_median_go), .port_19f9a8b9_(bus_374dfb27_), .port_40b58ee7_(bus_374dfb27_), 
  .port_53a71870_(bus_2bd281cf_), .port_43c5d04c_(bus_736b3a9d_), .port_4aafd5ce_(bus_7d9a83ad_), 
  .DONE(medianRow16_compute_median_instance_DONE), .RESULT(compute_median), .RESULT_u2222(compute_median_u644), 
  .RESULT_u2229(compute_median_u645), .RESULT_u2230(compute_median_u646), .RESULT_u2231(compute_median_u647), 
  .RESULT_u2232(compute_median_u648), .RESULT_u2226(compute_median_u649), .RESULT_u2227(compute_median_u650), 
  .RESULT_u2228(compute_median_u651), .RESULT_u2223(compute_median_u652), .RESULT_u2224(compute_median_u653), 
  .RESULT_u2225(compute_median_u654), .RESULT_u2233(compute_median_u655), .RESULT_u2234(compute_median_u656), 
  .RESULT_u2235(compute_median_u657));
assign bus_4bb433c0_=medianRow16_compute_median_instance_DONE&{1{medianRow16_compute_median_instance_DONE}};
medianRow16_stateVar_i medianRow16_stateVar_i_1(.bus_3d2da586_(CLK), .bus_4b9da0ca_(bus_6e13e060_), 
  .bus_72bc1f9b_(receive), .bus_0450faa8_(receive_u276), .bus_0d4a8339_(compute_median), 
  .bus_40411b48_(32'h0), .bus_283dd90f_(bus_283dd90f_));
assign bus_455600cd_=medianRow16_receive_instance_DONE&{1{medianRow16_receive_instance_DONE}};
medianRow16_simplememoryreferee_3ece07d3_ medianRow16_simplememoryreferee_3ece07d3__1(.bus_4225c911_(CLK), 
  .bus_1911215d_(bus_6e13e060_), .bus_1fc1c0ae_(bus_049c99a8_), .bus_0f77866b_(bus_69d2d349_), 
  .bus_3efbe91a_(receive_u277), .bus_5ed83f9a_({24'b0, receive_u279[7:0]}), .bus_5b46e87f_(receive_u278), 
  .bus_19c5e972_(3'h1), .bus_2dae25e0_(compute_median_u649), .bus_78a258a2_(compute_median_u645), 
  .bus_091116cb_(compute_median_u647), .bus_387f221d_(compute_median_u646), .bus_75c0f5f9_(3'h1), 
  .bus_7b23c32c_(bus_7b23c32c_), .bus_6119244d_(bus_6119244d_), .bus_37b13109_(bus_37b13109_), 
  .bus_4049f1ab_(bus_4049f1ab_), .bus_59113781_(bus_59113781_), .bus_00e0d341_(bus_00e0d341_), 
  .bus_2bd281cf_(bus_2bd281cf_), .bus_374dfb27_(bus_374dfb27_));
medianRow16_scheduler medianRow16_scheduler_instance(.CLK(CLK), .RESET(bus_6e13e060_), 
  .GO(bus_0de0a591_), .port_5d3fe7dc_(bus_00eda733_), .port_180d77fd_(bus_283dd90f_), 
  .port_4c22b9a8_(receive_done), .port_6f85f68f_(compute_median_done), .port_47344a02_(in1_SEND), 
  .port_201e216b_(median_RDY), .DONE(medianRow16_scheduler_instance_DONE), .RESULT(scheduler), 
  .RESULT_u2236(scheduler_u814), .RESULT_u2237(scheduler_u815), .RESULT_u2238(scheduler_u816));
medianRow16_Kicker_58 medianRow16_Kicker_58_1(.CLK(CLK), .RESET(bus_6e13e060_), 
  .bus_0de0a591_(bus_0de0a591_));
medianRow16_stateVar_fsmState_medianRow16 medianRow16_stateVar_fsmState_medianRow16_1(.bus_0eb5fabd_(CLK), 
  .bus_5a9ffcd7_(bus_6e13e060_), .bus_05ae6e75_(scheduler), .bus_01b32561_(scheduler_u814), 
  .bus_00eda733_(bus_00eda733_));
medianRow16_receive medianRow16_receive_instance(.CLK(CLK), .RESET(bus_6e13e060_), 
  .GO(receive_go), .port_7325c58b_(bus_283dd90f_), .port_7d0de144_(bus_00e0d341_), 
  .port_03f079e3_(in1_DATA), .DONE(medianRow16_receive_instance_DONE), .RESULT(receive), 
  .RESULT_u2239(receive_u276), .RESULT_u2240(receive_u277), .RESULT_u2241(receive_u278), 
  .RESULT_u2242(receive_u279), .RESULT_u2243(receive_u280), .RESULT_u2244(receive_u281));
endmodule



module medianRow16_simplememoryreferee_4e5820db_(bus_0b05c28c_, bus_1eddb586_, bus_04483a89_, bus_59f9e02a_, bus_7e4ac54a_, bus_4c92f742_, bus_47f536db_, bus_50c61c9c_, bus_690f0626_, bus_1fa39201_, bus_643a5b28_, bus_12022a24_, bus_7d9a83ad_, bus_736b3a9d_);
input		bus_0b05c28c_;
input		bus_1eddb586_;
input		bus_04483a89_;
input	[31:0]	bus_59f9e02a_;
input		bus_7e4ac54a_;
input	[31:0]	bus_4c92f742_;
input	[2:0]	bus_47f536db_;
output	[31:0]	bus_50c61c9c_;
output	[31:0]	bus_690f0626_;
output		bus_1fa39201_;
output		bus_643a5b28_;
output	[2:0]	bus_12022a24_;
output	[31:0]	bus_7d9a83ad_;
output		bus_736b3a9d_;
assign bus_50c61c9c_=32'h0;
assign bus_690f0626_=bus_4c92f742_;
assign bus_1fa39201_=1'h0;
assign bus_643a5b28_=bus_7e4ac54a_;
assign bus_12022a24_=3'h1;
assign bus_7d9a83ad_=bus_59f9e02a_;
assign bus_736b3a9d_=bus_04483a89_;
endmodule



module medianRow16_forge_memory_101x32_145(CLK, ENA, WEA, DINA, ENB, ADDRA, ADDRB, DOUTA, DOUTB, DONEA, DONEB);
input		CLK;
input		ENA;
input		WEA;
input	[31:0]	DINA;
input		ENB;
input	[31:0]	ADDRA;
input	[31:0]	ADDRB;
output	[31:0]	DOUTA;
output	[31:0]	DOUTB;
output		DONEA;
output		DONEB;
wire		wea_0;
wire	[31:0]	pre_douta_0;
wire	[31:0]	pre_doutb_0;
wire		wea_1;
wire	[31:0]	pre_douta_1;
wire	[31:0]	pre_doutb_1;
reg		wea_done;
reg		rea_done;
reg	[31:0]	mux_outa;
reg	[31:0]	mux_outb;
assign wea_0=WEA&(ADDRA[31:6]==26'h0);
assign wea_1=WEA&(ADDRA[31:6]==26'h1);
always @(posedge CLK)
begin
wea_done<=WEA;
end
assign DONEA=rea_done|wea_done;
always @(posedge CLK)
begin
end
assign DONEB=ENB;
always @(pre_douta_0 or pre_douta_1 or ADDRA)
begin
case (ADDRA[31:6])26'd0:mux_outa=pre_douta_0;
26'd1:mux_outa=pre_douta_1;
default:mux_outa=32'h0;
endcase end
assign DOUTA=mux_outa;
always @(pre_doutb_0 or pre_doutb_1 or ADDRB)
begin
case (ADDRB[31:6])26'd0:mux_outb=pre_doutb_0;
26'd1:mux_outb=pre_doutb_1;
default:mux_outb=32'h0;
endcase end
assign DOUTB=mux_outb;
// Memory array element: COL: 0, ROW: 0
//  Initialization of Dual Port LUT ram now done through explicit
// parameter setting.
RAM64X1D#(.INIT(64'h0000000000000000))RAM64X1D_instance_2944(.WCLK(CLK), .WE(wea_0), 
  .A0(ADDRA[0]), .A1(ADDRA[1]), .A2(ADDRA[2]), .A3(ADDRA[3]), .A4(ADDRA[4]), .A5(ADDRA[5]), 
  .D(DINA[0]), .SPO(pre_douta_0[0]), .DPRA0(ADDRB[0]), .DPRA1(ADDRB[1]), .DPRA2(ADDRB[2]), 
  .DPRA3(ADDRB[3]), .DPRA4(ADDRB[4]), .DPRA5(ADDRB[5]), .DPO(pre_doutb_0[0]));
// Memory array element: COL: 0, ROW: 1
//  Initialization of Dual Port LUT ram now done through explicit
// parameter setting.
RAM64X1D#(.INIT(64'h0000000000000000))RAM64X1D_instance_2945(.WCLK(CLK), .WE(wea_0), 
  .A0(ADDRA[0]), .A1(ADDRA[1]), .A2(ADDRA[2]), .A3(ADDRA[3]), .A4(ADDRA[4]), .A5(ADDRA[5]), 
  .D(DINA[1]), .SPO(pre_douta_0[1]), .DPRA0(ADDRB[0]), .DPRA1(ADDRB[1]), .DPRA2(ADDRB[2]), 
  .DPRA3(ADDRB[3]), .DPRA4(ADDRB[4]), .DPRA5(ADDRB[5]), .DPO(pre_doutb_0[1]));
// Memory array element: COL: 0, ROW: 2
//  Initialization of Dual Port LUT ram now done through explicit
// parameter setting.
RAM64X1D#(.INIT(64'h0000000000000000))RAM64X1D_instance_2946(.WCLK(CLK), .WE(wea_0), 
  .A0(ADDRA[0]), .A1(ADDRA[1]), .A2(ADDRA[2]), .A3(ADDRA[3]), .A4(ADDRA[4]), .A5(ADDRA[5]), 
  .D(DINA[2]), .SPO(pre_douta_0[2]), .DPRA0(ADDRB[0]), .DPRA1(ADDRB[1]), .DPRA2(ADDRB[2]), 
  .DPRA3(ADDRB[3]), .DPRA4(ADDRB[4]), .DPRA5(ADDRB[5]), .DPO(pre_doutb_0[2]));
// Memory array element: COL: 0, ROW: 3
//  Initialization of Dual Port LUT ram now done through explicit
// parameter setting.
RAM64X1D#(.INIT(64'h0000000000000000))RAM64X1D_instance_2947(.WCLK(CLK), .WE(wea_0), 
  .A0(ADDRA[0]), .A1(ADDRA[1]), .A2(ADDRA[2]), .A3(ADDRA[3]), .A4(ADDRA[4]), .A5(ADDRA[5]), 
  .D(DINA[3]), .SPO(pre_douta_0[3]), .DPRA0(ADDRB[0]), .DPRA1(ADDRB[1]), .DPRA2(ADDRB[2]), 
  .DPRA3(ADDRB[3]), .DPRA4(ADDRB[4]), .DPRA5(ADDRB[5]), .DPO(pre_doutb_0[3]));
// Memory array element: COL: 0, ROW: 4
//  Initialization of Dual Port LUT ram now done through explicit
// parameter setting.
RAM64X1D#(.INIT(64'h0000000000000000))RAM64X1D_instance_2948(.WCLK(CLK), .WE(wea_0), 
  .A0(ADDRA[0]), .A1(ADDRA[1]), .A2(ADDRA[2]), .A3(ADDRA[3]), .A4(ADDRA[4]), .A5(ADDRA[5]), 
  .D(DINA[4]), .SPO(pre_douta_0[4]), .DPRA0(ADDRB[0]), .DPRA1(ADDRB[1]), .DPRA2(ADDRB[2]), 
  .DPRA3(ADDRB[3]), .DPRA4(ADDRB[4]), .DPRA5(ADDRB[5]), .DPO(pre_doutb_0[4]));
// Memory array element: COL: 0, ROW: 5
//  Initialization of Dual Port LUT ram now done through explicit
// parameter setting.
RAM64X1D#(.INIT(64'h0000000000000000))RAM64X1D_instance_2949(.WCLK(CLK), .WE(wea_0), 
  .A0(ADDRA[0]), .A1(ADDRA[1]), .A2(ADDRA[2]), .A3(ADDRA[3]), .A4(ADDRA[4]), .A5(ADDRA[5]), 
  .D(DINA[5]), .SPO(pre_douta_0[5]), .DPRA0(ADDRB[0]), .DPRA1(ADDRB[1]), .DPRA2(ADDRB[2]), 
  .DPRA3(ADDRB[3]), .DPRA4(ADDRB[4]), .DPRA5(ADDRB[5]), .DPO(pre_doutb_0[5]));
// Memory array element: COL: 0, ROW: 6
//  Initialization of Dual Port LUT ram now done through explicit
// parameter setting.
RAM64X1D#(.INIT(64'h0000000000000000))RAM64X1D_instance_2950(.WCLK(CLK), .WE(wea_0), 
  .A0(ADDRA[0]), .A1(ADDRA[1]), .A2(ADDRA[2]), .A3(ADDRA[3]), .A4(ADDRA[4]), .A5(ADDRA[5]), 
  .D(DINA[6]), .SPO(pre_douta_0[6]), .DPRA0(ADDRB[0]), .DPRA1(ADDRB[1]), .DPRA2(ADDRB[2]), 
  .DPRA3(ADDRB[3]), .DPRA4(ADDRB[4]), .DPRA5(ADDRB[5]), .DPO(pre_doutb_0[6]));
// Memory array element: COL: 0, ROW: 7
//  Initialization of Dual Port LUT ram now done through explicit
// parameter setting.
RAM64X1D#(.INIT(64'h0000000000000000))RAM64X1D_instance_2951(.WCLK(CLK), .WE(wea_0), 
  .A0(ADDRA[0]), .A1(ADDRA[1]), .A2(ADDRA[2]), .A3(ADDRA[3]), .A4(ADDRA[4]), .A5(ADDRA[5]), 
  .D(DINA[7]), .SPO(pre_douta_0[7]), .DPRA0(ADDRB[0]), .DPRA1(ADDRB[1]), .DPRA2(ADDRB[2]), 
  .DPRA3(ADDRB[3]), .DPRA4(ADDRB[4]), .DPRA5(ADDRB[5]), .DPO(pre_doutb_0[7]));
// Memory array element: COL: 0, ROW: 8
//  Initialization of Dual Port LUT ram now done through explicit
// parameter setting.
RAM64X1D#(.INIT(64'h0000000000000000))RAM64X1D_instance_2952(.WCLK(CLK), .WE(wea_0), 
  .A0(ADDRA[0]), .A1(ADDRA[1]), .A2(ADDRA[2]), .A3(ADDRA[3]), .A4(ADDRA[4]), .A5(ADDRA[5]), 
  .D(DINA[8]), .SPO(pre_douta_0[8]), .DPRA0(ADDRB[0]), .DPRA1(ADDRB[1]), .DPRA2(ADDRB[2]), 
  .DPRA3(ADDRB[3]), .DPRA4(ADDRB[4]), .DPRA5(ADDRB[5]), .DPO(pre_doutb_0[8]));
// Memory array element: COL: 0, ROW: 9
//  Initialization of Dual Port LUT ram now done through explicit
// parameter setting.
RAM64X1D#(.INIT(64'h0000000000000000))RAM64X1D_instance_2953(.WCLK(CLK), .WE(wea_0), 
  .A0(ADDRA[0]), .A1(ADDRA[1]), .A2(ADDRA[2]), .A3(ADDRA[3]), .A4(ADDRA[4]), .A5(ADDRA[5]), 
  .D(DINA[9]), .SPO(pre_douta_0[9]), .DPRA0(ADDRB[0]), .DPRA1(ADDRB[1]), .DPRA2(ADDRB[2]), 
  .DPRA3(ADDRB[3]), .DPRA4(ADDRB[4]), .DPRA5(ADDRB[5]), .DPO(pre_doutb_0[9]));
// Memory array element: COL: 0, ROW: 10
//  Initialization of Dual Port LUT ram now done through explicit
// parameter setting.
RAM64X1D#(.INIT(64'h0000000000000000))RAM64X1D_instance_2954(.WCLK(CLK), .WE(wea_0), 
  .A0(ADDRA[0]), .A1(ADDRA[1]), .A2(ADDRA[2]), .A3(ADDRA[3]), .A4(ADDRA[4]), .A5(ADDRA[5]), 
  .D(DINA[10]), .SPO(pre_douta_0[10]), .DPRA0(ADDRB[0]), .DPRA1(ADDRB[1]), .DPRA2(ADDRB[2]), 
  .DPRA3(ADDRB[3]), .DPRA4(ADDRB[4]), .DPRA5(ADDRB[5]), .DPO(pre_doutb_0[10]));
// Memory array element: COL: 0, ROW: 11
//  Initialization of Dual Port LUT ram now done through explicit
// parameter setting.
RAM64X1D#(.INIT(64'h0000000000000000))RAM64X1D_instance_2955(.WCLK(CLK), .WE(wea_0), 
  .A0(ADDRA[0]), .A1(ADDRA[1]), .A2(ADDRA[2]), .A3(ADDRA[3]), .A4(ADDRA[4]), .A5(ADDRA[5]), 
  .D(DINA[11]), .SPO(pre_douta_0[11]), .DPRA0(ADDRB[0]), .DPRA1(ADDRB[1]), .DPRA2(ADDRB[2]), 
  .DPRA3(ADDRB[3]), .DPRA4(ADDRB[4]), .DPRA5(ADDRB[5]), .DPO(pre_doutb_0[11]));
// Memory array element: COL: 0, ROW: 12
//  Initialization of Dual Port LUT ram now done through explicit
// parameter setting.
RAM64X1D#(.INIT(64'h0000000000000000))RAM64X1D_instance_2956(.WCLK(CLK), .WE(wea_0), 
  .A0(ADDRA[0]), .A1(ADDRA[1]), .A2(ADDRA[2]), .A3(ADDRA[3]), .A4(ADDRA[4]), .A5(ADDRA[5]), 
  .D(DINA[12]), .SPO(pre_douta_0[12]), .DPRA0(ADDRB[0]), .DPRA1(ADDRB[1]), .DPRA2(ADDRB[2]), 
  .DPRA3(ADDRB[3]), .DPRA4(ADDRB[4]), .DPRA5(ADDRB[5]), .DPO(pre_doutb_0[12]));
// Memory array element: COL: 0, ROW: 13
//  Initialization of Dual Port LUT ram now done through explicit
// parameter setting.
RAM64X1D#(.INIT(64'h0000000000000000))RAM64X1D_instance_2957(.WCLK(CLK), .WE(wea_0), 
  .A0(ADDRA[0]), .A1(ADDRA[1]), .A2(ADDRA[2]), .A3(ADDRA[3]), .A4(ADDRA[4]), .A5(ADDRA[5]), 
  .D(DINA[13]), .SPO(pre_douta_0[13]), .DPRA0(ADDRB[0]), .DPRA1(ADDRB[1]), .DPRA2(ADDRB[2]), 
  .DPRA3(ADDRB[3]), .DPRA4(ADDRB[4]), .DPRA5(ADDRB[5]), .DPO(pre_doutb_0[13]));
// Memory array element: COL: 0, ROW: 14
//  Initialization of Dual Port LUT ram now done through explicit
// parameter setting.
RAM64X1D#(.INIT(64'h0000000000000000))RAM64X1D_instance_2958(.WCLK(CLK), .WE(wea_0), 
  .A0(ADDRA[0]), .A1(ADDRA[1]), .A2(ADDRA[2]), .A3(ADDRA[3]), .A4(ADDRA[4]), .A5(ADDRA[5]), 
  .D(DINA[14]), .SPO(pre_douta_0[14]), .DPRA0(ADDRB[0]), .DPRA1(ADDRB[1]), .DPRA2(ADDRB[2]), 
  .DPRA3(ADDRB[3]), .DPRA4(ADDRB[4]), .DPRA5(ADDRB[5]), .DPO(pre_doutb_0[14]));
// Memory array element: COL: 0, ROW: 15
//  Initialization of Dual Port LUT ram now done through explicit
// parameter setting.
RAM64X1D#(.INIT(64'h0000000000000000))RAM64X1D_instance_2959(.WCLK(CLK), .WE(wea_0), 
  .A0(ADDRA[0]), .A1(ADDRA[1]), .A2(ADDRA[2]), .A3(ADDRA[3]), .A4(ADDRA[4]), .A5(ADDRA[5]), 
  .D(DINA[15]), .SPO(pre_douta_0[15]), .DPRA0(ADDRB[0]), .DPRA1(ADDRB[1]), .DPRA2(ADDRB[2]), 
  .DPRA3(ADDRB[3]), .DPRA4(ADDRB[4]), .DPRA5(ADDRB[5]), .DPO(pre_doutb_0[15]));
// Memory array element: COL: 0, ROW: 16
//  Initialization of Dual Port LUT ram now done through explicit
// parameter setting.
RAM64X1D#(.INIT(64'h0000000000000000))RAM64X1D_instance_2960(.WCLK(CLK), .WE(wea_0), 
  .A0(ADDRA[0]), .A1(ADDRA[1]), .A2(ADDRA[2]), .A3(ADDRA[3]), .A4(ADDRA[4]), .A5(ADDRA[5]), 
  .D(DINA[16]), .SPO(pre_douta_0[16]), .DPRA0(ADDRB[0]), .DPRA1(ADDRB[1]), .DPRA2(ADDRB[2]), 
  .DPRA3(ADDRB[3]), .DPRA4(ADDRB[4]), .DPRA5(ADDRB[5]), .DPO(pre_doutb_0[16]));
// Memory array element: COL: 0, ROW: 17
//  Initialization of Dual Port LUT ram now done through explicit
// parameter setting.
RAM64X1D#(.INIT(64'h0000000000000000))RAM64X1D_instance_2961(.WCLK(CLK), .WE(wea_0), 
  .A0(ADDRA[0]), .A1(ADDRA[1]), .A2(ADDRA[2]), .A3(ADDRA[3]), .A4(ADDRA[4]), .A5(ADDRA[5]), 
  .D(DINA[17]), .SPO(pre_douta_0[17]), .DPRA0(ADDRB[0]), .DPRA1(ADDRB[1]), .DPRA2(ADDRB[2]), 
  .DPRA3(ADDRB[3]), .DPRA4(ADDRB[4]), .DPRA5(ADDRB[5]), .DPO(pre_doutb_0[17]));
// Memory array element: COL: 0, ROW: 18
//  Initialization of Dual Port LUT ram now done through explicit
// parameter setting.
RAM64X1D#(.INIT(64'h0000000000000000))RAM64X1D_instance_2962(.WCLK(CLK), .WE(wea_0), 
  .A0(ADDRA[0]), .A1(ADDRA[1]), .A2(ADDRA[2]), .A3(ADDRA[3]), .A4(ADDRA[4]), .A5(ADDRA[5]), 
  .D(DINA[18]), .SPO(pre_douta_0[18]), .DPRA0(ADDRB[0]), .DPRA1(ADDRB[1]), .DPRA2(ADDRB[2]), 
  .DPRA3(ADDRB[3]), .DPRA4(ADDRB[4]), .DPRA5(ADDRB[5]), .DPO(pre_doutb_0[18]));
// Memory array element: COL: 0, ROW: 19
//  Initialization of Dual Port LUT ram now done through explicit
// parameter setting.
RAM64X1D#(.INIT(64'h0000000000000000))RAM64X1D_instance_2963(.WCLK(CLK), .WE(wea_0), 
  .A0(ADDRA[0]), .A1(ADDRA[1]), .A2(ADDRA[2]), .A3(ADDRA[3]), .A4(ADDRA[4]), .A5(ADDRA[5]), 
  .D(DINA[19]), .SPO(pre_douta_0[19]), .DPRA0(ADDRB[0]), .DPRA1(ADDRB[1]), .DPRA2(ADDRB[2]), 
  .DPRA3(ADDRB[3]), .DPRA4(ADDRB[4]), .DPRA5(ADDRB[5]), .DPO(pre_doutb_0[19]));
// Memory array element: COL: 0, ROW: 20
//  Initialization of Dual Port LUT ram now done through explicit
// parameter setting.
RAM64X1D#(.INIT(64'h0000000000000000))RAM64X1D_instance_2964(.WCLK(CLK), .WE(wea_0), 
  .A0(ADDRA[0]), .A1(ADDRA[1]), .A2(ADDRA[2]), .A3(ADDRA[3]), .A4(ADDRA[4]), .A5(ADDRA[5]), 
  .D(DINA[20]), .SPO(pre_douta_0[20]), .DPRA0(ADDRB[0]), .DPRA1(ADDRB[1]), .DPRA2(ADDRB[2]), 
  .DPRA3(ADDRB[3]), .DPRA4(ADDRB[4]), .DPRA5(ADDRB[5]), .DPO(pre_doutb_0[20]));
// Memory array element: COL: 0, ROW: 21
//  Initialization of Dual Port LUT ram now done through explicit
// parameter setting.
RAM64X1D#(.INIT(64'h0000000000000000))RAM64X1D_instance_2965(.WCLK(CLK), .WE(wea_0), 
  .A0(ADDRA[0]), .A1(ADDRA[1]), .A2(ADDRA[2]), .A3(ADDRA[3]), .A4(ADDRA[4]), .A5(ADDRA[5]), 
  .D(DINA[21]), .SPO(pre_douta_0[21]), .DPRA0(ADDRB[0]), .DPRA1(ADDRB[1]), .DPRA2(ADDRB[2]), 
  .DPRA3(ADDRB[3]), .DPRA4(ADDRB[4]), .DPRA5(ADDRB[5]), .DPO(pre_doutb_0[21]));
// Memory array element: COL: 0, ROW: 22
//  Initialization of Dual Port LUT ram now done through explicit
// parameter setting.
RAM64X1D#(.INIT(64'h0000000000000000))RAM64X1D_instance_2966(.WCLK(CLK), .WE(wea_0), 
  .A0(ADDRA[0]), .A1(ADDRA[1]), .A2(ADDRA[2]), .A3(ADDRA[3]), .A4(ADDRA[4]), .A5(ADDRA[5]), 
  .D(DINA[22]), .SPO(pre_douta_0[22]), .DPRA0(ADDRB[0]), .DPRA1(ADDRB[1]), .DPRA2(ADDRB[2]), 
  .DPRA3(ADDRB[3]), .DPRA4(ADDRB[4]), .DPRA5(ADDRB[5]), .DPO(pre_doutb_0[22]));
// Memory array element: COL: 0, ROW: 23
//  Initialization of Dual Port LUT ram now done through explicit
// parameter setting.
RAM64X1D#(.INIT(64'h0000000000000000))RAM64X1D_instance_2967(.WCLK(CLK), .WE(wea_0), 
  .A0(ADDRA[0]), .A1(ADDRA[1]), .A2(ADDRA[2]), .A3(ADDRA[3]), .A4(ADDRA[4]), .A5(ADDRA[5]), 
  .D(DINA[23]), .SPO(pre_douta_0[23]), .DPRA0(ADDRB[0]), .DPRA1(ADDRB[1]), .DPRA2(ADDRB[2]), 
  .DPRA3(ADDRB[3]), .DPRA4(ADDRB[4]), .DPRA5(ADDRB[5]), .DPO(pre_doutb_0[23]));
// Memory array element: COL: 0, ROW: 24
//  Initialization of Dual Port LUT ram now done through explicit
// parameter setting.
RAM64X1D#(.INIT(64'h0000000000000000))RAM64X1D_instance_2968(.WCLK(CLK), .WE(wea_0), 
  .A0(ADDRA[0]), .A1(ADDRA[1]), .A2(ADDRA[2]), .A3(ADDRA[3]), .A4(ADDRA[4]), .A5(ADDRA[5]), 
  .D(DINA[24]), .SPO(pre_douta_0[24]), .DPRA0(ADDRB[0]), .DPRA1(ADDRB[1]), .DPRA2(ADDRB[2]), 
  .DPRA3(ADDRB[3]), .DPRA4(ADDRB[4]), .DPRA5(ADDRB[5]), .DPO(pre_doutb_0[24]));
// Memory array element: COL: 0, ROW: 25
//  Initialization of Dual Port LUT ram now done through explicit
// parameter setting.
RAM64X1D#(.INIT(64'h0000000000000000))RAM64X1D_instance_2969(.WCLK(CLK), .WE(wea_0), 
  .A0(ADDRA[0]), .A1(ADDRA[1]), .A2(ADDRA[2]), .A3(ADDRA[3]), .A4(ADDRA[4]), .A5(ADDRA[5]), 
  .D(DINA[25]), .SPO(pre_douta_0[25]), .DPRA0(ADDRB[0]), .DPRA1(ADDRB[1]), .DPRA2(ADDRB[2]), 
  .DPRA3(ADDRB[3]), .DPRA4(ADDRB[4]), .DPRA5(ADDRB[5]), .DPO(pre_doutb_0[25]));
// Memory array element: COL: 0, ROW: 26
//  Initialization of Dual Port LUT ram now done through explicit
// parameter setting.
RAM64X1D#(.INIT(64'h0000000000000000))RAM64X1D_instance_2970(.WCLK(CLK), .WE(wea_0), 
  .A0(ADDRA[0]), .A1(ADDRA[1]), .A2(ADDRA[2]), .A3(ADDRA[3]), .A4(ADDRA[4]), .A5(ADDRA[5]), 
  .D(DINA[26]), .SPO(pre_douta_0[26]), .DPRA0(ADDRB[0]), .DPRA1(ADDRB[1]), .DPRA2(ADDRB[2]), 
  .DPRA3(ADDRB[3]), .DPRA4(ADDRB[4]), .DPRA5(ADDRB[5]), .DPO(pre_doutb_0[26]));
// Memory array element: COL: 0, ROW: 27
//  Initialization of Dual Port LUT ram now done through explicit
// parameter setting.
RAM64X1D#(.INIT(64'h0000000000000000))RAM64X1D_instance_2971(.WCLK(CLK), .WE(wea_0), 
  .A0(ADDRA[0]), .A1(ADDRA[1]), .A2(ADDRA[2]), .A3(ADDRA[3]), .A4(ADDRA[4]), .A5(ADDRA[5]), 
  .D(DINA[27]), .SPO(pre_douta_0[27]), .DPRA0(ADDRB[0]), .DPRA1(ADDRB[1]), .DPRA2(ADDRB[2]), 
  .DPRA3(ADDRB[3]), .DPRA4(ADDRB[4]), .DPRA5(ADDRB[5]), .DPO(pre_doutb_0[27]));
// Memory array element: COL: 0, ROW: 28
//  Initialization of Dual Port LUT ram now done through explicit
// parameter setting.
RAM64X1D#(.INIT(64'h0000000000000000))RAM64X1D_instance_2972(.WCLK(CLK), .WE(wea_0), 
  .A0(ADDRA[0]), .A1(ADDRA[1]), .A2(ADDRA[2]), .A3(ADDRA[3]), .A4(ADDRA[4]), .A5(ADDRA[5]), 
  .D(DINA[28]), .SPO(pre_douta_0[28]), .DPRA0(ADDRB[0]), .DPRA1(ADDRB[1]), .DPRA2(ADDRB[2]), 
  .DPRA3(ADDRB[3]), .DPRA4(ADDRB[4]), .DPRA5(ADDRB[5]), .DPO(pre_doutb_0[28]));
// Memory array element: COL: 0, ROW: 29
//  Initialization of Dual Port LUT ram now done through explicit
// parameter setting.
RAM64X1D#(.INIT(64'h0000000000000000))RAM64X1D_instance_2973(.WCLK(CLK), .WE(wea_0), 
  .A0(ADDRA[0]), .A1(ADDRA[1]), .A2(ADDRA[2]), .A3(ADDRA[3]), .A4(ADDRA[4]), .A5(ADDRA[5]), 
  .D(DINA[29]), .SPO(pre_douta_0[29]), .DPRA0(ADDRB[0]), .DPRA1(ADDRB[1]), .DPRA2(ADDRB[2]), 
  .DPRA3(ADDRB[3]), .DPRA4(ADDRB[4]), .DPRA5(ADDRB[5]), .DPO(pre_doutb_0[29]));
// Memory array element: COL: 0, ROW: 30
//  Initialization of Dual Port LUT ram now done through explicit
// parameter setting.
RAM64X1D#(.INIT(64'h0000000000000000))RAM64X1D_instance_2974(.WCLK(CLK), .WE(wea_0), 
  .A0(ADDRA[0]), .A1(ADDRA[1]), .A2(ADDRA[2]), .A3(ADDRA[3]), .A4(ADDRA[4]), .A5(ADDRA[5]), 
  .D(DINA[30]), .SPO(pre_douta_0[30]), .DPRA0(ADDRB[0]), .DPRA1(ADDRB[1]), .DPRA2(ADDRB[2]), 
  .DPRA3(ADDRB[3]), .DPRA4(ADDRB[4]), .DPRA5(ADDRB[5]), .DPO(pre_doutb_0[30]));
// Memory array element: COL: 0, ROW: 31
//  Initialization of Dual Port LUT ram now done through explicit
// parameter setting.
RAM64X1D#(.INIT(64'h0000000000000000))RAM64X1D_instance_2975(.WCLK(CLK), .WE(wea_0), 
  .A0(ADDRA[0]), .A1(ADDRA[1]), .A2(ADDRA[2]), .A3(ADDRA[3]), .A4(ADDRA[4]), .A5(ADDRA[5]), 
  .D(DINA[31]), .SPO(pre_douta_0[31]), .DPRA0(ADDRB[0]), .DPRA1(ADDRB[1]), .DPRA2(ADDRB[2]), 
  .DPRA3(ADDRB[3]), .DPRA4(ADDRB[4]), .DPRA5(ADDRB[5]), .DPO(pre_doutb_0[31]));
// Memory array element: COL: 1, ROW: 0
//  Initialization of Dual Port LUT ram now done through explicit
// parameter setting.
RAM64X1D#(.INIT(64'h0000000000000000))RAM64X1D_instance_2976(.WCLK(CLK), .WE(wea_1), 
  .A0(ADDRA[0]), .A1(ADDRA[1]), .A2(ADDRA[2]), .A3(ADDRA[3]), .A4(ADDRA[4]), .A5(ADDRA[5]), 
  .D(DINA[0]), .SPO(pre_douta_1[0]), .DPRA0(ADDRB[0]), .DPRA1(ADDRB[1]), .DPRA2(ADDRB[2]), 
  .DPRA3(ADDRB[3]), .DPRA4(ADDRB[4]), .DPRA5(ADDRB[5]), .DPO(pre_doutb_1[0]));
// Memory array element: COL: 1, ROW: 1
//  Initialization of Dual Port LUT ram now done through explicit
// parameter setting.
RAM64X1D#(.INIT(64'h0000000000000000))RAM64X1D_instance_2977(.WCLK(CLK), .WE(wea_1), 
  .A0(ADDRA[0]), .A1(ADDRA[1]), .A2(ADDRA[2]), .A3(ADDRA[3]), .A4(ADDRA[4]), .A5(ADDRA[5]), 
  .D(DINA[1]), .SPO(pre_douta_1[1]), .DPRA0(ADDRB[0]), .DPRA1(ADDRB[1]), .DPRA2(ADDRB[2]), 
  .DPRA3(ADDRB[3]), .DPRA4(ADDRB[4]), .DPRA5(ADDRB[5]), .DPO(pre_doutb_1[1]));
// Memory array element: COL: 1, ROW: 2
//  Initialization of Dual Port LUT ram now done through explicit
// parameter setting.
RAM64X1D#(.INIT(64'h0000000000000000))RAM64X1D_instance_2978(.WCLK(CLK), .WE(wea_1), 
  .A0(ADDRA[0]), .A1(ADDRA[1]), .A2(ADDRA[2]), .A3(ADDRA[3]), .A4(ADDRA[4]), .A5(ADDRA[5]), 
  .D(DINA[2]), .SPO(pre_douta_1[2]), .DPRA0(ADDRB[0]), .DPRA1(ADDRB[1]), .DPRA2(ADDRB[2]), 
  .DPRA3(ADDRB[3]), .DPRA4(ADDRB[4]), .DPRA5(ADDRB[5]), .DPO(pre_doutb_1[2]));
// Memory array element: COL: 1, ROW: 3
//  Initialization of Dual Port LUT ram now done through explicit
// parameter setting.
RAM64X1D#(.INIT(64'h0000000000000000))RAM64X1D_instance_2979(.WCLK(CLK), .WE(wea_1), 
  .A0(ADDRA[0]), .A1(ADDRA[1]), .A2(ADDRA[2]), .A3(ADDRA[3]), .A4(ADDRA[4]), .A5(ADDRA[5]), 
  .D(DINA[3]), .SPO(pre_douta_1[3]), .DPRA0(ADDRB[0]), .DPRA1(ADDRB[1]), .DPRA2(ADDRB[2]), 
  .DPRA3(ADDRB[3]), .DPRA4(ADDRB[4]), .DPRA5(ADDRB[5]), .DPO(pre_doutb_1[3]));
// Memory array element: COL: 1, ROW: 4
//  Initialization of Dual Port LUT ram now done through explicit
// parameter setting.
RAM64X1D#(.INIT(64'h0000000000000000))RAM64X1D_instance_2980(.WCLK(CLK), .WE(wea_1), 
  .A0(ADDRA[0]), .A1(ADDRA[1]), .A2(ADDRA[2]), .A3(ADDRA[3]), .A4(ADDRA[4]), .A5(ADDRA[5]), 
  .D(DINA[4]), .SPO(pre_douta_1[4]), .DPRA0(ADDRB[0]), .DPRA1(ADDRB[1]), .DPRA2(ADDRB[2]), 
  .DPRA3(ADDRB[3]), .DPRA4(ADDRB[4]), .DPRA5(ADDRB[5]), .DPO(pre_doutb_1[4]));
// Memory array element: COL: 1, ROW: 5
//  Initialization of Dual Port LUT ram now done through explicit
// parameter setting.
RAM64X1D#(.INIT(64'h0000000000000000))RAM64X1D_instance_2981(.WCLK(CLK), .WE(wea_1), 
  .A0(ADDRA[0]), .A1(ADDRA[1]), .A2(ADDRA[2]), .A3(ADDRA[3]), .A4(ADDRA[4]), .A5(ADDRA[5]), 
  .D(DINA[5]), .SPO(pre_douta_1[5]), .DPRA0(ADDRB[0]), .DPRA1(ADDRB[1]), .DPRA2(ADDRB[2]), 
  .DPRA3(ADDRB[3]), .DPRA4(ADDRB[4]), .DPRA5(ADDRB[5]), .DPO(pre_doutb_1[5]));
// Memory array element: COL: 1, ROW: 6
//  Initialization of Dual Port LUT ram now done through explicit
// parameter setting.
RAM64X1D#(.INIT(64'h0000000000000000))RAM64X1D_instance_2982(.WCLK(CLK), .WE(wea_1), 
  .A0(ADDRA[0]), .A1(ADDRA[1]), .A2(ADDRA[2]), .A3(ADDRA[3]), .A4(ADDRA[4]), .A5(ADDRA[5]), 
  .D(DINA[6]), .SPO(pre_douta_1[6]), .DPRA0(ADDRB[0]), .DPRA1(ADDRB[1]), .DPRA2(ADDRB[2]), 
  .DPRA3(ADDRB[3]), .DPRA4(ADDRB[4]), .DPRA5(ADDRB[5]), .DPO(pre_doutb_1[6]));
// Memory array element: COL: 1, ROW: 7
//  Initialization of Dual Port LUT ram now done through explicit
// parameter setting.
RAM64X1D#(.INIT(64'h0000000000000000))RAM64X1D_instance_2983(.WCLK(CLK), .WE(wea_1), 
  .A0(ADDRA[0]), .A1(ADDRA[1]), .A2(ADDRA[2]), .A3(ADDRA[3]), .A4(ADDRA[4]), .A5(ADDRA[5]), 
  .D(DINA[7]), .SPO(pre_douta_1[7]), .DPRA0(ADDRB[0]), .DPRA1(ADDRB[1]), .DPRA2(ADDRB[2]), 
  .DPRA3(ADDRB[3]), .DPRA4(ADDRB[4]), .DPRA5(ADDRB[5]), .DPO(pre_doutb_1[7]));
// Memory array element: COL: 1, ROW: 8
//  Initialization of Dual Port LUT ram now done through explicit
// parameter setting.
RAM64X1D#(.INIT(64'h0000000000000000))RAM64X1D_instance_2984(.WCLK(CLK), .WE(wea_1), 
  .A0(ADDRA[0]), .A1(ADDRA[1]), .A2(ADDRA[2]), .A3(ADDRA[3]), .A4(ADDRA[4]), .A5(ADDRA[5]), 
  .D(DINA[8]), .SPO(pre_douta_1[8]), .DPRA0(ADDRB[0]), .DPRA1(ADDRB[1]), .DPRA2(ADDRB[2]), 
  .DPRA3(ADDRB[3]), .DPRA4(ADDRB[4]), .DPRA5(ADDRB[5]), .DPO(pre_doutb_1[8]));
// Memory array element: COL: 1, ROW: 9
//  Initialization of Dual Port LUT ram now done through explicit
// parameter setting.
RAM64X1D#(.INIT(64'h0000000000000000))RAM64X1D_instance_2985(.WCLK(CLK), .WE(wea_1), 
  .A0(ADDRA[0]), .A1(ADDRA[1]), .A2(ADDRA[2]), .A3(ADDRA[3]), .A4(ADDRA[4]), .A5(ADDRA[5]), 
  .D(DINA[9]), .SPO(pre_douta_1[9]), .DPRA0(ADDRB[0]), .DPRA1(ADDRB[1]), .DPRA2(ADDRB[2]), 
  .DPRA3(ADDRB[3]), .DPRA4(ADDRB[4]), .DPRA5(ADDRB[5]), .DPO(pre_doutb_1[9]));
// Memory array element: COL: 1, ROW: 10
//  Initialization of Dual Port LUT ram now done through explicit
// parameter setting.
RAM64X1D#(.INIT(64'h0000000000000000))RAM64X1D_instance_2986(.WCLK(CLK), .WE(wea_1), 
  .A0(ADDRA[0]), .A1(ADDRA[1]), .A2(ADDRA[2]), .A3(ADDRA[3]), .A4(ADDRA[4]), .A5(ADDRA[5]), 
  .D(DINA[10]), .SPO(pre_douta_1[10]), .DPRA0(ADDRB[0]), .DPRA1(ADDRB[1]), .DPRA2(ADDRB[2]), 
  .DPRA3(ADDRB[3]), .DPRA4(ADDRB[4]), .DPRA5(ADDRB[5]), .DPO(pre_doutb_1[10]));
// Memory array element: COL: 1, ROW: 11
//  Initialization of Dual Port LUT ram now done through explicit
// parameter setting.
RAM64X1D#(.INIT(64'h0000000000000000))RAM64X1D_instance_2987(.WCLK(CLK), .WE(wea_1), 
  .A0(ADDRA[0]), .A1(ADDRA[1]), .A2(ADDRA[2]), .A3(ADDRA[3]), .A4(ADDRA[4]), .A5(ADDRA[5]), 
  .D(DINA[11]), .SPO(pre_douta_1[11]), .DPRA0(ADDRB[0]), .DPRA1(ADDRB[1]), .DPRA2(ADDRB[2]), 
  .DPRA3(ADDRB[3]), .DPRA4(ADDRB[4]), .DPRA5(ADDRB[5]), .DPO(pre_doutb_1[11]));
// Memory array element: COL: 1, ROW: 12
//  Initialization of Dual Port LUT ram now done through explicit
// parameter setting.
RAM64X1D#(.INIT(64'h0000000000000000))RAM64X1D_instance_2988(.WCLK(CLK), .WE(wea_1), 
  .A0(ADDRA[0]), .A1(ADDRA[1]), .A2(ADDRA[2]), .A3(ADDRA[3]), .A4(ADDRA[4]), .A5(ADDRA[5]), 
  .D(DINA[12]), .SPO(pre_douta_1[12]), .DPRA0(ADDRB[0]), .DPRA1(ADDRB[1]), .DPRA2(ADDRB[2]), 
  .DPRA3(ADDRB[3]), .DPRA4(ADDRB[4]), .DPRA5(ADDRB[5]), .DPO(pre_doutb_1[12]));
// Memory array element: COL: 1, ROW: 13
//  Initialization of Dual Port LUT ram now done through explicit
// parameter setting.
RAM64X1D#(.INIT(64'h0000000000000000))RAM64X1D_instance_2989(.WCLK(CLK), .WE(wea_1), 
  .A0(ADDRA[0]), .A1(ADDRA[1]), .A2(ADDRA[2]), .A3(ADDRA[3]), .A4(ADDRA[4]), .A5(ADDRA[5]), 
  .D(DINA[13]), .SPO(pre_douta_1[13]), .DPRA0(ADDRB[0]), .DPRA1(ADDRB[1]), .DPRA2(ADDRB[2]), 
  .DPRA3(ADDRB[3]), .DPRA4(ADDRB[4]), .DPRA5(ADDRB[5]), .DPO(pre_doutb_1[13]));
// Memory array element: COL: 1, ROW: 14
//  Initialization of Dual Port LUT ram now done through explicit
// parameter setting.
RAM64X1D#(.INIT(64'h0000000000000000))RAM64X1D_instance_2990(.WCLK(CLK), .WE(wea_1), 
  .A0(ADDRA[0]), .A1(ADDRA[1]), .A2(ADDRA[2]), .A3(ADDRA[3]), .A4(ADDRA[4]), .A5(ADDRA[5]), 
  .D(DINA[14]), .SPO(pre_douta_1[14]), .DPRA0(ADDRB[0]), .DPRA1(ADDRB[1]), .DPRA2(ADDRB[2]), 
  .DPRA3(ADDRB[3]), .DPRA4(ADDRB[4]), .DPRA5(ADDRB[5]), .DPO(pre_doutb_1[14]));
// Memory array element: COL: 1, ROW: 15
//  Initialization of Dual Port LUT ram now done through explicit
// parameter setting.
RAM64X1D#(.INIT(64'h0000000000000000))RAM64X1D_instance_2991(.WCLK(CLK), .WE(wea_1), 
  .A0(ADDRA[0]), .A1(ADDRA[1]), .A2(ADDRA[2]), .A3(ADDRA[3]), .A4(ADDRA[4]), .A5(ADDRA[5]), 
  .D(DINA[15]), .SPO(pre_douta_1[15]), .DPRA0(ADDRB[0]), .DPRA1(ADDRB[1]), .DPRA2(ADDRB[2]), 
  .DPRA3(ADDRB[3]), .DPRA4(ADDRB[4]), .DPRA5(ADDRB[5]), .DPO(pre_doutb_1[15]));
// Memory array element: COL: 1, ROW: 16
//  Initialization of Dual Port LUT ram now done through explicit
// parameter setting.
RAM64X1D#(.INIT(64'h0000000000000000))RAM64X1D_instance_2992(.WCLK(CLK), .WE(wea_1), 
  .A0(ADDRA[0]), .A1(ADDRA[1]), .A2(ADDRA[2]), .A3(ADDRA[3]), .A4(ADDRA[4]), .A5(ADDRA[5]), 
  .D(DINA[16]), .SPO(pre_douta_1[16]), .DPRA0(ADDRB[0]), .DPRA1(ADDRB[1]), .DPRA2(ADDRB[2]), 
  .DPRA3(ADDRB[3]), .DPRA4(ADDRB[4]), .DPRA5(ADDRB[5]), .DPO(pre_doutb_1[16]));
// Memory array element: COL: 1, ROW: 17
//  Initialization of Dual Port LUT ram now done through explicit
// parameter setting.
RAM64X1D#(.INIT(64'h0000000000000000))RAM64X1D_instance_2993(.WCLK(CLK), .WE(wea_1), 
  .A0(ADDRA[0]), .A1(ADDRA[1]), .A2(ADDRA[2]), .A3(ADDRA[3]), .A4(ADDRA[4]), .A5(ADDRA[5]), 
  .D(DINA[17]), .SPO(pre_douta_1[17]), .DPRA0(ADDRB[0]), .DPRA1(ADDRB[1]), .DPRA2(ADDRB[2]), 
  .DPRA3(ADDRB[3]), .DPRA4(ADDRB[4]), .DPRA5(ADDRB[5]), .DPO(pre_doutb_1[17]));
// Memory array element: COL: 1, ROW: 18
//  Initialization of Dual Port LUT ram now done through explicit
// parameter setting.
RAM64X1D#(.INIT(64'h0000000000000000))RAM64X1D_instance_2994(.WCLK(CLK), .WE(wea_1), 
  .A0(ADDRA[0]), .A1(ADDRA[1]), .A2(ADDRA[2]), .A3(ADDRA[3]), .A4(ADDRA[4]), .A5(ADDRA[5]), 
  .D(DINA[18]), .SPO(pre_douta_1[18]), .DPRA0(ADDRB[0]), .DPRA1(ADDRB[1]), .DPRA2(ADDRB[2]), 
  .DPRA3(ADDRB[3]), .DPRA4(ADDRB[4]), .DPRA5(ADDRB[5]), .DPO(pre_doutb_1[18]));
// Memory array element: COL: 1, ROW: 19
//  Initialization of Dual Port LUT ram now done through explicit
// parameter setting.
RAM64X1D#(.INIT(64'h0000000000000000))RAM64X1D_instance_2995(.WCLK(CLK), .WE(wea_1), 
  .A0(ADDRA[0]), .A1(ADDRA[1]), .A2(ADDRA[2]), .A3(ADDRA[3]), .A4(ADDRA[4]), .A5(ADDRA[5]), 
  .D(DINA[19]), .SPO(pre_douta_1[19]), .DPRA0(ADDRB[0]), .DPRA1(ADDRB[1]), .DPRA2(ADDRB[2]), 
  .DPRA3(ADDRB[3]), .DPRA4(ADDRB[4]), .DPRA5(ADDRB[5]), .DPO(pre_doutb_1[19]));
// Memory array element: COL: 1, ROW: 20
//  Initialization of Dual Port LUT ram now done through explicit
// parameter setting.
RAM64X1D#(.INIT(64'h0000000000000000))RAM64X1D_instance_2996(.WCLK(CLK), .WE(wea_1), 
  .A0(ADDRA[0]), .A1(ADDRA[1]), .A2(ADDRA[2]), .A3(ADDRA[3]), .A4(ADDRA[4]), .A5(ADDRA[5]), 
  .D(DINA[20]), .SPO(pre_douta_1[20]), .DPRA0(ADDRB[0]), .DPRA1(ADDRB[1]), .DPRA2(ADDRB[2]), 
  .DPRA3(ADDRB[3]), .DPRA4(ADDRB[4]), .DPRA5(ADDRB[5]), .DPO(pre_doutb_1[20]));
// Memory array element: COL: 1, ROW: 21
//  Initialization of Dual Port LUT ram now done through explicit
// parameter setting.
RAM64X1D#(.INIT(64'h0000000000000000))RAM64X1D_instance_2997(.WCLK(CLK), .WE(wea_1), 
  .A0(ADDRA[0]), .A1(ADDRA[1]), .A2(ADDRA[2]), .A3(ADDRA[3]), .A4(ADDRA[4]), .A5(ADDRA[5]), 
  .D(DINA[21]), .SPO(pre_douta_1[21]), .DPRA0(ADDRB[0]), .DPRA1(ADDRB[1]), .DPRA2(ADDRB[2]), 
  .DPRA3(ADDRB[3]), .DPRA4(ADDRB[4]), .DPRA5(ADDRB[5]), .DPO(pre_doutb_1[21]));
// Memory array element: COL: 1, ROW: 22
//  Initialization of Dual Port LUT ram now done through explicit
// parameter setting.
RAM64X1D#(.INIT(64'h0000000000000000))RAM64X1D_instance_2998(.WCLK(CLK), .WE(wea_1), 
  .A0(ADDRA[0]), .A1(ADDRA[1]), .A2(ADDRA[2]), .A3(ADDRA[3]), .A4(ADDRA[4]), .A5(ADDRA[5]), 
  .D(DINA[22]), .SPO(pre_douta_1[22]), .DPRA0(ADDRB[0]), .DPRA1(ADDRB[1]), .DPRA2(ADDRB[2]), 
  .DPRA3(ADDRB[3]), .DPRA4(ADDRB[4]), .DPRA5(ADDRB[5]), .DPO(pre_doutb_1[22]));
// Memory array element: COL: 1, ROW: 23
//  Initialization of Dual Port LUT ram now done through explicit
// parameter setting.
RAM64X1D#(.INIT(64'h0000000000000000))RAM64X1D_instance_2999(.WCLK(CLK), .WE(wea_1), 
  .A0(ADDRA[0]), .A1(ADDRA[1]), .A2(ADDRA[2]), .A3(ADDRA[3]), .A4(ADDRA[4]), .A5(ADDRA[5]), 
  .D(DINA[23]), .SPO(pre_douta_1[23]), .DPRA0(ADDRB[0]), .DPRA1(ADDRB[1]), .DPRA2(ADDRB[2]), 
  .DPRA3(ADDRB[3]), .DPRA4(ADDRB[4]), .DPRA5(ADDRB[5]), .DPO(pre_doutb_1[23]));
// Memory array element: COL: 1, ROW: 24
//  Initialization of Dual Port LUT ram now done through explicit
// parameter setting.
RAM64X1D#(.INIT(64'h0000000000000000))RAM64X1D_instance_3000(.WCLK(CLK), .WE(wea_1), 
  .A0(ADDRA[0]), .A1(ADDRA[1]), .A2(ADDRA[2]), .A3(ADDRA[3]), .A4(ADDRA[4]), .A5(ADDRA[5]), 
  .D(DINA[24]), .SPO(pre_douta_1[24]), .DPRA0(ADDRB[0]), .DPRA1(ADDRB[1]), .DPRA2(ADDRB[2]), 
  .DPRA3(ADDRB[3]), .DPRA4(ADDRB[4]), .DPRA5(ADDRB[5]), .DPO(pre_doutb_1[24]));
// Memory array element: COL: 1, ROW: 25
//  Initialization of Dual Port LUT ram now done through explicit
// parameter setting.
RAM64X1D#(.INIT(64'h0000000000000000))RAM64X1D_instance_3001(.WCLK(CLK), .WE(wea_1), 
  .A0(ADDRA[0]), .A1(ADDRA[1]), .A2(ADDRA[2]), .A3(ADDRA[3]), .A4(ADDRA[4]), .A5(ADDRA[5]), 
  .D(DINA[25]), .SPO(pre_douta_1[25]), .DPRA0(ADDRB[0]), .DPRA1(ADDRB[1]), .DPRA2(ADDRB[2]), 
  .DPRA3(ADDRB[3]), .DPRA4(ADDRB[4]), .DPRA5(ADDRB[5]), .DPO(pre_doutb_1[25]));
// Memory array element: COL: 1, ROW: 26
//  Initialization of Dual Port LUT ram now done through explicit
// parameter setting.
RAM64X1D#(.INIT(64'h0000000000000000))RAM64X1D_instance_3002(.WCLK(CLK), .WE(wea_1), 
  .A0(ADDRA[0]), .A1(ADDRA[1]), .A2(ADDRA[2]), .A3(ADDRA[3]), .A4(ADDRA[4]), .A5(ADDRA[5]), 
  .D(DINA[26]), .SPO(pre_douta_1[26]), .DPRA0(ADDRB[0]), .DPRA1(ADDRB[1]), .DPRA2(ADDRB[2]), 
  .DPRA3(ADDRB[3]), .DPRA4(ADDRB[4]), .DPRA5(ADDRB[5]), .DPO(pre_doutb_1[26]));
// Memory array element: COL: 1, ROW: 27
//  Initialization of Dual Port LUT ram now done through explicit
// parameter setting.
RAM64X1D#(.INIT(64'h0000000000000000))RAM64X1D_instance_3003(.WCLK(CLK), .WE(wea_1), 
  .A0(ADDRA[0]), .A1(ADDRA[1]), .A2(ADDRA[2]), .A3(ADDRA[3]), .A4(ADDRA[4]), .A5(ADDRA[5]), 
  .D(DINA[27]), .SPO(pre_douta_1[27]), .DPRA0(ADDRB[0]), .DPRA1(ADDRB[1]), .DPRA2(ADDRB[2]), 
  .DPRA3(ADDRB[3]), .DPRA4(ADDRB[4]), .DPRA5(ADDRB[5]), .DPO(pre_doutb_1[27]));
// Memory array element: COL: 1, ROW: 28
//  Initialization of Dual Port LUT ram now done through explicit
// parameter setting.
RAM64X1D#(.INIT(64'h0000000000000000))RAM64X1D_instance_3004(.WCLK(CLK), .WE(wea_1), 
  .A0(ADDRA[0]), .A1(ADDRA[1]), .A2(ADDRA[2]), .A3(ADDRA[3]), .A4(ADDRA[4]), .A5(ADDRA[5]), 
  .D(DINA[28]), .SPO(pre_douta_1[28]), .DPRA0(ADDRB[0]), .DPRA1(ADDRB[1]), .DPRA2(ADDRB[2]), 
  .DPRA3(ADDRB[3]), .DPRA4(ADDRB[4]), .DPRA5(ADDRB[5]), .DPO(pre_doutb_1[28]));
// Memory array element: COL: 1, ROW: 29
//  Initialization of Dual Port LUT ram now done through explicit
// parameter setting.
RAM64X1D#(.INIT(64'h0000000000000000))RAM64X1D_instance_3005(.WCLK(CLK), .WE(wea_1), 
  .A0(ADDRA[0]), .A1(ADDRA[1]), .A2(ADDRA[2]), .A3(ADDRA[3]), .A4(ADDRA[4]), .A5(ADDRA[5]), 
  .D(DINA[29]), .SPO(pre_douta_1[29]), .DPRA0(ADDRB[0]), .DPRA1(ADDRB[1]), .DPRA2(ADDRB[2]), 
  .DPRA3(ADDRB[3]), .DPRA4(ADDRB[4]), .DPRA5(ADDRB[5]), .DPO(pre_doutb_1[29]));
// Memory array element: COL: 1, ROW: 30
//  Initialization of Dual Port LUT ram now done through explicit
// parameter setting.
RAM64X1D#(.INIT(64'h0000000000000000))RAM64X1D_instance_3006(.WCLK(CLK), .WE(wea_1), 
  .A0(ADDRA[0]), .A1(ADDRA[1]), .A2(ADDRA[2]), .A3(ADDRA[3]), .A4(ADDRA[4]), .A5(ADDRA[5]), 
  .D(DINA[30]), .SPO(pre_douta_1[30]), .DPRA0(ADDRB[0]), .DPRA1(ADDRB[1]), .DPRA2(ADDRB[2]), 
  .DPRA3(ADDRB[3]), .DPRA4(ADDRB[4]), .DPRA5(ADDRB[5]), .DPO(pre_doutb_1[30]));
// Memory array element: COL: 1, ROW: 31
//  Initialization of Dual Port LUT ram now done through explicit
// parameter setting.
RAM64X1D#(.INIT(64'h0000000000000000))RAM64X1D_instance_3007(.WCLK(CLK), .WE(wea_1), 
  .A0(ADDRA[0]), .A1(ADDRA[1]), .A2(ADDRA[2]), .A3(ADDRA[3]), .A4(ADDRA[4]), .A5(ADDRA[5]), 
  .D(DINA[31]), .SPO(pre_douta_1[31]), .DPRA0(ADDRB[0]), .DPRA1(ADDRB[1]), .DPRA2(ADDRB[2]), 
  .DPRA3(ADDRB[3]), .DPRA4(ADDRB[4]), .DPRA5(ADDRB[5]), .DPO(pre_doutb_1[31]));
endmodule



module medianRow16_structuralmemory_0d8bbb88_(CLK_u92, bus_55ea36cd_, bus_16c1ae6c_, bus_614f8980_, bus_7828d1f0_, bus_1b905b34_, bus_0495362c_, bus_230ec4d9_, bus_58bbdcbb_, bus_3e2e4f72_, bus_39f9a3a4_, bus_4a01031d_, bus_69d2d349_, bus_049c99a8_);
input		CLK_u92;
input		bus_55ea36cd_;
input	[31:0]	bus_16c1ae6c_;
input	[2:0]	bus_614f8980_;
input		bus_7828d1f0_;
input	[31:0]	bus_1b905b34_;
input	[2:0]	bus_0495362c_;
input		bus_230ec4d9_;
input		bus_58bbdcbb_;
input	[31:0]	bus_3e2e4f72_;
output	[31:0]	bus_39f9a3a4_;
output		bus_4a01031d_;
output	[31:0]	bus_69d2d349_;
output		bus_049c99a8_;
wire		or_179708b1_u0;
reg		logicalMem_4f115b98_we_delay0_u0=1'h0;
wire		or_6be49610_u0;
wire		and_077ed548_u0;
wire		not_0bd39b0f_u0;
wire	[31:0]	bus_247b199f_;
wire	[31:0]	bus_55332f90_;
assign bus_39f9a3a4_=bus_247b199f_;
assign bus_4a01031d_=bus_7828d1f0_;
assign bus_69d2d349_=bus_55332f90_;
assign bus_049c99a8_=or_6be49610_u0;
assign or_179708b1_u0=bus_230ec4d9_|bus_58bbdcbb_;
always @(posedge CLK_u92 or posedge bus_55ea36cd_)
begin
if (bus_55ea36cd_)
logicalMem_4f115b98_we_delay0_u0<=1'h0;
else logicalMem_4f115b98_we_delay0_u0<=bus_58bbdcbb_;
end
assign or_6be49610_u0=and_077ed548_u0|logicalMem_4f115b98_we_delay0_u0;
assign and_077ed548_u0=bus_230ec4d9_&not_0bd39b0f_u0;
assign not_0bd39b0f_u0=~bus_58bbdcbb_;
medianRow16_forge_memory_101x32_145 medianRow16_forge_memory_101x32_145_instance0(.CLK(CLK_u92), 
  .ENA(or_179708b1_u0), .WEA(bus_58bbdcbb_), .DINA(bus_3e2e4f72_), .ADDRA(bus_1b905b34_), 
  .DOUTA(bus_55332f90_), .DONEA(), .ENB(bus_7828d1f0_), .ADDRB(bus_16c1ae6c_), .DOUTB(bus_247b199f_), 
  .DONEB());
endmodule



module medianRow16_globalreset_physical_38352fa5_(bus_03f935a5_, bus_23c2cb23_, bus_6e13e060_);
input		bus_03f935a5_;
input		bus_23c2cb23_;
output		bus_6e13e060_;
reg		sample_u58=1'h0;
wire		not_7a2bf8bd_u0;
reg		final_u58=1'h1;
wire		or_20cada6d_u0;
reg		glitch_u58=1'h0;
wire		and_403a3c27_u0;
reg		cross_u58=1'h0;
always @(posedge bus_03f935a5_)
begin
sample_u58<=1'h1;
end
assign bus_6e13e060_=or_20cada6d_u0;
assign not_7a2bf8bd_u0=~and_403a3c27_u0;
always @(posedge bus_03f935a5_)
begin
final_u58<=not_7a2bf8bd_u0;
end
assign or_20cada6d_u0=bus_23c2cb23_|final_u58;
always @(posedge bus_03f935a5_)
begin
glitch_u58<=cross_u58;
end
assign and_403a3c27_u0=cross_u58&glitch_u58;
always @(posedge bus_03f935a5_)
begin
cross_u58<=sample_u58;
end
endmodule



module medianRow16_compute_median(CLK, RESET, GO, port_43c5d04c_, port_4aafd5ce_, port_40b58ee7_, port_53a71870_, port_19f9a8b9_, RESULT, RESULT_u2222, RESULT_u2223, RESULT_u2224, RESULT_u2225, RESULT_u2226, RESULT_u2227, RESULT_u2228, RESULT_u2229, RESULT_u2230, RESULT_u2231, RESULT_u2232, RESULT_u2233, RESULT_u2234, RESULT_u2235, DONE);
input		CLK;
input		RESET;
input		GO;
input		port_43c5d04c_;
input	[31:0]	port_4aafd5ce_;
input		port_40b58ee7_;
input	[31:0]	port_53a71870_;
input		port_19f9a8b9_;
output		RESULT;
output	[31:0]	RESULT_u2222;
output		RESULT_u2223;
output	[31:0]	RESULT_u2224;
output	[2:0]	RESULT_u2225;
output		RESULT_u2226;
output	[31:0]	RESULT_u2227;
output	[2:0]	RESULT_u2228;
output		RESULT_u2229;
output	[31:0]	RESULT_u2230;
output	[31:0]	RESULT_u2231;
output	[2:0]	RESULT_u2232;
output		RESULT_u2233;
output	[7:0]	RESULT_u2234;
output	[15:0]	RESULT_u2235;
output		DONE;
wire		and_u3782_u0;
wire		lessThan;
wire signed	[32:0]	lessThan_b_signed;
wire signed	[32:0]	lessThan_a_signed;
wire		not_u882_u0;
wire		and_u3783_u0;
wire		and_u3784_u0;
wire	[31:0]	add;
wire		and_u3785_u0;
wire	[31:0]	add_u672;
wire	[31:0]	add_u673;
wire		and_u3786_u0;
wire		greaterThan;
wire signed	[31:0]	greaterThan_b_signed;
wire signed	[31:0]	greaterThan_a_signed;
wire		not_u883_u0;
wire		and_u3787_u0;
wire		and_u3788_u0;
wire	[31:0]	add_u674;
wire		and_u3789_u0;
wire	[31:0]	add_u675;
wire	[31:0]	add_u676;
wire		and_u3790_u0;
wire	[31:0]	add_u677;
reg		reg_0b164a14_u0=1'h0;
wire		or_u1444_u0;
wire		and_u3791_u0;
wire	[31:0]	add_u678;
wire	[31:0]	add_u679;
wire		or_u1445_u0;
reg		reg_2a88b31f_u0=1'h0;
wire		and_u3792_u0;
reg	[31:0]	syncEnable_u1426=32'h0;
reg	[31:0]	syncEnable_u1427_u0=32'h0;
reg	[31:0]	syncEnable_u1428_u0=32'h0;
wire	[31:0]	mux_u1912;
wire		or_u1446_u0;
wire	[31:0]	mux_u1913_u0;
reg		reg_260ff92e_u0=1'h0;
reg	[31:0]	syncEnable_u1429_u0=32'h0;
reg	[31:0]	syncEnable_u1430_u0=32'h0;
reg		block_GO_delayed_u98=1'h0;
reg	[31:0]	syncEnable_u1431_u0=32'h0;
reg	[31:0]	syncEnable_u1432_u0=32'h0;
reg		reg_652dd3ee_u0=1'h0;
reg		syncEnable_u1433_u0=1'h0;
wire		or_u1447_u0;
wire	[31:0]	mux_u1914_u0;
reg		reg_0c8a82c4_u0=1'h0;
wire		and_u3793_u0;
wire		mux_u1915_u0;
reg		reg_47512a01_u0=1'h0;
reg		and_delayed_u390=1'h0;
reg		reg_0c8a82c4_result_delayed_u0=1'h0;
wire		and_u3794_u0;
wire	[31:0]	mux_u1916_u0;
reg	[31:0]	syncEnable_u1434_u0=32'h0;
wire	[31:0]	add_u680;
reg	[31:0]	syncEnable_u1435_u0=32'h0;
reg	[31:0]	syncEnable_u1436_u0=32'h0;
wire		or_u1448_u0;
wire	[31:0]	mux_u1917_u0;
reg	[31:0]	syncEnable_u1437_u0=32'h0;
wire	[31:0]	mux_u1918_u0;
wire		or_u1449_u0;
reg	[31:0]	syncEnable_u1438_u0=32'h0;
reg		syncEnable_u1439_u0=1'h0;
reg	[31:0]	syncEnable_u1440_u0=32'h0;
reg		block_GO_delayed_u99_u0=1'h0;
reg	[31:0]	syncEnable_u1441_u0=32'h0;
reg	[31:0]	syncEnable_u1442_u0=32'h0;
wire		and_u3795_u0;
wire	[31:0]	mux_u1919_u0;
wire	[31:0]	mux_u1920_u0;
wire	[31:0]	mux_u1921_u0;
wire	[31:0]	mux_u1922_u0;
wire		mux_u1923_u0;
wire		or_u1450_u0;
wire	[31:0]	mux_u1924_u0;
wire	[15:0]	add_u681;
reg		reg_12228a42_u0=1'h0;
reg		latch_1d2c6aad_reg=1'h0;
wire		latch_1d2c6aad_out;
reg		scoreboard_6c551fbd_reg1=1'h0;
wire		bus_6013d0a5_;
wire		scoreboard_6c551fbd_and;
reg		scoreboard_6c551fbd_reg0=1'h0;
wire		scoreboard_6c551fbd_resOr1;
wire		scoreboard_6c551fbd_resOr0;
reg	[31:0]	latch_151bc7cb_reg=32'h0;
wire	[31:0]	latch_151bc7cb_out;
wire	[31:0]	latch_637372e5_out;
reg	[31:0]	latch_637372e5_reg=32'h0;
wire	[31:0]	latch_2bdb09ec_out;
reg	[31:0]	latch_2bdb09ec_reg=32'h0;
reg	[31:0]	latch_7813180b_reg=32'h0;
wire	[31:0]	latch_7813180b_out;
reg	[15:0]	syncEnable_u1443_u0=16'h0;
wire	[15:0]	lessThan_u104_a_unsigned;
wire		lessThan_u104;
wire	[15:0]	lessThan_u104_b_unsigned;
wire		andOp;
wire		and_u3796_u0;
wire		and_u3797_u0;
wire		not_u884_u0;
wire		and_u3798_u0;
reg		loopControl_u96=1'h0;
wire	[15:0]	mux_u1925_u0;
reg	[31:0]	fbReg_tmp_row1_u46=32'h0;
reg	[31:0]	fbReg_tmp_row0_u46=32'h0;
wire	[31:0]	mux_u1926_u0;
wire	[31:0]	mux_u1927_u0;
reg		fbReg_swapped_u46=1'h0;
reg		syncEnable_u1444_u0=1'h0;
wire		mux_u1928_u0;
reg	[15:0]	fbReg_idx_u46=16'h0;
wire	[31:0]	mux_u1929_u0;
reg	[31:0]	fbReg_tmp_row_u46=32'h0;
wire		or_u1451_u0;
reg	[31:0]	fbReg_tmp_u46=32'h0;
wire	[31:0]	mux_u1930_u0;
wire		and_u3799_u0;
wire		simplePinWrite;
wire	[15:0]	simplePinWrite_u622;
wire	[7:0]	simplePinWrite_u623;
reg		reg_67b61567_u0=1'h0;
reg		reg_67b61567_result_delayed_u0=1'h0;
wire	[31:0]	mux_u1931_u0;
wire		or_u1452_u0;
assign and_u3782_u0=and_u3796_u0&or_u1451_u0;
assign lessThan_a_signed={1'b0, mux_u1921_u0};
assign lessThan_b_signed=33'h64;
assign lessThan=lessThan_a_signed<lessThan_b_signed;
assign not_u882_u0=~lessThan;
assign and_u3783_u0=or_u1450_u0&not_u882_u0;
assign and_u3784_u0=or_u1450_u0&lessThan;
assign add=mux_u1921_u0+32'h0;
assign and_u3785_u0=and_u3795_u0&port_43c5d04c_;
assign add_u672=mux_u1921_u0+32'h1;
assign add_u673=add_u672+32'h0;
assign and_u3786_u0=and_u3795_u0&port_19f9a8b9_;
assign greaterThan_a_signed=syncEnable_u1436_u0;
assign greaterThan_b_signed=syncEnable_u1440_u0;
assign greaterThan=greaterThan_a_signed>greaterThan_b_signed;
assign not_u883_u0=~greaterThan;
assign and_u3787_u0=block_GO_delayed_u99_u0&not_u883_u0;
assign and_u3788_u0=block_GO_delayed_u99_u0&greaterThan;
assign add_u674=syncEnable_u1438_u0+32'h0;
assign and_u3789_u0=and_u3794_u0&port_43c5d04c_;
assign add_u675=syncEnable_u1438_u0+32'h1;
assign add_u676=add_u675+32'h0;
assign and_u3790_u0=and_u3794_u0&port_19f9a8b9_;
assign add_u677=syncEnable_u1438_u0+32'h0;
always @(posedge CLK or posedge block_GO_delayed_u98 or posedge or_u1444_u0)
begin
if (or_u1444_u0)
reg_0b164a14_u0<=1'h0;
else if (block_GO_delayed_u98)
reg_0b164a14_u0<=1'h1;
else reg_0b164a14_u0<=reg_0b164a14_u0;
end
assign or_u1444_u0=and_u3791_u0|RESET;
assign and_u3791_u0=reg_0b164a14_u0&port_19f9a8b9_;
assign add_u678=syncEnable_u1438_u0+32'h1;
assign add_u679=add_u678+32'h0;
assign or_u1445_u0=and_u3792_u0|RESET;
always @(posedge CLK or posedge reg_260ff92e_u0 or posedge or_u1445_u0)
begin
if (or_u1445_u0)
reg_2a88b31f_u0<=1'h0;
else if (reg_260ff92e_u0)
reg_2a88b31f_u0<=1'h1;
else reg_2a88b31f_u0<=reg_2a88b31f_u0;
end
assign and_u3792_u0=reg_2a88b31f_u0&port_19f9a8b9_;
always @(posedge CLK)
begin
if (and_u3794_u0)
syncEnable_u1426<=add_u677;
end
always @(posedge CLK)
begin
if (and_u3794_u0)
syncEnable_u1427_u0<=port_4aafd5ce_;
end
always @(posedge CLK)
begin
if (and_u3794_u0)
syncEnable_u1428_u0<=port_53a71870_;
end
assign mux_u1912=(block_GO_delayed_u98)?syncEnable_u1430_u0:syncEnable_u1431_u0;
assign or_u1446_u0=block_GO_delayed_u98|reg_260ff92e_u0;
assign mux_u1913_u0=({32{block_GO_delayed_u98}}&syncEnable_u1426)|({32{reg_260ff92e_u0}}&syncEnable_u1429_u0)|({32{and_u3794_u0}}&add_u676);
always @(posedge CLK or posedge RESET)
begin
if (RESET)
reg_260ff92e_u0<=1'h0;
else reg_260ff92e_u0<=block_GO_delayed_u98;
end
always @(posedge CLK)
begin
if (and_u3794_u0)
syncEnable_u1429_u0<=add_u679;
end
always @(posedge CLK)
begin
if (and_u3794_u0)
syncEnable_u1430_u0<=port_53a71870_;
end
always @(posedge CLK or posedge RESET)
begin
if (RESET)
block_GO_delayed_u98<=1'h0;
else block_GO_delayed_u98<=and_u3794_u0;
end
always @(posedge CLK)
begin
if (and_u3794_u0)
syncEnable_u1431_u0<=port_4aafd5ce_;
end
always @(posedge CLK)
begin
if (block_GO_delayed_u99_u0)
syncEnable_u1432_u0<=syncEnable_u1437_u0;
end
always @(posedge CLK or posedge RESET)
begin
if (RESET)
reg_652dd3ee_u0<=1'h0;
else reg_652dd3ee_u0<=reg_0c8a82c4_result_delayed_u0;
end
always @(posedge CLK)
begin
if (block_GO_delayed_u99_u0)
syncEnable_u1433_u0<=syncEnable_u1439_u0;
end
assign or_u1447_u0=and_delayed_u390|reg_652dd3ee_u0;
assign mux_u1914_u0=(and_delayed_u390)?syncEnable_u1434_u0:syncEnable_u1427_u0;
always @(posedge CLK or posedge RESET)
begin
if (RESET)
reg_0c8a82c4_u0<=1'h0;
else reg_0c8a82c4_u0<=reg_47512a01_u0;
end
assign and_u3793_u0=and_u3787_u0&block_GO_delayed_u99_u0;
assign mux_u1915_u0=(and_delayed_u390)?syncEnable_u1433_u0:1'h1;
always @(posedge CLK or posedge RESET)
begin
if (RESET)
reg_47512a01_u0<=1'h0;
else reg_47512a01_u0<=and_u3794_u0;
end
always @(posedge CLK or posedge RESET)
begin
if (RESET)
and_delayed_u390<=1'h0;
else and_delayed_u390<=and_u3793_u0;
end
always @(posedge CLK or posedge RESET)
begin
if (RESET)
reg_0c8a82c4_result_delayed_u0<=1'h0;
else reg_0c8a82c4_result_delayed_u0<=reg_0c8a82c4_u0;
end
assign and_u3794_u0=and_u3788_u0&block_GO_delayed_u99_u0;
assign mux_u1916_u0=(and_delayed_u390)?syncEnable_u1432_u0:syncEnable_u1428_u0;
always @(posedge CLK)
begin
if (block_GO_delayed_u99_u0)
syncEnable_u1434_u0<=syncEnable_u1435_u0;
end
assign add_u680=mux_u1921_u0+32'h1;
always @(posedge CLK)
begin
if (and_u3795_u0)
syncEnable_u1435_u0<=mux_u1920_u0;
end
always @(posedge CLK)
begin
if (and_u3795_u0)
syncEnable_u1436_u0<=port_4aafd5ce_;
end
assign or_u1448_u0=and_u3795_u0|and_u3794_u0;
assign mux_u1917_u0=({32{or_u1446_u0}}&mux_u1913_u0)|({32{and_u3795_u0}}&add_u673)|({32{and_u3794_u0}}&mux_u1913_u0);
always @(posedge CLK)
begin
if (and_u3795_u0)
syncEnable_u1437_u0<=mux_u1922_u0;
end
assign mux_u1918_u0=(and_u3795_u0)?add:add_u674;
assign or_u1449_u0=and_u3795_u0|and_u3794_u0;
always @(posedge CLK)
begin
if (and_u3795_u0)
syncEnable_u1438_u0<=mux_u1921_u0;
end
always @(posedge CLK)
begin
if (and_u3795_u0)
syncEnable_u1439_u0<=mux_u1923_u0;
end
always @(posedge CLK)
begin
if (and_u3795_u0)
syncEnable_u1440_u0<=port_53a71870_;
end
always @(posedge CLK or posedge RESET)
begin
if (RESET)
block_GO_delayed_u99_u0<=1'h0;
else block_GO_delayed_u99_u0<=and_u3795_u0;
end
always @(posedge CLK)
begin
if (and_u3795_u0)
syncEnable_u1441_u0<=add_u680;
end
always @(posedge CLK)
begin
if (or_u1450_u0)
syncEnable_u1442_u0<=mux_u1924_u0;
end
assign and_u3795_u0=and_u3784_u0&or_u1450_u0;
assign mux_u1919_u0=(and_u3782_u0)?mux_u1930_u0:syncEnable_u1440_u0;
assign mux_u1920_u0=(and_u3782_u0)?mux_u1926_u0:mux_u1914_u0;
assign mux_u1921_u0=(and_u3782_u0)?32'h0:syncEnable_u1441_u0;
assign mux_u1922_u0=(and_u3782_u0)?mux_u1929_u0:mux_u1916_u0;
assign mux_u1923_u0=(and_u3782_u0)?1'h0:mux_u1915_u0;
assign or_u1450_u0=and_u3782_u0|or_u1447_u0;
assign mux_u1924_u0=(and_u3782_u0)?mux_u1927_u0:syncEnable_u1436_u0;
assign add_u681=mux_u1925_u0+16'h1;
always @(posedge CLK or posedge RESET)
begin
if (RESET)
reg_12228a42_u0<=1'h0;
else reg_12228a42_u0<=or_u1447_u0;
end
always @(posedge CLK)
begin
if (or_u1447_u0)
latch_1d2c6aad_reg<=mux_u1915_u0;
end
assign latch_1d2c6aad_out=(or_u1447_u0)?mux_u1915_u0:latch_1d2c6aad_reg;
always @(posedge CLK)
begin
if (bus_6013d0a5_)
scoreboard_6c551fbd_reg1<=1'h0;
else if (reg_12228a42_u0)
scoreboard_6c551fbd_reg1<=1'h1;
else scoreboard_6c551fbd_reg1<=scoreboard_6c551fbd_reg1;
end
assign bus_6013d0a5_=scoreboard_6c551fbd_and|RESET;
assign scoreboard_6c551fbd_and=scoreboard_6c551fbd_resOr0&scoreboard_6c551fbd_resOr1;
always @(posedge CLK)
begin
if (bus_6013d0a5_)
scoreboard_6c551fbd_reg0<=1'h0;
else if (or_u1447_u0)
scoreboard_6c551fbd_reg0<=1'h1;
else scoreboard_6c551fbd_reg0<=scoreboard_6c551fbd_reg0;
end
assign scoreboard_6c551fbd_resOr1=reg_12228a42_u0|scoreboard_6c551fbd_reg1;
assign scoreboard_6c551fbd_resOr0=or_u1447_u0|scoreboard_6c551fbd_reg0;
always @(posedge CLK)
begin
if (or_u1447_u0)
latch_151bc7cb_reg<=mux_u1914_u0;
end
assign latch_151bc7cb_out=(or_u1447_u0)?mux_u1914_u0:latch_151bc7cb_reg;
assign latch_637372e5_out=(or_u1447_u0)?syncEnable_u1440_u0:latch_637372e5_reg;
always @(posedge CLK)
begin
if (or_u1447_u0)
latch_637372e5_reg<=syncEnable_u1440_u0;
end
assign latch_2bdb09ec_out=(or_u1447_u0)?mux_u1916_u0:latch_2bdb09ec_reg;
always @(posedge CLK)
begin
if (or_u1447_u0)
latch_2bdb09ec_reg<=mux_u1916_u0;
end
always @(posedge CLK)
begin
if (or_u1447_u0)
latch_7813180b_reg<=syncEnable_u1442_u0;
end
assign latch_7813180b_out=(or_u1447_u0)?syncEnable_u1442_u0:latch_7813180b_reg;
always @(posedge CLK)
begin
if (and_u3782_u0)
syncEnable_u1443_u0<=add_u681;
end
assign lessThan_u104_a_unsigned=mux_u1925_u0;
assign lessThan_u104_b_unsigned=16'h65;
assign lessThan_u104=lessThan_u104_a_unsigned<lessThan_u104_b_unsigned;
assign andOp=lessThan_u104&mux_u1928_u0;
assign and_u3796_u0=or_u1451_u0&andOp;
assign and_u3797_u0=or_u1451_u0&not_u884_u0;
assign not_u884_u0=~andOp;
assign and_u3798_u0=and_u3797_u0&or_u1451_u0;
always @(posedge CLK or posedge syncEnable_u1444_u0)
begin
if (syncEnable_u1444_u0)
loopControl_u96<=1'h0;
else loopControl_u96<=scoreboard_6c551fbd_and;
end
assign mux_u1925_u0=(GO)?16'h0:fbReg_idx_u46;
always @(posedge CLK)
begin
if (scoreboard_6c551fbd_and)
fbReg_tmp_row1_u46<=latch_2bdb09ec_out;
end
always @(posedge CLK)
begin
if (scoreboard_6c551fbd_and)
fbReg_tmp_row0_u46<=latch_637372e5_out;
end
assign mux_u1926_u0=(GO)?32'h0:fbReg_tmp_u46;
assign mux_u1927_u0=(GO)?32'h0:fbReg_tmp_row_u46;
always @(posedge CLK)
begin
if (scoreboard_6c551fbd_and)
fbReg_swapped_u46<=latch_1d2c6aad_out;
end
always @(posedge CLK)
begin
if (GO)
syncEnable_u1444_u0<=RESET;
end
assign mux_u1928_u0=(GO)?1'h0:fbReg_swapped_u46;
always @(posedge CLK)
begin
if (scoreboard_6c551fbd_and)
fbReg_idx_u46<=syncEnable_u1443_u0;
end
assign mux_u1929_u0=(GO)?32'h0:fbReg_tmp_row1_u46;
always @(posedge CLK)
begin
if (scoreboard_6c551fbd_and)
fbReg_tmp_row_u46<=latch_7813180b_out;
end
assign or_u1451_u0=GO|loopControl_u96;
always @(posedge CLK)
begin
if (scoreboard_6c551fbd_and)
fbReg_tmp_u46<=latch_151bc7cb_out;
end
assign mux_u1930_u0=(GO)?32'h0:fbReg_tmp_row0_u46;
assign and_u3799_u0=reg_67b61567_u0&port_43c5d04c_;
assign simplePinWrite=reg_67b61567_u0&{1{reg_67b61567_u0}};
assign simplePinWrite_u622=16'h1&{16{1'h1}};
assign simplePinWrite_u623=port_4aafd5ce_[7:0];
always @(posedge CLK or posedge RESET)
begin
if (RESET)
reg_67b61567_u0<=1'h0;
else reg_67b61567_u0<=and_u3798_u0;
end
always @(posedge CLK or posedge RESET)
begin
if (RESET)
reg_67b61567_result_delayed_u0<=1'h0;
else reg_67b61567_result_delayed_u0<=reg_67b61567_u0;
end
assign mux_u1931_u0=(or_u1449_u0)?mux_u1918_u0:32'h32;
assign or_u1452_u0=or_u1449_u0|reg_67b61567_u0;
assign RESULT=GO;
assign RESULT_u2222=32'h0;
assign RESULT_u2223=or_u1452_u0;
assign RESULT_u2224=mux_u1931_u0;
assign RESULT_u2225=3'h1;
assign RESULT_u2226=or_u1448_u0;
assign RESULT_u2227=mux_u1917_u0;
assign RESULT_u2228=3'h1;
assign RESULT_u2229=or_u1446_u0;
assign RESULT_u2230=mux_u1917_u0;
assign RESULT_u2231=mux_u1912;
assign RESULT_u2232=3'h1;
assign RESULT_u2233=simplePinWrite;
assign RESULT_u2234=simplePinWrite_u623;
assign RESULT_u2235=simplePinWrite_u622;
assign DONE=reg_67b61567_result_delayed_u0;
endmodule



module medianRow16_endianswapper_4fb41bdc_(endianswapper_4fb41bdc_in, endianswapper_4fb41bdc_out);
input	[31:0]	endianswapper_4fb41bdc_in;
output	[31:0]	endianswapper_4fb41bdc_out;
assign endianswapper_4fb41bdc_out=endianswapper_4fb41bdc_in;
endmodule



module medianRow16_endianswapper_40ba5c35_(endianswapper_40ba5c35_in, endianswapper_40ba5c35_out);
input	[31:0]	endianswapper_40ba5c35_in;
output	[31:0]	endianswapper_40ba5c35_out;
assign endianswapper_40ba5c35_out=endianswapper_40ba5c35_in;
endmodule



module medianRow16_stateVar_i(bus_3d2da586_, bus_4b9da0ca_, bus_72bc1f9b_, bus_0450faa8_, bus_0d4a8339_, bus_40411b48_, bus_283dd90f_);
input		bus_3d2da586_;
input		bus_4b9da0ca_;
input		bus_72bc1f9b_;
input	[31:0]	bus_0450faa8_;
input		bus_0d4a8339_;
input	[31:0]	bus_40411b48_;
output	[31:0]	bus_283dd90f_;
wire	[31:0]	mux_1fe66d82_u0;
reg	[31:0]	stateVar_i_u48=32'h0;
wire	[31:0]	endianswapper_4fb41bdc_out;
wire		or_53e408f8_u0;
wire	[31:0]	endianswapper_40ba5c35_out;
assign bus_283dd90f_=endianswapper_40ba5c35_out;
assign mux_1fe66d82_u0=(bus_72bc1f9b_)?bus_0450faa8_:32'h0;
always @(posedge bus_3d2da586_ or posedge bus_4b9da0ca_)
begin
if (bus_4b9da0ca_)
stateVar_i_u48<=32'h0;
else if (or_53e408f8_u0)
stateVar_i_u48<=endianswapper_4fb41bdc_out;
end
medianRow16_endianswapper_4fb41bdc_ medianRow16_endianswapper_4fb41bdc__1(.endianswapper_4fb41bdc_in(mux_1fe66d82_u0), 
  .endianswapper_4fb41bdc_out(endianswapper_4fb41bdc_out));
assign or_53e408f8_u0=bus_72bc1f9b_|bus_0d4a8339_;
medianRow16_endianswapper_40ba5c35_ medianRow16_endianswapper_40ba5c35__1(.endianswapper_40ba5c35_in(stateVar_i_u48), 
  .endianswapper_40ba5c35_out(endianswapper_40ba5c35_out));
endmodule



module medianRow16_simplememoryreferee_3ece07d3_(bus_4225c911_, bus_1911215d_, bus_1fc1c0ae_, bus_0f77866b_, bus_3efbe91a_, bus_5ed83f9a_, bus_5b46e87f_, bus_19c5e972_, bus_2dae25e0_, bus_78a258a2_, bus_091116cb_, bus_387f221d_, bus_75c0f5f9_, bus_7b23c32c_, bus_6119244d_, bus_37b13109_, bus_4049f1ab_, bus_59113781_, bus_00e0d341_, bus_2bd281cf_, bus_374dfb27_);
input		bus_4225c911_;
input		bus_1911215d_;
input		bus_1fc1c0ae_;
input	[31:0]	bus_0f77866b_;
input		bus_3efbe91a_;
input	[31:0]	bus_5ed83f9a_;
input	[31:0]	bus_5b46e87f_;
input	[2:0]	bus_19c5e972_;
input		bus_2dae25e0_;
input		bus_78a258a2_;
input	[31:0]	bus_091116cb_;
input	[31:0]	bus_387f221d_;
input	[2:0]	bus_75c0f5f9_;
output	[31:0]	bus_7b23c32c_;
output	[31:0]	bus_6119244d_;
output		bus_37b13109_;
output		bus_4049f1ab_;
output	[2:0]	bus_59113781_;
output		bus_00e0d341_;
output	[31:0]	bus_2bd281cf_;
output		bus_374dfb27_;
wire		and_0cc76084_u0;
wire		or_4b550a11_u0;
wire		or_46b6a03e_u0;
wire		and_5cf882f2_u0;
wire		or_0af9c892_u0;
wire		or_1cca2981_u0;
reg		done_qual_u220=1'h0;
wire		not_38eda100_u0;
wire	[31:0]	mux_32563527_u0;
wire	[31:0]	mux_0387bf95_u0;
wire		or_170cffda_u0;
reg		done_qual_u221_u0=1'h0;
wire		not_1138d4a1_u0;
assign and_0cc76084_u0=or_170cffda_u0&bus_1fc1c0ae_;
assign or_4b550a11_u0=bus_3efbe91a_|bus_78a258a2_;
assign or_46b6a03e_u0=bus_3efbe91a_|or_1cca2981_u0;
assign bus_7b23c32c_=mux_32563527_u0;
assign bus_6119244d_=mux_0387bf95_u0;
assign bus_37b13109_=or_4b550a11_u0;
assign bus_4049f1ab_=or_46b6a03e_u0;
assign bus_59113781_=3'h1;
assign bus_00e0d341_=and_5cf882f2_u0;
assign bus_2bd281cf_=bus_0f77866b_;
assign bus_374dfb27_=and_0cc76084_u0;
assign and_5cf882f2_u0=or_0af9c892_u0&bus_1fc1c0ae_;
assign or_0af9c892_u0=bus_3efbe91a_|done_qual_u221_u0;
assign or_1cca2981_u0=bus_2dae25e0_|bus_78a258a2_;
always @(posedge bus_4225c911_)
begin
if (bus_1911215d_)
done_qual_u220<=1'h0;
else done_qual_u220<=or_1cca2981_u0;
end
assign not_38eda100_u0=~bus_1fc1c0ae_;
assign mux_32563527_u0=(bus_3efbe91a_)?{24'b0, bus_5ed83f9a_[7:0]}:bus_091116cb_;
assign mux_0387bf95_u0=(bus_3efbe91a_)?bus_5b46e87f_:bus_387f221d_;
assign or_170cffda_u0=or_1cca2981_u0|done_qual_u220;
always @(posedge bus_4225c911_)
begin
if (bus_1911215d_)
done_qual_u221_u0<=1'h0;
else done_qual_u221_u0<=bus_3efbe91a_;
end
assign not_1138d4a1_u0=~bus_1fc1c0ae_;
endmodule



module medianRow16_scheduler(CLK, RESET, GO, port_5d3fe7dc_, port_180d77fd_, port_4c22b9a8_, port_6f85f68f_, port_47344a02_, port_201e216b_, RESULT, RESULT_u2236, RESULT_u2237, RESULT_u2238, DONE);
input		CLK;
input		RESET;
input		GO;
input		port_5d3fe7dc_;
input	[31:0]	port_180d77fd_;
input		port_4c22b9a8_;
input		port_6f85f68f_;
input		port_47344a02_;
input		port_201e216b_;
output		RESULT;
output		RESULT_u2236;
output		RESULT_u2237;
output		RESULT_u2238;
output		DONE;
wire		and_u3800_u0;
wire signed	[31:0]	lessThan_b_signed;
wire signed	[31:0]	lessThan_a_signed;
wire		lessThan;
wire signed	[31:0]	equals_b_signed;
wire		equals;
wire signed	[31:0]	equals_a_signed;
wire signed	[31:0]	equals_u234_b_signed;
wire signed	[31:0]	equals_u234_a_signed;
wire		equals_u234;
wire		and_u3801_u0;
wire		not_u885_u0;
wire		and_u3802_u0;
wire		andOp;
wire		not_u886_u0;
wire		and_u3803_u0;
wire		and_u3804_u0;
wire		simplePinWrite;
wire		and_u3805_u0;
wire		and_u3806_u0;
wire signed	[31:0]	equals_u235_a_signed;
wire signed	[31:0]	equals_u235_b_signed;
wire		equals_u235;
wire		not_u887_u0;
wire		and_u3807_u0;
wire		and_u3808_u0;
wire		andOp_u80;
wire		not_u888_u0;
wire		and_u3809_u0;
wire		and_u3810_u0;
wire		simplePinWrite_u624;
wire		and_u3811_u0;
wire		and_u3812_u0;
wire		not_u889_u0;
wire		not_u890_u0;
wire		and_u3813_u0;
wire		and_u3814_u0;
wire		simplePinWrite_u625;
reg		reg_0b9b14a1_u0=1'h0;
wire		and_u3815_u0;
wire		or_u1453_u0;
wire		and_u3816_u0;
wire		and_u3817_u0;
wire		and_u3818_u0;
wire		or_u1454_u0;
reg		and_delayed_u391=1'h0;
wire		and_u3819_u0;
wire		and_u3820_u0;
wire		or_u1455_u0;
reg		and_delayed_u392_u0=1'h0;
wire		or_u1456_u0;
wire		mux_u1932;
wire		and_u3821_u0;
wire		or_u1457_u0;
reg		reg_5387326d_u0=1'h0;
wire		and_u3822_u0;
wire		mux_u1933_u0;
wire		or_u1458_u0;
wire		receive_go_merge;
wire		or_u1459_u0;
reg		loopControl_u97=1'h0;
reg		syncEnable_u1445=1'h0;
reg		reg_586bbb6b_u0=1'h0;
reg		reg_2deb052f_u0=1'h0;
wire		or_u1460_u0;
wire		mux_u1934_u0;
assign and_u3800_u0=or_u1459_u0&or_u1459_u0;
assign lessThan_a_signed=port_180d77fd_;
assign lessThan_b_signed=32'h65;
assign lessThan=lessThan_a_signed<lessThan_b_signed;
assign equals_a_signed=port_180d77fd_;
assign equals_b_signed=32'h65;
assign equals=equals_a_signed==equals_b_signed;
assign equals_u234_a_signed={31'b0, port_5d3fe7dc_};
assign equals_u234_b_signed=32'h0;
assign equals_u234=equals_u234_a_signed==equals_u234_b_signed;
assign and_u3801_u0=and_u3800_u0&equals_u234;
assign not_u885_u0=~equals_u234;
assign and_u3802_u0=and_u3800_u0&not_u885_u0;
assign andOp=lessThan&port_47344a02_;
assign not_u886_u0=~andOp;
assign and_u3803_u0=and_u3806_u0&not_u886_u0;
assign and_u3804_u0=and_u3806_u0&andOp;
assign simplePinWrite=and_u3805_u0&{1{and_u3805_u0}};
assign and_u3805_u0=and_u3804_u0&and_u3806_u0;
assign and_u3806_u0=and_u3801_u0&and_u3800_u0;
assign equals_u235_a_signed={31'b0, port_5d3fe7dc_};
assign equals_u235_b_signed=32'h1;
assign equals_u235=equals_u235_a_signed==equals_u235_b_signed;
assign not_u887_u0=~equals_u235;
assign and_u3807_u0=and_u3800_u0&equals_u235;
assign and_u3808_u0=and_u3800_u0&not_u887_u0;
assign andOp_u80=lessThan&port_47344a02_;
assign not_u888_u0=~andOp_u80;
assign and_u3809_u0=and_u3821_u0&not_u888_u0;
assign and_u3810_u0=and_u3821_u0&andOp_u80;
assign simplePinWrite_u624=and_u3820_u0&{1{and_u3820_u0}};
assign and_u3811_u0=and_u3819_u0&not_u889_u0;
assign and_u3812_u0=and_u3819_u0&equals;
assign not_u889_u0=~equals;
assign not_u890_u0=~port_201e216b_;
assign and_u3813_u0=and_u3818_u0&port_201e216b_;
assign and_u3814_u0=and_u3818_u0&not_u890_u0;
assign simplePinWrite_u625=and_u3816_u0&{1{and_u3816_u0}};
always @(posedge CLK or posedge RESET)
begin
if (RESET)
reg_0b9b14a1_u0<=1'h0;
else reg_0b9b14a1_u0<=and_u3815_u0;
end
assign and_u3815_u0=and_u3814_u0&and_u3818_u0;
assign or_u1453_u0=port_6f85f68f_|reg_0b9b14a1_u0;
assign and_u3816_u0=and_u3813_u0&and_u3818_u0;
assign and_u3817_u0=and_u3811_u0&and_u3819_u0;
assign and_u3818_u0=and_u3812_u0&and_u3819_u0;
assign or_u1454_u0=and_delayed_u391|or_u1453_u0;
always @(posedge CLK or posedge RESET)
begin
if (RESET)
and_delayed_u391<=1'h0;
else and_delayed_u391<=and_u3817_u0;
end
assign and_u3819_u0=and_u3809_u0&and_u3821_u0;
assign and_u3820_u0=and_u3810_u0&and_u3821_u0;
assign or_u1455_u0=or_u1454_u0|and_delayed_u392_u0;
always @(posedge CLK or posedge RESET)
begin
if (RESET)
and_delayed_u392_u0<=1'h0;
else and_delayed_u392_u0<=and_u3820_u0;
end
assign or_u1456_u0=and_u3820_u0|and_u3816_u0;
assign mux_u1932=(and_u3820_u0)?1'h1:1'h0;
assign and_u3821_u0=and_u3807_u0&and_u3800_u0;
assign or_u1457_u0=or_u1455_u0|reg_5387326d_u0;
always @(posedge CLK or posedge RESET)
begin
if (RESET)
reg_5387326d_u0<=1'h0;
else reg_5387326d_u0<=and_u3822_u0;
end
assign and_u3822_u0=and_u3808_u0&and_u3800_u0;
assign mux_u1933_u0=(and_u3805_u0)?1'h1:mux_u1932;
assign or_u1458_u0=and_u3805_u0|or_u1456_u0;
assign receive_go_merge=simplePinWrite|simplePinWrite_u624;
assign or_u1459_u0=reg_586bbb6b_u0|loopControl_u97;
always @(posedge CLK or posedge syncEnable_u1445)
begin
if (syncEnable_u1445)
loopControl_u97<=1'h0;
else loopControl_u97<=or_u1457_u0;
end
always @(posedge CLK)
begin
if (reg_586bbb6b_u0)
syncEnable_u1445<=RESET;
end
always @(posedge CLK or posedge RESET)
begin
if (RESET)
reg_586bbb6b_u0<=1'h0;
else reg_586bbb6b_u0<=reg_2deb052f_u0;
end
always @(posedge CLK or posedge RESET)
begin
if (RESET)
reg_2deb052f_u0<=1'h0;
else reg_2deb052f_u0<=GO;
end
assign or_u1460_u0=GO|or_u1458_u0;
assign mux_u1934_u0=(GO)?1'h0:mux_u1933_u0;
assign RESULT=or_u1460_u0;
assign RESULT_u2236=mux_u1934_u0;
assign RESULT_u2237=simplePinWrite_u625;
assign RESULT_u2238=receive_go_merge;
assign DONE=1'h0;
endmodule



module medianRow16_Kicker_58(CLK, RESET, bus_0de0a591_);
input		CLK;
input		RESET;
output		bus_0de0a591_;
wire		bus_669aa80b_;
reg		kicker_1=1'h0;
wire		bus_6757154e_;
wire		bus_7cf112c9_;
reg		kicker_2=1'h0;
reg		kicker_res=1'h0;
wire		bus_17b33a3c_;
assign bus_0de0a591_=kicker_res;
assign bus_669aa80b_=kicker_1&bus_17b33a3c_&bus_6757154e_;
always @(posedge CLK)
begin
kicker_1<=bus_17b33a3c_;
end
assign bus_6757154e_=~kicker_2;
assign bus_7cf112c9_=bus_17b33a3c_&kicker_1;
always @(posedge CLK)
begin
kicker_2<=bus_7cf112c9_;
end
always @(posedge CLK)
begin
kicker_res<=bus_669aa80b_;
end
assign bus_17b33a3c_=~RESET;
endmodule



module medianRow16_endianswapper_72b485a1_(endianswapper_72b485a1_in, endianswapper_72b485a1_out);
input		endianswapper_72b485a1_in;
output		endianswapper_72b485a1_out;
assign endianswapper_72b485a1_out=endianswapper_72b485a1_in;
endmodule



module medianRow16_endianswapper_3bbe29f1_(endianswapper_3bbe29f1_in, endianswapper_3bbe29f1_out);
input		endianswapper_3bbe29f1_in;
output		endianswapper_3bbe29f1_out;
assign endianswapper_3bbe29f1_out=endianswapper_3bbe29f1_in;
endmodule



module medianRow16_stateVar_fsmState_medianRow16(bus_0eb5fabd_, bus_5a9ffcd7_, bus_05ae6e75_, bus_01b32561_, bus_00eda733_);
input		bus_0eb5fabd_;
input		bus_5a9ffcd7_;
input		bus_05ae6e75_;
input		bus_01b32561_;
output		bus_00eda733_;
wire		endianswapper_72b485a1_out;
reg		stateVar_fsmState_medianRow16_u2=1'h0;
wire		endianswapper_3bbe29f1_out;
medianRow16_endianswapper_72b485a1_ medianRow16_endianswapper_72b485a1__1(.endianswapper_72b485a1_in(stateVar_fsmState_medianRow16_u2), 
  .endianswapper_72b485a1_out(endianswapper_72b485a1_out));
always @(posedge bus_0eb5fabd_ or posedge bus_5a9ffcd7_)
begin
if (bus_5a9ffcd7_)
stateVar_fsmState_medianRow16_u2<=1'h0;
else if (bus_05ae6e75_)
stateVar_fsmState_medianRow16_u2<=endianswapper_3bbe29f1_out;
end
assign bus_00eda733_=endianswapper_72b485a1_out;
medianRow16_endianswapper_3bbe29f1_ medianRow16_endianswapper_3bbe29f1__1(.endianswapper_3bbe29f1_in(bus_01b32561_), 
  .endianswapper_3bbe29f1_out(endianswapper_3bbe29f1_out));
endmodule



module medianRow16_receive(CLK, RESET, GO, port_7325c58b_, port_7d0de144_, port_03f079e3_, RESULT, RESULT_u2239, RESULT_u2240, RESULT_u2241, RESULT_u2242, RESULT_u2243, RESULT_u2244, DONE);
input		CLK;
input		RESET;
input		GO;
input	[31:0]	port_7325c58b_;
input		port_7d0de144_;
input	[7:0]	port_03f079e3_;
output		RESULT;
output	[31:0]	RESULT_u2239;
output		RESULT_u2240;
output	[31:0]	RESULT_u2241;
output	[31:0]	RESULT_u2242;
output	[2:0]	RESULT_u2243;
output		RESULT_u2244;
output		DONE;
wire		simplePinWrite;
wire	[31:0]	add;
wire		and_u3823_u0;
wire		or_u1461_u0;
reg		reg_6dcdb067_u0=1'h0;
wire	[31:0]	add_u682;
reg		reg_3b150581_u0=1'h0;
assign simplePinWrite=GO&{1{GO}};
assign add=port_7325c58b_+32'h0;
assign and_u3823_u0=reg_6dcdb067_u0&port_7d0de144_;
assign or_u1461_u0=and_u3823_u0|RESET;
always @(posedge CLK or posedge GO or posedge or_u1461_u0)
begin
if (or_u1461_u0)
reg_6dcdb067_u0<=1'h0;
else if (GO)
reg_6dcdb067_u0<=1'h1;
else reg_6dcdb067_u0<=reg_6dcdb067_u0;
end
assign add_u682=port_7325c58b_+32'h1;
always @(posedge CLK or posedge RESET)
begin
if (RESET)
reg_3b150581_u0<=1'h0;
else reg_3b150581_u0<=GO;
end
assign RESULT=GO;
assign RESULT_u2239=add_u682;
assign RESULT_u2240=GO;
assign RESULT_u2241=add;
assign RESULT_u2242={24'b0, port_03f079e3_};
assign RESULT_u2243=3'h1;
assign RESULT_u2244=simplePinWrite;
assign DONE=reg_3b150581_u0;
endmodule


