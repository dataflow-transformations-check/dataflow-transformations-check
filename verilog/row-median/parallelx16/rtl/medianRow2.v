// __  ___ __ ___  _ __   ___  ___ 
// \ \/ / '__/ _ \| '_ \ / _ \/ __|
//  >  <| | | (_) | | | | (_) \__ \
// /_/\_\_|  \___/|_| |_|\___/|___/
// 
// Xronos synthesizer version
// Run date: Fri 23 Feb 2018 14:28:39 +0000
// 

module medianRow2(in1_ACK, in1_COUNT, median_COUNT, CLK, RESET, median_DATA, in1_DATA, median_SEND, in1_SEND, median_RDY, median_ACK);
wire		compute_median_done;
wire		receive_done;
output		in1_ACK;
input	[15:0]	in1_COUNT;
output	[15:0]	median_COUNT;
input		CLK;
input		RESET;
output	[7:0]	median_DATA;
input	[7:0]	in1_DATA;
output		median_SEND;
input		in1_SEND;
wire		compute_median_go;
wire		receive_go;
input		median_RDY;
input		median_ACK;
wire		medianRow2_compute_median_instance_DONE;
wire	[31:0]	compute_median_u635;
wire	[2:0]	compute_median_u636;
wire		compute_median_u637;
wire	[7:0]	compute_median_u642;
wire		compute_median;
wire	[2:0]	compute_median_u633;
wire		compute_median_u631;
wire		compute_median_u634;
wire	[31:0]	compute_median_u632;
wire	[15:0]	compute_median_u641;
wire		compute_median_u643;
wire	[31:0]	compute_median_u638;
wire	[31:0]	compute_median_u630;
wire	[31:0]	compute_median_u639;
wire	[2:0]	compute_median_u640;
wire	[31:0]	bus_1907e964_;
wire		bus_301022b8_;
wire	[31:0]	bus_04ce9be2_;
wire		bus_26368992_;
wire	[2:0]	bus_2be1189d_;
wire		bus_039f8cd7_;
wire		bus_05044a29_;
wire	[31:0]	bus_38c1df49_;
wire	[31:0]	bus_031b07d3_;
wire		bus_3cb5479c_;
wire	[31:0]	bus_083410c7_;
wire		bus_08703637_;
wire		bus_7882e1f9_;
wire		bus_7362593c_;
wire		medianRow2_scheduler_instance_DONE;
wire		scheduler_u813;
wire		scheduler;
wire		scheduler_u812;
wire		scheduler_u811;
wire		bus_605818f0_;
wire		receive;
wire		receive_u271;
wire		receive_u275;
wire	[31:0]	receive_u270;
wire	[31:0]	receive_u273;
wire		medianRow2_receive_instance_DONE;
wire	[31:0]	receive_u272;
wire	[2:0]	receive_u274;
wire	[31:0]	bus_0b237537_;
wire		bus_749f14bd_;
wire	[31:0]	bus_12e4289b_;
wire		bus_0b6dba37_;
wire	[2:0]	bus_7b837a32_;
wire		bus_79e15c10_;
wire		bus_227a1de5_;
wire	[31:0]	bus_582cec80_;
wire	[31:0]	bus_293a0ea1_;
wire		bus_7a17e071_;
assign compute_median_done=bus_7882e1f9_;
assign receive_done=bus_7362593c_;
assign in1_ACK=receive_u275;
assign median_COUNT=compute_median_u641;
assign median_DATA=compute_median_u642;
assign median_SEND=compute_median_u643;
assign compute_median_go=scheduler_u813;
assign receive_go=scheduler_u812;
medianRow2_compute_median medianRow2_compute_median_instance(.CLK(CLK), .RESET(bus_7a17e071_), 
  .GO(compute_median_go), .port_0dd45bfe_(bus_749f14bd_), .port_7ed8e015_(bus_12e4289b_), 
  .port_413915ba_(bus_039f8cd7_), .port_094f5baa_(bus_38c1df49_), .port_436c0a58_(bus_749f14bd_), 
  .DONE(medianRow2_compute_median_instance_DONE), .RESULT(compute_median), .RESULT_u2199(compute_median_u630), 
  .RESULT_u2203(compute_median_u631), .RESULT_u2204(compute_median_u632), .RESULT_u2205(compute_median_u633), 
  .RESULT_u2200(compute_median_u634), .RESULT_u2201(compute_median_u635), .RESULT_u2202(compute_median_u636), 
  .RESULT_u2206(compute_median_u637), .RESULT_u2207(compute_median_u638), .RESULT_u2208(compute_median_u639), 
  .RESULT_u2209(compute_median_u640), .RESULT_u2210(compute_median_u641), .RESULT_u2211(compute_median_u642), 
  .RESULT_u2212(compute_median_u643));
medianRow2_structuralmemory_0aed58e8_ medianRow2_structuralmemory_0aed58e8__1(.CLK_u91(CLK), 
  .bus_6df58784_(bus_7a17e071_), .bus_2ab86133_(bus_083410c7_), .bus_406898fc_(3'h1), 
  .bus_18a3dbb5_(bus_05044a29_), .bus_319ddac6_(bus_293a0ea1_), .bus_0fc82b7a_(3'h1), 
  .bus_47ce1462_(bus_79e15c10_), .bus_30ece563_(bus_0b6dba37_), .bus_15545edf_(bus_582cec80_), 
  .bus_1907e964_(bus_1907e964_), .bus_301022b8_(bus_301022b8_), .bus_04ce9be2_(bus_04ce9be2_), 
  .bus_26368992_(bus_26368992_));
medianRow2_simplememoryreferee_58e8a8fe_ medianRow2_simplememoryreferee_58e8a8fe__1(.bus_15de3e13_(CLK), 
  .bus_6ca1e275_(bus_7a17e071_), .bus_744ce090_(bus_301022b8_), .bus_4a14b098_(bus_1907e964_), 
  .bus_1b47094c_(compute_median_u634), .bus_2ee109b3_(compute_median_u635), .bus_75473844_(3'h1), 
  .bus_031b07d3_(bus_031b07d3_), .bus_083410c7_(bus_083410c7_), .bus_3cb5479c_(bus_3cb5479c_), 
  .bus_05044a29_(bus_05044a29_), .bus_2be1189d_(bus_2be1189d_), .bus_38c1df49_(bus_38c1df49_), 
  .bus_039f8cd7_(bus_039f8cd7_));
medianRow2_Kicker_57 medianRow2_Kicker_57_1(.CLK(CLK), .RESET(bus_7a17e071_), .bus_08703637_(bus_08703637_));
assign bus_7882e1f9_=medianRow2_compute_median_instance_DONE&{1{medianRow2_compute_median_instance_DONE}};
assign bus_7362593c_=medianRow2_receive_instance_DONE&{1{medianRow2_receive_instance_DONE}};
medianRow2_scheduler medianRow2_scheduler_instance(.CLK(CLK), .RESET(bus_7a17e071_), 
  .GO(bus_08703637_), .port_5b110a64_(bus_605818f0_), .port_373611a4_(bus_0b237537_), 
  .port_5c13afd1_(receive_done), .port_78856155_(compute_median_done), .port_659b6104_(median_RDY), 
  .port_413ea20c_(in1_SEND), .DONE(medianRow2_scheduler_instance_DONE), .RESULT(scheduler), 
  .RESULT_u2213(scheduler_u811), .RESULT_u2214(scheduler_u812), .RESULT_u2215(scheduler_u813));
medianRow2_stateVar_fsmState_medianRow2 medianRow2_stateVar_fsmState_medianRow2_1(.bus_5081a752_(CLK), 
  .bus_78b59679_(bus_7a17e071_), .bus_35ddba70_(scheduler), .bus_3c69759e_(scheduler_u811), 
  .bus_605818f0_(bus_605818f0_));
medianRow2_receive medianRow2_receive_instance(.CLK(CLK), .RESET(bus_7a17e071_), 
  .GO(receive_go), .port_2a180d5f_(bus_0b237537_), .port_611f60f5_(bus_227a1de5_), 
  .port_71d2ad28_(in1_DATA), .DONE(medianRow2_receive_instance_DONE), .RESULT(receive), 
  .RESULT_u2216(receive_u270), .RESULT_u2217(receive_u271), .RESULT_u2218(receive_u272), 
  .RESULT_u2219(receive_u273), .RESULT_u2220(receive_u274), .RESULT_u2221(receive_u275));
medianRow2_stateVar_i medianRow2_stateVar_i_1(.bus_4a1b0f2c_(CLK), .bus_3f675d65_(bus_7a17e071_), 
  .bus_50471737_(receive), .bus_47bef13a_(receive_u270), .bus_2ff4dce4_(compute_median), 
  .bus_653d013a_(32'h0), .bus_0b237537_(bus_0b237537_));
medianRow2_simplememoryreferee_546e01d8_ medianRow2_simplememoryreferee_546e01d8__1(.bus_5439b6dd_(CLK), 
  .bus_37cea00b_(bus_7a17e071_), .bus_739e5f32_(bus_26368992_), .bus_68cebe70_(bus_04ce9be2_), 
  .bus_6384f2ab_(receive_u271), .bus_4baff515_({24'b0, receive_u273[7:0]}), .bus_22d9c211_(receive_u272), 
  .bus_27dd4632_(3'h1), .bus_6083945b_(compute_median_u631), .bus_644153d3_(compute_median_u637), 
  .bus_065ccafe_(compute_median_u639), .bus_1166b695_(compute_median_u638), .bus_36e1bb03_(3'h1), 
  .bus_582cec80_(bus_582cec80_), .bus_293a0ea1_(bus_293a0ea1_), .bus_0b6dba37_(bus_0b6dba37_), 
  .bus_79e15c10_(bus_79e15c10_), .bus_7b837a32_(bus_7b837a32_), .bus_227a1de5_(bus_227a1de5_), 
  .bus_12e4289b_(bus_12e4289b_), .bus_749f14bd_(bus_749f14bd_));
medianRow2_globalreset_physical_047a1b81_ medianRow2_globalreset_physical_047a1b81__1(.bus_01f77c28_(CLK), 
  .bus_58c6d4a8_(RESET), .bus_7a17e071_(bus_7a17e071_));
endmodule



module medianRow2_compute_median(CLK, RESET, GO, port_413915ba_, port_094f5baa_, port_0dd45bfe_, port_7ed8e015_, port_436c0a58_, RESULT, RESULT_u2199, RESULT_u2200, RESULT_u2201, RESULT_u2202, RESULT_u2203, RESULT_u2204, RESULT_u2205, RESULT_u2206, RESULT_u2207, RESULT_u2208, RESULT_u2209, RESULT_u2210, RESULT_u2211, RESULT_u2212, DONE);
input		CLK;
input		RESET;
input		GO;
input		port_413915ba_;
input	[31:0]	port_094f5baa_;
input		port_0dd45bfe_;
input	[31:0]	port_7ed8e015_;
input		port_436c0a58_;
output		RESULT;
output	[31:0]	RESULT_u2199;
output		RESULT_u2200;
output	[31:0]	RESULT_u2201;
output	[2:0]	RESULT_u2202;
output		RESULT_u2203;
output	[31:0]	RESULT_u2204;
output	[2:0]	RESULT_u2205;
output		RESULT_u2206;
output	[31:0]	RESULT_u2207;
output	[31:0]	RESULT_u2208;
output	[2:0]	RESULT_u2209;
output	[15:0]	RESULT_u2210;
output	[7:0]	RESULT_u2211;
output		RESULT_u2212;
output		DONE;
wire	[15:0]	lessThan_a_unsigned;
wire		lessThan;
wire	[15:0]	lessThan_b_unsigned;
wire		andOp;
wire		not_u873_u0;
wire		and_u3740_u0;
wire		and_u3741_u0;
wire		lessThan_u103;
wire signed	[32:0]	lessThan_u103_a_signed;
wire signed	[32:0]	lessThan_u103_b_signed;
wire		and_u3742_u0;
wire		and_u3743_u0;
wire		not_u874_u0;
wire		and_u3744_u0;
reg	[31:0]	syncEnable_u1405=32'h0;
reg	[31:0]	syncEnable_u1406_u0=32'h0;
wire	[31:0]	add;
wire		and_u3745_u0;
wire	[31:0]	add_u661;
wire	[31:0]	add_u662;
wire		and_u3746_u0;
wire signed	[31:0]	greaterThan_a_signed;
wire signed	[31:0]	greaterThan_b_signed;
wire		greaterThan;
wire		and_u3747_u0;
wire		not_u875_u0;
wire		and_u3748_u0;
wire	[31:0]	add_u663;
wire		and_u3749_u0;
wire	[31:0]	add_u664;
wire	[31:0]	add_u665;
wire		and_u3750_u0;
wire	[31:0]	add_u666;
wire		and_u3751_u0;
wire		or_u1426_u0;
reg		reg_0c685e95_u0=1'h0;
wire	[31:0]	add_u667;
wire	[31:0]	add_u668;
reg		reg_2b643040_u0=1'h0;
wire		or_u1427_u0;
wire		and_u3752_u0;
reg	[31:0]	syncEnable_u1407_u0=32'h0;
reg	[31:0]	syncEnable_u1408_u0=32'h0;
wire		or_u1428_u0;
wire	[31:0]	mux_u1889;
wire	[31:0]	mux_u1890_u0;
reg	[31:0]	syncEnable_u1409_u0=32'h0;
reg	[31:0]	syncEnable_u1410_u0=32'h0;
reg	[31:0]	syncEnable_u1411_u0=32'h0;
reg		block_GO_delayed_u96=1'h0;
reg	[31:0]	syncEnable_u1412_u0=32'h0;
reg		block_GO_delayed_result_delayed_u18=1'h0;
wire		mux_u1891_u0;
reg		reg_1a39e81c_u0=1'h0;
wire		and_u3753_u0;
wire	[31:0]	mux_u1892_u0;
reg		syncEnable_u1413_u0=1'h0;
reg		reg_7cd585d9_u0=1'h0;
reg		and_delayed_u387=1'h0;
reg		reg_7cd585d9_result_delayed_u0=1'h0;
reg		reg_4839da2f_u0=1'h0;
reg	[31:0]	syncEnable_u1414_u0=32'h0;
wire		and_u3754_u0;
reg	[31:0]	syncEnable_u1415_u0=32'h0;
wire		or_u1429_u0;
wire	[31:0]	mux_u1893_u0;
wire	[31:0]	add_u669;
reg		syncEnable_u1416_u0=1'h0;
reg	[31:0]	syncEnable_u1417_u0=32'h0;
reg	[31:0]	syncEnable_u1418_u0=32'h0;
reg	[31:0]	syncEnable_u1419_u0=32'h0;
reg		block_GO_delayed_u97_u0=1'h0;
wire		or_u1430_u0;
wire	[31:0]	mux_u1894_u0;
wire		or_u1431_u0;
wire	[31:0]	mux_u1895_u0;
reg	[31:0]	syncEnable_u1420_u0=32'h0;
reg	[31:0]	syncEnable_u1421_u0=32'h0;
reg	[31:0]	syncEnable_u1422_u0=32'h0;
wire		or_u1432_u0;
wire	[31:0]	mux_u1896_u0;
wire	[31:0]	mux_u1897_u0;
wire	[31:0]	mux_u1898_u0;
wire	[31:0]	mux_u1899_u0;
wire	[31:0]	mux_u1900_u0;
wire		mux_u1901_u0;
wire	[15:0]	add_u670;
reg		reg_5ce7c32d_u0=1'h0;
wire	[31:0]	latch_076328e9_out;
reg	[31:0]	latch_076328e9_reg=32'h0;
reg	[15:0]	syncEnable_u1423_u0=16'h0;
reg	[31:0]	latch_05210cb6_reg=32'h0;
wire	[31:0]	latch_05210cb6_out;
reg		latch_69577fc0_reg=1'h0;
wire		latch_69577fc0_out;
wire	[31:0]	latch_213b6442_out;
reg	[31:0]	latch_213b6442_reg=32'h0;
wire		bus_72e8f01d_;
wire		scoreboard_6eee8f54_and;
reg		scoreboard_6eee8f54_reg1=1'h0;
wire		scoreboard_6eee8f54_resOr1;
reg		scoreboard_6eee8f54_reg0=1'h0;
wire		scoreboard_6eee8f54_resOr0;
wire	[31:0]	latch_165b13c5_out;
reg	[31:0]	latch_165b13c5_reg=32'h0;
wire		and_u3755_u0;
wire		and_u3756_u0;
reg	[31:0]	fbReg_tmp_row1_u45=32'h0;
reg		loopControl_u94=1'h0;
wire		mux_u1902_u0;
wire	[31:0]	mux_u1903_u0;
wire	[31:0]	mux_u1904_u0;
reg	[31:0]	fbReg_tmp_row_u45=32'h0;
reg	[15:0]	fbReg_idx_u45=16'h0;
reg		syncEnable_u1424_u0=1'h0;
wire	[31:0]	mux_u1905_u0;
reg	[31:0]	fbReg_tmp_row0_u45=32'h0;
wire		or_u1433_u0;
wire	[15:0]	mux_u1906_u0;
wire	[31:0]	mux_u1907_u0;
reg		fbReg_swapped_u45=1'h0;
reg	[31:0]	fbReg_tmp_u45=32'h0;
wire		and_u3757_u0;
wire	[7:0]	simplePinWrite;
wire		simplePinWrite_u618;
wire	[15:0]	simplePinWrite_u619;
reg		reg_043bb27d_u0=1'h0;
reg		reg_3dba3964_u0=1'h0;
wire	[31:0]	mux_u1908_u0;
wire		or_u1434_u0;
assign lessThan_a_unsigned=mux_u1906_u0;
assign lessThan_b_unsigned=16'h65;
assign lessThan=lessThan_a_unsigned<lessThan_b_unsigned;
assign andOp=lessThan&mux_u1902_u0;
assign not_u873_u0=~andOp;
assign and_u3740_u0=or_u1433_u0&andOp;
assign and_u3741_u0=or_u1433_u0&not_u873_u0;
assign lessThan_u103_a_signed={1'b0, mux_u1896_u0};
assign lessThan_u103_b_signed=33'h64;
assign lessThan_u103=lessThan_u103_a_signed<lessThan_u103_b_signed;
assign and_u3742_u0=or_u1432_u0&not_u874_u0;
assign and_u3743_u0=or_u1432_u0&lessThan_u103;
assign not_u874_u0=~lessThan_u103;
assign and_u3744_u0=and_u3743_u0&or_u1432_u0;
always @(posedge CLK)
begin
if (or_u1432_u0)
syncEnable_u1405<=mux_u1900_u0;
end
always @(posedge CLK)
begin
if (or_u1432_u0)
syncEnable_u1406_u0<=mux_u1898_u0;
end
assign add=mux_u1896_u0+32'h0;
assign and_u3745_u0=and_u3744_u0&port_413915ba_;
assign add_u661=mux_u1896_u0+32'h1;
assign add_u662=add_u661+32'h0;
assign and_u3746_u0=and_u3744_u0&port_436c0a58_;
assign greaterThan_a_signed=syncEnable_u1418_u0;
assign greaterThan_b_signed=syncEnable_u1422_u0;
assign greaterThan=greaterThan_a_signed>greaterThan_b_signed;
assign and_u3747_u0=block_GO_delayed_u97_u0&not_u875_u0;
assign not_u875_u0=~greaterThan;
assign and_u3748_u0=block_GO_delayed_u97_u0&greaterThan;
assign add_u663=syncEnable_u1421_u0+32'h0;
assign and_u3749_u0=and_u3754_u0&port_413915ba_;
assign add_u664=syncEnable_u1421_u0+32'h1;
assign add_u665=add_u664+32'h0;
assign and_u3750_u0=and_u3754_u0&port_436c0a58_;
assign add_u666=syncEnable_u1421_u0+32'h0;
assign and_u3751_u0=reg_0c685e95_u0&port_436c0a58_;
assign or_u1426_u0=and_u3751_u0|RESET;
always @(posedge CLK or posedge block_GO_delayed_u96 or posedge or_u1426_u0)
begin
if (or_u1426_u0)
reg_0c685e95_u0<=1'h0;
else if (block_GO_delayed_u96)
reg_0c685e95_u0<=1'h1;
else reg_0c685e95_u0<=reg_0c685e95_u0;
end
assign add_u667=syncEnable_u1421_u0+32'h1;
assign add_u668=add_u667+32'h0;
always @(posedge CLK or posedge block_GO_delayed_result_delayed_u18 or posedge or_u1427_u0)
begin
if (or_u1427_u0)
reg_2b643040_u0<=1'h0;
else if (block_GO_delayed_result_delayed_u18)
reg_2b643040_u0<=1'h1;
else reg_2b643040_u0<=reg_2b643040_u0;
end
assign or_u1427_u0=and_u3752_u0|RESET;
assign and_u3752_u0=reg_2b643040_u0&port_436c0a58_;
always @(posedge CLK)
begin
if (and_u3754_u0)
syncEnable_u1407_u0<=port_094f5baa_;
end
always @(posedge CLK)
begin
if (and_u3754_u0)
syncEnable_u1408_u0<=add_u666;
end
assign or_u1428_u0=block_GO_delayed_u96|block_GO_delayed_result_delayed_u18;
assign mux_u1889=({32{block_GO_delayed_u96}}&syncEnable_u1408_u0)|({32{block_GO_delayed_result_delayed_u18}}&syncEnable_u1412_u0)|({32{and_u3754_u0}}&add_u665);
assign mux_u1890_u0=(block_GO_delayed_u96)?syncEnable_u1411_u0:syncEnable_u1409_u0;
always @(posedge CLK)
begin
if (and_u3754_u0)
syncEnable_u1409_u0<=port_094f5baa_;
end
always @(posedge CLK)
begin
if (and_u3754_u0)
syncEnable_u1410_u0<=port_7ed8e015_;
end
always @(posedge CLK)
begin
if (and_u3754_u0)
syncEnable_u1411_u0<=port_7ed8e015_;
end
always @(posedge CLK or posedge RESET)
begin
if (RESET)
block_GO_delayed_u96<=1'h0;
else block_GO_delayed_u96<=and_u3754_u0;
end
always @(posedge CLK)
begin
if (and_u3754_u0)
syncEnable_u1412_u0<=add_u668;
end
always @(posedge CLK or posedge RESET)
begin
if (RESET)
block_GO_delayed_result_delayed_u18<=1'h0;
else block_GO_delayed_result_delayed_u18<=block_GO_delayed_u96;
end
assign mux_u1891_u0=(reg_1a39e81c_u0)?1'h1:syncEnable_u1413_u0;
always @(posedge CLK or posedge RESET)
begin
if (RESET)
reg_1a39e81c_u0<=1'h0;
else reg_1a39e81c_u0<=reg_7cd585d9_result_delayed_u0;
end
assign and_u3753_u0=and_u3747_u0&block_GO_delayed_u97_u0;
assign mux_u1892_u0=(reg_1a39e81c_u0)?syncEnable_u1407_u0:syncEnable_u1414_u0;
always @(posedge CLK)
begin
if (block_GO_delayed_u97_u0)
syncEnable_u1413_u0<=syncEnable_u1416_u0;
end
always @(posedge CLK or posedge RESET)
begin
if (RESET)
reg_7cd585d9_u0<=1'h0;
else reg_7cd585d9_u0<=reg_4839da2f_u0;
end
always @(posedge CLK or posedge RESET)
begin
if (RESET)
and_delayed_u387<=1'h0;
else and_delayed_u387<=and_u3753_u0;
end
always @(posedge CLK or posedge RESET)
begin
if (RESET)
reg_7cd585d9_result_delayed_u0<=1'h0;
else reg_7cd585d9_result_delayed_u0<=reg_7cd585d9_u0;
end
always @(posedge CLK or posedge RESET)
begin
if (RESET)
reg_4839da2f_u0<=1'h0;
else reg_4839da2f_u0<=and_u3754_u0;
end
always @(posedge CLK)
begin
if (block_GO_delayed_u97_u0)
syncEnable_u1414_u0<=syncEnable_u1420_u0;
end
assign and_u3754_u0=and_u3748_u0&block_GO_delayed_u97_u0;
always @(posedge CLK)
begin
if (block_GO_delayed_u97_u0)
syncEnable_u1415_u0<=syncEnable_u1417_u0;
end
assign or_u1429_u0=reg_1a39e81c_u0|and_delayed_u387;
assign mux_u1893_u0=(reg_1a39e81c_u0)?syncEnable_u1410_u0:syncEnable_u1415_u0;
assign add_u669=mux_u1896_u0+32'h1;
always @(posedge CLK)
begin
if (and_u3744_u0)
syncEnable_u1416_u0<=mux_u1901_u0;
end
always @(posedge CLK)
begin
if (and_u3744_u0)
syncEnable_u1417_u0<=mux_u1897_u0;
end
always @(posedge CLK)
begin
if (and_u3744_u0)
syncEnable_u1418_u0<=port_094f5baa_;
end
always @(posedge CLK)
begin
if (and_u3744_u0)
syncEnable_u1419_u0<=add_u669;
end
always @(posedge CLK or posedge RESET)
begin
if (RESET)
block_GO_delayed_u97_u0<=1'h0;
else block_GO_delayed_u97_u0<=and_u3744_u0;
end
assign or_u1430_u0=and_u3744_u0|and_u3754_u0;
assign mux_u1894_u0=(and_u3744_u0)?add:add_u663;
assign or_u1431_u0=and_u3744_u0|and_u3754_u0;
assign mux_u1895_u0=({32{or_u1428_u0}}&mux_u1889)|({32{and_u3744_u0}}&add_u662)|({32{and_u3754_u0}}&mux_u1889);
always @(posedge CLK)
begin
if (and_u3744_u0)
syncEnable_u1420_u0<=mux_u1900_u0;
end
always @(posedge CLK)
begin
if (and_u3744_u0)
syncEnable_u1421_u0<=mux_u1896_u0;
end
always @(posedge CLK)
begin
if (and_u3744_u0)
syncEnable_u1422_u0<=port_7ed8e015_;
end
assign or_u1432_u0=and_u3755_u0|or_u1429_u0;
assign mux_u1896_u0=(and_u3755_u0)?32'h0:syncEnable_u1419_u0;
assign mux_u1897_u0=(and_u3755_u0)?mux_u1904_u0:mux_u1893_u0;
assign mux_u1898_u0=(and_u3755_u0)?mux_u1905_u0:syncEnable_u1418_u0;
assign mux_u1899_u0=(and_u3755_u0)?mux_u1907_u0:syncEnable_u1422_u0;
assign mux_u1900_u0=(and_u3755_u0)?mux_u1903_u0:mux_u1892_u0;
assign mux_u1901_u0=(and_u3755_u0)?1'h0:mux_u1891_u0;
assign add_u670=mux_u1906_u0+16'h1;
always @(posedge CLK or posedge RESET)
begin
if (RESET)
reg_5ce7c32d_u0<=1'h0;
else reg_5ce7c32d_u0<=or_u1429_u0;
end
assign latch_076328e9_out=(or_u1429_u0)?syncEnable_u1406_u0:latch_076328e9_reg;
always @(posedge CLK)
begin
if (or_u1429_u0)
latch_076328e9_reg<=syncEnable_u1406_u0;
end
always @(posedge CLK)
begin
if (and_u3755_u0)
syncEnable_u1423_u0<=add_u670;
end
always @(posedge CLK)
begin
if (or_u1429_u0)
latch_05210cb6_reg<=mux_u1893_u0;
end
assign latch_05210cb6_out=(or_u1429_u0)?mux_u1893_u0:latch_05210cb6_reg;
always @(posedge CLK)
begin
if (or_u1429_u0)
latch_69577fc0_reg<=mux_u1891_u0;
end
assign latch_69577fc0_out=(or_u1429_u0)?mux_u1891_u0:latch_69577fc0_reg;
assign latch_213b6442_out=(or_u1429_u0)?syncEnable_u1422_u0:latch_213b6442_reg;
always @(posedge CLK)
begin
if (or_u1429_u0)
latch_213b6442_reg<=syncEnable_u1422_u0;
end
assign bus_72e8f01d_=scoreboard_6eee8f54_and|RESET;
assign scoreboard_6eee8f54_and=scoreboard_6eee8f54_resOr0&scoreboard_6eee8f54_resOr1;
always @(posedge CLK)
begin
if (bus_72e8f01d_)
scoreboard_6eee8f54_reg1<=1'h0;
else if (reg_5ce7c32d_u0)
scoreboard_6eee8f54_reg1<=1'h1;
else scoreboard_6eee8f54_reg1<=scoreboard_6eee8f54_reg1;
end
assign scoreboard_6eee8f54_resOr1=reg_5ce7c32d_u0|scoreboard_6eee8f54_reg1;
always @(posedge CLK)
begin
if (bus_72e8f01d_)
scoreboard_6eee8f54_reg0<=1'h0;
else if (or_u1429_u0)
scoreboard_6eee8f54_reg0<=1'h1;
else scoreboard_6eee8f54_reg0<=scoreboard_6eee8f54_reg0;
end
assign scoreboard_6eee8f54_resOr0=or_u1429_u0|scoreboard_6eee8f54_reg0;
assign latch_165b13c5_out=(or_u1429_u0)?syncEnable_u1405:latch_165b13c5_reg;
always @(posedge CLK)
begin
if (or_u1429_u0)
latch_165b13c5_reg<=syncEnable_u1405;
end
assign and_u3755_u0=and_u3740_u0&or_u1433_u0;
assign and_u3756_u0=and_u3741_u0&or_u1433_u0;
always @(posedge CLK)
begin
if (scoreboard_6eee8f54_and)
fbReg_tmp_row1_u45<=latch_05210cb6_out;
end
always @(posedge CLK or posedge syncEnable_u1424_u0)
begin
if (syncEnable_u1424_u0)
loopControl_u94<=1'h0;
else loopControl_u94<=scoreboard_6eee8f54_and;
end
assign mux_u1902_u0=(GO)?1'h0:fbReg_swapped_u45;
assign mux_u1903_u0=(GO)?32'h0:fbReg_tmp_u45;
assign mux_u1904_u0=(GO)?32'h0:fbReg_tmp_row1_u45;
always @(posedge CLK)
begin
if (scoreboard_6eee8f54_and)
fbReg_tmp_row_u45<=latch_076328e9_out;
end
always @(posedge CLK)
begin
if (scoreboard_6eee8f54_and)
fbReg_idx_u45<=syncEnable_u1423_u0;
end
always @(posedge CLK)
begin
if (GO)
syncEnable_u1424_u0<=RESET;
end
assign mux_u1905_u0=(GO)?32'h0:fbReg_tmp_row_u45;
always @(posedge CLK)
begin
if (scoreboard_6eee8f54_and)
fbReg_tmp_row0_u45<=latch_213b6442_out;
end
assign or_u1433_u0=GO|loopControl_u94;
assign mux_u1906_u0=(GO)?16'h0:fbReg_idx_u45;
assign mux_u1907_u0=(GO)?32'h0:fbReg_tmp_row0_u45;
always @(posedge CLK)
begin
if (scoreboard_6eee8f54_and)
fbReg_swapped_u45<=latch_69577fc0_out;
end
always @(posedge CLK)
begin
if (scoreboard_6eee8f54_and)
fbReg_tmp_u45<=latch_165b13c5_out;
end
assign and_u3757_u0=reg_3dba3964_u0&port_413915ba_;
assign simplePinWrite=port_094f5baa_[7:0];
assign simplePinWrite_u618=reg_3dba3964_u0&{1{reg_3dba3964_u0}};
assign simplePinWrite_u619=16'h1&{16{1'h1}};
always @(posedge CLK or posedge RESET)
begin
if (RESET)
reg_043bb27d_u0<=1'h0;
else reg_043bb27d_u0<=reg_3dba3964_u0;
end
always @(posedge CLK or posedge RESET)
begin
if (RESET)
reg_3dba3964_u0<=1'h0;
else reg_3dba3964_u0<=and_u3756_u0;
end
assign mux_u1908_u0=(or_u1430_u0)?mux_u1894_u0:32'h32;
assign or_u1434_u0=or_u1430_u0|reg_3dba3964_u0;
assign RESULT=GO;
assign RESULT_u2199=32'h0;
assign RESULT_u2200=or_u1434_u0;
assign RESULT_u2201=mux_u1908_u0;
assign RESULT_u2202=3'h1;
assign RESULT_u2203=or_u1431_u0;
assign RESULT_u2204=mux_u1895_u0;
assign RESULT_u2205=3'h1;
assign RESULT_u2206=or_u1428_u0;
assign RESULT_u2207=mux_u1895_u0;
assign RESULT_u2208=mux_u1890_u0;
assign RESULT_u2209=3'h1;
assign RESULT_u2210=simplePinWrite_u619;
assign RESULT_u2211=simplePinWrite;
assign RESULT_u2212=simplePinWrite_u618;
assign DONE=reg_043bb27d_u0;
endmodule



module medianRow2_forge_memory_101x32_143(CLK, ENA, WEA, DINA, ENB, ADDRA, ADDRB, DOUTA, DOUTB, DONEA, DONEB);
input		CLK;
input		ENA;
input		WEA;
input	[31:0]	DINA;
input		ENB;
input	[31:0]	ADDRA;
input	[31:0]	ADDRB;
output	[31:0]	DOUTA;
output	[31:0]	DOUTB;
output		DONEA;
output		DONEB;
wire		wea_0;
wire	[31:0]	pre_douta_0;
wire	[31:0]	pre_doutb_0;
wire		wea_1;
wire	[31:0]	pre_douta_1;
wire	[31:0]	pre_doutb_1;
reg		wea_done;
reg		rea_done;
reg	[31:0]	mux_outa;
reg	[31:0]	mux_outb;
assign wea_0=WEA&(ADDRA[31:6]==26'h0);
assign wea_1=WEA&(ADDRA[31:6]==26'h1);
always @(posedge CLK)
begin
wea_done<=WEA;
end
assign DONEA=rea_done|wea_done;
always @(posedge CLK)
begin
end
assign DONEB=ENB;
always @(pre_douta_0 or pre_douta_1 or ADDRA)
begin
case (ADDRA[31:6])26'd0:mux_outa=pre_douta_0;
26'd1:mux_outa=pre_douta_1;
default:mux_outa=32'h0;
endcase end
assign DOUTA=mux_outa;
always @(pre_doutb_0 or pre_doutb_1 or ADDRB)
begin
case (ADDRB[31:6])26'd0:mux_outb=pre_doutb_0;
26'd1:mux_outb=pre_doutb_1;
default:mux_outb=32'h0;
endcase end
assign DOUTB=mux_outb;
// Memory array element: COL: 0, ROW: 0
//  Initialization of Dual Port LUT ram now done through explicit
// parameter setting.
RAM64X1D#(.INIT(64'h0000000000000000))RAM64X1D_instance_2880(.WCLK(CLK), .WE(wea_0), 
  .A0(ADDRA[0]), .A1(ADDRA[1]), .A2(ADDRA[2]), .A3(ADDRA[3]), .A4(ADDRA[4]), .A5(ADDRA[5]), 
  .D(DINA[0]), .SPO(pre_douta_0[0]), .DPRA0(ADDRB[0]), .DPRA1(ADDRB[1]), .DPRA2(ADDRB[2]), 
  .DPRA3(ADDRB[3]), .DPRA4(ADDRB[4]), .DPRA5(ADDRB[5]), .DPO(pre_doutb_0[0]));
// Memory array element: COL: 0, ROW: 1
//  Initialization of Dual Port LUT ram now done through explicit
// parameter setting.
RAM64X1D#(.INIT(64'h0000000000000000))RAM64X1D_instance_2881(.WCLK(CLK), .WE(wea_0), 
  .A0(ADDRA[0]), .A1(ADDRA[1]), .A2(ADDRA[2]), .A3(ADDRA[3]), .A4(ADDRA[4]), .A5(ADDRA[5]), 
  .D(DINA[1]), .SPO(pre_douta_0[1]), .DPRA0(ADDRB[0]), .DPRA1(ADDRB[1]), .DPRA2(ADDRB[2]), 
  .DPRA3(ADDRB[3]), .DPRA4(ADDRB[4]), .DPRA5(ADDRB[5]), .DPO(pre_doutb_0[1]));
// Memory array element: COL: 0, ROW: 2
//  Initialization of Dual Port LUT ram now done through explicit
// parameter setting.
RAM64X1D#(.INIT(64'h0000000000000000))RAM64X1D_instance_2882(.WCLK(CLK), .WE(wea_0), 
  .A0(ADDRA[0]), .A1(ADDRA[1]), .A2(ADDRA[2]), .A3(ADDRA[3]), .A4(ADDRA[4]), .A5(ADDRA[5]), 
  .D(DINA[2]), .SPO(pre_douta_0[2]), .DPRA0(ADDRB[0]), .DPRA1(ADDRB[1]), .DPRA2(ADDRB[2]), 
  .DPRA3(ADDRB[3]), .DPRA4(ADDRB[4]), .DPRA5(ADDRB[5]), .DPO(pre_doutb_0[2]));
// Memory array element: COL: 0, ROW: 3
//  Initialization of Dual Port LUT ram now done through explicit
// parameter setting.
RAM64X1D#(.INIT(64'h0000000000000000))RAM64X1D_instance_2883(.WCLK(CLK), .WE(wea_0), 
  .A0(ADDRA[0]), .A1(ADDRA[1]), .A2(ADDRA[2]), .A3(ADDRA[3]), .A4(ADDRA[4]), .A5(ADDRA[5]), 
  .D(DINA[3]), .SPO(pre_douta_0[3]), .DPRA0(ADDRB[0]), .DPRA1(ADDRB[1]), .DPRA2(ADDRB[2]), 
  .DPRA3(ADDRB[3]), .DPRA4(ADDRB[4]), .DPRA5(ADDRB[5]), .DPO(pre_doutb_0[3]));
// Memory array element: COL: 0, ROW: 4
//  Initialization of Dual Port LUT ram now done through explicit
// parameter setting.
RAM64X1D#(.INIT(64'h0000000000000000))RAM64X1D_instance_2884(.WCLK(CLK), .WE(wea_0), 
  .A0(ADDRA[0]), .A1(ADDRA[1]), .A2(ADDRA[2]), .A3(ADDRA[3]), .A4(ADDRA[4]), .A5(ADDRA[5]), 
  .D(DINA[4]), .SPO(pre_douta_0[4]), .DPRA0(ADDRB[0]), .DPRA1(ADDRB[1]), .DPRA2(ADDRB[2]), 
  .DPRA3(ADDRB[3]), .DPRA4(ADDRB[4]), .DPRA5(ADDRB[5]), .DPO(pre_doutb_0[4]));
// Memory array element: COL: 0, ROW: 5
//  Initialization of Dual Port LUT ram now done through explicit
// parameter setting.
RAM64X1D#(.INIT(64'h0000000000000000))RAM64X1D_instance_2885(.WCLK(CLK), .WE(wea_0), 
  .A0(ADDRA[0]), .A1(ADDRA[1]), .A2(ADDRA[2]), .A3(ADDRA[3]), .A4(ADDRA[4]), .A5(ADDRA[5]), 
  .D(DINA[5]), .SPO(pre_douta_0[5]), .DPRA0(ADDRB[0]), .DPRA1(ADDRB[1]), .DPRA2(ADDRB[2]), 
  .DPRA3(ADDRB[3]), .DPRA4(ADDRB[4]), .DPRA5(ADDRB[5]), .DPO(pre_doutb_0[5]));
// Memory array element: COL: 0, ROW: 6
//  Initialization of Dual Port LUT ram now done through explicit
// parameter setting.
RAM64X1D#(.INIT(64'h0000000000000000))RAM64X1D_instance_2886(.WCLK(CLK), .WE(wea_0), 
  .A0(ADDRA[0]), .A1(ADDRA[1]), .A2(ADDRA[2]), .A3(ADDRA[3]), .A4(ADDRA[4]), .A5(ADDRA[5]), 
  .D(DINA[6]), .SPO(pre_douta_0[6]), .DPRA0(ADDRB[0]), .DPRA1(ADDRB[1]), .DPRA2(ADDRB[2]), 
  .DPRA3(ADDRB[3]), .DPRA4(ADDRB[4]), .DPRA5(ADDRB[5]), .DPO(pre_doutb_0[6]));
// Memory array element: COL: 0, ROW: 7
//  Initialization of Dual Port LUT ram now done through explicit
// parameter setting.
RAM64X1D#(.INIT(64'h0000000000000000))RAM64X1D_instance_2887(.WCLK(CLK), .WE(wea_0), 
  .A0(ADDRA[0]), .A1(ADDRA[1]), .A2(ADDRA[2]), .A3(ADDRA[3]), .A4(ADDRA[4]), .A5(ADDRA[5]), 
  .D(DINA[7]), .SPO(pre_douta_0[7]), .DPRA0(ADDRB[0]), .DPRA1(ADDRB[1]), .DPRA2(ADDRB[2]), 
  .DPRA3(ADDRB[3]), .DPRA4(ADDRB[4]), .DPRA5(ADDRB[5]), .DPO(pre_doutb_0[7]));
// Memory array element: COL: 0, ROW: 8
//  Initialization of Dual Port LUT ram now done through explicit
// parameter setting.
RAM64X1D#(.INIT(64'h0000000000000000))RAM64X1D_instance_2888(.WCLK(CLK), .WE(wea_0), 
  .A0(ADDRA[0]), .A1(ADDRA[1]), .A2(ADDRA[2]), .A3(ADDRA[3]), .A4(ADDRA[4]), .A5(ADDRA[5]), 
  .D(DINA[8]), .SPO(pre_douta_0[8]), .DPRA0(ADDRB[0]), .DPRA1(ADDRB[1]), .DPRA2(ADDRB[2]), 
  .DPRA3(ADDRB[3]), .DPRA4(ADDRB[4]), .DPRA5(ADDRB[5]), .DPO(pre_doutb_0[8]));
// Memory array element: COL: 0, ROW: 9
//  Initialization of Dual Port LUT ram now done through explicit
// parameter setting.
RAM64X1D#(.INIT(64'h0000000000000000))RAM64X1D_instance_2889(.WCLK(CLK), .WE(wea_0), 
  .A0(ADDRA[0]), .A1(ADDRA[1]), .A2(ADDRA[2]), .A3(ADDRA[3]), .A4(ADDRA[4]), .A5(ADDRA[5]), 
  .D(DINA[9]), .SPO(pre_douta_0[9]), .DPRA0(ADDRB[0]), .DPRA1(ADDRB[1]), .DPRA2(ADDRB[2]), 
  .DPRA3(ADDRB[3]), .DPRA4(ADDRB[4]), .DPRA5(ADDRB[5]), .DPO(pre_doutb_0[9]));
// Memory array element: COL: 0, ROW: 10
//  Initialization of Dual Port LUT ram now done through explicit
// parameter setting.
RAM64X1D#(.INIT(64'h0000000000000000))RAM64X1D_instance_2890(.WCLK(CLK), .WE(wea_0), 
  .A0(ADDRA[0]), .A1(ADDRA[1]), .A2(ADDRA[2]), .A3(ADDRA[3]), .A4(ADDRA[4]), .A5(ADDRA[5]), 
  .D(DINA[10]), .SPO(pre_douta_0[10]), .DPRA0(ADDRB[0]), .DPRA1(ADDRB[1]), .DPRA2(ADDRB[2]), 
  .DPRA3(ADDRB[3]), .DPRA4(ADDRB[4]), .DPRA5(ADDRB[5]), .DPO(pre_doutb_0[10]));
// Memory array element: COL: 0, ROW: 11
//  Initialization of Dual Port LUT ram now done through explicit
// parameter setting.
RAM64X1D#(.INIT(64'h0000000000000000))RAM64X1D_instance_2891(.WCLK(CLK), .WE(wea_0), 
  .A0(ADDRA[0]), .A1(ADDRA[1]), .A2(ADDRA[2]), .A3(ADDRA[3]), .A4(ADDRA[4]), .A5(ADDRA[5]), 
  .D(DINA[11]), .SPO(pre_douta_0[11]), .DPRA0(ADDRB[0]), .DPRA1(ADDRB[1]), .DPRA2(ADDRB[2]), 
  .DPRA3(ADDRB[3]), .DPRA4(ADDRB[4]), .DPRA5(ADDRB[5]), .DPO(pre_doutb_0[11]));
// Memory array element: COL: 0, ROW: 12
//  Initialization of Dual Port LUT ram now done through explicit
// parameter setting.
RAM64X1D#(.INIT(64'h0000000000000000))RAM64X1D_instance_2892(.WCLK(CLK), .WE(wea_0), 
  .A0(ADDRA[0]), .A1(ADDRA[1]), .A2(ADDRA[2]), .A3(ADDRA[3]), .A4(ADDRA[4]), .A5(ADDRA[5]), 
  .D(DINA[12]), .SPO(pre_douta_0[12]), .DPRA0(ADDRB[0]), .DPRA1(ADDRB[1]), .DPRA2(ADDRB[2]), 
  .DPRA3(ADDRB[3]), .DPRA4(ADDRB[4]), .DPRA5(ADDRB[5]), .DPO(pre_doutb_0[12]));
// Memory array element: COL: 0, ROW: 13
//  Initialization of Dual Port LUT ram now done through explicit
// parameter setting.
RAM64X1D#(.INIT(64'h0000000000000000))RAM64X1D_instance_2893(.WCLK(CLK), .WE(wea_0), 
  .A0(ADDRA[0]), .A1(ADDRA[1]), .A2(ADDRA[2]), .A3(ADDRA[3]), .A4(ADDRA[4]), .A5(ADDRA[5]), 
  .D(DINA[13]), .SPO(pre_douta_0[13]), .DPRA0(ADDRB[0]), .DPRA1(ADDRB[1]), .DPRA2(ADDRB[2]), 
  .DPRA3(ADDRB[3]), .DPRA4(ADDRB[4]), .DPRA5(ADDRB[5]), .DPO(pre_doutb_0[13]));
// Memory array element: COL: 0, ROW: 14
//  Initialization of Dual Port LUT ram now done through explicit
// parameter setting.
RAM64X1D#(.INIT(64'h0000000000000000))RAM64X1D_instance_2894(.WCLK(CLK), .WE(wea_0), 
  .A0(ADDRA[0]), .A1(ADDRA[1]), .A2(ADDRA[2]), .A3(ADDRA[3]), .A4(ADDRA[4]), .A5(ADDRA[5]), 
  .D(DINA[14]), .SPO(pre_douta_0[14]), .DPRA0(ADDRB[0]), .DPRA1(ADDRB[1]), .DPRA2(ADDRB[2]), 
  .DPRA3(ADDRB[3]), .DPRA4(ADDRB[4]), .DPRA5(ADDRB[5]), .DPO(pre_doutb_0[14]));
// Memory array element: COL: 0, ROW: 15
//  Initialization of Dual Port LUT ram now done through explicit
// parameter setting.
RAM64X1D#(.INIT(64'h0000000000000000))RAM64X1D_instance_2895(.WCLK(CLK), .WE(wea_0), 
  .A0(ADDRA[0]), .A1(ADDRA[1]), .A2(ADDRA[2]), .A3(ADDRA[3]), .A4(ADDRA[4]), .A5(ADDRA[5]), 
  .D(DINA[15]), .SPO(pre_douta_0[15]), .DPRA0(ADDRB[0]), .DPRA1(ADDRB[1]), .DPRA2(ADDRB[2]), 
  .DPRA3(ADDRB[3]), .DPRA4(ADDRB[4]), .DPRA5(ADDRB[5]), .DPO(pre_doutb_0[15]));
// Memory array element: COL: 0, ROW: 16
//  Initialization of Dual Port LUT ram now done through explicit
// parameter setting.
RAM64X1D#(.INIT(64'h0000000000000000))RAM64X1D_instance_2896(.WCLK(CLK), .WE(wea_0), 
  .A0(ADDRA[0]), .A1(ADDRA[1]), .A2(ADDRA[2]), .A3(ADDRA[3]), .A4(ADDRA[4]), .A5(ADDRA[5]), 
  .D(DINA[16]), .SPO(pre_douta_0[16]), .DPRA0(ADDRB[0]), .DPRA1(ADDRB[1]), .DPRA2(ADDRB[2]), 
  .DPRA3(ADDRB[3]), .DPRA4(ADDRB[4]), .DPRA5(ADDRB[5]), .DPO(pre_doutb_0[16]));
// Memory array element: COL: 0, ROW: 17
//  Initialization of Dual Port LUT ram now done through explicit
// parameter setting.
RAM64X1D#(.INIT(64'h0000000000000000))RAM64X1D_instance_2897(.WCLK(CLK), .WE(wea_0), 
  .A0(ADDRA[0]), .A1(ADDRA[1]), .A2(ADDRA[2]), .A3(ADDRA[3]), .A4(ADDRA[4]), .A5(ADDRA[5]), 
  .D(DINA[17]), .SPO(pre_douta_0[17]), .DPRA0(ADDRB[0]), .DPRA1(ADDRB[1]), .DPRA2(ADDRB[2]), 
  .DPRA3(ADDRB[3]), .DPRA4(ADDRB[4]), .DPRA5(ADDRB[5]), .DPO(pre_doutb_0[17]));
// Memory array element: COL: 0, ROW: 18
//  Initialization of Dual Port LUT ram now done through explicit
// parameter setting.
RAM64X1D#(.INIT(64'h0000000000000000))RAM64X1D_instance_2898(.WCLK(CLK), .WE(wea_0), 
  .A0(ADDRA[0]), .A1(ADDRA[1]), .A2(ADDRA[2]), .A3(ADDRA[3]), .A4(ADDRA[4]), .A5(ADDRA[5]), 
  .D(DINA[18]), .SPO(pre_douta_0[18]), .DPRA0(ADDRB[0]), .DPRA1(ADDRB[1]), .DPRA2(ADDRB[2]), 
  .DPRA3(ADDRB[3]), .DPRA4(ADDRB[4]), .DPRA5(ADDRB[5]), .DPO(pre_doutb_0[18]));
// Memory array element: COL: 0, ROW: 19
//  Initialization of Dual Port LUT ram now done through explicit
// parameter setting.
RAM64X1D#(.INIT(64'h0000000000000000))RAM64X1D_instance_2899(.WCLK(CLK), .WE(wea_0), 
  .A0(ADDRA[0]), .A1(ADDRA[1]), .A2(ADDRA[2]), .A3(ADDRA[3]), .A4(ADDRA[4]), .A5(ADDRA[5]), 
  .D(DINA[19]), .SPO(pre_douta_0[19]), .DPRA0(ADDRB[0]), .DPRA1(ADDRB[1]), .DPRA2(ADDRB[2]), 
  .DPRA3(ADDRB[3]), .DPRA4(ADDRB[4]), .DPRA5(ADDRB[5]), .DPO(pre_doutb_0[19]));
// Memory array element: COL: 0, ROW: 20
//  Initialization of Dual Port LUT ram now done through explicit
// parameter setting.
RAM64X1D#(.INIT(64'h0000000000000000))RAM64X1D_instance_2900(.WCLK(CLK), .WE(wea_0), 
  .A0(ADDRA[0]), .A1(ADDRA[1]), .A2(ADDRA[2]), .A3(ADDRA[3]), .A4(ADDRA[4]), .A5(ADDRA[5]), 
  .D(DINA[20]), .SPO(pre_douta_0[20]), .DPRA0(ADDRB[0]), .DPRA1(ADDRB[1]), .DPRA2(ADDRB[2]), 
  .DPRA3(ADDRB[3]), .DPRA4(ADDRB[4]), .DPRA5(ADDRB[5]), .DPO(pre_doutb_0[20]));
// Memory array element: COL: 0, ROW: 21
//  Initialization of Dual Port LUT ram now done through explicit
// parameter setting.
RAM64X1D#(.INIT(64'h0000000000000000))RAM64X1D_instance_2901(.WCLK(CLK), .WE(wea_0), 
  .A0(ADDRA[0]), .A1(ADDRA[1]), .A2(ADDRA[2]), .A3(ADDRA[3]), .A4(ADDRA[4]), .A5(ADDRA[5]), 
  .D(DINA[21]), .SPO(pre_douta_0[21]), .DPRA0(ADDRB[0]), .DPRA1(ADDRB[1]), .DPRA2(ADDRB[2]), 
  .DPRA3(ADDRB[3]), .DPRA4(ADDRB[4]), .DPRA5(ADDRB[5]), .DPO(pre_doutb_0[21]));
// Memory array element: COL: 0, ROW: 22
//  Initialization of Dual Port LUT ram now done through explicit
// parameter setting.
RAM64X1D#(.INIT(64'h0000000000000000))RAM64X1D_instance_2902(.WCLK(CLK), .WE(wea_0), 
  .A0(ADDRA[0]), .A1(ADDRA[1]), .A2(ADDRA[2]), .A3(ADDRA[3]), .A4(ADDRA[4]), .A5(ADDRA[5]), 
  .D(DINA[22]), .SPO(pre_douta_0[22]), .DPRA0(ADDRB[0]), .DPRA1(ADDRB[1]), .DPRA2(ADDRB[2]), 
  .DPRA3(ADDRB[3]), .DPRA4(ADDRB[4]), .DPRA5(ADDRB[5]), .DPO(pre_doutb_0[22]));
// Memory array element: COL: 0, ROW: 23
//  Initialization of Dual Port LUT ram now done through explicit
// parameter setting.
RAM64X1D#(.INIT(64'h0000000000000000))RAM64X1D_instance_2903(.WCLK(CLK), .WE(wea_0), 
  .A0(ADDRA[0]), .A1(ADDRA[1]), .A2(ADDRA[2]), .A3(ADDRA[3]), .A4(ADDRA[4]), .A5(ADDRA[5]), 
  .D(DINA[23]), .SPO(pre_douta_0[23]), .DPRA0(ADDRB[0]), .DPRA1(ADDRB[1]), .DPRA2(ADDRB[2]), 
  .DPRA3(ADDRB[3]), .DPRA4(ADDRB[4]), .DPRA5(ADDRB[5]), .DPO(pre_doutb_0[23]));
// Memory array element: COL: 0, ROW: 24
//  Initialization of Dual Port LUT ram now done through explicit
// parameter setting.
RAM64X1D#(.INIT(64'h0000000000000000))RAM64X1D_instance_2904(.WCLK(CLK), .WE(wea_0), 
  .A0(ADDRA[0]), .A1(ADDRA[1]), .A2(ADDRA[2]), .A3(ADDRA[3]), .A4(ADDRA[4]), .A5(ADDRA[5]), 
  .D(DINA[24]), .SPO(pre_douta_0[24]), .DPRA0(ADDRB[0]), .DPRA1(ADDRB[1]), .DPRA2(ADDRB[2]), 
  .DPRA3(ADDRB[3]), .DPRA4(ADDRB[4]), .DPRA5(ADDRB[5]), .DPO(pre_doutb_0[24]));
// Memory array element: COL: 0, ROW: 25
//  Initialization of Dual Port LUT ram now done through explicit
// parameter setting.
RAM64X1D#(.INIT(64'h0000000000000000))RAM64X1D_instance_2905(.WCLK(CLK), .WE(wea_0), 
  .A0(ADDRA[0]), .A1(ADDRA[1]), .A2(ADDRA[2]), .A3(ADDRA[3]), .A4(ADDRA[4]), .A5(ADDRA[5]), 
  .D(DINA[25]), .SPO(pre_douta_0[25]), .DPRA0(ADDRB[0]), .DPRA1(ADDRB[1]), .DPRA2(ADDRB[2]), 
  .DPRA3(ADDRB[3]), .DPRA4(ADDRB[4]), .DPRA5(ADDRB[5]), .DPO(pre_doutb_0[25]));
// Memory array element: COL: 0, ROW: 26
//  Initialization of Dual Port LUT ram now done through explicit
// parameter setting.
RAM64X1D#(.INIT(64'h0000000000000000))RAM64X1D_instance_2906(.WCLK(CLK), .WE(wea_0), 
  .A0(ADDRA[0]), .A1(ADDRA[1]), .A2(ADDRA[2]), .A3(ADDRA[3]), .A4(ADDRA[4]), .A5(ADDRA[5]), 
  .D(DINA[26]), .SPO(pre_douta_0[26]), .DPRA0(ADDRB[0]), .DPRA1(ADDRB[1]), .DPRA2(ADDRB[2]), 
  .DPRA3(ADDRB[3]), .DPRA4(ADDRB[4]), .DPRA5(ADDRB[5]), .DPO(pre_doutb_0[26]));
// Memory array element: COL: 0, ROW: 27
//  Initialization of Dual Port LUT ram now done through explicit
// parameter setting.
RAM64X1D#(.INIT(64'h0000000000000000))RAM64X1D_instance_2907(.WCLK(CLK), .WE(wea_0), 
  .A0(ADDRA[0]), .A1(ADDRA[1]), .A2(ADDRA[2]), .A3(ADDRA[3]), .A4(ADDRA[4]), .A5(ADDRA[5]), 
  .D(DINA[27]), .SPO(pre_douta_0[27]), .DPRA0(ADDRB[0]), .DPRA1(ADDRB[1]), .DPRA2(ADDRB[2]), 
  .DPRA3(ADDRB[3]), .DPRA4(ADDRB[4]), .DPRA5(ADDRB[5]), .DPO(pre_doutb_0[27]));
// Memory array element: COL: 0, ROW: 28
//  Initialization of Dual Port LUT ram now done through explicit
// parameter setting.
RAM64X1D#(.INIT(64'h0000000000000000))RAM64X1D_instance_2908(.WCLK(CLK), .WE(wea_0), 
  .A0(ADDRA[0]), .A1(ADDRA[1]), .A2(ADDRA[2]), .A3(ADDRA[3]), .A4(ADDRA[4]), .A5(ADDRA[5]), 
  .D(DINA[28]), .SPO(pre_douta_0[28]), .DPRA0(ADDRB[0]), .DPRA1(ADDRB[1]), .DPRA2(ADDRB[2]), 
  .DPRA3(ADDRB[3]), .DPRA4(ADDRB[4]), .DPRA5(ADDRB[5]), .DPO(pre_doutb_0[28]));
// Memory array element: COL: 0, ROW: 29
//  Initialization of Dual Port LUT ram now done through explicit
// parameter setting.
RAM64X1D#(.INIT(64'h0000000000000000))RAM64X1D_instance_2909(.WCLK(CLK), .WE(wea_0), 
  .A0(ADDRA[0]), .A1(ADDRA[1]), .A2(ADDRA[2]), .A3(ADDRA[3]), .A4(ADDRA[4]), .A5(ADDRA[5]), 
  .D(DINA[29]), .SPO(pre_douta_0[29]), .DPRA0(ADDRB[0]), .DPRA1(ADDRB[1]), .DPRA2(ADDRB[2]), 
  .DPRA3(ADDRB[3]), .DPRA4(ADDRB[4]), .DPRA5(ADDRB[5]), .DPO(pre_doutb_0[29]));
// Memory array element: COL: 0, ROW: 30
//  Initialization of Dual Port LUT ram now done through explicit
// parameter setting.
RAM64X1D#(.INIT(64'h0000000000000000))RAM64X1D_instance_2910(.WCLK(CLK), .WE(wea_0), 
  .A0(ADDRA[0]), .A1(ADDRA[1]), .A2(ADDRA[2]), .A3(ADDRA[3]), .A4(ADDRA[4]), .A5(ADDRA[5]), 
  .D(DINA[30]), .SPO(pre_douta_0[30]), .DPRA0(ADDRB[0]), .DPRA1(ADDRB[1]), .DPRA2(ADDRB[2]), 
  .DPRA3(ADDRB[3]), .DPRA4(ADDRB[4]), .DPRA5(ADDRB[5]), .DPO(pre_doutb_0[30]));
// Memory array element: COL: 0, ROW: 31
//  Initialization of Dual Port LUT ram now done through explicit
// parameter setting.
RAM64X1D#(.INIT(64'h0000000000000000))RAM64X1D_instance_2911(.WCLK(CLK), .WE(wea_0), 
  .A0(ADDRA[0]), .A1(ADDRA[1]), .A2(ADDRA[2]), .A3(ADDRA[3]), .A4(ADDRA[4]), .A5(ADDRA[5]), 
  .D(DINA[31]), .SPO(pre_douta_0[31]), .DPRA0(ADDRB[0]), .DPRA1(ADDRB[1]), .DPRA2(ADDRB[2]), 
  .DPRA3(ADDRB[3]), .DPRA4(ADDRB[4]), .DPRA5(ADDRB[5]), .DPO(pre_doutb_0[31]));
// Memory array element: COL: 1, ROW: 0
//  Initialization of Dual Port LUT ram now done through explicit
// parameter setting.
RAM64X1D#(.INIT(64'h0000000000000000))RAM64X1D_instance_2912(.WCLK(CLK), .WE(wea_1), 
  .A0(ADDRA[0]), .A1(ADDRA[1]), .A2(ADDRA[2]), .A3(ADDRA[3]), .A4(ADDRA[4]), .A5(ADDRA[5]), 
  .D(DINA[0]), .SPO(pre_douta_1[0]), .DPRA0(ADDRB[0]), .DPRA1(ADDRB[1]), .DPRA2(ADDRB[2]), 
  .DPRA3(ADDRB[3]), .DPRA4(ADDRB[4]), .DPRA5(ADDRB[5]), .DPO(pre_doutb_1[0]));
// Memory array element: COL: 1, ROW: 1
//  Initialization of Dual Port LUT ram now done through explicit
// parameter setting.
RAM64X1D#(.INIT(64'h0000000000000000))RAM64X1D_instance_2913(.WCLK(CLK), .WE(wea_1), 
  .A0(ADDRA[0]), .A1(ADDRA[1]), .A2(ADDRA[2]), .A3(ADDRA[3]), .A4(ADDRA[4]), .A5(ADDRA[5]), 
  .D(DINA[1]), .SPO(pre_douta_1[1]), .DPRA0(ADDRB[0]), .DPRA1(ADDRB[1]), .DPRA2(ADDRB[2]), 
  .DPRA3(ADDRB[3]), .DPRA4(ADDRB[4]), .DPRA5(ADDRB[5]), .DPO(pre_doutb_1[1]));
// Memory array element: COL: 1, ROW: 2
//  Initialization of Dual Port LUT ram now done through explicit
// parameter setting.
RAM64X1D#(.INIT(64'h0000000000000000))RAM64X1D_instance_2914(.WCLK(CLK), .WE(wea_1), 
  .A0(ADDRA[0]), .A1(ADDRA[1]), .A2(ADDRA[2]), .A3(ADDRA[3]), .A4(ADDRA[4]), .A5(ADDRA[5]), 
  .D(DINA[2]), .SPO(pre_douta_1[2]), .DPRA0(ADDRB[0]), .DPRA1(ADDRB[1]), .DPRA2(ADDRB[2]), 
  .DPRA3(ADDRB[3]), .DPRA4(ADDRB[4]), .DPRA5(ADDRB[5]), .DPO(pre_doutb_1[2]));
// Memory array element: COL: 1, ROW: 3
//  Initialization of Dual Port LUT ram now done through explicit
// parameter setting.
RAM64X1D#(.INIT(64'h0000000000000000))RAM64X1D_instance_2915(.WCLK(CLK), .WE(wea_1), 
  .A0(ADDRA[0]), .A1(ADDRA[1]), .A2(ADDRA[2]), .A3(ADDRA[3]), .A4(ADDRA[4]), .A5(ADDRA[5]), 
  .D(DINA[3]), .SPO(pre_douta_1[3]), .DPRA0(ADDRB[0]), .DPRA1(ADDRB[1]), .DPRA2(ADDRB[2]), 
  .DPRA3(ADDRB[3]), .DPRA4(ADDRB[4]), .DPRA5(ADDRB[5]), .DPO(pre_doutb_1[3]));
// Memory array element: COL: 1, ROW: 4
//  Initialization of Dual Port LUT ram now done through explicit
// parameter setting.
RAM64X1D#(.INIT(64'h0000000000000000))RAM64X1D_instance_2916(.WCLK(CLK), .WE(wea_1), 
  .A0(ADDRA[0]), .A1(ADDRA[1]), .A2(ADDRA[2]), .A3(ADDRA[3]), .A4(ADDRA[4]), .A5(ADDRA[5]), 
  .D(DINA[4]), .SPO(pre_douta_1[4]), .DPRA0(ADDRB[0]), .DPRA1(ADDRB[1]), .DPRA2(ADDRB[2]), 
  .DPRA3(ADDRB[3]), .DPRA4(ADDRB[4]), .DPRA5(ADDRB[5]), .DPO(pre_doutb_1[4]));
// Memory array element: COL: 1, ROW: 5
//  Initialization of Dual Port LUT ram now done through explicit
// parameter setting.
RAM64X1D#(.INIT(64'h0000000000000000))RAM64X1D_instance_2917(.WCLK(CLK), .WE(wea_1), 
  .A0(ADDRA[0]), .A1(ADDRA[1]), .A2(ADDRA[2]), .A3(ADDRA[3]), .A4(ADDRA[4]), .A5(ADDRA[5]), 
  .D(DINA[5]), .SPO(pre_douta_1[5]), .DPRA0(ADDRB[0]), .DPRA1(ADDRB[1]), .DPRA2(ADDRB[2]), 
  .DPRA3(ADDRB[3]), .DPRA4(ADDRB[4]), .DPRA5(ADDRB[5]), .DPO(pre_doutb_1[5]));
// Memory array element: COL: 1, ROW: 6
//  Initialization of Dual Port LUT ram now done through explicit
// parameter setting.
RAM64X1D#(.INIT(64'h0000000000000000))RAM64X1D_instance_2918(.WCLK(CLK), .WE(wea_1), 
  .A0(ADDRA[0]), .A1(ADDRA[1]), .A2(ADDRA[2]), .A3(ADDRA[3]), .A4(ADDRA[4]), .A5(ADDRA[5]), 
  .D(DINA[6]), .SPO(pre_douta_1[6]), .DPRA0(ADDRB[0]), .DPRA1(ADDRB[1]), .DPRA2(ADDRB[2]), 
  .DPRA3(ADDRB[3]), .DPRA4(ADDRB[4]), .DPRA5(ADDRB[5]), .DPO(pre_doutb_1[6]));
// Memory array element: COL: 1, ROW: 7
//  Initialization of Dual Port LUT ram now done through explicit
// parameter setting.
RAM64X1D#(.INIT(64'h0000000000000000))RAM64X1D_instance_2919(.WCLK(CLK), .WE(wea_1), 
  .A0(ADDRA[0]), .A1(ADDRA[1]), .A2(ADDRA[2]), .A3(ADDRA[3]), .A4(ADDRA[4]), .A5(ADDRA[5]), 
  .D(DINA[7]), .SPO(pre_douta_1[7]), .DPRA0(ADDRB[0]), .DPRA1(ADDRB[1]), .DPRA2(ADDRB[2]), 
  .DPRA3(ADDRB[3]), .DPRA4(ADDRB[4]), .DPRA5(ADDRB[5]), .DPO(pre_doutb_1[7]));
// Memory array element: COL: 1, ROW: 8
//  Initialization of Dual Port LUT ram now done through explicit
// parameter setting.
RAM64X1D#(.INIT(64'h0000000000000000))RAM64X1D_instance_2920(.WCLK(CLK), .WE(wea_1), 
  .A0(ADDRA[0]), .A1(ADDRA[1]), .A2(ADDRA[2]), .A3(ADDRA[3]), .A4(ADDRA[4]), .A5(ADDRA[5]), 
  .D(DINA[8]), .SPO(pre_douta_1[8]), .DPRA0(ADDRB[0]), .DPRA1(ADDRB[1]), .DPRA2(ADDRB[2]), 
  .DPRA3(ADDRB[3]), .DPRA4(ADDRB[4]), .DPRA5(ADDRB[5]), .DPO(pre_doutb_1[8]));
// Memory array element: COL: 1, ROW: 9
//  Initialization of Dual Port LUT ram now done through explicit
// parameter setting.
RAM64X1D#(.INIT(64'h0000000000000000))RAM64X1D_instance_2921(.WCLK(CLK), .WE(wea_1), 
  .A0(ADDRA[0]), .A1(ADDRA[1]), .A2(ADDRA[2]), .A3(ADDRA[3]), .A4(ADDRA[4]), .A5(ADDRA[5]), 
  .D(DINA[9]), .SPO(pre_douta_1[9]), .DPRA0(ADDRB[0]), .DPRA1(ADDRB[1]), .DPRA2(ADDRB[2]), 
  .DPRA3(ADDRB[3]), .DPRA4(ADDRB[4]), .DPRA5(ADDRB[5]), .DPO(pre_doutb_1[9]));
// Memory array element: COL: 1, ROW: 10
//  Initialization of Dual Port LUT ram now done through explicit
// parameter setting.
RAM64X1D#(.INIT(64'h0000000000000000))RAM64X1D_instance_2922(.WCLK(CLK), .WE(wea_1), 
  .A0(ADDRA[0]), .A1(ADDRA[1]), .A2(ADDRA[2]), .A3(ADDRA[3]), .A4(ADDRA[4]), .A5(ADDRA[5]), 
  .D(DINA[10]), .SPO(pre_douta_1[10]), .DPRA0(ADDRB[0]), .DPRA1(ADDRB[1]), .DPRA2(ADDRB[2]), 
  .DPRA3(ADDRB[3]), .DPRA4(ADDRB[4]), .DPRA5(ADDRB[5]), .DPO(pre_doutb_1[10]));
// Memory array element: COL: 1, ROW: 11
//  Initialization of Dual Port LUT ram now done through explicit
// parameter setting.
RAM64X1D#(.INIT(64'h0000000000000000))RAM64X1D_instance_2923(.WCLK(CLK), .WE(wea_1), 
  .A0(ADDRA[0]), .A1(ADDRA[1]), .A2(ADDRA[2]), .A3(ADDRA[3]), .A4(ADDRA[4]), .A5(ADDRA[5]), 
  .D(DINA[11]), .SPO(pre_douta_1[11]), .DPRA0(ADDRB[0]), .DPRA1(ADDRB[1]), .DPRA2(ADDRB[2]), 
  .DPRA3(ADDRB[3]), .DPRA4(ADDRB[4]), .DPRA5(ADDRB[5]), .DPO(pre_doutb_1[11]));
// Memory array element: COL: 1, ROW: 12
//  Initialization of Dual Port LUT ram now done through explicit
// parameter setting.
RAM64X1D#(.INIT(64'h0000000000000000))RAM64X1D_instance_2924(.WCLK(CLK), .WE(wea_1), 
  .A0(ADDRA[0]), .A1(ADDRA[1]), .A2(ADDRA[2]), .A3(ADDRA[3]), .A4(ADDRA[4]), .A5(ADDRA[5]), 
  .D(DINA[12]), .SPO(pre_douta_1[12]), .DPRA0(ADDRB[0]), .DPRA1(ADDRB[1]), .DPRA2(ADDRB[2]), 
  .DPRA3(ADDRB[3]), .DPRA4(ADDRB[4]), .DPRA5(ADDRB[5]), .DPO(pre_doutb_1[12]));
// Memory array element: COL: 1, ROW: 13
//  Initialization of Dual Port LUT ram now done through explicit
// parameter setting.
RAM64X1D#(.INIT(64'h0000000000000000))RAM64X1D_instance_2925(.WCLK(CLK), .WE(wea_1), 
  .A0(ADDRA[0]), .A1(ADDRA[1]), .A2(ADDRA[2]), .A3(ADDRA[3]), .A4(ADDRA[4]), .A5(ADDRA[5]), 
  .D(DINA[13]), .SPO(pre_douta_1[13]), .DPRA0(ADDRB[0]), .DPRA1(ADDRB[1]), .DPRA2(ADDRB[2]), 
  .DPRA3(ADDRB[3]), .DPRA4(ADDRB[4]), .DPRA5(ADDRB[5]), .DPO(pre_doutb_1[13]));
// Memory array element: COL: 1, ROW: 14
//  Initialization of Dual Port LUT ram now done through explicit
// parameter setting.
RAM64X1D#(.INIT(64'h0000000000000000))RAM64X1D_instance_2926(.WCLK(CLK), .WE(wea_1), 
  .A0(ADDRA[0]), .A1(ADDRA[1]), .A2(ADDRA[2]), .A3(ADDRA[3]), .A4(ADDRA[4]), .A5(ADDRA[5]), 
  .D(DINA[14]), .SPO(pre_douta_1[14]), .DPRA0(ADDRB[0]), .DPRA1(ADDRB[1]), .DPRA2(ADDRB[2]), 
  .DPRA3(ADDRB[3]), .DPRA4(ADDRB[4]), .DPRA5(ADDRB[5]), .DPO(pre_doutb_1[14]));
// Memory array element: COL: 1, ROW: 15
//  Initialization of Dual Port LUT ram now done through explicit
// parameter setting.
RAM64X1D#(.INIT(64'h0000000000000000))RAM64X1D_instance_2927(.WCLK(CLK), .WE(wea_1), 
  .A0(ADDRA[0]), .A1(ADDRA[1]), .A2(ADDRA[2]), .A3(ADDRA[3]), .A4(ADDRA[4]), .A5(ADDRA[5]), 
  .D(DINA[15]), .SPO(pre_douta_1[15]), .DPRA0(ADDRB[0]), .DPRA1(ADDRB[1]), .DPRA2(ADDRB[2]), 
  .DPRA3(ADDRB[3]), .DPRA4(ADDRB[4]), .DPRA5(ADDRB[5]), .DPO(pre_doutb_1[15]));
// Memory array element: COL: 1, ROW: 16
//  Initialization of Dual Port LUT ram now done through explicit
// parameter setting.
RAM64X1D#(.INIT(64'h0000000000000000))RAM64X1D_instance_2928(.WCLK(CLK), .WE(wea_1), 
  .A0(ADDRA[0]), .A1(ADDRA[1]), .A2(ADDRA[2]), .A3(ADDRA[3]), .A4(ADDRA[4]), .A5(ADDRA[5]), 
  .D(DINA[16]), .SPO(pre_douta_1[16]), .DPRA0(ADDRB[0]), .DPRA1(ADDRB[1]), .DPRA2(ADDRB[2]), 
  .DPRA3(ADDRB[3]), .DPRA4(ADDRB[4]), .DPRA5(ADDRB[5]), .DPO(pre_doutb_1[16]));
// Memory array element: COL: 1, ROW: 17
//  Initialization of Dual Port LUT ram now done through explicit
// parameter setting.
RAM64X1D#(.INIT(64'h0000000000000000))RAM64X1D_instance_2929(.WCLK(CLK), .WE(wea_1), 
  .A0(ADDRA[0]), .A1(ADDRA[1]), .A2(ADDRA[2]), .A3(ADDRA[3]), .A4(ADDRA[4]), .A5(ADDRA[5]), 
  .D(DINA[17]), .SPO(pre_douta_1[17]), .DPRA0(ADDRB[0]), .DPRA1(ADDRB[1]), .DPRA2(ADDRB[2]), 
  .DPRA3(ADDRB[3]), .DPRA4(ADDRB[4]), .DPRA5(ADDRB[5]), .DPO(pre_doutb_1[17]));
// Memory array element: COL: 1, ROW: 18
//  Initialization of Dual Port LUT ram now done through explicit
// parameter setting.
RAM64X1D#(.INIT(64'h0000000000000000))RAM64X1D_instance_2930(.WCLK(CLK), .WE(wea_1), 
  .A0(ADDRA[0]), .A1(ADDRA[1]), .A2(ADDRA[2]), .A3(ADDRA[3]), .A4(ADDRA[4]), .A5(ADDRA[5]), 
  .D(DINA[18]), .SPO(pre_douta_1[18]), .DPRA0(ADDRB[0]), .DPRA1(ADDRB[1]), .DPRA2(ADDRB[2]), 
  .DPRA3(ADDRB[3]), .DPRA4(ADDRB[4]), .DPRA5(ADDRB[5]), .DPO(pre_doutb_1[18]));
// Memory array element: COL: 1, ROW: 19
//  Initialization of Dual Port LUT ram now done through explicit
// parameter setting.
RAM64X1D#(.INIT(64'h0000000000000000))RAM64X1D_instance_2931(.WCLK(CLK), .WE(wea_1), 
  .A0(ADDRA[0]), .A1(ADDRA[1]), .A2(ADDRA[2]), .A3(ADDRA[3]), .A4(ADDRA[4]), .A5(ADDRA[5]), 
  .D(DINA[19]), .SPO(pre_douta_1[19]), .DPRA0(ADDRB[0]), .DPRA1(ADDRB[1]), .DPRA2(ADDRB[2]), 
  .DPRA3(ADDRB[3]), .DPRA4(ADDRB[4]), .DPRA5(ADDRB[5]), .DPO(pre_doutb_1[19]));
// Memory array element: COL: 1, ROW: 20
//  Initialization of Dual Port LUT ram now done through explicit
// parameter setting.
RAM64X1D#(.INIT(64'h0000000000000000))RAM64X1D_instance_2932(.WCLK(CLK), .WE(wea_1), 
  .A0(ADDRA[0]), .A1(ADDRA[1]), .A2(ADDRA[2]), .A3(ADDRA[3]), .A4(ADDRA[4]), .A5(ADDRA[5]), 
  .D(DINA[20]), .SPO(pre_douta_1[20]), .DPRA0(ADDRB[0]), .DPRA1(ADDRB[1]), .DPRA2(ADDRB[2]), 
  .DPRA3(ADDRB[3]), .DPRA4(ADDRB[4]), .DPRA5(ADDRB[5]), .DPO(pre_doutb_1[20]));
// Memory array element: COL: 1, ROW: 21
//  Initialization of Dual Port LUT ram now done through explicit
// parameter setting.
RAM64X1D#(.INIT(64'h0000000000000000))RAM64X1D_instance_2933(.WCLK(CLK), .WE(wea_1), 
  .A0(ADDRA[0]), .A1(ADDRA[1]), .A2(ADDRA[2]), .A3(ADDRA[3]), .A4(ADDRA[4]), .A5(ADDRA[5]), 
  .D(DINA[21]), .SPO(pre_douta_1[21]), .DPRA0(ADDRB[0]), .DPRA1(ADDRB[1]), .DPRA2(ADDRB[2]), 
  .DPRA3(ADDRB[3]), .DPRA4(ADDRB[4]), .DPRA5(ADDRB[5]), .DPO(pre_doutb_1[21]));
// Memory array element: COL: 1, ROW: 22
//  Initialization of Dual Port LUT ram now done through explicit
// parameter setting.
RAM64X1D#(.INIT(64'h0000000000000000))RAM64X1D_instance_2934(.WCLK(CLK), .WE(wea_1), 
  .A0(ADDRA[0]), .A1(ADDRA[1]), .A2(ADDRA[2]), .A3(ADDRA[3]), .A4(ADDRA[4]), .A5(ADDRA[5]), 
  .D(DINA[22]), .SPO(pre_douta_1[22]), .DPRA0(ADDRB[0]), .DPRA1(ADDRB[1]), .DPRA2(ADDRB[2]), 
  .DPRA3(ADDRB[3]), .DPRA4(ADDRB[4]), .DPRA5(ADDRB[5]), .DPO(pre_doutb_1[22]));
// Memory array element: COL: 1, ROW: 23
//  Initialization of Dual Port LUT ram now done through explicit
// parameter setting.
RAM64X1D#(.INIT(64'h0000000000000000))RAM64X1D_instance_2935(.WCLK(CLK), .WE(wea_1), 
  .A0(ADDRA[0]), .A1(ADDRA[1]), .A2(ADDRA[2]), .A3(ADDRA[3]), .A4(ADDRA[4]), .A5(ADDRA[5]), 
  .D(DINA[23]), .SPO(pre_douta_1[23]), .DPRA0(ADDRB[0]), .DPRA1(ADDRB[1]), .DPRA2(ADDRB[2]), 
  .DPRA3(ADDRB[3]), .DPRA4(ADDRB[4]), .DPRA5(ADDRB[5]), .DPO(pre_doutb_1[23]));
// Memory array element: COL: 1, ROW: 24
//  Initialization of Dual Port LUT ram now done through explicit
// parameter setting.
RAM64X1D#(.INIT(64'h0000000000000000))RAM64X1D_instance_2936(.WCLK(CLK), .WE(wea_1), 
  .A0(ADDRA[0]), .A1(ADDRA[1]), .A2(ADDRA[2]), .A3(ADDRA[3]), .A4(ADDRA[4]), .A5(ADDRA[5]), 
  .D(DINA[24]), .SPO(pre_douta_1[24]), .DPRA0(ADDRB[0]), .DPRA1(ADDRB[1]), .DPRA2(ADDRB[2]), 
  .DPRA3(ADDRB[3]), .DPRA4(ADDRB[4]), .DPRA5(ADDRB[5]), .DPO(pre_doutb_1[24]));
// Memory array element: COL: 1, ROW: 25
//  Initialization of Dual Port LUT ram now done through explicit
// parameter setting.
RAM64X1D#(.INIT(64'h0000000000000000))RAM64X1D_instance_2937(.WCLK(CLK), .WE(wea_1), 
  .A0(ADDRA[0]), .A1(ADDRA[1]), .A2(ADDRA[2]), .A3(ADDRA[3]), .A4(ADDRA[4]), .A5(ADDRA[5]), 
  .D(DINA[25]), .SPO(pre_douta_1[25]), .DPRA0(ADDRB[0]), .DPRA1(ADDRB[1]), .DPRA2(ADDRB[2]), 
  .DPRA3(ADDRB[3]), .DPRA4(ADDRB[4]), .DPRA5(ADDRB[5]), .DPO(pre_doutb_1[25]));
// Memory array element: COL: 1, ROW: 26
//  Initialization of Dual Port LUT ram now done through explicit
// parameter setting.
RAM64X1D#(.INIT(64'h0000000000000000))RAM64X1D_instance_2938(.WCLK(CLK), .WE(wea_1), 
  .A0(ADDRA[0]), .A1(ADDRA[1]), .A2(ADDRA[2]), .A3(ADDRA[3]), .A4(ADDRA[4]), .A5(ADDRA[5]), 
  .D(DINA[26]), .SPO(pre_douta_1[26]), .DPRA0(ADDRB[0]), .DPRA1(ADDRB[1]), .DPRA2(ADDRB[2]), 
  .DPRA3(ADDRB[3]), .DPRA4(ADDRB[4]), .DPRA5(ADDRB[5]), .DPO(pre_doutb_1[26]));
// Memory array element: COL: 1, ROW: 27
//  Initialization of Dual Port LUT ram now done through explicit
// parameter setting.
RAM64X1D#(.INIT(64'h0000000000000000))RAM64X1D_instance_2939(.WCLK(CLK), .WE(wea_1), 
  .A0(ADDRA[0]), .A1(ADDRA[1]), .A2(ADDRA[2]), .A3(ADDRA[3]), .A4(ADDRA[4]), .A5(ADDRA[5]), 
  .D(DINA[27]), .SPO(pre_douta_1[27]), .DPRA0(ADDRB[0]), .DPRA1(ADDRB[1]), .DPRA2(ADDRB[2]), 
  .DPRA3(ADDRB[3]), .DPRA4(ADDRB[4]), .DPRA5(ADDRB[5]), .DPO(pre_doutb_1[27]));
// Memory array element: COL: 1, ROW: 28
//  Initialization of Dual Port LUT ram now done through explicit
// parameter setting.
RAM64X1D#(.INIT(64'h0000000000000000))RAM64X1D_instance_2940(.WCLK(CLK), .WE(wea_1), 
  .A0(ADDRA[0]), .A1(ADDRA[1]), .A2(ADDRA[2]), .A3(ADDRA[3]), .A4(ADDRA[4]), .A5(ADDRA[5]), 
  .D(DINA[28]), .SPO(pre_douta_1[28]), .DPRA0(ADDRB[0]), .DPRA1(ADDRB[1]), .DPRA2(ADDRB[2]), 
  .DPRA3(ADDRB[3]), .DPRA4(ADDRB[4]), .DPRA5(ADDRB[5]), .DPO(pre_doutb_1[28]));
// Memory array element: COL: 1, ROW: 29
//  Initialization of Dual Port LUT ram now done through explicit
// parameter setting.
RAM64X1D#(.INIT(64'h0000000000000000))RAM64X1D_instance_2941(.WCLK(CLK), .WE(wea_1), 
  .A0(ADDRA[0]), .A1(ADDRA[1]), .A2(ADDRA[2]), .A3(ADDRA[3]), .A4(ADDRA[4]), .A5(ADDRA[5]), 
  .D(DINA[29]), .SPO(pre_douta_1[29]), .DPRA0(ADDRB[0]), .DPRA1(ADDRB[1]), .DPRA2(ADDRB[2]), 
  .DPRA3(ADDRB[3]), .DPRA4(ADDRB[4]), .DPRA5(ADDRB[5]), .DPO(pre_doutb_1[29]));
// Memory array element: COL: 1, ROW: 30
//  Initialization of Dual Port LUT ram now done through explicit
// parameter setting.
RAM64X1D#(.INIT(64'h0000000000000000))RAM64X1D_instance_2942(.WCLK(CLK), .WE(wea_1), 
  .A0(ADDRA[0]), .A1(ADDRA[1]), .A2(ADDRA[2]), .A3(ADDRA[3]), .A4(ADDRA[4]), .A5(ADDRA[5]), 
  .D(DINA[30]), .SPO(pre_douta_1[30]), .DPRA0(ADDRB[0]), .DPRA1(ADDRB[1]), .DPRA2(ADDRB[2]), 
  .DPRA3(ADDRB[3]), .DPRA4(ADDRB[4]), .DPRA5(ADDRB[5]), .DPO(pre_doutb_1[30]));
// Memory array element: COL: 1, ROW: 31
//  Initialization of Dual Port LUT ram now done through explicit
// parameter setting.
RAM64X1D#(.INIT(64'h0000000000000000))RAM64X1D_instance_2943(.WCLK(CLK), .WE(wea_1), 
  .A0(ADDRA[0]), .A1(ADDRA[1]), .A2(ADDRA[2]), .A3(ADDRA[3]), .A4(ADDRA[4]), .A5(ADDRA[5]), 
  .D(DINA[31]), .SPO(pre_douta_1[31]), .DPRA0(ADDRB[0]), .DPRA1(ADDRB[1]), .DPRA2(ADDRB[2]), 
  .DPRA3(ADDRB[3]), .DPRA4(ADDRB[4]), .DPRA5(ADDRB[5]), .DPO(pre_doutb_1[31]));
endmodule



module medianRow2_structuralmemory_0aed58e8_(CLK_u91, bus_6df58784_, bus_2ab86133_, bus_406898fc_, bus_18a3dbb5_, bus_319ddac6_, bus_0fc82b7a_, bus_47ce1462_, bus_30ece563_, bus_15545edf_, bus_1907e964_, bus_301022b8_, bus_04ce9be2_, bus_26368992_);
input		CLK_u91;
input		bus_6df58784_;
input	[31:0]	bus_2ab86133_;
input	[2:0]	bus_406898fc_;
input		bus_18a3dbb5_;
input	[31:0]	bus_319ddac6_;
input	[2:0]	bus_0fc82b7a_;
input		bus_47ce1462_;
input		bus_30ece563_;
input	[31:0]	bus_15545edf_;
output	[31:0]	bus_1907e964_;
output		bus_301022b8_;
output	[31:0]	bus_04ce9be2_;
output		bus_26368992_;
reg		logicalMem_477292e0_we_delay0_u0=1'h0;
wire		or_0dc28ee5_u0;
wire		and_025bca55_u0;
wire		not_191233fd_u0;
wire	[31:0]	bus_7d793040_;
wire	[31:0]	bus_7e6f3936_;
wire		or_23d77f01_u0;
assign bus_1907e964_=bus_7d793040_;
assign bus_301022b8_=bus_18a3dbb5_;
assign bus_04ce9be2_=bus_7e6f3936_;
assign bus_26368992_=or_23d77f01_u0;
always @(posedge CLK_u91 or posedge bus_6df58784_)
begin
if (bus_6df58784_)
logicalMem_477292e0_we_delay0_u0<=1'h0;
else logicalMem_477292e0_we_delay0_u0<=bus_30ece563_;
end
assign or_0dc28ee5_u0=bus_47ce1462_|bus_30ece563_;
assign and_025bca55_u0=bus_47ce1462_&not_191233fd_u0;
assign not_191233fd_u0=~bus_30ece563_;
medianRow2_forge_memory_101x32_143 medianRow2_forge_memory_101x32_143_instance0(.CLK(CLK_u91), 
  .ENA(or_0dc28ee5_u0), .WEA(bus_30ece563_), .DINA(bus_15545edf_), .ADDRA(bus_319ddac6_), 
  .DOUTA(bus_7e6f3936_), .DONEA(), .ENB(bus_18a3dbb5_), .ADDRB(bus_2ab86133_), .DOUTB(bus_7d793040_), 
  .DONEB());
assign or_23d77f01_u0=and_025bca55_u0|logicalMem_477292e0_we_delay0_u0;
endmodule



module medianRow2_simplememoryreferee_58e8a8fe_(bus_15de3e13_, bus_6ca1e275_, bus_744ce090_, bus_4a14b098_, bus_1b47094c_, bus_2ee109b3_, bus_75473844_, bus_031b07d3_, bus_083410c7_, bus_3cb5479c_, bus_05044a29_, bus_2be1189d_, bus_38c1df49_, bus_039f8cd7_);
input		bus_15de3e13_;
input		bus_6ca1e275_;
input		bus_744ce090_;
input	[31:0]	bus_4a14b098_;
input		bus_1b47094c_;
input	[31:0]	bus_2ee109b3_;
input	[2:0]	bus_75473844_;
output	[31:0]	bus_031b07d3_;
output	[31:0]	bus_083410c7_;
output		bus_3cb5479c_;
output		bus_05044a29_;
output	[2:0]	bus_2be1189d_;
output	[31:0]	bus_38c1df49_;
output		bus_039f8cd7_;
assign bus_031b07d3_=32'h0;
assign bus_083410c7_=bus_2ee109b3_;
assign bus_3cb5479c_=1'h0;
assign bus_05044a29_=bus_1b47094c_;
assign bus_2be1189d_=3'h1;
assign bus_38c1df49_=bus_4a14b098_;
assign bus_039f8cd7_=bus_744ce090_;
endmodule



module medianRow2_Kicker_57(CLK, RESET, bus_08703637_);
input		CLK;
input		RESET;
output		bus_08703637_;
reg		kicker_1=1'h0;
reg		kicker_res=1'h0;
wire		bus_46755b49_;
reg		kicker_2=1'h0;
wire		bus_355b5fc9_;
wire		bus_084ba278_;
wire		bus_19eb17b4_;
assign bus_08703637_=kicker_res;
always @(posedge CLK)
begin
kicker_1<=bus_355b5fc9_;
end
always @(posedge CLK)
begin
kicker_res<=bus_46755b49_;
end
assign bus_46755b49_=kicker_1&bus_355b5fc9_&bus_084ba278_;
always @(posedge CLK)
begin
kicker_2<=bus_19eb17b4_;
end
assign bus_355b5fc9_=~RESET;
assign bus_084ba278_=~kicker_2;
assign bus_19eb17b4_=bus_355b5fc9_&kicker_1;
endmodule



module medianRow2_scheduler(CLK, RESET, GO, port_5b110a64_, port_373611a4_, port_5c13afd1_, port_78856155_, port_659b6104_, port_413ea20c_, RESULT, RESULT_u2213, RESULT_u2214, RESULT_u2215, DONE);
input		CLK;
input		RESET;
input		GO;
input		port_5b110a64_;
input	[31:0]	port_373611a4_;
input		port_5c13afd1_;
input		port_78856155_;
input		port_659b6104_;
input		port_413ea20c_;
output		RESULT;
output		RESULT_u2213;
output		RESULT_u2214;
output		RESULT_u2215;
output		DONE;
wire signed	[31:0]	lessThan_b_signed;
wire		lessThan;
wire signed	[31:0]	lessThan_a_signed;
wire		equals;
wire signed	[31:0]	equals_b_signed;
wire signed	[31:0]	equals_a_signed;
wire		equals_u232;
wire signed	[31:0]	equals_u232_b_signed;
wire signed	[31:0]	equals_u232_a_signed;
wire		not_u876_u0;
wire		and_u3758_u0;
wire		and_u3759_u0;
wire		andOp;
wire		and_u3760_u0;
wire		not_u877_u0;
wire		and_u3761_u0;
wire		simplePinWrite;
wire		and_u3762_u0;
wire		and_u3763_u0;
wire signed	[31:0]	equals_u233_b_signed;
wire		equals_u233;
wire signed	[31:0]	equals_u233_a_signed;
wire		not_u878_u0;
wire		and_u3764_u0;
wire		and_u3765_u0;
wire		andOp_u79;
wire		not_u879_u0;
wire		and_u3766_u0;
wire		and_u3767_u0;
wire		simplePinWrite_u620;
wire		and_u3768_u0;
wire		and_u3769_u0;
wire		not_u880_u0;
wire		and_u3770_u0;
wire		not_u881_u0;
wire		and_u3771_u0;
wire		simplePinWrite_u621;
wire		and_u3772_u0;
wire		or_u1435_u0;
wire		and_u3773_u0;
reg		and_delayed_u388=1'h0;
wire		and_u3774_u0;
wire		and_u3775_u0;
reg		and_delayed_u389_u0=1'h0;
wire		or_u1436_u0;
wire		or_u1437_u0;
wire		and_u3776_u0;
reg		reg_5e7414b3_u0=1'h0;
wire		and_u3777_u0;
wire		or_u1438_u0;
wire		mux_u1909;
wire		or_u1439_u0;
reg		reg_7af6556c_u0=1'h0;
wire		and_u3778_u0;
wire		and_u3779_u0;
wire		mux_u1910_u0;
wire		or_u1440_u0;
wire		receive_go_merge;
wire		and_u3780_u0;
reg		loopControl_u95=1'h0;
reg		syncEnable_u1425=1'h0;
wire		or_u1441_u0;
reg		reg_64ecf97d_u0=1'h0;
reg		reg_64ecf97d_result_delayed_u0=1'h0;
wire		mux_u1911_u0;
wire		or_u1442_u0;
assign lessThan_a_signed=port_373611a4_;
assign lessThan_b_signed=32'h65;
assign lessThan=lessThan_a_signed<lessThan_b_signed;
assign equals_a_signed=port_373611a4_;
assign equals_b_signed=32'h65;
assign equals=equals_a_signed==equals_b_signed;
assign equals_u232_a_signed={31'b0, port_5b110a64_};
assign equals_u232_b_signed=32'h0;
assign equals_u232=equals_u232_a_signed==equals_u232_b_signed;
assign not_u876_u0=~equals_u232;
assign and_u3758_u0=and_u3780_u0&equals_u232;
assign and_u3759_u0=and_u3780_u0&not_u876_u0;
assign andOp=lessThan&port_413ea20c_;
assign and_u3760_u0=and_u3763_u0&not_u877_u0;
assign not_u877_u0=~andOp;
assign and_u3761_u0=and_u3763_u0&andOp;
assign simplePinWrite=and_u3762_u0&{1{and_u3762_u0}};
assign and_u3762_u0=and_u3761_u0&and_u3763_u0;
assign and_u3763_u0=and_u3758_u0&and_u3780_u0;
assign equals_u233_a_signed={31'b0, port_5b110a64_};
assign equals_u233_b_signed=32'h1;
assign equals_u233=equals_u233_a_signed==equals_u233_b_signed;
assign not_u878_u0=~equals_u233;
assign and_u3764_u0=and_u3780_u0&not_u878_u0;
assign and_u3765_u0=and_u3780_u0&equals_u233;
assign andOp_u79=lessThan&port_413ea20c_;
assign not_u879_u0=~andOp_u79;
assign and_u3766_u0=and_u3779_u0&andOp_u79;
assign and_u3767_u0=and_u3779_u0&not_u879_u0;
assign simplePinWrite_u620=and_u3777_u0&{1{and_u3777_u0}};
assign and_u3768_u0=and_u3776_u0&equals;
assign and_u3769_u0=and_u3776_u0&not_u880_u0;
assign not_u880_u0=~equals;
assign and_u3770_u0=and_u3774_u0&port_659b6104_;
assign not_u881_u0=~port_659b6104_;
assign and_u3771_u0=and_u3774_u0&not_u881_u0;
assign simplePinWrite_u621=and_u3773_u0&{1{and_u3773_u0}};
assign and_u3772_u0=and_u3771_u0&and_u3774_u0;
assign or_u1435_u0=port_78856155_|and_delayed_u388;
assign and_u3773_u0=and_u3770_u0&and_u3774_u0;
always @(posedge CLK or posedge RESET)
begin
if (RESET)
and_delayed_u388<=1'h0;
else and_delayed_u388<=and_u3772_u0;
end
assign and_u3774_u0=and_u3768_u0&and_u3776_u0;
assign and_u3775_u0=and_u3769_u0&and_u3776_u0;
always @(posedge CLK or posedge RESET)
begin
if (RESET)
and_delayed_u389_u0<=1'h0;
else and_delayed_u389_u0<=and_u3775_u0;
end
assign or_u1436_u0=or_u1435_u0|and_delayed_u389_u0;
assign or_u1437_u0=or_u1436_u0|reg_5e7414b3_u0;
assign and_u3776_u0=and_u3767_u0&and_u3779_u0;
always @(posedge CLK or posedge RESET)
begin
if (RESET)
reg_5e7414b3_u0<=1'h0;
else reg_5e7414b3_u0<=and_u3777_u0;
end
assign and_u3777_u0=and_u3766_u0&and_u3779_u0;
assign or_u1438_u0=and_u3777_u0|and_u3773_u0;
assign mux_u1909=(and_u3777_u0)?1'h1:1'h0;
assign or_u1439_u0=or_u1437_u0|reg_7af6556c_u0;
always @(posedge CLK or posedge RESET)
begin
if (RESET)
reg_7af6556c_u0<=1'h0;
else reg_7af6556c_u0<=and_u3778_u0;
end
assign and_u3778_u0=and_u3764_u0&and_u3780_u0;
assign and_u3779_u0=and_u3765_u0&and_u3780_u0;
assign mux_u1910_u0=(and_u3762_u0)?1'h1:mux_u1909;
assign or_u1440_u0=and_u3762_u0|or_u1438_u0;
assign receive_go_merge=simplePinWrite|simplePinWrite_u620;
assign and_u3780_u0=or_u1441_u0&or_u1441_u0;
always @(posedge CLK or posedge syncEnable_u1425)
begin
if (syncEnable_u1425)
loopControl_u95<=1'h0;
else loopControl_u95<=or_u1439_u0;
end
always @(posedge CLK)
begin
if (reg_64ecf97d_result_delayed_u0)
syncEnable_u1425<=RESET;
end
assign or_u1441_u0=reg_64ecf97d_result_delayed_u0|loopControl_u95;
always @(posedge CLK or posedge RESET)
begin
if (RESET)
reg_64ecf97d_u0<=1'h0;
else reg_64ecf97d_u0<=GO;
end
always @(posedge CLK or posedge RESET)
begin
if (RESET)
reg_64ecf97d_result_delayed_u0<=1'h0;
else reg_64ecf97d_result_delayed_u0<=reg_64ecf97d_u0;
end
assign mux_u1911_u0=(GO)?1'h0:mux_u1910_u0;
assign or_u1442_u0=GO|or_u1440_u0;
assign RESULT=or_u1442_u0;
assign RESULT_u2213=mux_u1911_u0;
assign RESULT_u2214=receive_go_merge;
assign RESULT_u2215=simplePinWrite_u621;
assign DONE=1'h0;
endmodule



module medianRow2_endianswapper_210027f5_(endianswapper_210027f5_in, endianswapper_210027f5_out);
input		endianswapper_210027f5_in;
output		endianswapper_210027f5_out;
assign endianswapper_210027f5_out=endianswapper_210027f5_in;
endmodule



module medianRow2_endianswapper_4ffcccb0_(endianswapper_4ffcccb0_in, endianswapper_4ffcccb0_out);
input		endianswapper_4ffcccb0_in;
output		endianswapper_4ffcccb0_out;
assign endianswapper_4ffcccb0_out=endianswapper_4ffcccb0_in;
endmodule



module medianRow2_stateVar_fsmState_medianRow2(bus_5081a752_, bus_78b59679_, bus_35ddba70_, bus_3c69759e_, bus_605818f0_);
input		bus_5081a752_;
input		bus_78b59679_;
input		bus_35ddba70_;
input		bus_3c69759e_;
output		bus_605818f0_;
wire		endianswapper_210027f5_out;
wire		endianswapper_4ffcccb0_out;
reg		stateVar_fsmState_medianRow2_u5=1'h0;
assign bus_605818f0_=endianswapper_4ffcccb0_out;
medianRow2_endianswapper_210027f5_ medianRow2_endianswapper_210027f5__1(.endianswapper_210027f5_in(bus_3c69759e_), 
  .endianswapper_210027f5_out(endianswapper_210027f5_out));
medianRow2_endianswapper_4ffcccb0_ medianRow2_endianswapper_4ffcccb0__1(.endianswapper_4ffcccb0_in(stateVar_fsmState_medianRow2_u5), 
  .endianswapper_4ffcccb0_out(endianswapper_4ffcccb0_out));
always @(posedge bus_5081a752_ or posedge bus_78b59679_)
begin
if (bus_78b59679_)
stateVar_fsmState_medianRow2_u5<=1'h0;
else if (bus_35ddba70_)
stateVar_fsmState_medianRow2_u5<=endianswapper_210027f5_out;
end
endmodule



module medianRow2_receive(CLK, RESET, GO, port_2a180d5f_, port_611f60f5_, port_71d2ad28_, RESULT, RESULT_u2216, RESULT_u2217, RESULT_u2218, RESULT_u2219, RESULT_u2220, RESULT_u2221, DONE);
input		CLK;
input		RESET;
input		GO;
input	[31:0]	port_2a180d5f_;
input		port_611f60f5_;
input	[7:0]	port_71d2ad28_;
output		RESULT;
output	[31:0]	RESULT_u2216;
output		RESULT_u2217;
output	[31:0]	RESULT_u2218;
output	[31:0]	RESULT_u2219;
output	[2:0]	RESULT_u2220;
output		RESULT_u2221;
output		DONE;
wire		simplePinWrite;
wire	[31:0]	add;
wire		and_u3781_u0;
wire		or_u1443_u0;
reg		reg_1520cfea_u0=1'h0;
wire	[31:0]	add_u671;
reg		reg_01481d0a_u0=1'h0;
assign simplePinWrite=GO&{1{GO}};
assign add=port_2a180d5f_+32'h0;
assign and_u3781_u0=reg_1520cfea_u0&port_611f60f5_;
assign or_u1443_u0=and_u3781_u0|RESET;
always @(posedge CLK or posedge GO or posedge or_u1443_u0)
begin
if (or_u1443_u0)
reg_1520cfea_u0<=1'h0;
else if (GO)
reg_1520cfea_u0<=1'h1;
else reg_1520cfea_u0<=reg_1520cfea_u0;
end
assign add_u671=port_2a180d5f_+32'h1;
always @(posedge CLK or posedge RESET)
begin
if (RESET)
reg_01481d0a_u0<=1'h0;
else reg_01481d0a_u0<=GO;
end
assign RESULT=GO;
assign RESULT_u2216=add_u671;
assign RESULT_u2217=GO;
assign RESULT_u2218=add;
assign RESULT_u2219={24'b0, port_71d2ad28_};
assign RESULT_u2220=3'h1;
assign RESULT_u2221=simplePinWrite;
assign DONE=reg_01481d0a_u0;
endmodule



module medianRow2_endianswapper_35cfd92f_(endianswapper_35cfd92f_in, endianswapper_35cfd92f_out);
input	[31:0]	endianswapper_35cfd92f_in;
output	[31:0]	endianswapper_35cfd92f_out;
assign endianswapper_35cfd92f_out=endianswapper_35cfd92f_in;
endmodule



module medianRow2_endianswapper_7852ec2c_(endianswapper_7852ec2c_in, endianswapper_7852ec2c_out);
input	[31:0]	endianswapper_7852ec2c_in;
output	[31:0]	endianswapper_7852ec2c_out;
assign endianswapper_7852ec2c_out=endianswapper_7852ec2c_in;
endmodule



module medianRow2_stateVar_i(bus_4a1b0f2c_, bus_3f675d65_, bus_50471737_, bus_47bef13a_, bus_2ff4dce4_, bus_653d013a_, bus_0b237537_);
input		bus_4a1b0f2c_;
input		bus_3f675d65_;
input		bus_50471737_;
input	[31:0]	bus_47bef13a_;
input		bus_2ff4dce4_;
input	[31:0]	bus_653d013a_;
output	[31:0]	bus_0b237537_;
wire	[31:0]	endianswapper_35cfd92f_out;
wire	[31:0]	mux_117c7c4d_u0;
wire		or_0297e826_u0;
wire	[31:0]	endianswapper_7852ec2c_out;
reg	[31:0]	stateVar_i_u47=32'h0;
medianRow2_endianswapper_35cfd92f_ medianRow2_endianswapper_35cfd92f__1(.endianswapper_35cfd92f_in(stateVar_i_u47), 
  .endianswapper_35cfd92f_out(endianswapper_35cfd92f_out));
assign mux_117c7c4d_u0=(bus_50471737_)?bus_47bef13a_:32'h0;
assign or_0297e826_u0=bus_50471737_|bus_2ff4dce4_;
medianRow2_endianswapper_7852ec2c_ medianRow2_endianswapper_7852ec2c__1(.endianswapper_7852ec2c_in(mux_117c7c4d_u0), 
  .endianswapper_7852ec2c_out(endianswapper_7852ec2c_out));
always @(posedge bus_4a1b0f2c_ or posedge bus_3f675d65_)
begin
if (bus_3f675d65_)
stateVar_i_u47<=32'h0;
else if (or_0297e826_u0)
stateVar_i_u47<=endianswapper_7852ec2c_out;
end
assign bus_0b237537_=endianswapper_35cfd92f_out;
endmodule



module medianRow2_simplememoryreferee_546e01d8_(bus_5439b6dd_, bus_37cea00b_, bus_739e5f32_, bus_68cebe70_, bus_6384f2ab_, bus_4baff515_, bus_22d9c211_, bus_27dd4632_, bus_6083945b_, bus_644153d3_, bus_065ccafe_, bus_1166b695_, bus_36e1bb03_, bus_582cec80_, bus_293a0ea1_, bus_0b6dba37_, bus_79e15c10_, bus_7b837a32_, bus_227a1de5_, bus_12e4289b_, bus_749f14bd_);
input		bus_5439b6dd_;
input		bus_37cea00b_;
input		bus_739e5f32_;
input	[31:0]	bus_68cebe70_;
input		bus_6384f2ab_;
input	[31:0]	bus_4baff515_;
input	[31:0]	bus_22d9c211_;
input	[2:0]	bus_27dd4632_;
input		bus_6083945b_;
input		bus_644153d3_;
input	[31:0]	bus_065ccafe_;
input	[31:0]	bus_1166b695_;
input	[2:0]	bus_36e1bb03_;
output	[31:0]	bus_582cec80_;
output	[31:0]	bus_293a0ea1_;
output		bus_0b6dba37_;
output		bus_79e15c10_;
output	[2:0]	bus_7b837a32_;
output		bus_227a1de5_;
output	[31:0]	bus_12e4289b_;
output		bus_749f14bd_;
wire		or_761f6095_u0;
reg		done_qual_u218=1'h0;
wire	[31:0]	mux_5261f60e_u0;
reg		done_qual_u219_u0=1'h0;
wire		or_4bc183b0_u0;
wire		or_3218868a_u0;
wire		and_22227510_u0;
wire		not_39af081b_u0;
wire		not_01a61d72_u0;
wire	[31:0]	mux_5cb9508c_u0;
wire		or_24470a91_u0;
wire		and_74eada73_u0;
wire		or_781a3566_u0;
assign or_761f6095_u0=bus_6083945b_|bus_644153d3_;
always @(posedge bus_5439b6dd_)
begin
if (bus_37cea00b_)
done_qual_u218<=1'h0;
else done_qual_u218<=or_761f6095_u0;
end
assign bus_582cec80_=mux_5261f60e_u0;
assign bus_293a0ea1_=mux_5cb9508c_u0;
assign bus_0b6dba37_=or_24470a91_u0;
assign bus_79e15c10_=or_3218868a_u0;
assign bus_7b837a32_=3'h1;
assign bus_227a1de5_=and_74eada73_u0;
assign bus_12e4289b_=bus_68cebe70_;
assign bus_749f14bd_=and_22227510_u0;
assign mux_5261f60e_u0=(bus_6384f2ab_)?{24'b0, bus_4baff515_[7:0]}:bus_065ccafe_;
always @(posedge bus_5439b6dd_)
begin
if (bus_37cea00b_)
done_qual_u219_u0<=1'h0;
else done_qual_u219_u0<=bus_6384f2ab_;
end
assign or_4bc183b0_u0=bus_6384f2ab_|done_qual_u219_u0;
assign or_3218868a_u0=bus_6384f2ab_|or_761f6095_u0;
assign and_22227510_u0=or_781a3566_u0&bus_739e5f32_;
assign not_39af081b_u0=~bus_739e5f32_;
assign not_01a61d72_u0=~bus_739e5f32_;
assign mux_5cb9508c_u0=(bus_6384f2ab_)?bus_22d9c211_:bus_1166b695_;
assign or_24470a91_u0=bus_6384f2ab_|bus_644153d3_;
assign and_74eada73_u0=or_4bc183b0_u0&bus_739e5f32_;
assign or_781a3566_u0=or_761f6095_u0|done_qual_u218;
endmodule



module medianRow2_globalreset_physical_047a1b81_(bus_01f77c28_, bus_58c6d4a8_, bus_7a17e071_);
input		bus_01f77c28_;
input		bus_58c6d4a8_;
output		bus_7a17e071_;
reg		sample_u57=1'h0;
reg		final_u57=1'h1;
wire		and_1d8b736e_u0;
wire		not_04648992_u0;
reg		glitch_u57=1'h0;
reg		cross_u57=1'h0;
wire		or_296c3f71_u0;
always @(posedge bus_01f77c28_)
begin
sample_u57<=1'h1;
end
always @(posedge bus_01f77c28_)
begin
final_u57<=not_04648992_u0;
end
assign and_1d8b736e_u0=cross_u57&glitch_u57;
assign not_04648992_u0=~and_1d8b736e_u0;
always @(posedge bus_01f77c28_)
begin
glitch_u57<=cross_u57;
end
assign bus_7a17e071_=or_296c3f71_u0;
always @(posedge bus_01f77c28_)
begin
cross_u57<=sample_u57;
end
assign or_296c3f71_u0=bus_58c6d4a8_|final_u57;
endmodule


