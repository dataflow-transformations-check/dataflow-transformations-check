// __  ___ __ ___  _ __   ___  ___ 
// \ \/ / '__/ _ \| '_ \ / _ \/ __|
//  >  <| | | (_) | | | | (_) \__ \
// /_/\_\_|  \___/|_| |_|\___/|___/
// 
// Xronos synthesizer version
// Run date: Fri 23 Feb 2018 14:28:53 +0000
// 

module medianRow3(in1_SEND, RESET, median_COUNT, median_ACK, in1_DATA, median_RDY, CLK, median_DATA, in1_COUNT, median_SEND, in1_ACK);
input		in1_SEND;
input		RESET;
output	[15:0]	median_COUNT;
input		median_ACK;
wire		receive_done;
input	[7:0]	in1_DATA;
input		median_RDY;
wire		receive_go;
input		CLK;
output	[7:0]	median_DATA;
wire		compute_median_done;
wire		compute_median_go;
input	[15:0]	in1_COUNT;
output		median_SEND;
output		in1_ACK;
wire		compute_median;
wire		compute_median_u838;
wire		compute_median_u830;
wire	[2:0]	compute_median_u833;
wire	[2:0]	compute_median_u829;
wire		compute_median_u827;
wire	[31:0]	compute_median_u835;
wire	[7:0]	compute_median_u839;
wire	[2:0]	compute_median_u836;
wire	[15:0]	compute_median_u837;
wire	[31:0]	compute_median_u832;
wire		medianRow3_compute_median_instance_DONE;
wire	[31:0]	compute_median_u831;
wire	[31:0]	compute_median_u828;
wire	[31:0]	compute_median_u826;
wire		compute_median_u834;
wire		receive;
wire	[2:0]	receive_u358;
wire		medianRow3_receive_instance_DONE;
wire	[31:0]	receive_u357;
wire	[31:0]	receive_u354;
wire		receive_u355;
wire		receive_u359;
wire	[31:0]	receive_u356;
wire		bus_5df4c3eb_;
wire		bus_7b181296_;
wire	[2:0]	bus_432eb2fd_;
wire	[31:0]	bus_3bab6525_;
wire	[31:0]	bus_4d3d960f_;
wire	[31:0]	bus_665bc2a1_;
wire		bus_4f92dc83_;
wire	[31:0]	bus_10866236_;
wire	[31:0]	bus_0f205f76_;
wire		bus_1d63cd81_;
wire	[31:0]	bus_04a95b56_;
wire		bus_023d6394_;
wire		bus_30415261_;
wire	[31:0]	bus_5429c24b_;
wire		bus_15e60590_;
wire	[2:0]	bus_37f0c12f_;
wire		scheduler_u853;
wire		scheduler_u854;
wire		medianRow3_scheduler_instance_DONE;
wire		scheduler_u855;
wire		scheduler;
wire		bus_4b0d38a1_;
wire		bus_5c137097_;
wire		bus_0d8e9439_;
wire		bus_6dff35d1_;
wire		bus_783e1a34_;
wire	[31:0]	bus_689020e2_;
wire		bus_478eddae_;
wire	[31:0]	bus_4841690b_;
wire		bus_622585f9_;
assign median_COUNT=compute_median_u837;
assign receive_done=bus_0d8e9439_;
assign receive_go=scheduler_u854;
assign median_DATA=compute_median_u839;
assign compute_median_done=bus_5c137097_;
assign compute_median_go=scheduler_u855;
assign median_SEND=compute_median_u838;
assign in1_ACK=receive_u359;
medianRow3_compute_median medianRow3_compute_median_instance(.CLK(CLK), .RESET(bus_4b0d38a1_), 
  .GO(compute_median_go), .port_0f8b1639_(bus_1d63cd81_), .port_60bf1791_(bus_5429c24b_), 
  .port_20e961db_(bus_1d63cd81_), .port_299e271b_(bus_7b181296_), .port_1713d469_(bus_4d3d960f_), 
  .DONE(medianRow3_compute_median_instance_DONE), .RESULT(compute_median), .RESULT_u2521(compute_median_u826), 
  .RESULT_u2525(compute_median_u827), .RESULT_u2526(compute_median_u828), .RESULT_u2527(compute_median_u829), 
  .RESULT_u2528(compute_median_u830), .RESULT_u2529(compute_median_u831), .RESULT_u2530(compute_median_u832), 
  .RESULT_u2531(compute_median_u833), .RESULT_u2522(compute_median_u834), .RESULT_u2523(compute_median_u835), 
  .RESULT_u2524(compute_median_u836), .RESULT_u2532(compute_median_u837), .RESULT_u2533(compute_median_u838), 
  .RESULT_u2534(compute_median_u839));
medianRow3_receive medianRow3_receive_instance(.CLK(CLK), .RESET(bus_4b0d38a1_), 
  .GO(receive_go), .port_27afde22_(bus_10866236_), .port_6f30814a_(bus_30415261_), 
  .port_74eb927d_(in1_DATA), .DONE(medianRow3_receive_instance_DONE), .RESULT(receive), 
  .RESULT_u2535(receive_u354), .RESULT_u2536(receive_u355), .RESULT_u2537(receive_u356), 
  .RESULT_u2538(receive_u357), .RESULT_u2539(receive_u358), .RESULT_u2540(receive_u359));
medianRow3_simplememoryreferee_7b633bb5_ medianRow3_simplememoryreferee_7b633bb5__1(.bus_14498e15_(CLK), 
  .bus_5e15c8a5_(bus_4b0d38a1_), .bus_648fffd5_(bus_478eddae_), .bus_6b06ae3e_(bus_689020e2_), 
  .bus_7bb27ade_(compute_median_u834), .bus_01446f3a_(compute_median_u835), .bus_569f9626_(3'h1), 
  .bus_665bc2a1_(bus_665bc2a1_), .bus_3bab6525_(bus_3bab6525_), .bus_5df4c3eb_(bus_5df4c3eb_), 
  .bus_4f92dc83_(bus_4f92dc83_), .bus_432eb2fd_(bus_432eb2fd_), .bus_4d3d960f_(bus_4d3d960f_), 
  .bus_7b181296_(bus_7b181296_));
medianRow3_stateVar_i medianRow3_stateVar_i_1(.bus_79900325_(CLK), .bus_348b48ef_(bus_4b0d38a1_), 
  .bus_7c007d4a_(receive), .bus_6b523cb5_(receive_u354), .bus_3716b2ee_(compute_median), 
  .bus_4eae9785_(32'h0), .bus_10866236_(bus_10866236_));
medianRow3_simplememoryreferee_13054e58_ medianRow3_simplememoryreferee_13054e58__1(.bus_627590ed_(CLK), 
  .bus_0d38c250_(bus_4b0d38a1_), .bus_0320350c_(bus_783e1a34_), .bus_00986f64_(bus_4841690b_), 
  .bus_14c4cbd3_(receive_u355), .bus_464aa575_({24'b0, receive_u357[7:0]}), .bus_48c2c08e_(receive_u356), 
  .bus_1a33876a_(3'h1), .bus_6e4147b8_(compute_median_u827), .bus_032ef2db_(compute_median_u830), 
  .bus_465dcc4c_(compute_median_u832), .bus_65d8cf43_(compute_median_u831), .bus_2eb8ff89_(3'h1), 
  .bus_04a95b56_(bus_04a95b56_), .bus_0f205f76_(bus_0f205f76_), .bus_023d6394_(bus_023d6394_), 
  .bus_15e60590_(bus_15e60590_), .bus_37f0c12f_(bus_37f0c12f_), .bus_30415261_(bus_30415261_), 
  .bus_5429c24b_(bus_5429c24b_), .bus_1d63cd81_(bus_1d63cd81_));
medianRow3_scheduler medianRow3_scheduler_instance(.CLK(CLK), .RESET(bus_4b0d38a1_), 
  .GO(bus_6dff35d1_), .port_78892330_(bus_622585f9_), .port_6e92cc31_(bus_10866236_), 
  .port_595bdfd9_(median_RDY), .port_3baa0de3_(in1_SEND), .port_6293b8ae_(compute_median_done), 
  .port_78aa26e8_(receive_done), .DONE(medianRow3_scheduler_instance_DONE), .RESULT(scheduler), 
  .RESULT_u2541(scheduler_u853), .RESULT_u2542(scheduler_u854), .RESULT_u2543(scheduler_u855));
medianRow3_globalreset_physical_73b1f459_ medianRow3_globalreset_physical_73b1f459__1(.bus_5a59e08b_(CLK), 
  .bus_2d427e0a_(RESET), .bus_4b0d38a1_(bus_4b0d38a1_));
assign bus_5c137097_=medianRow3_compute_median_instance_DONE&{1{medianRow3_compute_median_instance_DONE}};
assign bus_0d8e9439_=medianRow3_receive_instance_DONE&{1{medianRow3_receive_instance_DONE}};
medianRow3_Kicker_71 medianRow3_Kicker_71_1(.CLK(CLK), .RESET(bus_4b0d38a1_), .bus_6dff35d1_(bus_6dff35d1_));
medianRow3_structuralmemory_6841107c_ medianRow3_structuralmemory_6841107c__1(.CLK_u105(CLK), 
  .bus_3d3efe39_(bus_4b0d38a1_), .bus_31b68a67_(bus_3bab6525_), .bus_3cf9e450_(3'h1), 
  .bus_027b291d_(bus_4f92dc83_), .bus_1f414236_(bus_0f205f76_), .bus_4d4d408a_(3'h1), 
  .bus_3ad6d8b7_(bus_15e60590_), .bus_57eb5423_(bus_023d6394_), .bus_02d47618_(bus_04a95b56_), 
  .bus_689020e2_(bus_689020e2_), .bus_478eddae_(bus_478eddae_), .bus_4841690b_(bus_4841690b_), 
  .bus_783e1a34_(bus_783e1a34_));
medianRow3_stateVar_fsmState_medianRow3 medianRow3_stateVar_fsmState_medianRow3_1(.bus_6a846e6f_(CLK), 
  .bus_75cd2d9b_(bus_4b0d38a1_), .bus_6c0a99f7_(scheduler), .bus_390925d7_(scheduler_u853), 
  .bus_622585f9_(bus_622585f9_));
endmodule



module medianRow3_compute_median(CLK, RESET, GO, port_299e271b_, port_1713d469_, port_0f8b1639_, port_60bf1791_, port_20e961db_, RESULT, RESULT_u2521, RESULT_u2522, RESULT_u2523, RESULT_u2524, RESULT_u2525, RESULT_u2526, RESULT_u2527, RESULT_u2528, RESULT_u2529, RESULT_u2530, RESULT_u2531, RESULT_u2532, RESULT_u2533, RESULT_u2534, DONE);
input		CLK;
input		RESET;
input		GO;
input		port_299e271b_;
input	[31:0]	port_1713d469_;
input		port_0f8b1639_;
input	[31:0]	port_60bf1791_;
input		port_20e961db_;
output		RESULT;
output	[31:0]	RESULT_u2521;
output		RESULT_u2522;
output	[31:0]	RESULT_u2523;
output	[2:0]	RESULT_u2524;
output		RESULT_u2525;
output	[31:0]	RESULT_u2526;
output	[2:0]	RESULT_u2527;
output		RESULT_u2528;
output	[31:0]	RESULT_u2529;
output	[31:0]	RESULT_u2530;
output	[2:0]	RESULT_u2531;
output	[15:0]	RESULT_u2532;
output		RESULT_u2533;
output	[7:0]	RESULT_u2534;
output		DONE;
wire		and_u4328_u0;
wire		and_u4329_u0;
reg	[31:0]	syncEnable_u1692=32'h0;
wire signed	[32:0]	lessThan_b_signed;
wire		lessThan;
wire signed	[32:0]	lessThan_a_signed;
wire		and_u4330_u0;
wire		and_u4331_u0;
wire		not_u999_u0;
reg	[31:0]	syncEnable_u1693_u0=32'h0;
wire	[31:0]	add;
wire		and_u4332_u0;
wire	[31:0]	add_u815;
wire	[31:0]	add_u816;
wire		and_u4333_u0;
wire signed	[31:0]	greaterThan_a_signed;
wire signed	[31:0]	greaterThan_b_signed;
wire		greaterThan;
wire		and_u4334_u0;
wire		and_u4335_u0;
wire		not_u1000_u0;
wire	[31:0]	add_u817;
wire		and_u4336_u0;
wire	[31:0]	add_u818;
wire	[31:0]	add_u819;
wire		and_u4337_u0;
wire	[31:0]	add_u820;
wire		or_u1678_u0;
reg		reg_72777ce1_u0=1'h0;
wire		and_u4338_u0;
wire	[31:0]	add_u821;
wire	[31:0]	add_u822;
reg		reg_34847a95_u0=1'h0;
wire		and_u4339_u0;
wire		or_u1679_u0;
wire	[31:0]	mux_u2211;
wire		or_u1680_u0;
wire	[31:0]	mux_u2212_u0;
reg	[31:0]	syncEnable_u1694_u0=32'h0;
reg	[31:0]	syncEnable_u1695_u0=32'h0;
reg	[31:0]	syncEnable_u1696_u0=32'h0;
reg	[31:0]	syncEnable_u1697_u0=32'h0;
reg	[31:0]	syncEnable_u1698_u0=32'h0;
reg		block_GO_delayed_u124=1'h0;
reg		block_GO_delayed_result_delayed_u24=1'h0;
reg	[31:0]	syncEnable_u1699_u0=32'h0;
wire		and_u4340_u0;
wire		or_u1681_u0;
wire		and_u4341_u0;
reg		syncEnable_u1700_u0=1'h0;
reg	[31:0]	syncEnable_u1701_u0=32'h0;
reg		reg_0d6f0d44_u0=1'h0;
reg		reg_57373019_u0=1'h0;
wire	[31:0]	mux_u2213_u0;
reg	[31:0]	syncEnable_u1702_u0=32'h0;
wire		mux_u2214_u0;
reg		and_delayed_u425=1'h0;
reg		reg_0d6f0d44_result_delayed_u0=1'h0;
wire	[31:0]	mux_u2215_u0;
reg		and_delayed_u426_u0=1'h0;
wire	[31:0]	add_u823;
reg	[31:0]	syncEnable_u1703_u0=32'h0;
reg		block_GO_delayed_u125_u0=1'h0;
reg	[31:0]	syncEnable_u1704_u0=32'h0;
reg	[31:0]	syncEnable_u1705_u0=32'h0;
reg	[31:0]	syncEnable_u1706_u0=32'h0;
reg	[31:0]	syncEnable_u1707_u0=32'h0;
reg		syncEnable_u1708_u0=1'h0;
wire		or_u1682_u0;
wire	[31:0]	mux_u2216_u0;
wire		or_u1683_u0;
wire	[31:0]	mux_u2217_u0;
reg	[31:0]	syncEnable_u1709_u0=32'h0;
wire	[31:0]	mux_u2218_u0;
wire		or_u1684_u0;
wire	[31:0]	mux_u2219_u0;
wire	[31:0]	mux_u2220_u0;
wire		mux_u2221_u0;
wire	[31:0]	mux_u2222_u0;
wire	[31:0]	mux_u2223_u0;
wire	[15:0]	add_u824;
wire		scoreboard_221403b5_and;
reg		scoreboard_221403b5_reg1=1'h0;
wire		bus_21f19452_;
wire		scoreboard_221403b5_resOr0;
wire		scoreboard_221403b5_resOr1;
reg		scoreboard_221403b5_reg0=1'h0;
reg	[15:0]	syncEnable_u1710_u0=16'h0;
wire	[31:0]	latch_48f80df1_out;
reg	[31:0]	latch_48f80df1_reg=32'h0;
wire	[31:0]	latch_206b2079_out;
reg	[31:0]	latch_206b2079_reg=32'h0;
reg		reg_53083032_u0=1'h0;
wire	[31:0]	latch_2be68afa_out;
reg	[31:0]	latch_2be68afa_reg=32'h0;
reg		latch_195ce0c1_reg=1'h0;
wire		latch_195ce0c1_out;
wire	[31:0]	latch_327ef1e0_out;
reg	[31:0]	latch_327ef1e0_reg=32'h0;
wire	[15:0]	lessThan_u117_b_unsigned;
wire	[15:0]	lessThan_u117_a_unsigned;
wire		lessThan_u117;
wire		andOp;
wire		and_u4342_u0;
wire		and_u4343_u0;
wire		not_u1001_u0;
wire		and_u4344_u0;
reg	[31:0]	fbReg_tmp_row0_u59=32'h0;
wire	[15:0]	mux_u2224_u0;
reg	[31:0]	fbReg_tmp_u59=32'h0;
wire	[31:0]	mux_u2225_u0;
wire		or_u1685_u0;
wire	[31:0]	mux_u2226_u0;
reg	[31:0]	fbReg_tmp_row1_u59=32'h0;
reg	[15:0]	fbReg_idx_u59=16'h0;
wire	[31:0]	mux_u2227_u0;
wire		mux_u2228_u0;
wire	[31:0]	mux_u2229_u0;
reg		loopControl_u122=1'h0;
reg		syncEnable_u1711_u0=1'h0;
reg	[31:0]	fbReg_tmp_row_u59=32'h0;
reg		fbReg_swapped_u59=1'h0;
wire		and_u4345_u0;
wire	[7:0]	simplePinWrite;
wire		simplePinWrite_u674;
wire	[15:0]	simplePinWrite_u675;
wire		or_u1686_u0;
wire	[31:0]	mux_u2230_u0;
reg		reg_6fed06cb_u0=1'h0;
reg		reg_19514ea2_u0=1'h0;
assign and_u4328_u0=and_u4343_u0&or_u1685_u0;
assign and_u4329_u0=and_u4331_u0&or_u1684_u0;
always @(posedge CLK)
begin
if (or_u1684_u0)
syncEnable_u1692<=mux_u2218_u0;
end
assign lessThan_a_signed={1'b0, mux_u2220_u0};
assign lessThan_b_signed=33'h64;
assign lessThan=lessThan_a_signed<lessThan_b_signed;
assign and_u4330_u0=or_u1684_u0&not_u999_u0;
assign and_u4331_u0=or_u1684_u0&lessThan;
assign not_u999_u0=~lessThan;
always @(posedge CLK)
begin
if (or_u1684_u0)
syncEnable_u1693_u0<=mux_u2219_u0;
end
assign add=mux_u2220_u0+32'h0;
assign and_u4332_u0=and_u4329_u0&port_299e271b_;
assign add_u815=mux_u2220_u0+32'h1;
assign add_u816=add_u815+32'h0;
assign and_u4333_u0=and_u4329_u0&port_20e961db_;
assign greaterThan_a_signed=syncEnable_u1706_u0;
assign greaterThan_b_signed=syncEnable_u1704_u0;
assign greaterThan=greaterThan_a_signed>greaterThan_b_signed;
assign and_u4334_u0=block_GO_delayed_u125_u0&greaterThan;
assign and_u4335_u0=block_GO_delayed_u125_u0&not_u1000_u0;
assign not_u1000_u0=~greaterThan;
assign add_u817=syncEnable_u1703_u0+32'h0;
assign and_u4336_u0=and_u4341_u0&port_299e271b_;
assign add_u818=syncEnable_u1703_u0+32'h1;
assign add_u819=add_u818+32'h0;
assign and_u4337_u0=and_u4341_u0&port_20e961db_;
assign add_u820=syncEnable_u1703_u0+32'h0;
assign or_u1678_u0=and_u4338_u0|RESET;
always @(posedge CLK or posedge block_GO_delayed_u124 or posedge or_u1678_u0)
begin
if (or_u1678_u0)
reg_72777ce1_u0<=1'h0;
else if (block_GO_delayed_u124)
reg_72777ce1_u0<=1'h1;
else reg_72777ce1_u0<=reg_72777ce1_u0;
end
assign and_u4338_u0=reg_72777ce1_u0&port_20e961db_;
assign add_u821=syncEnable_u1703_u0+32'h1;
assign add_u822=add_u821+32'h0;
always @(posedge CLK or posedge block_GO_delayed_result_delayed_u24 or posedge or_u1679_u0)
begin
if (or_u1679_u0)
reg_34847a95_u0<=1'h0;
else if (block_GO_delayed_result_delayed_u24)
reg_34847a95_u0<=1'h1;
else reg_34847a95_u0<=reg_34847a95_u0;
end
assign and_u4339_u0=reg_34847a95_u0&port_20e961db_;
assign or_u1679_u0=and_u4339_u0|RESET;
assign mux_u2211=({32{block_GO_delayed_u124}}&syncEnable_u1699_u0)|({32{block_GO_delayed_result_delayed_u24}}&syncEnable_u1694_u0)|({32{and_u4341_u0}}&add_u819);
assign or_u1680_u0=block_GO_delayed_u124|block_GO_delayed_result_delayed_u24;
assign mux_u2212_u0=(block_GO_delayed_u124)?syncEnable_u1696_u0:syncEnable_u1698_u0;
always @(posedge CLK)
begin
if (and_u4341_u0)
syncEnable_u1694_u0<=add_u822;
end
always @(posedge CLK)
begin
if (and_u4341_u0)
syncEnable_u1695_u0<=port_1713d469_;
end
always @(posedge CLK)
begin
if (and_u4341_u0)
syncEnable_u1696_u0<=port_60bf1791_;
end
always @(posedge CLK)
begin
if (and_u4341_u0)
syncEnable_u1697_u0<=port_60bf1791_;
end
always @(posedge CLK)
begin
if (and_u4341_u0)
syncEnable_u1698_u0<=port_1713d469_;
end
always @(posedge CLK or posedge RESET)
begin
if (RESET)
block_GO_delayed_u124<=1'h0;
else block_GO_delayed_u124<=and_u4341_u0;
end
always @(posedge CLK or posedge RESET)
begin
if (RESET)
block_GO_delayed_result_delayed_u24<=1'h0;
else block_GO_delayed_result_delayed_u24<=block_GO_delayed_u124;
end
always @(posedge CLK)
begin
if (and_u4341_u0)
syncEnable_u1699_u0<=add_u820;
end
assign and_u4340_u0=and_u4335_u0&block_GO_delayed_u125_u0;
assign or_u1681_u0=reg_57373019_u0|and_delayed_u426_u0;
assign and_u4341_u0=and_u4334_u0&block_GO_delayed_u125_u0;
always @(posedge CLK)
begin
if (block_GO_delayed_u125_u0)
syncEnable_u1700_u0<=syncEnable_u1708_u0;
end
always @(posedge CLK)
begin
if (block_GO_delayed_u125_u0)
syncEnable_u1701_u0<=syncEnable_u1705_u0;
end
always @(posedge CLK or posedge RESET)
begin
if (RESET)
reg_0d6f0d44_u0<=1'h0;
else reg_0d6f0d44_u0<=and_delayed_u425;
end
always @(posedge CLK or posedge RESET)
begin
if (RESET)
reg_57373019_u0<=1'h0;
else reg_57373019_u0<=reg_0d6f0d44_result_delayed_u0;
end
assign mux_u2213_u0=(reg_57373019_u0)?syncEnable_u1695_u0:syncEnable_u1702_u0;
always @(posedge CLK)
begin
if (block_GO_delayed_u125_u0)
syncEnable_u1702_u0<=syncEnable_u1707_u0;
end
assign mux_u2214_u0=(reg_57373019_u0)?1'h1:syncEnable_u1700_u0;
always @(posedge CLK or posedge RESET)
begin
if (RESET)
and_delayed_u425<=1'h0;
else and_delayed_u425<=and_u4341_u0;
end
always @(posedge CLK or posedge RESET)
begin
if (RESET)
reg_0d6f0d44_result_delayed_u0<=1'h0;
else reg_0d6f0d44_result_delayed_u0<=reg_0d6f0d44_u0;
end
assign mux_u2215_u0=(reg_57373019_u0)?syncEnable_u1697_u0:syncEnable_u1701_u0;
always @(posedge CLK or posedge RESET)
begin
if (RESET)
and_delayed_u426_u0<=1'h0;
else and_delayed_u426_u0<=and_u4340_u0;
end
assign add_u823=mux_u2220_u0+32'h1;
always @(posedge CLK)
begin
if (and_u4329_u0)
syncEnable_u1703_u0<=mux_u2220_u0;
end
always @(posedge CLK or posedge RESET)
begin
if (RESET)
block_GO_delayed_u125_u0<=1'h0;
else block_GO_delayed_u125_u0<=and_u4329_u0;
end
always @(posedge CLK)
begin
if (and_u4329_u0)
syncEnable_u1704_u0<=port_60bf1791_;
end
always @(posedge CLK)
begin
if (and_u4329_u0)
syncEnable_u1705_u0<=mux_u2222_u0;
end
always @(posedge CLK)
begin
if (and_u4329_u0)
syncEnable_u1706_u0<=port_1713d469_;
end
always @(posedge CLK)
begin
if (and_u4329_u0)
syncEnable_u1707_u0<=mux_u2223_u0;
end
always @(posedge CLK)
begin
if (and_u4329_u0)
syncEnable_u1708_u0<=mux_u2221_u0;
end
assign or_u1682_u0=and_u4329_u0|and_u4341_u0;
assign mux_u2216_u0=(and_u4329_u0)?add:add_u817;
assign or_u1683_u0=and_u4329_u0|and_u4341_u0;
assign mux_u2217_u0=({32{or_u1680_u0}}&mux_u2211)|({32{and_u4329_u0}}&add_u816)|({32{and_u4341_u0}}&mux_u2211);
always @(posedge CLK)
begin
if (and_u4329_u0)
syncEnable_u1709_u0<=add_u823;
end
assign mux_u2218_u0=(and_u4328_u0)?mux_u2227_u0:syncEnable_u1704_u0;
assign or_u1684_u0=and_u4328_u0|or_u1681_u0;
assign mux_u2219_u0=(and_u4328_u0)?mux_u2226_u0:syncEnable_u1706_u0;
assign mux_u2220_u0=(and_u4328_u0)?32'h0:syncEnable_u1709_u0;
assign mux_u2221_u0=(and_u4328_u0)?1'h0:mux_u2214_u0;
assign mux_u2222_u0=(and_u4328_u0)?mux_u2229_u0:mux_u2215_u0;
assign mux_u2223_u0=(and_u4328_u0)?mux_u2225_u0:mux_u2213_u0;
assign add_u824=mux_u2224_u0+16'h1;
assign scoreboard_221403b5_and=scoreboard_221403b5_resOr0&scoreboard_221403b5_resOr1;
always @(posedge CLK)
begin
if (bus_21f19452_)
scoreboard_221403b5_reg1<=1'h0;
else if (reg_53083032_u0)
scoreboard_221403b5_reg1<=1'h1;
else scoreboard_221403b5_reg1<=scoreboard_221403b5_reg1;
end
assign bus_21f19452_=scoreboard_221403b5_and|RESET;
assign scoreboard_221403b5_resOr0=or_u1681_u0|scoreboard_221403b5_reg0;
assign scoreboard_221403b5_resOr1=reg_53083032_u0|scoreboard_221403b5_reg1;
always @(posedge CLK)
begin
if (bus_21f19452_)
scoreboard_221403b5_reg0<=1'h0;
else if (or_u1681_u0)
scoreboard_221403b5_reg0<=1'h1;
else scoreboard_221403b5_reg0<=scoreboard_221403b5_reg0;
end
always @(posedge CLK)
begin
if (and_u4328_u0)
syncEnable_u1710_u0<=add_u824;
end
assign latch_48f80df1_out=(or_u1681_u0)?syncEnable_u1692:latch_48f80df1_reg;
always @(posedge CLK)
begin
if (or_u1681_u0)
latch_48f80df1_reg<=syncEnable_u1692;
end
assign latch_206b2079_out=(or_u1681_u0)?syncEnable_u1693_u0:latch_206b2079_reg;
always @(posedge CLK)
begin
if (or_u1681_u0)
latch_206b2079_reg<=syncEnable_u1693_u0;
end
always @(posedge CLK or posedge RESET)
begin
if (RESET)
reg_53083032_u0<=1'h0;
else reg_53083032_u0<=or_u1681_u0;
end
assign latch_2be68afa_out=(or_u1681_u0)?mux_u2213_u0:latch_2be68afa_reg;
always @(posedge CLK)
begin
if (or_u1681_u0)
latch_2be68afa_reg<=mux_u2213_u0;
end
always @(posedge CLK)
begin
if (or_u1681_u0)
latch_195ce0c1_reg<=mux_u2214_u0;
end
assign latch_195ce0c1_out=(or_u1681_u0)?mux_u2214_u0:latch_195ce0c1_reg;
assign latch_327ef1e0_out=(or_u1681_u0)?mux_u2215_u0:latch_327ef1e0_reg;
always @(posedge CLK)
begin
if (or_u1681_u0)
latch_327ef1e0_reg<=mux_u2215_u0;
end
assign lessThan_u117_a_unsigned=mux_u2224_u0;
assign lessThan_u117_b_unsigned=16'h65;
assign lessThan_u117=lessThan_u117_a_unsigned<lessThan_u117_b_unsigned;
assign andOp=lessThan_u117&mux_u2228_u0;
assign and_u4342_u0=or_u1685_u0&not_u1001_u0;
assign and_u4343_u0=or_u1685_u0&andOp;
assign not_u1001_u0=~andOp;
assign and_u4344_u0=and_u4342_u0&or_u1685_u0;
always @(posedge CLK)
begin
if (scoreboard_221403b5_and)
fbReg_tmp_row0_u59<=latch_48f80df1_out;
end
assign mux_u2224_u0=(GO)?16'h0:fbReg_idx_u59;
always @(posedge CLK)
begin
if (scoreboard_221403b5_and)
fbReg_tmp_u59<=latch_2be68afa_out;
end
assign mux_u2225_u0=(GO)?32'h0:fbReg_tmp_u59;
assign or_u1685_u0=GO|loopControl_u122;
assign mux_u2226_u0=(GO)?32'h0:fbReg_tmp_row_u59;
always @(posedge CLK)
begin
if (scoreboard_221403b5_and)
fbReg_tmp_row1_u59<=latch_327ef1e0_out;
end
always @(posedge CLK)
begin
if (scoreboard_221403b5_and)
fbReg_idx_u59<=syncEnable_u1710_u0;
end
assign mux_u2227_u0=(GO)?32'h0:fbReg_tmp_row0_u59;
assign mux_u2228_u0=(GO)?1'h0:fbReg_swapped_u59;
assign mux_u2229_u0=(GO)?32'h0:fbReg_tmp_row1_u59;
always @(posedge CLK or posedge syncEnable_u1711_u0)
begin
if (syncEnable_u1711_u0)
loopControl_u122<=1'h0;
else loopControl_u122<=scoreboard_221403b5_and;
end
always @(posedge CLK)
begin
if (GO)
syncEnable_u1711_u0<=RESET;
end
always @(posedge CLK)
begin
if (scoreboard_221403b5_and)
fbReg_tmp_row_u59<=latch_206b2079_out;
end
always @(posedge CLK)
begin
if (scoreboard_221403b5_and)
fbReg_swapped_u59<=latch_195ce0c1_out;
end
assign and_u4345_u0=reg_19514ea2_u0&port_299e271b_;
assign simplePinWrite=port_1713d469_[7:0];
assign simplePinWrite_u674=reg_19514ea2_u0&{1{reg_19514ea2_u0}};
assign simplePinWrite_u675=16'h1&{16{1'h1}};
assign or_u1686_u0=or_u1682_u0|reg_19514ea2_u0;
assign mux_u2230_u0=(or_u1682_u0)?mux_u2216_u0:32'h32;
always @(posedge CLK or posedge RESET)
begin
if (RESET)
reg_6fed06cb_u0<=1'h0;
else reg_6fed06cb_u0<=reg_19514ea2_u0;
end
always @(posedge CLK or posedge RESET)
begin
if (RESET)
reg_19514ea2_u0<=1'h0;
else reg_19514ea2_u0<=and_u4344_u0;
end
assign RESULT=GO;
assign RESULT_u2521=32'h0;
assign RESULT_u2522=or_u1686_u0;
assign RESULT_u2523=mux_u2230_u0;
assign RESULT_u2524=3'h1;
assign RESULT_u2525=or_u1683_u0;
assign RESULT_u2526=mux_u2217_u0;
assign RESULT_u2527=3'h1;
assign RESULT_u2528=or_u1680_u0;
assign RESULT_u2529=mux_u2217_u0;
assign RESULT_u2530=mux_u2212_u0;
assign RESULT_u2531=3'h1;
assign RESULT_u2532=simplePinWrite_u675;
assign RESULT_u2533=simplePinWrite_u674;
assign RESULT_u2534=simplePinWrite;
assign DONE=reg_6fed06cb_u0;
endmodule



module medianRow3_receive(CLK, RESET, GO, port_27afde22_, port_6f30814a_, port_74eb927d_, RESULT, RESULT_u2535, RESULT_u2536, RESULT_u2537, RESULT_u2538, RESULT_u2539, RESULT_u2540, DONE);
input		CLK;
input		RESET;
input		GO;
input	[31:0]	port_27afde22_;
input		port_6f30814a_;
input	[7:0]	port_74eb927d_;
output		RESULT;
output	[31:0]	RESULT_u2535;
output		RESULT_u2536;
output	[31:0]	RESULT_u2537;
output	[31:0]	RESULT_u2538;
output	[2:0]	RESULT_u2539;
output		RESULT_u2540;
output		DONE;
wire		simplePinWrite;
wire	[31:0]	add;
wire		or_u1687_u0;
reg		reg_111d4e63_u0=1'h0;
wire		and_u4346_u0;
wire	[31:0]	add_u825;
reg		reg_1ff16458_u0=1'h0;
assign simplePinWrite=GO&{1{GO}};
assign add=port_27afde22_+32'h0;
assign or_u1687_u0=and_u4346_u0|RESET;
always @(posedge CLK or posedge GO or posedge or_u1687_u0)
begin
if (or_u1687_u0)
reg_111d4e63_u0<=1'h0;
else if (GO)
reg_111d4e63_u0<=1'h1;
else reg_111d4e63_u0<=reg_111d4e63_u0;
end
assign and_u4346_u0=reg_111d4e63_u0&port_6f30814a_;
assign add_u825=port_27afde22_+32'h1;
always @(posedge CLK or posedge RESET)
begin
if (RESET)
reg_1ff16458_u0<=1'h0;
else reg_1ff16458_u0<=GO;
end
assign RESULT=GO;
assign RESULT_u2535=add_u825;
assign RESULT_u2536=GO;
assign RESULT_u2537=add;
assign RESULT_u2538={24'b0, port_74eb927d_};
assign RESULT_u2539=3'h1;
assign RESULT_u2540=simplePinWrite;
assign DONE=reg_1ff16458_u0;
endmodule



module medianRow3_simplememoryreferee_7b633bb5_(bus_14498e15_, bus_5e15c8a5_, bus_648fffd5_, bus_6b06ae3e_, bus_7bb27ade_, bus_01446f3a_, bus_569f9626_, bus_665bc2a1_, bus_3bab6525_, bus_5df4c3eb_, bus_4f92dc83_, bus_432eb2fd_, bus_4d3d960f_, bus_7b181296_);
input		bus_14498e15_;
input		bus_5e15c8a5_;
input		bus_648fffd5_;
input	[31:0]	bus_6b06ae3e_;
input		bus_7bb27ade_;
input	[31:0]	bus_01446f3a_;
input	[2:0]	bus_569f9626_;
output	[31:0]	bus_665bc2a1_;
output	[31:0]	bus_3bab6525_;
output		bus_5df4c3eb_;
output		bus_4f92dc83_;
output	[2:0]	bus_432eb2fd_;
output	[31:0]	bus_4d3d960f_;
output		bus_7b181296_;
assign bus_665bc2a1_=32'h0;
assign bus_3bab6525_=bus_01446f3a_;
assign bus_5df4c3eb_=1'h0;
assign bus_4f92dc83_=bus_7bb27ade_;
assign bus_432eb2fd_=3'h1;
assign bus_4d3d960f_=bus_6b06ae3e_;
assign bus_7b181296_=bus_648fffd5_;
endmodule



module medianRow3_endianswapper_65f8c946_(endianswapper_65f8c946_in, endianswapper_65f8c946_out);
input	[31:0]	endianswapper_65f8c946_in;
output	[31:0]	endianswapper_65f8c946_out;
assign endianswapper_65f8c946_out=endianswapper_65f8c946_in;
endmodule



module medianRow3_endianswapper_741ea0c0_(endianswapper_741ea0c0_in, endianswapper_741ea0c0_out);
input	[31:0]	endianswapper_741ea0c0_in;
output	[31:0]	endianswapper_741ea0c0_out;
assign endianswapper_741ea0c0_out=endianswapper_741ea0c0_in;
endmodule



module medianRow3_stateVar_i(bus_79900325_, bus_348b48ef_, bus_7c007d4a_, bus_6b523cb5_, bus_3716b2ee_, bus_4eae9785_, bus_10866236_);
input		bus_79900325_;
input		bus_348b48ef_;
input		bus_7c007d4a_;
input	[31:0]	bus_6b523cb5_;
input		bus_3716b2ee_;
input	[31:0]	bus_4eae9785_;
output	[31:0]	bus_10866236_;
wire		or_18648dcd_u0;
wire	[31:0]	endianswapper_65f8c946_out;
wire	[31:0]	endianswapper_741ea0c0_out;
wire	[31:0]	mux_705a0855_u0;
reg	[31:0]	stateVar_i_u61=32'h0;
assign or_18648dcd_u0=bus_7c007d4a_|bus_3716b2ee_;
assign bus_10866236_=endianswapper_741ea0c0_out;
medianRow3_endianswapper_65f8c946_ medianRow3_endianswapper_65f8c946__1(.endianswapper_65f8c946_in(mux_705a0855_u0), 
  .endianswapper_65f8c946_out(endianswapper_65f8c946_out));
medianRow3_endianswapper_741ea0c0_ medianRow3_endianswapper_741ea0c0__1(.endianswapper_741ea0c0_in(stateVar_i_u61), 
  .endianswapper_741ea0c0_out(endianswapper_741ea0c0_out));
assign mux_705a0855_u0=(bus_7c007d4a_)?bus_6b523cb5_:32'h0;
always @(posedge bus_79900325_ or posedge bus_348b48ef_)
begin
if (bus_348b48ef_)
stateVar_i_u61<=32'h0;
else if (or_18648dcd_u0)
stateVar_i_u61<=endianswapper_65f8c946_out;
end
endmodule



module medianRow3_simplememoryreferee_13054e58_(bus_627590ed_, bus_0d38c250_, bus_0320350c_, bus_00986f64_, bus_14c4cbd3_, bus_464aa575_, bus_48c2c08e_, bus_1a33876a_, bus_6e4147b8_, bus_032ef2db_, bus_465dcc4c_, bus_65d8cf43_, bus_2eb8ff89_, bus_04a95b56_, bus_0f205f76_, bus_023d6394_, bus_15e60590_, bus_37f0c12f_, bus_30415261_, bus_5429c24b_, bus_1d63cd81_);
input		bus_627590ed_;
input		bus_0d38c250_;
input		bus_0320350c_;
input	[31:0]	bus_00986f64_;
input		bus_14c4cbd3_;
input	[31:0]	bus_464aa575_;
input	[31:0]	bus_48c2c08e_;
input	[2:0]	bus_1a33876a_;
input		bus_6e4147b8_;
input		bus_032ef2db_;
input	[31:0]	bus_465dcc4c_;
input	[31:0]	bus_65d8cf43_;
input	[2:0]	bus_2eb8ff89_;
output	[31:0]	bus_04a95b56_;
output	[31:0]	bus_0f205f76_;
output		bus_023d6394_;
output		bus_15e60590_;
output	[2:0]	bus_37f0c12f_;
output		bus_30415261_;
output	[31:0]	bus_5429c24b_;
output		bus_1d63cd81_;
wire	[31:0]	mux_43d56ad5_u0;
wire		or_4406e7e3_u0;
wire		or_442677ef_u0;
wire		and_4a8a4ee3_u0;
wire		and_2750ad3a_u0;
wire		or_69d2ba5c_u0;
wire	[31:0]	mux_39e77957_u0;
wire		or_35fd3eef_u0;
reg		done_qual_u246=1'h0;
wire		not_43308845_u0;
wire		not_0da07e96_u0;
wire		or_5f3fe327_u0;
reg		done_qual_u247_u0=1'h0;
assign mux_43d56ad5_u0=(bus_14c4cbd3_)?{24'b0, bus_464aa575_[7:0]}:bus_465dcc4c_;
assign or_4406e7e3_u0=bus_6e4147b8_|bus_032ef2db_;
assign or_442677ef_u0=or_4406e7e3_u0|done_qual_u246;
assign and_4a8a4ee3_u0=or_442677ef_u0&bus_0320350c_;
assign and_2750ad3a_u0=or_69d2ba5c_u0&bus_0320350c_;
assign or_69d2ba5c_u0=bus_14c4cbd3_|done_qual_u247_u0;
assign mux_39e77957_u0=(bus_14c4cbd3_)?bus_48c2c08e_:bus_65d8cf43_;
assign or_35fd3eef_u0=bus_14c4cbd3_|or_4406e7e3_u0;
always @(posedge bus_627590ed_)
begin
if (bus_0d38c250_)
done_qual_u246<=1'h0;
else done_qual_u246<=or_4406e7e3_u0;
end
assign not_43308845_u0=~bus_0320350c_;
assign not_0da07e96_u0=~bus_0320350c_;
assign or_5f3fe327_u0=bus_14c4cbd3_|bus_032ef2db_;
assign bus_04a95b56_=mux_43d56ad5_u0;
assign bus_0f205f76_=mux_39e77957_u0;
assign bus_023d6394_=or_5f3fe327_u0;
assign bus_15e60590_=or_35fd3eef_u0;
assign bus_37f0c12f_=3'h1;
assign bus_30415261_=and_2750ad3a_u0;
assign bus_5429c24b_=bus_00986f64_;
assign bus_1d63cd81_=and_4a8a4ee3_u0;
always @(posedge bus_627590ed_)
begin
if (bus_0d38c250_)
done_qual_u247_u0<=1'h0;
else done_qual_u247_u0<=bus_14c4cbd3_;
end
endmodule



module medianRow3_scheduler(CLK, RESET, GO, port_78892330_, port_6e92cc31_, port_595bdfd9_, port_3baa0de3_, port_6293b8ae_, port_78aa26e8_, RESULT, RESULT_u2541, RESULT_u2542, RESULT_u2543, DONE);
input		CLK;
input		RESET;
input		GO;
input		port_78892330_;
input	[31:0]	port_6e92cc31_;
input		port_595bdfd9_;
input		port_3baa0de3_;
input		port_6293b8ae_;
input		port_78aa26e8_;
output		RESULT;
output		RESULT_u2541;
output		RESULT_u2542;
output		RESULT_u2543;
output		DONE;
wire signed	[31:0]	lessThan_b_signed;
wire signed	[31:0]	lessThan_a_signed;
wire		lessThan;
wire		equals;
wire signed	[31:0]	equals_a_signed;
wire signed	[31:0]	equals_b_signed;
wire signed	[31:0]	equals_u260_b_signed;
wire		equals_u260;
wire signed	[31:0]	equals_u260_a_signed;
wire		and_u4347_u0;
wire		and_u4348_u0;
wire		not_u1002_u0;
wire		andOp;
wire		and_u4349_u0;
wire		and_u4350_u0;
wire		not_u1003_u0;
wire		simplePinWrite;
wire		and_u4351_u0;
wire		and_u4352_u0;
wire signed	[31:0]	equals_u261_b_signed;
wire signed	[31:0]	equals_u261_a_signed;
wire		equals_u261;
wire		and_u4353_u0;
wire		not_u1004_u0;
wire		and_u4354_u0;
wire		andOp_u93;
wire		and_u4355_u0;
wire		and_u4356_u0;
wire		not_u1005_u0;
wire		simplePinWrite_u676;
wire		not_u1006_u0;
wire		and_u4357_u0;
wire		and_u4358_u0;
wire		and_u4359_u0;
wire		not_u1007_u0;
wire		and_u4360_u0;
wire		simplePinWrite_u677;
wire		and_u4361_u0;
wire		or_u1688_u0;
wire		and_u4362_u0;
reg		and_delayed_u427=1'h0;
wire		and_u4363_u0;
reg		and_delayed_u428_u0=1'h0;
wire		and_u4364_u0;
wire		or_u1689_u0;
wire		or_u1690_u0;
wire		and_u4365_u0;
wire		mux_u2231;
wire		or_u1691_u0;
reg		reg_62ca37b0_u0=1'h0;
wire		and_u4366_u0;
wire		and_u4367_u0;
wire		or_u1692_u0;
wire		and_u4368_u0;
reg		and_delayed_u429_u0=1'h0;
wire		receive_go_merge;
wire		or_u1693_u0;
wire		mux_u2232_u0;
wire		and_u4369_u0;
reg		syncEnable_u1712=1'h0;
wire		or_u1694_u0;
reg		loopControl_u123=1'h0;
wire		or_u1695_u0;
wire		mux_u2233_u0;
reg		reg_6b967523_u0=1'h0;
reg		reg_2ba3f614_u0=1'h0;
assign lessThan_a_signed=port_6e92cc31_;
assign lessThan_b_signed=32'h65;
assign lessThan=lessThan_a_signed<lessThan_b_signed;
assign equals_a_signed=port_6e92cc31_;
assign equals_b_signed=32'h65;
assign equals=equals_a_signed==equals_b_signed;
assign equals_u260_a_signed={31'b0, port_78892330_};
assign equals_u260_b_signed=32'h0;
assign equals_u260=equals_u260_a_signed==equals_u260_b_signed;
assign and_u4347_u0=and_u4369_u0&equals_u260;
assign and_u4348_u0=and_u4369_u0&not_u1002_u0;
assign not_u1002_u0=~equals_u260;
assign andOp=lessThan&port_3baa0de3_;
assign and_u4349_u0=and_u4352_u0&not_u1003_u0;
assign and_u4350_u0=and_u4352_u0&andOp;
assign not_u1003_u0=~andOp;
assign simplePinWrite=and_u4351_u0&{1{and_u4351_u0}};
assign and_u4351_u0=and_u4350_u0&and_u4352_u0;
assign and_u4352_u0=and_u4347_u0&and_u4369_u0;
assign equals_u261_a_signed={31'b0, port_78892330_};
assign equals_u261_b_signed=32'h1;
assign equals_u261=equals_u261_a_signed==equals_u261_b_signed;
assign and_u4353_u0=and_u4369_u0&equals_u261;
assign not_u1004_u0=~equals_u261;
assign and_u4354_u0=and_u4369_u0&not_u1004_u0;
assign andOp_u93=lessThan&port_3baa0de3_;
assign and_u4355_u0=and_u4368_u0&andOp_u93;
assign and_u4356_u0=and_u4368_u0&not_u1005_u0;
assign not_u1005_u0=~andOp_u93;
assign simplePinWrite_u676=and_u4366_u0&{1{and_u4366_u0}};
assign not_u1006_u0=~equals;
assign and_u4357_u0=and_u4365_u0&equals;
assign and_u4358_u0=and_u4365_u0&not_u1006_u0;
assign and_u4359_u0=and_u4364_u0&port_595bdfd9_;
assign not_u1007_u0=~port_595bdfd9_;
assign and_u4360_u0=and_u4364_u0&not_u1007_u0;
assign simplePinWrite_u677=and_u4361_u0&{1{and_u4361_u0}};
assign and_u4361_u0=and_u4359_u0&and_u4364_u0;
assign or_u1688_u0=port_6293b8ae_|and_delayed_u427;
assign and_u4362_u0=and_u4360_u0&and_u4364_u0;
always @(posedge CLK or posedge RESET)
begin
if (RESET)
and_delayed_u427<=1'h0;
else and_delayed_u427<=and_u4362_u0;
end
assign and_u4363_u0=and_u4358_u0&and_u4365_u0;
always @(posedge CLK or posedge RESET)
begin
if (RESET)
and_delayed_u428_u0<=1'h0;
else and_delayed_u428_u0<=and_u4363_u0;
end
assign and_u4364_u0=and_u4357_u0&and_u4365_u0;
assign or_u1689_u0=or_u1688_u0|and_delayed_u428_u0;
assign or_u1690_u0=reg_62ca37b0_u0|or_u1689_u0;
assign and_u4365_u0=and_u4356_u0&and_u4368_u0;
assign mux_u2231=(and_u4366_u0)?1'h1:1'h0;
assign or_u1691_u0=and_u4366_u0|and_u4361_u0;
always @(posedge CLK or posedge RESET)
begin
if (RESET)
reg_62ca37b0_u0<=1'h0;
else reg_62ca37b0_u0<=and_u4366_u0;
end
assign and_u4366_u0=and_u4355_u0&and_u4368_u0;
assign and_u4367_u0=and_u4354_u0&and_u4369_u0;
assign or_u1692_u0=or_u1690_u0|and_delayed_u429_u0;
assign and_u4368_u0=and_u4353_u0&and_u4369_u0;
always @(posedge CLK or posedge RESET)
begin
if (RESET)
and_delayed_u429_u0<=1'h0;
else and_delayed_u429_u0<=and_u4367_u0;
end
assign receive_go_merge=simplePinWrite|simplePinWrite_u676;
assign or_u1693_u0=and_u4351_u0|or_u1691_u0;
assign mux_u2232_u0=(and_u4351_u0)?1'h1:mux_u2231;
assign and_u4369_u0=or_u1694_u0&or_u1694_u0;
always @(posedge CLK)
begin
if (reg_6b967523_u0)
syncEnable_u1712<=RESET;
end
assign or_u1694_u0=reg_6b967523_u0|loopControl_u123;
always @(posedge CLK or posedge syncEnable_u1712)
begin
if (syncEnable_u1712)
loopControl_u123<=1'h0;
else loopControl_u123<=or_u1692_u0;
end
assign or_u1695_u0=GO|or_u1693_u0;
assign mux_u2233_u0=(GO)?1'h0:mux_u2232_u0;
always @(posedge CLK or posedge RESET)
begin
if (RESET)
reg_6b967523_u0<=1'h0;
else reg_6b967523_u0<=reg_2ba3f614_u0;
end
always @(posedge CLK or posedge RESET)
begin
if (RESET)
reg_2ba3f614_u0<=1'h0;
else reg_2ba3f614_u0<=GO;
end
assign RESULT=or_u1695_u0;
assign RESULT_u2541=mux_u2233_u0;
assign RESULT_u2542=receive_go_merge;
assign RESULT_u2543=simplePinWrite_u677;
assign DONE=1'h0;
endmodule



module medianRow3_globalreset_physical_73b1f459_(bus_5a59e08b_, bus_2d427e0a_, bus_4b0d38a1_);
input		bus_5a59e08b_;
input		bus_2d427e0a_;
output		bus_4b0d38a1_;
reg		sample_u71=1'h0;
reg		glitch_u71=1'h0;
wire		or_2dd838e8_u0;
reg		final_u71=1'h1;
wire		and_65a16137_u0;
wire		not_05afc944_u0;
reg		cross_u71=1'h0;
always @(posedge bus_5a59e08b_)
begin
sample_u71<=1'h1;
end
always @(posedge bus_5a59e08b_)
begin
glitch_u71<=cross_u71;
end
assign or_2dd838e8_u0=bus_2d427e0a_|final_u71;
always @(posedge bus_5a59e08b_)
begin
final_u71<=not_05afc944_u0;
end
assign and_65a16137_u0=cross_u71&glitch_u71;
assign bus_4b0d38a1_=or_2dd838e8_u0;
assign not_05afc944_u0=~and_65a16137_u0;
always @(posedge bus_5a59e08b_)
begin
cross_u71<=sample_u71;
end
endmodule



module medianRow3_Kicker_71(CLK, RESET, bus_6dff35d1_);
input		CLK;
input		RESET;
output		bus_6dff35d1_;
wire		bus_7c820ed7_;
wire		bus_4a29b47c_;
wire		bus_63a63d5d_;
reg		kicker_res=1'h0;
reg		kicker_1=1'h0;
reg		kicker_2=1'h0;
wire		bus_3926a82b_;
assign bus_7c820ed7_=~RESET;
assign bus_6dff35d1_=kicker_res;
assign bus_4a29b47c_=kicker_1&bus_7c820ed7_&bus_63a63d5d_;
assign bus_63a63d5d_=~kicker_2;
always @(posedge CLK)
begin
kicker_res<=bus_4a29b47c_;
end
always @(posedge CLK)
begin
kicker_1<=bus_7c820ed7_;
end
always @(posedge CLK)
begin
kicker_2<=bus_3926a82b_;
end
assign bus_3926a82b_=bus_7c820ed7_&kicker_1;
endmodule



module medianRow3_forge_memory_101x32_171(CLK, ENA, WEA, DINA, ENB, ADDRA, ADDRB, DOUTA, DOUTB, DONEA, DONEB);
input		CLK;
input		ENA;
input		WEA;
input	[31:0]	DINA;
input		ENB;
input	[31:0]	ADDRA;
input	[31:0]	ADDRB;
output	[31:0]	DOUTA;
output	[31:0]	DOUTB;
output		DONEA;
output		DONEB;
wire		wea_0;
wire	[31:0]	pre_douta_0;
wire	[31:0]	pre_doutb_0;
wire		wea_1;
wire	[31:0]	pre_douta_1;
wire	[31:0]	pre_doutb_1;
reg		wea_done;
reg		rea_done;
reg	[31:0]	mux_outa;
reg	[31:0]	mux_outb;
assign wea_0=WEA&(ADDRA[31:6]==26'h0);
assign wea_1=WEA&(ADDRA[31:6]==26'h1);
always @(posedge CLK)
begin
wea_done<=WEA;
end
assign DONEA=rea_done|wea_done;
always @(posedge CLK)
begin
end
assign DONEB=ENB;
always @(pre_douta_0 or pre_douta_1 or ADDRA)
begin
case (ADDRA[31:6])26'd0:mux_outa=pre_douta_0;
26'd1:mux_outa=pre_douta_1;
default:mux_outa=32'h0;
endcase end
assign DOUTA=mux_outa;
always @(pre_doutb_0 or pre_doutb_1 or ADDRB)
begin
case (ADDRB[31:6])26'd0:mux_outb=pre_doutb_0;
26'd1:mux_outb=pre_doutb_1;
default:mux_outb=32'h0;
endcase end
assign DOUTB=mux_outb;
// Memory array element: COL: 0, ROW: 0
//  Initialization of Dual Port LUT ram now done through explicit
// parameter setting.
RAM64X1D#(.INIT(64'h0000000000000000))RAM64X1D_instance_3776(.WCLK(CLK), .WE(wea_0), 
  .A0(ADDRA[0]), .A1(ADDRA[1]), .A2(ADDRA[2]), .A3(ADDRA[3]), .A4(ADDRA[4]), .A5(ADDRA[5]), 
  .D(DINA[0]), .SPO(pre_douta_0[0]), .DPRA0(ADDRB[0]), .DPRA1(ADDRB[1]), .DPRA2(ADDRB[2]), 
  .DPRA3(ADDRB[3]), .DPRA4(ADDRB[4]), .DPRA5(ADDRB[5]), .DPO(pre_doutb_0[0]));
// Memory array element: COL: 0, ROW: 1
//  Initialization of Dual Port LUT ram now done through explicit
// parameter setting.
RAM64X1D#(.INIT(64'h0000000000000000))RAM64X1D_instance_3777(.WCLK(CLK), .WE(wea_0), 
  .A0(ADDRA[0]), .A1(ADDRA[1]), .A2(ADDRA[2]), .A3(ADDRA[3]), .A4(ADDRA[4]), .A5(ADDRA[5]), 
  .D(DINA[1]), .SPO(pre_douta_0[1]), .DPRA0(ADDRB[0]), .DPRA1(ADDRB[1]), .DPRA2(ADDRB[2]), 
  .DPRA3(ADDRB[3]), .DPRA4(ADDRB[4]), .DPRA5(ADDRB[5]), .DPO(pre_doutb_0[1]));
// Memory array element: COL: 0, ROW: 2
//  Initialization of Dual Port LUT ram now done through explicit
// parameter setting.
RAM64X1D#(.INIT(64'h0000000000000000))RAM64X1D_instance_3778(.WCLK(CLK), .WE(wea_0), 
  .A0(ADDRA[0]), .A1(ADDRA[1]), .A2(ADDRA[2]), .A3(ADDRA[3]), .A4(ADDRA[4]), .A5(ADDRA[5]), 
  .D(DINA[2]), .SPO(pre_douta_0[2]), .DPRA0(ADDRB[0]), .DPRA1(ADDRB[1]), .DPRA2(ADDRB[2]), 
  .DPRA3(ADDRB[3]), .DPRA4(ADDRB[4]), .DPRA5(ADDRB[5]), .DPO(pre_doutb_0[2]));
// Memory array element: COL: 0, ROW: 3
//  Initialization of Dual Port LUT ram now done through explicit
// parameter setting.
RAM64X1D#(.INIT(64'h0000000000000000))RAM64X1D_instance_3779(.WCLK(CLK), .WE(wea_0), 
  .A0(ADDRA[0]), .A1(ADDRA[1]), .A2(ADDRA[2]), .A3(ADDRA[3]), .A4(ADDRA[4]), .A5(ADDRA[5]), 
  .D(DINA[3]), .SPO(pre_douta_0[3]), .DPRA0(ADDRB[0]), .DPRA1(ADDRB[1]), .DPRA2(ADDRB[2]), 
  .DPRA3(ADDRB[3]), .DPRA4(ADDRB[4]), .DPRA5(ADDRB[5]), .DPO(pre_doutb_0[3]));
// Memory array element: COL: 0, ROW: 4
//  Initialization of Dual Port LUT ram now done through explicit
// parameter setting.
RAM64X1D#(.INIT(64'h0000000000000000))RAM64X1D_instance_3780(.WCLK(CLK), .WE(wea_0), 
  .A0(ADDRA[0]), .A1(ADDRA[1]), .A2(ADDRA[2]), .A3(ADDRA[3]), .A4(ADDRA[4]), .A5(ADDRA[5]), 
  .D(DINA[4]), .SPO(pre_douta_0[4]), .DPRA0(ADDRB[0]), .DPRA1(ADDRB[1]), .DPRA2(ADDRB[2]), 
  .DPRA3(ADDRB[3]), .DPRA4(ADDRB[4]), .DPRA5(ADDRB[5]), .DPO(pre_doutb_0[4]));
// Memory array element: COL: 0, ROW: 5
//  Initialization of Dual Port LUT ram now done through explicit
// parameter setting.
RAM64X1D#(.INIT(64'h0000000000000000))RAM64X1D_instance_3781(.WCLK(CLK), .WE(wea_0), 
  .A0(ADDRA[0]), .A1(ADDRA[1]), .A2(ADDRA[2]), .A3(ADDRA[3]), .A4(ADDRA[4]), .A5(ADDRA[5]), 
  .D(DINA[5]), .SPO(pre_douta_0[5]), .DPRA0(ADDRB[0]), .DPRA1(ADDRB[1]), .DPRA2(ADDRB[2]), 
  .DPRA3(ADDRB[3]), .DPRA4(ADDRB[4]), .DPRA5(ADDRB[5]), .DPO(pre_doutb_0[5]));
// Memory array element: COL: 0, ROW: 6
//  Initialization of Dual Port LUT ram now done through explicit
// parameter setting.
RAM64X1D#(.INIT(64'h0000000000000000))RAM64X1D_instance_3782(.WCLK(CLK), .WE(wea_0), 
  .A0(ADDRA[0]), .A1(ADDRA[1]), .A2(ADDRA[2]), .A3(ADDRA[3]), .A4(ADDRA[4]), .A5(ADDRA[5]), 
  .D(DINA[6]), .SPO(pre_douta_0[6]), .DPRA0(ADDRB[0]), .DPRA1(ADDRB[1]), .DPRA2(ADDRB[2]), 
  .DPRA3(ADDRB[3]), .DPRA4(ADDRB[4]), .DPRA5(ADDRB[5]), .DPO(pre_doutb_0[6]));
// Memory array element: COL: 0, ROW: 7
//  Initialization of Dual Port LUT ram now done through explicit
// parameter setting.
RAM64X1D#(.INIT(64'h0000000000000000))RAM64X1D_instance_3783(.WCLK(CLK), .WE(wea_0), 
  .A0(ADDRA[0]), .A1(ADDRA[1]), .A2(ADDRA[2]), .A3(ADDRA[3]), .A4(ADDRA[4]), .A5(ADDRA[5]), 
  .D(DINA[7]), .SPO(pre_douta_0[7]), .DPRA0(ADDRB[0]), .DPRA1(ADDRB[1]), .DPRA2(ADDRB[2]), 
  .DPRA3(ADDRB[3]), .DPRA4(ADDRB[4]), .DPRA5(ADDRB[5]), .DPO(pre_doutb_0[7]));
// Memory array element: COL: 0, ROW: 8
//  Initialization of Dual Port LUT ram now done through explicit
// parameter setting.
RAM64X1D#(.INIT(64'h0000000000000000))RAM64X1D_instance_3784(.WCLK(CLK), .WE(wea_0), 
  .A0(ADDRA[0]), .A1(ADDRA[1]), .A2(ADDRA[2]), .A3(ADDRA[3]), .A4(ADDRA[4]), .A5(ADDRA[5]), 
  .D(DINA[8]), .SPO(pre_douta_0[8]), .DPRA0(ADDRB[0]), .DPRA1(ADDRB[1]), .DPRA2(ADDRB[2]), 
  .DPRA3(ADDRB[3]), .DPRA4(ADDRB[4]), .DPRA5(ADDRB[5]), .DPO(pre_doutb_0[8]));
// Memory array element: COL: 0, ROW: 9
//  Initialization of Dual Port LUT ram now done through explicit
// parameter setting.
RAM64X1D#(.INIT(64'h0000000000000000))RAM64X1D_instance_3785(.WCLK(CLK), .WE(wea_0), 
  .A0(ADDRA[0]), .A1(ADDRA[1]), .A2(ADDRA[2]), .A3(ADDRA[3]), .A4(ADDRA[4]), .A5(ADDRA[5]), 
  .D(DINA[9]), .SPO(pre_douta_0[9]), .DPRA0(ADDRB[0]), .DPRA1(ADDRB[1]), .DPRA2(ADDRB[2]), 
  .DPRA3(ADDRB[3]), .DPRA4(ADDRB[4]), .DPRA5(ADDRB[5]), .DPO(pre_doutb_0[9]));
// Memory array element: COL: 0, ROW: 10
//  Initialization of Dual Port LUT ram now done through explicit
// parameter setting.
RAM64X1D#(.INIT(64'h0000000000000000))RAM64X1D_instance_3786(.WCLK(CLK), .WE(wea_0), 
  .A0(ADDRA[0]), .A1(ADDRA[1]), .A2(ADDRA[2]), .A3(ADDRA[3]), .A4(ADDRA[4]), .A5(ADDRA[5]), 
  .D(DINA[10]), .SPO(pre_douta_0[10]), .DPRA0(ADDRB[0]), .DPRA1(ADDRB[1]), .DPRA2(ADDRB[2]), 
  .DPRA3(ADDRB[3]), .DPRA4(ADDRB[4]), .DPRA5(ADDRB[5]), .DPO(pre_doutb_0[10]));
// Memory array element: COL: 0, ROW: 11
//  Initialization of Dual Port LUT ram now done through explicit
// parameter setting.
RAM64X1D#(.INIT(64'h0000000000000000))RAM64X1D_instance_3787(.WCLK(CLK), .WE(wea_0), 
  .A0(ADDRA[0]), .A1(ADDRA[1]), .A2(ADDRA[2]), .A3(ADDRA[3]), .A4(ADDRA[4]), .A5(ADDRA[5]), 
  .D(DINA[11]), .SPO(pre_douta_0[11]), .DPRA0(ADDRB[0]), .DPRA1(ADDRB[1]), .DPRA2(ADDRB[2]), 
  .DPRA3(ADDRB[3]), .DPRA4(ADDRB[4]), .DPRA5(ADDRB[5]), .DPO(pre_doutb_0[11]));
// Memory array element: COL: 0, ROW: 12
//  Initialization of Dual Port LUT ram now done through explicit
// parameter setting.
RAM64X1D#(.INIT(64'h0000000000000000))RAM64X1D_instance_3788(.WCLK(CLK), .WE(wea_0), 
  .A0(ADDRA[0]), .A1(ADDRA[1]), .A2(ADDRA[2]), .A3(ADDRA[3]), .A4(ADDRA[4]), .A5(ADDRA[5]), 
  .D(DINA[12]), .SPO(pre_douta_0[12]), .DPRA0(ADDRB[0]), .DPRA1(ADDRB[1]), .DPRA2(ADDRB[2]), 
  .DPRA3(ADDRB[3]), .DPRA4(ADDRB[4]), .DPRA5(ADDRB[5]), .DPO(pre_doutb_0[12]));
// Memory array element: COL: 0, ROW: 13
//  Initialization of Dual Port LUT ram now done through explicit
// parameter setting.
RAM64X1D#(.INIT(64'h0000000000000000))RAM64X1D_instance_3789(.WCLK(CLK), .WE(wea_0), 
  .A0(ADDRA[0]), .A1(ADDRA[1]), .A2(ADDRA[2]), .A3(ADDRA[3]), .A4(ADDRA[4]), .A5(ADDRA[5]), 
  .D(DINA[13]), .SPO(pre_douta_0[13]), .DPRA0(ADDRB[0]), .DPRA1(ADDRB[1]), .DPRA2(ADDRB[2]), 
  .DPRA3(ADDRB[3]), .DPRA4(ADDRB[4]), .DPRA5(ADDRB[5]), .DPO(pre_doutb_0[13]));
// Memory array element: COL: 0, ROW: 14
//  Initialization of Dual Port LUT ram now done through explicit
// parameter setting.
RAM64X1D#(.INIT(64'h0000000000000000))RAM64X1D_instance_3790(.WCLK(CLK), .WE(wea_0), 
  .A0(ADDRA[0]), .A1(ADDRA[1]), .A2(ADDRA[2]), .A3(ADDRA[3]), .A4(ADDRA[4]), .A5(ADDRA[5]), 
  .D(DINA[14]), .SPO(pre_douta_0[14]), .DPRA0(ADDRB[0]), .DPRA1(ADDRB[1]), .DPRA2(ADDRB[2]), 
  .DPRA3(ADDRB[3]), .DPRA4(ADDRB[4]), .DPRA5(ADDRB[5]), .DPO(pre_doutb_0[14]));
// Memory array element: COL: 0, ROW: 15
//  Initialization of Dual Port LUT ram now done through explicit
// parameter setting.
RAM64X1D#(.INIT(64'h0000000000000000))RAM64X1D_instance_3791(.WCLK(CLK), .WE(wea_0), 
  .A0(ADDRA[0]), .A1(ADDRA[1]), .A2(ADDRA[2]), .A3(ADDRA[3]), .A4(ADDRA[4]), .A5(ADDRA[5]), 
  .D(DINA[15]), .SPO(pre_douta_0[15]), .DPRA0(ADDRB[0]), .DPRA1(ADDRB[1]), .DPRA2(ADDRB[2]), 
  .DPRA3(ADDRB[3]), .DPRA4(ADDRB[4]), .DPRA5(ADDRB[5]), .DPO(pre_doutb_0[15]));
// Memory array element: COL: 0, ROW: 16
//  Initialization of Dual Port LUT ram now done through explicit
// parameter setting.
RAM64X1D#(.INIT(64'h0000000000000000))RAM64X1D_instance_3792(.WCLK(CLK), .WE(wea_0), 
  .A0(ADDRA[0]), .A1(ADDRA[1]), .A2(ADDRA[2]), .A3(ADDRA[3]), .A4(ADDRA[4]), .A5(ADDRA[5]), 
  .D(DINA[16]), .SPO(pre_douta_0[16]), .DPRA0(ADDRB[0]), .DPRA1(ADDRB[1]), .DPRA2(ADDRB[2]), 
  .DPRA3(ADDRB[3]), .DPRA4(ADDRB[4]), .DPRA5(ADDRB[5]), .DPO(pre_doutb_0[16]));
// Memory array element: COL: 0, ROW: 17
//  Initialization of Dual Port LUT ram now done through explicit
// parameter setting.
RAM64X1D#(.INIT(64'h0000000000000000))RAM64X1D_instance_3793(.WCLK(CLK), .WE(wea_0), 
  .A0(ADDRA[0]), .A1(ADDRA[1]), .A2(ADDRA[2]), .A3(ADDRA[3]), .A4(ADDRA[4]), .A5(ADDRA[5]), 
  .D(DINA[17]), .SPO(pre_douta_0[17]), .DPRA0(ADDRB[0]), .DPRA1(ADDRB[1]), .DPRA2(ADDRB[2]), 
  .DPRA3(ADDRB[3]), .DPRA4(ADDRB[4]), .DPRA5(ADDRB[5]), .DPO(pre_doutb_0[17]));
// Memory array element: COL: 0, ROW: 18
//  Initialization of Dual Port LUT ram now done through explicit
// parameter setting.
RAM64X1D#(.INIT(64'h0000000000000000))RAM64X1D_instance_3794(.WCLK(CLK), .WE(wea_0), 
  .A0(ADDRA[0]), .A1(ADDRA[1]), .A2(ADDRA[2]), .A3(ADDRA[3]), .A4(ADDRA[4]), .A5(ADDRA[5]), 
  .D(DINA[18]), .SPO(pre_douta_0[18]), .DPRA0(ADDRB[0]), .DPRA1(ADDRB[1]), .DPRA2(ADDRB[2]), 
  .DPRA3(ADDRB[3]), .DPRA4(ADDRB[4]), .DPRA5(ADDRB[5]), .DPO(pre_doutb_0[18]));
// Memory array element: COL: 0, ROW: 19
//  Initialization of Dual Port LUT ram now done through explicit
// parameter setting.
RAM64X1D#(.INIT(64'h0000000000000000))RAM64X1D_instance_3795(.WCLK(CLK), .WE(wea_0), 
  .A0(ADDRA[0]), .A1(ADDRA[1]), .A2(ADDRA[2]), .A3(ADDRA[3]), .A4(ADDRA[4]), .A5(ADDRA[5]), 
  .D(DINA[19]), .SPO(pre_douta_0[19]), .DPRA0(ADDRB[0]), .DPRA1(ADDRB[1]), .DPRA2(ADDRB[2]), 
  .DPRA3(ADDRB[3]), .DPRA4(ADDRB[4]), .DPRA5(ADDRB[5]), .DPO(pre_doutb_0[19]));
// Memory array element: COL: 0, ROW: 20
//  Initialization of Dual Port LUT ram now done through explicit
// parameter setting.
RAM64X1D#(.INIT(64'h0000000000000000))RAM64X1D_instance_3796(.WCLK(CLK), .WE(wea_0), 
  .A0(ADDRA[0]), .A1(ADDRA[1]), .A2(ADDRA[2]), .A3(ADDRA[3]), .A4(ADDRA[4]), .A5(ADDRA[5]), 
  .D(DINA[20]), .SPO(pre_douta_0[20]), .DPRA0(ADDRB[0]), .DPRA1(ADDRB[1]), .DPRA2(ADDRB[2]), 
  .DPRA3(ADDRB[3]), .DPRA4(ADDRB[4]), .DPRA5(ADDRB[5]), .DPO(pre_doutb_0[20]));
// Memory array element: COL: 0, ROW: 21
//  Initialization of Dual Port LUT ram now done through explicit
// parameter setting.
RAM64X1D#(.INIT(64'h0000000000000000))RAM64X1D_instance_3797(.WCLK(CLK), .WE(wea_0), 
  .A0(ADDRA[0]), .A1(ADDRA[1]), .A2(ADDRA[2]), .A3(ADDRA[3]), .A4(ADDRA[4]), .A5(ADDRA[5]), 
  .D(DINA[21]), .SPO(pre_douta_0[21]), .DPRA0(ADDRB[0]), .DPRA1(ADDRB[1]), .DPRA2(ADDRB[2]), 
  .DPRA3(ADDRB[3]), .DPRA4(ADDRB[4]), .DPRA5(ADDRB[5]), .DPO(pre_doutb_0[21]));
// Memory array element: COL: 0, ROW: 22
//  Initialization of Dual Port LUT ram now done through explicit
// parameter setting.
RAM64X1D#(.INIT(64'h0000000000000000))RAM64X1D_instance_3798(.WCLK(CLK), .WE(wea_0), 
  .A0(ADDRA[0]), .A1(ADDRA[1]), .A2(ADDRA[2]), .A3(ADDRA[3]), .A4(ADDRA[4]), .A5(ADDRA[5]), 
  .D(DINA[22]), .SPO(pre_douta_0[22]), .DPRA0(ADDRB[0]), .DPRA1(ADDRB[1]), .DPRA2(ADDRB[2]), 
  .DPRA3(ADDRB[3]), .DPRA4(ADDRB[4]), .DPRA5(ADDRB[5]), .DPO(pre_doutb_0[22]));
// Memory array element: COL: 0, ROW: 23
//  Initialization of Dual Port LUT ram now done through explicit
// parameter setting.
RAM64X1D#(.INIT(64'h0000000000000000))RAM64X1D_instance_3799(.WCLK(CLK), .WE(wea_0), 
  .A0(ADDRA[0]), .A1(ADDRA[1]), .A2(ADDRA[2]), .A3(ADDRA[3]), .A4(ADDRA[4]), .A5(ADDRA[5]), 
  .D(DINA[23]), .SPO(pre_douta_0[23]), .DPRA0(ADDRB[0]), .DPRA1(ADDRB[1]), .DPRA2(ADDRB[2]), 
  .DPRA3(ADDRB[3]), .DPRA4(ADDRB[4]), .DPRA5(ADDRB[5]), .DPO(pre_doutb_0[23]));
// Memory array element: COL: 0, ROW: 24
//  Initialization of Dual Port LUT ram now done through explicit
// parameter setting.
RAM64X1D#(.INIT(64'h0000000000000000))RAM64X1D_instance_3800(.WCLK(CLK), .WE(wea_0), 
  .A0(ADDRA[0]), .A1(ADDRA[1]), .A2(ADDRA[2]), .A3(ADDRA[3]), .A4(ADDRA[4]), .A5(ADDRA[5]), 
  .D(DINA[24]), .SPO(pre_douta_0[24]), .DPRA0(ADDRB[0]), .DPRA1(ADDRB[1]), .DPRA2(ADDRB[2]), 
  .DPRA3(ADDRB[3]), .DPRA4(ADDRB[4]), .DPRA5(ADDRB[5]), .DPO(pre_doutb_0[24]));
// Memory array element: COL: 0, ROW: 25
//  Initialization of Dual Port LUT ram now done through explicit
// parameter setting.
RAM64X1D#(.INIT(64'h0000000000000000))RAM64X1D_instance_3801(.WCLK(CLK), .WE(wea_0), 
  .A0(ADDRA[0]), .A1(ADDRA[1]), .A2(ADDRA[2]), .A3(ADDRA[3]), .A4(ADDRA[4]), .A5(ADDRA[5]), 
  .D(DINA[25]), .SPO(pre_douta_0[25]), .DPRA0(ADDRB[0]), .DPRA1(ADDRB[1]), .DPRA2(ADDRB[2]), 
  .DPRA3(ADDRB[3]), .DPRA4(ADDRB[4]), .DPRA5(ADDRB[5]), .DPO(pre_doutb_0[25]));
// Memory array element: COL: 0, ROW: 26
//  Initialization of Dual Port LUT ram now done through explicit
// parameter setting.
RAM64X1D#(.INIT(64'h0000000000000000))RAM64X1D_instance_3802(.WCLK(CLK), .WE(wea_0), 
  .A0(ADDRA[0]), .A1(ADDRA[1]), .A2(ADDRA[2]), .A3(ADDRA[3]), .A4(ADDRA[4]), .A5(ADDRA[5]), 
  .D(DINA[26]), .SPO(pre_douta_0[26]), .DPRA0(ADDRB[0]), .DPRA1(ADDRB[1]), .DPRA2(ADDRB[2]), 
  .DPRA3(ADDRB[3]), .DPRA4(ADDRB[4]), .DPRA5(ADDRB[5]), .DPO(pre_doutb_0[26]));
// Memory array element: COL: 0, ROW: 27
//  Initialization of Dual Port LUT ram now done through explicit
// parameter setting.
RAM64X1D#(.INIT(64'h0000000000000000))RAM64X1D_instance_3803(.WCLK(CLK), .WE(wea_0), 
  .A0(ADDRA[0]), .A1(ADDRA[1]), .A2(ADDRA[2]), .A3(ADDRA[3]), .A4(ADDRA[4]), .A5(ADDRA[5]), 
  .D(DINA[27]), .SPO(pre_douta_0[27]), .DPRA0(ADDRB[0]), .DPRA1(ADDRB[1]), .DPRA2(ADDRB[2]), 
  .DPRA3(ADDRB[3]), .DPRA4(ADDRB[4]), .DPRA5(ADDRB[5]), .DPO(pre_doutb_0[27]));
// Memory array element: COL: 0, ROW: 28
//  Initialization of Dual Port LUT ram now done through explicit
// parameter setting.
RAM64X1D#(.INIT(64'h0000000000000000))RAM64X1D_instance_3804(.WCLK(CLK), .WE(wea_0), 
  .A0(ADDRA[0]), .A1(ADDRA[1]), .A2(ADDRA[2]), .A3(ADDRA[3]), .A4(ADDRA[4]), .A5(ADDRA[5]), 
  .D(DINA[28]), .SPO(pre_douta_0[28]), .DPRA0(ADDRB[0]), .DPRA1(ADDRB[1]), .DPRA2(ADDRB[2]), 
  .DPRA3(ADDRB[3]), .DPRA4(ADDRB[4]), .DPRA5(ADDRB[5]), .DPO(pre_doutb_0[28]));
// Memory array element: COL: 0, ROW: 29
//  Initialization of Dual Port LUT ram now done through explicit
// parameter setting.
RAM64X1D#(.INIT(64'h0000000000000000))RAM64X1D_instance_3805(.WCLK(CLK), .WE(wea_0), 
  .A0(ADDRA[0]), .A1(ADDRA[1]), .A2(ADDRA[2]), .A3(ADDRA[3]), .A4(ADDRA[4]), .A5(ADDRA[5]), 
  .D(DINA[29]), .SPO(pre_douta_0[29]), .DPRA0(ADDRB[0]), .DPRA1(ADDRB[1]), .DPRA2(ADDRB[2]), 
  .DPRA3(ADDRB[3]), .DPRA4(ADDRB[4]), .DPRA5(ADDRB[5]), .DPO(pre_doutb_0[29]));
// Memory array element: COL: 0, ROW: 30
//  Initialization of Dual Port LUT ram now done through explicit
// parameter setting.
RAM64X1D#(.INIT(64'h0000000000000000))RAM64X1D_instance_3806(.WCLK(CLK), .WE(wea_0), 
  .A0(ADDRA[0]), .A1(ADDRA[1]), .A2(ADDRA[2]), .A3(ADDRA[3]), .A4(ADDRA[4]), .A5(ADDRA[5]), 
  .D(DINA[30]), .SPO(pre_douta_0[30]), .DPRA0(ADDRB[0]), .DPRA1(ADDRB[1]), .DPRA2(ADDRB[2]), 
  .DPRA3(ADDRB[3]), .DPRA4(ADDRB[4]), .DPRA5(ADDRB[5]), .DPO(pre_doutb_0[30]));
// Memory array element: COL: 0, ROW: 31
//  Initialization of Dual Port LUT ram now done through explicit
// parameter setting.
RAM64X1D#(.INIT(64'h0000000000000000))RAM64X1D_instance_3807(.WCLK(CLK), .WE(wea_0), 
  .A0(ADDRA[0]), .A1(ADDRA[1]), .A2(ADDRA[2]), .A3(ADDRA[3]), .A4(ADDRA[4]), .A5(ADDRA[5]), 
  .D(DINA[31]), .SPO(pre_douta_0[31]), .DPRA0(ADDRB[0]), .DPRA1(ADDRB[1]), .DPRA2(ADDRB[2]), 
  .DPRA3(ADDRB[3]), .DPRA4(ADDRB[4]), .DPRA5(ADDRB[5]), .DPO(pre_doutb_0[31]));
// Memory array element: COL: 1, ROW: 0
//  Initialization of Dual Port LUT ram now done through explicit
// parameter setting.
RAM64X1D#(.INIT(64'h0000000000000000))RAM64X1D_instance_3808(.WCLK(CLK), .WE(wea_1), 
  .A0(ADDRA[0]), .A1(ADDRA[1]), .A2(ADDRA[2]), .A3(ADDRA[3]), .A4(ADDRA[4]), .A5(ADDRA[5]), 
  .D(DINA[0]), .SPO(pre_douta_1[0]), .DPRA0(ADDRB[0]), .DPRA1(ADDRB[1]), .DPRA2(ADDRB[2]), 
  .DPRA3(ADDRB[3]), .DPRA4(ADDRB[4]), .DPRA5(ADDRB[5]), .DPO(pre_doutb_1[0]));
// Memory array element: COL: 1, ROW: 1
//  Initialization of Dual Port LUT ram now done through explicit
// parameter setting.
RAM64X1D#(.INIT(64'h0000000000000000))RAM64X1D_instance_3809(.WCLK(CLK), .WE(wea_1), 
  .A0(ADDRA[0]), .A1(ADDRA[1]), .A2(ADDRA[2]), .A3(ADDRA[3]), .A4(ADDRA[4]), .A5(ADDRA[5]), 
  .D(DINA[1]), .SPO(pre_douta_1[1]), .DPRA0(ADDRB[0]), .DPRA1(ADDRB[1]), .DPRA2(ADDRB[2]), 
  .DPRA3(ADDRB[3]), .DPRA4(ADDRB[4]), .DPRA5(ADDRB[5]), .DPO(pre_doutb_1[1]));
// Memory array element: COL: 1, ROW: 2
//  Initialization of Dual Port LUT ram now done through explicit
// parameter setting.
RAM64X1D#(.INIT(64'h0000000000000000))RAM64X1D_instance_3810(.WCLK(CLK), .WE(wea_1), 
  .A0(ADDRA[0]), .A1(ADDRA[1]), .A2(ADDRA[2]), .A3(ADDRA[3]), .A4(ADDRA[4]), .A5(ADDRA[5]), 
  .D(DINA[2]), .SPO(pre_douta_1[2]), .DPRA0(ADDRB[0]), .DPRA1(ADDRB[1]), .DPRA2(ADDRB[2]), 
  .DPRA3(ADDRB[3]), .DPRA4(ADDRB[4]), .DPRA5(ADDRB[5]), .DPO(pre_doutb_1[2]));
// Memory array element: COL: 1, ROW: 3
//  Initialization of Dual Port LUT ram now done through explicit
// parameter setting.
RAM64X1D#(.INIT(64'h0000000000000000))RAM64X1D_instance_3811(.WCLK(CLK), .WE(wea_1), 
  .A0(ADDRA[0]), .A1(ADDRA[1]), .A2(ADDRA[2]), .A3(ADDRA[3]), .A4(ADDRA[4]), .A5(ADDRA[5]), 
  .D(DINA[3]), .SPO(pre_douta_1[3]), .DPRA0(ADDRB[0]), .DPRA1(ADDRB[1]), .DPRA2(ADDRB[2]), 
  .DPRA3(ADDRB[3]), .DPRA4(ADDRB[4]), .DPRA5(ADDRB[5]), .DPO(pre_doutb_1[3]));
// Memory array element: COL: 1, ROW: 4
//  Initialization of Dual Port LUT ram now done through explicit
// parameter setting.
RAM64X1D#(.INIT(64'h0000000000000000))RAM64X1D_instance_3812(.WCLK(CLK), .WE(wea_1), 
  .A0(ADDRA[0]), .A1(ADDRA[1]), .A2(ADDRA[2]), .A3(ADDRA[3]), .A4(ADDRA[4]), .A5(ADDRA[5]), 
  .D(DINA[4]), .SPO(pre_douta_1[4]), .DPRA0(ADDRB[0]), .DPRA1(ADDRB[1]), .DPRA2(ADDRB[2]), 
  .DPRA3(ADDRB[3]), .DPRA4(ADDRB[4]), .DPRA5(ADDRB[5]), .DPO(pre_doutb_1[4]));
// Memory array element: COL: 1, ROW: 5
//  Initialization of Dual Port LUT ram now done through explicit
// parameter setting.
RAM64X1D#(.INIT(64'h0000000000000000))RAM64X1D_instance_3813(.WCLK(CLK), .WE(wea_1), 
  .A0(ADDRA[0]), .A1(ADDRA[1]), .A2(ADDRA[2]), .A3(ADDRA[3]), .A4(ADDRA[4]), .A5(ADDRA[5]), 
  .D(DINA[5]), .SPO(pre_douta_1[5]), .DPRA0(ADDRB[0]), .DPRA1(ADDRB[1]), .DPRA2(ADDRB[2]), 
  .DPRA3(ADDRB[3]), .DPRA4(ADDRB[4]), .DPRA5(ADDRB[5]), .DPO(pre_doutb_1[5]));
// Memory array element: COL: 1, ROW: 6
//  Initialization of Dual Port LUT ram now done through explicit
// parameter setting.
RAM64X1D#(.INIT(64'h0000000000000000))RAM64X1D_instance_3814(.WCLK(CLK), .WE(wea_1), 
  .A0(ADDRA[0]), .A1(ADDRA[1]), .A2(ADDRA[2]), .A3(ADDRA[3]), .A4(ADDRA[4]), .A5(ADDRA[5]), 
  .D(DINA[6]), .SPO(pre_douta_1[6]), .DPRA0(ADDRB[0]), .DPRA1(ADDRB[1]), .DPRA2(ADDRB[2]), 
  .DPRA3(ADDRB[3]), .DPRA4(ADDRB[4]), .DPRA5(ADDRB[5]), .DPO(pre_doutb_1[6]));
// Memory array element: COL: 1, ROW: 7
//  Initialization of Dual Port LUT ram now done through explicit
// parameter setting.
RAM64X1D#(.INIT(64'h0000000000000000))RAM64X1D_instance_3815(.WCLK(CLK), .WE(wea_1), 
  .A0(ADDRA[0]), .A1(ADDRA[1]), .A2(ADDRA[2]), .A3(ADDRA[3]), .A4(ADDRA[4]), .A5(ADDRA[5]), 
  .D(DINA[7]), .SPO(pre_douta_1[7]), .DPRA0(ADDRB[0]), .DPRA1(ADDRB[1]), .DPRA2(ADDRB[2]), 
  .DPRA3(ADDRB[3]), .DPRA4(ADDRB[4]), .DPRA5(ADDRB[5]), .DPO(pre_doutb_1[7]));
// Memory array element: COL: 1, ROW: 8
//  Initialization of Dual Port LUT ram now done through explicit
// parameter setting.
RAM64X1D#(.INIT(64'h0000000000000000))RAM64X1D_instance_3816(.WCLK(CLK), .WE(wea_1), 
  .A0(ADDRA[0]), .A1(ADDRA[1]), .A2(ADDRA[2]), .A3(ADDRA[3]), .A4(ADDRA[4]), .A5(ADDRA[5]), 
  .D(DINA[8]), .SPO(pre_douta_1[8]), .DPRA0(ADDRB[0]), .DPRA1(ADDRB[1]), .DPRA2(ADDRB[2]), 
  .DPRA3(ADDRB[3]), .DPRA4(ADDRB[4]), .DPRA5(ADDRB[5]), .DPO(pre_doutb_1[8]));
// Memory array element: COL: 1, ROW: 9
//  Initialization of Dual Port LUT ram now done through explicit
// parameter setting.
RAM64X1D#(.INIT(64'h0000000000000000))RAM64X1D_instance_3817(.WCLK(CLK), .WE(wea_1), 
  .A0(ADDRA[0]), .A1(ADDRA[1]), .A2(ADDRA[2]), .A3(ADDRA[3]), .A4(ADDRA[4]), .A5(ADDRA[5]), 
  .D(DINA[9]), .SPO(pre_douta_1[9]), .DPRA0(ADDRB[0]), .DPRA1(ADDRB[1]), .DPRA2(ADDRB[2]), 
  .DPRA3(ADDRB[3]), .DPRA4(ADDRB[4]), .DPRA5(ADDRB[5]), .DPO(pre_doutb_1[9]));
// Memory array element: COL: 1, ROW: 10
//  Initialization of Dual Port LUT ram now done through explicit
// parameter setting.
RAM64X1D#(.INIT(64'h0000000000000000))RAM64X1D_instance_3818(.WCLK(CLK), .WE(wea_1), 
  .A0(ADDRA[0]), .A1(ADDRA[1]), .A2(ADDRA[2]), .A3(ADDRA[3]), .A4(ADDRA[4]), .A5(ADDRA[5]), 
  .D(DINA[10]), .SPO(pre_douta_1[10]), .DPRA0(ADDRB[0]), .DPRA1(ADDRB[1]), .DPRA2(ADDRB[2]), 
  .DPRA3(ADDRB[3]), .DPRA4(ADDRB[4]), .DPRA5(ADDRB[5]), .DPO(pre_doutb_1[10]));
// Memory array element: COL: 1, ROW: 11
//  Initialization of Dual Port LUT ram now done through explicit
// parameter setting.
RAM64X1D#(.INIT(64'h0000000000000000))RAM64X1D_instance_3819(.WCLK(CLK), .WE(wea_1), 
  .A0(ADDRA[0]), .A1(ADDRA[1]), .A2(ADDRA[2]), .A3(ADDRA[3]), .A4(ADDRA[4]), .A5(ADDRA[5]), 
  .D(DINA[11]), .SPO(pre_douta_1[11]), .DPRA0(ADDRB[0]), .DPRA1(ADDRB[1]), .DPRA2(ADDRB[2]), 
  .DPRA3(ADDRB[3]), .DPRA4(ADDRB[4]), .DPRA5(ADDRB[5]), .DPO(pre_doutb_1[11]));
// Memory array element: COL: 1, ROW: 12
//  Initialization of Dual Port LUT ram now done through explicit
// parameter setting.
RAM64X1D#(.INIT(64'h0000000000000000))RAM64X1D_instance_3820(.WCLK(CLK), .WE(wea_1), 
  .A0(ADDRA[0]), .A1(ADDRA[1]), .A2(ADDRA[2]), .A3(ADDRA[3]), .A4(ADDRA[4]), .A5(ADDRA[5]), 
  .D(DINA[12]), .SPO(pre_douta_1[12]), .DPRA0(ADDRB[0]), .DPRA1(ADDRB[1]), .DPRA2(ADDRB[2]), 
  .DPRA3(ADDRB[3]), .DPRA4(ADDRB[4]), .DPRA5(ADDRB[5]), .DPO(pre_doutb_1[12]));
// Memory array element: COL: 1, ROW: 13
//  Initialization of Dual Port LUT ram now done through explicit
// parameter setting.
RAM64X1D#(.INIT(64'h0000000000000000))RAM64X1D_instance_3821(.WCLK(CLK), .WE(wea_1), 
  .A0(ADDRA[0]), .A1(ADDRA[1]), .A2(ADDRA[2]), .A3(ADDRA[3]), .A4(ADDRA[4]), .A5(ADDRA[5]), 
  .D(DINA[13]), .SPO(pre_douta_1[13]), .DPRA0(ADDRB[0]), .DPRA1(ADDRB[1]), .DPRA2(ADDRB[2]), 
  .DPRA3(ADDRB[3]), .DPRA4(ADDRB[4]), .DPRA5(ADDRB[5]), .DPO(pre_doutb_1[13]));
// Memory array element: COL: 1, ROW: 14
//  Initialization of Dual Port LUT ram now done through explicit
// parameter setting.
RAM64X1D#(.INIT(64'h0000000000000000))RAM64X1D_instance_3822(.WCLK(CLK), .WE(wea_1), 
  .A0(ADDRA[0]), .A1(ADDRA[1]), .A2(ADDRA[2]), .A3(ADDRA[3]), .A4(ADDRA[4]), .A5(ADDRA[5]), 
  .D(DINA[14]), .SPO(pre_douta_1[14]), .DPRA0(ADDRB[0]), .DPRA1(ADDRB[1]), .DPRA2(ADDRB[2]), 
  .DPRA3(ADDRB[3]), .DPRA4(ADDRB[4]), .DPRA5(ADDRB[5]), .DPO(pre_doutb_1[14]));
// Memory array element: COL: 1, ROW: 15
//  Initialization of Dual Port LUT ram now done through explicit
// parameter setting.
RAM64X1D#(.INIT(64'h0000000000000000))RAM64X1D_instance_3823(.WCLK(CLK), .WE(wea_1), 
  .A0(ADDRA[0]), .A1(ADDRA[1]), .A2(ADDRA[2]), .A3(ADDRA[3]), .A4(ADDRA[4]), .A5(ADDRA[5]), 
  .D(DINA[15]), .SPO(pre_douta_1[15]), .DPRA0(ADDRB[0]), .DPRA1(ADDRB[1]), .DPRA2(ADDRB[2]), 
  .DPRA3(ADDRB[3]), .DPRA4(ADDRB[4]), .DPRA5(ADDRB[5]), .DPO(pre_doutb_1[15]));
// Memory array element: COL: 1, ROW: 16
//  Initialization of Dual Port LUT ram now done through explicit
// parameter setting.
RAM64X1D#(.INIT(64'h0000000000000000))RAM64X1D_instance_3824(.WCLK(CLK), .WE(wea_1), 
  .A0(ADDRA[0]), .A1(ADDRA[1]), .A2(ADDRA[2]), .A3(ADDRA[3]), .A4(ADDRA[4]), .A5(ADDRA[5]), 
  .D(DINA[16]), .SPO(pre_douta_1[16]), .DPRA0(ADDRB[0]), .DPRA1(ADDRB[1]), .DPRA2(ADDRB[2]), 
  .DPRA3(ADDRB[3]), .DPRA4(ADDRB[4]), .DPRA5(ADDRB[5]), .DPO(pre_doutb_1[16]));
// Memory array element: COL: 1, ROW: 17
//  Initialization of Dual Port LUT ram now done through explicit
// parameter setting.
RAM64X1D#(.INIT(64'h0000000000000000))RAM64X1D_instance_3825(.WCLK(CLK), .WE(wea_1), 
  .A0(ADDRA[0]), .A1(ADDRA[1]), .A2(ADDRA[2]), .A3(ADDRA[3]), .A4(ADDRA[4]), .A5(ADDRA[5]), 
  .D(DINA[17]), .SPO(pre_douta_1[17]), .DPRA0(ADDRB[0]), .DPRA1(ADDRB[1]), .DPRA2(ADDRB[2]), 
  .DPRA3(ADDRB[3]), .DPRA4(ADDRB[4]), .DPRA5(ADDRB[5]), .DPO(pre_doutb_1[17]));
// Memory array element: COL: 1, ROW: 18
//  Initialization of Dual Port LUT ram now done through explicit
// parameter setting.
RAM64X1D#(.INIT(64'h0000000000000000))RAM64X1D_instance_3826(.WCLK(CLK), .WE(wea_1), 
  .A0(ADDRA[0]), .A1(ADDRA[1]), .A2(ADDRA[2]), .A3(ADDRA[3]), .A4(ADDRA[4]), .A5(ADDRA[5]), 
  .D(DINA[18]), .SPO(pre_douta_1[18]), .DPRA0(ADDRB[0]), .DPRA1(ADDRB[1]), .DPRA2(ADDRB[2]), 
  .DPRA3(ADDRB[3]), .DPRA4(ADDRB[4]), .DPRA5(ADDRB[5]), .DPO(pre_doutb_1[18]));
// Memory array element: COL: 1, ROW: 19
//  Initialization of Dual Port LUT ram now done through explicit
// parameter setting.
RAM64X1D#(.INIT(64'h0000000000000000))RAM64X1D_instance_3827(.WCLK(CLK), .WE(wea_1), 
  .A0(ADDRA[0]), .A1(ADDRA[1]), .A2(ADDRA[2]), .A3(ADDRA[3]), .A4(ADDRA[4]), .A5(ADDRA[5]), 
  .D(DINA[19]), .SPO(pre_douta_1[19]), .DPRA0(ADDRB[0]), .DPRA1(ADDRB[1]), .DPRA2(ADDRB[2]), 
  .DPRA3(ADDRB[3]), .DPRA4(ADDRB[4]), .DPRA5(ADDRB[5]), .DPO(pre_doutb_1[19]));
// Memory array element: COL: 1, ROW: 20
//  Initialization of Dual Port LUT ram now done through explicit
// parameter setting.
RAM64X1D#(.INIT(64'h0000000000000000))RAM64X1D_instance_3828(.WCLK(CLK), .WE(wea_1), 
  .A0(ADDRA[0]), .A1(ADDRA[1]), .A2(ADDRA[2]), .A3(ADDRA[3]), .A4(ADDRA[4]), .A5(ADDRA[5]), 
  .D(DINA[20]), .SPO(pre_douta_1[20]), .DPRA0(ADDRB[0]), .DPRA1(ADDRB[1]), .DPRA2(ADDRB[2]), 
  .DPRA3(ADDRB[3]), .DPRA4(ADDRB[4]), .DPRA5(ADDRB[5]), .DPO(pre_doutb_1[20]));
// Memory array element: COL: 1, ROW: 21
//  Initialization of Dual Port LUT ram now done through explicit
// parameter setting.
RAM64X1D#(.INIT(64'h0000000000000000))RAM64X1D_instance_3829(.WCLK(CLK), .WE(wea_1), 
  .A0(ADDRA[0]), .A1(ADDRA[1]), .A2(ADDRA[2]), .A3(ADDRA[3]), .A4(ADDRA[4]), .A5(ADDRA[5]), 
  .D(DINA[21]), .SPO(pre_douta_1[21]), .DPRA0(ADDRB[0]), .DPRA1(ADDRB[1]), .DPRA2(ADDRB[2]), 
  .DPRA3(ADDRB[3]), .DPRA4(ADDRB[4]), .DPRA5(ADDRB[5]), .DPO(pre_doutb_1[21]));
// Memory array element: COL: 1, ROW: 22
//  Initialization of Dual Port LUT ram now done through explicit
// parameter setting.
RAM64X1D#(.INIT(64'h0000000000000000))RAM64X1D_instance_3830(.WCLK(CLK), .WE(wea_1), 
  .A0(ADDRA[0]), .A1(ADDRA[1]), .A2(ADDRA[2]), .A3(ADDRA[3]), .A4(ADDRA[4]), .A5(ADDRA[5]), 
  .D(DINA[22]), .SPO(pre_douta_1[22]), .DPRA0(ADDRB[0]), .DPRA1(ADDRB[1]), .DPRA2(ADDRB[2]), 
  .DPRA3(ADDRB[3]), .DPRA4(ADDRB[4]), .DPRA5(ADDRB[5]), .DPO(pre_doutb_1[22]));
// Memory array element: COL: 1, ROW: 23
//  Initialization of Dual Port LUT ram now done through explicit
// parameter setting.
RAM64X1D#(.INIT(64'h0000000000000000))RAM64X1D_instance_3831(.WCLK(CLK), .WE(wea_1), 
  .A0(ADDRA[0]), .A1(ADDRA[1]), .A2(ADDRA[2]), .A3(ADDRA[3]), .A4(ADDRA[4]), .A5(ADDRA[5]), 
  .D(DINA[23]), .SPO(pre_douta_1[23]), .DPRA0(ADDRB[0]), .DPRA1(ADDRB[1]), .DPRA2(ADDRB[2]), 
  .DPRA3(ADDRB[3]), .DPRA4(ADDRB[4]), .DPRA5(ADDRB[5]), .DPO(pre_doutb_1[23]));
// Memory array element: COL: 1, ROW: 24
//  Initialization of Dual Port LUT ram now done through explicit
// parameter setting.
RAM64X1D#(.INIT(64'h0000000000000000))RAM64X1D_instance_3832(.WCLK(CLK), .WE(wea_1), 
  .A0(ADDRA[0]), .A1(ADDRA[1]), .A2(ADDRA[2]), .A3(ADDRA[3]), .A4(ADDRA[4]), .A5(ADDRA[5]), 
  .D(DINA[24]), .SPO(pre_douta_1[24]), .DPRA0(ADDRB[0]), .DPRA1(ADDRB[1]), .DPRA2(ADDRB[2]), 
  .DPRA3(ADDRB[3]), .DPRA4(ADDRB[4]), .DPRA5(ADDRB[5]), .DPO(pre_doutb_1[24]));
// Memory array element: COL: 1, ROW: 25
//  Initialization of Dual Port LUT ram now done through explicit
// parameter setting.
RAM64X1D#(.INIT(64'h0000000000000000))RAM64X1D_instance_3833(.WCLK(CLK), .WE(wea_1), 
  .A0(ADDRA[0]), .A1(ADDRA[1]), .A2(ADDRA[2]), .A3(ADDRA[3]), .A4(ADDRA[4]), .A5(ADDRA[5]), 
  .D(DINA[25]), .SPO(pre_douta_1[25]), .DPRA0(ADDRB[0]), .DPRA1(ADDRB[1]), .DPRA2(ADDRB[2]), 
  .DPRA3(ADDRB[3]), .DPRA4(ADDRB[4]), .DPRA5(ADDRB[5]), .DPO(pre_doutb_1[25]));
// Memory array element: COL: 1, ROW: 26
//  Initialization of Dual Port LUT ram now done through explicit
// parameter setting.
RAM64X1D#(.INIT(64'h0000000000000000))RAM64X1D_instance_3834(.WCLK(CLK), .WE(wea_1), 
  .A0(ADDRA[0]), .A1(ADDRA[1]), .A2(ADDRA[2]), .A3(ADDRA[3]), .A4(ADDRA[4]), .A5(ADDRA[5]), 
  .D(DINA[26]), .SPO(pre_douta_1[26]), .DPRA0(ADDRB[0]), .DPRA1(ADDRB[1]), .DPRA2(ADDRB[2]), 
  .DPRA3(ADDRB[3]), .DPRA4(ADDRB[4]), .DPRA5(ADDRB[5]), .DPO(pre_doutb_1[26]));
// Memory array element: COL: 1, ROW: 27
//  Initialization of Dual Port LUT ram now done through explicit
// parameter setting.
RAM64X1D#(.INIT(64'h0000000000000000))RAM64X1D_instance_3835(.WCLK(CLK), .WE(wea_1), 
  .A0(ADDRA[0]), .A1(ADDRA[1]), .A2(ADDRA[2]), .A3(ADDRA[3]), .A4(ADDRA[4]), .A5(ADDRA[5]), 
  .D(DINA[27]), .SPO(pre_douta_1[27]), .DPRA0(ADDRB[0]), .DPRA1(ADDRB[1]), .DPRA2(ADDRB[2]), 
  .DPRA3(ADDRB[3]), .DPRA4(ADDRB[4]), .DPRA5(ADDRB[5]), .DPO(pre_doutb_1[27]));
// Memory array element: COL: 1, ROW: 28
//  Initialization of Dual Port LUT ram now done through explicit
// parameter setting.
RAM64X1D#(.INIT(64'h0000000000000000))RAM64X1D_instance_3836(.WCLK(CLK), .WE(wea_1), 
  .A0(ADDRA[0]), .A1(ADDRA[1]), .A2(ADDRA[2]), .A3(ADDRA[3]), .A4(ADDRA[4]), .A5(ADDRA[5]), 
  .D(DINA[28]), .SPO(pre_douta_1[28]), .DPRA0(ADDRB[0]), .DPRA1(ADDRB[1]), .DPRA2(ADDRB[2]), 
  .DPRA3(ADDRB[3]), .DPRA4(ADDRB[4]), .DPRA5(ADDRB[5]), .DPO(pre_doutb_1[28]));
// Memory array element: COL: 1, ROW: 29
//  Initialization of Dual Port LUT ram now done through explicit
// parameter setting.
RAM64X1D#(.INIT(64'h0000000000000000))RAM64X1D_instance_3837(.WCLK(CLK), .WE(wea_1), 
  .A0(ADDRA[0]), .A1(ADDRA[1]), .A2(ADDRA[2]), .A3(ADDRA[3]), .A4(ADDRA[4]), .A5(ADDRA[5]), 
  .D(DINA[29]), .SPO(pre_douta_1[29]), .DPRA0(ADDRB[0]), .DPRA1(ADDRB[1]), .DPRA2(ADDRB[2]), 
  .DPRA3(ADDRB[3]), .DPRA4(ADDRB[4]), .DPRA5(ADDRB[5]), .DPO(pre_doutb_1[29]));
// Memory array element: COL: 1, ROW: 30
//  Initialization of Dual Port LUT ram now done through explicit
// parameter setting.
RAM64X1D#(.INIT(64'h0000000000000000))RAM64X1D_instance_3838(.WCLK(CLK), .WE(wea_1), 
  .A0(ADDRA[0]), .A1(ADDRA[1]), .A2(ADDRA[2]), .A3(ADDRA[3]), .A4(ADDRA[4]), .A5(ADDRA[5]), 
  .D(DINA[30]), .SPO(pre_douta_1[30]), .DPRA0(ADDRB[0]), .DPRA1(ADDRB[1]), .DPRA2(ADDRB[2]), 
  .DPRA3(ADDRB[3]), .DPRA4(ADDRB[4]), .DPRA5(ADDRB[5]), .DPO(pre_doutb_1[30]));
// Memory array element: COL: 1, ROW: 31
//  Initialization of Dual Port LUT ram now done through explicit
// parameter setting.
RAM64X1D#(.INIT(64'h0000000000000000))RAM64X1D_instance_3839(.WCLK(CLK), .WE(wea_1), 
  .A0(ADDRA[0]), .A1(ADDRA[1]), .A2(ADDRA[2]), .A3(ADDRA[3]), .A4(ADDRA[4]), .A5(ADDRA[5]), 
  .D(DINA[31]), .SPO(pre_douta_1[31]), .DPRA0(ADDRB[0]), .DPRA1(ADDRB[1]), .DPRA2(ADDRB[2]), 
  .DPRA3(ADDRB[3]), .DPRA4(ADDRB[4]), .DPRA5(ADDRB[5]), .DPO(pre_doutb_1[31]));
endmodule



module medianRow3_structuralmemory_6841107c_(CLK_u105, bus_3d3efe39_, bus_31b68a67_, bus_3cf9e450_, bus_027b291d_, bus_1f414236_, bus_4d4d408a_, bus_3ad6d8b7_, bus_57eb5423_, bus_02d47618_, bus_689020e2_, bus_478eddae_, bus_4841690b_, bus_783e1a34_);
input		CLK_u105;
input		bus_3d3efe39_;
input	[31:0]	bus_31b68a67_;
input	[2:0]	bus_3cf9e450_;
input		bus_027b291d_;
input	[31:0]	bus_1f414236_;
input	[2:0]	bus_4d4d408a_;
input		bus_3ad6d8b7_;
input		bus_57eb5423_;
input	[31:0]	bus_02d47618_;
output	[31:0]	bus_689020e2_;
output		bus_478eddae_;
output	[31:0]	bus_4841690b_;
output		bus_783e1a34_;
wire		and_7e265506_u0;
wire		or_16a26cd4_u0;
wire		or_2376eee1_u0;
wire	[31:0]	bus_358fcad1_;
wire	[31:0]	bus_70f84dc9_;
wire		not_589ef4a2_u0;
reg		logicalMem_18458692_we_delay0_u0=1'h0;
assign and_7e265506_u0=bus_3ad6d8b7_&not_589ef4a2_u0;
assign or_16a26cd4_u0=bus_3ad6d8b7_|bus_57eb5423_;
assign or_2376eee1_u0=and_7e265506_u0|logicalMem_18458692_we_delay0_u0;
medianRow3_forge_memory_101x32_171 medianRow3_forge_memory_101x32_171_instance0(.CLK(CLK_u105), 
  .ENA(or_16a26cd4_u0), .WEA(bus_57eb5423_), .DINA(bus_02d47618_), .ADDRA(bus_1f414236_), 
  .DOUTA(bus_358fcad1_), .DONEA(), .ENB(bus_027b291d_), .ADDRB(bus_31b68a67_), .DOUTB(bus_70f84dc9_), 
  .DONEB());
assign not_589ef4a2_u0=~bus_57eb5423_;
assign bus_689020e2_=bus_70f84dc9_;
assign bus_478eddae_=bus_027b291d_;
assign bus_4841690b_=bus_358fcad1_;
assign bus_783e1a34_=or_2376eee1_u0;
always @(posedge CLK_u105 or posedge bus_3d3efe39_)
begin
if (bus_3d3efe39_)
logicalMem_18458692_we_delay0_u0<=1'h0;
else logicalMem_18458692_we_delay0_u0<=bus_57eb5423_;
end
endmodule



module medianRow3_endianswapper_75cae3f8_(endianswapper_75cae3f8_in, endianswapper_75cae3f8_out);
input		endianswapper_75cae3f8_in;
output		endianswapper_75cae3f8_out;
assign endianswapper_75cae3f8_out=endianswapper_75cae3f8_in;
endmodule



module medianRow3_endianswapper_64cc7427_(endianswapper_64cc7427_in, endianswapper_64cc7427_out);
input		endianswapper_64cc7427_in;
output		endianswapper_64cc7427_out;
assign endianswapper_64cc7427_out=endianswapper_64cc7427_in;
endmodule



module medianRow3_stateVar_fsmState_medianRow3(bus_6a846e6f_, bus_75cd2d9b_, bus_6c0a99f7_, bus_390925d7_, bus_622585f9_);
input		bus_6a846e6f_;
input		bus_75cd2d9b_;
input		bus_6c0a99f7_;
input		bus_390925d7_;
output		bus_622585f9_;
reg		stateVar_fsmState_medianRow3_u5=1'h0;
wire		endianswapper_75cae3f8_out;
wire		endianswapper_64cc7427_out;
always @(posedge bus_6a846e6f_ or posedge bus_75cd2d9b_)
begin
if (bus_75cd2d9b_)
stateVar_fsmState_medianRow3_u5<=1'h0;
else if (bus_6c0a99f7_)
stateVar_fsmState_medianRow3_u5<=endianswapper_75cae3f8_out;
end
medianRow3_endianswapper_75cae3f8_ medianRow3_endianswapper_75cae3f8__1(.endianswapper_75cae3f8_in(bus_390925d7_), 
  .endianswapper_75cae3f8_out(endianswapper_75cae3f8_out));
assign bus_622585f9_=endianswapper_64cc7427_out;
medianRow3_endianswapper_64cc7427_ medianRow3_endianswapper_64cc7427__1(.endianswapper_64cc7427_in(stateVar_fsmState_medianRow3_u5), 
  .endianswapper_64cc7427_out(endianswapper_64cc7427_out));
endmodule


