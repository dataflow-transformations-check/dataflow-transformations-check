// __  ___ __ ___  _ __   ___  ___ 
// \ \/ / '__/ _ \| '_ \ / _ \/ __|
//  >  <| | | (_) | | | | (_) \__ \
// /_/\_\_|  \___/|_| |_|\___/|___/
// 
// Xronos synthesizer version
// Run date: Fri 23 Feb 2018 14:28:52 +0000
// 

module medianRow4(median_RDY, RESET, median_SEND, in1_DATA, median_COUNT, median_ACK, in1_ACK, CLK, in1_COUNT, in1_SEND, median_DATA);
input		median_RDY;
input		RESET;
output		median_SEND;
input	[7:0]	in1_DATA;
output	[15:0]	median_COUNT;
input		median_ACK;
wire		receive_go;
output		in1_ACK;
input		CLK;
input	[15:0]	in1_COUNT;
wire		compute_median_done;
wire		receive_done;
wire		compute_median_go;
input		in1_SEND;
output	[7:0]	median_DATA;
wire	[31:0]	bus_2859a9a4_;
wire		bus_15bafb2d_;
wire		bus_6f651f11_;
wire	[31:0]	bus_636cdf1e_;
wire	[2:0]	bus_3afd15e4_;
wire	[31:0]	bus_58796d12_;
wire		bus_5306f62b_;
wire		bus_66d9a378_;
wire		scheduler_u851;
wire		scheduler_u850;
wire		medianRow4_scheduler_instance_DONE;
wire		scheduler;
wire		scheduler_u852;
wire	[31:0]	receive_u350;
wire		medianRow4_receive_instance_DONE;
wire		receive_u353;
wire		receive;
wire	[31:0]	receive_u348;
wire	[31:0]	receive_u351;
wire		receive_u349;
wire	[2:0]	receive_u352;
wire	[31:0]	bus_53cf999a_;
wire		bus_3183a413_;
wire		bus_770fec06_;
wire	[2:0]	bus_30db5dc0_;
wire		bus_297f9404_;
wire	[31:0]	bus_2d765842_;
wire		bus_54f9292a_;
wire	[31:0]	bus_510080f8_;
wire	[31:0]	bus_3e0c3763_;
wire	[31:0]	compute_median_u817;
wire	[7:0]	compute_median_u824;
wire		compute_median_u820;
wire		compute_median;
wire		compute_median_u816;
wire	[2:0]	compute_median_u819;
wire	[31:0]	compute_median_u812;
wire		medianRow4_compute_median_instance_DONE;
wire	[2:0]	compute_median_u822;
wire	[31:0]	compute_median_u814;
wire		compute_median_u813;
wire	[2:0]	compute_median_u815;
wire	[15:0]	compute_median_u823;
wire	[31:0]	compute_median_u818;
wire	[31:0]	compute_median_u821;
wire		compute_median_u825;
wire		bus_46b7b7ab_;
wire	[31:0]	bus_71cde040_;
wire		bus_7ca0a101_;
wire	[31:0]	bus_28f9a28a_;
wire		bus_01b7b955_;
wire		bus_62845f6b_;
wire		bus_0a246e51_;
wire		bus_3ed7adc5_;
assign median_SEND=compute_median_u825;
assign median_COUNT=compute_median_u823;
assign receive_go=scheduler_u852;
assign in1_ACK=receive_u353;
assign compute_median_done=bus_62845f6b_;
assign receive_done=bus_0a246e51_;
assign compute_median_go=scheduler_u851;
assign median_DATA=compute_median_u824;
medianRow4_simplememoryreferee_4f8b5442_ medianRow4_simplememoryreferee_4f8b5442__1(.bus_110f7f7d_(CLK), 
  .bus_0af37278_(bus_3ed7adc5_), .bus_33ff62fa_(bus_7ca0a101_), .bus_022a02b9_(bus_71cde040_), 
  .bus_7b38b624_(receive_u349), .bus_0e060822_({24'b0, receive_u351[7:0]}), .bus_6bb63811_(receive_u350), 
  .bus_3aec7803_(3'h1), .bus_48056705_(compute_median_u813), .bus_07b224f6_(compute_median_u816), 
  .bus_4619362a_(compute_median_u818), .bus_5621587c_(compute_median_u817), .bus_47c2d8de_(3'h1), 
  .bus_2859a9a4_(bus_2859a9a4_), .bus_58796d12_(bus_58796d12_), .bus_6f651f11_(bus_6f651f11_), 
  .bus_5306f62b_(bus_5306f62b_), .bus_3afd15e4_(bus_3afd15e4_), .bus_66d9a378_(bus_66d9a378_), 
  .bus_636cdf1e_(bus_636cdf1e_), .bus_15bafb2d_(bus_15bafb2d_));
medianRow4_scheduler medianRow4_scheduler_instance(.CLK(CLK), .RESET(bus_3ed7adc5_), 
  .GO(bus_3183a413_), .port_45f9dace_(bus_01b7b955_), .port_02c28327_(bus_53cf999a_), 
  .port_698c88d1_(median_RDY), .port_5a280e32_(in1_SEND), .port_3aa418e4_(compute_median_done), 
  .port_4d03d827_(receive_done), .DONE(medianRow4_scheduler_instance_DONE), .RESULT(scheduler), 
  .RESULT_u2498(scheduler_u850), .RESULT_u2499(scheduler_u851), .RESULT_u2500(scheduler_u852));
medianRow4_receive medianRow4_receive_instance(.CLK(CLK), .RESET(bus_3ed7adc5_), 
  .GO(receive_go), .port_2d311686_(bus_53cf999a_), .port_15c7688f_(bus_66d9a378_), 
  .port_7d9c0852_(in1_DATA), .DONE(medianRow4_receive_instance_DONE), .RESULT(receive), 
  .RESULT_u2501(receive_u348), .RESULT_u2502(receive_u349), .RESULT_u2503(receive_u350), 
  .RESULT_u2504(receive_u351), .RESULT_u2505(receive_u352), .RESULT_u2506(receive_u353));
medianRow4_stateVar_i medianRow4_stateVar_i_1(.bus_75cdf1f7_(CLK), .bus_2a3e4b71_(bus_3ed7adc5_), 
  .bus_3dfcdaf7_(receive), .bus_7f1974b0_(receive_u348), .bus_08636757_(compute_median), 
  .bus_3df95aa1_(32'h0), .bus_53cf999a_(bus_53cf999a_));
medianRow4_Kicker_70 medianRow4_Kicker_70_1(.CLK(CLK), .RESET(bus_3ed7adc5_), .bus_3183a413_(bus_3183a413_));
medianRow4_simplememoryreferee_1ed50ef4_ medianRow4_simplememoryreferee_1ed50ef4__1(.bus_0a740ac3_(CLK), 
  .bus_756cb045_(bus_3ed7adc5_), .bus_214f7884_(bus_46b7b7ab_), .bus_1b1b2f14_(bus_28f9a28a_), 
  .bus_4729383a_(compute_median_u820), .bus_16ffc768_(compute_median_u821), .bus_021319dd_(3'h1), 
  .bus_510080f8_(bus_510080f8_), .bus_3e0c3763_(bus_3e0c3763_), .bus_297f9404_(bus_297f9404_), 
  .bus_770fec06_(bus_770fec06_), .bus_30db5dc0_(bus_30db5dc0_), .bus_2d765842_(bus_2d765842_), 
  .bus_54f9292a_(bus_54f9292a_));
medianRow4_compute_median medianRow4_compute_median_instance(.CLK(CLK), .RESET(bus_3ed7adc5_), 
  .GO(compute_median_go), .port_6596c202_(bus_15bafb2d_), .port_53cb0912_(bus_636cdf1e_), 
  .port_447b6fc7_(bus_15bafb2d_), .port_4fcc8c32_(bus_54f9292a_), .port_6b492ff9_(bus_2d765842_), 
  .DONE(medianRow4_compute_median_instance_DONE), .RESULT(compute_median), .RESULT_u2507(compute_median_u812), 
  .RESULT_u2511(compute_median_u813), .RESULT_u2512(compute_median_u814), .RESULT_u2513(compute_median_u815), 
  .RESULT_u2514(compute_median_u816), .RESULT_u2515(compute_median_u817), .RESULT_u2516(compute_median_u818), 
  .RESULT_u2517(compute_median_u819), .RESULT_u2508(compute_median_u820), .RESULT_u2509(compute_median_u821), 
  .RESULT_u2510(compute_median_u822), .RESULT_u2518(compute_median_u823), .RESULT_u2519(compute_median_u824), 
  .RESULT_u2520(compute_median_u825));
medianRow4_structuralmemory_0ee317d2_ medianRow4_structuralmemory_0ee317d2__1(.CLK_u104(CLK), 
  .bus_10b48a09_(bus_3ed7adc5_), .bus_30cb1685_(bus_3e0c3763_), .bus_169472e8_(3'h1), 
  .bus_7cbea262_(bus_770fec06_), .bus_49e1e50a_(bus_58796d12_), .bus_7272e9a8_(3'h1), 
  .bus_3631d8eb_(bus_5306f62b_), .bus_2930c05f_(bus_6f651f11_), .bus_181a00e5_(bus_2859a9a4_), 
  .bus_28f9a28a_(bus_28f9a28a_), .bus_46b7b7ab_(bus_46b7b7ab_), .bus_71cde040_(bus_71cde040_), 
  .bus_7ca0a101_(bus_7ca0a101_));
medianRow4_stateVar_fsmState_medianRow4 medianRow4_stateVar_fsmState_medianRow4_1(.bus_30f7b649_(CLK), 
  .bus_01735a00_(bus_3ed7adc5_), .bus_738b712c_(scheduler), .bus_657b3268_(scheduler_u850), 
  .bus_01b7b955_(bus_01b7b955_));
assign bus_62845f6b_=medianRow4_compute_median_instance_DONE&{1{medianRow4_compute_median_instance_DONE}};
assign bus_0a246e51_=medianRow4_receive_instance_DONE&{1{medianRow4_receive_instance_DONE}};
medianRow4_globalreset_physical_3225545b_ medianRow4_globalreset_physical_3225545b__1(.bus_5bbd3ce8_(CLK), 
  .bus_26279676_(RESET), .bus_3ed7adc5_(bus_3ed7adc5_));
endmodule



module medianRow4_simplememoryreferee_4f8b5442_(bus_110f7f7d_, bus_0af37278_, bus_33ff62fa_, bus_022a02b9_, bus_7b38b624_, bus_0e060822_, bus_6bb63811_, bus_3aec7803_, bus_48056705_, bus_07b224f6_, bus_4619362a_, bus_5621587c_, bus_47c2d8de_, bus_2859a9a4_, bus_58796d12_, bus_6f651f11_, bus_5306f62b_, bus_3afd15e4_, bus_66d9a378_, bus_636cdf1e_, bus_15bafb2d_);
input		bus_110f7f7d_;
input		bus_0af37278_;
input		bus_33ff62fa_;
input	[31:0]	bus_022a02b9_;
input		bus_7b38b624_;
input	[31:0]	bus_0e060822_;
input	[31:0]	bus_6bb63811_;
input	[2:0]	bus_3aec7803_;
input		bus_48056705_;
input		bus_07b224f6_;
input	[31:0]	bus_4619362a_;
input	[31:0]	bus_5621587c_;
input	[2:0]	bus_47c2d8de_;
output	[31:0]	bus_2859a9a4_;
output	[31:0]	bus_58796d12_;
output		bus_6f651f11_;
output		bus_5306f62b_;
output	[2:0]	bus_3afd15e4_;
output		bus_66d9a378_;
output	[31:0]	bus_636cdf1e_;
output		bus_15bafb2d_;
wire	[31:0]	mux_6ff659d7_u0;
reg		done_qual_u244=1'h0;
wire		or_106ca569_u0;
wire		not_3af7655b_u0;
wire		not_00392d17_u0;
wire		or_18078ff7_u0;
wire	[31:0]	mux_297d4c8f_u0;
wire		or_1182dd10_u0;
wire		or_2bdca6ae_u0;
wire		or_5e450536_u0;
wire		and_2d61e0b9_u0;
reg		done_qual_u245_u0=1'h0;
wire		and_3d5618cb_u0;
assign mux_6ff659d7_u0=(bus_7b38b624_)?bus_6bb63811_:bus_5621587c_;
always @(posedge bus_110f7f7d_)
begin
if (bus_0af37278_)
done_qual_u244<=1'h0;
else done_qual_u244<=or_1182dd10_u0;
end
assign or_106ca569_u0=bus_7b38b624_|or_1182dd10_u0;
assign not_3af7655b_u0=~bus_33ff62fa_;
assign not_00392d17_u0=~bus_33ff62fa_;
assign or_18078ff7_u0=bus_7b38b624_|bus_07b224f6_;
assign mux_297d4c8f_u0=(bus_7b38b624_)?{24'b0, bus_0e060822_[7:0]}:bus_4619362a_;
assign or_1182dd10_u0=bus_48056705_|bus_07b224f6_;
assign or_2bdca6ae_u0=or_1182dd10_u0|done_qual_u244;
assign or_5e450536_u0=bus_7b38b624_|done_qual_u245_u0;
assign bus_2859a9a4_=mux_297d4c8f_u0;
assign bus_58796d12_=mux_6ff659d7_u0;
assign bus_6f651f11_=or_18078ff7_u0;
assign bus_5306f62b_=or_106ca569_u0;
assign bus_3afd15e4_=3'h1;
assign bus_66d9a378_=and_3d5618cb_u0;
assign bus_636cdf1e_=bus_022a02b9_;
assign bus_15bafb2d_=and_2d61e0b9_u0;
assign and_2d61e0b9_u0=or_2bdca6ae_u0&bus_33ff62fa_;
always @(posedge bus_110f7f7d_)
begin
if (bus_0af37278_)
done_qual_u245_u0<=1'h0;
else done_qual_u245_u0<=bus_7b38b624_;
end
assign and_3d5618cb_u0=or_5e450536_u0&bus_33ff62fa_;
endmodule



module medianRow4_scheduler(CLK, RESET, GO, port_45f9dace_, port_02c28327_, port_698c88d1_, port_5a280e32_, port_3aa418e4_, port_4d03d827_, RESULT, RESULT_u2498, RESULT_u2499, RESULT_u2500, DONE);
input		CLK;
input		RESET;
input		GO;
input		port_45f9dace_;
input	[31:0]	port_02c28327_;
input		port_698c88d1_;
input		port_5a280e32_;
input		port_3aa418e4_;
input		port_4d03d827_;
output		RESULT;
output		RESULT_u2498;
output		RESULT_u2499;
output		RESULT_u2500;
output		DONE;
wire signed	[31:0]	lessThan_b_signed;
wire		lessThan;
wire signed	[31:0]	lessThan_a_signed;
wire signed	[31:0]	equals_a_signed;
wire signed	[31:0]	equals_b_signed;
wire		equals;
wire		equals_u258;
wire signed	[31:0]	equals_u258_a_signed;
wire signed	[31:0]	equals_u258_b_signed;
wire		not_u990_u0;
wire		and_u4286_u0;
wire		and_u4287_u0;
wire		andOp;
wire		not_u991_u0;
wire		and_u4288_u0;
wire		and_u4289_u0;
wire		simplePinWrite;
wire		and_u4290_u0;
wire		and_u4291_u0;
wire signed	[31:0]	equals_u259_a_signed;
wire signed	[31:0]	equals_u259_b_signed;
wire		equals_u259;
wire		and_u4292_u0;
wire		not_u992_u0;
wire		and_u4293_u0;
wire		andOp_u92;
wire		not_u993_u0;
wire		and_u4294_u0;
wire		and_u4295_u0;
wire		simplePinWrite_u670;
wire		and_u4296_u0;
wire		and_u4297_u0;
wire		not_u994_u0;
wire		and_u4298_u0;
wire		and_u4299_u0;
wire		not_u995_u0;
wire		simplePinWrite_u671;
wire		and_u4300_u0;
wire		and_u4301_u0;
reg		and_delayed_u423=1'h0;
wire		or_u1660_u0;
reg		reg_08aef67f_u0=1'h0;
wire		and_u4302_u0;
wire		or_u1661_u0;
wire		and_u4303_u0;
reg		reg_581e109c_u0=1'h0;
wire		mux_u2188;
wire		or_u1662_u0;
wire		or_u1663_u0;
wire		and_u4304_u0;
wire		and_u4305_u0;
wire		and_u4306_u0;
wire		and_u4307_u0;
reg		and_delayed_u424_u0=1'h0;
wire		or_u1664_u0;
wire		receive_go_merge;
wire		or_u1665_u0;
wire		mux_u2189_u0;
wire		and_u4308_u0;
reg		loopControl_u120=1'h0;
wire		or_u1666_u0;
reg		syncEnable_u1671=1'h0;
reg		reg_25259aff_u0=1'h0;
reg		reg_25259aff_result_delayed_u0=1'h0;
wire		or_u1667_u0;
wire		mux_u2190_u0;
assign lessThan_a_signed=port_02c28327_;
assign lessThan_b_signed=32'h65;
assign lessThan=lessThan_a_signed<lessThan_b_signed;
assign equals_a_signed=port_02c28327_;
assign equals_b_signed=32'h65;
assign equals=equals_a_signed==equals_b_signed;
assign equals_u258_a_signed={31'b0, port_45f9dace_};
assign equals_u258_b_signed=32'h0;
assign equals_u258=equals_u258_a_signed==equals_u258_b_signed;
assign not_u990_u0=~equals_u258;
assign and_u4286_u0=and_u4308_u0&equals_u258;
assign and_u4287_u0=and_u4308_u0&not_u990_u0;
assign andOp=lessThan&port_5a280e32_;
assign not_u991_u0=~andOp;
assign and_u4288_u0=and_u4291_u0&andOp;
assign and_u4289_u0=and_u4291_u0&not_u991_u0;
assign simplePinWrite=and_u4290_u0&{1{and_u4290_u0}};
assign and_u4290_u0=and_u4288_u0&and_u4291_u0;
assign and_u4291_u0=and_u4286_u0&and_u4308_u0;
assign equals_u259_a_signed={31'b0, port_45f9dace_};
assign equals_u259_b_signed=32'h1;
assign equals_u259=equals_u259_a_signed==equals_u259_b_signed;
assign and_u4292_u0=and_u4308_u0&not_u992_u0;
assign not_u992_u0=~equals_u259;
assign and_u4293_u0=and_u4308_u0&equals_u259;
assign andOp_u92=lessThan&port_5a280e32_;
assign not_u993_u0=~andOp_u92;
assign and_u4294_u0=and_u4307_u0&andOp_u92;
assign and_u4295_u0=and_u4307_u0&not_u993_u0;
assign simplePinWrite_u670=and_u4305_u0&{1{and_u4305_u0}};
assign and_u4296_u0=and_u4304_u0&not_u994_u0;
assign and_u4297_u0=and_u4304_u0&equals;
assign not_u994_u0=~equals;
assign and_u4298_u0=and_u4302_u0&port_698c88d1_;
assign and_u4299_u0=and_u4302_u0&not_u995_u0;
assign not_u995_u0=~port_698c88d1_;
assign simplePinWrite_u671=and_u4301_u0&{1{and_u4301_u0}};
assign and_u4300_u0=and_u4299_u0&and_u4302_u0;
assign and_u4301_u0=and_u4298_u0&and_u4302_u0;
always @(posedge CLK or posedge RESET)
begin
if (RESET)
and_delayed_u423<=1'h0;
else and_delayed_u423<=and_u4300_u0;
end
assign or_u1660_u0=and_delayed_u423|port_3aa418e4_;
always @(posedge CLK or posedge RESET)
begin
if (RESET)
reg_08aef67f_u0<=1'h0;
else reg_08aef67f_u0<=and_u4303_u0;
end
assign and_u4302_u0=and_u4297_u0&and_u4304_u0;
assign or_u1661_u0=or_u1660_u0|reg_08aef67f_u0;
assign and_u4303_u0=and_u4296_u0&and_u4304_u0;
always @(posedge CLK or posedge RESET)
begin
if (RESET)
reg_581e109c_u0<=1'h0;
else reg_581e109c_u0<=and_u4305_u0;
end
assign mux_u2188=(and_u4305_u0)?1'h1:1'h0;
assign or_u1662_u0=and_u4305_u0|and_u4301_u0;
assign or_u1663_u0=or_u1661_u0|reg_581e109c_u0;
assign and_u4304_u0=and_u4295_u0&and_u4307_u0;
assign and_u4305_u0=and_u4294_u0&and_u4307_u0;
assign and_u4306_u0=and_u4292_u0&and_u4308_u0;
assign and_u4307_u0=and_u4293_u0&and_u4308_u0;
always @(posedge CLK or posedge RESET)
begin
if (RESET)
and_delayed_u424_u0<=1'h0;
else and_delayed_u424_u0<=and_u4306_u0;
end
assign or_u1664_u0=or_u1663_u0|and_delayed_u424_u0;
assign receive_go_merge=simplePinWrite|simplePinWrite_u670;
assign or_u1665_u0=and_u4290_u0|or_u1662_u0;
assign mux_u2189_u0=(and_u4290_u0)?1'h1:mux_u2188;
assign and_u4308_u0=or_u1666_u0&or_u1666_u0;
always @(posedge CLK or posedge syncEnable_u1671)
begin
if (syncEnable_u1671)
loopControl_u120<=1'h0;
else loopControl_u120<=or_u1664_u0;
end
assign or_u1666_u0=loopControl_u120|reg_25259aff_result_delayed_u0;
always @(posedge CLK)
begin
if (reg_25259aff_result_delayed_u0)
syncEnable_u1671<=RESET;
end
always @(posedge CLK or posedge RESET)
begin
if (RESET)
reg_25259aff_u0<=1'h0;
else reg_25259aff_u0<=GO;
end
always @(posedge CLK or posedge RESET)
begin
if (RESET)
reg_25259aff_result_delayed_u0<=1'h0;
else reg_25259aff_result_delayed_u0<=reg_25259aff_u0;
end
assign or_u1667_u0=GO|or_u1665_u0;
assign mux_u2190_u0=(GO)?1'h0:mux_u2189_u0;
assign RESULT=or_u1667_u0;
assign RESULT_u2498=mux_u2190_u0;
assign RESULT_u2499=simplePinWrite_u671;
assign RESULT_u2500=receive_go_merge;
assign DONE=1'h0;
endmodule



module medianRow4_receive(CLK, RESET, GO, port_2d311686_, port_15c7688f_, port_7d9c0852_, RESULT, RESULT_u2501, RESULT_u2502, RESULT_u2503, RESULT_u2504, RESULT_u2505, RESULT_u2506, DONE);
input		CLK;
input		RESET;
input		GO;
input	[31:0]	port_2d311686_;
input		port_15c7688f_;
input	[7:0]	port_7d9c0852_;
output		RESULT;
output	[31:0]	RESULT_u2501;
output		RESULT_u2502;
output	[31:0]	RESULT_u2503;
output	[31:0]	RESULT_u2504;
output	[2:0]	RESULT_u2505;
output		RESULT_u2506;
output		DONE;
wire		simplePinWrite;
wire	[31:0]	add;
wire		or_u1668_u0;
wire		and_u4309_u0;
reg		reg_0f0a6733_u0=1'h0;
wire	[31:0]	add_u804;
reg		reg_0c79a533_u0=1'h0;
assign simplePinWrite=GO&{1{GO}};
assign add=port_2d311686_+32'h0;
assign or_u1668_u0=and_u4309_u0|RESET;
assign and_u4309_u0=reg_0f0a6733_u0&port_15c7688f_;
always @(posedge CLK or posedge GO or posedge or_u1668_u0)
begin
if (or_u1668_u0)
reg_0f0a6733_u0<=1'h0;
else if (GO)
reg_0f0a6733_u0<=1'h1;
else reg_0f0a6733_u0<=reg_0f0a6733_u0;
end
assign add_u804=port_2d311686_+32'h1;
always @(posedge CLK or posedge RESET)
begin
if (RESET)
reg_0c79a533_u0<=1'h0;
else reg_0c79a533_u0<=GO;
end
assign RESULT=GO;
assign RESULT_u2501=add_u804;
assign RESULT_u2502=GO;
assign RESULT_u2503=add;
assign RESULT_u2504={24'b0, port_7d9c0852_};
assign RESULT_u2505=3'h1;
assign RESULT_u2506=simplePinWrite;
assign DONE=reg_0c79a533_u0;
endmodule



module medianRow4_endianswapper_213bcd90_(endianswapper_213bcd90_in, endianswapper_213bcd90_out);
input	[31:0]	endianswapper_213bcd90_in;
output	[31:0]	endianswapper_213bcd90_out;
assign endianswapper_213bcd90_out=endianswapper_213bcd90_in;
endmodule



module medianRow4_endianswapper_765c0ec7_(endianswapper_765c0ec7_in, endianswapper_765c0ec7_out);
input	[31:0]	endianswapper_765c0ec7_in;
output	[31:0]	endianswapper_765c0ec7_out;
assign endianswapper_765c0ec7_out=endianswapper_765c0ec7_in;
endmodule



module medianRow4_stateVar_i(bus_75cdf1f7_, bus_2a3e4b71_, bus_3dfcdaf7_, bus_7f1974b0_, bus_08636757_, bus_3df95aa1_, bus_53cf999a_);
input		bus_75cdf1f7_;
input		bus_2a3e4b71_;
input		bus_3dfcdaf7_;
input	[31:0]	bus_7f1974b0_;
input		bus_08636757_;
input	[31:0]	bus_3df95aa1_;
output	[31:0]	bus_53cf999a_;
reg	[31:0]	stateVar_i_u60=32'h0;
wire		or_17eff5f5_u0;
wire	[31:0]	endianswapper_213bcd90_out;
wire	[31:0]	endianswapper_765c0ec7_out;
wire	[31:0]	mux_029c71f1_u0;
always @(posedge bus_75cdf1f7_ or posedge bus_2a3e4b71_)
begin
if (bus_2a3e4b71_)
stateVar_i_u60<=32'h0;
else if (or_17eff5f5_u0)
stateVar_i_u60<=endianswapper_213bcd90_out;
end
assign or_17eff5f5_u0=bus_3dfcdaf7_|bus_08636757_;
medianRow4_endianswapper_213bcd90_ medianRow4_endianswapper_213bcd90__1(.endianswapper_213bcd90_in(mux_029c71f1_u0), 
  .endianswapper_213bcd90_out(endianswapper_213bcd90_out));
medianRow4_endianswapper_765c0ec7_ medianRow4_endianswapper_765c0ec7__1(.endianswapper_765c0ec7_in(stateVar_i_u60), 
  .endianswapper_765c0ec7_out(endianswapper_765c0ec7_out));
assign bus_53cf999a_=endianswapper_765c0ec7_out;
assign mux_029c71f1_u0=(bus_3dfcdaf7_)?bus_7f1974b0_:32'h0;
endmodule



module medianRow4_Kicker_70(CLK, RESET, bus_3183a413_);
input		CLK;
input		RESET;
output		bus_3183a413_;
wire		bus_3f316817_;
wire		bus_58b00b4b_;
reg		kicker_2=1'h0;
reg		kicker_1=1'h0;
wire		bus_662f1728_;
wire		bus_76286f94_;
reg		kicker_res=1'h0;
assign bus_3f316817_=bus_662f1728_&kicker_1;
assign bus_58b00b4b_=~kicker_2;
always @(posedge CLK)
begin
kicker_2<=bus_3f316817_;
end
always @(posedge CLK)
begin
kicker_1<=bus_662f1728_;
end
assign bus_3183a413_=kicker_res;
assign bus_662f1728_=~RESET;
assign bus_76286f94_=kicker_1&bus_662f1728_&bus_58b00b4b_;
always @(posedge CLK)
begin
kicker_res<=bus_76286f94_;
end
endmodule



module medianRow4_simplememoryreferee_1ed50ef4_(bus_0a740ac3_, bus_756cb045_, bus_214f7884_, bus_1b1b2f14_, bus_4729383a_, bus_16ffc768_, bus_021319dd_, bus_510080f8_, bus_3e0c3763_, bus_297f9404_, bus_770fec06_, bus_30db5dc0_, bus_2d765842_, bus_54f9292a_);
input		bus_0a740ac3_;
input		bus_756cb045_;
input		bus_214f7884_;
input	[31:0]	bus_1b1b2f14_;
input		bus_4729383a_;
input	[31:0]	bus_16ffc768_;
input	[2:0]	bus_021319dd_;
output	[31:0]	bus_510080f8_;
output	[31:0]	bus_3e0c3763_;
output		bus_297f9404_;
output		bus_770fec06_;
output	[2:0]	bus_30db5dc0_;
output	[31:0]	bus_2d765842_;
output		bus_54f9292a_;
assign bus_510080f8_=32'h0;
assign bus_3e0c3763_=bus_16ffc768_;
assign bus_297f9404_=1'h0;
assign bus_770fec06_=bus_4729383a_;
assign bus_30db5dc0_=3'h1;
assign bus_2d765842_=bus_1b1b2f14_;
assign bus_54f9292a_=bus_214f7884_;
endmodule



module medianRow4_compute_median(CLK, RESET, GO, port_4fcc8c32_, port_6b492ff9_, port_6596c202_, port_53cb0912_, port_447b6fc7_, RESULT, RESULT_u2507, RESULT_u2508, RESULT_u2509, RESULT_u2510, RESULT_u2511, RESULT_u2512, RESULT_u2513, RESULT_u2514, RESULT_u2515, RESULT_u2516, RESULT_u2517, RESULT_u2518, RESULT_u2519, RESULT_u2520, DONE);
input		CLK;
input		RESET;
input		GO;
input		port_4fcc8c32_;
input	[31:0]	port_6b492ff9_;
input		port_6596c202_;
input	[31:0]	port_53cb0912_;
input		port_447b6fc7_;
output		RESULT;
output	[31:0]	RESULT_u2507;
output		RESULT_u2508;
output	[31:0]	RESULT_u2509;
output	[2:0]	RESULT_u2510;
output		RESULT_u2511;
output	[31:0]	RESULT_u2512;
output	[2:0]	RESULT_u2513;
output		RESULT_u2514;
output	[31:0]	RESULT_u2515;
output	[31:0]	RESULT_u2516;
output	[2:0]	RESULT_u2517;
output	[15:0]	RESULT_u2518;
output	[7:0]	RESULT_u2519;
output		RESULT_u2520;
output		DONE;
wire		and_u4310_u0;
reg	[31:0]	syncEnable_u1672=32'h0;
wire		and_u4311_u0;
wire signed	[32:0]	lessThan_a_signed;
wire signed	[32:0]	lessThan_b_signed;
wire		lessThan;
wire		and_u4312_u0;
wire		and_u4313_u0;
wire		not_u996_u0;
reg	[31:0]	syncEnable_u1673_u0=32'h0;
wire	[31:0]	add;
wire		and_u4314_u0;
wire	[31:0]	add_u805;
wire	[31:0]	add_u806;
wire		and_u4315_u0;
wire		greaterThan;
wire signed	[31:0]	greaterThan_a_signed;
wire signed	[31:0]	greaterThan_b_signed;
wire		and_u4316_u0;
wire		and_u4317_u0;
wire		not_u997_u0;
wire	[31:0]	add_u807;
wire		and_u4318_u0;
wire	[31:0]	add_u808;
wire	[31:0]	add_u809;
wire		and_u4319_u0;
wire	[31:0]	add_u810;
wire		or_u1669_u0;
reg		reg_7093880b_u0=1'h0;
wire		and_u4320_u0;
wire	[31:0]	add_u811;
wire	[31:0]	add_u812;
wire		and_u4321_u0;
wire		or_u1670_u0;
reg		reg_73da7627_u0=1'h0;
reg	[31:0]	syncEnable_u1674_u0=32'h0;
reg	[31:0]	syncEnable_u1675_u0=32'h0;
reg		block_GO_delayed_u122=1'h0;
reg	[31:0]	syncEnable_u1676_u0=32'h0;
reg	[31:0]	syncEnable_u1677_u0=32'h0;
reg		block_GO_delayed_result_delayed_u23=1'h0;
reg	[31:0]	syncEnable_u1678_u0=32'h0;
wire	[31:0]	mux_u2191;
wire	[31:0]	mux_u2192_u0;
wire		or_u1671_u0;
reg	[31:0]	syncEnable_u1679_u0=32'h0;
reg	[31:0]	syncEnable_u1680_u0=32'h0;
wire		or_u1672_u0;
reg		reg_509f5715_u0=1'h0;
reg		reg_4350e73b_u0=1'h0;
wire	[31:0]	mux_u2193_u0;
wire	[31:0]	mux_u2194_u0;
reg	[31:0]	syncEnable_u1681_u0=32'h0;
reg		reg_023275a1_u0=1'h0;
reg		reg_509f5715_result_delayed_u0=1'h0;
reg		reg_444ec376_u0=1'h0;
wire		and_u4322_u0;
wire		mux_u2195_u0;
wire		and_u4323_u0;
reg		syncEnable_u1682_u0=1'h0;
wire	[31:0]	add_u813;
reg		block_GO_delayed_u123_u0=1'h0;
reg	[31:0]	syncEnable_u1683_u0=32'h0;
reg		syncEnable_u1684_u0=1'h0;
reg	[31:0]	syncEnable_u1685_u0=32'h0;
reg	[31:0]	syncEnable_u1686_u0=32'h0;
reg	[31:0]	syncEnable_u1687_u0=32'h0;
reg	[31:0]	syncEnable_u1688_u0=32'h0;
wire	[31:0]	mux_u2196_u0;
wire		or_u1673_u0;
reg	[31:0]	syncEnable_u1689_u0=32'h0;
wire		or_u1674_u0;
wire	[31:0]	mux_u2197_u0;
wire		mux_u2198_u0;
wire		or_u1675_u0;
wire	[31:0]	mux_u2199_u0;
wire	[31:0]	mux_u2200_u0;
wire	[31:0]	mux_u2201_u0;
wire	[31:0]	mux_u2202_u0;
wire	[31:0]	mux_u2203_u0;
wire	[15:0]	add_u814;
wire	[31:0]	latch_340c6e8c_out;
reg	[31:0]	latch_340c6e8c_reg=32'h0;
wire	[31:0]	latch_2f6d8ac8_out;
reg	[31:0]	latch_2f6d8ac8_reg=32'h0;
reg		reg_386a6a60_u0=1'h0;
wire		scoreboard_035e2414_and;
wire		scoreboard_035e2414_resOr0;
wire		scoreboard_035e2414_resOr1;
wire		bus_48182097_;
reg		scoreboard_035e2414_reg0=1'h0;
reg		scoreboard_035e2414_reg1=1'h0;
reg	[15:0]	syncEnable_u1690_u0=16'h0;
reg		latch_524f1382_reg=1'h0;
wire		latch_524f1382_out;
wire	[31:0]	latch_38039538_out;
reg	[31:0]	latch_38039538_reg=32'h0;
wire	[31:0]	latch_525691ab_out;
reg	[31:0]	latch_525691ab_reg=32'h0;
wire	[15:0]	lessThan_u116_b_unsigned;
wire	[15:0]	lessThan_u116_a_unsigned;
wire		lessThan_u116;
wire		andOp;
wire		not_u998_u0;
wire		and_u4324_u0;
wire		and_u4325_u0;
wire		and_u4326_u0;
wire		mux_u2204_u0;
wire		or_u1676_u0;
reg	[31:0]	fbReg_tmp_row0_u58=32'h0;
reg		loopControl_u121=1'h0;
reg	[15:0]	fbReg_idx_u58=16'h0;
wire	[31:0]	mux_u2205_u0;
reg		fbReg_swapped_u58=1'h0;
wire	[31:0]	mux_u2206_u0;
reg	[31:0]	fbReg_tmp_row_u58=32'h0;
wire	[31:0]	mux_u2207_u0;
wire	[15:0]	mux_u2208_u0;
reg	[31:0]	fbReg_tmp_row1_u58=32'h0;
wire	[31:0]	mux_u2209_u0;
reg		syncEnable_u1691_u0=1'h0;
reg	[31:0]	fbReg_tmp_u58=32'h0;
wire		and_u4327_u0;
wire	[7:0]	simplePinWrite;
wire	[15:0]	simplePinWrite_u672;
wire		simplePinWrite_u673;
reg		reg_10c38592_u0=1'h0;
reg		reg_20e1b780_u0=1'h0;
wire		or_u1677_u0;
wire	[31:0]	mux_u2210_u0;
assign and_u4310_u0=and_u4324_u0&or_u1676_u0;
always @(posedge CLK)
begin
if (or_u1675_u0)
syncEnable_u1672<=mux_u2203_u0;
end
assign and_u4311_u0=and_u4312_u0&or_u1675_u0;
assign lessThan_a_signed={1'b0, mux_u2200_u0};
assign lessThan_b_signed=33'h64;
assign lessThan=lessThan_a_signed<lessThan_b_signed;
assign and_u4312_u0=or_u1675_u0&lessThan;
assign and_u4313_u0=or_u1675_u0&not_u996_u0;
assign not_u996_u0=~lessThan;
always @(posedge CLK)
begin
if (or_u1675_u0)
syncEnable_u1673_u0<=mux_u2201_u0;
end
assign add=mux_u2200_u0+32'h0;
assign and_u4314_u0=and_u4311_u0&port_4fcc8c32_;
assign add_u805=mux_u2200_u0+32'h1;
assign add_u806=add_u805+32'h0;
assign and_u4315_u0=and_u4311_u0&port_447b6fc7_;
assign greaterThan_a_signed=syncEnable_u1685_u0;
assign greaterThan_b_signed=syncEnable_u1686_u0;
assign greaterThan=greaterThan_a_signed>greaterThan_b_signed;
assign and_u4316_u0=block_GO_delayed_u123_u0&not_u997_u0;
assign and_u4317_u0=block_GO_delayed_u123_u0&greaterThan;
assign not_u997_u0=~greaterThan;
assign add_u807=syncEnable_u1687_u0+32'h0;
assign and_u4318_u0=and_u4322_u0&port_4fcc8c32_;
assign add_u808=syncEnable_u1687_u0+32'h1;
assign add_u809=add_u808+32'h0;
assign and_u4319_u0=and_u4322_u0&port_447b6fc7_;
assign add_u810=syncEnable_u1687_u0+32'h0;
assign or_u1669_u0=and_u4320_u0|RESET;
always @(posedge CLK or posedge block_GO_delayed_u122 or posedge or_u1669_u0)
begin
if (or_u1669_u0)
reg_7093880b_u0<=1'h0;
else if (block_GO_delayed_u122)
reg_7093880b_u0<=1'h1;
else reg_7093880b_u0<=reg_7093880b_u0;
end
assign and_u4320_u0=reg_7093880b_u0&port_447b6fc7_;
assign add_u811=syncEnable_u1687_u0+32'h1;
assign add_u812=add_u811+32'h0;
assign and_u4321_u0=reg_73da7627_u0&port_447b6fc7_;
assign or_u1670_u0=and_u4321_u0|RESET;
always @(posedge CLK or posedge block_GO_delayed_result_delayed_u23 or posedge or_u1670_u0)
begin
if (or_u1670_u0)
reg_73da7627_u0<=1'h0;
else if (block_GO_delayed_result_delayed_u23)
reg_73da7627_u0<=1'h1;
else reg_73da7627_u0<=reg_73da7627_u0;
end
always @(posedge CLK)
begin
if (and_u4322_u0)
syncEnable_u1674_u0<=add_u812;
end
always @(posedge CLK)
begin
if (and_u4322_u0)
syncEnable_u1675_u0<=port_53cb0912_;
end
always @(posedge CLK or posedge RESET)
begin
if (RESET)
block_GO_delayed_u122<=1'h0;
else block_GO_delayed_u122<=and_u4322_u0;
end
always @(posedge CLK)
begin
if (and_u4322_u0)
syncEnable_u1676_u0<=port_6b492ff9_;
end
always @(posedge CLK)
begin
if (and_u4322_u0)
syncEnable_u1677_u0<=port_53cb0912_;
end
always @(posedge CLK or posedge RESET)
begin
if (RESET)
block_GO_delayed_result_delayed_u23<=1'h0;
else block_GO_delayed_result_delayed_u23<=block_GO_delayed_u122;
end
always @(posedge CLK)
begin
if (and_u4322_u0)
syncEnable_u1678_u0<=add_u810;
end
assign mux_u2191=({32{block_GO_delayed_u122}}&syncEnable_u1678_u0)|({32{block_GO_delayed_result_delayed_u23}}&syncEnable_u1674_u0)|({32{and_u4322_u0}}&add_u809);
assign mux_u2192_u0=(block_GO_delayed_u122)?syncEnable_u1675_u0:syncEnable_u1679_u0;
assign or_u1671_u0=block_GO_delayed_u122|block_GO_delayed_result_delayed_u23;
always @(posedge CLK)
begin
if (and_u4322_u0)
syncEnable_u1679_u0<=port_6b492ff9_;
end
always @(posedge CLK)
begin
if (block_GO_delayed_u123_u0)
syncEnable_u1680_u0<=syncEnable_u1683_u0;
end
assign or_u1672_u0=reg_023275a1_u0|reg_444ec376_u0;
always @(posedge CLK or posedge RESET)
begin
if (RESET)
reg_509f5715_u0<=1'h0;
else reg_509f5715_u0<=reg_4350e73b_u0;
end
always @(posedge CLK or posedge RESET)
begin
if (RESET)
reg_4350e73b_u0<=1'h0;
else reg_4350e73b_u0<=and_u4322_u0;
end
assign mux_u2193_u0=(reg_023275a1_u0)?syncEnable_u1677_u0:syncEnable_u1681_u0;
assign mux_u2194_u0=(reg_023275a1_u0)?syncEnable_u1676_u0:syncEnable_u1680_u0;
always @(posedge CLK)
begin
if (block_GO_delayed_u123_u0)
syncEnable_u1681_u0<=syncEnable_u1688_u0;
end
always @(posedge CLK or posedge RESET)
begin
if (RESET)
reg_023275a1_u0<=1'h0;
else reg_023275a1_u0<=reg_509f5715_result_delayed_u0;
end
always @(posedge CLK or posedge RESET)
begin
if (RESET)
reg_509f5715_result_delayed_u0<=1'h0;
else reg_509f5715_result_delayed_u0<=reg_509f5715_u0;
end
always @(posedge CLK or posedge RESET)
begin
if (RESET)
reg_444ec376_u0<=1'h0;
else reg_444ec376_u0<=and_u4323_u0;
end
assign and_u4322_u0=and_u4317_u0&block_GO_delayed_u123_u0;
assign mux_u2195_u0=(reg_023275a1_u0)?1'h1:syncEnable_u1682_u0;
assign and_u4323_u0=and_u4316_u0&block_GO_delayed_u123_u0;
always @(posedge CLK)
begin
if (block_GO_delayed_u123_u0)
syncEnable_u1682_u0<=syncEnable_u1684_u0;
end
assign add_u813=mux_u2200_u0+32'h1;
always @(posedge CLK or posedge RESET)
begin
if (RESET)
block_GO_delayed_u123_u0<=1'h0;
else block_GO_delayed_u123_u0<=and_u4311_u0;
end
always @(posedge CLK)
begin
if (and_u4311_u0)
syncEnable_u1683_u0<=mux_u2202_u0;
end
always @(posedge CLK)
begin
if (and_u4311_u0)
syncEnable_u1684_u0<=mux_u2198_u0;
end
always @(posedge CLK)
begin
if (and_u4311_u0)
syncEnable_u1685_u0<=port_6b492ff9_;
end
always @(posedge CLK)
begin
if (and_u4311_u0)
syncEnable_u1686_u0<=port_53cb0912_;
end
always @(posedge CLK)
begin
if (and_u4311_u0)
syncEnable_u1687_u0<=mux_u2200_u0;
end
always @(posedge CLK)
begin
if (and_u4311_u0)
syncEnable_u1688_u0<=mux_u2201_u0;
end
assign mux_u2196_u0=({32{or_u1671_u0}}&mux_u2191)|({32{and_u4311_u0}}&add_u806)|({32{and_u4322_u0}}&mux_u2191);
assign or_u1673_u0=and_u4311_u0|and_u4322_u0;
always @(posedge CLK)
begin
if (and_u4311_u0)
syncEnable_u1689_u0<=add_u813;
end
assign or_u1674_u0=and_u4311_u0|and_u4322_u0;
assign mux_u2197_u0=(and_u4311_u0)?add:add_u807;
assign mux_u2198_u0=(and_u4310_u0)?1'h0:mux_u2195_u0;
assign or_u1675_u0=and_u4310_u0|or_u1672_u0;
assign mux_u2199_u0=(and_u4310_u0)?mux_u2205_u0:syncEnable_u1685_u0;
assign mux_u2200_u0=(and_u4310_u0)?32'h0:syncEnable_u1689_u0;
assign mux_u2201_u0=(and_u4310_u0)?mux_u2207_u0:mux_u2193_u0;
assign mux_u2202_u0=(and_u4310_u0)?mux_u2206_u0:mux_u2194_u0;
assign mux_u2203_u0=(and_u4310_u0)?mux_u2209_u0:syncEnable_u1686_u0;
assign add_u814=mux_u2208_u0+16'h1;
assign latch_340c6e8c_out=(or_u1672_u0)?syncEnable_u1672:latch_340c6e8c_reg;
always @(posedge CLK)
begin
if (or_u1672_u0)
latch_340c6e8c_reg<=syncEnable_u1672;
end
assign latch_2f6d8ac8_out=(or_u1672_u0)?syncEnable_u1673_u0:latch_2f6d8ac8_reg;
always @(posedge CLK)
begin
if (or_u1672_u0)
latch_2f6d8ac8_reg<=syncEnable_u1673_u0;
end
always @(posedge CLK or posedge RESET)
begin
if (RESET)
reg_386a6a60_u0<=1'h0;
else reg_386a6a60_u0<=or_u1672_u0;
end
assign scoreboard_035e2414_and=scoreboard_035e2414_resOr0&scoreboard_035e2414_resOr1;
assign scoreboard_035e2414_resOr0=reg_386a6a60_u0|scoreboard_035e2414_reg0;
assign scoreboard_035e2414_resOr1=or_u1672_u0|scoreboard_035e2414_reg1;
assign bus_48182097_=scoreboard_035e2414_and|RESET;
always @(posedge CLK)
begin
if (bus_48182097_)
scoreboard_035e2414_reg0<=1'h0;
else if (reg_386a6a60_u0)
scoreboard_035e2414_reg0<=1'h1;
else scoreboard_035e2414_reg0<=scoreboard_035e2414_reg0;
end
always @(posedge CLK)
begin
if (bus_48182097_)
scoreboard_035e2414_reg1<=1'h0;
else if (or_u1672_u0)
scoreboard_035e2414_reg1<=1'h1;
else scoreboard_035e2414_reg1<=scoreboard_035e2414_reg1;
end
always @(posedge CLK)
begin
if (and_u4310_u0)
syncEnable_u1690_u0<=add_u814;
end
always @(posedge CLK)
begin
if (or_u1672_u0)
latch_524f1382_reg<=mux_u2195_u0;
end
assign latch_524f1382_out=(or_u1672_u0)?mux_u2195_u0:latch_524f1382_reg;
assign latch_38039538_out=(or_u1672_u0)?mux_u2194_u0:latch_38039538_reg;
always @(posedge CLK)
begin
if (or_u1672_u0)
latch_38039538_reg<=mux_u2194_u0;
end
assign latch_525691ab_out=(or_u1672_u0)?syncEnable_u1685_u0:latch_525691ab_reg;
always @(posedge CLK)
begin
if (or_u1672_u0)
latch_525691ab_reg<=syncEnable_u1685_u0;
end
assign lessThan_u116_a_unsigned=mux_u2208_u0;
assign lessThan_u116_b_unsigned=16'h65;
assign lessThan_u116=lessThan_u116_a_unsigned<lessThan_u116_b_unsigned;
assign andOp=lessThan_u116&mux_u2204_u0;
assign not_u998_u0=~andOp;
assign and_u4324_u0=or_u1676_u0&andOp;
assign and_u4325_u0=or_u1676_u0&not_u998_u0;
assign and_u4326_u0=and_u4325_u0&or_u1676_u0;
assign mux_u2204_u0=(loopControl_u121)?fbReg_swapped_u58:1'h0;
assign or_u1676_u0=loopControl_u121|GO;
always @(posedge CLK)
begin
if (scoreboard_035e2414_and)
fbReg_tmp_row0_u58<=latch_340c6e8c_out;
end
always @(posedge CLK or posedge syncEnable_u1691_u0)
begin
if (syncEnable_u1691_u0)
loopControl_u121<=1'h0;
else loopControl_u121<=scoreboard_035e2414_and;
end
always @(posedge CLK)
begin
if (scoreboard_035e2414_and)
fbReg_idx_u58<=syncEnable_u1690_u0;
end
assign mux_u2205_u0=(loopControl_u121)?fbReg_tmp_row_u58:32'h0;
always @(posedge CLK)
begin
if (scoreboard_035e2414_and)
fbReg_swapped_u58<=latch_524f1382_out;
end
assign mux_u2206_u0=(loopControl_u121)?fbReg_tmp_u58:32'h0;
always @(posedge CLK)
begin
if (scoreboard_035e2414_and)
fbReg_tmp_row_u58<=latch_525691ab_out;
end
assign mux_u2207_u0=(loopControl_u121)?fbReg_tmp_row1_u58:32'h0;
assign mux_u2208_u0=(loopControl_u121)?fbReg_idx_u58:16'h0;
always @(posedge CLK)
begin
if (scoreboard_035e2414_and)
fbReg_tmp_row1_u58<=latch_2f6d8ac8_out;
end
assign mux_u2209_u0=(loopControl_u121)?fbReg_tmp_row0_u58:32'h0;
always @(posedge CLK)
begin
if (GO)
syncEnable_u1691_u0<=RESET;
end
always @(posedge CLK)
begin
if (scoreboard_035e2414_and)
fbReg_tmp_u58<=latch_38039538_out;
end
assign and_u4327_u0=reg_20e1b780_u0&port_4fcc8c32_;
assign simplePinWrite=port_6b492ff9_[7:0];
assign simplePinWrite_u672=16'h1&{16{1'h1}};
assign simplePinWrite_u673=reg_20e1b780_u0&{1{reg_20e1b780_u0}};
always @(posedge CLK or posedge RESET)
begin
if (RESET)
reg_10c38592_u0<=1'h0;
else reg_10c38592_u0<=reg_20e1b780_u0;
end
always @(posedge CLK or posedge RESET)
begin
if (RESET)
reg_20e1b780_u0<=1'h0;
else reg_20e1b780_u0<=and_u4326_u0;
end
assign or_u1677_u0=or_u1674_u0|reg_20e1b780_u0;
assign mux_u2210_u0=(or_u1674_u0)?mux_u2197_u0:32'h32;
assign RESULT=GO;
assign RESULT_u2507=32'h0;
assign RESULT_u2508=or_u1677_u0;
assign RESULT_u2509=mux_u2210_u0;
assign RESULT_u2510=3'h1;
assign RESULT_u2511=or_u1673_u0;
assign RESULT_u2512=mux_u2196_u0;
assign RESULT_u2513=3'h1;
assign RESULT_u2514=or_u1671_u0;
assign RESULT_u2515=mux_u2196_u0;
assign RESULT_u2516=mux_u2192_u0;
assign RESULT_u2517=3'h1;
assign RESULT_u2518=simplePinWrite_u672;
assign RESULT_u2519=simplePinWrite;
assign RESULT_u2520=simplePinWrite_u673;
assign DONE=reg_10c38592_u0;
endmodule



module medianRow4_forge_memory_101x32_169(CLK, ENA, WEA, DINA, ENB, ADDRA, ADDRB, DOUTA, DOUTB, DONEA, DONEB);
input		CLK;
input		ENA;
input		WEA;
input	[31:0]	DINA;
input		ENB;
input	[31:0]	ADDRA;
input	[31:0]	ADDRB;
output	[31:0]	DOUTA;
output	[31:0]	DOUTB;
output		DONEA;
output		DONEB;
wire		wea_0;
wire	[31:0]	pre_douta_0;
wire	[31:0]	pre_doutb_0;
wire		wea_1;
wire	[31:0]	pre_douta_1;
wire	[31:0]	pre_doutb_1;
reg		wea_done;
reg		rea_done;
reg	[31:0]	mux_outa;
reg	[31:0]	mux_outb;
assign wea_0=WEA&(ADDRA[31:6]==26'h0);
assign wea_1=WEA&(ADDRA[31:6]==26'h1);
always @(posedge CLK)
begin
wea_done<=WEA;
end
assign DONEA=rea_done|wea_done;
always @(posedge CLK)
begin
end
assign DONEB=ENB;
always @(pre_douta_0 or pre_douta_1 or ADDRA)
begin
case (ADDRA[31:6])26'd0:mux_outa=pre_douta_0;
26'd1:mux_outa=pre_douta_1;
default:mux_outa=32'h0;
endcase end
assign DOUTA=mux_outa;
always @(pre_doutb_0 or pre_doutb_1 or ADDRB)
begin
case (ADDRB[31:6])26'd0:mux_outb=pre_doutb_0;
26'd1:mux_outb=pre_doutb_1;
default:mux_outb=32'h0;
endcase end
assign DOUTB=mux_outb;
// Memory array element: COL: 0, ROW: 0
//  Initialization of Dual Port LUT ram now done through explicit
// parameter setting.
RAM64X1D#(.INIT(64'h0000000000000000))RAM64X1D_instance_3712(.WCLK(CLK), .WE(wea_0), 
  .A0(ADDRA[0]), .A1(ADDRA[1]), .A2(ADDRA[2]), .A3(ADDRA[3]), .A4(ADDRA[4]), .A5(ADDRA[5]), 
  .D(DINA[0]), .SPO(pre_douta_0[0]), .DPRA0(ADDRB[0]), .DPRA1(ADDRB[1]), .DPRA2(ADDRB[2]), 
  .DPRA3(ADDRB[3]), .DPRA4(ADDRB[4]), .DPRA5(ADDRB[5]), .DPO(pre_doutb_0[0]));
// Memory array element: COL: 0, ROW: 1
//  Initialization of Dual Port LUT ram now done through explicit
// parameter setting.
RAM64X1D#(.INIT(64'h0000000000000000))RAM64X1D_instance_3713(.WCLK(CLK), .WE(wea_0), 
  .A0(ADDRA[0]), .A1(ADDRA[1]), .A2(ADDRA[2]), .A3(ADDRA[3]), .A4(ADDRA[4]), .A5(ADDRA[5]), 
  .D(DINA[1]), .SPO(pre_douta_0[1]), .DPRA0(ADDRB[0]), .DPRA1(ADDRB[1]), .DPRA2(ADDRB[2]), 
  .DPRA3(ADDRB[3]), .DPRA4(ADDRB[4]), .DPRA5(ADDRB[5]), .DPO(pre_doutb_0[1]));
// Memory array element: COL: 0, ROW: 2
//  Initialization of Dual Port LUT ram now done through explicit
// parameter setting.
RAM64X1D#(.INIT(64'h0000000000000000))RAM64X1D_instance_3714(.WCLK(CLK), .WE(wea_0), 
  .A0(ADDRA[0]), .A1(ADDRA[1]), .A2(ADDRA[2]), .A3(ADDRA[3]), .A4(ADDRA[4]), .A5(ADDRA[5]), 
  .D(DINA[2]), .SPO(pre_douta_0[2]), .DPRA0(ADDRB[0]), .DPRA1(ADDRB[1]), .DPRA2(ADDRB[2]), 
  .DPRA3(ADDRB[3]), .DPRA4(ADDRB[4]), .DPRA5(ADDRB[5]), .DPO(pre_doutb_0[2]));
// Memory array element: COL: 0, ROW: 3
//  Initialization of Dual Port LUT ram now done through explicit
// parameter setting.
RAM64X1D#(.INIT(64'h0000000000000000))RAM64X1D_instance_3715(.WCLK(CLK), .WE(wea_0), 
  .A0(ADDRA[0]), .A1(ADDRA[1]), .A2(ADDRA[2]), .A3(ADDRA[3]), .A4(ADDRA[4]), .A5(ADDRA[5]), 
  .D(DINA[3]), .SPO(pre_douta_0[3]), .DPRA0(ADDRB[0]), .DPRA1(ADDRB[1]), .DPRA2(ADDRB[2]), 
  .DPRA3(ADDRB[3]), .DPRA4(ADDRB[4]), .DPRA5(ADDRB[5]), .DPO(pre_doutb_0[3]));
// Memory array element: COL: 0, ROW: 4
//  Initialization of Dual Port LUT ram now done through explicit
// parameter setting.
RAM64X1D#(.INIT(64'h0000000000000000))RAM64X1D_instance_3716(.WCLK(CLK), .WE(wea_0), 
  .A0(ADDRA[0]), .A1(ADDRA[1]), .A2(ADDRA[2]), .A3(ADDRA[3]), .A4(ADDRA[4]), .A5(ADDRA[5]), 
  .D(DINA[4]), .SPO(pre_douta_0[4]), .DPRA0(ADDRB[0]), .DPRA1(ADDRB[1]), .DPRA2(ADDRB[2]), 
  .DPRA3(ADDRB[3]), .DPRA4(ADDRB[4]), .DPRA5(ADDRB[5]), .DPO(pre_doutb_0[4]));
// Memory array element: COL: 0, ROW: 5
//  Initialization of Dual Port LUT ram now done through explicit
// parameter setting.
RAM64X1D#(.INIT(64'h0000000000000000))RAM64X1D_instance_3717(.WCLK(CLK), .WE(wea_0), 
  .A0(ADDRA[0]), .A1(ADDRA[1]), .A2(ADDRA[2]), .A3(ADDRA[3]), .A4(ADDRA[4]), .A5(ADDRA[5]), 
  .D(DINA[5]), .SPO(pre_douta_0[5]), .DPRA0(ADDRB[0]), .DPRA1(ADDRB[1]), .DPRA2(ADDRB[2]), 
  .DPRA3(ADDRB[3]), .DPRA4(ADDRB[4]), .DPRA5(ADDRB[5]), .DPO(pre_doutb_0[5]));
// Memory array element: COL: 0, ROW: 6
//  Initialization of Dual Port LUT ram now done through explicit
// parameter setting.
RAM64X1D#(.INIT(64'h0000000000000000))RAM64X1D_instance_3718(.WCLK(CLK), .WE(wea_0), 
  .A0(ADDRA[0]), .A1(ADDRA[1]), .A2(ADDRA[2]), .A3(ADDRA[3]), .A4(ADDRA[4]), .A5(ADDRA[5]), 
  .D(DINA[6]), .SPO(pre_douta_0[6]), .DPRA0(ADDRB[0]), .DPRA1(ADDRB[1]), .DPRA2(ADDRB[2]), 
  .DPRA3(ADDRB[3]), .DPRA4(ADDRB[4]), .DPRA5(ADDRB[5]), .DPO(pre_doutb_0[6]));
// Memory array element: COL: 0, ROW: 7
//  Initialization of Dual Port LUT ram now done through explicit
// parameter setting.
RAM64X1D#(.INIT(64'h0000000000000000))RAM64X1D_instance_3719(.WCLK(CLK), .WE(wea_0), 
  .A0(ADDRA[0]), .A1(ADDRA[1]), .A2(ADDRA[2]), .A3(ADDRA[3]), .A4(ADDRA[4]), .A5(ADDRA[5]), 
  .D(DINA[7]), .SPO(pre_douta_0[7]), .DPRA0(ADDRB[0]), .DPRA1(ADDRB[1]), .DPRA2(ADDRB[2]), 
  .DPRA3(ADDRB[3]), .DPRA4(ADDRB[4]), .DPRA5(ADDRB[5]), .DPO(pre_doutb_0[7]));
// Memory array element: COL: 0, ROW: 8
//  Initialization of Dual Port LUT ram now done through explicit
// parameter setting.
RAM64X1D#(.INIT(64'h0000000000000000))RAM64X1D_instance_3720(.WCLK(CLK), .WE(wea_0), 
  .A0(ADDRA[0]), .A1(ADDRA[1]), .A2(ADDRA[2]), .A3(ADDRA[3]), .A4(ADDRA[4]), .A5(ADDRA[5]), 
  .D(DINA[8]), .SPO(pre_douta_0[8]), .DPRA0(ADDRB[0]), .DPRA1(ADDRB[1]), .DPRA2(ADDRB[2]), 
  .DPRA3(ADDRB[3]), .DPRA4(ADDRB[4]), .DPRA5(ADDRB[5]), .DPO(pre_doutb_0[8]));
// Memory array element: COL: 0, ROW: 9
//  Initialization of Dual Port LUT ram now done through explicit
// parameter setting.
RAM64X1D#(.INIT(64'h0000000000000000))RAM64X1D_instance_3721(.WCLK(CLK), .WE(wea_0), 
  .A0(ADDRA[0]), .A1(ADDRA[1]), .A2(ADDRA[2]), .A3(ADDRA[3]), .A4(ADDRA[4]), .A5(ADDRA[5]), 
  .D(DINA[9]), .SPO(pre_douta_0[9]), .DPRA0(ADDRB[0]), .DPRA1(ADDRB[1]), .DPRA2(ADDRB[2]), 
  .DPRA3(ADDRB[3]), .DPRA4(ADDRB[4]), .DPRA5(ADDRB[5]), .DPO(pre_doutb_0[9]));
// Memory array element: COL: 0, ROW: 10
//  Initialization of Dual Port LUT ram now done through explicit
// parameter setting.
RAM64X1D#(.INIT(64'h0000000000000000))RAM64X1D_instance_3722(.WCLK(CLK), .WE(wea_0), 
  .A0(ADDRA[0]), .A1(ADDRA[1]), .A2(ADDRA[2]), .A3(ADDRA[3]), .A4(ADDRA[4]), .A5(ADDRA[5]), 
  .D(DINA[10]), .SPO(pre_douta_0[10]), .DPRA0(ADDRB[0]), .DPRA1(ADDRB[1]), .DPRA2(ADDRB[2]), 
  .DPRA3(ADDRB[3]), .DPRA4(ADDRB[4]), .DPRA5(ADDRB[5]), .DPO(pre_doutb_0[10]));
// Memory array element: COL: 0, ROW: 11
//  Initialization of Dual Port LUT ram now done through explicit
// parameter setting.
RAM64X1D#(.INIT(64'h0000000000000000))RAM64X1D_instance_3723(.WCLK(CLK), .WE(wea_0), 
  .A0(ADDRA[0]), .A1(ADDRA[1]), .A2(ADDRA[2]), .A3(ADDRA[3]), .A4(ADDRA[4]), .A5(ADDRA[5]), 
  .D(DINA[11]), .SPO(pre_douta_0[11]), .DPRA0(ADDRB[0]), .DPRA1(ADDRB[1]), .DPRA2(ADDRB[2]), 
  .DPRA3(ADDRB[3]), .DPRA4(ADDRB[4]), .DPRA5(ADDRB[5]), .DPO(pre_doutb_0[11]));
// Memory array element: COL: 0, ROW: 12
//  Initialization of Dual Port LUT ram now done through explicit
// parameter setting.
RAM64X1D#(.INIT(64'h0000000000000000))RAM64X1D_instance_3724(.WCLK(CLK), .WE(wea_0), 
  .A0(ADDRA[0]), .A1(ADDRA[1]), .A2(ADDRA[2]), .A3(ADDRA[3]), .A4(ADDRA[4]), .A5(ADDRA[5]), 
  .D(DINA[12]), .SPO(pre_douta_0[12]), .DPRA0(ADDRB[0]), .DPRA1(ADDRB[1]), .DPRA2(ADDRB[2]), 
  .DPRA3(ADDRB[3]), .DPRA4(ADDRB[4]), .DPRA5(ADDRB[5]), .DPO(pre_doutb_0[12]));
// Memory array element: COL: 0, ROW: 13
//  Initialization of Dual Port LUT ram now done through explicit
// parameter setting.
RAM64X1D#(.INIT(64'h0000000000000000))RAM64X1D_instance_3725(.WCLK(CLK), .WE(wea_0), 
  .A0(ADDRA[0]), .A1(ADDRA[1]), .A2(ADDRA[2]), .A3(ADDRA[3]), .A4(ADDRA[4]), .A5(ADDRA[5]), 
  .D(DINA[13]), .SPO(pre_douta_0[13]), .DPRA0(ADDRB[0]), .DPRA1(ADDRB[1]), .DPRA2(ADDRB[2]), 
  .DPRA3(ADDRB[3]), .DPRA4(ADDRB[4]), .DPRA5(ADDRB[5]), .DPO(pre_doutb_0[13]));
// Memory array element: COL: 0, ROW: 14
//  Initialization of Dual Port LUT ram now done through explicit
// parameter setting.
RAM64X1D#(.INIT(64'h0000000000000000))RAM64X1D_instance_3726(.WCLK(CLK), .WE(wea_0), 
  .A0(ADDRA[0]), .A1(ADDRA[1]), .A2(ADDRA[2]), .A3(ADDRA[3]), .A4(ADDRA[4]), .A5(ADDRA[5]), 
  .D(DINA[14]), .SPO(pre_douta_0[14]), .DPRA0(ADDRB[0]), .DPRA1(ADDRB[1]), .DPRA2(ADDRB[2]), 
  .DPRA3(ADDRB[3]), .DPRA4(ADDRB[4]), .DPRA5(ADDRB[5]), .DPO(pre_doutb_0[14]));
// Memory array element: COL: 0, ROW: 15
//  Initialization of Dual Port LUT ram now done through explicit
// parameter setting.
RAM64X1D#(.INIT(64'h0000000000000000))RAM64X1D_instance_3727(.WCLK(CLK), .WE(wea_0), 
  .A0(ADDRA[0]), .A1(ADDRA[1]), .A2(ADDRA[2]), .A3(ADDRA[3]), .A4(ADDRA[4]), .A5(ADDRA[5]), 
  .D(DINA[15]), .SPO(pre_douta_0[15]), .DPRA0(ADDRB[0]), .DPRA1(ADDRB[1]), .DPRA2(ADDRB[2]), 
  .DPRA3(ADDRB[3]), .DPRA4(ADDRB[4]), .DPRA5(ADDRB[5]), .DPO(pre_doutb_0[15]));
// Memory array element: COL: 0, ROW: 16
//  Initialization of Dual Port LUT ram now done through explicit
// parameter setting.
RAM64X1D#(.INIT(64'h0000000000000000))RAM64X1D_instance_3728(.WCLK(CLK), .WE(wea_0), 
  .A0(ADDRA[0]), .A1(ADDRA[1]), .A2(ADDRA[2]), .A3(ADDRA[3]), .A4(ADDRA[4]), .A5(ADDRA[5]), 
  .D(DINA[16]), .SPO(pre_douta_0[16]), .DPRA0(ADDRB[0]), .DPRA1(ADDRB[1]), .DPRA2(ADDRB[2]), 
  .DPRA3(ADDRB[3]), .DPRA4(ADDRB[4]), .DPRA5(ADDRB[5]), .DPO(pre_doutb_0[16]));
// Memory array element: COL: 0, ROW: 17
//  Initialization of Dual Port LUT ram now done through explicit
// parameter setting.
RAM64X1D#(.INIT(64'h0000000000000000))RAM64X1D_instance_3729(.WCLK(CLK), .WE(wea_0), 
  .A0(ADDRA[0]), .A1(ADDRA[1]), .A2(ADDRA[2]), .A3(ADDRA[3]), .A4(ADDRA[4]), .A5(ADDRA[5]), 
  .D(DINA[17]), .SPO(pre_douta_0[17]), .DPRA0(ADDRB[0]), .DPRA1(ADDRB[1]), .DPRA2(ADDRB[2]), 
  .DPRA3(ADDRB[3]), .DPRA4(ADDRB[4]), .DPRA5(ADDRB[5]), .DPO(pre_doutb_0[17]));
// Memory array element: COL: 0, ROW: 18
//  Initialization of Dual Port LUT ram now done through explicit
// parameter setting.
RAM64X1D#(.INIT(64'h0000000000000000))RAM64X1D_instance_3730(.WCLK(CLK), .WE(wea_0), 
  .A0(ADDRA[0]), .A1(ADDRA[1]), .A2(ADDRA[2]), .A3(ADDRA[3]), .A4(ADDRA[4]), .A5(ADDRA[5]), 
  .D(DINA[18]), .SPO(pre_douta_0[18]), .DPRA0(ADDRB[0]), .DPRA1(ADDRB[1]), .DPRA2(ADDRB[2]), 
  .DPRA3(ADDRB[3]), .DPRA4(ADDRB[4]), .DPRA5(ADDRB[5]), .DPO(pre_doutb_0[18]));
// Memory array element: COL: 0, ROW: 19
//  Initialization of Dual Port LUT ram now done through explicit
// parameter setting.
RAM64X1D#(.INIT(64'h0000000000000000))RAM64X1D_instance_3731(.WCLK(CLK), .WE(wea_0), 
  .A0(ADDRA[0]), .A1(ADDRA[1]), .A2(ADDRA[2]), .A3(ADDRA[3]), .A4(ADDRA[4]), .A5(ADDRA[5]), 
  .D(DINA[19]), .SPO(pre_douta_0[19]), .DPRA0(ADDRB[0]), .DPRA1(ADDRB[1]), .DPRA2(ADDRB[2]), 
  .DPRA3(ADDRB[3]), .DPRA4(ADDRB[4]), .DPRA5(ADDRB[5]), .DPO(pre_doutb_0[19]));
// Memory array element: COL: 0, ROW: 20
//  Initialization of Dual Port LUT ram now done through explicit
// parameter setting.
RAM64X1D#(.INIT(64'h0000000000000000))RAM64X1D_instance_3732(.WCLK(CLK), .WE(wea_0), 
  .A0(ADDRA[0]), .A1(ADDRA[1]), .A2(ADDRA[2]), .A3(ADDRA[3]), .A4(ADDRA[4]), .A5(ADDRA[5]), 
  .D(DINA[20]), .SPO(pre_douta_0[20]), .DPRA0(ADDRB[0]), .DPRA1(ADDRB[1]), .DPRA2(ADDRB[2]), 
  .DPRA3(ADDRB[3]), .DPRA4(ADDRB[4]), .DPRA5(ADDRB[5]), .DPO(pre_doutb_0[20]));
// Memory array element: COL: 0, ROW: 21
//  Initialization of Dual Port LUT ram now done through explicit
// parameter setting.
RAM64X1D#(.INIT(64'h0000000000000000))RAM64X1D_instance_3733(.WCLK(CLK), .WE(wea_0), 
  .A0(ADDRA[0]), .A1(ADDRA[1]), .A2(ADDRA[2]), .A3(ADDRA[3]), .A4(ADDRA[4]), .A5(ADDRA[5]), 
  .D(DINA[21]), .SPO(pre_douta_0[21]), .DPRA0(ADDRB[0]), .DPRA1(ADDRB[1]), .DPRA2(ADDRB[2]), 
  .DPRA3(ADDRB[3]), .DPRA4(ADDRB[4]), .DPRA5(ADDRB[5]), .DPO(pre_doutb_0[21]));
// Memory array element: COL: 0, ROW: 22
//  Initialization of Dual Port LUT ram now done through explicit
// parameter setting.
RAM64X1D#(.INIT(64'h0000000000000000))RAM64X1D_instance_3734(.WCLK(CLK), .WE(wea_0), 
  .A0(ADDRA[0]), .A1(ADDRA[1]), .A2(ADDRA[2]), .A3(ADDRA[3]), .A4(ADDRA[4]), .A5(ADDRA[5]), 
  .D(DINA[22]), .SPO(pre_douta_0[22]), .DPRA0(ADDRB[0]), .DPRA1(ADDRB[1]), .DPRA2(ADDRB[2]), 
  .DPRA3(ADDRB[3]), .DPRA4(ADDRB[4]), .DPRA5(ADDRB[5]), .DPO(pre_doutb_0[22]));
// Memory array element: COL: 0, ROW: 23
//  Initialization of Dual Port LUT ram now done through explicit
// parameter setting.
RAM64X1D#(.INIT(64'h0000000000000000))RAM64X1D_instance_3735(.WCLK(CLK), .WE(wea_0), 
  .A0(ADDRA[0]), .A1(ADDRA[1]), .A2(ADDRA[2]), .A3(ADDRA[3]), .A4(ADDRA[4]), .A5(ADDRA[5]), 
  .D(DINA[23]), .SPO(pre_douta_0[23]), .DPRA0(ADDRB[0]), .DPRA1(ADDRB[1]), .DPRA2(ADDRB[2]), 
  .DPRA3(ADDRB[3]), .DPRA4(ADDRB[4]), .DPRA5(ADDRB[5]), .DPO(pre_doutb_0[23]));
// Memory array element: COL: 0, ROW: 24
//  Initialization of Dual Port LUT ram now done through explicit
// parameter setting.
RAM64X1D#(.INIT(64'h0000000000000000))RAM64X1D_instance_3736(.WCLK(CLK), .WE(wea_0), 
  .A0(ADDRA[0]), .A1(ADDRA[1]), .A2(ADDRA[2]), .A3(ADDRA[3]), .A4(ADDRA[4]), .A5(ADDRA[5]), 
  .D(DINA[24]), .SPO(pre_douta_0[24]), .DPRA0(ADDRB[0]), .DPRA1(ADDRB[1]), .DPRA2(ADDRB[2]), 
  .DPRA3(ADDRB[3]), .DPRA4(ADDRB[4]), .DPRA5(ADDRB[5]), .DPO(pre_doutb_0[24]));
// Memory array element: COL: 0, ROW: 25
//  Initialization of Dual Port LUT ram now done through explicit
// parameter setting.
RAM64X1D#(.INIT(64'h0000000000000000))RAM64X1D_instance_3737(.WCLK(CLK), .WE(wea_0), 
  .A0(ADDRA[0]), .A1(ADDRA[1]), .A2(ADDRA[2]), .A3(ADDRA[3]), .A4(ADDRA[4]), .A5(ADDRA[5]), 
  .D(DINA[25]), .SPO(pre_douta_0[25]), .DPRA0(ADDRB[0]), .DPRA1(ADDRB[1]), .DPRA2(ADDRB[2]), 
  .DPRA3(ADDRB[3]), .DPRA4(ADDRB[4]), .DPRA5(ADDRB[5]), .DPO(pre_doutb_0[25]));
// Memory array element: COL: 0, ROW: 26
//  Initialization of Dual Port LUT ram now done through explicit
// parameter setting.
RAM64X1D#(.INIT(64'h0000000000000000))RAM64X1D_instance_3738(.WCLK(CLK), .WE(wea_0), 
  .A0(ADDRA[0]), .A1(ADDRA[1]), .A2(ADDRA[2]), .A3(ADDRA[3]), .A4(ADDRA[4]), .A5(ADDRA[5]), 
  .D(DINA[26]), .SPO(pre_douta_0[26]), .DPRA0(ADDRB[0]), .DPRA1(ADDRB[1]), .DPRA2(ADDRB[2]), 
  .DPRA3(ADDRB[3]), .DPRA4(ADDRB[4]), .DPRA5(ADDRB[5]), .DPO(pre_doutb_0[26]));
// Memory array element: COL: 0, ROW: 27
//  Initialization of Dual Port LUT ram now done through explicit
// parameter setting.
RAM64X1D#(.INIT(64'h0000000000000000))RAM64X1D_instance_3739(.WCLK(CLK), .WE(wea_0), 
  .A0(ADDRA[0]), .A1(ADDRA[1]), .A2(ADDRA[2]), .A3(ADDRA[3]), .A4(ADDRA[4]), .A5(ADDRA[5]), 
  .D(DINA[27]), .SPO(pre_douta_0[27]), .DPRA0(ADDRB[0]), .DPRA1(ADDRB[1]), .DPRA2(ADDRB[2]), 
  .DPRA3(ADDRB[3]), .DPRA4(ADDRB[4]), .DPRA5(ADDRB[5]), .DPO(pre_doutb_0[27]));
// Memory array element: COL: 0, ROW: 28
//  Initialization of Dual Port LUT ram now done through explicit
// parameter setting.
RAM64X1D#(.INIT(64'h0000000000000000))RAM64X1D_instance_3740(.WCLK(CLK), .WE(wea_0), 
  .A0(ADDRA[0]), .A1(ADDRA[1]), .A2(ADDRA[2]), .A3(ADDRA[3]), .A4(ADDRA[4]), .A5(ADDRA[5]), 
  .D(DINA[28]), .SPO(pre_douta_0[28]), .DPRA0(ADDRB[0]), .DPRA1(ADDRB[1]), .DPRA2(ADDRB[2]), 
  .DPRA3(ADDRB[3]), .DPRA4(ADDRB[4]), .DPRA5(ADDRB[5]), .DPO(pre_doutb_0[28]));
// Memory array element: COL: 0, ROW: 29
//  Initialization of Dual Port LUT ram now done through explicit
// parameter setting.
RAM64X1D#(.INIT(64'h0000000000000000))RAM64X1D_instance_3741(.WCLK(CLK), .WE(wea_0), 
  .A0(ADDRA[0]), .A1(ADDRA[1]), .A2(ADDRA[2]), .A3(ADDRA[3]), .A4(ADDRA[4]), .A5(ADDRA[5]), 
  .D(DINA[29]), .SPO(pre_douta_0[29]), .DPRA0(ADDRB[0]), .DPRA1(ADDRB[1]), .DPRA2(ADDRB[2]), 
  .DPRA3(ADDRB[3]), .DPRA4(ADDRB[4]), .DPRA5(ADDRB[5]), .DPO(pre_doutb_0[29]));
// Memory array element: COL: 0, ROW: 30
//  Initialization of Dual Port LUT ram now done through explicit
// parameter setting.
RAM64X1D#(.INIT(64'h0000000000000000))RAM64X1D_instance_3742(.WCLK(CLK), .WE(wea_0), 
  .A0(ADDRA[0]), .A1(ADDRA[1]), .A2(ADDRA[2]), .A3(ADDRA[3]), .A4(ADDRA[4]), .A5(ADDRA[5]), 
  .D(DINA[30]), .SPO(pre_douta_0[30]), .DPRA0(ADDRB[0]), .DPRA1(ADDRB[1]), .DPRA2(ADDRB[2]), 
  .DPRA3(ADDRB[3]), .DPRA4(ADDRB[4]), .DPRA5(ADDRB[5]), .DPO(pre_doutb_0[30]));
// Memory array element: COL: 0, ROW: 31
//  Initialization of Dual Port LUT ram now done through explicit
// parameter setting.
RAM64X1D#(.INIT(64'h0000000000000000))RAM64X1D_instance_3743(.WCLK(CLK), .WE(wea_0), 
  .A0(ADDRA[0]), .A1(ADDRA[1]), .A2(ADDRA[2]), .A3(ADDRA[3]), .A4(ADDRA[4]), .A5(ADDRA[5]), 
  .D(DINA[31]), .SPO(pre_douta_0[31]), .DPRA0(ADDRB[0]), .DPRA1(ADDRB[1]), .DPRA2(ADDRB[2]), 
  .DPRA3(ADDRB[3]), .DPRA4(ADDRB[4]), .DPRA5(ADDRB[5]), .DPO(pre_doutb_0[31]));
// Memory array element: COL: 1, ROW: 0
//  Initialization of Dual Port LUT ram now done through explicit
// parameter setting.
RAM64X1D#(.INIT(64'h0000000000000000))RAM64X1D_instance_3744(.WCLK(CLK), .WE(wea_1), 
  .A0(ADDRA[0]), .A1(ADDRA[1]), .A2(ADDRA[2]), .A3(ADDRA[3]), .A4(ADDRA[4]), .A5(ADDRA[5]), 
  .D(DINA[0]), .SPO(pre_douta_1[0]), .DPRA0(ADDRB[0]), .DPRA1(ADDRB[1]), .DPRA2(ADDRB[2]), 
  .DPRA3(ADDRB[3]), .DPRA4(ADDRB[4]), .DPRA5(ADDRB[5]), .DPO(pre_doutb_1[0]));
// Memory array element: COL: 1, ROW: 1
//  Initialization of Dual Port LUT ram now done through explicit
// parameter setting.
RAM64X1D#(.INIT(64'h0000000000000000))RAM64X1D_instance_3745(.WCLK(CLK), .WE(wea_1), 
  .A0(ADDRA[0]), .A1(ADDRA[1]), .A2(ADDRA[2]), .A3(ADDRA[3]), .A4(ADDRA[4]), .A5(ADDRA[5]), 
  .D(DINA[1]), .SPO(pre_douta_1[1]), .DPRA0(ADDRB[0]), .DPRA1(ADDRB[1]), .DPRA2(ADDRB[2]), 
  .DPRA3(ADDRB[3]), .DPRA4(ADDRB[4]), .DPRA5(ADDRB[5]), .DPO(pre_doutb_1[1]));
// Memory array element: COL: 1, ROW: 2
//  Initialization of Dual Port LUT ram now done through explicit
// parameter setting.
RAM64X1D#(.INIT(64'h0000000000000000))RAM64X1D_instance_3746(.WCLK(CLK), .WE(wea_1), 
  .A0(ADDRA[0]), .A1(ADDRA[1]), .A2(ADDRA[2]), .A3(ADDRA[3]), .A4(ADDRA[4]), .A5(ADDRA[5]), 
  .D(DINA[2]), .SPO(pre_douta_1[2]), .DPRA0(ADDRB[0]), .DPRA1(ADDRB[1]), .DPRA2(ADDRB[2]), 
  .DPRA3(ADDRB[3]), .DPRA4(ADDRB[4]), .DPRA5(ADDRB[5]), .DPO(pre_doutb_1[2]));
// Memory array element: COL: 1, ROW: 3
//  Initialization of Dual Port LUT ram now done through explicit
// parameter setting.
RAM64X1D#(.INIT(64'h0000000000000000))RAM64X1D_instance_3747(.WCLK(CLK), .WE(wea_1), 
  .A0(ADDRA[0]), .A1(ADDRA[1]), .A2(ADDRA[2]), .A3(ADDRA[3]), .A4(ADDRA[4]), .A5(ADDRA[5]), 
  .D(DINA[3]), .SPO(pre_douta_1[3]), .DPRA0(ADDRB[0]), .DPRA1(ADDRB[1]), .DPRA2(ADDRB[2]), 
  .DPRA3(ADDRB[3]), .DPRA4(ADDRB[4]), .DPRA5(ADDRB[5]), .DPO(pre_doutb_1[3]));
// Memory array element: COL: 1, ROW: 4
//  Initialization of Dual Port LUT ram now done through explicit
// parameter setting.
RAM64X1D#(.INIT(64'h0000000000000000))RAM64X1D_instance_3748(.WCLK(CLK), .WE(wea_1), 
  .A0(ADDRA[0]), .A1(ADDRA[1]), .A2(ADDRA[2]), .A3(ADDRA[3]), .A4(ADDRA[4]), .A5(ADDRA[5]), 
  .D(DINA[4]), .SPO(pre_douta_1[4]), .DPRA0(ADDRB[0]), .DPRA1(ADDRB[1]), .DPRA2(ADDRB[2]), 
  .DPRA3(ADDRB[3]), .DPRA4(ADDRB[4]), .DPRA5(ADDRB[5]), .DPO(pre_doutb_1[4]));
// Memory array element: COL: 1, ROW: 5
//  Initialization of Dual Port LUT ram now done through explicit
// parameter setting.
RAM64X1D#(.INIT(64'h0000000000000000))RAM64X1D_instance_3749(.WCLK(CLK), .WE(wea_1), 
  .A0(ADDRA[0]), .A1(ADDRA[1]), .A2(ADDRA[2]), .A3(ADDRA[3]), .A4(ADDRA[4]), .A5(ADDRA[5]), 
  .D(DINA[5]), .SPO(pre_douta_1[5]), .DPRA0(ADDRB[0]), .DPRA1(ADDRB[1]), .DPRA2(ADDRB[2]), 
  .DPRA3(ADDRB[3]), .DPRA4(ADDRB[4]), .DPRA5(ADDRB[5]), .DPO(pre_doutb_1[5]));
// Memory array element: COL: 1, ROW: 6
//  Initialization of Dual Port LUT ram now done through explicit
// parameter setting.
RAM64X1D#(.INIT(64'h0000000000000000))RAM64X1D_instance_3750(.WCLK(CLK), .WE(wea_1), 
  .A0(ADDRA[0]), .A1(ADDRA[1]), .A2(ADDRA[2]), .A3(ADDRA[3]), .A4(ADDRA[4]), .A5(ADDRA[5]), 
  .D(DINA[6]), .SPO(pre_douta_1[6]), .DPRA0(ADDRB[0]), .DPRA1(ADDRB[1]), .DPRA2(ADDRB[2]), 
  .DPRA3(ADDRB[3]), .DPRA4(ADDRB[4]), .DPRA5(ADDRB[5]), .DPO(pre_doutb_1[6]));
// Memory array element: COL: 1, ROW: 7
//  Initialization of Dual Port LUT ram now done through explicit
// parameter setting.
RAM64X1D#(.INIT(64'h0000000000000000))RAM64X1D_instance_3751(.WCLK(CLK), .WE(wea_1), 
  .A0(ADDRA[0]), .A1(ADDRA[1]), .A2(ADDRA[2]), .A3(ADDRA[3]), .A4(ADDRA[4]), .A5(ADDRA[5]), 
  .D(DINA[7]), .SPO(pre_douta_1[7]), .DPRA0(ADDRB[0]), .DPRA1(ADDRB[1]), .DPRA2(ADDRB[2]), 
  .DPRA3(ADDRB[3]), .DPRA4(ADDRB[4]), .DPRA5(ADDRB[5]), .DPO(pre_doutb_1[7]));
// Memory array element: COL: 1, ROW: 8
//  Initialization of Dual Port LUT ram now done through explicit
// parameter setting.
RAM64X1D#(.INIT(64'h0000000000000000))RAM64X1D_instance_3752(.WCLK(CLK), .WE(wea_1), 
  .A0(ADDRA[0]), .A1(ADDRA[1]), .A2(ADDRA[2]), .A3(ADDRA[3]), .A4(ADDRA[4]), .A5(ADDRA[5]), 
  .D(DINA[8]), .SPO(pre_douta_1[8]), .DPRA0(ADDRB[0]), .DPRA1(ADDRB[1]), .DPRA2(ADDRB[2]), 
  .DPRA3(ADDRB[3]), .DPRA4(ADDRB[4]), .DPRA5(ADDRB[5]), .DPO(pre_doutb_1[8]));
// Memory array element: COL: 1, ROW: 9
//  Initialization of Dual Port LUT ram now done through explicit
// parameter setting.
RAM64X1D#(.INIT(64'h0000000000000000))RAM64X1D_instance_3753(.WCLK(CLK), .WE(wea_1), 
  .A0(ADDRA[0]), .A1(ADDRA[1]), .A2(ADDRA[2]), .A3(ADDRA[3]), .A4(ADDRA[4]), .A5(ADDRA[5]), 
  .D(DINA[9]), .SPO(pre_douta_1[9]), .DPRA0(ADDRB[0]), .DPRA1(ADDRB[1]), .DPRA2(ADDRB[2]), 
  .DPRA3(ADDRB[3]), .DPRA4(ADDRB[4]), .DPRA5(ADDRB[5]), .DPO(pre_doutb_1[9]));
// Memory array element: COL: 1, ROW: 10
//  Initialization of Dual Port LUT ram now done through explicit
// parameter setting.
RAM64X1D#(.INIT(64'h0000000000000000))RAM64X1D_instance_3754(.WCLK(CLK), .WE(wea_1), 
  .A0(ADDRA[0]), .A1(ADDRA[1]), .A2(ADDRA[2]), .A3(ADDRA[3]), .A4(ADDRA[4]), .A5(ADDRA[5]), 
  .D(DINA[10]), .SPO(pre_douta_1[10]), .DPRA0(ADDRB[0]), .DPRA1(ADDRB[1]), .DPRA2(ADDRB[2]), 
  .DPRA3(ADDRB[3]), .DPRA4(ADDRB[4]), .DPRA5(ADDRB[5]), .DPO(pre_doutb_1[10]));
// Memory array element: COL: 1, ROW: 11
//  Initialization of Dual Port LUT ram now done through explicit
// parameter setting.
RAM64X1D#(.INIT(64'h0000000000000000))RAM64X1D_instance_3755(.WCLK(CLK), .WE(wea_1), 
  .A0(ADDRA[0]), .A1(ADDRA[1]), .A2(ADDRA[2]), .A3(ADDRA[3]), .A4(ADDRA[4]), .A5(ADDRA[5]), 
  .D(DINA[11]), .SPO(pre_douta_1[11]), .DPRA0(ADDRB[0]), .DPRA1(ADDRB[1]), .DPRA2(ADDRB[2]), 
  .DPRA3(ADDRB[3]), .DPRA4(ADDRB[4]), .DPRA5(ADDRB[5]), .DPO(pre_doutb_1[11]));
// Memory array element: COL: 1, ROW: 12
//  Initialization of Dual Port LUT ram now done through explicit
// parameter setting.
RAM64X1D#(.INIT(64'h0000000000000000))RAM64X1D_instance_3756(.WCLK(CLK), .WE(wea_1), 
  .A0(ADDRA[0]), .A1(ADDRA[1]), .A2(ADDRA[2]), .A3(ADDRA[3]), .A4(ADDRA[4]), .A5(ADDRA[5]), 
  .D(DINA[12]), .SPO(pre_douta_1[12]), .DPRA0(ADDRB[0]), .DPRA1(ADDRB[1]), .DPRA2(ADDRB[2]), 
  .DPRA3(ADDRB[3]), .DPRA4(ADDRB[4]), .DPRA5(ADDRB[5]), .DPO(pre_doutb_1[12]));
// Memory array element: COL: 1, ROW: 13
//  Initialization of Dual Port LUT ram now done through explicit
// parameter setting.
RAM64X1D#(.INIT(64'h0000000000000000))RAM64X1D_instance_3757(.WCLK(CLK), .WE(wea_1), 
  .A0(ADDRA[0]), .A1(ADDRA[1]), .A2(ADDRA[2]), .A3(ADDRA[3]), .A4(ADDRA[4]), .A5(ADDRA[5]), 
  .D(DINA[13]), .SPO(pre_douta_1[13]), .DPRA0(ADDRB[0]), .DPRA1(ADDRB[1]), .DPRA2(ADDRB[2]), 
  .DPRA3(ADDRB[3]), .DPRA4(ADDRB[4]), .DPRA5(ADDRB[5]), .DPO(pre_doutb_1[13]));
// Memory array element: COL: 1, ROW: 14
//  Initialization of Dual Port LUT ram now done through explicit
// parameter setting.
RAM64X1D#(.INIT(64'h0000000000000000))RAM64X1D_instance_3758(.WCLK(CLK), .WE(wea_1), 
  .A0(ADDRA[0]), .A1(ADDRA[1]), .A2(ADDRA[2]), .A3(ADDRA[3]), .A4(ADDRA[4]), .A5(ADDRA[5]), 
  .D(DINA[14]), .SPO(pre_douta_1[14]), .DPRA0(ADDRB[0]), .DPRA1(ADDRB[1]), .DPRA2(ADDRB[2]), 
  .DPRA3(ADDRB[3]), .DPRA4(ADDRB[4]), .DPRA5(ADDRB[5]), .DPO(pre_doutb_1[14]));
// Memory array element: COL: 1, ROW: 15
//  Initialization of Dual Port LUT ram now done through explicit
// parameter setting.
RAM64X1D#(.INIT(64'h0000000000000000))RAM64X1D_instance_3759(.WCLK(CLK), .WE(wea_1), 
  .A0(ADDRA[0]), .A1(ADDRA[1]), .A2(ADDRA[2]), .A3(ADDRA[3]), .A4(ADDRA[4]), .A5(ADDRA[5]), 
  .D(DINA[15]), .SPO(pre_douta_1[15]), .DPRA0(ADDRB[0]), .DPRA1(ADDRB[1]), .DPRA2(ADDRB[2]), 
  .DPRA3(ADDRB[3]), .DPRA4(ADDRB[4]), .DPRA5(ADDRB[5]), .DPO(pre_doutb_1[15]));
// Memory array element: COL: 1, ROW: 16
//  Initialization of Dual Port LUT ram now done through explicit
// parameter setting.
RAM64X1D#(.INIT(64'h0000000000000000))RAM64X1D_instance_3760(.WCLK(CLK), .WE(wea_1), 
  .A0(ADDRA[0]), .A1(ADDRA[1]), .A2(ADDRA[2]), .A3(ADDRA[3]), .A4(ADDRA[4]), .A5(ADDRA[5]), 
  .D(DINA[16]), .SPO(pre_douta_1[16]), .DPRA0(ADDRB[0]), .DPRA1(ADDRB[1]), .DPRA2(ADDRB[2]), 
  .DPRA3(ADDRB[3]), .DPRA4(ADDRB[4]), .DPRA5(ADDRB[5]), .DPO(pre_doutb_1[16]));
// Memory array element: COL: 1, ROW: 17
//  Initialization of Dual Port LUT ram now done through explicit
// parameter setting.
RAM64X1D#(.INIT(64'h0000000000000000))RAM64X1D_instance_3761(.WCLK(CLK), .WE(wea_1), 
  .A0(ADDRA[0]), .A1(ADDRA[1]), .A2(ADDRA[2]), .A3(ADDRA[3]), .A4(ADDRA[4]), .A5(ADDRA[5]), 
  .D(DINA[17]), .SPO(pre_douta_1[17]), .DPRA0(ADDRB[0]), .DPRA1(ADDRB[1]), .DPRA2(ADDRB[2]), 
  .DPRA3(ADDRB[3]), .DPRA4(ADDRB[4]), .DPRA5(ADDRB[5]), .DPO(pre_doutb_1[17]));
// Memory array element: COL: 1, ROW: 18
//  Initialization of Dual Port LUT ram now done through explicit
// parameter setting.
RAM64X1D#(.INIT(64'h0000000000000000))RAM64X1D_instance_3762(.WCLK(CLK), .WE(wea_1), 
  .A0(ADDRA[0]), .A1(ADDRA[1]), .A2(ADDRA[2]), .A3(ADDRA[3]), .A4(ADDRA[4]), .A5(ADDRA[5]), 
  .D(DINA[18]), .SPO(pre_douta_1[18]), .DPRA0(ADDRB[0]), .DPRA1(ADDRB[1]), .DPRA2(ADDRB[2]), 
  .DPRA3(ADDRB[3]), .DPRA4(ADDRB[4]), .DPRA5(ADDRB[5]), .DPO(pre_doutb_1[18]));
// Memory array element: COL: 1, ROW: 19
//  Initialization of Dual Port LUT ram now done through explicit
// parameter setting.
RAM64X1D#(.INIT(64'h0000000000000000))RAM64X1D_instance_3763(.WCLK(CLK), .WE(wea_1), 
  .A0(ADDRA[0]), .A1(ADDRA[1]), .A2(ADDRA[2]), .A3(ADDRA[3]), .A4(ADDRA[4]), .A5(ADDRA[5]), 
  .D(DINA[19]), .SPO(pre_douta_1[19]), .DPRA0(ADDRB[0]), .DPRA1(ADDRB[1]), .DPRA2(ADDRB[2]), 
  .DPRA3(ADDRB[3]), .DPRA4(ADDRB[4]), .DPRA5(ADDRB[5]), .DPO(pre_doutb_1[19]));
// Memory array element: COL: 1, ROW: 20
//  Initialization of Dual Port LUT ram now done through explicit
// parameter setting.
RAM64X1D#(.INIT(64'h0000000000000000))RAM64X1D_instance_3764(.WCLK(CLK), .WE(wea_1), 
  .A0(ADDRA[0]), .A1(ADDRA[1]), .A2(ADDRA[2]), .A3(ADDRA[3]), .A4(ADDRA[4]), .A5(ADDRA[5]), 
  .D(DINA[20]), .SPO(pre_douta_1[20]), .DPRA0(ADDRB[0]), .DPRA1(ADDRB[1]), .DPRA2(ADDRB[2]), 
  .DPRA3(ADDRB[3]), .DPRA4(ADDRB[4]), .DPRA5(ADDRB[5]), .DPO(pre_doutb_1[20]));
// Memory array element: COL: 1, ROW: 21
//  Initialization of Dual Port LUT ram now done through explicit
// parameter setting.
RAM64X1D#(.INIT(64'h0000000000000000))RAM64X1D_instance_3765(.WCLK(CLK), .WE(wea_1), 
  .A0(ADDRA[0]), .A1(ADDRA[1]), .A2(ADDRA[2]), .A3(ADDRA[3]), .A4(ADDRA[4]), .A5(ADDRA[5]), 
  .D(DINA[21]), .SPO(pre_douta_1[21]), .DPRA0(ADDRB[0]), .DPRA1(ADDRB[1]), .DPRA2(ADDRB[2]), 
  .DPRA3(ADDRB[3]), .DPRA4(ADDRB[4]), .DPRA5(ADDRB[5]), .DPO(pre_doutb_1[21]));
// Memory array element: COL: 1, ROW: 22
//  Initialization of Dual Port LUT ram now done through explicit
// parameter setting.
RAM64X1D#(.INIT(64'h0000000000000000))RAM64X1D_instance_3766(.WCLK(CLK), .WE(wea_1), 
  .A0(ADDRA[0]), .A1(ADDRA[1]), .A2(ADDRA[2]), .A3(ADDRA[3]), .A4(ADDRA[4]), .A5(ADDRA[5]), 
  .D(DINA[22]), .SPO(pre_douta_1[22]), .DPRA0(ADDRB[0]), .DPRA1(ADDRB[1]), .DPRA2(ADDRB[2]), 
  .DPRA3(ADDRB[3]), .DPRA4(ADDRB[4]), .DPRA5(ADDRB[5]), .DPO(pre_doutb_1[22]));
// Memory array element: COL: 1, ROW: 23
//  Initialization of Dual Port LUT ram now done through explicit
// parameter setting.
RAM64X1D#(.INIT(64'h0000000000000000))RAM64X1D_instance_3767(.WCLK(CLK), .WE(wea_1), 
  .A0(ADDRA[0]), .A1(ADDRA[1]), .A2(ADDRA[2]), .A3(ADDRA[3]), .A4(ADDRA[4]), .A5(ADDRA[5]), 
  .D(DINA[23]), .SPO(pre_douta_1[23]), .DPRA0(ADDRB[0]), .DPRA1(ADDRB[1]), .DPRA2(ADDRB[2]), 
  .DPRA3(ADDRB[3]), .DPRA4(ADDRB[4]), .DPRA5(ADDRB[5]), .DPO(pre_doutb_1[23]));
// Memory array element: COL: 1, ROW: 24
//  Initialization of Dual Port LUT ram now done through explicit
// parameter setting.
RAM64X1D#(.INIT(64'h0000000000000000))RAM64X1D_instance_3768(.WCLK(CLK), .WE(wea_1), 
  .A0(ADDRA[0]), .A1(ADDRA[1]), .A2(ADDRA[2]), .A3(ADDRA[3]), .A4(ADDRA[4]), .A5(ADDRA[5]), 
  .D(DINA[24]), .SPO(pre_douta_1[24]), .DPRA0(ADDRB[0]), .DPRA1(ADDRB[1]), .DPRA2(ADDRB[2]), 
  .DPRA3(ADDRB[3]), .DPRA4(ADDRB[4]), .DPRA5(ADDRB[5]), .DPO(pre_doutb_1[24]));
// Memory array element: COL: 1, ROW: 25
//  Initialization of Dual Port LUT ram now done through explicit
// parameter setting.
RAM64X1D#(.INIT(64'h0000000000000000))RAM64X1D_instance_3769(.WCLK(CLK), .WE(wea_1), 
  .A0(ADDRA[0]), .A1(ADDRA[1]), .A2(ADDRA[2]), .A3(ADDRA[3]), .A4(ADDRA[4]), .A5(ADDRA[5]), 
  .D(DINA[25]), .SPO(pre_douta_1[25]), .DPRA0(ADDRB[0]), .DPRA1(ADDRB[1]), .DPRA2(ADDRB[2]), 
  .DPRA3(ADDRB[3]), .DPRA4(ADDRB[4]), .DPRA5(ADDRB[5]), .DPO(pre_doutb_1[25]));
// Memory array element: COL: 1, ROW: 26
//  Initialization of Dual Port LUT ram now done through explicit
// parameter setting.
RAM64X1D#(.INIT(64'h0000000000000000))RAM64X1D_instance_3770(.WCLK(CLK), .WE(wea_1), 
  .A0(ADDRA[0]), .A1(ADDRA[1]), .A2(ADDRA[2]), .A3(ADDRA[3]), .A4(ADDRA[4]), .A5(ADDRA[5]), 
  .D(DINA[26]), .SPO(pre_douta_1[26]), .DPRA0(ADDRB[0]), .DPRA1(ADDRB[1]), .DPRA2(ADDRB[2]), 
  .DPRA3(ADDRB[3]), .DPRA4(ADDRB[4]), .DPRA5(ADDRB[5]), .DPO(pre_doutb_1[26]));
// Memory array element: COL: 1, ROW: 27
//  Initialization of Dual Port LUT ram now done through explicit
// parameter setting.
RAM64X1D#(.INIT(64'h0000000000000000))RAM64X1D_instance_3771(.WCLK(CLK), .WE(wea_1), 
  .A0(ADDRA[0]), .A1(ADDRA[1]), .A2(ADDRA[2]), .A3(ADDRA[3]), .A4(ADDRA[4]), .A5(ADDRA[5]), 
  .D(DINA[27]), .SPO(pre_douta_1[27]), .DPRA0(ADDRB[0]), .DPRA1(ADDRB[1]), .DPRA2(ADDRB[2]), 
  .DPRA3(ADDRB[3]), .DPRA4(ADDRB[4]), .DPRA5(ADDRB[5]), .DPO(pre_doutb_1[27]));
// Memory array element: COL: 1, ROW: 28
//  Initialization of Dual Port LUT ram now done through explicit
// parameter setting.
RAM64X1D#(.INIT(64'h0000000000000000))RAM64X1D_instance_3772(.WCLK(CLK), .WE(wea_1), 
  .A0(ADDRA[0]), .A1(ADDRA[1]), .A2(ADDRA[2]), .A3(ADDRA[3]), .A4(ADDRA[4]), .A5(ADDRA[5]), 
  .D(DINA[28]), .SPO(pre_douta_1[28]), .DPRA0(ADDRB[0]), .DPRA1(ADDRB[1]), .DPRA2(ADDRB[2]), 
  .DPRA3(ADDRB[3]), .DPRA4(ADDRB[4]), .DPRA5(ADDRB[5]), .DPO(pre_doutb_1[28]));
// Memory array element: COL: 1, ROW: 29
//  Initialization of Dual Port LUT ram now done through explicit
// parameter setting.
RAM64X1D#(.INIT(64'h0000000000000000))RAM64X1D_instance_3773(.WCLK(CLK), .WE(wea_1), 
  .A0(ADDRA[0]), .A1(ADDRA[1]), .A2(ADDRA[2]), .A3(ADDRA[3]), .A4(ADDRA[4]), .A5(ADDRA[5]), 
  .D(DINA[29]), .SPO(pre_douta_1[29]), .DPRA0(ADDRB[0]), .DPRA1(ADDRB[1]), .DPRA2(ADDRB[2]), 
  .DPRA3(ADDRB[3]), .DPRA4(ADDRB[4]), .DPRA5(ADDRB[5]), .DPO(pre_doutb_1[29]));
// Memory array element: COL: 1, ROW: 30
//  Initialization of Dual Port LUT ram now done through explicit
// parameter setting.
RAM64X1D#(.INIT(64'h0000000000000000))RAM64X1D_instance_3774(.WCLK(CLK), .WE(wea_1), 
  .A0(ADDRA[0]), .A1(ADDRA[1]), .A2(ADDRA[2]), .A3(ADDRA[3]), .A4(ADDRA[4]), .A5(ADDRA[5]), 
  .D(DINA[30]), .SPO(pre_douta_1[30]), .DPRA0(ADDRB[0]), .DPRA1(ADDRB[1]), .DPRA2(ADDRB[2]), 
  .DPRA3(ADDRB[3]), .DPRA4(ADDRB[4]), .DPRA5(ADDRB[5]), .DPO(pre_doutb_1[30]));
// Memory array element: COL: 1, ROW: 31
//  Initialization of Dual Port LUT ram now done through explicit
// parameter setting.
RAM64X1D#(.INIT(64'h0000000000000000))RAM64X1D_instance_3775(.WCLK(CLK), .WE(wea_1), 
  .A0(ADDRA[0]), .A1(ADDRA[1]), .A2(ADDRA[2]), .A3(ADDRA[3]), .A4(ADDRA[4]), .A5(ADDRA[5]), 
  .D(DINA[31]), .SPO(pre_douta_1[31]), .DPRA0(ADDRB[0]), .DPRA1(ADDRB[1]), .DPRA2(ADDRB[2]), 
  .DPRA3(ADDRB[3]), .DPRA4(ADDRB[4]), .DPRA5(ADDRB[5]), .DPO(pre_doutb_1[31]));
endmodule



module medianRow4_structuralmemory_0ee317d2_(CLK_u104, bus_10b48a09_, bus_30cb1685_, bus_169472e8_, bus_7cbea262_, bus_49e1e50a_, bus_7272e9a8_, bus_3631d8eb_, bus_2930c05f_, bus_181a00e5_, bus_28f9a28a_, bus_46b7b7ab_, bus_71cde040_, bus_7ca0a101_);
input		CLK_u104;
input		bus_10b48a09_;
input	[31:0]	bus_30cb1685_;
input	[2:0]	bus_169472e8_;
input		bus_7cbea262_;
input	[31:0]	bus_49e1e50a_;
input	[2:0]	bus_7272e9a8_;
input		bus_3631d8eb_;
input		bus_2930c05f_;
input	[31:0]	bus_181a00e5_;
output	[31:0]	bus_28f9a28a_;
output		bus_46b7b7ab_;
output	[31:0]	bus_71cde040_;
output		bus_7ca0a101_;
wire		or_43cb4bcb_u0;
wire	[31:0]	bus_122ac5a0_;
wire	[31:0]	bus_0631a555_;
wire		not_5d288865_u0;
wire		and_6e257854_u0;
reg		logicalMem_24e5ec06_we_delay0_u0=1'h0;
wire		or_1dae6e98_u0;
assign or_43cb4bcb_u0=and_6e257854_u0|logicalMem_24e5ec06_we_delay0_u0;
medianRow4_forge_memory_101x32_169 medianRow4_forge_memory_101x32_169_instance0(.CLK(CLK_u104), 
  .ENA(or_1dae6e98_u0), .WEA(bus_2930c05f_), .DINA(bus_181a00e5_), .ADDRA(bus_49e1e50a_), 
  .DOUTA(bus_0631a555_), .DONEA(), .ENB(bus_7cbea262_), .ADDRB(bus_30cb1685_), .DOUTB(bus_122ac5a0_), 
  .DONEB());
assign not_5d288865_u0=~bus_2930c05f_;
assign bus_28f9a28a_=bus_122ac5a0_;
assign bus_46b7b7ab_=bus_7cbea262_;
assign bus_71cde040_=bus_0631a555_;
assign bus_7ca0a101_=or_43cb4bcb_u0;
assign and_6e257854_u0=bus_3631d8eb_&not_5d288865_u0;
always @(posedge CLK_u104 or posedge bus_10b48a09_)
begin
if (bus_10b48a09_)
logicalMem_24e5ec06_we_delay0_u0<=1'h0;
else logicalMem_24e5ec06_we_delay0_u0<=bus_2930c05f_;
end
assign or_1dae6e98_u0=bus_3631d8eb_|bus_2930c05f_;
endmodule



module medianRow4_endianswapper_285a4087_(endianswapper_285a4087_in, endianswapper_285a4087_out);
input		endianswapper_285a4087_in;
output		endianswapper_285a4087_out;
assign endianswapper_285a4087_out=endianswapper_285a4087_in;
endmodule



module medianRow4_endianswapper_4fd0951f_(endianswapper_4fd0951f_in, endianswapper_4fd0951f_out);
input		endianswapper_4fd0951f_in;
output		endianswapper_4fd0951f_out;
assign endianswapper_4fd0951f_out=endianswapper_4fd0951f_in;
endmodule



module medianRow4_stateVar_fsmState_medianRow4(bus_30f7b649_, bus_01735a00_, bus_738b712c_, bus_657b3268_, bus_01b7b955_);
input		bus_30f7b649_;
input		bus_01735a00_;
input		bus_738b712c_;
input		bus_657b3268_;
output		bus_01b7b955_;
reg		stateVar_fsmState_medianRow4_u5=1'h0;
wire		endianswapper_285a4087_out;
wire		endianswapper_4fd0951f_out;
assign bus_01b7b955_=endianswapper_285a4087_out;
always @(posedge bus_30f7b649_ or posedge bus_01735a00_)
begin
if (bus_01735a00_)
stateVar_fsmState_medianRow4_u5<=1'h0;
else if (bus_738b712c_)
stateVar_fsmState_medianRow4_u5<=endianswapper_4fd0951f_out;
end
medianRow4_endianswapper_285a4087_ medianRow4_endianswapper_285a4087__1(.endianswapper_285a4087_in(stateVar_fsmState_medianRow4_u5), 
  .endianswapper_285a4087_out(endianswapper_285a4087_out));
medianRow4_endianswapper_4fd0951f_ medianRow4_endianswapper_4fd0951f__1(.endianswapper_4fd0951f_in(bus_657b3268_), 
  .endianswapper_4fd0951f_out(endianswapper_4fd0951f_out));
endmodule



module medianRow4_globalreset_physical_3225545b_(bus_5bbd3ce8_, bus_26279676_, bus_3ed7adc5_);
input		bus_5bbd3ce8_;
input		bus_26279676_;
output		bus_3ed7adc5_;
wire		or_2af6d222_u0;
wire		not_04d3aba6_u0;
reg		final_u70=1'h1;
reg		cross_u70=1'h0;
reg		glitch_u70=1'h0;
wire		and_36441258_u0;
reg		sample_u70=1'h0;
assign bus_3ed7adc5_=or_2af6d222_u0;
assign or_2af6d222_u0=bus_26279676_|final_u70;
assign not_04d3aba6_u0=~and_36441258_u0;
always @(posedge bus_5bbd3ce8_)
begin
final_u70<=not_04d3aba6_u0;
end
always @(posedge bus_5bbd3ce8_)
begin
cross_u70<=sample_u70;
end
always @(posedge bus_5bbd3ce8_)
begin
glitch_u70<=cross_u70;
end
assign and_36441258_u0=cross_u70&glitch_u70;
always @(posedge bus_5bbd3ce8_)
begin
sample_u70<=1'h1;
end
endmodule


