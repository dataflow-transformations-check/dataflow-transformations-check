// __  ___ __ ___  _ __   ___  ___ 
// \ \/ / '__/ _ \| '_ \ / _ \/ __|
//  >  <| | | (_) | | | | (_) \__ \
// /_/\_\_|  \___/|_| |_|\___/|___/
// 
// Xronos synthesizer version
// Run date: Fri 23 Feb 2018 14:28:51 +0000
// 

module medianRow5(median_DATA, in1_COUNT, RESET, median_COUNT, in1_DATA, median_RDY, median_SEND, in1_ACK, CLK, median_ACK, in1_SEND);
output	[7:0]	median_DATA;
input	[15:0]	in1_COUNT;
input		RESET;
output	[15:0]	median_COUNT;
input	[7:0]	in1_DATA;
wire		compute_median_done;
input		median_RDY;
output		median_SEND;
output		in1_ACK;
wire		compute_median_go;
wire		receive_done;
input		CLK;
input		median_ACK;
wire		receive_go;
input		in1_SEND;
wire		bus_4a6cd956_;
wire		bus_575c00f8_;
wire	[31:0]	bus_3a68eaee_;
wire	[31:0]	bus_5cd8f520_;
wire		bus_74799d62_;
wire	[31:0]	bus_4692e77d_;
wire		bus_2b750b2c_;
wire	[2:0]	bus_77436a36_;
wire		bus_750bc447_;
wire	[31:0]	bus_4146616d_;
wire		bus_536ba60d_;
wire	[31:0]	bus_4996197f_;
wire		receive;
wire		medianRow5_receive_instance_DONE;
wire	[31:0]	receive_u344;
wire		receive_u347;
wire		receive_u343;
wire	[31:0]	receive_u345;
wire	[2:0]	receive_u346;
wire	[31:0]	receive_u342;
wire	[2:0]	bus_769480aa_;
wire	[31:0]	bus_052201e7_;
wire		bus_4bd27031_;
wire	[31:0]	bus_2d041b31_;
wire	[31:0]	bus_090e5ab7_;
wire		bus_0c88b159_;
wire		bus_7776c9ba_;
wire		bus_537b4701_;
wire	[31:0]	bus_658e3100_;
wire		bus_2bfbbdd0_;
wire	[2:0]	compute_median_u808;
wire	[2:0]	compute_median_u805;
wire		compute_median_u806;
wire	[7:0]	compute_median_u809;
wire		compute_median;
wire	[31:0]	compute_median_u807;
wire		compute_median_u799;
wire	[31:0]	compute_median_u798;
wire	[31:0]	compute_median_u801;
wire		compute_median_u810;
wire	[15:0]	compute_median_u811;
wire	[31:0]	compute_median_u800;
wire	[2:0]	compute_median_u802;
wire	[31:0]	compute_median_u804;
wire		medianRow5_compute_median_instance_DONE;
wire		compute_median_u803;
wire		bus_3b6739fb_;
wire		bus_5b45de31_;
wire		scheduler_u848;
wire		scheduler_u847;
wire		scheduler_u849;
wire		medianRow5_scheduler_instance_DONE;
wire		scheduler;
wire		bus_40f7bf8a_;
assign median_DATA=compute_median_u809;
assign median_COUNT=compute_median_u811;
assign compute_median_done=bus_74799d62_;
assign median_SEND=compute_median_u810;
assign in1_ACK=receive_u347;
assign compute_median_go=scheduler_u848;
assign receive_done=bus_3b6739fb_;
assign receive_go=scheduler_u849;
medianRow5_structuralmemory_6d0c4483_ medianRow5_structuralmemory_6d0c4483__1(.CLK_u103(CLK), 
  .bus_234dbc3f_(bus_2bfbbdd0_), .bus_7b7e8552_(bus_4692e77d_), .bus_099a4fc5_(3'h1), 
  .bus_01c09e8f_(bus_2b750b2c_), .bus_2574b221_(bus_2d041b31_), .bus_570e1ba8_(3'h1), 
  .bus_63854005_(bus_0c88b159_), .bus_374ce83f_(bus_7776c9ba_), .bus_34c61156_(bus_090e5ab7_), 
  .bus_5cd8f520_(bus_5cd8f520_), .bus_575c00f8_(bus_575c00f8_), .bus_3a68eaee_(bus_3a68eaee_), 
  .bus_4a6cd956_(bus_4a6cd956_));
assign bus_74799d62_=medianRow5_compute_median_instance_DONE&{1{medianRow5_compute_median_instance_DONE}};
medianRow5_simplememoryreferee_42e67cb1_ medianRow5_simplememoryreferee_42e67cb1__1(.bus_12ae9510_(CLK), 
  .bus_3d78c405_(bus_2bfbbdd0_), .bus_4de31cd7_(bus_575c00f8_), .bus_2b38f134_(bus_5cd8f520_), 
  .bus_2d38bc26_(compute_median_u806), .bus_50fb6894_(compute_median_u807), .bus_1cf94e7d_(3'h1), 
  .bus_4996197f_(bus_4996197f_), .bus_4692e77d_(bus_4692e77d_), .bus_536ba60d_(bus_536ba60d_), 
  .bus_2b750b2c_(bus_2b750b2c_), .bus_77436a36_(bus_77436a36_), .bus_4146616d_(bus_4146616d_), 
  .bus_750bc447_(bus_750bc447_));
medianRow5_receive medianRow5_receive_instance(.CLK(CLK), .RESET(bus_2bfbbdd0_), 
  .GO(receive_go), .port_257b1990_(bus_658e3100_), .port_7505c8b2_(bus_537b4701_), 
  .port_4825179b_(in1_DATA), .DONE(medianRow5_receive_instance_DONE), .RESULT(receive), 
  .RESULT_u2475(receive_u342), .RESULT_u2476(receive_u343), .RESULT_u2477(receive_u344), 
  .RESULT_u2478(receive_u345), .RESULT_u2479(receive_u346), .RESULT_u2480(receive_u347));
medianRow5_simplememoryreferee_4af7316d_ medianRow5_simplememoryreferee_4af7316d__1(.bus_08f8f23c_(CLK), 
  .bus_4282af84_(bus_2bfbbdd0_), .bus_446c2389_(bus_4a6cd956_), .bus_2710cc68_(bus_3a68eaee_), 
  .bus_3bb4e83e_(receive_u343), .bus_7bab9224_({24'b0, receive_u345[7:0]}), .bus_672e6114_(receive_u344), 
  .bus_14e14b04_(3'h1), .bus_0ce81c34_(compute_median_u803), .bus_2fb224d4_(compute_median_u799), 
  .bus_166e4518_(compute_median_u801), .bus_77055d8f_(compute_median_u800), .bus_78e96352_(3'h1), 
  .bus_090e5ab7_(bus_090e5ab7_), .bus_2d041b31_(bus_2d041b31_), .bus_7776c9ba_(bus_7776c9ba_), 
  .bus_0c88b159_(bus_0c88b159_), .bus_769480aa_(bus_769480aa_), .bus_537b4701_(bus_537b4701_), 
  .bus_052201e7_(bus_052201e7_), .bus_4bd27031_(bus_4bd27031_));
medianRow5_stateVar_i medianRow5_stateVar_i_1(.bus_7a3c2848_(CLK), .bus_22d2adb2_(bus_2bfbbdd0_), 
  .bus_51f468ba_(receive), .bus_7d99ef69_(receive_u342), .bus_16074736_(compute_median), 
  .bus_6522ea36_(32'h0), .bus_658e3100_(bus_658e3100_));
medianRow5_globalreset_physical_6819a945_ medianRow5_globalreset_physical_6819a945__1(.bus_22407b3d_(CLK), 
  .bus_6a63b797_(RESET), .bus_2bfbbdd0_(bus_2bfbbdd0_));
medianRow5_compute_median medianRow5_compute_median_instance(.CLK(CLK), .RESET(bus_2bfbbdd0_), 
  .GO(compute_median_go), .port_159e63b8_(bus_4bd27031_), .port_3a469087_(bus_4bd27031_), 
  .port_02bd747b_(bus_052201e7_), .port_5d27d6ca_(bus_750bc447_), .port_658d936b_(bus_4146616d_), 
  .DONE(medianRow5_compute_median_instance_DONE), .RESULT(compute_median), .RESULT_u2481(compute_median_u798), 
  .RESULT_u2488(compute_median_u799), .RESULT_u2489(compute_median_u800), .RESULT_u2490(compute_median_u801), 
  .RESULT_u2491(compute_median_u802), .RESULT_u2485(compute_median_u803), .RESULT_u2486(compute_median_u804), 
  .RESULT_u2487(compute_median_u805), .RESULT_u2482(compute_median_u806), .RESULT_u2483(compute_median_u807), 
  .RESULT_u2484(compute_median_u808), .RESULT_u2492(compute_median_u809), .RESULT_u2493(compute_median_u810), 
  .RESULT_u2494(compute_median_u811));
assign bus_3b6739fb_=medianRow5_receive_instance_DONE&{1{medianRow5_receive_instance_DONE}};
medianRow5_Kicker_69 medianRow5_Kicker_69_1(.CLK(CLK), .RESET(bus_2bfbbdd0_), .bus_5b45de31_(bus_5b45de31_));
medianRow5_scheduler medianRow5_scheduler_instance(.CLK(CLK), .RESET(bus_2bfbbdd0_), 
  .GO(bus_5b45de31_), .port_736d77c2_(bus_40f7bf8a_), .port_62033f75_(bus_658e3100_), 
  .port_33cf4f1c_(receive_done), .port_44d7133e_(compute_median_done), .port_4cb63261_(in1_SEND), 
  .port_5074841c_(median_RDY), .DONE(medianRow5_scheduler_instance_DONE), .RESULT(scheduler), 
  .RESULT_u2495(scheduler_u847), .RESULT_u2496(scheduler_u848), .RESULT_u2497(scheduler_u849));
medianRow5_stateVar_fsmState_medianRow5 medianRow5_stateVar_fsmState_medianRow5_1(.bus_612214c2_(CLK), 
  .bus_2d9f961f_(bus_2bfbbdd0_), .bus_7f2ba9d5_(scheduler), .bus_5f20665c_(scheduler_u847), 
  .bus_40f7bf8a_(bus_40f7bf8a_));
endmodule



module medianRow5_forge_memory_101x32_167(CLK, ENA, WEA, DINA, ENB, ADDRA, ADDRB, DOUTA, DOUTB, DONEA, DONEB);
input		CLK;
input		ENA;
input		WEA;
input	[31:0]	DINA;
input		ENB;
input	[31:0]	ADDRA;
input	[31:0]	ADDRB;
output	[31:0]	DOUTA;
output	[31:0]	DOUTB;
output		DONEA;
output		DONEB;
wire		wea_0;
wire	[31:0]	pre_douta_0;
wire	[31:0]	pre_doutb_0;
wire		wea_1;
wire	[31:0]	pre_douta_1;
wire	[31:0]	pre_doutb_1;
reg		wea_done;
reg		rea_done;
reg	[31:0]	mux_outa;
reg	[31:0]	mux_outb;
assign wea_0=WEA&(ADDRA[31:6]==26'h0);
assign wea_1=WEA&(ADDRA[31:6]==26'h1);
always @(posedge CLK)
begin
wea_done<=WEA;
end
assign DONEA=rea_done|wea_done;
always @(posedge CLK)
begin
end
assign DONEB=ENB;
always @(pre_douta_0 or pre_douta_1 or ADDRA)
begin
case (ADDRA[31:6])26'd0:mux_outa=pre_douta_0;
26'd1:mux_outa=pre_douta_1;
default:mux_outa=32'h0;
endcase end
assign DOUTA=mux_outa;
always @(pre_doutb_0 or pre_doutb_1 or ADDRB)
begin
case (ADDRB[31:6])26'd0:mux_outb=pre_doutb_0;
26'd1:mux_outb=pre_doutb_1;
default:mux_outb=32'h0;
endcase end
assign DOUTB=mux_outb;
// Memory array element: COL: 0, ROW: 0
//  Initialization of Dual Port LUT ram now done through explicit
// parameter setting.
RAM64X1D#(.INIT(64'h0000000000000000))RAM64X1D_instance_3648(.WCLK(CLK), .WE(wea_0), 
  .A0(ADDRA[0]), .A1(ADDRA[1]), .A2(ADDRA[2]), .A3(ADDRA[3]), .A4(ADDRA[4]), .A5(ADDRA[5]), 
  .D(DINA[0]), .SPO(pre_douta_0[0]), .DPRA0(ADDRB[0]), .DPRA1(ADDRB[1]), .DPRA2(ADDRB[2]), 
  .DPRA3(ADDRB[3]), .DPRA4(ADDRB[4]), .DPRA5(ADDRB[5]), .DPO(pre_doutb_0[0]));
// Memory array element: COL: 0, ROW: 1
//  Initialization of Dual Port LUT ram now done through explicit
// parameter setting.
RAM64X1D#(.INIT(64'h0000000000000000))RAM64X1D_instance_3649(.WCLK(CLK), .WE(wea_0), 
  .A0(ADDRA[0]), .A1(ADDRA[1]), .A2(ADDRA[2]), .A3(ADDRA[3]), .A4(ADDRA[4]), .A5(ADDRA[5]), 
  .D(DINA[1]), .SPO(pre_douta_0[1]), .DPRA0(ADDRB[0]), .DPRA1(ADDRB[1]), .DPRA2(ADDRB[2]), 
  .DPRA3(ADDRB[3]), .DPRA4(ADDRB[4]), .DPRA5(ADDRB[5]), .DPO(pre_doutb_0[1]));
// Memory array element: COL: 0, ROW: 2
//  Initialization of Dual Port LUT ram now done through explicit
// parameter setting.
RAM64X1D#(.INIT(64'h0000000000000000))RAM64X1D_instance_3650(.WCLK(CLK), .WE(wea_0), 
  .A0(ADDRA[0]), .A1(ADDRA[1]), .A2(ADDRA[2]), .A3(ADDRA[3]), .A4(ADDRA[4]), .A5(ADDRA[5]), 
  .D(DINA[2]), .SPO(pre_douta_0[2]), .DPRA0(ADDRB[0]), .DPRA1(ADDRB[1]), .DPRA2(ADDRB[2]), 
  .DPRA3(ADDRB[3]), .DPRA4(ADDRB[4]), .DPRA5(ADDRB[5]), .DPO(pre_doutb_0[2]));
// Memory array element: COL: 0, ROW: 3
//  Initialization of Dual Port LUT ram now done through explicit
// parameter setting.
RAM64X1D#(.INIT(64'h0000000000000000))RAM64X1D_instance_3651(.WCLK(CLK), .WE(wea_0), 
  .A0(ADDRA[0]), .A1(ADDRA[1]), .A2(ADDRA[2]), .A3(ADDRA[3]), .A4(ADDRA[4]), .A5(ADDRA[5]), 
  .D(DINA[3]), .SPO(pre_douta_0[3]), .DPRA0(ADDRB[0]), .DPRA1(ADDRB[1]), .DPRA2(ADDRB[2]), 
  .DPRA3(ADDRB[3]), .DPRA4(ADDRB[4]), .DPRA5(ADDRB[5]), .DPO(pre_doutb_0[3]));
// Memory array element: COL: 0, ROW: 4
//  Initialization of Dual Port LUT ram now done through explicit
// parameter setting.
RAM64X1D#(.INIT(64'h0000000000000000))RAM64X1D_instance_3652(.WCLK(CLK), .WE(wea_0), 
  .A0(ADDRA[0]), .A1(ADDRA[1]), .A2(ADDRA[2]), .A3(ADDRA[3]), .A4(ADDRA[4]), .A5(ADDRA[5]), 
  .D(DINA[4]), .SPO(pre_douta_0[4]), .DPRA0(ADDRB[0]), .DPRA1(ADDRB[1]), .DPRA2(ADDRB[2]), 
  .DPRA3(ADDRB[3]), .DPRA4(ADDRB[4]), .DPRA5(ADDRB[5]), .DPO(pre_doutb_0[4]));
// Memory array element: COL: 0, ROW: 5
//  Initialization of Dual Port LUT ram now done through explicit
// parameter setting.
RAM64X1D#(.INIT(64'h0000000000000000))RAM64X1D_instance_3653(.WCLK(CLK), .WE(wea_0), 
  .A0(ADDRA[0]), .A1(ADDRA[1]), .A2(ADDRA[2]), .A3(ADDRA[3]), .A4(ADDRA[4]), .A5(ADDRA[5]), 
  .D(DINA[5]), .SPO(pre_douta_0[5]), .DPRA0(ADDRB[0]), .DPRA1(ADDRB[1]), .DPRA2(ADDRB[2]), 
  .DPRA3(ADDRB[3]), .DPRA4(ADDRB[4]), .DPRA5(ADDRB[5]), .DPO(pre_doutb_0[5]));
// Memory array element: COL: 0, ROW: 6
//  Initialization of Dual Port LUT ram now done through explicit
// parameter setting.
RAM64X1D#(.INIT(64'h0000000000000000))RAM64X1D_instance_3654(.WCLK(CLK), .WE(wea_0), 
  .A0(ADDRA[0]), .A1(ADDRA[1]), .A2(ADDRA[2]), .A3(ADDRA[3]), .A4(ADDRA[4]), .A5(ADDRA[5]), 
  .D(DINA[6]), .SPO(pre_douta_0[6]), .DPRA0(ADDRB[0]), .DPRA1(ADDRB[1]), .DPRA2(ADDRB[2]), 
  .DPRA3(ADDRB[3]), .DPRA4(ADDRB[4]), .DPRA5(ADDRB[5]), .DPO(pre_doutb_0[6]));
// Memory array element: COL: 0, ROW: 7
//  Initialization of Dual Port LUT ram now done through explicit
// parameter setting.
RAM64X1D#(.INIT(64'h0000000000000000))RAM64X1D_instance_3655(.WCLK(CLK), .WE(wea_0), 
  .A0(ADDRA[0]), .A1(ADDRA[1]), .A2(ADDRA[2]), .A3(ADDRA[3]), .A4(ADDRA[4]), .A5(ADDRA[5]), 
  .D(DINA[7]), .SPO(pre_douta_0[7]), .DPRA0(ADDRB[0]), .DPRA1(ADDRB[1]), .DPRA2(ADDRB[2]), 
  .DPRA3(ADDRB[3]), .DPRA4(ADDRB[4]), .DPRA5(ADDRB[5]), .DPO(pre_doutb_0[7]));
// Memory array element: COL: 0, ROW: 8
//  Initialization of Dual Port LUT ram now done through explicit
// parameter setting.
RAM64X1D#(.INIT(64'h0000000000000000))RAM64X1D_instance_3656(.WCLK(CLK), .WE(wea_0), 
  .A0(ADDRA[0]), .A1(ADDRA[1]), .A2(ADDRA[2]), .A3(ADDRA[3]), .A4(ADDRA[4]), .A5(ADDRA[5]), 
  .D(DINA[8]), .SPO(pre_douta_0[8]), .DPRA0(ADDRB[0]), .DPRA1(ADDRB[1]), .DPRA2(ADDRB[2]), 
  .DPRA3(ADDRB[3]), .DPRA4(ADDRB[4]), .DPRA5(ADDRB[5]), .DPO(pre_doutb_0[8]));
// Memory array element: COL: 0, ROW: 9
//  Initialization of Dual Port LUT ram now done through explicit
// parameter setting.
RAM64X1D#(.INIT(64'h0000000000000000))RAM64X1D_instance_3657(.WCLK(CLK), .WE(wea_0), 
  .A0(ADDRA[0]), .A1(ADDRA[1]), .A2(ADDRA[2]), .A3(ADDRA[3]), .A4(ADDRA[4]), .A5(ADDRA[5]), 
  .D(DINA[9]), .SPO(pre_douta_0[9]), .DPRA0(ADDRB[0]), .DPRA1(ADDRB[1]), .DPRA2(ADDRB[2]), 
  .DPRA3(ADDRB[3]), .DPRA4(ADDRB[4]), .DPRA5(ADDRB[5]), .DPO(pre_doutb_0[9]));
// Memory array element: COL: 0, ROW: 10
//  Initialization of Dual Port LUT ram now done through explicit
// parameter setting.
RAM64X1D#(.INIT(64'h0000000000000000))RAM64X1D_instance_3658(.WCLK(CLK), .WE(wea_0), 
  .A0(ADDRA[0]), .A1(ADDRA[1]), .A2(ADDRA[2]), .A3(ADDRA[3]), .A4(ADDRA[4]), .A5(ADDRA[5]), 
  .D(DINA[10]), .SPO(pre_douta_0[10]), .DPRA0(ADDRB[0]), .DPRA1(ADDRB[1]), .DPRA2(ADDRB[2]), 
  .DPRA3(ADDRB[3]), .DPRA4(ADDRB[4]), .DPRA5(ADDRB[5]), .DPO(pre_doutb_0[10]));
// Memory array element: COL: 0, ROW: 11
//  Initialization of Dual Port LUT ram now done through explicit
// parameter setting.
RAM64X1D#(.INIT(64'h0000000000000000))RAM64X1D_instance_3659(.WCLK(CLK), .WE(wea_0), 
  .A0(ADDRA[0]), .A1(ADDRA[1]), .A2(ADDRA[2]), .A3(ADDRA[3]), .A4(ADDRA[4]), .A5(ADDRA[5]), 
  .D(DINA[11]), .SPO(pre_douta_0[11]), .DPRA0(ADDRB[0]), .DPRA1(ADDRB[1]), .DPRA2(ADDRB[2]), 
  .DPRA3(ADDRB[3]), .DPRA4(ADDRB[4]), .DPRA5(ADDRB[5]), .DPO(pre_doutb_0[11]));
// Memory array element: COL: 0, ROW: 12
//  Initialization of Dual Port LUT ram now done through explicit
// parameter setting.
RAM64X1D#(.INIT(64'h0000000000000000))RAM64X1D_instance_3660(.WCLK(CLK), .WE(wea_0), 
  .A0(ADDRA[0]), .A1(ADDRA[1]), .A2(ADDRA[2]), .A3(ADDRA[3]), .A4(ADDRA[4]), .A5(ADDRA[5]), 
  .D(DINA[12]), .SPO(pre_douta_0[12]), .DPRA0(ADDRB[0]), .DPRA1(ADDRB[1]), .DPRA2(ADDRB[2]), 
  .DPRA3(ADDRB[3]), .DPRA4(ADDRB[4]), .DPRA5(ADDRB[5]), .DPO(pre_doutb_0[12]));
// Memory array element: COL: 0, ROW: 13
//  Initialization of Dual Port LUT ram now done through explicit
// parameter setting.
RAM64X1D#(.INIT(64'h0000000000000000))RAM64X1D_instance_3661(.WCLK(CLK), .WE(wea_0), 
  .A0(ADDRA[0]), .A1(ADDRA[1]), .A2(ADDRA[2]), .A3(ADDRA[3]), .A4(ADDRA[4]), .A5(ADDRA[5]), 
  .D(DINA[13]), .SPO(pre_douta_0[13]), .DPRA0(ADDRB[0]), .DPRA1(ADDRB[1]), .DPRA2(ADDRB[2]), 
  .DPRA3(ADDRB[3]), .DPRA4(ADDRB[4]), .DPRA5(ADDRB[5]), .DPO(pre_doutb_0[13]));
// Memory array element: COL: 0, ROW: 14
//  Initialization of Dual Port LUT ram now done through explicit
// parameter setting.
RAM64X1D#(.INIT(64'h0000000000000000))RAM64X1D_instance_3662(.WCLK(CLK), .WE(wea_0), 
  .A0(ADDRA[0]), .A1(ADDRA[1]), .A2(ADDRA[2]), .A3(ADDRA[3]), .A4(ADDRA[4]), .A5(ADDRA[5]), 
  .D(DINA[14]), .SPO(pre_douta_0[14]), .DPRA0(ADDRB[0]), .DPRA1(ADDRB[1]), .DPRA2(ADDRB[2]), 
  .DPRA3(ADDRB[3]), .DPRA4(ADDRB[4]), .DPRA5(ADDRB[5]), .DPO(pre_doutb_0[14]));
// Memory array element: COL: 0, ROW: 15
//  Initialization of Dual Port LUT ram now done through explicit
// parameter setting.
RAM64X1D#(.INIT(64'h0000000000000000))RAM64X1D_instance_3663(.WCLK(CLK), .WE(wea_0), 
  .A0(ADDRA[0]), .A1(ADDRA[1]), .A2(ADDRA[2]), .A3(ADDRA[3]), .A4(ADDRA[4]), .A5(ADDRA[5]), 
  .D(DINA[15]), .SPO(pre_douta_0[15]), .DPRA0(ADDRB[0]), .DPRA1(ADDRB[1]), .DPRA2(ADDRB[2]), 
  .DPRA3(ADDRB[3]), .DPRA4(ADDRB[4]), .DPRA5(ADDRB[5]), .DPO(pre_doutb_0[15]));
// Memory array element: COL: 0, ROW: 16
//  Initialization of Dual Port LUT ram now done through explicit
// parameter setting.
RAM64X1D#(.INIT(64'h0000000000000000))RAM64X1D_instance_3664(.WCLK(CLK), .WE(wea_0), 
  .A0(ADDRA[0]), .A1(ADDRA[1]), .A2(ADDRA[2]), .A3(ADDRA[3]), .A4(ADDRA[4]), .A5(ADDRA[5]), 
  .D(DINA[16]), .SPO(pre_douta_0[16]), .DPRA0(ADDRB[0]), .DPRA1(ADDRB[1]), .DPRA2(ADDRB[2]), 
  .DPRA3(ADDRB[3]), .DPRA4(ADDRB[4]), .DPRA5(ADDRB[5]), .DPO(pre_doutb_0[16]));
// Memory array element: COL: 0, ROW: 17
//  Initialization of Dual Port LUT ram now done through explicit
// parameter setting.
RAM64X1D#(.INIT(64'h0000000000000000))RAM64X1D_instance_3665(.WCLK(CLK), .WE(wea_0), 
  .A0(ADDRA[0]), .A1(ADDRA[1]), .A2(ADDRA[2]), .A3(ADDRA[3]), .A4(ADDRA[4]), .A5(ADDRA[5]), 
  .D(DINA[17]), .SPO(pre_douta_0[17]), .DPRA0(ADDRB[0]), .DPRA1(ADDRB[1]), .DPRA2(ADDRB[2]), 
  .DPRA3(ADDRB[3]), .DPRA4(ADDRB[4]), .DPRA5(ADDRB[5]), .DPO(pre_doutb_0[17]));
// Memory array element: COL: 0, ROW: 18
//  Initialization of Dual Port LUT ram now done through explicit
// parameter setting.
RAM64X1D#(.INIT(64'h0000000000000000))RAM64X1D_instance_3666(.WCLK(CLK), .WE(wea_0), 
  .A0(ADDRA[0]), .A1(ADDRA[1]), .A2(ADDRA[2]), .A3(ADDRA[3]), .A4(ADDRA[4]), .A5(ADDRA[5]), 
  .D(DINA[18]), .SPO(pre_douta_0[18]), .DPRA0(ADDRB[0]), .DPRA1(ADDRB[1]), .DPRA2(ADDRB[2]), 
  .DPRA3(ADDRB[3]), .DPRA4(ADDRB[4]), .DPRA5(ADDRB[5]), .DPO(pre_doutb_0[18]));
// Memory array element: COL: 0, ROW: 19
//  Initialization of Dual Port LUT ram now done through explicit
// parameter setting.
RAM64X1D#(.INIT(64'h0000000000000000))RAM64X1D_instance_3667(.WCLK(CLK), .WE(wea_0), 
  .A0(ADDRA[0]), .A1(ADDRA[1]), .A2(ADDRA[2]), .A3(ADDRA[3]), .A4(ADDRA[4]), .A5(ADDRA[5]), 
  .D(DINA[19]), .SPO(pre_douta_0[19]), .DPRA0(ADDRB[0]), .DPRA1(ADDRB[1]), .DPRA2(ADDRB[2]), 
  .DPRA3(ADDRB[3]), .DPRA4(ADDRB[4]), .DPRA5(ADDRB[5]), .DPO(pre_doutb_0[19]));
// Memory array element: COL: 0, ROW: 20
//  Initialization of Dual Port LUT ram now done through explicit
// parameter setting.
RAM64X1D#(.INIT(64'h0000000000000000))RAM64X1D_instance_3668(.WCLK(CLK), .WE(wea_0), 
  .A0(ADDRA[0]), .A1(ADDRA[1]), .A2(ADDRA[2]), .A3(ADDRA[3]), .A4(ADDRA[4]), .A5(ADDRA[5]), 
  .D(DINA[20]), .SPO(pre_douta_0[20]), .DPRA0(ADDRB[0]), .DPRA1(ADDRB[1]), .DPRA2(ADDRB[2]), 
  .DPRA3(ADDRB[3]), .DPRA4(ADDRB[4]), .DPRA5(ADDRB[5]), .DPO(pre_doutb_0[20]));
// Memory array element: COL: 0, ROW: 21
//  Initialization of Dual Port LUT ram now done through explicit
// parameter setting.
RAM64X1D#(.INIT(64'h0000000000000000))RAM64X1D_instance_3669(.WCLK(CLK), .WE(wea_0), 
  .A0(ADDRA[0]), .A1(ADDRA[1]), .A2(ADDRA[2]), .A3(ADDRA[3]), .A4(ADDRA[4]), .A5(ADDRA[5]), 
  .D(DINA[21]), .SPO(pre_douta_0[21]), .DPRA0(ADDRB[0]), .DPRA1(ADDRB[1]), .DPRA2(ADDRB[2]), 
  .DPRA3(ADDRB[3]), .DPRA4(ADDRB[4]), .DPRA5(ADDRB[5]), .DPO(pre_doutb_0[21]));
// Memory array element: COL: 0, ROW: 22
//  Initialization of Dual Port LUT ram now done through explicit
// parameter setting.
RAM64X1D#(.INIT(64'h0000000000000000))RAM64X1D_instance_3670(.WCLK(CLK), .WE(wea_0), 
  .A0(ADDRA[0]), .A1(ADDRA[1]), .A2(ADDRA[2]), .A3(ADDRA[3]), .A4(ADDRA[4]), .A5(ADDRA[5]), 
  .D(DINA[22]), .SPO(pre_douta_0[22]), .DPRA0(ADDRB[0]), .DPRA1(ADDRB[1]), .DPRA2(ADDRB[2]), 
  .DPRA3(ADDRB[3]), .DPRA4(ADDRB[4]), .DPRA5(ADDRB[5]), .DPO(pre_doutb_0[22]));
// Memory array element: COL: 0, ROW: 23
//  Initialization of Dual Port LUT ram now done through explicit
// parameter setting.
RAM64X1D#(.INIT(64'h0000000000000000))RAM64X1D_instance_3671(.WCLK(CLK), .WE(wea_0), 
  .A0(ADDRA[0]), .A1(ADDRA[1]), .A2(ADDRA[2]), .A3(ADDRA[3]), .A4(ADDRA[4]), .A5(ADDRA[5]), 
  .D(DINA[23]), .SPO(pre_douta_0[23]), .DPRA0(ADDRB[0]), .DPRA1(ADDRB[1]), .DPRA2(ADDRB[2]), 
  .DPRA3(ADDRB[3]), .DPRA4(ADDRB[4]), .DPRA5(ADDRB[5]), .DPO(pre_doutb_0[23]));
// Memory array element: COL: 0, ROW: 24
//  Initialization of Dual Port LUT ram now done through explicit
// parameter setting.
RAM64X1D#(.INIT(64'h0000000000000000))RAM64X1D_instance_3672(.WCLK(CLK), .WE(wea_0), 
  .A0(ADDRA[0]), .A1(ADDRA[1]), .A2(ADDRA[2]), .A3(ADDRA[3]), .A4(ADDRA[4]), .A5(ADDRA[5]), 
  .D(DINA[24]), .SPO(pre_douta_0[24]), .DPRA0(ADDRB[0]), .DPRA1(ADDRB[1]), .DPRA2(ADDRB[2]), 
  .DPRA3(ADDRB[3]), .DPRA4(ADDRB[4]), .DPRA5(ADDRB[5]), .DPO(pre_doutb_0[24]));
// Memory array element: COL: 0, ROW: 25
//  Initialization of Dual Port LUT ram now done through explicit
// parameter setting.
RAM64X1D#(.INIT(64'h0000000000000000))RAM64X1D_instance_3673(.WCLK(CLK), .WE(wea_0), 
  .A0(ADDRA[0]), .A1(ADDRA[1]), .A2(ADDRA[2]), .A3(ADDRA[3]), .A4(ADDRA[4]), .A5(ADDRA[5]), 
  .D(DINA[25]), .SPO(pre_douta_0[25]), .DPRA0(ADDRB[0]), .DPRA1(ADDRB[1]), .DPRA2(ADDRB[2]), 
  .DPRA3(ADDRB[3]), .DPRA4(ADDRB[4]), .DPRA5(ADDRB[5]), .DPO(pre_doutb_0[25]));
// Memory array element: COL: 0, ROW: 26
//  Initialization of Dual Port LUT ram now done through explicit
// parameter setting.
RAM64X1D#(.INIT(64'h0000000000000000))RAM64X1D_instance_3674(.WCLK(CLK), .WE(wea_0), 
  .A0(ADDRA[0]), .A1(ADDRA[1]), .A2(ADDRA[2]), .A3(ADDRA[3]), .A4(ADDRA[4]), .A5(ADDRA[5]), 
  .D(DINA[26]), .SPO(pre_douta_0[26]), .DPRA0(ADDRB[0]), .DPRA1(ADDRB[1]), .DPRA2(ADDRB[2]), 
  .DPRA3(ADDRB[3]), .DPRA4(ADDRB[4]), .DPRA5(ADDRB[5]), .DPO(pre_doutb_0[26]));
// Memory array element: COL: 0, ROW: 27
//  Initialization of Dual Port LUT ram now done through explicit
// parameter setting.
RAM64X1D#(.INIT(64'h0000000000000000))RAM64X1D_instance_3675(.WCLK(CLK), .WE(wea_0), 
  .A0(ADDRA[0]), .A1(ADDRA[1]), .A2(ADDRA[2]), .A3(ADDRA[3]), .A4(ADDRA[4]), .A5(ADDRA[5]), 
  .D(DINA[27]), .SPO(pre_douta_0[27]), .DPRA0(ADDRB[0]), .DPRA1(ADDRB[1]), .DPRA2(ADDRB[2]), 
  .DPRA3(ADDRB[3]), .DPRA4(ADDRB[4]), .DPRA5(ADDRB[5]), .DPO(pre_doutb_0[27]));
// Memory array element: COL: 0, ROW: 28
//  Initialization of Dual Port LUT ram now done through explicit
// parameter setting.
RAM64X1D#(.INIT(64'h0000000000000000))RAM64X1D_instance_3676(.WCLK(CLK), .WE(wea_0), 
  .A0(ADDRA[0]), .A1(ADDRA[1]), .A2(ADDRA[2]), .A3(ADDRA[3]), .A4(ADDRA[4]), .A5(ADDRA[5]), 
  .D(DINA[28]), .SPO(pre_douta_0[28]), .DPRA0(ADDRB[0]), .DPRA1(ADDRB[1]), .DPRA2(ADDRB[2]), 
  .DPRA3(ADDRB[3]), .DPRA4(ADDRB[4]), .DPRA5(ADDRB[5]), .DPO(pre_doutb_0[28]));
// Memory array element: COL: 0, ROW: 29
//  Initialization of Dual Port LUT ram now done through explicit
// parameter setting.
RAM64X1D#(.INIT(64'h0000000000000000))RAM64X1D_instance_3677(.WCLK(CLK), .WE(wea_0), 
  .A0(ADDRA[0]), .A1(ADDRA[1]), .A2(ADDRA[2]), .A3(ADDRA[3]), .A4(ADDRA[4]), .A5(ADDRA[5]), 
  .D(DINA[29]), .SPO(pre_douta_0[29]), .DPRA0(ADDRB[0]), .DPRA1(ADDRB[1]), .DPRA2(ADDRB[2]), 
  .DPRA3(ADDRB[3]), .DPRA4(ADDRB[4]), .DPRA5(ADDRB[5]), .DPO(pre_doutb_0[29]));
// Memory array element: COL: 0, ROW: 30
//  Initialization of Dual Port LUT ram now done through explicit
// parameter setting.
RAM64X1D#(.INIT(64'h0000000000000000))RAM64X1D_instance_3678(.WCLK(CLK), .WE(wea_0), 
  .A0(ADDRA[0]), .A1(ADDRA[1]), .A2(ADDRA[2]), .A3(ADDRA[3]), .A4(ADDRA[4]), .A5(ADDRA[5]), 
  .D(DINA[30]), .SPO(pre_douta_0[30]), .DPRA0(ADDRB[0]), .DPRA1(ADDRB[1]), .DPRA2(ADDRB[2]), 
  .DPRA3(ADDRB[3]), .DPRA4(ADDRB[4]), .DPRA5(ADDRB[5]), .DPO(pre_doutb_0[30]));
// Memory array element: COL: 0, ROW: 31
//  Initialization of Dual Port LUT ram now done through explicit
// parameter setting.
RAM64X1D#(.INIT(64'h0000000000000000))RAM64X1D_instance_3679(.WCLK(CLK), .WE(wea_0), 
  .A0(ADDRA[0]), .A1(ADDRA[1]), .A2(ADDRA[2]), .A3(ADDRA[3]), .A4(ADDRA[4]), .A5(ADDRA[5]), 
  .D(DINA[31]), .SPO(pre_douta_0[31]), .DPRA0(ADDRB[0]), .DPRA1(ADDRB[1]), .DPRA2(ADDRB[2]), 
  .DPRA3(ADDRB[3]), .DPRA4(ADDRB[4]), .DPRA5(ADDRB[5]), .DPO(pre_doutb_0[31]));
// Memory array element: COL: 1, ROW: 0
//  Initialization of Dual Port LUT ram now done through explicit
// parameter setting.
RAM64X1D#(.INIT(64'h0000000000000000))RAM64X1D_instance_3680(.WCLK(CLK), .WE(wea_1), 
  .A0(ADDRA[0]), .A1(ADDRA[1]), .A2(ADDRA[2]), .A3(ADDRA[3]), .A4(ADDRA[4]), .A5(ADDRA[5]), 
  .D(DINA[0]), .SPO(pre_douta_1[0]), .DPRA0(ADDRB[0]), .DPRA1(ADDRB[1]), .DPRA2(ADDRB[2]), 
  .DPRA3(ADDRB[3]), .DPRA4(ADDRB[4]), .DPRA5(ADDRB[5]), .DPO(pre_doutb_1[0]));
// Memory array element: COL: 1, ROW: 1
//  Initialization of Dual Port LUT ram now done through explicit
// parameter setting.
RAM64X1D#(.INIT(64'h0000000000000000))RAM64X1D_instance_3681(.WCLK(CLK), .WE(wea_1), 
  .A0(ADDRA[0]), .A1(ADDRA[1]), .A2(ADDRA[2]), .A3(ADDRA[3]), .A4(ADDRA[4]), .A5(ADDRA[5]), 
  .D(DINA[1]), .SPO(pre_douta_1[1]), .DPRA0(ADDRB[0]), .DPRA1(ADDRB[1]), .DPRA2(ADDRB[2]), 
  .DPRA3(ADDRB[3]), .DPRA4(ADDRB[4]), .DPRA5(ADDRB[5]), .DPO(pre_doutb_1[1]));
// Memory array element: COL: 1, ROW: 2
//  Initialization of Dual Port LUT ram now done through explicit
// parameter setting.
RAM64X1D#(.INIT(64'h0000000000000000))RAM64X1D_instance_3682(.WCLK(CLK), .WE(wea_1), 
  .A0(ADDRA[0]), .A1(ADDRA[1]), .A2(ADDRA[2]), .A3(ADDRA[3]), .A4(ADDRA[4]), .A5(ADDRA[5]), 
  .D(DINA[2]), .SPO(pre_douta_1[2]), .DPRA0(ADDRB[0]), .DPRA1(ADDRB[1]), .DPRA2(ADDRB[2]), 
  .DPRA3(ADDRB[3]), .DPRA4(ADDRB[4]), .DPRA5(ADDRB[5]), .DPO(pre_doutb_1[2]));
// Memory array element: COL: 1, ROW: 3
//  Initialization of Dual Port LUT ram now done through explicit
// parameter setting.
RAM64X1D#(.INIT(64'h0000000000000000))RAM64X1D_instance_3683(.WCLK(CLK), .WE(wea_1), 
  .A0(ADDRA[0]), .A1(ADDRA[1]), .A2(ADDRA[2]), .A3(ADDRA[3]), .A4(ADDRA[4]), .A5(ADDRA[5]), 
  .D(DINA[3]), .SPO(pre_douta_1[3]), .DPRA0(ADDRB[0]), .DPRA1(ADDRB[1]), .DPRA2(ADDRB[2]), 
  .DPRA3(ADDRB[3]), .DPRA4(ADDRB[4]), .DPRA5(ADDRB[5]), .DPO(pre_doutb_1[3]));
// Memory array element: COL: 1, ROW: 4
//  Initialization of Dual Port LUT ram now done through explicit
// parameter setting.
RAM64X1D#(.INIT(64'h0000000000000000))RAM64X1D_instance_3684(.WCLK(CLK), .WE(wea_1), 
  .A0(ADDRA[0]), .A1(ADDRA[1]), .A2(ADDRA[2]), .A3(ADDRA[3]), .A4(ADDRA[4]), .A5(ADDRA[5]), 
  .D(DINA[4]), .SPO(pre_douta_1[4]), .DPRA0(ADDRB[0]), .DPRA1(ADDRB[1]), .DPRA2(ADDRB[2]), 
  .DPRA3(ADDRB[3]), .DPRA4(ADDRB[4]), .DPRA5(ADDRB[5]), .DPO(pre_doutb_1[4]));
// Memory array element: COL: 1, ROW: 5
//  Initialization of Dual Port LUT ram now done through explicit
// parameter setting.
RAM64X1D#(.INIT(64'h0000000000000000))RAM64X1D_instance_3685(.WCLK(CLK), .WE(wea_1), 
  .A0(ADDRA[0]), .A1(ADDRA[1]), .A2(ADDRA[2]), .A3(ADDRA[3]), .A4(ADDRA[4]), .A5(ADDRA[5]), 
  .D(DINA[5]), .SPO(pre_douta_1[5]), .DPRA0(ADDRB[0]), .DPRA1(ADDRB[1]), .DPRA2(ADDRB[2]), 
  .DPRA3(ADDRB[3]), .DPRA4(ADDRB[4]), .DPRA5(ADDRB[5]), .DPO(pre_doutb_1[5]));
// Memory array element: COL: 1, ROW: 6
//  Initialization of Dual Port LUT ram now done through explicit
// parameter setting.
RAM64X1D#(.INIT(64'h0000000000000000))RAM64X1D_instance_3686(.WCLK(CLK), .WE(wea_1), 
  .A0(ADDRA[0]), .A1(ADDRA[1]), .A2(ADDRA[2]), .A3(ADDRA[3]), .A4(ADDRA[4]), .A5(ADDRA[5]), 
  .D(DINA[6]), .SPO(pre_douta_1[6]), .DPRA0(ADDRB[0]), .DPRA1(ADDRB[1]), .DPRA2(ADDRB[2]), 
  .DPRA3(ADDRB[3]), .DPRA4(ADDRB[4]), .DPRA5(ADDRB[5]), .DPO(pre_doutb_1[6]));
// Memory array element: COL: 1, ROW: 7
//  Initialization of Dual Port LUT ram now done through explicit
// parameter setting.
RAM64X1D#(.INIT(64'h0000000000000000))RAM64X1D_instance_3687(.WCLK(CLK), .WE(wea_1), 
  .A0(ADDRA[0]), .A1(ADDRA[1]), .A2(ADDRA[2]), .A3(ADDRA[3]), .A4(ADDRA[4]), .A5(ADDRA[5]), 
  .D(DINA[7]), .SPO(pre_douta_1[7]), .DPRA0(ADDRB[0]), .DPRA1(ADDRB[1]), .DPRA2(ADDRB[2]), 
  .DPRA3(ADDRB[3]), .DPRA4(ADDRB[4]), .DPRA5(ADDRB[5]), .DPO(pre_doutb_1[7]));
// Memory array element: COL: 1, ROW: 8
//  Initialization of Dual Port LUT ram now done through explicit
// parameter setting.
RAM64X1D#(.INIT(64'h0000000000000000))RAM64X1D_instance_3688(.WCLK(CLK), .WE(wea_1), 
  .A0(ADDRA[0]), .A1(ADDRA[1]), .A2(ADDRA[2]), .A3(ADDRA[3]), .A4(ADDRA[4]), .A5(ADDRA[5]), 
  .D(DINA[8]), .SPO(pre_douta_1[8]), .DPRA0(ADDRB[0]), .DPRA1(ADDRB[1]), .DPRA2(ADDRB[2]), 
  .DPRA3(ADDRB[3]), .DPRA4(ADDRB[4]), .DPRA5(ADDRB[5]), .DPO(pre_doutb_1[8]));
// Memory array element: COL: 1, ROW: 9
//  Initialization of Dual Port LUT ram now done through explicit
// parameter setting.
RAM64X1D#(.INIT(64'h0000000000000000))RAM64X1D_instance_3689(.WCLK(CLK), .WE(wea_1), 
  .A0(ADDRA[0]), .A1(ADDRA[1]), .A2(ADDRA[2]), .A3(ADDRA[3]), .A4(ADDRA[4]), .A5(ADDRA[5]), 
  .D(DINA[9]), .SPO(pre_douta_1[9]), .DPRA0(ADDRB[0]), .DPRA1(ADDRB[1]), .DPRA2(ADDRB[2]), 
  .DPRA3(ADDRB[3]), .DPRA4(ADDRB[4]), .DPRA5(ADDRB[5]), .DPO(pre_doutb_1[9]));
// Memory array element: COL: 1, ROW: 10
//  Initialization of Dual Port LUT ram now done through explicit
// parameter setting.
RAM64X1D#(.INIT(64'h0000000000000000))RAM64X1D_instance_3690(.WCLK(CLK), .WE(wea_1), 
  .A0(ADDRA[0]), .A1(ADDRA[1]), .A2(ADDRA[2]), .A3(ADDRA[3]), .A4(ADDRA[4]), .A5(ADDRA[5]), 
  .D(DINA[10]), .SPO(pre_douta_1[10]), .DPRA0(ADDRB[0]), .DPRA1(ADDRB[1]), .DPRA2(ADDRB[2]), 
  .DPRA3(ADDRB[3]), .DPRA4(ADDRB[4]), .DPRA5(ADDRB[5]), .DPO(pre_doutb_1[10]));
// Memory array element: COL: 1, ROW: 11
//  Initialization of Dual Port LUT ram now done through explicit
// parameter setting.
RAM64X1D#(.INIT(64'h0000000000000000))RAM64X1D_instance_3691(.WCLK(CLK), .WE(wea_1), 
  .A0(ADDRA[0]), .A1(ADDRA[1]), .A2(ADDRA[2]), .A3(ADDRA[3]), .A4(ADDRA[4]), .A5(ADDRA[5]), 
  .D(DINA[11]), .SPO(pre_douta_1[11]), .DPRA0(ADDRB[0]), .DPRA1(ADDRB[1]), .DPRA2(ADDRB[2]), 
  .DPRA3(ADDRB[3]), .DPRA4(ADDRB[4]), .DPRA5(ADDRB[5]), .DPO(pre_doutb_1[11]));
// Memory array element: COL: 1, ROW: 12
//  Initialization of Dual Port LUT ram now done through explicit
// parameter setting.
RAM64X1D#(.INIT(64'h0000000000000000))RAM64X1D_instance_3692(.WCLK(CLK), .WE(wea_1), 
  .A0(ADDRA[0]), .A1(ADDRA[1]), .A2(ADDRA[2]), .A3(ADDRA[3]), .A4(ADDRA[4]), .A5(ADDRA[5]), 
  .D(DINA[12]), .SPO(pre_douta_1[12]), .DPRA0(ADDRB[0]), .DPRA1(ADDRB[1]), .DPRA2(ADDRB[2]), 
  .DPRA3(ADDRB[3]), .DPRA4(ADDRB[4]), .DPRA5(ADDRB[5]), .DPO(pre_doutb_1[12]));
// Memory array element: COL: 1, ROW: 13
//  Initialization of Dual Port LUT ram now done through explicit
// parameter setting.
RAM64X1D#(.INIT(64'h0000000000000000))RAM64X1D_instance_3693(.WCLK(CLK), .WE(wea_1), 
  .A0(ADDRA[0]), .A1(ADDRA[1]), .A2(ADDRA[2]), .A3(ADDRA[3]), .A4(ADDRA[4]), .A5(ADDRA[5]), 
  .D(DINA[13]), .SPO(pre_douta_1[13]), .DPRA0(ADDRB[0]), .DPRA1(ADDRB[1]), .DPRA2(ADDRB[2]), 
  .DPRA3(ADDRB[3]), .DPRA4(ADDRB[4]), .DPRA5(ADDRB[5]), .DPO(pre_doutb_1[13]));
// Memory array element: COL: 1, ROW: 14
//  Initialization of Dual Port LUT ram now done through explicit
// parameter setting.
RAM64X1D#(.INIT(64'h0000000000000000))RAM64X1D_instance_3694(.WCLK(CLK), .WE(wea_1), 
  .A0(ADDRA[0]), .A1(ADDRA[1]), .A2(ADDRA[2]), .A3(ADDRA[3]), .A4(ADDRA[4]), .A5(ADDRA[5]), 
  .D(DINA[14]), .SPO(pre_douta_1[14]), .DPRA0(ADDRB[0]), .DPRA1(ADDRB[1]), .DPRA2(ADDRB[2]), 
  .DPRA3(ADDRB[3]), .DPRA4(ADDRB[4]), .DPRA5(ADDRB[5]), .DPO(pre_doutb_1[14]));
// Memory array element: COL: 1, ROW: 15
//  Initialization of Dual Port LUT ram now done through explicit
// parameter setting.
RAM64X1D#(.INIT(64'h0000000000000000))RAM64X1D_instance_3695(.WCLK(CLK), .WE(wea_1), 
  .A0(ADDRA[0]), .A1(ADDRA[1]), .A2(ADDRA[2]), .A3(ADDRA[3]), .A4(ADDRA[4]), .A5(ADDRA[5]), 
  .D(DINA[15]), .SPO(pre_douta_1[15]), .DPRA0(ADDRB[0]), .DPRA1(ADDRB[1]), .DPRA2(ADDRB[2]), 
  .DPRA3(ADDRB[3]), .DPRA4(ADDRB[4]), .DPRA5(ADDRB[5]), .DPO(pre_doutb_1[15]));
// Memory array element: COL: 1, ROW: 16
//  Initialization of Dual Port LUT ram now done through explicit
// parameter setting.
RAM64X1D#(.INIT(64'h0000000000000000))RAM64X1D_instance_3696(.WCLK(CLK), .WE(wea_1), 
  .A0(ADDRA[0]), .A1(ADDRA[1]), .A2(ADDRA[2]), .A3(ADDRA[3]), .A4(ADDRA[4]), .A5(ADDRA[5]), 
  .D(DINA[16]), .SPO(pre_douta_1[16]), .DPRA0(ADDRB[0]), .DPRA1(ADDRB[1]), .DPRA2(ADDRB[2]), 
  .DPRA3(ADDRB[3]), .DPRA4(ADDRB[4]), .DPRA5(ADDRB[5]), .DPO(pre_doutb_1[16]));
// Memory array element: COL: 1, ROW: 17
//  Initialization of Dual Port LUT ram now done through explicit
// parameter setting.
RAM64X1D#(.INIT(64'h0000000000000000))RAM64X1D_instance_3697(.WCLK(CLK), .WE(wea_1), 
  .A0(ADDRA[0]), .A1(ADDRA[1]), .A2(ADDRA[2]), .A3(ADDRA[3]), .A4(ADDRA[4]), .A5(ADDRA[5]), 
  .D(DINA[17]), .SPO(pre_douta_1[17]), .DPRA0(ADDRB[0]), .DPRA1(ADDRB[1]), .DPRA2(ADDRB[2]), 
  .DPRA3(ADDRB[3]), .DPRA4(ADDRB[4]), .DPRA5(ADDRB[5]), .DPO(pre_doutb_1[17]));
// Memory array element: COL: 1, ROW: 18
//  Initialization of Dual Port LUT ram now done through explicit
// parameter setting.
RAM64X1D#(.INIT(64'h0000000000000000))RAM64X1D_instance_3698(.WCLK(CLK), .WE(wea_1), 
  .A0(ADDRA[0]), .A1(ADDRA[1]), .A2(ADDRA[2]), .A3(ADDRA[3]), .A4(ADDRA[4]), .A5(ADDRA[5]), 
  .D(DINA[18]), .SPO(pre_douta_1[18]), .DPRA0(ADDRB[0]), .DPRA1(ADDRB[1]), .DPRA2(ADDRB[2]), 
  .DPRA3(ADDRB[3]), .DPRA4(ADDRB[4]), .DPRA5(ADDRB[5]), .DPO(pre_doutb_1[18]));
// Memory array element: COL: 1, ROW: 19
//  Initialization of Dual Port LUT ram now done through explicit
// parameter setting.
RAM64X1D#(.INIT(64'h0000000000000000))RAM64X1D_instance_3699(.WCLK(CLK), .WE(wea_1), 
  .A0(ADDRA[0]), .A1(ADDRA[1]), .A2(ADDRA[2]), .A3(ADDRA[3]), .A4(ADDRA[4]), .A5(ADDRA[5]), 
  .D(DINA[19]), .SPO(pre_douta_1[19]), .DPRA0(ADDRB[0]), .DPRA1(ADDRB[1]), .DPRA2(ADDRB[2]), 
  .DPRA3(ADDRB[3]), .DPRA4(ADDRB[4]), .DPRA5(ADDRB[5]), .DPO(pre_doutb_1[19]));
// Memory array element: COL: 1, ROW: 20
//  Initialization of Dual Port LUT ram now done through explicit
// parameter setting.
RAM64X1D#(.INIT(64'h0000000000000000))RAM64X1D_instance_3700(.WCLK(CLK), .WE(wea_1), 
  .A0(ADDRA[0]), .A1(ADDRA[1]), .A2(ADDRA[2]), .A3(ADDRA[3]), .A4(ADDRA[4]), .A5(ADDRA[5]), 
  .D(DINA[20]), .SPO(pre_douta_1[20]), .DPRA0(ADDRB[0]), .DPRA1(ADDRB[1]), .DPRA2(ADDRB[2]), 
  .DPRA3(ADDRB[3]), .DPRA4(ADDRB[4]), .DPRA5(ADDRB[5]), .DPO(pre_doutb_1[20]));
// Memory array element: COL: 1, ROW: 21
//  Initialization of Dual Port LUT ram now done through explicit
// parameter setting.
RAM64X1D#(.INIT(64'h0000000000000000))RAM64X1D_instance_3701(.WCLK(CLK), .WE(wea_1), 
  .A0(ADDRA[0]), .A1(ADDRA[1]), .A2(ADDRA[2]), .A3(ADDRA[3]), .A4(ADDRA[4]), .A5(ADDRA[5]), 
  .D(DINA[21]), .SPO(pre_douta_1[21]), .DPRA0(ADDRB[0]), .DPRA1(ADDRB[1]), .DPRA2(ADDRB[2]), 
  .DPRA3(ADDRB[3]), .DPRA4(ADDRB[4]), .DPRA5(ADDRB[5]), .DPO(pre_doutb_1[21]));
// Memory array element: COL: 1, ROW: 22
//  Initialization of Dual Port LUT ram now done through explicit
// parameter setting.
RAM64X1D#(.INIT(64'h0000000000000000))RAM64X1D_instance_3702(.WCLK(CLK), .WE(wea_1), 
  .A0(ADDRA[0]), .A1(ADDRA[1]), .A2(ADDRA[2]), .A3(ADDRA[3]), .A4(ADDRA[4]), .A5(ADDRA[5]), 
  .D(DINA[22]), .SPO(pre_douta_1[22]), .DPRA0(ADDRB[0]), .DPRA1(ADDRB[1]), .DPRA2(ADDRB[2]), 
  .DPRA3(ADDRB[3]), .DPRA4(ADDRB[4]), .DPRA5(ADDRB[5]), .DPO(pre_doutb_1[22]));
// Memory array element: COL: 1, ROW: 23
//  Initialization of Dual Port LUT ram now done through explicit
// parameter setting.
RAM64X1D#(.INIT(64'h0000000000000000))RAM64X1D_instance_3703(.WCLK(CLK), .WE(wea_1), 
  .A0(ADDRA[0]), .A1(ADDRA[1]), .A2(ADDRA[2]), .A3(ADDRA[3]), .A4(ADDRA[4]), .A5(ADDRA[5]), 
  .D(DINA[23]), .SPO(pre_douta_1[23]), .DPRA0(ADDRB[0]), .DPRA1(ADDRB[1]), .DPRA2(ADDRB[2]), 
  .DPRA3(ADDRB[3]), .DPRA4(ADDRB[4]), .DPRA5(ADDRB[5]), .DPO(pre_doutb_1[23]));
// Memory array element: COL: 1, ROW: 24
//  Initialization of Dual Port LUT ram now done through explicit
// parameter setting.
RAM64X1D#(.INIT(64'h0000000000000000))RAM64X1D_instance_3704(.WCLK(CLK), .WE(wea_1), 
  .A0(ADDRA[0]), .A1(ADDRA[1]), .A2(ADDRA[2]), .A3(ADDRA[3]), .A4(ADDRA[4]), .A5(ADDRA[5]), 
  .D(DINA[24]), .SPO(pre_douta_1[24]), .DPRA0(ADDRB[0]), .DPRA1(ADDRB[1]), .DPRA2(ADDRB[2]), 
  .DPRA3(ADDRB[3]), .DPRA4(ADDRB[4]), .DPRA5(ADDRB[5]), .DPO(pre_doutb_1[24]));
// Memory array element: COL: 1, ROW: 25
//  Initialization of Dual Port LUT ram now done through explicit
// parameter setting.
RAM64X1D#(.INIT(64'h0000000000000000))RAM64X1D_instance_3705(.WCLK(CLK), .WE(wea_1), 
  .A0(ADDRA[0]), .A1(ADDRA[1]), .A2(ADDRA[2]), .A3(ADDRA[3]), .A4(ADDRA[4]), .A5(ADDRA[5]), 
  .D(DINA[25]), .SPO(pre_douta_1[25]), .DPRA0(ADDRB[0]), .DPRA1(ADDRB[1]), .DPRA2(ADDRB[2]), 
  .DPRA3(ADDRB[3]), .DPRA4(ADDRB[4]), .DPRA5(ADDRB[5]), .DPO(pre_doutb_1[25]));
// Memory array element: COL: 1, ROW: 26
//  Initialization of Dual Port LUT ram now done through explicit
// parameter setting.
RAM64X1D#(.INIT(64'h0000000000000000))RAM64X1D_instance_3706(.WCLK(CLK), .WE(wea_1), 
  .A0(ADDRA[0]), .A1(ADDRA[1]), .A2(ADDRA[2]), .A3(ADDRA[3]), .A4(ADDRA[4]), .A5(ADDRA[5]), 
  .D(DINA[26]), .SPO(pre_douta_1[26]), .DPRA0(ADDRB[0]), .DPRA1(ADDRB[1]), .DPRA2(ADDRB[2]), 
  .DPRA3(ADDRB[3]), .DPRA4(ADDRB[4]), .DPRA5(ADDRB[5]), .DPO(pre_doutb_1[26]));
// Memory array element: COL: 1, ROW: 27
//  Initialization of Dual Port LUT ram now done through explicit
// parameter setting.
RAM64X1D#(.INIT(64'h0000000000000000))RAM64X1D_instance_3707(.WCLK(CLK), .WE(wea_1), 
  .A0(ADDRA[0]), .A1(ADDRA[1]), .A2(ADDRA[2]), .A3(ADDRA[3]), .A4(ADDRA[4]), .A5(ADDRA[5]), 
  .D(DINA[27]), .SPO(pre_douta_1[27]), .DPRA0(ADDRB[0]), .DPRA1(ADDRB[1]), .DPRA2(ADDRB[2]), 
  .DPRA3(ADDRB[3]), .DPRA4(ADDRB[4]), .DPRA5(ADDRB[5]), .DPO(pre_doutb_1[27]));
// Memory array element: COL: 1, ROW: 28
//  Initialization of Dual Port LUT ram now done through explicit
// parameter setting.
RAM64X1D#(.INIT(64'h0000000000000000))RAM64X1D_instance_3708(.WCLK(CLK), .WE(wea_1), 
  .A0(ADDRA[0]), .A1(ADDRA[1]), .A2(ADDRA[2]), .A3(ADDRA[3]), .A4(ADDRA[4]), .A5(ADDRA[5]), 
  .D(DINA[28]), .SPO(pre_douta_1[28]), .DPRA0(ADDRB[0]), .DPRA1(ADDRB[1]), .DPRA2(ADDRB[2]), 
  .DPRA3(ADDRB[3]), .DPRA4(ADDRB[4]), .DPRA5(ADDRB[5]), .DPO(pre_doutb_1[28]));
// Memory array element: COL: 1, ROW: 29
//  Initialization of Dual Port LUT ram now done through explicit
// parameter setting.
RAM64X1D#(.INIT(64'h0000000000000000))RAM64X1D_instance_3709(.WCLK(CLK), .WE(wea_1), 
  .A0(ADDRA[0]), .A1(ADDRA[1]), .A2(ADDRA[2]), .A3(ADDRA[3]), .A4(ADDRA[4]), .A5(ADDRA[5]), 
  .D(DINA[29]), .SPO(pre_douta_1[29]), .DPRA0(ADDRB[0]), .DPRA1(ADDRB[1]), .DPRA2(ADDRB[2]), 
  .DPRA3(ADDRB[3]), .DPRA4(ADDRB[4]), .DPRA5(ADDRB[5]), .DPO(pre_doutb_1[29]));
// Memory array element: COL: 1, ROW: 30
//  Initialization of Dual Port LUT ram now done through explicit
// parameter setting.
RAM64X1D#(.INIT(64'h0000000000000000))RAM64X1D_instance_3710(.WCLK(CLK), .WE(wea_1), 
  .A0(ADDRA[0]), .A1(ADDRA[1]), .A2(ADDRA[2]), .A3(ADDRA[3]), .A4(ADDRA[4]), .A5(ADDRA[5]), 
  .D(DINA[30]), .SPO(pre_douta_1[30]), .DPRA0(ADDRB[0]), .DPRA1(ADDRB[1]), .DPRA2(ADDRB[2]), 
  .DPRA3(ADDRB[3]), .DPRA4(ADDRB[4]), .DPRA5(ADDRB[5]), .DPO(pre_doutb_1[30]));
// Memory array element: COL: 1, ROW: 31
//  Initialization of Dual Port LUT ram now done through explicit
// parameter setting.
RAM64X1D#(.INIT(64'h0000000000000000))RAM64X1D_instance_3711(.WCLK(CLK), .WE(wea_1), 
  .A0(ADDRA[0]), .A1(ADDRA[1]), .A2(ADDRA[2]), .A3(ADDRA[3]), .A4(ADDRA[4]), .A5(ADDRA[5]), 
  .D(DINA[31]), .SPO(pre_douta_1[31]), .DPRA0(ADDRB[0]), .DPRA1(ADDRB[1]), .DPRA2(ADDRB[2]), 
  .DPRA3(ADDRB[3]), .DPRA4(ADDRB[4]), .DPRA5(ADDRB[5]), .DPO(pre_doutb_1[31]));
endmodule



module medianRow5_structuralmemory_6d0c4483_(CLK_u103, bus_234dbc3f_, bus_7b7e8552_, bus_099a4fc5_, bus_01c09e8f_, bus_2574b221_, bus_570e1ba8_, bus_63854005_, bus_374ce83f_, bus_34c61156_, bus_5cd8f520_, bus_575c00f8_, bus_3a68eaee_, bus_4a6cd956_);
input		CLK_u103;
input		bus_234dbc3f_;
input	[31:0]	bus_7b7e8552_;
input	[2:0]	bus_099a4fc5_;
input		bus_01c09e8f_;
input	[31:0]	bus_2574b221_;
input	[2:0]	bus_570e1ba8_;
input		bus_63854005_;
input		bus_374ce83f_;
input	[31:0]	bus_34c61156_;
output	[31:0]	bus_5cd8f520_;
output		bus_575c00f8_;
output	[31:0]	bus_3a68eaee_;
output		bus_4a6cd956_;
reg		logicalMem_19c1abea_we_delay0_u0=1'h0;
wire		not_518c3b85_u0;
wire		and_7c11875b_u0;
wire	[31:0]	bus_6d03ab58_;
wire	[31:0]	bus_5b975372_;
wire		or_007cd412_u0;
wire		or_04559080_u0;
always @(posedge CLK_u103 or posedge bus_234dbc3f_)
begin
if (bus_234dbc3f_)
logicalMem_19c1abea_we_delay0_u0<=1'h0;
else logicalMem_19c1abea_we_delay0_u0<=bus_374ce83f_;
end
assign not_518c3b85_u0=~bus_374ce83f_;
assign and_7c11875b_u0=bus_63854005_&not_518c3b85_u0;
medianRow5_forge_memory_101x32_167 medianRow5_forge_memory_101x32_167_instance0(.CLK(CLK_u103), 
  .ENA(or_007cd412_u0), .WEA(bus_374ce83f_), .DINA(bus_34c61156_), .ADDRA(bus_2574b221_), 
  .DOUTA(bus_6d03ab58_), .DONEA(), .ENB(bus_01c09e8f_), .ADDRB(bus_7b7e8552_), .DOUTB(bus_5b975372_), 
  .DONEB());
assign or_007cd412_u0=bus_63854005_|bus_374ce83f_;
assign bus_5cd8f520_=bus_5b975372_;
assign bus_575c00f8_=bus_01c09e8f_;
assign bus_3a68eaee_=bus_6d03ab58_;
assign bus_4a6cd956_=or_04559080_u0;
assign or_04559080_u0=and_7c11875b_u0|logicalMem_19c1abea_we_delay0_u0;
endmodule



module medianRow5_simplememoryreferee_42e67cb1_(bus_12ae9510_, bus_3d78c405_, bus_4de31cd7_, bus_2b38f134_, bus_2d38bc26_, bus_50fb6894_, bus_1cf94e7d_, bus_4996197f_, bus_4692e77d_, bus_536ba60d_, bus_2b750b2c_, bus_77436a36_, bus_4146616d_, bus_750bc447_);
input		bus_12ae9510_;
input		bus_3d78c405_;
input		bus_4de31cd7_;
input	[31:0]	bus_2b38f134_;
input		bus_2d38bc26_;
input	[31:0]	bus_50fb6894_;
input	[2:0]	bus_1cf94e7d_;
output	[31:0]	bus_4996197f_;
output	[31:0]	bus_4692e77d_;
output		bus_536ba60d_;
output		bus_2b750b2c_;
output	[2:0]	bus_77436a36_;
output	[31:0]	bus_4146616d_;
output		bus_750bc447_;
assign bus_4996197f_=32'h0;
assign bus_4692e77d_=bus_50fb6894_;
assign bus_536ba60d_=1'h0;
assign bus_2b750b2c_=bus_2d38bc26_;
assign bus_77436a36_=3'h1;
assign bus_4146616d_=bus_2b38f134_;
assign bus_750bc447_=bus_4de31cd7_;
endmodule



module medianRow5_receive(CLK, RESET, GO, port_257b1990_, port_7505c8b2_, port_4825179b_, RESULT, RESULT_u2475, RESULT_u2476, RESULT_u2477, RESULT_u2478, RESULT_u2479, RESULT_u2480, DONE);
input		CLK;
input		RESET;
input		GO;
input	[31:0]	port_257b1990_;
input		port_7505c8b2_;
input	[7:0]	port_4825179b_;
output		RESULT;
output	[31:0]	RESULT_u2475;
output		RESULT_u2476;
output	[31:0]	RESULT_u2477;
output	[31:0]	RESULT_u2478;
output	[2:0]	RESULT_u2479;
output		RESULT_u2480;
output		DONE;
wire		simplePinWrite;
wire	[31:0]	add;
reg		reg_3e4255d2_u0=1'h0;
wire		or_u1642_u0;
wire		and_u4244_u0;
wire	[31:0]	add_u793;
reg		reg_1171f600_u0=1'h0;
assign simplePinWrite=GO&{1{GO}};
assign add=port_257b1990_+32'h0;
always @(posedge CLK or posedge GO or posedge or_u1642_u0)
begin
if (or_u1642_u0)
reg_3e4255d2_u0<=1'h0;
else if (GO)
reg_3e4255d2_u0<=1'h1;
else reg_3e4255d2_u0<=reg_3e4255d2_u0;
end
assign or_u1642_u0=and_u4244_u0|RESET;
assign and_u4244_u0=reg_3e4255d2_u0&port_7505c8b2_;
assign add_u793=port_257b1990_+32'h1;
always @(posedge CLK or posedge RESET)
begin
if (RESET)
reg_1171f600_u0<=1'h0;
else reg_1171f600_u0<=GO;
end
assign RESULT=GO;
assign RESULT_u2475=add_u793;
assign RESULT_u2476=GO;
assign RESULT_u2477=add;
assign RESULT_u2478={24'b0, port_4825179b_};
assign RESULT_u2479=3'h1;
assign RESULT_u2480=simplePinWrite;
assign DONE=reg_1171f600_u0;
endmodule



module medianRow5_simplememoryreferee_4af7316d_(bus_08f8f23c_, bus_4282af84_, bus_446c2389_, bus_2710cc68_, bus_3bb4e83e_, bus_7bab9224_, bus_672e6114_, bus_14e14b04_, bus_0ce81c34_, bus_2fb224d4_, bus_166e4518_, bus_77055d8f_, bus_78e96352_, bus_090e5ab7_, bus_2d041b31_, bus_7776c9ba_, bus_0c88b159_, bus_769480aa_, bus_537b4701_, bus_052201e7_, bus_4bd27031_);
input		bus_08f8f23c_;
input		bus_4282af84_;
input		bus_446c2389_;
input	[31:0]	bus_2710cc68_;
input		bus_3bb4e83e_;
input	[31:0]	bus_7bab9224_;
input	[31:0]	bus_672e6114_;
input	[2:0]	bus_14e14b04_;
input		bus_0ce81c34_;
input		bus_2fb224d4_;
input	[31:0]	bus_166e4518_;
input	[31:0]	bus_77055d8f_;
input	[2:0]	bus_78e96352_;
output	[31:0]	bus_090e5ab7_;
output	[31:0]	bus_2d041b31_;
output		bus_7776c9ba_;
output		bus_0c88b159_;
output	[2:0]	bus_769480aa_;
output		bus_537b4701_;
output	[31:0]	bus_052201e7_;
output		bus_4bd27031_;
wire		and_2e5ee91e_u0;
wire		or_2cbcb9bd_u0;
wire		and_40012a03_u0;
wire		not_7bc8f80f_u0;
wire	[31:0]	mux_447a3bf2_u0;
wire	[31:0]	mux_577675f8_u0;
reg		done_qual_u242=1'h0;
wire		or_0dce0ffd_u0;
reg		done_qual_u243_u0=1'h0;
wire		or_2b7c422a_u0;
wire		or_19287495_u0;
wire		or_5050296d_u0;
wire		not_285ec241_u0;
assign bus_090e5ab7_=mux_577675f8_u0;
assign bus_2d041b31_=mux_447a3bf2_u0;
assign bus_7776c9ba_=or_2cbcb9bd_u0;
assign bus_0c88b159_=or_2b7c422a_u0;
assign bus_769480aa_=3'h1;
assign bus_537b4701_=and_2e5ee91e_u0;
assign bus_052201e7_=bus_2710cc68_;
assign bus_4bd27031_=and_40012a03_u0;
assign and_2e5ee91e_u0=or_0dce0ffd_u0&bus_446c2389_;
assign or_2cbcb9bd_u0=bus_3bb4e83e_|bus_2fb224d4_;
assign and_40012a03_u0=or_5050296d_u0&bus_446c2389_;
assign not_7bc8f80f_u0=~bus_446c2389_;
assign mux_447a3bf2_u0=(bus_3bb4e83e_)?bus_672e6114_:bus_77055d8f_;
assign mux_577675f8_u0=(bus_3bb4e83e_)?{24'b0, bus_7bab9224_[7:0]}:bus_166e4518_;
always @(posedge bus_08f8f23c_)
begin
if (bus_4282af84_)
done_qual_u242<=1'h0;
else done_qual_u242<=bus_3bb4e83e_;
end
assign or_0dce0ffd_u0=bus_3bb4e83e_|done_qual_u242;
always @(posedge bus_08f8f23c_)
begin
if (bus_4282af84_)
done_qual_u243_u0<=1'h0;
else done_qual_u243_u0<=or_19287495_u0;
end
assign or_2b7c422a_u0=bus_3bb4e83e_|or_19287495_u0;
assign or_19287495_u0=bus_0ce81c34_|bus_2fb224d4_;
assign or_5050296d_u0=or_19287495_u0|done_qual_u243_u0;
assign not_285ec241_u0=~bus_446c2389_;
endmodule



module medianRow5_endianswapper_6b0a156b_(endianswapper_6b0a156b_in, endianswapper_6b0a156b_out);
input	[31:0]	endianswapper_6b0a156b_in;
output	[31:0]	endianswapper_6b0a156b_out;
assign endianswapper_6b0a156b_out=endianswapper_6b0a156b_in;
endmodule



module medianRow5_endianswapper_2c83440c_(endianswapper_2c83440c_in, endianswapper_2c83440c_out);
input	[31:0]	endianswapper_2c83440c_in;
output	[31:0]	endianswapper_2c83440c_out;
assign endianswapper_2c83440c_out=endianswapper_2c83440c_in;
endmodule



module medianRow5_stateVar_i(bus_7a3c2848_, bus_22d2adb2_, bus_51f468ba_, bus_7d99ef69_, bus_16074736_, bus_6522ea36_, bus_658e3100_);
input		bus_7a3c2848_;
input		bus_22d2adb2_;
input		bus_51f468ba_;
input	[31:0]	bus_7d99ef69_;
input		bus_16074736_;
input	[31:0]	bus_6522ea36_;
output	[31:0]	bus_658e3100_;
wire	[31:0]	endianswapper_6b0a156b_out;
reg	[31:0]	stateVar_i_u59=32'h0;
wire		or_3794eb5c_u0;
wire	[31:0]	mux_2ea3383c_u0;
wire	[31:0]	endianswapper_2c83440c_out;
medianRow5_endianswapper_6b0a156b_ medianRow5_endianswapper_6b0a156b__1(.endianswapper_6b0a156b_in(mux_2ea3383c_u0), 
  .endianswapper_6b0a156b_out(endianswapper_6b0a156b_out));
always @(posedge bus_7a3c2848_ or posedge bus_22d2adb2_)
begin
if (bus_22d2adb2_)
stateVar_i_u59<=32'h0;
else if (or_3794eb5c_u0)
stateVar_i_u59<=endianswapper_6b0a156b_out;
end
assign or_3794eb5c_u0=bus_51f468ba_|bus_16074736_;
assign bus_658e3100_=endianswapper_2c83440c_out;
assign mux_2ea3383c_u0=(bus_51f468ba_)?bus_7d99ef69_:32'h0;
medianRow5_endianswapper_2c83440c_ medianRow5_endianswapper_2c83440c__1(.endianswapper_2c83440c_in(stateVar_i_u59), 
  .endianswapper_2c83440c_out(endianswapper_2c83440c_out));
endmodule



module medianRow5_globalreset_physical_6819a945_(bus_22407b3d_, bus_6a63b797_, bus_2bfbbdd0_);
input		bus_22407b3d_;
input		bus_6a63b797_;
output		bus_2bfbbdd0_;
reg		cross_u69=1'h0;
reg		final_u69=1'h1;
reg		glitch_u69=1'h0;
wire		and_02bcad07_u0;
wire		not_4cb0109c_u0;
wire		or_1f10a38e_u0;
reg		sample_u69=1'h0;
always @(posedge bus_22407b3d_)
begin
cross_u69<=sample_u69;
end
always @(posedge bus_22407b3d_)
begin
final_u69<=not_4cb0109c_u0;
end
always @(posedge bus_22407b3d_)
begin
glitch_u69<=cross_u69;
end
assign and_02bcad07_u0=cross_u69&glitch_u69;
assign not_4cb0109c_u0=~and_02bcad07_u0;
assign bus_2bfbbdd0_=or_1f10a38e_u0;
assign or_1f10a38e_u0=bus_6a63b797_|final_u69;
always @(posedge bus_22407b3d_)
begin
sample_u69<=1'h1;
end
endmodule



module medianRow5_compute_median(CLK, RESET, GO, port_5d27d6ca_, port_658d936b_, port_3a469087_, port_02bd747b_, port_159e63b8_, RESULT, RESULT_u2481, RESULT_u2482, RESULT_u2483, RESULT_u2484, RESULT_u2485, RESULT_u2486, RESULT_u2487, RESULT_u2488, RESULT_u2489, RESULT_u2490, RESULT_u2491, RESULT_u2492, RESULT_u2493, RESULT_u2494, DONE);
input		CLK;
input		RESET;
input		GO;
input		port_5d27d6ca_;
input	[31:0]	port_658d936b_;
input		port_3a469087_;
input	[31:0]	port_02bd747b_;
input		port_159e63b8_;
output		RESULT;
output	[31:0]	RESULT_u2481;
output		RESULT_u2482;
output	[31:0]	RESULT_u2483;
output	[2:0]	RESULT_u2484;
output		RESULT_u2485;
output	[31:0]	RESULT_u2486;
output	[2:0]	RESULT_u2487;
output		RESULT_u2488;
output	[31:0]	RESULT_u2489;
output	[31:0]	RESULT_u2490;
output	[2:0]	RESULT_u2491;
output	[7:0]	RESULT_u2492;
output		RESULT_u2493;
output	[15:0]	RESULT_u2494;
output		DONE;
wire		and_u4245_u0;
reg	[31:0]	syncEnable_u1650=32'h0;
wire	[31:0]	add;
wire		and_u4246_u0;
wire	[31:0]	add_u794;
wire	[31:0]	add_u795;
wire		and_u4247_u0;
wire		greaterThan;
wire signed	[31:0]	greaterThan_a_signed;
wire signed	[31:0]	greaterThan_b_signed;
wire		not_u981_u0;
wire		and_u4248_u0;
wire		and_u4249_u0;
wire	[31:0]	add_u796;
wire		and_u4250_u0;
wire	[31:0]	add_u797;
wire	[31:0]	add_u798;
wire		and_u4251_u0;
wire	[31:0]	add_u799;
wire		or_u1643_u0;
wire		and_u4252_u0;
reg		reg_784e6c62_u0=1'h0;
wire	[31:0]	add_u800;
wire	[31:0]	add_u801;
wire		or_u1644_u0;
reg		reg_79e89f59_u0=1'h0;
wire		and_u4253_u0;
reg	[31:0]	syncEnable_u1651_u0=32'h0;
reg	[31:0]	syncEnable_u1652_u0=32'h0;
wire	[31:0]	mux_u2165;
wire	[31:0]	mux_u2166_u0;
wire		or_u1645_u0;
reg	[31:0]	syncEnable_u1653_u0=32'h0;
reg	[31:0]	syncEnable_u1654_u0=32'h0;
reg		reg_4e7a4f13_u0=1'h0;
reg		block_GO_delayed_u120=1'h0;
reg	[31:0]	syncEnable_u1655_u0=32'h0;
reg	[31:0]	syncEnable_u1656_u0=32'h0;
reg		reg_12db2e1b_u0=1'h0;
reg		reg_2e92a9f3_u0=1'h0;
reg		reg_2e92a9f3_result_delayed_u0=1'h0;
reg		reg_1c463e87_u0=1'h0;
reg		syncEnable_u1657_u0=1'h0;
reg	[31:0]	syncEnable_u1658_u0=32'h0;
wire	[31:0]	mux_u2167_u0;
wire	[31:0]	mux_u2168_u0;
reg		reg_12db2e1b_result_delayed_u0=1'h0;
wire		and_u4254_u0;
wire		or_u1646_u0;
wire		and_u4255_u0;
reg	[31:0]	syncEnable_u1659_u0=32'h0;
wire		mux_u2169_u0;
wire	[31:0]	add_u802;
reg	[31:0]	syncEnable_u1660_u0=32'h0;
reg	[31:0]	syncEnable_u1661_u0=32'h0;
reg		block_GO_delayed_u121_u0=1'h0;
reg		syncEnable_u1662_u0=1'h0;
reg	[31:0]	syncEnable_u1663_u0=32'h0;
wire	[31:0]	mux_u2170_u0;
wire		or_u1647_u0;
reg	[31:0]	syncEnable_u1664_u0=32'h0;
reg	[31:0]	syncEnable_u1665_u0=32'h0;
reg	[31:0]	syncEnable_u1666_u0=32'h0;
wire	[31:0]	mux_u2171_u0;
wire		or_u1648_u0;
wire		and_u4256_u0;
reg		syncEnable_u1667_u0=1'h0;
wire signed	[32:0]	lessThan_b_signed;
wire		lessThan;
wire signed	[32:0]	lessThan_a_signed;
wire		not_u982_u0;
wire		and_u4257_u0;
wire		and_u4258_u0;
wire	[31:0]	mux_u2172_u0;
wire	[31:0]	mux_u2173_u0;
wire	[31:0]	mux_u2174_u0;
wire		mux_u2175_u0;
wire		or_u1649_u0;
wire	[31:0]	mux_u2176_u0;
wire	[31:0]	mux_u2177_u0;
wire	[15:0]	add_u803;
reg		latch_6144a123_reg=1'h0;
wire		latch_6144a123_out;
reg		scoreboard_0bb08319_reg1=1'h0;
wire		scoreboard_0bb08319_resOr0;
wire		scoreboard_0bb08319_and;
wire		bus_3a7cd72e_;
reg		scoreboard_0bb08319_reg0=1'h0;
wire		scoreboard_0bb08319_resOr1;
reg	[15:0]	syncEnable_u1668_u0=16'h0;
wire	[31:0]	latch_312abb9a_out;
reg	[31:0]	latch_312abb9a_reg=32'h0;
wire	[31:0]	latch_4cd5f9e2_out;
reg	[31:0]	latch_4cd5f9e2_reg=32'h0;
reg		reg_060d60f5_u0=1'h0;
reg	[31:0]	latch_09b61f6f_reg=32'h0;
wire	[31:0]	latch_09b61f6f_out;
reg	[31:0]	latch_51729c0f_reg=32'h0;
wire	[31:0]	latch_51729c0f_out;
wire		and_u4259_u0;
wire	[15:0]	lessThan_u115_b_unsigned;
wire	[15:0]	lessThan_u115_a_unsigned;
wire		lessThan_u115;
wire		andOp;
wire		not_u983_u0;
wire		and_u4260_u0;
wire		and_u4261_u0;
reg		loopControl_u118=1'h0;
reg	[31:0]	fbReg_tmp_row0_u57=32'h0;
reg		fbReg_swapped_u57=1'h0;
reg	[15:0]	fbReg_idx_u57=16'h0;
wire	[15:0]	mux_u2178_u0;
wire	[31:0]	mux_u2179_u0;
reg		syncEnable_u1669_u0=1'h0;
wire		or_u1650_u0;
wire	[31:0]	mux_u2180_u0;
reg	[31:0]	fbReg_tmp_u57=32'h0;
reg	[31:0]	fbReg_tmp_row_u57=32'h0;
reg	[31:0]	fbReg_tmp_row1_u57=32'h0;
wire	[31:0]	mux_u2181_u0;
wire	[31:0]	mux_u2182_u0;
wire		mux_u2183_u0;
wire		and_u4262_u0;
wire	[7:0]	simplePinWrite;
wire		simplePinWrite_u666;
wire	[15:0]	simplePinWrite_u667;
reg		reg_142ad3eb_u0=1'h0;
reg		reg_142ad3eb_result_delayed_u0=1'h0;
wire		or_u1651_u0;
wire	[31:0]	mux_u2184_u0;
assign and_u4245_u0=and_u4260_u0&or_u1650_u0;
always @(posedge CLK)
begin
if (or_u1649_u0)
syncEnable_u1650<=mux_u2177_u0;
end
assign add=mux_u2172_u0+32'h0;
assign and_u4246_u0=and_u4256_u0&port_5d27d6ca_;
assign add_u794=mux_u2172_u0+32'h1;
assign add_u795=add_u794+32'h0;
assign and_u4247_u0=and_u4256_u0&port_159e63b8_;
assign greaterThan_a_signed=syncEnable_u1660_u0;
assign greaterThan_b_signed=syncEnable_u1661_u0;
assign greaterThan=greaterThan_a_signed>greaterThan_b_signed;
assign not_u981_u0=~greaterThan;
assign and_u4248_u0=block_GO_delayed_u121_u0&not_u981_u0;
assign and_u4249_u0=block_GO_delayed_u121_u0&greaterThan;
assign add_u796=syncEnable_u1664_u0+32'h0;
assign and_u4250_u0=and_u4255_u0&port_5d27d6ca_;
assign add_u797=syncEnable_u1664_u0+32'h1;
assign add_u798=add_u797+32'h0;
assign and_u4251_u0=and_u4255_u0&port_159e63b8_;
assign add_u799=syncEnable_u1664_u0+32'h0;
assign or_u1643_u0=and_u4252_u0|RESET;
assign and_u4252_u0=reg_784e6c62_u0&port_159e63b8_;
always @(posedge CLK or posedge block_GO_delayed_u120 or posedge or_u1643_u0)
begin
if (or_u1643_u0)
reg_784e6c62_u0<=1'h0;
else if (block_GO_delayed_u120)
reg_784e6c62_u0<=1'h1;
else reg_784e6c62_u0<=reg_784e6c62_u0;
end
assign add_u800=syncEnable_u1664_u0+32'h1;
assign add_u801=add_u800+32'h0;
assign or_u1644_u0=and_u4253_u0|RESET;
always @(posedge CLK or posedge reg_4e7a4f13_u0 or posedge or_u1644_u0)
begin
if (or_u1644_u0)
reg_79e89f59_u0<=1'h0;
else if (reg_4e7a4f13_u0)
reg_79e89f59_u0<=1'h1;
else reg_79e89f59_u0<=reg_79e89f59_u0;
end
assign and_u4253_u0=reg_79e89f59_u0&port_159e63b8_;
always @(posedge CLK)
begin
if (and_u4255_u0)
syncEnable_u1651_u0<=port_02bd747b_;
end
always @(posedge CLK)
begin
if (and_u4255_u0)
syncEnable_u1652_u0<=add_u801;
end
assign mux_u2165=(block_GO_delayed_u120)?syncEnable_u1651_u0:syncEnable_u1655_u0;
assign mux_u2166_u0=({32{block_GO_delayed_u120}}&syncEnable_u1654_u0)|({32{reg_4e7a4f13_u0}}&syncEnable_u1652_u0)|({32{and_u4255_u0}}&add_u798);
assign or_u1645_u0=block_GO_delayed_u120|reg_4e7a4f13_u0;
always @(posedge CLK)
begin
if (and_u4255_u0)
syncEnable_u1653_u0<=port_658d936b_;
end
always @(posedge CLK)
begin
if (and_u4255_u0)
syncEnable_u1654_u0<=add_u799;
end
always @(posedge CLK or posedge RESET)
begin
if (RESET)
reg_4e7a4f13_u0<=1'h0;
else reg_4e7a4f13_u0<=block_GO_delayed_u120;
end
always @(posedge CLK or posedge RESET)
begin
if (RESET)
block_GO_delayed_u120<=1'h0;
else block_GO_delayed_u120<=and_u4255_u0;
end
always @(posedge CLK)
begin
if (and_u4255_u0)
syncEnable_u1655_u0<=port_658d936b_;
end
always @(posedge CLK)
begin
if (and_u4255_u0)
syncEnable_u1656_u0<=port_02bd747b_;
end
always @(posedge CLK or posedge RESET)
begin
if (RESET)
reg_12db2e1b_u0<=1'h0;
else reg_12db2e1b_u0<=and_u4255_u0;
end
always @(posedge CLK or posedge RESET)
begin
if (RESET)
reg_2e92a9f3_u0<=1'h0;
else reg_2e92a9f3_u0<=reg_12db2e1b_result_delayed_u0;
end
always @(posedge CLK or posedge RESET)
begin
if (RESET)
reg_2e92a9f3_result_delayed_u0<=1'h0;
else reg_2e92a9f3_result_delayed_u0<=reg_2e92a9f3_u0;
end
always @(posedge CLK or posedge RESET)
begin
if (RESET)
reg_1c463e87_u0<=1'h0;
else reg_1c463e87_u0<=and_u4254_u0;
end
always @(posedge CLK)
begin
if (block_GO_delayed_u121_u0)
syncEnable_u1657_u0<=syncEnable_u1662_u0;
end
always @(posedge CLK)
begin
if (block_GO_delayed_u121_u0)
syncEnable_u1658_u0<=syncEnable_u1666_u0;
end
assign mux_u2167_u0=(reg_2e92a9f3_result_delayed_u0)?syncEnable_u1656_u0:syncEnable_u1658_u0;
assign mux_u2168_u0=(reg_2e92a9f3_result_delayed_u0)?syncEnable_u1653_u0:syncEnable_u1659_u0;
always @(posedge CLK or posedge RESET)
begin
if (RESET)
reg_12db2e1b_result_delayed_u0<=1'h0;
else reg_12db2e1b_result_delayed_u0<=reg_12db2e1b_u0;
end
assign and_u4254_u0=and_u4248_u0&block_GO_delayed_u121_u0;
assign or_u1646_u0=reg_2e92a9f3_result_delayed_u0|reg_1c463e87_u0;
assign and_u4255_u0=and_u4249_u0&block_GO_delayed_u121_u0;
always @(posedge CLK)
begin
if (block_GO_delayed_u121_u0)
syncEnable_u1659_u0<=syncEnable_u1665_u0;
end
assign mux_u2169_u0=(reg_2e92a9f3_result_delayed_u0)?1'h1:syncEnable_u1657_u0;
assign add_u802=mux_u2172_u0+32'h1;
always @(posedge CLK)
begin
if (and_u4256_u0)
syncEnable_u1660_u0<=port_658d936b_;
end
always @(posedge CLK)
begin
if (and_u4256_u0)
syncEnable_u1661_u0<=port_02bd747b_;
end
always @(posedge CLK or posedge RESET)
begin
if (RESET)
block_GO_delayed_u121_u0<=1'h0;
else block_GO_delayed_u121_u0<=and_u4256_u0;
end
always @(posedge CLK)
begin
if (and_u4256_u0)
syncEnable_u1662_u0<=mux_u2175_u0;
end
always @(posedge CLK)
begin
if (and_u4256_u0)
syncEnable_u1663_u0<=add_u802;
end
assign mux_u2170_u0=(and_u4256_u0)?add:add_u796;
assign or_u1647_u0=and_u4256_u0|and_u4255_u0;
always @(posedge CLK)
begin
if (and_u4256_u0)
syncEnable_u1664_u0<=mux_u2172_u0;
end
always @(posedge CLK)
begin
if (and_u4256_u0)
syncEnable_u1665_u0<=mux_u2174_u0;
end
always @(posedge CLK)
begin
if (and_u4256_u0)
syncEnable_u1666_u0<=mux_u2173_u0;
end
assign mux_u2171_u0=({32{or_u1645_u0}}&mux_u2166_u0)|({32{and_u4256_u0}}&add_u795)|({32{and_u4255_u0}}&mux_u2166_u0);
assign or_u1648_u0=and_u4256_u0|and_u4255_u0;
assign and_u4256_u0=and_u4257_u0&or_u1649_u0;
always @(posedge CLK)
begin
if (or_u1649_u0)
syncEnable_u1667_u0<=mux_u2175_u0;
end
assign lessThan_a_signed={1'b0, mux_u2172_u0};
assign lessThan_b_signed=33'h64;
assign lessThan=lessThan_a_signed<lessThan_b_signed;
assign not_u982_u0=~lessThan;
assign and_u4257_u0=or_u1649_u0&lessThan;
assign and_u4258_u0=or_u1649_u0&not_u982_u0;
assign mux_u2172_u0=(or_u1646_u0)?syncEnable_u1663_u0:32'h0;
assign mux_u2173_u0=(or_u1646_u0)?mux_u2167_u0:mux_u2180_u0;
assign mux_u2174_u0=(or_u1646_u0)?mux_u2168_u0:mux_u2182_u0;
assign mux_u2175_u0=(or_u1646_u0)?mux_u2169_u0:1'h0;
assign or_u1649_u0=or_u1646_u0|and_u4245_u0;
assign mux_u2176_u0=(or_u1646_u0)?syncEnable_u1660_u0:mux_u2181_u0;
assign mux_u2177_u0=(or_u1646_u0)?syncEnable_u1661_u0:mux_u2179_u0;
assign add_u803=mux_u2178_u0+16'h1;
always @(posedge CLK)
begin
if (or_u1646_u0)
latch_6144a123_reg<=syncEnable_u1667_u0;
end
assign latch_6144a123_out=(or_u1646_u0)?syncEnable_u1667_u0:latch_6144a123_reg;
always @(posedge CLK)
begin
if (bus_3a7cd72e_)
scoreboard_0bb08319_reg1<=1'h0;
else if (or_u1646_u0)
scoreboard_0bb08319_reg1<=1'h1;
else scoreboard_0bb08319_reg1<=scoreboard_0bb08319_reg1;
end
assign scoreboard_0bb08319_resOr0=reg_060d60f5_u0|scoreboard_0bb08319_reg0;
assign scoreboard_0bb08319_and=scoreboard_0bb08319_resOr0&scoreboard_0bb08319_resOr1;
assign bus_3a7cd72e_=scoreboard_0bb08319_and|RESET;
always @(posedge CLK)
begin
if (bus_3a7cd72e_)
scoreboard_0bb08319_reg0<=1'h0;
else if (reg_060d60f5_u0)
scoreboard_0bb08319_reg0<=1'h1;
else scoreboard_0bb08319_reg0<=scoreboard_0bb08319_reg0;
end
assign scoreboard_0bb08319_resOr1=or_u1646_u0|scoreboard_0bb08319_reg1;
always @(posedge CLK)
begin
if (and_u4245_u0)
syncEnable_u1668_u0<=add_u803;
end
assign latch_312abb9a_out=(or_u1646_u0)?mux_u2168_u0:latch_312abb9a_reg;
always @(posedge CLK)
begin
if (or_u1646_u0)
latch_312abb9a_reg<=mux_u2168_u0;
end
assign latch_4cd5f9e2_out=(or_u1646_u0)?syncEnable_u1660_u0:latch_4cd5f9e2_reg;
always @(posedge CLK)
begin
if (or_u1646_u0)
latch_4cd5f9e2_reg<=syncEnable_u1660_u0;
end
always @(posedge CLK or posedge RESET)
begin
if (RESET)
reg_060d60f5_u0<=1'h0;
else reg_060d60f5_u0<=or_u1646_u0;
end
always @(posedge CLK)
begin
if (or_u1646_u0)
latch_09b61f6f_reg<=mux_u2167_u0;
end
assign latch_09b61f6f_out=(or_u1646_u0)?mux_u2167_u0:latch_09b61f6f_reg;
always @(posedge CLK)
begin
if (or_u1646_u0)
latch_51729c0f_reg<=syncEnable_u1650;
end
assign latch_51729c0f_out=(or_u1646_u0)?syncEnable_u1650:latch_51729c0f_reg;
assign and_u4259_u0=and_u4261_u0&or_u1650_u0;
assign lessThan_u115_a_unsigned=mux_u2178_u0;
assign lessThan_u115_b_unsigned=16'h65;
assign lessThan_u115=lessThan_u115_a_unsigned<lessThan_u115_b_unsigned;
assign andOp=lessThan_u115&mux_u2183_u0;
assign not_u983_u0=~andOp;
assign and_u4260_u0=or_u1650_u0&andOp;
assign and_u4261_u0=or_u1650_u0&not_u983_u0;
always @(posedge CLK or posedge syncEnable_u1669_u0)
begin
if (syncEnable_u1669_u0)
loopControl_u118<=1'h0;
else loopControl_u118<=scoreboard_0bb08319_and;
end
always @(posedge CLK)
begin
if (scoreboard_0bb08319_and)
fbReg_tmp_row0_u57<=latch_51729c0f_out;
end
always @(posedge CLK)
begin
if (scoreboard_0bb08319_and)
fbReg_swapped_u57<=latch_6144a123_out;
end
always @(posedge CLK)
begin
if (scoreboard_0bb08319_and)
fbReg_idx_u57<=syncEnable_u1668_u0;
end
assign mux_u2178_u0=(loopControl_u118)?fbReg_idx_u57:16'h0;
assign mux_u2179_u0=(loopControl_u118)?fbReg_tmp_row0_u57:32'h0;
always @(posedge CLK)
begin
if (GO)
syncEnable_u1669_u0<=RESET;
end
assign or_u1650_u0=loopControl_u118|GO;
assign mux_u2180_u0=(loopControl_u118)?fbReg_tmp_row1_u57:32'h0;
always @(posedge CLK)
begin
if (scoreboard_0bb08319_and)
fbReg_tmp_u57<=latch_312abb9a_out;
end
always @(posedge CLK)
begin
if (scoreboard_0bb08319_and)
fbReg_tmp_row_u57<=latch_4cd5f9e2_out;
end
always @(posedge CLK)
begin
if (scoreboard_0bb08319_and)
fbReg_tmp_row1_u57<=latch_09b61f6f_out;
end
assign mux_u2181_u0=(loopControl_u118)?fbReg_tmp_row_u57:32'h0;
assign mux_u2182_u0=(loopControl_u118)?fbReg_tmp_u57:32'h0;
assign mux_u2183_u0=(loopControl_u118)?fbReg_swapped_u57:1'h0;
assign and_u4262_u0=reg_142ad3eb_u0&port_5d27d6ca_;
assign simplePinWrite=port_658d936b_[7:0];
assign simplePinWrite_u666=reg_142ad3eb_u0&{1{reg_142ad3eb_u0}};
assign simplePinWrite_u667=16'h1&{16{1'h1}};
always @(posedge CLK or posedge RESET)
begin
if (RESET)
reg_142ad3eb_u0<=1'h0;
else reg_142ad3eb_u0<=and_u4259_u0;
end
always @(posedge CLK or posedge RESET)
begin
if (RESET)
reg_142ad3eb_result_delayed_u0<=1'h0;
else reg_142ad3eb_result_delayed_u0<=reg_142ad3eb_u0;
end
assign or_u1651_u0=or_u1647_u0|reg_142ad3eb_u0;
assign mux_u2184_u0=(or_u1647_u0)?mux_u2170_u0:32'h32;
assign RESULT=GO;
assign RESULT_u2481=32'h0;
assign RESULT_u2482=or_u1651_u0;
assign RESULT_u2483=mux_u2184_u0;
assign RESULT_u2484=3'h1;
assign RESULT_u2485=or_u1648_u0;
assign RESULT_u2486=mux_u2171_u0;
assign RESULT_u2487=3'h1;
assign RESULT_u2488=or_u1645_u0;
assign RESULT_u2489=mux_u2171_u0;
assign RESULT_u2490=mux_u2165;
assign RESULT_u2491=3'h1;
assign RESULT_u2492=simplePinWrite;
assign RESULT_u2493=simplePinWrite_u666;
assign RESULT_u2494=simplePinWrite_u667;
assign DONE=reg_142ad3eb_result_delayed_u0;
endmodule



module medianRow5_Kicker_69(CLK, RESET, bus_5b45de31_);
input		CLK;
input		RESET;
output		bus_5b45de31_;
wire		bus_48b37f3d_;
wire		bus_2455cb61_;
reg		kicker_res=1'h0;
reg		kicker_1=1'h0;
wire		bus_0c38ce0c_;
reg		kicker_2=1'h0;
wire		bus_66d9c4ff_;
assign bus_48b37f3d_=~RESET;
assign bus_2455cb61_=~kicker_2;
always @(posedge CLK)
begin
kicker_res<=bus_66d9c4ff_;
end
always @(posedge CLK)
begin
kicker_1<=bus_48b37f3d_;
end
assign bus_5b45de31_=kicker_res;
assign bus_0c38ce0c_=bus_48b37f3d_&kicker_1;
always @(posedge CLK)
begin
kicker_2<=bus_0c38ce0c_;
end
assign bus_66d9c4ff_=kicker_1&bus_48b37f3d_&bus_2455cb61_;
endmodule



module medianRow5_scheduler(CLK, RESET, GO, port_736d77c2_, port_62033f75_, port_33cf4f1c_, port_44d7133e_, port_4cb63261_, port_5074841c_, RESULT, RESULT_u2495, RESULT_u2496, RESULT_u2497, DONE);
input		CLK;
input		RESET;
input		GO;
input		port_736d77c2_;
input	[31:0]	port_62033f75_;
input		port_33cf4f1c_;
input		port_44d7133e_;
input		port_4cb63261_;
input		port_5074841c_;
output		RESULT;
output		RESULT_u2495;
output		RESULT_u2496;
output		RESULT_u2497;
output		DONE;
wire		and_u4263_u0;
wire signed	[31:0]	lessThan_a_signed;
wire signed	[31:0]	lessThan_b_signed;
wire		lessThan;
wire signed	[31:0]	equals_a_signed;
wire		equals;
wire signed	[31:0]	equals_b_signed;
wire signed	[31:0]	equals_u256_b_signed;
wire signed	[31:0]	equals_u256_a_signed;
wire		equals_u256;
wire		not_u984_u0;
wire		and_u4264_u0;
wire		and_u4265_u0;
wire		andOp;
wire		not_u985_u0;
wire		and_u4266_u0;
wire		and_u4267_u0;
wire		simplePinWrite;
wire		and_u4268_u0;
wire		and_u4269_u0;
wire		equals_u257;
wire signed	[31:0]	equals_u257_a_signed;
wire signed	[31:0]	equals_u257_b_signed;
wire		and_u4270_u0;
wire		not_u986_u0;
wire		and_u4271_u0;
wire		andOp_u91;
wire		and_u4272_u0;
wire		not_u987_u0;
wire		and_u4273_u0;
wire		simplePinWrite_u668;
wire		and_u4274_u0;
wire		and_u4275_u0;
wire		not_u988_u0;
wire		not_u989_u0;
wire		and_u4276_u0;
wire		and_u4277_u0;
wire		simplePinWrite_u669;
wire		and_u4278_u0;
reg		reg_6160d254_u0=1'h0;
wire		or_u1652_u0;
wire		and_u4279_u0;
reg		reg_594aaeba_u0=1'h0;
wire		or_u1653_u0;
wire		and_u4280_u0;
wire		and_u4281_u0;
wire		or_u1654_u0;
wire		mux_u2185;
wire		and_u4282_u0;
reg		reg_52526c7c_u0=1'h0;
wire		or_u1655_u0;
wire		and_u4283_u0;
reg		reg_2f2f761f_u0=1'h0;
wire		and_u4284_u0;
wire		or_u1656_u0;
wire		and_u4285_u0;
wire		receive_go_merge;
wire		or_u1657_u0;
wire		mux_u2186_u0;
reg		syncEnable_u1670=1'h0;
wire		or_u1658_u0;
reg		loopControl_u119=1'h0;
reg		reg_1194f0f3_u0=1'h0;
wire		or_u1659_u0;
wire		mux_u2187_u0;
reg		reg_1194f0f3_result_delayed_u0=1'h0;
assign and_u4263_u0=or_u1658_u0&or_u1658_u0;
assign lessThan_a_signed=port_62033f75_;
assign lessThan_b_signed=32'h65;
assign lessThan=lessThan_a_signed<lessThan_b_signed;
assign equals_a_signed=port_62033f75_;
assign equals_b_signed=32'h65;
assign equals=equals_a_signed==equals_b_signed;
assign equals_u256_a_signed={31'b0, port_736d77c2_};
assign equals_u256_b_signed=32'h0;
assign equals_u256=equals_u256_a_signed==equals_u256_b_signed;
assign not_u984_u0=~equals_u256;
assign and_u4264_u0=and_u4263_u0&equals_u256;
assign and_u4265_u0=and_u4263_u0&not_u984_u0;
assign andOp=lessThan&port_4cb63261_;
assign not_u985_u0=~andOp;
assign and_u4266_u0=and_u4269_u0&not_u985_u0;
assign and_u4267_u0=and_u4269_u0&andOp;
assign simplePinWrite=and_u4268_u0&{1{and_u4268_u0}};
assign and_u4268_u0=and_u4267_u0&and_u4269_u0;
assign and_u4269_u0=and_u4264_u0&and_u4263_u0;
assign equals_u257_a_signed={31'b0, port_736d77c2_};
assign equals_u257_b_signed=32'h1;
assign equals_u257=equals_u257_a_signed==equals_u257_b_signed;
assign and_u4270_u0=and_u4263_u0&equals_u257;
assign not_u986_u0=~equals_u257;
assign and_u4271_u0=and_u4263_u0&not_u986_u0;
assign andOp_u91=lessThan&port_4cb63261_;
assign and_u4272_u0=and_u4285_u0&andOp_u91;
assign not_u987_u0=~andOp_u91;
assign and_u4273_u0=and_u4285_u0&not_u987_u0;
assign simplePinWrite_u668=and_u4283_u0&{1{and_u4283_u0}};
assign and_u4274_u0=and_u4282_u0&equals;
assign and_u4275_u0=and_u4282_u0&not_u988_u0;
assign not_u988_u0=~equals;
assign not_u989_u0=~port_5074841c_;
assign and_u4276_u0=and_u4280_u0&not_u989_u0;
assign and_u4277_u0=and_u4280_u0&port_5074841c_;
assign simplePinWrite_u669=and_u4278_u0&{1{and_u4278_u0}};
assign and_u4278_u0=and_u4277_u0&and_u4280_u0;
always @(posedge CLK or posedge RESET)
begin
if (RESET)
reg_6160d254_u0<=1'h0;
else reg_6160d254_u0<=and_u4279_u0;
end
assign or_u1652_u0=port_44d7133e_|reg_6160d254_u0;
assign and_u4279_u0=and_u4276_u0&and_u4280_u0;
always @(posedge CLK or posedge RESET)
begin
if (RESET)
reg_594aaeba_u0<=1'h0;
else reg_594aaeba_u0<=and_u4281_u0;
end
assign or_u1653_u0=or_u1652_u0|reg_594aaeba_u0;
assign and_u4280_u0=and_u4274_u0&and_u4282_u0;
assign and_u4281_u0=and_u4275_u0&and_u4282_u0;
assign or_u1654_u0=and_u4283_u0|and_u4278_u0;
assign mux_u2185=(and_u4283_u0)?1'h1:1'h0;
assign and_u4282_u0=and_u4273_u0&and_u4285_u0;
always @(posedge CLK or posedge RESET)
begin
if (RESET)
reg_52526c7c_u0<=1'h0;
else reg_52526c7c_u0<=and_u4283_u0;
end
assign or_u1655_u0=reg_52526c7c_u0|or_u1653_u0;
assign and_u4283_u0=and_u4272_u0&and_u4285_u0;
always @(posedge CLK or posedge RESET)
begin
if (RESET)
reg_2f2f761f_u0<=1'h0;
else reg_2f2f761f_u0<=and_u4284_u0;
end
assign and_u4284_u0=and_u4271_u0&and_u4263_u0;
assign or_u1656_u0=or_u1655_u0|reg_2f2f761f_u0;
assign and_u4285_u0=and_u4270_u0&and_u4263_u0;
assign receive_go_merge=simplePinWrite|simplePinWrite_u668;
assign or_u1657_u0=and_u4268_u0|or_u1654_u0;
assign mux_u2186_u0=(and_u4268_u0)?1'h1:mux_u2185;
always @(posedge CLK)
begin
if (reg_1194f0f3_result_delayed_u0)
syncEnable_u1670<=RESET;
end
assign or_u1658_u0=reg_1194f0f3_result_delayed_u0|loopControl_u119;
always @(posedge CLK or posedge syncEnable_u1670)
begin
if (syncEnable_u1670)
loopControl_u119<=1'h0;
else loopControl_u119<=or_u1656_u0;
end
always @(posedge CLK or posedge RESET)
begin
if (RESET)
reg_1194f0f3_u0<=1'h0;
else reg_1194f0f3_u0<=GO;
end
assign or_u1659_u0=GO|or_u1657_u0;
assign mux_u2187_u0=(GO)?1'h0:mux_u2186_u0;
always @(posedge CLK or posedge RESET)
begin
if (RESET)
reg_1194f0f3_result_delayed_u0<=1'h0;
else reg_1194f0f3_result_delayed_u0<=reg_1194f0f3_u0;
end
assign RESULT=or_u1659_u0;
assign RESULT_u2495=mux_u2187_u0;
assign RESULT_u2496=simplePinWrite_u669;
assign RESULT_u2497=receive_go_merge;
assign DONE=1'h0;
endmodule



module medianRow5_endianswapper_1492503f_(endianswapper_1492503f_in, endianswapper_1492503f_out);
input		endianswapper_1492503f_in;
output		endianswapper_1492503f_out;
assign endianswapper_1492503f_out=endianswapper_1492503f_in;
endmodule



module medianRow5_endianswapper_14c69d59_(endianswapper_14c69d59_in, endianswapper_14c69d59_out);
input		endianswapper_14c69d59_in;
output		endianswapper_14c69d59_out;
assign endianswapper_14c69d59_out=endianswapper_14c69d59_in;
endmodule



module medianRow5_stateVar_fsmState_medianRow5(bus_612214c2_, bus_2d9f961f_, bus_7f2ba9d5_, bus_5f20665c_, bus_40f7bf8a_);
input		bus_612214c2_;
input		bus_2d9f961f_;
input		bus_7f2ba9d5_;
input		bus_5f20665c_;
output		bus_40f7bf8a_;
reg		stateVar_fsmState_medianRow5_u2=1'h0;
wire		endianswapper_1492503f_out;
wire		endianswapper_14c69d59_out;
always @(posedge bus_612214c2_ or posedge bus_2d9f961f_)
begin
if (bus_2d9f961f_)
stateVar_fsmState_medianRow5_u2<=1'h0;
else if (bus_7f2ba9d5_)
stateVar_fsmState_medianRow5_u2<=endianswapper_14c69d59_out;
end
assign bus_40f7bf8a_=endianswapper_1492503f_out;
medianRow5_endianswapper_1492503f_ medianRow5_endianswapper_1492503f__1(.endianswapper_1492503f_in(stateVar_fsmState_medianRow5_u2), 
  .endianswapper_1492503f_out(endianswapper_1492503f_out));
medianRow5_endianswapper_14c69d59_ medianRow5_endianswapper_14c69d59__1(.endianswapper_14c69d59_in(bus_5f20665c_), 
  .endianswapper_14c69d59_out(endianswapper_14c69d59_out));
endmodule


