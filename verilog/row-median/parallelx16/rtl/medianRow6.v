// __  ___ __ ___  _ __   ___  ___ 
// \ \/ / '__/ _ \| '_ \ / _ \/ __|
//  >  <| | | (_) | | | | (_) \__ \
// /_/\_\_|  \___/|_| |_|\___/|___/
// 
// Xronos synthesizer version
// Run date: Fri 23 Feb 2018 14:28:50 +0000
// 

module medianRow6(median_SEND, in1_ACK, median_COUNT, RESET, in1_SEND, median_ACK, median_DATA, CLK, in1_COUNT, median_RDY, in1_DATA);
output		median_SEND;
output		in1_ACK;
output	[15:0]	median_COUNT;
wire		compute_median_go;
input		RESET;
wire		compute_median_done;
input		in1_SEND;
wire		receive_done;
input		median_ACK;
output	[7:0]	median_DATA;
wire		receive_go;
input		CLK;
input	[15:0]	in1_COUNT;
input		median_RDY;
input	[7:0]	in1_DATA;
wire		bus_0665fdbb_;
wire	[31:0]	bus_23af3dd2_;
wire		bus_52c46619_;
wire	[2:0]	bus_3c874988_;
wire	[31:0]	bus_7c561f1b_;
wire	[31:0]	bus_530167d9_;
wire		bus_2b05c25a_;
wire		bus_4469061b_;
wire		bus_589b175d_;
wire	[31:0]	bus_431baf30_;
wire	[31:0]	receive_u338;
wire	[2:0]	receive_u340;
wire		receive_u337;
wire	[31:0]	receive_u339;
wire		receive_u341;
wire		medianRow6_receive_instance_DONE;
wire	[31:0]	receive_u336;
wire		receive;
wire		bus_6ae35830_;
wire		scheduler;
wire		medianRow6_scheduler_instance_DONE;
wire		scheduler_u846;
wire		scheduler_u845;
wire		scheduler_u844;
wire		bus_16b6fffb_;
wire	[2:0]	bus_1bf8d762_;
wire	[31:0]	bus_219762fa_;
wire		bus_72cb88f4_;
wire		bus_52a0137c_;
wire	[31:0]	bus_5f673851_;
wire		bus_09b45143_;
wire		bus_5028981f_;
wire	[31:0]	bus_139a0327_;
wire	[31:0]	compute_median_u790;
wire	[31:0]	compute_median_u784;
wire		compute_median_u785;
wire		compute_median_u796;
wire	[31:0]	compute_median_u793;
wire	[31:0]	compute_median_u786;
wire	[15:0]	compute_median_u797;
wire		compute_median;
wire	[2:0]	compute_median_u791;
wire	[7:0]	compute_median_u795;
wire	[2:0]	compute_median_u794;
wire		compute_median_u789;
wire	[2:0]	compute_median_u788;
wire		compute_median_u792;
wire	[31:0]	compute_median_u787;
wire		medianRow6_compute_median_instance_DONE;
wire		bus_10e1ac9b_;
wire		bus_19b156a5_;
wire	[31:0]	bus_6cb408ed_;
wire	[31:0]	bus_529ca8ca_;
wire		bus_6f27408e_;
assign median_SEND=compute_median_u796;
assign in1_ACK=receive_u341;
assign median_COUNT=compute_median_u797;
assign compute_median_go=scheduler_u845;
assign compute_median_done=bus_52c46619_;
assign receive_done=bus_6f27408e_;
assign median_DATA=compute_median_u795;
assign receive_go=scheduler_u846;
medianRow6_globalreset_physical_5d77e476_ medianRow6_globalreset_physical_5d77e476__1(.bus_472e7dc8_(CLK), 
  .bus_680228e4_(RESET), .bus_0665fdbb_(bus_0665fdbb_));
medianRow6_stateVar_i medianRow6_stateVar_i_1(.bus_3bee662d_(CLK), .bus_40525a85_(bus_0665fdbb_), 
  .bus_6580dd59_(receive), .bus_7fc3f19f_(receive_u336), .bus_08fb8de6_(compute_median), 
  .bus_7d50985a_(32'h0), .bus_23af3dd2_(bus_23af3dd2_));
assign bus_52c46619_=medianRow6_compute_median_instance_DONE&{1{medianRow6_compute_median_instance_DONE}};
medianRow6_simplememoryreferee_7a196f17_ medianRow6_simplememoryreferee_7a196f17__1(.bus_58eb0a80_(CLK), 
  .bus_0967ff2e_(bus_0665fdbb_), .bus_3b6667c4_(bus_19b156a5_), .bus_55d6bdc0_(bus_529ca8ca_), 
  .bus_45d6650b_(compute_median_u792), .bus_3b2acdd4_(compute_median_u793), .bus_764073f3_(3'h1), 
  .bus_530167d9_(bus_530167d9_), .bus_7c561f1b_(bus_7c561f1b_), .bus_4469061b_(bus_4469061b_), 
  .bus_589b175d_(bus_589b175d_), .bus_3c874988_(bus_3c874988_), .bus_431baf30_(bus_431baf30_), 
  .bus_2b05c25a_(bus_2b05c25a_));
medianRow6_receive medianRow6_receive_instance(.CLK(CLK), .RESET(bus_0665fdbb_), 
  .GO(receive_go), .port_1cdf0f83_(bus_23af3dd2_), .port_35699909_(bus_52a0137c_), 
  .port_2b68661e_(in1_DATA), .DONE(medianRow6_receive_instance_DONE), .RESULT(receive), 
  .RESULT_u2452(receive_u336), .RESULT_u2453(receive_u337), .RESULT_u2454(receive_u338), 
  .RESULT_u2455(receive_u339), .RESULT_u2456(receive_u340), .RESULT_u2457(receive_u341));
medianRow6_stateVar_fsmState_medianRow6 medianRow6_stateVar_fsmState_medianRow6_1(.bus_2c1e0c5e_(CLK), 
  .bus_66d326f3_(bus_0665fdbb_), .bus_069ede1d_(scheduler), .bus_25250f76_(scheduler_u844), 
  .bus_6ae35830_(bus_6ae35830_));
medianRow6_scheduler medianRow6_scheduler_instance(.CLK(CLK), .RESET(bus_0665fdbb_), 
  .GO(bus_16b6fffb_), .port_4f509f94_(bus_6ae35830_), .port_003c3ede_(bus_23af3dd2_), 
  .port_492faa19_(compute_median_done), .port_1ae8cf2b_(in1_SEND), .port_70e75c1d_(receive_done), 
  .port_4f0de17c_(median_RDY), .DONE(medianRow6_scheduler_instance_DONE), .RESULT(scheduler), 
  .RESULT_u2458(scheduler_u844), .RESULT_u2459(scheduler_u845), .RESULT_u2460(scheduler_u846));
medianRow6_Kicker_68 medianRow6_Kicker_68_1(.CLK(CLK), .RESET(bus_0665fdbb_), .bus_16b6fffb_(bus_16b6fffb_));
medianRow6_simplememoryreferee_68bebf1e_ medianRow6_simplememoryreferee_68bebf1e__1(.bus_6b3c4517_(CLK), 
  .bus_5e0ba844_(bus_0665fdbb_), .bus_774e4f78_(bus_10e1ac9b_), .bus_3d97b47e_(bus_6cb408ed_), 
  .bus_00aeb7ba_(receive_u337), .bus_1bca6168_({24'b0, receive_u339[7:0]}), .bus_58d015d2_(receive_u338), 
  .bus_4c2d257c_(3'h1), .bus_0d8ef16f_(compute_median_u789), .bus_59e284e8_(compute_median_u785), 
  .bus_2493d875_(compute_median_u787), .bus_222edaa6_(compute_median_u786), .bus_2b87aedc_(3'h1), 
  .bus_219762fa_(bus_219762fa_), .bus_139a0327_(bus_139a0327_), .bus_09b45143_(bus_09b45143_), 
  .bus_5028981f_(bus_5028981f_), .bus_1bf8d762_(bus_1bf8d762_), .bus_52a0137c_(bus_52a0137c_), 
  .bus_5f673851_(bus_5f673851_), .bus_72cb88f4_(bus_72cb88f4_));
medianRow6_compute_median medianRow6_compute_median_instance(.CLK(CLK), .RESET(bus_0665fdbb_), 
  .GO(compute_median_go), .port_7b08cf53_(bus_72cb88f4_), .port_2852be82_(bus_72cb88f4_), 
  .port_7aa5eed2_(bus_5f673851_), .port_7a5339ec_(bus_2b05c25a_), .port_6bb75d82_(bus_431baf30_), 
  .DONE(medianRow6_compute_median_instance_DONE), .RESULT(compute_median), .RESULT_u2461(compute_median_u784), 
  .RESULT_u2468(compute_median_u785), .RESULT_u2469(compute_median_u786), .RESULT_u2470(compute_median_u787), 
  .RESULT_u2471(compute_median_u788), .RESULT_u2465(compute_median_u789), .RESULT_u2466(compute_median_u790), 
  .RESULT_u2467(compute_median_u791), .RESULT_u2462(compute_median_u792), .RESULT_u2463(compute_median_u793), 
  .RESULT_u2464(compute_median_u794), .RESULT_u2472(compute_median_u795), .RESULT_u2473(compute_median_u796), 
  .RESULT_u2474(compute_median_u797));
medianRow6_structuralmemory_313b77d4_ medianRow6_structuralmemory_313b77d4__1(.CLK_u102(CLK), 
  .bus_2e6fef76_(bus_0665fdbb_), .bus_0257f18a_(bus_7c561f1b_), .bus_44993a90_(3'h1), 
  .bus_7557a073_(bus_589b175d_), .bus_177e5519_(bus_139a0327_), .bus_0b59d15f_(3'h1), 
  .bus_55d61223_(bus_5028981f_), .bus_5602bec8_(bus_09b45143_), .bus_24597958_(bus_219762fa_), 
  .bus_529ca8ca_(bus_529ca8ca_), .bus_19b156a5_(bus_19b156a5_), .bus_6cb408ed_(bus_6cb408ed_), 
  .bus_10e1ac9b_(bus_10e1ac9b_));
assign bus_6f27408e_=medianRow6_receive_instance_DONE&{1{medianRow6_receive_instance_DONE}};
endmodule



module medianRow6_globalreset_physical_5d77e476_(bus_472e7dc8_, bus_680228e4_, bus_0665fdbb_);
input		bus_472e7dc8_;
input		bus_680228e4_;
output		bus_0665fdbb_;
wire		and_549265e2_u0;
reg		glitch_u68=1'h0;
reg		cross_u68=1'h0;
wire		not_153fb787_u0;
wire		or_39353859_u0;
reg		sample_u68=1'h0;
reg		final_u68=1'h1;
assign and_549265e2_u0=cross_u68&glitch_u68;
always @(posedge bus_472e7dc8_)
begin
glitch_u68<=cross_u68;
end
always @(posedge bus_472e7dc8_)
begin
cross_u68<=sample_u68;
end
assign not_153fb787_u0=~and_549265e2_u0;
assign bus_0665fdbb_=or_39353859_u0;
assign or_39353859_u0=bus_680228e4_|final_u68;
always @(posedge bus_472e7dc8_)
begin
sample_u68<=1'h1;
end
always @(posedge bus_472e7dc8_)
begin
final_u68<=not_153fb787_u0;
end
endmodule



module medianRow6_endianswapper_43996c7e_(endianswapper_43996c7e_in, endianswapper_43996c7e_out);
input	[31:0]	endianswapper_43996c7e_in;
output	[31:0]	endianswapper_43996c7e_out;
assign endianswapper_43996c7e_out=endianswapper_43996c7e_in;
endmodule



module medianRow6_endianswapper_74204558_(endianswapper_74204558_in, endianswapper_74204558_out);
input	[31:0]	endianswapper_74204558_in;
output	[31:0]	endianswapper_74204558_out;
assign endianswapper_74204558_out=endianswapper_74204558_in;
endmodule



module medianRow6_stateVar_i(bus_3bee662d_, bus_40525a85_, bus_6580dd59_, bus_7fc3f19f_, bus_08fb8de6_, bus_7d50985a_, bus_23af3dd2_);
input		bus_3bee662d_;
input		bus_40525a85_;
input		bus_6580dd59_;
input	[31:0]	bus_7fc3f19f_;
input		bus_08fb8de6_;
input	[31:0]	bus_7d50985a_;
output	[31:0]	bus_23af3dd2_;
reg	[31:0]	stateVar_i_u58=32'h0;
wire	[31:0]	endianswapper_43996c7e_out;
wire		or_4416f4be_u0;
wire	[31:0]	endianswapper_74204558_out;
wire	[31:0]	mux_0fe0615a_u0;
always @(posedge bus_3bee662d_ or posedge bus_40525a85_)
begin
if (bus_40525a85_)
stateVar_i_u58<=32'h0;
else if (or_4416f4be_u0)
stateVar_i_u58<=endianswapper_74204558_out;
end
medianRow6_endianswapper_43996c7e_ medianRow6_endianswapper_43996c7e__1(.endianswapper_43996c7e_in(stateVar_i_u58), 
  .endianswapper_43996c7e_out(endianswapper_43996c7e_out));
assign bus_23af3dd2_=endianswapper_43996c7e_out;
assign or_4416f4be_u0=bus_6580dd59_|bus_08fb8de6_;
medianRow6_endianswapper_74204558_ medianRow6_endianswapper_74204558__1(.endianswapper_74204558_in(mux_0fe0615a_u0), 
  .endianswapper_74204558_out(endianswapper_74204558_out));
assign mux_0fe0615a_u0=(bus_6580dd59_)?bus_7fc3f19f_:32'h0;
endmodule



module medianRow6_simplememoryreferee_7a196f17_(bus_58eb0a80_, bus_0967ff2e_, bus_3b6667c4_, bus_55d6bdc0_, bus_45d6650b_, bus_3b2acdd4_, bus_764073f3_, bus_530167d9_, bus_7c561f1b_, bus_4469061b_, bus_589b175d_, bus_3c874988_, bus_431baf30_, bus_2b05c25a_);
input		bus_58eb0a80_;
input		bus_0967ff2e_;
input		bus_3b6667c4_;
input	[31:0]	bus_55d6bdc0_;
input		bus_45d6650b_;
input	[31:0]	bus_3b2acdd4_;
input	[2:0]	bus_764073f3_;
output	[31:0]	bus_530167d9_;
output	[31:0]	bus_7c561f1b_;
output		bus_4469061b_;
output		bus_589b175d_;
output	[2:0]	bus_3c874988_;
output	[31:0]	bus_431baf30_;
output		bus_2b05c25a_;
assign bus_530167d9_=32'h0;
assign bus_7c561f1b_=bus_3b2acdd4_;
assign bus_4469061b_=1'h0;
assign bus_589b175d_=bus_45d6650b_;
assign bus_3c874988_=3'h1;
assign bus_431baf30_=bus_55d6bdc0_;
assign bus_2b05c25a_=bus_3b6667c4_;
endmodule



module medianRow6_receive(CLK, RESET, GO, port_1cdf0f83_, port_35699909_, port_2b68661e_, RESULT, RESULT_u2452, RESULT_u2453, RESULT_u2454, RESULT_u2455, RESULT_u2456, RESULT_u2457, DONE);
input		CLK;
input		RESET;
input		GO;
input	[31:0]	port_1cdf0f83_;
input		port_35699909_;
input	[7:0]	port_2b68661e_;
output		RESULT;
output	[31:0]	RESULT_u2452;
output		RESULT_u2453;
output	[31:0]	RESULT_u2454;
output	[31:0]	RESULT_u2455;
output	[2:0]	RESULT_u2456;
output		RESULT_u2457;
output		DONE;
wire		simplePinWrite;
wire	[31:0]	add;
wire		and_u4202_u0;
wire		or_u1624_u0;
reg		reg_06ad8604_u0=1'h0;
wire	[31:0]	add_u782;
reg		reg_62a9d789_u0=1'h0;
assign simplePinWrite=GO&{1{GO}};
assign add=port_1cdf0f83_+32'h0;
assign and_u4202_u0=reg_06ad8604_u0&port_35699909_;
assign or_u1624_u0=and_u4202_u0|RESET;
always @(posedge CLK or posedge GO or posedge or_u1624_u0)
begin
if (or_u1624_u0)
reg_06ad8604_u0<=1'h0;
else if (GO)
reg_06ad8604_u0<=1'h1;
else reg_06ad8604_u0<=reg_06ad8604_u0;
end
assign add_u782=port_1cdf0f83_+32'h1;
always @(posedge CLK or posedge RESET)
begin
if (RESET)
reg_62a9d789_u0<=1'h0;
else reg_62a9d789_u0<=GO;
end
assign RESULT=GO;
assign RESULT_u2452=add_u782;
assign RESULT_u2453=GO;
assign RESULT_u2454=add;
assign RESULT_u2455={24'b0, port_2b68661e_};
assign RESULT_u2456=3'h1;
assign RESULT_u2457=simplePinWrite;
assign DONE=reg_62a9d789_u0;
endmodule



module medianRow6_endianswapper_434e265a_(endianswapper_434e265a_in, endianswapper_434e265a_out);
input		endianswapper_434e265a_in;
output		endianswapper_434e265a_out;
assign endianswapper_434e265a_out=endianswapper_434e265a_in;
endmodule



module medianRow6_endianswapper_50862d50_(endianswapper_50862d50_in, endianswapper_50862d50_out);
input		endianswapper_50862d50_in;
output		endianswapper_50862d50_out;
assign endianswapper_50862d50_out=endianswapper_50862d50_in;
endmodule



module medianRow6_stateVar_fsmState_medianRow6(bus_2c1e0c5e_, bus_66d326f3_, bus_069ede1d_, bus_25250f76_, bus_6ae35830_);
input		bus_2c1e0c5e_;
input		bus_66d326f3_;
input		bus_069ede1d_;
input		bus_25250f76_;
output		bus_6ae35830_;
wire		endianswapper_434e265a_out;
wire		endianswapper_50862d50_out;
reg		stateVar_fsmState_medianRow6_u2=1'h0;
medianRow6_endianswapper_434e265a_ medianRow6_endianswapper_434e265a__1(.endianswapper_434e265a_in(bus_25250f76_), 
  .endianswapper_434e265a_out(endianswapper_434e265a_out));
medianRow6_endianswapper_50862d50_ medianRow6_endianswapper_50862d50__1(.endianswapper_50862d50_in(stateVar_fsmState_medianRow6_u2), 
  .endianswapper_50862d50_out(endianswapper_50862d50_out));
always @(posedge bus_2c1e0c5e_ or posedge bus_66d326f3_)
begin
if (bus_66d326f3_)
stateVar_fsmState_medianRow6_u2<=1'h0;
else if (bus_069ede1d_)
stateVar_fsmState_medianRow6_u2<=endianswapper_434e265a_out;
end
assign bus_6ae35830_=endianswapper_50862d50_out;
endmodule



module medianRow6_scheduler(CLK, RESET, GO, port_4f509f94_, port_003c3ede_, port_492faa19_, port_1ae8cf2b_, port_70e75c1d_, port_4f0de17c_, RESULT, RESULT_u2458, RESULT_u2459, RESULT_u2460, DONE);
input		CLK;
input		RESET;
input		GO;
input		port_4f509f94_;
input	[31:0]	port_003c3ede_;
input		port_492faa19_;
input		port_1ae8cf2b_;
input		port_70e75c1d_;
input		port_4f0de17c_;
output		RESULT;
output		RESULT_u2458;
output		RESULT_u2459;
output		RESULT_u2460;
output		DONE;
wire		and_u4203_u0;
wire signed	[31:0]	lessThan_b_signed;
wire signed	[31:0]	lessThan_a_signed;
wire		lessThan;
wire signed	[31:0]	equals_a_signed;
wire		equals;
wire signed	[31:0]	equals_b_signed;
wire signed	[31:0]	equals_u254_a_signed;
wire		equals_u254;
wire signed	[31:0]	equals_u254_b_signed;
wire		not_u972_u0;
wire		and_u4204_u0;
wire		and_u4205_u0;
wire		andOp;
wire		not_u973_u0;
wire		and_u4206_u0;
wire		and_u4207_u0;
wire		simplePinWrite;
wire		and_u4208_u0;
wire		and_u4209_u0;
wire		equals_u255;
wire signed	[31:0]	equals_u255_a_signed;
wire signed	[31:0]	equals_u255_b_signed;
wire		not_u974_u0;
wire		and_u4210_u0;
wire		and_u4211_u0;
wire		andOp_u90;
wire		and_u4212_u0;
wire		and_u4213_u0;
wire		not_u975_u0;
wire		simplePinWrite_u662;
wire		not_u976_u0;
wire		and_u4214_u0;
wire		and_u4215_u0;
wire		and_u4216_u0;
wire		and_u4217_u0;
wire		not_u977_u0;
wire		simplePinWrite_u663;
wire		and_u4218_u0;
reg		and_delayed_u420=1'h0;
wire		and_u4219_u0;
wire		or_u1625_u0;
wire		and_u4220_u0;
reg		and_delayed_u421_u0=1'h0;
wire		and_u4221_u0;
wire		or_u1626_u0;
wire		or_u1627_u0;
wire		and_u4222_u0;
wire		or_u1628_u0;
wire		mux_u2142;
reg		reg_50ae30c0_u0=1'h0;
wire		and_u4223_u0;
reg		reg_6da5cba0_u0=1'h0;
wire		and_u4224_u0;
wire		and_u4225_u0;
wire		or_u1629_u0;
wire		receive_go_merge;
wire		mux_u2143_u0;
wire		or_u1630_u0;
reg		syncEnable_u1628=1'h0;
reg		loopControl_u116=1'h0;
wire		or_u1631_u0;
reg		reg_733e7a39_u0=1'h0;
wire		or_u1632_u0;
wire		mux_u2144_u0;
reg		reg_09ea6fcc_u0=1'h0;
assign and_u4203_u0=or_u1631_u0&or_u1631_u0;
assign lessThan_a_signed=port_003c3ede_;
assign lessThan_b_signed=32'h65;
assign lessThan=lessThan_a_signed<lessThan_b_signed;
assign equals_a_signed=port_003c3ede_;
assign equals_b_signed=32'h65;
assign equals=equals_a_signed==equals_b_signed;
assign equals_u254_a_signed={31'b0, port_4f509f94_};
assign equals_u254_b_signed=32'h0;
assign equals_u254=equals_u254_a_signed==equals_u254_b_signed;
assign not_u972_u0=~equals_u254;
assign and_u4204_u0=and_u4203_u0&not_u972_u0;
assign and_u4205_u0=and_u4203_u0&equals_u254;
assign andOp=lessThan&port_1ae8cf2b_;
assign not_u973_u0=~andOp;
assign and_u4206_u0=and_u4209_u0&andOp;
assign and_u4207_u0=and_u4209_u0&not_u973_u0;
assign simplePinWrite=and_u4208_u0&{1{and_u4208_u0}};
assign and_u4208_u0=and_u4206_u0&and_u4209_u0;
assign and_u4209_u0=and_u4205_u0&and_u4203_u0;
assign equals_u255_a_signed={31'b0, port_4f509f94_};
assign equals_u255_b_signed=32'h1;
assign equals_u255=equals_u255_a_signed==equals_u255_b_signed;
assign not_u974_u0=~equals_u255;
assign and_u4210_u0=and_u4203_u0&not_u974_u0;
assign and_u4211_u0=and_u4203_u0&equals_u255;
assign andOp_u90=lessThan&port_1ae8cf2b_;
assign and_u4212_u0=and_u4225_u0&andOp_u90;
assign and_u4213_u0=and_u4225_u0&not_u975_u0;
assign not_u975_u0=~andOp_u90;
assign simplePinWrite_u662=and_u4223_u0&{1{and_u4223_u0}};
assign not_u976_u0=~equals;
assign and_u4214_u0=and_u4222_u0&equals;
assign and_u4215_u0=and_u4222_u0&not_u976_u0;
assign and_u4216_u0=and_u4221_u0&port_4f0de17c_;
assign and_u4217_u0=and_u4221_u0&not_u977_u0;
assign not_u977_u0=~port_4f0de17c_;
assign simplePinWrite_u663=and_u4219_u0&{1{and_u4219_u0}};
assign and_u4218_u0=and_u4217_u0&and_u4221_u0;
always @(posedge CLK or posedge RESET)
begin
if (RESET)
and_delayed_u420<=1'h0;
else and_delayed_u420<=and_u4218_u0;
end
assign and_u4219_u0=and_u4216_u0&and_u4221_u0;
assign or_u1625_u0=and_delayed_u420|port_492faa19_;
assign and_u4220_u0=and_u4215_u0&and_u4222_u0;
always @(posedge CLK or posedge RESET)
begin
if (RESET)
and_delayed_u421_u0<=1'h0;
else and_delayed_u421_u0<=and_u4220_u0;
end
assign and_u4221_u0=and_u4214_u0&and_u4222_u0;
assign or_u1626_u0=or_u1625_u0|and_delayed_u421_u0;
assign or_u1627_u0=reg_50ae30c0_u0|or_u1626_u0;
assign and_u4222_u0=and_u4213_u0&and_u4225_u0;
assign or_u1628_u0=and_u4223_u0|and_u4219_u0;
assign mux_u2142=(and_u4223_u0)?1'h1:1'h0;
always @(posedge CLK or posedge RESET)
begin
if (RESET)
reg_50ae30c0_u0<=1'h0;
else reg_50ae30c0_u0<=and_u4223_u0;
end
assign and_u4223_u0=and_u4212_u0&and_u4225_u0;
always @(posedge CLK or posedge RESET)
begin
if (RESET)
reg_6da5cba0_u0<=1'h0;
else reg_6da5cba0_u0<=and_u4224_u0;
end
assign and_u4224_u0=and_u4210_u0&and_u4203_u0;
assign and_u4225_u0=and_u4211_u0&and_u4203_u0;
assign or_u1629_u0=reg_6da5cba0_u0|or_u1627_u0;
assign receive_go_merge=simplePinWrite|simplePinWrite_u662;
assign mux_u2143_u0=(and_u4208_u0)?1'h1:mux_u2142;
assign or_u1630_u0=and_u4208_u0|or_u1628_u0;
always @(posedge CLK)
begin
if (reg_733e7a39_u0)
syncEnable_u1628<=RESET;
end
always @(posedge CLK or posedge syncEnable_u1628)
begin
if (syncEnable_u1628)
loopControl_u116<=1'h0;
else loopControl_u116<=or_u1629_u0;
end
assign or_u1631_u0=reg_733e7a39_u0|loopControl_u116;
always @(posedge CLK or posedge RESET)
begin
if (RESET)
reg_733e7a39_u0<=1'h0;
else reg_733e7a39_u0<=reg_09ea6fcc_u0;
end
assign or_u1632_u0=GO|or_u1630_u0;
assign mux_u2144_u0=(GO)?1'h0:mux_u2143_u0;
always @(posedge CLK or posedge RESET)
begin
if (RESET)
reg_09ea6fcc_u0<=1'h0;
else reg_09ea6fcc_u0<=GO;
end
assign RESULT=or_u1632_u0;
assign RESULT_u2458=mux_u2144_u0;
assign RESULT_u2459=simplePinWrite_u663;
assign RESULT_u2460=receive_go_merge;
assign DONE=1'h0;
endmodule



module medianRow6_Kicker_68(CLK, RESET, bus_16b6fffb_);
input		CLK;
input		RESET;
output		bus_16b6fffb_;
wire		bus_53a3da99_;
reg		kicker_1=1'h0;
reg		kicker_2=1'h0;
wire		bus_58d08333_;
wire		bus_6a93a531_;
wire		bus_29299c53_;
reg		kicker_res=1'h0;
assign bus_53a3da99_=kicker_1&bus_6a93a531_&bus_58d08333_;
assign bus_16b6fffb_=kicker_res;
always @(posedge CLK)
begin
kicker_1<=bus_6a93a531_;
end
always @(posedge CLK)
begin
kicker_2<=bus_29299c53_;
end
assign bus_58d08333_=~kicker_2;
assign bus_6a93a531_=~RESET;
assign bus_29299c53_=bus_6a93a531_&kicker_1;
always @(posedge CLK)
begin
kicker_res<=bus_53a3da99_;
end
endmodule



module medianRow6_simplememoryreferee_68bebf1e_(bus_6b3c4517_, bus_5e0ba844_, bus_774e4f78_, bus_3d97b47e_, bus_00aeb7ba_, bus_1bca6168_, bus_58d015d2_, bus_4c2d257c_, bus_0d8ef16f_, bus_59e284e8_, bus_2493d875_, bus_222edaa6_, bus_2b87aedc_, bus_219762fa_, bus_139a0327_, bus_09b45143_, bus_5028981f_, bus_1bf8d762_, bus_52a0137c_, bus_5f673851_, bus_72cb88f4_);
input		bus_6b3c4517_;
input		bus_5e0ba844_;
input		bus_774e4f78_;
input	[31:0]	bus_3d97b47e_;
input		bus_00aeb7ba_;
input	[31:0]	bus_1bca6168_;
input	[31:0]	bus_58d015d2_;
input	[2:0]	bus_4c2d257c_;
input		bus_0d8ef16f_;
input		bus_59e284e8_;
input	[31:0]	bus_2493d875_;
input	[31:0]	bus_222edaa6_;
input	[2:0]	bus_2b87aedc_;
output	[31:0]	bus_219762fa_;
output	[31:0]	bus_139a0327_;
output		bus_09b45143_;
output		bus_5028981f_;
output	[2:0]	bus_1bf8d762_;
output		bus_52a0137c_;
output	[31:0]	bus_5f673851_;
output		bus_72cb88f4_;
wire		not_047c223c_u0;
wire		or_494f2d6d_u0;
wire		or_0346c965_u0;
wire		and_077dc179_u0;
wire	[31:0]	mux_5b932575_u0;
reg		done_qual_u240=1'h0;
wire		and_25f24764_u0;
wire		or_31d505a2_u0;
wire		or_7e7a0682_u0;
wire		not_4652030b_u0;
wire	[31:0]	mux_36191943_u0;
reg		done_qual_u241_u0=1'h0;
wire		or_021cf3a1_u0;
assign not_047c223c_u0=~bus_774e4f78_;
assign or_494f2d6d_u0=bus_00aeb7ba_|bus_59e284e8_;
assign or_0346c965_u0=or_31d505a2_u0|done_qual_u240;
assign and_077dc179_u0=or_0346c965_u0&bus_774e4f78_;
assign mux_5b932575_u0=(bus_00aeb7ba_)?{24'b0, bus_1bca6168_[7:0]}:bus_2493d875_;
always @(posedge bus_6b3c4517_)
begin
if (bus_5e0ba844_)
done_qual_u240<=1'h0;
else done_qual_u240<=or_31d505a2_u0;
end
assign and_25f24764_u0=or_7e7a0682_u0&bus_774e4f78_;
assign or_31d505a2_u0=bus_0d8ef16f_|bus_59e284e8_;
assign or_7e7a0682_u0=bus_00aeb7ba_|done_qual_u241_u0;
assign not_4652030b_u0=~bus_774e4f78_;
assign bus_219762fa_=mux_5b932575_u0;
assign bus_139a0327_=mux_36191943_u0;
assign bus_09b45143_=or_494f2d6d_u0;
assign bus_5028981f_=or_021cf3a1_u0;
assign bus_1bf8d762_=3'h1;
assign bus_52a0137c_=and_25f24764_u0;
assign bus_5f673851_=bus_3d97b47e_;
assign bus_72cb88f4_=and_077dc179_u0;
assign mux_36191943_u0=(bus_00aeb7ba_)?bus_58d015d2_:bus_222edaa6_;
always @(posedge bus_6b3c4517_)
begin
if (bus_5e0ba844_)
done_qual_u241_u0<=1'h0;
else done_qual_u241_u0<=bus_00aeb7ba_;
end
assign or_021cf3a1_u0=bus_00aeb7ba_|or_31d505a2_u0;
endmodule



module medianRow6_compute_median(CLK, RESET, GO, port_7a5339ec_, port_6bb75d82_, port_2852be82_, port_7aa5eed2_, port_7b08cf53_, RESULT, RESULT_u2461, RESULT_u2462, RESULT_u2463, RESULT_u2464, RESULT_u2465, RESULT_u2466, RESULT_u2467, RESULT_u2468, RESULT_u2469, RESULT_u2470, RESULT_u2471, RESULT_u2472, RESULT_u2473, RESULT_u2474, DONE);
input		CLK;
input		RESET;
input		GO;
input		port_7a5339ec_;
input	[31:0]	port_6bb75d82_;
input		port_2852be82_;
input	[31:0]	port_7aa5eed2_;
input		port_7b08cf53_;
output		RESULT;
output	[31:0]	RESULT_u2461;
output		RESULT_u2462;
output	[31:0]	RESULT_u2463;
output	[2:0]	RESULT_u2464;
output		RESULT_u2465;
output	[31:0]	RESULT_u2466;
output	[2:0]	RESULT_u2467;
output		RESULT_u2468;
output	[31:0]	RESULT_u2469;
output	[31:0]	RESULT_u2470;
output	[2:0]	RESULT_u2471;
output	[7:0]	RESULT_u2472;
output		RESULT_u2473;
output	[15:0]	RESULT_u2474;
output		DONE;
wire		and_u4226_u0;
wire		and_u4227_u0;
wire		lessThan;
wire	[15:0]	lessThan_b_unsigned;
wire	[15:0]	lessThan_a_unsigned;
wire		andOp;
wire		and_u4228_u0;
wire		and_u4229_u0;
wire		not_u978_u0;
reg		syncEnable_u1629=1'h0;
wire signed	[32:0]	lessThan_u114_a_signed;
wire		lessThan_u114;
wire signed	[32:0]	lessThan_u114_b_signed;
wire		not_u979_u0;
wire		and_u4230_u0;
wire		and_u4231_u0;
reg	[31:0]	syncEnable_u1630_u0=32'h0;
wire		and_u4232_u0;
wire	[31:0]	add;
wire		and_u4233_u0;
wire	[31:0]	add_u783;
wire	[31:0]	add_u784;
wire		and_u4234_u0;
wire signed	[31:0]	greaterThan_a_signed;
wire signed	[31:0]	greaterThan_b_signed;
wire		greaterThan;
wire		and_u4235_u0;
wire		not_u980_u0;
wire		and_u4236_u0;
wire	[31:0]	add_u785;
wire		and_u4237_u0;
wire	[31:0]	add_u786;
wire	[31:0]	add_u787;
wire		and_u4238_u0;
wire	[31:0]	add_u788;
wire		and_u4239_u0;
wire		or_u1633_u0;
reg		reg_14a2446d_u0=1'h0;
wire	[31:0]	add_u789;
wire	[31:0]	add_u790;
wire		or_u1634_u0;
reg		reg_1d8e222b_u0=1'h0;
wire		and_u4240_u0;
reg	[31:0]	syncEnable_u1631_u0=32'h0;
reg	[31:0]	syncEnable_u1632_u0=32'h0;
reg	[31:0]	syncEnable_u1633_u0=32'h0;
reg		reg_22505fb0_u0=1'h0;
reg		block_GO_delayed_u118=1'h0;
reg	[31:0]	syncEnable_u1634_u0=32'h0;
reg	[31:0]	syncEnable_u1635_u0=32'h0;
reg	[31:0]	syncEnable_u1636_u0=32'h0;
wire	[31:0]	mux_u2145;
wire	[31:0]	mux_u2146_u0;
wire		or_u1635_u0;
reg	[31:0]	syncEnable_u1637_u0=32'h0;
wire		or_u1636_u0;
reg		reg_319b8394_u0=1'h0;
reg		syncEnable_u1638_u0=1'h0;
wire	[31:0]	mux_u2147_u0;
reg		reg_319b8394_result_delayed_u0=1'h0;
wire		and_u4241_u0;
reg		reg_54543c07_u0=1'h0;
reg	[31:0]	syncEnable_u1639_u0=32'h0;
wire		and_u4242_u0;
reg		reg_319b8394_result_delayed_result_delayed_u0=1'h0;
reg		and_delayed_u422=1'h0;
wire		mux_u2148_u0;
wire	[31:0]	mux_u2149_u0;
wire	[31:0]	add_u791;
reg		syncEnable_u1640_u0=1'h0;
reg	[31:0]	syncEnable_u1641_u0=32'h0;
reg	[31:0]	syncEnable_u1642_u0=32'h0;
wire		or_u1637_u0;
wire	[31:0]	mux_u2150_u0;
reg	[31:0]	syncEnable_u1643_u0=32'h0;
reg	[31:0]	syncEnable_u1644_u0=32'h0;
reg		block_GO_delayed_u119_u0=1'h0;
reg	[31:0]	syncEnable_u1645_u0=32'h0;
wire	[31:0]	mux_u2151_u0;
wire		or_u1638_u0;
reg	[31:0]	syncEnable_u1646_u0=32'h0;
reg	[31:0]	syncEnable_u1647_u0=32'h0;
wire		mux_u2152_u0;
wire	[31:0]	mux_u2153_u0;
wire	[31:0]	mux_u2154_u0;
wire	[31:0]	mux_u2155_u0;
wire	[31:0]	mux_u2156_u0;
wire	[31:0]	mux_u2157_u0;
wire		or_u1639_u0;
wire	[15:0]	add_u792;
reg		reg_48858a46_u0=1'h0;
reg		scoreboard_50b26d7d_reg1=1'h0;
wire		bus_1d33e52f_;
reg		scoreboard_50b26d7d_reg0=1'h0;
wire		scoreboard_50b26d7d_resOr1;
wire		scoreboard_50b26d7d_resOr0;
wire		scoreboard_50b26d7d_and;
reg	[31:0]	latch_09a28e76_reg=32'h0;
wire	[31:0]	latch_09a28e76_out;
reg	[31:0]	latch_7aba9fcf_reg=32'h0;
wire	[31:0]	latch_7aba9fcf_out;
wire	[31:0]	latch_3c40397c_out;
reg	[31:0]	latch_3c40397c_reg=32'h0;
wire		latch_36c08d1d_out;
reg		latch_36c08d1d_reg=1'h0;
reg	[15:0]	syncEnable_u1648_u0=16'h0;
reg	[31:0]	latch_145a44a5_reg=32'h0;
wire	[31:0]	latch_145a44a5_out;
reg		loopControl_u117=1'h0;
wire	[31:0]	mux_u2158_u0;
reg		syncEnable_u1649_u0=1'h0;
reg	[31:0]	fbReg_tmp_row1_u56=32'h0;
wire		mux_u2159_u0;
wire	[31:0]	mux_u2160_u0;
reg		fbReg_swapped_u56=1'h0;
reg	[15:0]	fbReg_idx_u56=16'h0;
reg	[31:0]	fbReg_tmp_u56=32'h0;
wire	[31:0]	mux_u2161_u0;
wire		or_u1640_u0;
reg	[31:0]	fbReg_tmp_row0_u56=32'h0;
wire	[31:0]	mux_u2162_u0;
reg	[31:0]	fbReg_tmp_row_u56=32'h0;
wire	[15:0]	mux_u2163_u0;
wire		and_u4243_u0;
wire	[7:0]	simplePinWrite;
wire		simplePinWrite_u664;
wire	[15:0]	simplePinWrite_u665;
wire	[31:0]	mux_u2164_u0;
wire		or_u1641_u0;
reg		reg_14ed0f99_u0=1'h0;
reg		reg_39183166_u0=1'h0;
assign and_u4226_u0=and_u4229_u0&or_u1640_u0;
assign and_u4227_u0=and_u4228_u0&or_u1640_u0;
assign lessThan_a_unsigned=mux_u2163_u0;
assign lessThan_b_unsigned=16'h65;
assign lessThan=lessThan_a_unsigned<lessThan_b_unsigned;
assign andOp=lessThan&mux_u2159_u0;
assign and_u4228_u0=or_u1640_u0&andOp;
assign and_u4229_u0=or_u1640_u0&not_u978_u0;
assign not_u978_u0=~andOp;
always @(posedge CLK)
begin
if (or_u1639_u0)
syncEnable_u1629<=mux_u2152_u0;
end
assign lessThan_u114_a_signed={1'b0, mux_u2154_u0};
assign lessThan_u114_b_signed=33'h64;
assign lessThan_u114=lessThan_u114_a_signed<lessThan_u114_b_signed;
assign not_u979_u0=~lessThan_u114;
assign and_u4230_u0=or_u1639_u0&lessThan_u114;
assign and_u4231_u0=or_u1639_u0&not_u979_u0;
always @(posedge CLK)
begin
if (or_u1639_u0)
syncEnable_u1630_u0<=mux_u2157_u0;
end
assign and_u4232_u0=and_u4230_u0&or_u1639_u0;
assign add=mux_u2154_u0+32'h0;
assign and_u4233_u0=and_u4232_u0&port_7a5339ec_;
assign add_u783=mux_u2154_u0+32'h1;
assign add_u784=add_u783+32'h0;
assign and_u4234_u0=and_u4232_u0&port_7b08cf53_;
assign greaterThan_a_signed=syncEnable_u1643_u0;
assign greaterThan_b_signed=syncEnable_u1646_u0;
assign greaterThan=greaterThan_a_signed>greaterThan_b_signed;
assign and_u4235_u0=block_GO_delayed_u119_u0&greaterThan;
assign not_u980_u0=~greaterThan;
assign and_u4236_u0=block_GO_delayed_u119_u0&not_u980_u0;
assign add_u785=syncEnable_u1642_u0+32'h0;
assign and_u4237_u0=and_u4241_u0&port_7a5339ec_;
assign add_u786=syncEnable_u1642_u0+32'h1;
assign add_u787=add_u786+32'h0;
assign and_u4238_u0=and_u4241_u0&port_7b08cf53_;
assign add_u788=syncEnable_u1642_u0+32'h0;
assign and_u4239_u0=reg_14a2446d_u0&port_7b08cf53_;
assign or_u1633_u0=and_u4239_u0|RESET;
always @(posedge CLK or posedge block_GO_delayed_u118 or posedge or_u1633_u0)
begin
if (or_u1633_u0)
reg_14a2446d_u0<=1'h0;
else if (block_GO_delayed_u118)
reg_14a2446d_u0<=1'h1;
else reg_14a2446d_u0<=reg_14a2446d_u0;
end
assign add_u789=syncEnable_u1642_u0+32'h1;
assign add_u790=add_u789+32'h0;
assign or_u1634_u0=and_u4240_u0|RESET;
always @(posedge CLK or posedge reg_22505fb0_u0 or posedge or_u1634_u0)
begin
if (or_u1634_u0)
reg_1d8e222b_u0<=1'h0;
else if (reg_22505fb0_u0)
reg_1d8e222b_u0<=1'h1;
else reg_1d8e222b_u0<=reg_1d8e222b_u0;
end
assign and_u4240_u0=reg_1d8e222b_u0&port_7b08cf53_;
always @(posedge CLK)
begin
if (and_u4241_u0)
syncEnable_u1631_u0<=port_7aa5eed2_;
end
always @(posedge CLK)
begin
if (and_u4241_u0)
syncEnable_u1632_u0<=port_6bb75d82_;
end
always @(posedge CLK)
begin
if (and_u4241_u0)
syncEnable_u1633_u0<=port_6bb75d82_;
end
always @(posedge CLK or posedge RESET)
begin
if (RESET)
reg_22505fb0_u0<=1'h0;
else reg_22505fb0_u0<=block_GO_delayed_u118;
end
always @(posedge CLK or posedge RESET)
begin
if (RESET)
block_GO_delayed_u118<=1'h0;
else block_GO_delayed_u118<=and_u4241_u0;
end
always @(posedge CLK)
begin
if (and_u4241_u0)
syncEnable_u1634_u0<=add_u790;
end
always @(posedge CLK)
begin
if (and_u4241_u0)
syncEnable_u1635_u0<=port_7aa5eed2_;
end
always @(posedge CLK)
begin
if (and_u4241_u0)
syncEnable_u1636_u0<=add_u788;
end
assign mux_u2145=({32{block_GO_delayed_u118}}&syncEnable_u1636_u0)|({32{reg_22505fb0_u0}}&syncEnable_u1634_u0)|({32{and_u4241_u0}}&add_u787);
assign mux_u2146_u0=(block_GO_delayed_u118)?syncEnable_u1631_u0:syncEnable_u1633_u0;
assign or_u1635_u0=block_GO_delayed_u118|reg_22505fb0_u0;
always @(posedge CLK)
begin
if (block_GO_delayed_u119_u0)
syncEnable_u1637_u0<=syncEnable_u1645_u0;
end
assign or_u1636_u0=reg_54543c07_u0|and_delayed_u422;
always @(posedge CLK or posedge RESET)
begin
if (RESET)
reg_319b8394_u0<=1'h0;
else reg_319b8394_u0<=and_u4241_u0;
end
always @(posedge CLK)
begin
if (block_GO_delayed_u119_u0)
syncEnable_u1638_u0<=syncEnable_u1640_u0;
end
assign mux_u2147_u0=(reg_54543c07_u0)?syncEnable_u1635_u0:syncEnable_u1637_u0;
always @(posedge CLK or posedge RESET)
begin
if (RESET)
reg_319b8394_result_delayed_u0<=1'h0;
else reg_319b8394_result_delayed_u0<=reg_319b8394_u0;
end
assign and_u4241_u0=and_u4235_u0&block_GO_delayed_u119_u0;
always @(posedge CLK or posedge RESET)
begin
if (RESET)
reg_54543c07_u0<=1'h0;
else reg_54543c07_u0<=reg_319b8394_result_delayed_result_delayed_u0;
end
always @(posedge CLK)
begin
if (block_GO_delayed_u119_u0)
syncEnable_u1639_u0<=syncEnable_u1644_u0;
end
assign and_u4242_u0=and_u4236_u0&block_GO_delayed_u119_u0;
always @(posedge CLK or posedge RESET)
begin
if (RESET)
reg_319b8394_result_delayed_result_delayed_u0<=1'h0;
else reg_319b8394_result_delayed_result_delayed_u0<=reg_319b8394_result_delayed_u0;
end
always @(posedge CLK or posedge RESET)
begin
if (RESET)
and_delayed_u422<=1'h0;
else and_delayed_u422<=and_u4242_u0;
end
assign mux_u2148_u0=(reg_54543c07_u0)?1'h1:syncEnable_u1638_u0;
assign mux_u2149_u0=(reg_54543c07_u0)?syncEnable_u1632_u0:syncEnable_u1639_u0;
assign add_u791=mux_u2154_u0+32'h1;
always @(posedge CLK)
begin
if (and_u4232_u0)
syncEnable_u1640_u0<=mux_u2152_u0;
end
always @(posedge CLK)
begin
if (and_u4232_u0)
syncEnable_u1641_u0<=add_u791;
end
always @(posedge CLK)
begin
if (and_u4232_u0)
syncEnable_u1642_u0<=mux_u2154_u0;
end
assign or_u1637_u0=and_u4232_u0|and_u4241_u0;
assign mux_u2150_u0=({32{or_u1635_u0}}&mux_u2145)|({32{and_u4232_u0}}&add_u784)|({32{and_u4241_u0}}&mux_u2145);
always @(posedge CLK)
begin
if (and_u4232_u0)
syncEnable_u1643_u0<=port_6bb75d82_;
end
always @(posedge CLK)
begin
if (and_u4232_u0)
syncEnable_u1644_u0<=mux_u2153_u0;
end
always @(posedge CLK or posedge RESET)
begin
if (RESET)
block_GO_delayed_u119_u0<=1'h0;
else block_GO_delayed_u119_u0<=and_u4232_u0;
end
always @(posedge CLK)
begin
if (and_u4232_u0)
syncEnable_u1645_u0<=mux_u2156_u0;
end
assign mux_u2151_u0=(and_u4232_u0)?add:add_u785;
assign or_u1638_u0=and_u4232_u0|and_u4241_u0;
always @(posedge CLK)
begin
if (and_u4232_u0)
syncEnable_u1646_u0<=port_7aa5eed2_;
end
always @(posedge CLK)
begin
if (or_u1639_u0)
syncEnable_u1647_u0<=mux_u2153_u0;
end
assign mux_u2152_u0=(and_u4227_u0)?1'h0:mux_u2148_u0;
assign mux_u2153_u0=(and_u4227_u0)?mux_u2162_u0:mux_u2149_u0;
assign mux_u2154_u0=(and_u4227_u0)?32'h0:syncEnable_u1641_u0;
assign mux_u2155_u0=(and_u4227_u0)?mux_u2160_u0:syncEnable_u1643_u0;
assign mux_u2156_u0=(and_u4227_u0)?mux_u2161_u0:mux_u2147_u0;
assign mux_u2157_u0=(and_u4227_u0)?mux_u2158_u0:syncEnable_u1646_u0;
assign or_u1639_u0=and_u4227_u0|or_u1636_u0;
assign add_u792=mux_u2163_u0+16'h1;
always @(posedge CLK or posedge RESET)
begin
if (RESET)
reg_48858a46_u0<=1'h0;
else reg_48858a46_u0<=or_u1636_u0;
end
always @(posedge CLK)
begin
if (bus_1d33e52f_)
scoreboard_50b26d7d_reg1<=1'h0;
else if (or_u1636_u0)
scoreboard_50b26d7d_reg1<=1'h1;
else scoreboard_50b26d7d_reg1<=scoreboard_50b26d7d_reg1;
end
assign bus_1d33e52f_=scoreboard_50b26d7d_and|RESET;
always @(posedge CLK)
begin
if (bus_1d33e52f_)
scoreboard_50b26d7d_reg0<=1'h0;
else if (reg_48858a46_u0)
scoreboard_50b26d7d_reg0<=1'h1;
else scoreboard_50b26d7d_reg0<=scoreboard_50b26d7d_reg0;
end
assign scoreboard_50b26d7d_resOr1=or_u1636_u0|scoreboard_50b26d7d_reg1;
assign scoreboard_50b26d7d_resOr0=reg_48858a46_u0|scoreboard_50b26d7d_reg0;
assign scoreboard_50b26d7d_and=scoreboard_50b26d7d_resOr0&scoreboard_50b26d7d_resOr1;
always @(posedge CLK)
begin
if (or_u1636_u0)
latch_09a28e76_reg<=mux_u2147_u0;
end
assign latch_09a28e76_out=(or_u1636_u0)?mux_u2147_u0:latch_09a28e76_reg;
always @(posedge CLK)
begin
if (or_u1636_u0)
latch_7aba9fcf_reg<=syncEnable_u1643_u0;
end
assign latch_7aba9fcf_out=(or_u1636_u0)?syncEnable_u1643_u0:latch_7aba9fcf_reg;
assign latch_3c40397c_out=(or_u1636_u0)?syncEnable_u1647_u0:latch_3c40397c_reg;
always @(posedge CLK)
begin
if (or_u1636_u0)
latch_3c40397c_reg<=syncEnable_u1647_u0;
end
assign latch_36c08d1d_out=(or_u1636_u0)?syncEnable_u1629:latch_36c08d1d_reg;
always @(posedge CLK)
begin
if (or_u1636_u0)
latch_36c08d1d_reg<=syncEnable_u1629;
end
always @(posedge CLK)
begin
if (and_u4227_u0)
syncEnable_u1648_u0<=add_u792;
end
always @(posedge CLK)
begin
if (or_u1636_u0)
latch_145a44a5_reg<=syncEnable_u1630_u0;
end
assign latch_145a44a5_out=(or_u1636_u0)?syncEnable_u1630_u0:latch_145a44a5_reg;
always @(posedge CLK or posedge syncEnable_u1649_u0)
begin
if (syncEnable_u1649_u0)
loopControl_u117<=1'h0;
else loopControl_u117<=scoreboard_50b26d7d_and;
end
assign mux_u2158_u0=(loopControl_u117)?fbReg_tmp_row0_u56:32'h0;
always @(posedge CLK)
begin
if (GO)
syncEnable_u1649_u0<=RESET;
end
always @(posedge CLK)
begin
if (scoreboard_50b26d7d_and)
fbReg_tmp_row1_u56<=latch_09a28e76_out;
end
assign mux_u2159_u0=(loopControl_u117)?fbReg_swapped_u56:1'h0;
assign mux_u2160_u0=(loopControl_u117)?fbReg_tmp_row_u56:32'h0;
always @(posedge CLK)
begin
if (scoreboard_50b26d7d_and)
fbReg_swapped_u56<=latch_36c08d1d_out;
end
always @(posedge CLK)
begin
if (scoreboard_50b26d7d_and)
fbReg_idx_u56<=syncEnable_u1648_u0;
end
always @(posedge CLK)
begin
if (scoreboard_50b26d7d_and)
fbReg_tmp_u56<=latch_3c40397c_out;
end
assign mux_u2161_u0=(loopControl_u117)?fbReg_tmp_row1_u56:32'h0;
assign or_u1640_u0=loopControl_u117|GO;
always @(posedge CLK)
begin
if (scoreboard_50b26d7d_and)
fbReg_tmp_row0_u56<=latch_145a44a5_out;
end
assign mux_u2162_u0=(loopControl_u117)?fbReg_tmp_u56:32'h0;
always @(posedge CLK)
begin
if (scoreboard_50b26d7d_and)
fbReg_tmp_row_u56<=latch_7aba9fcf_out;
end
assign mux_u2163_u0=(loopControl_u117)?fbReg_idx_u56:16'h0;
assign and_u4243_u0=reg_39183166_u0&port_7a5339ec_;
assign simplePinWrite=port_6bb75d82_[7:0];
assign simplePinWrite_u664=reg_39183166_u0&{1{reg_39183166_u0}};
assign simplePinWrite_u665=16'h1&{16{1'h1}};
assign mux_u2164_u0=(or_u1638_u0)?mux_u2151_u0:32'h32;
assign or_u1641_u0=or_u1638_u0|reg_39183166_u0;
always @(posedge CLK or posedge RESET)
begin
if (RESET)
reg_14ed0f99_u0<=1'h0;
else reg_14ed0f99_u0<=reg_39183166_u0;
end
always @(posedge CLK or posedge RESET)
begin
if (RESET)
reg_39183166_u0<=1'h0;
else reg_39183166_u0<=and_u4226_u0;
end
assign RESULT=GO;
assign RESULT_u2461=32'h0;
assign RESULT_u2462=or_u1641_u0;
assign RESULT_u2463=mux_u2164_u0;
assign RESULT_u2464=3'h1;
assign RESULT_u2465=or_u1637_u0;
assign RESULT_u2466=mux_u2150_u0;
assign RESULT_u2467=3'h1;
assign RESULT_u2468=or_u1635_u0;
assign RESULT_u2469=mux_u2150_u0;
assign RESULT_u2470=mux_u2146_u0;
assign RESULT_u2471=3'h1;
assign RESULT_u2472=simplePinWrite;
assign RESULT_u2473=simplePinWrite_u664;
assign RESULT_u2474=simplePinWrite_u665;
assign DONE=reg_14ed0f99_u0;
endmodule



module medianRow6_forge_memory_101x32_165(CLK, ENA, WEA, DINA, ENB, ADDRA, ADDRB, DOUTA, DOUTB, DONEA, DONEB);
input		CLK;
input		ENA;
input		WEA;
input	[31:0]	DINA;
input		ENB;
input	[31:0]	ADDRA;
input	[31:0]	ADDRB;
output	[31:0]	DOUTA;
output	[31:0]	DOUTB;
output		DONEA;
output		DONEB;
wire		wea_0;
wire	[31:0]	pre_douta_0;
wire	[31:0]	pre_doutb_0;
wire		wea_1;
wire	[31:0]	pre_douta_1;
wire	[31:0]	pre_doutb_1;
reg		wea_done;
reg		rea_done;
reg	[31:0]	mux_outa;
reg	[31:0]	mux_outb;
assign wea_0=WEA&(ADDRA[31:6]==26'h0);
assign wea_1=WEA&(ADDRA[31:6]==26'h1);
always @(posedge CLK)
begin
wea_done<=WEA;
end
assign DONEA=rea_done|wea_done;
always @(posedge CLK)
begin
end
assign DONEB=ENB;
always @(pre_douta_0 or pre_douta_1 or ADDRA)
begin
case (ADDRA[31:6])26'd0:mux_outa=pre_douta_0;
26'd1:mux_outa=pre_douta_1;
default:mux_outa=32'h0;
endcase end
assign DOUTA=mux_outa;
always @(pre_doutb_0 or pre_doutb_1 or ADDRB)
begin
case (ADDRB[31:6])26'd0:mux_outb=pre_doutb_0;
26'd1:mux_outb=pre_doutb_1;
default:mux_outb=32'h0;
endcase end
assign DOUTB=mux_outb;
// Memory array element: COL: 0, ROW: 0
//  Initialization of Dual Port LUT ram now done through explicit
// parameter setting.
RAM64X1D#(.INIT(64'h0000000000000000))RAM64X1D_instance_3584(.WCLK(CLK), .WE(wea_0), 
  .A0(ADDRA[0]), .A1(ADDRA[1]), .A2(ADDRA[2]), .A3(ADDRA[3]), .A4(ADDRA[4]), .A5(ADDRA[5]), 
  .D(DINA[0]), .SPO(pre_douta_0[0]), .DPRA0(ADDRB[0]), .DPRA1(ADDRB[1]), .DPRA2(ADDRB[2]), 
  .DPRA3(ADDRB[3]), .DPRA4(ADDRB[4]), .DPRA5(ADDRB[5]), .DPO(pre_doutb_0[0]));
// Memory array element: COL: 0, ROW: 1
//  Initialization of Dual Port LUT ram now done through explicit
// parameter setting.
RAM64X1D#(.INIT(64'h0000000000000000))RAM64X1D_instance_3585(.WCLK(CLK), .WE(wea_0), 
  .A0(ADDRA[0]), .A1(ADDRA[1]), .A2(ADDRA[2]), .A3(ADDRA[3]), .A4(ADDRA[4]), .A5(ADDRA[5]), 
  .D(DINA[1]), .SPO(pre_douta_0[1]), .DPRA0(ADDRB[0]), .DPRA1(ADDRB[1]), .DPRA2(ADDRB[2]), 
  .DPRA3(ADDRB[3]), .DPRA4(ADDRB[4]), .DPRA5(ADDRB[5]), .DPO(pre_doutb_0[1]));
// Memory array element: COL: 0, ROW: 2
//  Initialization of Dual Port LUT ram now done through explicit
// parameter setting.
RAM64X1D#(.INIT(64'h0000000000000000))RAM64X1D_instance_3586(.WCLK(CLK), .WE(wea_0), 
  .A0(ADDRA[0]), .A1(ADDRA[1]), .A2(ADDRA[2]), .A3(ADDRA[3]), .A4(ADDRA[4]), .A5(ADDRA[5]), 
  .D(DINA[2]), .SPO(pre_douta_0[2]), .DPRA0(ADDRB[0]), .DPRA1(ADDRB[1]), .DPRA2(ADDRB[2]), 
  .DPRA3(ADDRB[3]), .DPRA4(ADDRB[4]), .DPRA5(ADDRB[5]), .DPO(pre_doutb_0[2]));
// Memory array element: COL: 0, ROW: 3
//  Initialization of Dual Port LUT ram now done through explicit
// parameter setting.
RAM64X1D#(.INIT(64'h0000000000000000))RAM64X1D_instance_3587(.WCLK(CLK), .WE(wea_0), 
  .A0(ADDRA[0]), .A1(ADDRA[1]), .A2(ADDRA[2]), .A3(ADDRA[3]), .A4(ADDRA[4]), .A5(ADDRA[5]), 
  .D(DINA[3]), .SPO(pre_douta_0[3]), .DPRA0(ADDRB[0]), .DPRA1(ADDRB[1]), .DPRA2(ADDRB[2]), 
  .DPRA3(ADDRB[3]), .DPRA4(ADDRB[4]), .DPRA5(ADDRB[5]), .DPO(pre_doutb_0[3]));
// Memory array element: COL: 0, ROW: 4
//  Initialization of Dual Port LUT ram now done through explicit
// parameter setting.
RAM64X1D#(.INIT(64'h0000000000000000))RAM64X1D_instance_3588(.WCLK(CLK), .WE(wea_0), 
  .A0(ADDRA[0]), .A1(ADDRA[1]), .A2(ADDRA[2]), .A3(ADDRA[3]), .A4(ADDRA[4]), .A5(ADDRA[5]), 
  .D(DINA[4]), .SPO(pre_douta_0[4]), .DPRA0(ADDRB[0]), .DPRA1(ADDRB[1]), .DPRA2(ADDRB[2]), 
  .DPRA3(ADDRB[3]), .DPRA4(ADDRB[4]), .DPRA5(ADDRB[5]), .DPO(pre_doutb_0[4]));
// Memory array element: COL: 0, ROW: 5
//  Initialization of Dual Port LUT ram now done through explicit
// parameter setting.
RAM64X1D#(.INIT(64'h0000000000000000))RAM64X1D_instance_3589(.WCLK(CLK), .WE(wea_0), 
  .A0(ADDRA[0]), .A1(ADDRA[1]), .A2(ADDRA[2]), .A3(ADDRA[3]), .A4(ADDRA[4]), .A5(ADDRA[5]), 
  .D(DINA[5]), .SPO(pre_douta_0[5]), .DPRA0(ADDRB[0]), .DPRA1(ADDRB[1]), .DPRA2(ADDRB[2]), 
  .DPRA3(ADDRB[3]), .DPRA4(ADDRB[4]), .DPRA5(ADDRB[5]), .DPO(pre_doutb_0[5]));
// Memory array element: COL: 0, ROW: 6
//  Initialization of Dual Port LUT ram now done through explicit
// parameter setting.
RAM64X1D#(.INIT(64'h0000000000000000))RAM64X1D_instance_3590(.WCLK(CLK), .WE(wea_0), 
  .A0(ADDRA[0]), .A1(ADDRA[1]), .A2(ADDRA[2]), .A3(ADDRA[3]), .A4(ADDRA[4]), .A5(ADDRA[5]), 
  .D(DINA[6]), .SPO(pre_douta_0[6]), .DPRA0(ADDRB[0]), .DPRA1(ADDRB[1]), .DPRA2(ADDRB[2]), 
  .DPRA3(ADDRB[3]), .DPRA4(ADDRB[4]), .DPRA5(ADDRB[5]), .DPO(pre_doutb_0[6]));
// Memory array element: COL: 0, ROW: 7
//  Initialization of Dual Port LUT ram now done through explicit
// parameter setting.
RAM64X1D#(.INIT(64'h0000000000000000))RAM64X1D_instance_3591(.WCLK(CLK), .WE(wea_0), 
  .A0(ADDRA[0]), .A1(ADDRA[1]), .A2(ADDRA[2]), .A3(ADDRA[3]), .A4(ADDRA[4]), .A5(ADDRA[5]), 
  .D(DINA[7]), .SPO(pre_douta_0[7]), .DPRA0(ADDRB[0]), .DPRA1(ADDRB[1]), .DPRA2(ADDRB[2]), 
  .DPRA3(ADDRB[3]), .DPRA4(ADDRB[4]), .DPRA5(ADDRB[5]), .DPO(pre_doutb_0[7]));
// Memory array element: COL: 0, ROW: 8
//  Initialization of Dual Port LUT ram now done through explicit
// parameter setting.
RAM64X1D#(.INIT(64'h0000000000000000))RAM64X1D_instance_3592(.WCLK(CLK), .WE(wea_0), 
  .A0(ADDRA[0]), .A1(ADDRA[1]), .A2(ADDRA[2]), .A3(ADDRA[3]), .A4(ADDRA[4]), .A5(ADDRA[5]), 
  .D(DINA[8]), .SPO(pre_douta_0[8]), .DPRA0(ADDRB[0]), .DPRA1(ADDRB[1]), .DPRA2(ADDRB[2]), 
  .DPRA3(ADDRB[3]), .DPRA4(ADDRB[4]), .DPRA5(ADDRB[5]), .DPO(pre_doutb_0[8]));
// Memory array element: COL: 0, ROW: 9
//  Initialization of Dual Port LUT ram now done through explicit
// parameter setting.
RAM64X1D#(.INIT(64'h0000000000000000))RAM64X1D_instance_3593(.WCLK(CLK), .WE(wea_0), 
  .A0(ADDRA[0]), .A1(ADDRA[1]), .A2(ADDRA[2]), .A3(ADDRA[3]), .A4(ADDRA[4]), .A5(ADDRA[5]), 
  .D(DINA[9]), .SPO(pre_douta_0[9]), .DPRA0(ADDRB[0]), .DPRA1(ADDRB[1]), .DPRA2(ADDRB[2]), 
  .DPRA3(ADDRB[3]), .DPRA4(ADDRB[4]), .DPRA5(ADDRB[5]), .DPO(pre_doutb_0[9]));
// Memory array element: COL: 0, ROW: 10
//  Initialization of Dual Port LUT ram now done through explicit
// parameter setting.
RAM64X1D#(.INIT(64'h0000000000000000))RAM64X1D_instance_3594(.WCLK(CLK), .WE(wea_0), 
  .A0(ADDRA[0]), .A1(ADDRA[1]), .A2(ADDRA[2]), .A3(ADDRA[3]), .A4(ADDRA[4]), .A5(ADDRA[5]), 
  .D(DINA[10]), .SPO(pre_douta_0[10]), .DPRA0(ADDRB[0]), .DPRA1(ADDRB[1]), .DPRA2(ADDRB[2]), 
  .DPRA3(ADDRB[3]), .DPRA4(ADDRB[4]), .DPRA5(ADDRB[5]), .DPO(pre_doutb_0[10]));
// Memory array element: COL: 0, ROW: 11
//  Initialization of Dual Port LUT ram now done through explicit
// parameter setting.
RAM64X1D#(.INIT(64'h0000000000000000))RAM64X1D_instance_3595(.WCLK(CLK), .WE(wea_0), 
  .A0(ADDRA[0]), .A1(ADDRA[1]), .A2(ADDRA[2]), .A3(ADDRA[3]), .A4(ADDRA[4]), .A5(ADDRA[5]), 
  .D(DINA[11]), .SPO(pre_douta_0[11]), .DPRA0(ADDRB[0]), .DPRA1(ADDRB[1]), .DPRA2(ADDRB[2]), 
  .DPRA3(ADDRB[3]), .DPRA4(ADDRB[4]), .DPRA5(ADDRB[5]), .DPO(pre_doutb_0[11]));
// Memory array element: COL: 0, ROW: 12
//  Initialization of Dual Port LUT ram now done through explicit
// parameter setting.
RAM64X1D#(.INIT(64'h0000000000000000))RAM64X1D_instance_3596(.WCLK(CLK), .WE(wea_0), 
  .A0(ADDRA[0]), .A1(ADDRA[1]), .A2(ADDRA[2]), .A3(ADDRA[3]), .A4(ADDRA[4]), .A5(ADDRA[5]), 
  .D(DINA[12]), .SPO(pre_douta_0[12]), .DPRA0(ADDRB[0]), .DPRA1(ADDRB[1]), .DPRA2(ADDRB[2]), 
  .DPRA3(ADDRB[3]), .DPRA4(ADDRB[4]), .DPRA5(ADDRB[5]), .DPO(pre_doutb_0[12]));
// Memory array element: COL: 0, ROW: 13
//  Initialization of Dual Port LUT ram now done through explicit
// parameter setting.
RAM64X1D#(.INIT(64'h0000000000000000))RAM64X1D_instance_3597(.WCLK(CLK), .WE(wea_0), 
  .A0(ADDRA[0]), .A1(ADDRA[1]), .A2(ADDRA[2]), .A3(ADDRA[3]), .A4(ADDRA[4]), .A5(ADDRA[5]), 
  .D(DINA[13]), .SPO(pre_douta_0[13]), .DPRA0(ADDRB[0]), .DPRA1(ADDRB[1]), .DPRA2(ADDRB[2]), 
  .DPRA3(ADDRB[3]), .DPRA4(ADDRB[4]), .DPRA5(ADDRB[5]), .DPO(pre_doutb_0[13]));
// Memory array element: COL: 0, ROW: 14
//  Initialization of Dual Port LUT ram now done through explicit
// parameter setting.
RAM64X1D#(.INIT(64'h0000000000000000))RAM64X1D_instance_3598(.WCLK(CLK), .WE(wea_0), 
  .A0(ADDRA[0]), .A1(ADDRA[1]), .A2(ADDRA[2]), .A3(ADDRA[3]), .A4(ADDRA[4]), .A5(ADDRA[5]), 
  .D(DINA[14]), .SPO(pre_douta_0[14]), .DPRA0(ADDRB[0]), .DPRA1(ADDRB[1]), .DPRA2(ADDRB[2]), 
  .DPRA3(ADDRB[3]), .DPRA4(ADDRB[4]), .DPRA5(ADDRB[5]), .DPO(pre_doutb_0[14]));
// Memory array element: COL: 0, ROW: 15
//  Initialization of Dual Port LUT ram now done through explicit
// parameter setting.
RAM64X1D#(.INIT(64'h0000000000000000))RAM64X1D_instance_3599(.WCLK(CLK), .WE(wea_0), 
  .A0(ADDRA[0]), .A1(ADDRA[1]), .A2(ADDRA[2]), .A3(ADDRA[3]), .A4(ADDRA[4]), .A5(ADDRA[5]), 
  .D(DINA[15]), .SPO(pre_douta_0[15]), .DPRA0(ADDRB[0]), .DPRA1(ADDRB[1]), .DPRA2(ADDRB[2]), 
  .DPRA3(ADDRB[3]), .DPRA4(ADDRB[4]), .DPRA5(ADDRB[5]), .DPO(pre_doutb_0[15]));
// Memory array element: COL: 0, ROW: 16
//  Initialization of Dual Port LUT ram now done through explicit
// parameter setting.
RAM64X1D#(.INIT(64'h0000000000000000))RAM64X1D_instance_3600(.WCLK(CLK), .WE(wea_0), 
  .A0(ADDRA[0]), .A1(ADDRA[1]), .A2(ADDRA[2]), .A3(ADDRA[3]), .A4(ADDRA[4]), .A5(ADDRA[5]), 
  .D(DINA[16]), .SPO(pre_douta_0[16]), .DPRA0(ADDRB[0]), .DPRA1(ADDRB[1]), .DPRA2(ADDRB[2]), 
  .DPRA3(ADDRB[3]), .DPRA4(ADDRB[4]), .DPRA5(ADDRB[5]), .DPO(pre_doutb_0[16]));
// Memory array element: COL: 0, ROW: 17
//  Initialization of Dual Port LUT ram now done through explicit
// parameter setting.
RAM64X1D#(.INIT(64'h0000000000000000))RAM64X1D_instance_3601(.WCLK(CLK), .WE(wea_0), 
  .A0(ADDRA[0]), .A1(ADDRA[1]), .A2(ADDRA[2]), .A3(ADDRA[3]), .A4(ADDRA[4]), .A5(ADDRA[5]), 
  .D(DINA[17]), .SPO(pre_douta_0[17]), .DPRA0(ADDRB[0]), .DPRA1(ADDRB[1]), .DPRA2(ADDRB[2]), 
  .DPRA3(ADDRB[3]), .DPRA4(ADDRB[4]), .DPRA5(ADDRB[5]), .DPO(pre_doutb_0[17]));
// Memory array element: COL: 0, ROW: 18
//  Initialization of Dual Port LUT ram now done through explicit
// parameter setting.
RAM64X1D#(.INIT(64'h0000000000000000))RAM64X1D_instance_3602(.WCLK(CLK), .WE(wea_0), 
  .A0(ADDRA[0]), .A1(ADDRA[1]), .A2(ADDRA[2]), .A3(ADDRA[3]), .A4(ADDRA[4]), .A5(ADDRA[5]), 
  .D(DINA[18]), .SPO(pre_douta_0[18]), .DPRA0(ADDRB[0]), .DPRA1(ADDRB[1]), .DPRA2(ADDRB[2]), 
  .DPRA3(ADDRB[3]), .DPRA4(ADDRB[4]), .DPRA5(ADDRB[5]), .DPO(pre_doutb_0[18]));
// Memory array element: COL: 0, ROW: 19
//  Initialization of Dual Port LUT ram now done through explicit
// parameter setting.
RAM64X1D#(.INIT(64'h0000000000000000))RAM64X1D_instance_3603(.WCLK(CLK), .WE(wea_0), 
  .A0(ADDRA[0]), .A1(ADDRA[1]), .A2(ADDRA[2]), .A3(ADDRA[3]), .A4(ADDRA[4]), .A5(ADDRA[5]), 
  .D(DINA[19]), .SPO(pre_douta_0[19]), .DPRA0(ADDRB[0]), .DPRA1(ADDRB[1]), .DPRA2(ADDRB[2]), 
  .DPRA3(ADDRB[3]), .DPRA4(ADDRB[4]), .DPRA5(ADDRB[5]), .DPO(pre_doutb_0[19]));
// Memory array element: COL: 0, ROW: 20
//  Initialization of Dual Port LUT ram now done through explicit
// parameter setting.
RAM64X1D#(.INIT(64'h0000000000000000))RAM64X1D_instance_3604(.WCLK(CLK), .WE(wea_0), 
  .A0(ADDRA[0]), .A1(ADDRA[1]), .A2(ADDRA[2]), .A3(ADDRA[3]), .A4(ADDRA[4]), .A5(ADDRA[5]), 
  .D(DINA[20]), .SPO(pre_douta_0[20]), .DPRA0(ADDRB[0]), .DPRA1(ADDRB[1]), .DPRA2(ADDRB[2]), 
  .DPRA3(ADDRB[3]), .DPRA4(ADDRB[4]), .DPRA5(ADDRB[5]), .DPO(pre_doutb_0[20]));
// Memory array element: COL: 0, ROW: 21
//  Initialization of Dual Port LUT ram now done through explicit
// parameter setting.
RAM64X1D#(.INIT(64'h0000000000000000))RAM64X1D_instance_3605(.WCLK(CLK), .WE(wea_0), 
  .A0(ADDRA[0]), .A1(ADDRA[1]), .A2(ADDRA[2]), .A3(ADDRA[3]), .A4(ADDRA[4]), .A5(ADDRA[5]), 
  .D(DINA[21]), .SPO(pre_douta_0[21]), .DPRA0(ADDRB[0]), .DPRA1(ADDRB[1]), .DPRA2(ADDRB[2]), 
  .DPRA3(ADDRB[3]), .DPRA4(ADDRB[4]), .DPRA5(ADDRB[5]), .DPO(pre_doutb_0[21]));
// Memory array element: COL: 0, ROW: 22
//  Initialization of Dual Port LUT ram now done through explicit
// parameter setting.
RAM64X1D#(.INIT(64'h0000000000000000))RAM64X1D_instance_3606(.WCLK(CLK), .WE(wea_0), 
  .A0(ADDRA[0]), .A1(ADDRA[1]), .A2(ADDRA[2]), .A3(ADDRA[3]), .A4(ADDRA[4]), .A5(ADDRA[5]), 
  .D(DINA[22]), .SPO(pre_douta_0[22]), .DPRA0(ADDRB[0]), .DPRA1(ADDRB[1]), .DPRA2(ADDRB[2]), 
  .DPRA3(ADDRB[3]), .DPRA4(ADDRB[4]), .DPRA5(ADDRB[5]), .DPO(pre_doutb_0[22]));
// Memory array element: COL: 0, ROW: 23
//  Initialization of Dual Port LUT ram now done through explicit
// parameter setting.
RAM64X1D#(.INIT(64'h0000000000000000))RAM64X1D_instance_3607(.WCLK(CLK), .WE(wea_0), 
  .A0(ADDRA[0]), .A1(ADDRA[1]), .A2(ADDRA[2]), .A3(ADDRA[3]), .A4(ADDRA[4]), .A5(ADDRA[5]), 
  .D(DINA[23]), .SPO(pre_douta_0[23]), .DPRA0(ADDRB[0]), .DPRA1(ADDRB[1]), .DPRA2(ADDRB[2]), 
  .DPRA3(ADDRB[3]), .DPRA4(ADDRB[4]), .DPRA5(ADDRB[5]), .DPO(pre_doutb_0[23]));
// Memory array element: COL: 0, ROW: 24
//  Initialization of Dual Port LUT ram now done through explicit
// parameter setting.
RAM64X1D#(.INIT(64'h0000000000000000))RAM64X1D_instance_3608(.WCLK(CLK), .WE(wea_0), 
  .A0(ADDRA[0]), .A1(ADDRA[1]), .A2(ADDRA[2]), .A3(ADDRA[3]), .A4(ADDRA[4]), .A5(ADDRA[5]), 
  .D(DINA[24]), .SPO(pre_douta_0[24]), .DPRA0(ADDRB[0]), .DPRA1(ADDRB[1]), .DPRA2(ADDRB[2]), 
  .DPRA3(ADDRB[3]), .DPRA4(ADDRB[4]), .DPRA5(ADDRB[5]), .DPO(pre_doutb_0[24]));
// Memory array element: COL: 0, ROW: 25
//  Initialization of Dual Port LUT ram now done through explicit
// parameter setting.
RAM64X1D#(.INIT(64'h0000000000000000))RAM64X1D_instance_3609(.WCLK(CLK), .WE(wea_0), 
  .A0(ADDRA[0]), .A1(ADDRA[1]), .A2(ADDRA[2]), .A3(ADDRA[3]), .A4(ADDRA[4]), .A5(ADDRA[5]), 
  .D(DINA[25]), .SPO(pre_douta_0[25]), .DPRA0(ADDRB[0]), .DPRA1(ADDRB[1]), .DPRA2(ADDRB[2]), 
  .DPRA3(ADDRB[3]), .DPRA4(ADDRB[4]), .DPRA5(ADDRB[5]), .DPO(pre_doutb_0[25]));
// Memory array element: COL: 0, ROW: 26
//  Initialization of Dual Port LUT ram now done through explicit
// parameter setting.
RAM64X1D#(.INIT(64'h0000000000000000))RAM64X1D_instance_3610(.WCLK(CLK), .WE(wea_0), 
  .A0(ADDRA[0]), .A1(ADDRA[1]), .A2(ADDRA[2]), .A3(ADDRA[3]), .A4(ADDRA[4]), .A5(ADDRA[5]), 
  .D(DINA[26]), .SPO(pre_douta_0[26]), .DPRA0(ADDRB[0]), .DPRA1(ADDRB[1]), .DPRA2(ADDRB[2]), 
  .DPRA3(ADDRB[3]), .DPRA4(ADDRB[4]), .DPRA5(ADDRB[5]), .DPO(pre_doutb_0[26]));
// Memory array element: COL: 0, ROW: 27
//  Initialization of Dual Port LUT ram now done through explicit
// parameter setting.
RAM64X1D#(.INIT(64'h0000000000000000))RAM64X1D_instance_3611(.WCLK(CLK), .WE(wea_0), 
  .A0(ADDRA[0]), .A1(ADDRA[1]), .A2(ADDRA[2]), .A3(ADDRA[3]), .A4(ADDRA[4]), .A5(ADDRA[5]), 
  .D(DINA[27]), .SPO(pre_douta_0[27]), .DPRA0(ADDRB[0]), .DPRA1(ADDRB[1]), .DPRA2(ADDRB[2]), 
  .DPRA3(ADDRB[3]), .DPRA4(ADDRB[4]), .DPRA5(ADDRB[5]), .DPO(pre_doutb_0[27]));
// Memory array element: COL: 0, ROW: 28
//  Initialization of Dual Port LUT ram now done through explicit
// parameter setting.
RAM64X1D#(.INIT(64'h0000000000000000))RAM64X1D_instance_3612(.WCLK(CLK), .WE(wea_0), 
  .A0(ADDRA[0]), .A1(ADDRA[1]), .A2(ADDRA[2]), .A3(ADDRA[3]), .A4(ADDRA[4]), .A5(ADDRA[5]), 
  .D(DINA[28]), .SPO(pre_douta_0[28]), .DPRA0(ADDRB[0]), .DPRA1(ADDRB[1]), .DPRA2(ADDRB[2]), 
  .DPRA3(ADDRB[3]), .DPRA4(ADDRB[4]), .DPRA5(ADDRB[5]), .DPO(pre_doutb_0[28]));
// Memory array element: COL: 0, ROW: 29
//  Initialization of Dual Port LUT ram now done through explicit
// parameter setting.
RAM64X1D#(.INIT(64'h0000000000000000))RAM64X1D_instance_3613(.WCLK(CLK), .WE(wea_0), 
  .A0(ADDRA[0]), .A1(ADDRA[1]), .A2(ADDRA[2]), .A3(ADDRA[3]), .A4(ADDRA[4]), .A5(ADDRA[5]), 
  .D(DINA[29]), .SPO(pre_douta_0[29]), .DPRA0(ADDRB[0]), .DPRA1(ADDRB[1]), .DPRA2(ADDRB[2]), 
  .DPRA3(ADDRB[3]), .DPRA4(ADDRB[4]), .DPRA5(ADDRB[5]), .DPO(pre_doutb_0[29]));
// Memory array element: COL: 0, ROW: 30
//  Initialization of Dual Port LUT ram now done through explicit
// parameter setting.
RAM64X1D#(.INIT(64'h0000000000000000))RAM64X1D_instance_3614(.WCLK(CLK), .WE(wea_0), 
  .A0(ADDRA[0]), .A1(ADDRA[1]), .A2(ADDRA[2]), .A3(ADDRA[3]), .A4(ADDRA[4]), .A5(ADDRA[5]), 
  .D(DINA[30]), .SPO(pre_douta_0[30]), .DPRA0(ADDRB[0]), .DPRA1(ADDRB[1]), .DPRA2(ADDRB[2]), 
  .DPRA3(ADDRB[3]), .DPRA4(ADDRB[4]), .DPRA5(ADDRB[5]), .DPO(pre_doutb_0[30]));
// Memory array element: COL: 0, ROW: 31
//  Initialization of Dual Port LUT ram now done through explicit
// parameter setting.
RAM64X1D#(.INIT(64'h0000000000000000))RAM64X1D_instance_3615(.WCLK(CLK), .WE(wea_0), 
  .A0(ADDRA[0]), .A1(ADDRA[1]), .A2(ADDRA[2]), .A3(ADDRA[3]), .A4(ADDRA[4]), .A5(ADDRA[5]), 
  .D(DINA[31]), .SPO(pre_douta_0[31]), .DPRA0(ADDRB[0]), .DPRA1(ADDRB[1]), .DPRA2(ADDRB[2]), 
  .DPRA3(ADDRB[3]), .DPRA4(ADDRB[4]), .DPRA5(ADDRB[5]), .DPO(pre_doutb_0[31]));
// Memory array element: COL: 1, ROW: 0
//  Initialization of Dual Port LUT ram now done through explicit
// parameter setting.
RAM64X1D#(.INIT(64'h0000000000000000))RAM64X1D_instance_3616(.WCLK(CLK), .WE(wea_1), 
  .A0(ADDRA[0]), .A1(ADDRA[1]), .A2(ADDRA[2]), .A3(ADDRA[3]), .A4(ADDRA[4]), .A5(ADDRA[5]), 
  .D(DINA[0]), .SPO(pre_douta_1[0]), .DPRA0(ADDRB[0]), .DPRA1(ADDRB[1]), .DPRA2(ADDRB[2]), 
  .DPRA3(ADDRB[3]), .DPRA4(ADDRB[4]), .DPRA5(ADDRB[5]), .DPO(pre_doutb_1[0]));
// Memory array element: COL: 1, ROW: 1
//  Initialization of Dual Port LUT ram now done through explicit
// parameter setting.
RAM64X1D#(.INIT(64'h0000000000000000))RAM64X1D_instance_3617(.WCLK(CLK), .WE(wea_1), 
  .A0(ADDRA[0]), .A1(ADDRA[1]), .A2(ADDRA[2]), .A3(ADDRA[3]), .A4(ADDRA[4]), .A5(ADDRA[5]), 
  .D(DINA[1]), .SPO(pre_douta_1[1]), .DPRA0(ADDRB[0]), .DPRA1(ADDRB[1]), .DPRA2(ADDRB[2]), 
  .DPRA3(ADDRB[3]), .DPRA4(ADDRB[4]), .DPRA5(ADDRB[5]), .DPO(pre_doutb_1[1]));
// Memory array element: COL: 1, ROW: 2
//  Initialization of Dual Port LUT ram now done through explicit
// parameter setting.
RAM64X1D#(.INIT(64'h0000000000000000))RAM64X1D_instance_3618(.WCLK(CLK), .WE(wea_1), 
  .A0(ADDRA[0]), .A1(ADDRA[1]), .A2(ADDRA[2]), .A3(ADDRA[3]), .A4(ADDRA[4]), .A5(ADDRA[5]), 
  .D(DINA[2]), .SPO(pre_douta_1[2]), .DPRA0(ADDRB[0]), .DPRA1(ADDRB[1]), .DPRA2(ADDRB[2]), 
  .DPRA3(ADDRB[3]), .DPRA4(ADDRB[4]), .DPRA5(ADDRB[5]), .DPO(pre_doutb_1[2]));
// Memory array element: COL: 1, ROW: 3
//  Initialization of Dual Port LUT ram now done through explicit
// parameter setting.
RAM64X1D#(.INIT(64'h0000000000000000))RAM64X1D_instance_3619(.WCLK(CLK), .WE(wea_1), 
  .A0(ADDRA[0]), .A1(ADDRA[1]), .A2(ADDRA[2]), .A3(ADDRA[3]), .A4(ADDRA[4]), .A5(ADDRA[5]), 
  .D(DINA[3]), .SPO(pre_douta_1[3]), .DPRA0(ADDRB[0]), .DPRA1(ADDRB[1]), .DPRA2(ADDRB[2]), 
  .DPRA3(ADDRB[3]), .DPRA4(ADDRB[4]), .DPRA5(ADDRB[5]), .DPO(pre_doutb_1[3]));
// Memory array element: COL: 1, ROW: 4
//  Initialization of Dual Port LUT ram now done through explicit
// parameter setting.
RAM64X1D#(.INIT(64'h0000000000000000))RAM64X1D_instance_3620(.WCLK(CLK), .WE(wea_1), 
  .A0(ADDRA[0]), .A1(ADDRA[1]), .A2(ADDRA[2]), .A3(ADDRA[3]), .A4(ADDRA[4]), .A5(ADDRA[5]), 
  .D(DINA[4]), .SPO(pre_douta_1[4]), .DPRA0(ADDRB[0]), .DPRA1(ADDRB[1]), .DPRA2(ADDRB[2]), 
  .DPRA3(ADDRB[3]), .DPRA4(ADDRB[4]), .DPRA5(ADDRB[5]), .DPO(pre_doutb_1[4]));
// Memory array element: COL: 1, ROW: 5
//  Initialization of Dual Port LUT ram now done through explicit
// parameter setting.
RAM64X1D#(.INIT(64'h0000000000000000))RAM64X1D_instance_3621(.WCLK(CLK), .WE(wea_1), 
  .A0(ADDRA[0]), .A1(ADDRA[1]), .A2(ADDRA[2]), .A3(ADDRA[3]), .A4(ADDRA[4]), .A5(ADDRA[5]), 
  .D(DINA[5]), .SPO(pre_douta_1[5]), .DPRA0(ADDRB[0]), .DPRA1(ADDRB[1]), .DPRA2(ADDRB[2]), 
  .DPRA3(ADDRB[3]), .DPRA4(ADDRB[4]), .DPRA5(ADDRB[5]), .DPO(pre_doutb_1[5]));
// Memory array element: COL: 1, ROW: 6
//  Initialization of Dual Port LUT ram now done through explicit
// parameter setting.
RAM64X1D#(.INIT(64'h0000000000000000))RAM64X1D_instance_3622(.WCLK(CLK), .WE(wea_1), 
  .A0(ADDRA[0]), .A1(ADDRA[1]), .A2(ADDRA[2]), .A3(ADDRA[3]), .A4(ADDRA[4]), .A5(ADDRA[5]), 
  .D(DINA[6]), .SPO(pre_douta_1[6]), .DPRA0(ADDRB[0]), .DPRA1(ADDRB[1]), .DPRA2(ADDRB[2]), 
  .DPRA3(ADDRB[3]), .DPRA4(ADDRB[4]), .DPRA5(ADDRB[5]), .DPO(pre_doutb_1[6]));
// Memory array element: COL: 1, ROW: 7
//  Initialization of Dual Port LUT ram now done through explicit
// parameter setting.
RAM64X1D#(.INIT(64'h0000000000000000))RAM64X1D_instance_3623(.WCLK(CLK), .WE(wea_1), 
  .A0(ADDRA[0]), .A1(ADDRA[1]), .A2(ADDRA[2]), .A3(ADDRA[3]), .A4(ADDRA[4]), .A5(ADDRA[5]), 
  .D(DINA[7]), .SPO(pre_douta_1[7]), .DPRA0(ADDRB[0]), .DPRA1(ADDRB[1]), .DPRA2(ADDRB[2]), 
  .DPRA3(ADDRB[3]), .DPRA4(ADDRB[4]), .DPRA5(ADDRB[5]), .DPO(pre_doutb_1[7]));
// Memory array element: COL: 1, ROW: 8
//  Initialization of Dual Port LUT ram now done through explicit
// parameter setting.
RAM64X1D#(.INIT(64'h0000000000000000))RAM64X1D_instance_3624(.WCLK(CLK), .WE(wea_1), 
  .A0(ADDRA[0]), .A1(ADDRA[1]), .A2(ADDRA[2]), .A3(ADDRA[3]), .A4(ADDRA[4]), .A5(ADDRA[5]), 
  .D(DINA[8]), .SPO(pre_douta_1[8]), .DPRA0(ADDRB[0]), .DPRA1(ADDRB[1]), .DPRA2(ADDRB[2]), 
  .DPRA3(ADDRB[3]), .DPRA4(ADDRB[4]), .DPRA5(ADDRB[5]), .DPO(pre_doutb_1[8]));
// Memory array element: COL: 1, ROW: 9
//  Initialization of Dual Port LUT ram now done through explicit
// parameter setting.
RAM64X1D#(.INIT(64'h0000000000000000))RAM64X1D_instance_3625(.WCLK(CLK), .WE(wea_1), 
  .A0(ADDRA[0]), .A1(ADDRA[1]), .A2(ADDRA[2]), .A3(ADDRA[3]), .A4(ADDRA[4]), .A5(ADDRA[5]), 
  .D(DINA[9]), .SPO(pre_douta_1[9]), .DPRA0(ADDRB[0]), .DPRA1(ADDRB[1]), .DPRA2(ADDRB[2]), 
  .DPRA3(ADDRB[3]), .DPRA4(ADDRB[4]), .DPRA5(ADDRB[5]), .DPO(pre_doutb_1[9]));
// Memory array element: COL: 1, ROW: 10
//  Initialization of Dual Port LUT ram now done through explicit
// parameter setting.
RAM64X1D#(.INIT(64'h0000000000000000))RAM64X1D_instance_3626(.WCLK(CLK), .WE(wea_1), 
  .A0(ADDRA[0]), .A1(ADDRA[1]), .A2(ADDRA[2]), .A3(ADDRA[3]), .A4(ADDRA[4]), .A5(ADDRA[5]), 
  .D(DINA[10]), .SPO(pre_douta_1[10]), .DPRA0(ADDRB[0]), .DPRA1(ADDRB[1]), .DPRA2(ADDRB[2]), 
  .DPRA3(ADDRB[3]), .DPRA4(ADDRB[4]), .DPRA5(ADDRB[5]), .DPO(pre_doutb_1[10]));
// Memory array element: COL: 1, ROW: 11
//  Initialization of Dual Port LUT ram now done through explicit
// parameter setting.
RAM64X1D#(.INIT(64'h0000000000000000))RAM64X1D_instance_3627(.WCLK(CLK), .WE(wea_1), 
  .A0(ADDRA[0]), .A1(ADDRA[1]), .A2(ADDRA[2]), .A3(ADDRA[3]), .A4(ADDRA[4]), .A5(ADDRA[5]), 
  .D(DINA[11]), .SPO(pre_douta_1[11]), .DPRA0(ADDRB[0]), .DPRA1(ADDRB[1]), .DPRA2(ADDRB[2]), 
  .DPRA3(ADDRB[3]), .DPRA4(ADDRB[4]), .DPRA5(ADDRB[5]), .DPO(pre_doutb_1[11]));
// Memory array element: COL: 1, ROW: 12
//  Initialization of Dual Port LUT ram now done through explicit
// parameter setting.
RAM64X1D#(.INIT(64'h0000000000000000))RAM64X1D_instance_3628(.WCLK(CLK), .WE(wea_1), 
  .A0(ADDRA[0]), .A1(ADDRA[1]), .A2(ADDRA[2]), .A3(ADDRA[3]), .A4(ADDRA[4]), .A5(ADDRA[5]), 
  .D(DINA[12]), .SPO(pre_douta_1[12]), .DPRA0(ADDRB[0]), .DPRA1(ADDRB[1]), .DPRA2(ADDRB[2]), 
  .DPRA3(ADDRB[3]), .DPRA4(ADDRB[4]), .DPRA5(ADDRB[5]), .DPO(pre_doutb_1[12]));
// Memory array element: COL: 1, ROW: 13
//  Initialization of Dual Port LUT ram now done through explicit
// parameter setting.
RAM64X1D#(.INIT(64'h0000000000000000))RAM64X1D_instance_3629(.WCLK(CLK), .WE(wea_1), 
  .A0(ADDRA[0]), .A1(ADDRA[1]), .A2(ADDRA[2]), .A3(ADDRA[3]), .A4(ADDRA[4]), .A5(ADDRA[5]), 
  .D(DINA[13]), .SPO(pre_douta_1[13]), .DPRA0(ADDRB[0]), .DPRA1(ADDRB[1]), .DPRA2(ADDRB[2]), 
  .DPRA3(ADDRB[3]), .DPRA4(ADDRB[4]), .DPRA5(ADDRB[5]), .DPO(pre_doutb_1[13]));
// Memory array element: COL: 1, ROW: 14
//  Initialization of Dual Port LUT ram now done through explicit
// parameter setting.
RAM64X1D#(.INIT(64'h0000000000000000))RAM64X1D_instance_3630(.WCLK(CLK), .WE(wea_1), 
  .A0(ADDRA[0]), .A1(ADDRA[1]), .A2(ADDRA[2]), .A3(ADDRA[3]), .A4(ADDRA[4]), .A5(ADDRA[5]), 
  .D(DINA[14]), .SPO(pre_douta_1[14]), .DPRA0(ADDRB[0]), .DPRA1(ADDRB[1]), .DPRA2(ADDRB[2]), 
  .DPRA3(ADDRB[3]), .DPRA4(ADDRB[4]), .DPRA5(ADDRB[5]), .DPO(pre_doutb_1[14]));
// Memory array element: COL: 1, ROW: 15
//  Initialization of Dual Port LUT ram now done through explicit
// parameter setting.
RAM64X1D#(.INIT(64'h0000000000000000))RAM64X1D_instance_3631(.WCLK(CLK), .WE(wea_1), 
  .A0(ADDRA[0]), .A1(ADDRA[1]), .A2(ADDRA[2]), .A3(ADDRA[3]), .A4(ADDRA[4]), .A5(ADDRA[5]), 
  .D(DINA[15]), .SPO(pre_douta_1[15]), .DPRA0(ADDRB[0]), .DPRA1(ADDRB[1]), .DPRA2(ADDRB[2]), 
  .DPRA3(ADDRB[3]), .DPRA4(ADDRB[4]), .DPRA5(ADDRB[5]), .DPO(pre_doutb_1[15]));
// Memory array element: COL: 1, ROW: 16
//  Initialization of Dual Port LUT ram now done through explicit
// parameter setting.
RAM64X1D#(.INIT(64'h0000000000000000))RAM64X1D_instance_3632(.WCLK(CLK), .WE(wea_1), 
  .A0(ADDRA[0]), .A1(ADDRA[1]), .A2(ADDRA[2]), .A3(ADDRA[3]), .A4(ADDRA[4]), .A5(ADDRA[5]), 
  .D(DINA[16]), .SPO(pre_douta_1[16]), .DPRA0(ADDRB[0]), .DPRA1(ADDRB[1]), .DPRA2(ADDRB[2]), 
  .DPRA3(ADDRB[3]), .DPRA4(ADDRB[4]), .DPRA5(ADDRB[5]), .DPO(pre_doutb_1[16]));
// Memory array element: COL: 1, ROW: 17
//  Initialization of Dual Port LUT ram now done through explicit
// parameter setting.
RAM64X1D#(.INIT(64'h0000000000000000))RAM64X1D_instance_3633(.WCLK(CLK), .WE(wea_1), 
  .A0(ADDRA[0]), .A1(ADDRA[1]), .A2(ADDRA[2]), .A3(ADDRA[3]), .A4(ADDRA[4]), .A5(ADDRA[5]), 
  .D(DINA[17]), .SPO(pre_douta_1[17]), .DPRA0(ADDRB[0]), .DPRA1(ADDRB[1]), .DPRA2(ADDRB[2]), 
  .DPRA3(ADDRB[3]), .DPRA4(ADDRB[4]), .DPRA5(ADDRB[5]), .DPO(pre_doutb_1[17]));
// Memory array element: COL: 1, ROW: 18
//  Initialization of Dual Port LUT ram now done through explicit
// parameter setting.
RAM64X1D#(.INIT(64'h0000000000000000))RAM64X1D_instance_3634(.WCLK(CLK), .WE(wea_1), 
  .A0(ADDRA[0]), .A1(ADDRA[1]), .A2(ADDRA[2]), .A3(ADDRA[3]), .A4(ADDRA[4]), .A5(ADDRA[5]), 
  .D(DINA[18]), .SPO(pre_douta_1[18]), .DPRA0(ADDRB[0]), .DPRA1(ADDRB[1]), .DPRA2(ADDRB[2]), 
  .DPRA3(ADDRB[3]), .DPRA4(ADDRB[4]), .DPRA5(ADDRB[5]), .DPO(pre_doutb_1[18]));
// Memory array element: COL: 1, ROW: 19
//  Initialization of Dual Port LUT ram now done through explicit
// parameter setting.
RAM64X1D#(.INIT(64'h0000000000000000))RAM64X1D_instance_3635(.WCLK(CLK), .WE(wea_1), 
  .A0(ADDRA[0]), .A1(ADDRA[1]), .A2(ADDRA[2]), .A3(ADDRA[3]), .A4(ADDRA[4]), .A5(ADDRA[5]), 
  .D(DINA[19]), .SPO(pre_douta_1[19]), .DPRA0(ADDRB[0]), .DPRA1(ADDRB[1]), .DPRA2(ADDRB[2]), 
  .DPRA3(ADDRB[3]), .DPRA4(ADDRB[4]), .DPRA5(ADDRB[5]), .DPO(pre_doutb_1[19]));
// Memory array element: COL: 1, ROW: 20
//  Initialization of Dual Port LUT ram now done through explicit
// parameter setting.
RAM64X1D#(.INIT(64'h0000000000000000))RAM64X1D_instance_3636(.WCLK(CLK), .WE(wea_1), 
  .A0(ADDRA[0]), .A1(ADDRA[1]), .A2(ADDRA[2]), .A3(ADDRA[3]), .A4(ADDRA[4]), .A5(ADDRA[5]), 
  .D(DINA[20]), .SPO(pre_douta_1[20]), .DPRA0(ADDRB[0]), .DPRA1(ADDRB[1]), .DPRA2(ADDRB[2]), 
  .DPRA3(ADDRB[3]), .DPRA4(ADDRB[4]), .DPRA5(ADDRB[5]), .DPO(pre_doutb_1[20]));
// Memory array element: COL: 1, ROW: 21
//  Initialization of Dual Port LUT ram now done through explicit
// parameter setting.
RAM64X1D#(.INIT(64'h0000000000000000))RAM64X1D_instance_3637(.WCLK(CLK), .WE(wea_1), 
  .A0(ADDRA[0]), .A1(ADDRA[1]), .A2(ADDRA[2]), .A3(ADDRA[3]), .A4(ADDRA[4]), .A5(ADDRA[5]), 
  .D(DINA[21]), .SPO(pre_douta_1[21]), .DPRA0(ADDRB[0]), .DPRA1(ADDRB[1]), .DPRA2(ADDRB[2]), 
  .DPRA3(ADDRB[3]), .DPRA4(ADDRB[4]), .DPRA5(ADDRB[5]), .DPO(pre_doutb_1[21]));
// Memory array element: COL: 1, ROW: 22
//  Initialization of Dual Port LUT ram now done through explicit
// parameter setting.
RAM64X1D#(.INIT(64'h0000000000000000))RAM64X1D_instance_3638(.WCLK(CLK), .WE(wea_1), 
  .A0(ADDRA[0]), .A1(ADDRA[1]), .A2(ADDRA[2]), .A3(ADDRA[3]), .A4(ADDRA[4]), .A5(ADDRA[5]), 
  .D(DINA[22]), .SPO(pre_douta_1[22]), .DPRA0(ADDRB[0]), .DPRA1(ADDRB[1]), .DPRA2(ADDRB[2]), 
  .DPRA3(ADDRB[3]), .DPRA4(ADDRB[4]), .DPRA5(ADDRB[5]), .DPO(pre_doutb_1[22]));
// Memory array element: COL: 1, ROW: 23
//  Initialization of Dual Port LUT ram now done through explicit
// parameter setting.
RAM64X1D#(.INIT(64'h0000000000000000))RAM64X1D_instance_3639(.WCLK(CLK), .WE(wea_1), 
  .A0(ADDRA[0]), .A1(ADDRA[1]), .A2(ADDRA[2]), .A3(ADDRA[3]), .A4(ADDRA[4]), .A5(ADDRA[5]), 
  .D(DINA[23]), .SPO(pre_douta_1[23]), .DPRA0(ADDRB[0]), .DPRA1(ADDRB[1]), .DPRA2(ADDRB[2]), 
  .DPRA3(ADDRB[3]), .DPRA4(ADDRB[4]), .DPRA5(ADDRB[5]), .DPO(pre_doutb_1[23]));
// Memory array element: COL: 1, ROW: 24
//  Initialization of Dual Port LUT ram now done through explicit
// parameter setting.
RAM64X1D#(.INIT(64'h0000000000000000))RAM64X1D_instance_3640(.WCLK(CLK), .WE(wea_1), 
  .A0(ADDRA[0]), .A1(ADDRA[1]), .A2(ADDRA[2]), .A3(ADDRA[3]), .A4(ADDRA[4]), .A5(ADDRA[5]), 
  .D(DINA[24]), .SPO(pre_douta_1[24]), .DPRA0(ADDRB[0]), .DPRA1(ADDRB[1]), .DPRA2(ADDRB[2]), 
  .DPRA3(ADDRB[3]), .DPRA4(ADDRB[4]), .DPRA5(ADDRB[5]), .DPO(pre_doutb_1[24]));
// Memory array element: COL: 1, ROW: 25
//  Initialization of Dual Port LUT ram now done through explicit
// parameter setting.
RAM64X1D#(.INIT(64'h0000000000000000))RAM64X1D_instance_3641(.WCLK(CLK), .WE(wea_1), 
  .A0(ADDRA[0]), .A1(ADDRA[1]), .A2(ADDRA[2]), .A3(ADDRA[3]), .A4(ADDRA[4]), .A5(ADDRA[5]), 
  .D(DINA[25]), .SPO(pre_douta_1[25]), .DPRA0(ADDRB[0]), .DPRA1(ADDRB[1]), .DPRA2(ADDRB[2]), 
  .DPRA3(ADDRB[3]), .DPRA4(ADDRB[4]), .DPRA5(ADDRB[5]), .DPO(pre_doutb_1[25]));
// Memory array element: COL: 1, ROW: 26
//  Initialization of Dual Port LUT ram now done through explicit
// parameter setting.
RAM64X1D#(.INIT(64'h0000000000000000))RAM64X1D_instance_3642(.WCLK(CLK), .WE(wea_1), 
  .A0(ADDRA[0]), .A1(ADDRA[1]), .A2(ADDRA[2]), .A3(ADDRA[3]), .A4(ADDRA[4]), .A5(ADDRA[5]), 
  .D(DINA[26]), .SPO(pre_douta_1[26]), .DPRA0(ADDRB[0]), .DPRA1(ADDRB[1]), .DPRA2(ADDRB[2]), 
  .DPRA3(ADDRB[3]), .DPRA4(ADDRB[4]), .DPRA5(ADDRB[5]), .DPO(pre_doutb_1[26]));
// Memory array element: COL: 1, ROW: 27
//  Initialization of Dual Port LUT ram now done through explicit
// parameter setting.
RAM64X1D#(.INIT(64'h0000000000000000))RAM64X1D_instance_3643(.WCLK(CLK), .WE(wea_1), 
  .A0(ADDRA[0]), .A1(ADDRA[1]), .A2(ADDRA[2]), .A3(ADDRA[3]), .A4(ADDRA[4]), .A5(ADDRA[5]), 
  .D(DINA[27]), .SPO(pre_douta_1[27]), .DPRA0(ADDRB[0]), .DPRA1(ADDRB[1]), .DPRA2(ADDRB[2]), 
  .DPRA3(ADDRB[3]), .DPRA4(ADDRB[4]), .DPRA5(ADDRB[5]), .DPO(pre_doutb_1[27]));
// Memory array element: COL: 1, ROW: 28
//  Initialization of Dual Port LUT ram now done through explicit
// parameter setting.
RAM64X1D#(.INIT(64'h0000000000000000))RAM64X1D_instance_3644(.WCLK(CLK), .WE(wea_1), 
  .A0(ADDRA[0]), .A1(ADDRA[1]), .A2(ADDRA[2]), .A3(ADDRA[3]), .A4(ADDRA[4]), .A5(ADDRA[5]), 
  .D(DINA[28]), .SPO(pre_douta_1[28]), .DPRA0(ADDRB[0]), .DPRA1(ADDRB[1]), .DPRA2(ADDRB[2]), 
  .DPRA3(ADDRB[3]), .DPRA4(ADDRB[4]), .DPRA5(ADDRB[5]), .DPO(pre_doutb_1[28]));
// Memory array element: COL: 1, ROW: 29
//  Initialization of Dual Port LUT ram now done through explicit
// parameter setting.
RAM64X1D#(.INIT(64'h0000000000000000))RAM64X1D_instance_3645(.WCLK(CLK), .WE(wea_1), 
  .A0(ADDRA[0]), .A1(ADDRA[1]), .A2(ADDRA[2]), .A3(ADDRA[3]), .A4(ADDRA[4]), .A5(ADDRA[5]), 
  .D(DINA[29]), .SPO(pre_douta_1[29]), .DPRA0(ADDRB[0]), .DPRA1(ADDRB[1]), .DPRA2(ADDRB[2]), 
  .DPRA3(ADDRB[3]), .DPRA4(ADDRB[4]), .DPRA5(ADDRB[5]), .DPO(pre_doutb_1[29]));
// Memory array element: COL: 1, ROW: 30
//  Initialization of Dual Port LUT ram now done through explicit
// parameter setting.
RAM64X1D#(.INIT(64'h0000000000000000))RAM64X1D_instance_3646(.WCLK(CLK), .WE(wea_1), 
  .A0(ADDRA[0]), .A1(ADDRA[1]), .A2(ADDRA[2]), .A3(ADDRA[3]), .A4(ADDRA[4]), .A5(ADDRA[5]), 
  .D(DINA[30]), .SPO(pre_douta_1[30]), .DPRA0(ADDRB[0]), .DPRA1(ADDRB[1]), .DPRA2(ADDRB[2]), 
  .DPRA3(ADDRB[3]), .DPRA4(ADDRB[4]), .DPRA5(ADDRB[5]), .DPO(pre_doutb_1[30]));
// Memory array element: COL: 1, ROW: 31
//  Initialization of Dual Port LUT ram now done through explicit
// parameter setting.
RAM64X1D#(.INIT(64'h0000000000000000))RAM64X1D_instance_3647(.WCLK(CLK), .WE(wea_1), 
  .A0(ADDRA[0]), .A1(ADDRA[1]), .A2(ADDRA[2]), .A3(ADDRA[3]), .A4(ADDRA[4]), .A5(ADDRA[5]), 
  .D(DINA[31]), .SPO(pre_douta_1[31]), .DPRA0(ADDRB[0]), .DPRA1(ADDRB[1]), .DPRA2(ADDRB[2]), 
  .DPRA3(ADDRB[3]), .DPRA4(ADDRB[4]), .DPRA5(ADDRB[5]), .DPO(pre_doutb_1[31]));
endmodule



module medianRow6_structuralmemory_313b77d4_(CLK_u102, bus_2e6fef76_, bus_0257f18a_, bus_44993a90_, bus_7557a073_, bus_177e5519_, bus_0b59d15f_, bus_55d61223_, bus_5602bec8_, bus_24597958_, bus_529ca8ca_, bus_19b156a5_, bus_6cb408ed_, bus_10e1ac9b_);
input		CLK_u102;
input		bus_2e6fef76_;
input	[31:0]	bus_0257f18a_;
input	[2:0]	bus_44993a90_;
input		bus_7557a073_;
input	[31:0]	bus_177e5519_;
input	[2:0]	bus_0b59d15f_;
input		bus_55d61223_;
input		bus_5602bec8_;
input	[31:0]	bus_24597958_;
output	[31:0]	bus_529ca8ca_;
output		bus_19b156a5_;
output	[31:0]	bus_6cb408ed_;
output		bus_10e1ac9b_;
wire		or_0dc4f74e_u0;
wire	[31:0]	bus_78e900b5_;
wire	[31:0]	bus_31e2809a_;
wire		and_19c286f1_u0;
wire		not_349b99a7_u0;
reg		logicalMem_72a172ae_we_delay0_u0=1'h0;
wire		or_7523489e_u0;
assign or_0dc4f74e_u0=bus_55d61223_|bus_5602bec8_;
medianRow6_forge_memory_101x32_165 medianRow6_forge_memory_101x32_165_instance0(.CLK(CLK_u102), 
  .ENA(or_0dc4f74e_u0), .WEA(bus_5602bec8_), .DINA(bus_24597958_), .ADDRA(bus_177e5519_), 
  .DOUTA(bus_31e2809a_), .DONEA(), .ENB(bus_7557a073_), .ADDRB(bus_0257f18a_), .DOUTB(bus_78e900b5_), 
  .DONEB());
assign and_19c286f1_u0=bus_55d61223_&not_349b99a7_u0;
assign bus_529ca8ca_=bus_78e900b5_;
assign bus_19b156a5_=bus_7557a073_;
assign bus_6cb408ed_=bus_31e2809a_;
assign bus_10e1ac9b_=or_7523489e_u0;
assign not_349b99a7_u0=~bus_5602bec8_;
always @(posedge CLK_u102 or posedge bus_2e6fef76_)
begin
if (bus_2e6fef76_)
logicalMem_72a172ae_we_delay0_u0<=1'h0;
else logicalMem_72a172ae_we_delay0_u0<=bus_5602bec8_;
end
assign or_7523489e_u0=and_19c286f1_u0|logicalMem_72a172ae_we_delay0_u0;
endmodule


