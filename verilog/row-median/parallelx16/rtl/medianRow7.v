// __  ___ __ ___  _ __   ___  ___ 
// \ \/ / '__/ _ \| '_ \ / _ \/ __|
//  >  <| | | (_) | | | | (_) \__ \
// /_/\_\_|  \___/|_| |_|\___/|___/
// 
// Xronos synthesizer version
// Run date: Fri 23 Feb 2018 14:28:49 +0000
// 

module medianRow7(RESET, median_RDY, CLK, in1_SEND, median_SEND, in1_DATA, median_DATA, median_COUNT, in1_ACK, median_ACK, in1_COUNT);
wire		compute_median_go;
input		RESET;
input		median_RDY;
wire		compute_median_done;
input		CLK;
input		in1_SEND;
output		median_SEND;
input	[7:0]	in1_DATA;
wire		receive_go;
output	[7:0]	median_DATA;
output	[15:0]	median_COUNT;
output		in1_ACK;
input		median_ACK;
wire		receive_done;
input	[15:0]	in1_COUNT;
wire	[31:0]	bus_0af85e9e_;
wire		bus_0352f08f_;
wire	[31:0]	bus_312a8c9c_;
wire		bus_3fdeba12_;
wire	[2:0]	bus_0338a3ef_;
wire	[31:0]	bus_66e94aa4_;
wire		bus_6ee8bc93_;
wire	[31:0]	bus_70215d15_;
wire		bus_2098e760_;
wire		bus_6680bcd2_;
wire		bus_6b187c32_;
wire	[2:0]	bus_54fccda5_;
wire	[31:0]	bus_44894780_;
wire		bus_60573fee_;
wire	[31:0]	bus_242c20e4_;
wire	[31:0]	bus_0490f155_;
wire		bus_435c5a1a_;
wire		bus_7ffa3ba1_;
wire		receive_u331;
wire	[2:0]	receive_u334;
wire		receive;
wire		receive_u335;
wire	[31:0]	receive_u333;
wire	[31:0]	receive_u332;
wire	[31:0]	receive_u330;
wire		medianRow7_receive_instance_DONE;
wire		bus_6a69f8cf_;
wire	[31:0]	bus_7014d17a_;
wire		bus_3fbd0408_;
wire	[31:0]	bus_79c19c10_;
wire		bus_3cee3342_;
wire	[31:0]	compute_median_u776;
wire	[31:0]	compute_median_u779;
wire		compute_median;
wire	[31:0]	compute_median_u770;
wire		medianRow7_compute_median_instance_DONE;
wire		compute_median_u782;
wire	[15:0]	compute_median_u781;
wire	[31:0]	compute_median_u772;
wire	[31:0]	compute_median_u775;
wire	[2:0]	compute_median_u773;
wire		compute_median_u774;
wire		compute_median_u778;
wire	[2:0]	compute_median_u777;
wire	[2:0]	compute_median_u780;
wire		compute_median_u771;
wire	[7:0]	compute_median_u783;
wire		scheduler_u841;
wire		medianRow7_scheduler_instance_DONE;
wire		scheduler_u843;
wire		scheduler;
wire		scheduler_u842;
wire		bus_06d04b9d_;
wire		bus_4c9dfdd6_;
assign compute_median_go=scheduler_u842;
assign compute_median_done=bus_2098e760_;
assign median_SEND=compute_median_u782;
assign receive_go=scheduler_u843;
assign median_DATA=compute_median_u783;
assign median_COUNT=compute_median_u781;
assign in1_ACK=receive_u335;
assign receive_done=bus_3cee3342_;
medianRow7_stateVar_i medianRow7_stateVar_i_1(.bus_0a1dc326_(CLK), .bus_7ba5ca21_(bus_4c9dfdd6_), 
  .bus_1224a3f1_(receive), .bus_23656e4b_(receive_u330), .bus_7e08435a_(compute_median), 
  .bus_2234c985_(32'h0), .bus_0af85e9e_(bus_0af85e9e_));
medianRow7_simplememoryreferee_545f7316_ medianRow7_simplememoryreferee_545f7316__1(.bus_24ff83b8_(CLK), 
  .bus_4d80d7f5_(bus_4c9dfdd6_), .bus_6b63773b_(bus_3fbd0408_), .bus_18d1c3f2_(bus_79c19c10_), 
  .bus_2008b291_(compute_median_u771), .bus_6834c0a6_(compute_median_u772), .bus_087e8aef_(3'h1), 
  .bus_312a8c9c_(bus_312a8c9c_), .bus_70215d15_(bus_70215d15_), .bus_0352f08f_(bus_0352f08f_), 
  .bus_3fdeba12_(bus_3fdeba12_), .bus_0338a3ef_(bus_0338a3ef_), .bus_66e94aa4_(bus_66e94aa4_), 
  .bus_6ee8bc93_(bus_6ee8bc93_));
assign bus_2098e760_=medianRow7_compute_median_instance_DONE&{1{medianRow7_compute_median_instance_DONE}};
medianRow7_Kicker_67 medianRow7_Kicker_67_1(.CLK(CLK), .RESET(bus_4c9dfdd6_), .bus_6680bcd2_(bus_6680bcd2_));
medianRow7_simplememoryreferee_315bcc4c_ medianRow7_simplememoryreferee_315bcc4c__1(.bus_0d5623fb_(CLK), 
  .bus_451c2ced_(bus_4c9dfdd6_), .bus_44aac440_(bus_6a69f8cf_), .bus_46158011_(bus_7014d17a_), 
  .bus_17ee85e8_(receive_u331), .bus_0711b56e_({24'b0, receive_u333[7:0]}), .bus_1029811c_(receive_u332), 
  .bus_5084fbfa_(3'h1), .bus_2a99f094_(compute_median_u778), .bus_08cd118f_(compute_median_u774), 
  .bus_66cbdb83_(compute_median_u776), .bus_411768de_(compute_median_u775), .bus_50d962bb_(3'h1), 
  .bus_242c20e4_(bus_242c20e4_), .bus_44894780_(bus_44894780_), .bus_6b187c32_(bus_6b187c32_), 
  .bus_60573fee_(bus_60573fee_), .bus_54fccda5_(bus_54fccda5_), .bus_7ffa3ba1_(bus_7ffa3ba1_), 
  .bus_0490f155_(bus_0490f155_), .bus_435c5a1a_(bus_435c5a1a_));
medianRow7_receive medianRow7_receive_instance(.CLK(CLK), .RESET(bus_4c9dfdd6_), 
  .GO(receive_go), .port_5edf33a0_(bus_0af85e9e_), .port_16d2f8e7_(bus_7ffa3ba1_), 
  .port_423a2181_(in1_DATA), .DONE(medianRow7_receive_instance_DONE), .RESULT(receive), 
  .RESULT_u2429(receive_u330), .RESULT_u2430(receive_u331), .RESULT_u2431(receive_u332), 
  .RESULT_u2432(receive_u333), .RESULT_u2433(receive_u334), .RESULT_u2434(receive_u335));
medianRow7_structuralmemory_0d65dcc8_ medianRow7_structuralmemory_0d65dcc8__1(.CLK_u101(CLK), 
  .bus_11d9afd8_(bus_4c9dfdd6_), .bus_1b355fb8_(bus_70215d15_), .bus_139162df_(3'h1), 
  .bus_7f5b5d16_(bus_3fdeba12_), .bus_1052d309_(bus_44894780_), .bus_716701ce_(3'h1), 
  .bus_06454314_(bus_60573fee_), .bus_5d904797_(bus_6b187c32_), .bus_144c361d_(bus_242c20e4_), 
  .bus_79c19c10_(bus_79c19c10_), .bus_3fbd0408_(bus_3fbd0408_), .bus_7014d17a_(bus_7014d17a_), 
  .bus_6a69f8cf_(bus_6a69f8cf_));
assign bus_3cee3342_=medianRow7_receive_instance_DONE&{1{medianRow7_receive_instance_DONE}};
medianRow7_compute_median medianRow7_compute_median_instance(.CLK(CLK), .RESET(bus_4c9dfdd6_), 
  .GO(compute_median_go), .port_003ab112_(bus_6ee8bc93_), .port_336f8840_(bus_66e94aa4_), 
  .port_3f171fb5_(bus_435c5a1a_), .port_1a1ed03d_(bus_435c5a1a_), .port_293a8b14_(bus_0490f155_), 
  .DONE(medianRow7_compute_median_instance_DONE), .RESULT(compute_median), .RESULT_u2435(compute_median_u770), 
  .RESULT_u2436(compute_median_u771), .RESULT_u2437(compute_median_u772), .RESULT_u2438(compute_median_u773), 
  .RESULT_u2442(compute_median_u774), .RESULT_u2443(compute_median_u775), .RESULT_u2444(compute_median_u776), 
  .RESULT_u2445(compute_median_u777), .RESULT_u2439(compute_median_u778), .RESULT_u2440(compute_median_u779), 
  .RESULT_u2441(compute_median_u780), .RESULT_u2446(compute_median_u781), .RESULT_u2447(compute_median_u782), 
  .RESULT_u2448(compute_median_u783));
medianRow7_scheduler medianRow7_scheduler_instance(.CLK(CLK), .RESET(bus_4c9dfdd6_), 
  .GO(bus_6680bcd2_), .port_6e4ae772_(bus_06d04b9d_), .port_282348bd_(bus_0af85e9e_), 
  .port_47c2549e_(in1_SEND), .port_4a9db61a_(receive_done), .port_690a5e4b_(median_RDY), 
  .port_574be0de_(compute_median_done), .DONE(medianRow7_scheduler_instance_DONE), 
  .RESULT(scheduler), .RESULT_u2449(scheduler_u841), .RESULT_u2450(scheduler_u842), 
  .RESULT_u2451(scheduler_u843));
medianRow7_stateVar_fsmState_medianRow7 medianRow7_stateVar_fsmState_medianRow7_1(.bus_76c75c5e_(CLK), 
  .bus_422b3039_(bus_4c9dfdd6_), .bus_6cb0eec1_(scheduler), .bus_3b682c6f_(scheduler_u841), 
  .bus_06d04b9d_(bus_06d04b9d_));
medianRow7_globalreset_physical_3892246d_ medianRow7_globalreset_physical_3892246d__1(.bus_74c4d109_(CLK), 
  .bus_301e6f0d_(RESET), .bus_4c9dfdd6_(bus_4c9dfdd6_));
endmodule



module medianRow7_endianswapper_75868499_(endianswapper_75868499_in, endianswapper_75868499_out);
input	[31:0]	endianswapper_75868499_in;
output	[31:0]	endianswapper_75868499_out;
assign endianswapper_75868499_out=endianswapper_75868499_in;
endmodule



module medianRow7_endianswapper_17599ca6_(endianswapper_17599ca6_in, endianswapper_17599ca6_out);
input	[31:0]	endianswapper_17599ca6_in;
output	[31:0]	endianswapper_17599ca6_out;
assign endianswapper_17599ca6_out=endianswapper_17599ca6_in;
endmodule



module medianRow7_stateVar_i(bus_0a1dc326_, bus_7ba5ca21_, bus_1224a3f1_, bus_23656e4b_, bus_7e08435a_, bus_2234c985_, bus_0af85e9e_);
input		bus_0a1dc326_;
input		bus_7ba5ca21_;
input		bus_1224a3f1_;
input	[31:0]	bus_23656e4b_;
input		bus_7e08435a_;
input	[31:0]	bus_2234c985_;
output	[31:0]	bus_0af85e9e_;
wire	[31:0]	mux_3c7724d1_u0;
wire		or_091b1796_u0;
wire	[31:0]	endianswapper_75868499_out;
reg	[31:0]	stateVar_i_u57=32'h0;
wire	[31:0]	endianswapper_17599ca6_out;
assign mux_3c7724d1_u0=(bus_1224a3f1_)?bus_23656e4b_:32'h0;
assign bus_0af85e9e_=endianswapper_17599ca6_out;
assign or_091b1796_u0=bus_1224a3f1_|bus_7e08435a_;
medianRow7_endianswapper_75868499_ medianRow7_endianswapper_75868499__1(.endianswapper_75868499_in(mux_3c7724d1_u0), 
  .endianswapper_75868499_out(endianswapper_75868499_out));
always @(posedge bus_0a1dc326_ or posedge bus_7ba5ca21_)
begin
if (bus_7ba5ca21_)
stateVar_i_u57<=32'h0;
else if (or_091b1796_u0)
stateVar_i_u57<=endianswapper_75868499_out;
end
medianRow7_endianswapper_17599ca6_ medianRow7_endianswapper_17599ca6__1(.endianswapper_17599ca6_in(stateVar_i_u57), 
  .endianswapper_17599ca6_out(endianswapper_17599ca6_out));
endmodule



module medianRow7_simplememoryreferee_545f7316_(bus_24ff83b8_, bus_4d80d7f5_, bus_6b63773b_, bus_18d1c3f2_, bus_2008b291_, bus_6834c0a6_, bus_087e8aef_, bus_312a8c9c_, bus_70215d15_, bus_0352f08f_, bus_3fdeba12_, bus_0338a3ef_, bus_66e94aa4_, bus_6ee8bc93_);
input		bus_24ff83b8_;
input		bus_4d80d7f5_;
input		bus_6b63773b_;
input	[31:0]	bus_18d1c3f2_;
input		bus_2008b291_;
input	[31:0]	bus_6834c0a6_;
input	[2:0]	bus_087e8aef_;
output	[31:0]	bus_312a8c9c_;
output	[31:0]	bus_70215d15_;
output		bus_0352f08f_;
output		bus_3fdeba12_;
output	[2:0]	bus_0338a3ef_;
output	[31:0]	bus_66e94aa4_;
output		bus_6ee8bc93_;
assign bus_312a8c9c_=32'h0;
assign bus_70215d15_=bus_6834c0a6_;
assign bus_0352f08f_=1'h0;
assign bus_3fdeba12_=bus_2008b291_;
assign bus_0338a3ef_=3'h1;
assign bus_66e94aa4_=bus_18d1c3f2_;
assign bus_6ee8bc93_=bus_6b63773b_;
endmodule



module medianRow7_Kicker_67(CLK, RESET, bus_6680bcd2_);
input		CLK;
input		RESET;
output		bus_6680bcd2_;
wire		bus_722568ff_;
reg		kicker_2=1'h0;
wire		bus_471d6bab_;
reg		kicker_res=1'h0;
wire		bus_6fbcc98e_;
reg		kicker_1=1'h0;
wire		bus_5775573c_;
assign bus_722568ff_=~kicker_2;
assign bus_6680bcd2_=kicker_res;
always @(posedge CLK)
begin
kicker_2<=bus_6fbcc98e_;
end
assign bus_471d6bab_=~RESET;
always @(posedge CLK)
begin
kicker_res<=bus_5775573c_;
end
assign bus_6fbcc98e_=bus_471d6bab_&kicker_1;
always @(posedge CLK)
begin
kicker_1<=bus_471d6bab_;
end
assign bus_5775573c_=kicker_1&bus_471d6bab_&bus_722568ff_;
endmodule



module medianRow7_simplememoryreferee_315bcc4c_(bus_0d5623fb_, bus_451c2ced_, bus_44aac440_, bus_46158011_, bus_17ee85e8_, bus_0711b56e_, bus_1029811c_, bus_5084fbfa_, bus_2a99f094_, bus_08cd118f_, bus_66cbdb83_, bus_411768de_, bus_50d962bb_, bus_242c20e4_, bus_44894780_, bus_6b187c32_, bus_60573fee_, bus_54fccda5_, bus_7ffa3ba1_, bus_0490f155_, bus_435c5a1a_);
input		bus_0d5623fb_;
input		bus_451c2ced_;
input		bus_44aac440_;
input	[31:0]	bus_46158011_;
input		bus_17ee85e8_;
input	[31:0]	bus_0711b56e_;
input	[31:0]	bus_1029811c_;
input	[2:0]	bus_5084fbfa_;
input		bus_2a99f094_;
input		bus_08cd118f_;
input	[31:0]	bus_66cbdb83_;
input	[31:0]	bus_411768de_;
input	[2:0]	bus_50d962bb_;
output	[31:0]	bus_242c20e4_;
output	[31:0]	bus_44894780_;
output		bus_6b187c32_;
output		bus_60573fee_;
output	[2:0]	bus_54fccda5_;
output		bus_7ffa3ba1_;
output	[31:0]	bus_0490f155_;
output		bus_435c5a1a_;
reg		done_qual_u238=1'h0;
wire		or_75dafa52_u0;
wire		and_3d1f8e17_u0;
wire		not_3a4e97c6_u0;
wire		or_7e06a06f_u0;
wire		or_69bd4471_u0;
wire	[31:0]	mux_6483266f_u0;
wire		and_1dbd1072_u0;
wire		not_484f891b_u0;
reg		done_qual_u239_u0=1'h0;
wire	[31:0]	mux_0078f381_u0;
wire		or_0c9f6f64_u0;
wire		or_2e74100f_u0;
always @(posedge bus_0d5623fb_)
begin
if (bus_451c2ced_)
done_qual_u238<=1'h0;
else done_qual_u238<=or_7e06a06f_u0;
end
assign or_75dafa52_u0=bus_17ee85e8_|or_7e06a06f_u0;
assign and_3d1f8e17_u0=or_0c9f6f64_u0&bus_44aac440_;
assign not_3a4e97c6_u0=~bus_44aac440_;
assign or_7e06a06f_u0=bus_2a99f094_|bus_08cd118f_;
assign or_69bd4471_u0=bus_17ee85e8_|bus_08cd118f_;
assign mux_6483266f_u0=(bus_17ee85e8_)?{24'b0, bus_0711b56e_[7:0]}:bus_66cbdb83_;
assign and_1dbd1072_u0=or_2e74100f_u0&bus_44aac440_;
assign not_484f891b_u0=~bus_44aac440_;
always @(posedge bus_0d5623fb_)
begin
if (bus_451c2ced_)
done_qual_u239_u0<=1'h0;
else done_qual_u239_u0<=bus_17ee85e8_;
end
assign mux_0078f381_u0=(bus_17ee85e8_)?bus_1029811c_:bus_411768de_;
assign or_0c9f6f64_u0=bus_17ee85e8_|done_qual_u239_u0;
assign or_2e74100f_u0=or_7e06a06f_u0|done_qual_u238;
assign bus_242c20e4_=mux_6483266f_u0;
assign bus_44894780_=mux_0078f381_u0;
assign bus_6b187c32_=or_69bd4471_u0;
assign bus_60573fee_=or_75dafa52_u0;
assign bus_54fccda5_=3'h1;
assign bus_7ffa3ba1_=and_3d1f8e17_u0;
assign bus_0490f155_=bus_46158011_;
assign bus_435c5a1a_=and_1dbd1072_u0;
endmodule



module medianRow7_receive(CLK, RESET, GO, port_5edf33a0_, port_16d2f8e7_, port_423a2181_, RESULT, RESULT_u2429, RESULT_u2430, RESULT_u2431, RESULT_u2432, RESULT_u2433, RESULT_u2434, DONE);
input		CLK;
input		RESET;
input		GO;
input	[31:0]	port_5edf33a0_;
input		port_16d2f8e7_;
input	[7:0]	port_423a2181_;
output		RESULT;
output	[31:0]	RESULT_u2429;
output		RESULT_u2430;
output	[31:0]	RESULT_u2431;
output	[31:0]	RESULT_u2432;
output	[2:0]	RESULT_u2433;
output		RESULT_u2434;
output		DONE;
wire		simplePinWrite;
wire	[31:0]	add;
wire		and_u4160_u0;
reg		reg_532f1d54_u0=1'h0;
wire		or_u1606_u0;
wire	[31:0]	add_u771;
reg		reg_543a9e4e_u0=1'h0;
assign simplePinWrite=GO&{1{GO}};
assign add=port_5edf33a0_+32'h0;
assign and_u4160_u0=reg_532f1d54_u0&port_16d2f8e7_;
always @(posedge CLK or posedge GO or posedge or_u1606_u0)
begin
if (or_u1606_u0)
reg_532f1d54_u0<=1'h0;
else if (GO)
reg_532f1d54_u0<=1'h1;
else reg_532f1d54_u0<=reg_532f1d54_u0;
end
assign or_u1606_u0=and_u4160_u0|RESET;
assign add_u771=port_5edf33a0_+32'h1;
always @(posedge CLK or posedge RESET)
begin
if (RESET)
reg_543a9e4e_u0<=1'h0;
else reg_543a9e4e_u0<=GO;
end
assign RESULT=GO;
assign RESULT_u2429=add_u771;
assign RESULT_u2430=GO;
assign RESULT_u2431=add;
assign RESULT_u2432={24'b0, port_423a2181_};
assign RESULT_u2433=3'h1;
assign RESULT_u2434=simplePinWrite;
assign DONE=reg_543a9e4e_u0;
endmodule



module medianRow7_forge_memory_101x32_163(CLK, ENA, WEA, DINA, ENB, ADDRA, ADDRB, DOUTA, DOUTB, DONEA, DONEB);
input		CLK;
input		ENA;
input		WEA;
input	[31:0]	DINA;
input		ENB;
input	[31:0]	ADDRA;
input	[31:0]	ADDRB;
output	[31:0]	DOUTA;
output	[31:0]	DOUTB;
output		DONEA;
output		DONEB;
wire		wea_0;
wire	[31:0]	pre_douta_0;
wire	[31:0]	pre_doutb_0;
wire		wea_1;
wire	[31:0]	pre_douta_1;
wire	[31:0]	pre_doutb_1;
reg		wea_done;
reg		rea_done;
reg	[31:0]	mux_outa;
reg	[31:0]	mux_outb;
assign wea_0=WEA&(ADDRA[31:6]==26'h0);
assign wea_1=WEA&(ADDRA[31:6]==26'h1);
always @(posedge CLK)
begin
wea_done<=WEA;
end
assign DONEA=rea_done|wea_done;
always @(posedge CLK)
begin
end
assign DONEB=ENB;
always @(pre_douta_0 or pre_douta_1 or ADDRA)
begin
case (ADDRA[31:6])26'd0:mux_outa=pre_douta_0;
26'd1:mux_outa=pre_douta_1;
default:mux_outa=32'h0;
endcase end
assign DOUTA=mux_outa;
always @(pre_doutb_0 or pre_doutb_1 or ADDRB)
begin
case (ADDRB[31:6])26'd0:mux_outb=pre_doutb_0;
26'd1:mux_outb=pre_doutb_1;
default:mux_outb=32'h0;
endcase end
assign DOUTB=mux_outb;
// Memory array element: COL: 0, ROW: 0
//  Initialization of Dual Port LUT ram now done through explicit
// parameter setting.
RAM64X1D#(.INIT(64'h0000000000000000))RAM64X1D_instance_3520(.WCLK(CLK), .WE(wea_0), 
  .A0(ADDRA[0]), .A1(ADDRA[1]), .A2(ADDRA[2]), .A3(ADDRA[3]), .A4(ADDRA[4]), .A5(ADDRA[5]), 
  .D(DINA[0]), .SPO(pre_douta_0[0]), .DPRA0(ADDRB[0]), .DPRA1(ADDRB[1]), .DPRA2(ADDRB[2]), 
  .DPRA3(ADDRB[3]), .DPRA4(ADDRB[4]), .DPRA5(ADDRB[5]), .DPO(pre_doutb_0[0]));
// Memory array element: COL: 0, ROW: 1
//  Initialization of Dual Port LUT ram now done through explicit
// parameter setting.
RAM64X1D#(.INIT(64'h0000000000000000))RAM64X1D_instance_3521(.WCLK(CLK), .WE(wea_0), 
  .A0(ADDRA[0]), .A1(ADDRA[1]), .A2(ADDRA[2]), .A3(ADDRA[3]), .A4(ADDRA[4]), .A5(ADDRA[5]), 
  .D(DINA[1]), .SPO(pre_douta_0[1]), .DPRA0(ADDRB[0]), .DPRA1(ADDRB[1]), .DPRA2(ADDRB[2]), 
  .DPRA3(ADDRB[3]), .DPRA4(ADDRB[4]), .DPRA5(ADDRB[5]), .DPO(pre_doutb_0[1]));
// Memory array element: COL: 0, ROW: 2
//  Initialization of Dual Port LUT ram now done through explicit
// parameter setting.
RAM64X1D#(.INIT(64'h0000000000000000))RAM64X1D_instance_3522(.WCLK(CLK), .WE(wea_0), 
  .A0(ADDRA[0]), .A1(ADDRA[1]), .A2(ADDRA[2]), .A3(ADDRA[3]), .A4(ADDRA[4]), .A5(ADDRA[5]), 
  .D(DINA[2]), .SPO(pre_douta_0[2]), .DPRA0(ADDRB[0]), .DPRA1(ADDRB[1]), .DPRA2(ADDRB[2]), 
  .DPRA3(ADDRB[3]), .DPRA4(ADDRB[4]), .DPRA5(ADDRB[5]), .DPO(pre_doutb_0[2]));
// Memory array element: COL: 0, ROW: 3
//  Initialization of Dual Port LUT ram now done through explicit
// parameter setting.
RAM64X1D#(.INIT(64'h0000000000000000))RAM64X1D_instance_3523(.WCLK(CLK), .WE(wea_0), 
  .A0(ADDRA[0]), .A1(ADDRA[1]), .A2(ADDRA[2]), .A3(ADDRA[3]), .A4(ADDRA[4]), .A5(ADDRA[5]), 
  .D(DINA[3]), .SPO(pre_douta_0[3]), .DPRA0(ADDRB[0]), .DPRA1(ADDRB[1]), .DPRA2(ADDRB[2]), 
  .DPRA3(ADDRB[3]), .DPRA4(ADDRB[4]), .DPRA5(ADDRB[5]), .DPO(pre_doutb_0[3]));
// Memory array element: COL: 0, ROW: 4
//  Initialization of Dual Port LUT ram now done through explicit
// parameter setting.
RAM64X1D#(.INIT(64'h0000000000000000))RAM64X1D_instance_3524(.WCLK(CLK), .WE(wea_0), 
  .A0(ADDRA[0]), .A1(ADDRA[1]), .A2(ADDRA[2]), .A3(ADDRA[3]), .A4(ADDRA[4]), .A5(ADDRA[5]), 
  .D(DINA[4]), .SPO(pre_douta_0[4]), .DPRA0(ADDRB[0]), .DPRA1(ADDRB[1]), .DPRA2(ADDRB[2]), 
  .DPRA3(ADDRB[3]), .DPRA4(ADDRB[4]), .DPRA5(ADDRB[5]), .DPO(pre_doutb_0[4]));
// Memory array element: COL: 0, ROW: 5
//  Initialization of Dual Port LUT ram now done through explicit
// parameter setting.
RAM64X1D#(.INIT(64'h0000000000000000))RAM64X1D_instance_3525(.WCLK(CLK), .WE(wea_0), 
  .A0(ADDRA[0]), .A1(ADDRA[1]), .A2(ADDRA[2]), .A3(ADDRA[3]), .A4(ADDRA[4]), .A5(ADDRA[5]), 
  .D(DINA[5]), .SPO(pre_douta_0[5]), .DPRA0(ADDRB[0]), .DPRA1(ADDRB[1]), .DPRA2(ADDRB[2]), 
  .DPRA3(ADDRB[3]), .DPRA4(ADDRB[4]), .DPRA5(ADDRB[5]), .DPO(pre_doutb_0[5]));
// Memory array element: COL: 0, ROW: 6
//  Initialization of Dual Port LUT ram now done through explicit
// parameter setting.
RAM64X1D#(.INIT(64'h0000000000000000))RAM64X1D_instance_3526(.WCLK(CLK), .WE(wea_0), 
  .A0(ADDRA[0]), .A1(ADDRA[1]), .A2(ADDRA[2]), .A3(ADDRA[3]), .A4(ADDRA[4]), .A5(ADDRA[5]), 
  .D(DINA[6]), .SPO(pre_douta_0[6]), .DPRA0(ADDRB[0]), .DPRA1(ADDRB[1]), .DPRA2(ADDRB[2]), 
  .DPRA3(ADDRB[3]), .DPRA4(ADDRB[4]), .DPRA5(ADDRB[5]), .DPO(pre_doutb_0[6]));
// Memory array element: COL: 0, ROW: 7
//  Initialization of Dual Port LUT ram now done through explicit
// parameter setting.
RAM64X1D#(.INIT(64'h0000000000000000))RAM64X1D_instance_3527(.WCLK(CLK), .WE(wea_0), 
  .A0(ADDRA[0]), .A1(ADDRA[1]), .A2(ADDRA[2]), .A3(ADDRA[3]), .A4(ADDRA[4]), .A5(ADDRA[5]), 
  .D(DINA[7]), .SPO(pre_douta_0[7]), .DPRA0(ADDRB[0]), .DPRA1(ADDRB[1]), .DPRA2(ADDRB[2]), 
  .DPRA3(ADDRB[3]), .DPRA4(ADDRB[4]), .DPRA5(ADDRB[5]), .DPO(pre_doutb_0[7]));
// Memory array element: COL: 0, ROW: 8
//  Initialization of Dual Port LUT ram now done through explicit
// parameter setting.
RAM64X1D#(.INIT(64'h0000000000000000))RAM64X1D_instance_3528(.WCLK(CLK), .WE(wea_0), 
  .A0(ADDRA[0]), .A1(ADDRA[1]), .A2(ADDRA[2]), .A3(ADDRA[3]), .A4(ADDRA[4]), .A5(ADDRA[5]), 
  .D(DINA[8]), .SPO(pre_douta_0[8]), .DPRA0(ADDRB[0]), .DPRA1(ADDRB[1]), .DPRA2(ADDRB[2]), 
  .DPRA3(ADDRB[3]), .DPRA4(ADDRB[4]), .DPRA5(ADDRB[5]), .DPO(pre_doutb_0[8]));
// Memory array element: COL: 0, ROW: 9
//  Initialization of Dual Port LUT ram now done through explicit
// parameter setting.
RAM64X1D#(.INIT(64'h0000000000000000))RAM64X1D_instance_3529(.WCLK(CLK), .WE(wea_0), 
  .A0(ADDRA[0]), .A1(ADDRA[1]), .A2(ADDRA[2]), .A3(ADDRA[3]), .A4(ADDRA[4]), .A5(ADDRA[5]), 
  .D(DINA[9]), .SPO(pre_douta_0[9]), .DPRA0(ADDRB[0]), .DPRA1(ADDRB[1]), .DPRA2(ADDRB[2]), 
  .DPRA3(ADDRB[3]), .DPRA4(ADDRB[4]), .DPRA5(ADDRB[5]), .DPO(pre_doutb_0[9]));
// Memory array element: COL: 0, ROW: 10
//  Initialization of Dual Port LUT ram now done through explicit
// parameter setting.
RAM64X1D#(.INIT(64'h0000000000000000))RAM64X1D_instance_3530(.WCLK(CLK), .WE(wea_0), 
  .A0(ADDRA[0]), .A1(ADDRA[1]), .A2(ADDRA[2]), .A3(ADDRA[3]), .A4(ADDRA[4]), .A5(ADDRA[5]), 
  .D(DINA[10]), .SPO(pre_douta_0[10]), .DPRA0(ADDRB[0]), .DPRA1(ADDRB[1]), .DPRA2(ADDRB[2]), 
  .DPRA3(ADDRB[3]), .DPRA4(ADDRB[4]), .DPRA5(ADDRB[5]), .DPO(pre_doutb_0[10]));
// Memory array element: COL: 0, ROW: 11
//  Initialization of Dual Port LUT ram now done through explicit
// parameter setting.
RAM64X1D#(.INIT(64'h0000000000000000))RAM64X1D_instance_3531(.WCLK(CLK), .WE(wea_0), 
  .A0(ADDRA[0]), .A1(ADDRA[1]), .A2(ADDRA[2]), .A3(ADDRA[3]), .A4(ADDRA[4]), .A5(ADDRA[5]), 
  .D(DINA[11]), .SPO(pre_douta_0[11]), .DPRA0(ADDRB[0]), .DPRA1(ADDRB[1]), .DPRA2(ADDRB[2]), 
  .DPRA3(ADDRB[3]), .DPRA4(ADDRB[4]), .DPRA5(ADDRB[5]), .DPO(pre_doutb_0[11]));
// Memory array element: COL: 0, ROW: 12
//  Initialization of Dual Port LUT ram now done through explicit
// parameter setting.
RAM64X1D#(.INIT(64'h0000000000000000))RAM64X1D_instance_3532(.WCLK(CLK), .WE(wea_0), 
  .A0(ADDRA[0]), .A1(ADDRA[1]), .A2(ADDRA[2]), .A3(ADDRA[3]), .A4(ADDRA[4]), .A5(ADDRA[5]), 
  .D(DINA[12]), .SPO(pre_douta_0[12]), .DPRA0(ADDRB[0]), .DPRA1(ADDRB[1]), .DPRA2(ADDRB[2]), 
  .DPRA3(ADDRB[3]), .DPRA4(ADDRB[4]), .DPRA5(ADDRB[5]), .DPO(pre_doutb_0[12]));
// Memory array element: COL: 0, ROW: 13
//  Initialization of Dual Port LUT ram now done through explicit
// parameter setting.
RAM64X1D#(.INIT(64'h0000000000000000))RAM64X1D_instance_3533(.WCLK(CLK), .WE(wea_0), 
  .A0(ADDRA[0]), .A1(ADDRA[1]), .A2(ADDRA[2]), .A3(ADDRA[3]), .A4(ADDRA[4]), .A5(ADDRA[5]), 
  .D(DINA[13]), .SPO(pre_douta_0[13]), .DPRA0(ADDRB[0]), .DPRA1(ADDRB[1]), .DPRA2(ADDRB[2]), 
  .DPRA3(ADDRB[3]), .DPRA4(ADDRB[4]), .DPRA5(ADDRB[5]), .DPO(pre_doutb_0[13]));
// Memory array element: COL: 0, ROW: 14
//  Initialization of Dual Port LUT ram now done through explicit
// parameter setting.
RAM64X1D#(.INIT(64'h0000000000000000))RAM64X1D_instance_3534(.WCLK(CLK), .WE(wea_0), 
  .A0(ADDRA[0]), .A1(ADDRA[1]), .A2(ADDRA[2]), .A3(ADDRA[3]), .A4(ADDRA[4]), .A5(ADDRA[5]), 
  .D(DINA[14]), .SPO(pre_douta_0[14]), .DPRA0(ADDRB[0]), .DPRA1(ADDRB[1]), .DPRA2(ADDRB[2]), 
  .DPRA3(ADDRB[3]), .DPRA4(ADDRB[4]), .DPRA5(ADDRB[5]), .DPO(pre_doutb_0[14]));
// Memory array element: COL: 0, ROW: 15
//  Initialization of Dual Port LUT ram now done through explicit
// parameter setting.
RAM64X1D#(.INIT(64'h0000000000000000))RAM64X1D_instance_3535(.WCLK(CLK), .WE(wea_0), 
  .A0(ADDRA[0]), .A1(ADDRA[1]), .A2(ADDRA[2]), .A3(ADDRA[3]), .A4(ADDRA[4]), .A5(ADDRA[5]), 
  .D(DINA[15]), .SPO(pre_douta_0[15]), .DPRA0(ADDRB[0]), .DPRA1(ADDRB[1]), .DPRA2(ADDRB[2]), 
  .DPRA3(ADDRB[3]), .DPRA4(ADDRB[4]), .DPRA5(ADDRB[5]), .DPO(pre_doutb_0[15]));
// Memory array element: COL: 0, ROW: 16
//  Initialization of Dual Port LUT ram now done through explicit
// parameter setting.
RAM64X1D#(.INIT(64'h0000000000000000))RAM64X1D_instance_3536(.WCLK(CLK), .WE(wea_0), 
  .A0(ADDRA[0]), .A1(ADDRA[1]), .A2(ADDRA[2]), .A3(ADDRA[3]), .A4(ADDRA[4]), .A5(ADDRA[5]), 
  .D(DINA[16]), .SPO(pre_douta_0[16]), .DPRA0(ADDRB[0]), .DPRA1(ADDRB[1]), .DPRA2(ADDRB[2]), 
  .DPRA3(ADDRB[3]), .DPRA4(ADDRB[4]), .DPRA5(ADDRB[5]), .DPO(pre_doutb_0[16]));
// Memory array element: COL: 0, ROW: 17
//  Initialization of Dual Port LUT ram now done through explicit
// parameter setting.
RAM64X1D#(.INIT(64'h0000000000000000))RAM64X1D_instance_3537(.WCLK(CLK), .WE(wea_0), 
  .A0(ADDRA[0]), .A1(ADDRA[1]), .A2(ADDRA[2]), .A3(ADDRA[3]), .A4(ADDRA[4]), .A5(ADDRA[5]), 
  .D(DINA[17]), .SPO(pre_douta_0[17]), .DPRA0(ADDRB[0]), .DPRA1(ADDRB[1]), .DPRA2(ADDRB[2]), 
  .DPRA3(ADDRB[3]), .DPRA4(ADDRB[4]), .DPRA5(ADDRB[5]), .DPO(pre_doutb_0[17]));
// Memory array element: COL: 0, ROW: 18
//  Initialization of Dual Port LUT ram now done through explicit
// parameter setting.
RAM64X1D#(.INIT(64'h0000000000000000))RAM64X1D_instance_3538(.WCLK(CLK), .WE(wea_0), 
  .A0(ADDRA[0]), .A1(ADDRA[1]), .A2(ADDRA[2]), .A3(ADDRA[3]), .A4(ADDRA[4]), .A5(ADDRA[5]), 
  .D(DINA[18]), .SPO(pre_douta_0[18]), .DPRA0(ADDRB[0]), .DPRA1(ADDRB[1]), .DPRA2(ADDRB[2]), 
  .DPRA3(ADDRB[3]), .DPRA4(ADDRB[4]), .DPRA5(ADDRB[5]), .DPO(pre_doutb_0[18]));
// Memory array element: COL: 0, ROW: 19
//  Initialization of Dual Port LUT ram now done through explicit
// parameter setting.
RAM64X1D#(.INIT(64'h0000000000000000))RAM64X1D_instance_3539(.WCLK(CLK), .WE(wea_0), 
  .A0(ADDRA[0]), .A1(ADDRA[1]), .A2(ADDRA[2]), .A3(ADDRA[3]), .A4(ADDRA[4]), .A5(ADDRA[5]), 
  .D(DINA[19]), .SPO(pre_douta_0[19]), .DPRA0(ADDRB[0]), .DPRA1(ADDRB[1]), .DPRA2(ADDRB[2]), 
  .DPRA3(ADDRB[3]), .DPRA4(ADDRB[4]), .DPRA5(ADDRB[5]), .DPO(pre_doutb_0[19]));
// Memory array element: COL: 0, ROW: 20
//  Initialization of Dual Port LUT ram now done through explicit
// parameter setting.
RAM64X1D#(.INIT(64'h0000000000000000))RAM64X1D_instance_3540(.WCLK(CLK), .WE(wea_0), 
  .A0(ADDRA[0]), .A1(ADDRA[1]), .A2(ADDRA[2]), .A3(ADDRA[3]), .A4(ADDRA[4]), .A5(ADDRA[5]), 
  .D(DINA[20]), .SPO(pre_douta_0[20]), .DPRA0(ADDRB[0]), .DPRA1(ADDRB[1]), .DPRA2(ADDRB[2]), 
  .DPRA3(ADDRB[3]), .DPRA4(ADDRB[4]), .DPRA5(ADDRB[5]), .DPO(pre_doutb_0[20]));
// Memory array element: COL: 0, ROW: 21
//  Initialization of Dual Port LUT ram now done through explicit
// parameter setting.
RAM64X1D#(.INIT(64'h0000000000000000))RAM64X1D_instance_3541(.WCLK(CLK), .WE(wea_0), 
  .A0(ADDRA[0]), .A1(ADDRA[1]), .A2(ADDRA[2]), .A3(ADDRA[3]), .A4(ADDRA[4]), .A5(ADDRA[5]), 
  .D(DINA[21]), .SPO(pre_douta_0[21]), .DPRA0(ADDRB[0]), .DPRA1(ADDRB[1]), .DPRA2(ADDRB[2]), 
  .DPRA3(ADDRB[3]), .DPRA4(ADDRB[4]), .DPRA5(ADDRB[5]), .DPO(pre_doutb_0[21]));
// Memory array element: COL: 0, ROW: 22
//  Initialization of Dual Port LUT ram now done through explicit
// parameter setting.
RAM64X1D#(.INIT(64'h0000000000000000))RAM64X1D_instance_3542(.WCLK(CLK), .WE(wea_0), 
  .A0(ADDRA[0]), .A1(ADDRA[1]), .A2(ADDRA[2]), .A3(ADDRA[3]), .A4(ADDRA[4]), .A5(ADDRA[5]), 
  .D(DINA[22]), .SPO(pre_douta_0[22]), .DPRA0(ADDRB[0]), .DPRA1(ADDRB[1]), .DPRA2(ADDRB[2]), 
  .DPRA3(ADDRB[3]), .DPRA4(ADDRB[4]), .DPRA5(ADDRB[5]), .DPO(pre_doutb_0[22]));
// Memory array element: COL: 0, ROW: 23
//  Initialization of Dual Port LUT ram now done through explicit
// parameter setting.
RAM64X1D#(.INIT(64'h0000000000000000))RAM64X1D_instance_3543(.WCLK(CLK), .WE(wea_0), 
  .A0(ADDRA[0]), .A1(ADDRA[1]), .A2(ADDRA[2]), .A3(ADDRA[3]), .A4(ADDRA[4]), .A5(ADDRA[5]), 
  .D(DINA[23]), .SPO(pre_douta_0[23]), .DPRA0(ADDRB[0]), .DPRA1(ADDRB[1]), .DPRA2(ADDRB[2]), 
  .DPRA3(ADDRB[3]), .DPRA4(ADDRB[4]), .DPRA5(ADDRB[5]), .DPO(pre_doutb_0[23]));
// Memory array element: COL: 0, ROW: 24
//  Initialization of Dual Port LUT ram now done through explicit
// parameter setting.
RAM64X1D#(.INIT(64'h0000000000000000))RAM64X1D_instance_3544(.WCLK(CLK), .WE(wea_0), 
  .A0(ADDRA[0]), .A1(ADDRA[1]), .A2(ADDRA[2]), .A3(ADDRA[3]), .A4(ADDRA[4]), .A5(ADDRA[5]), 
  .D(DINA[24]), .SPO(pre_douta_0[24]), .DPRA0(ADDRB[0]), .DPRA1(ADDRB[1]), .DPRA2(ADDRB[2]), 
  .DPRA3(ADDRB[3]), .DPRA4(ADDRB[4]), .DPRA5(ADDRB[5]), .DPO(pre_doutb_0[24]));
// Memory array element: COL: 0, ROW: 25
//  Initialization of Dual Port LUT ram now done through explicit
// parameter setting.
RAM64X1D#(.INIT(64'h0000000000000000))RAM64X1D_instance_3545(.WCLK(CLK), .WE(wea_0), 
  .A0(ADDRA[0]), .A1(ADDRA[1]), .A2(ADDRA[2]), .A3(ADDRA[3]), .A4(ADDRA[4]), .A5(ADDRA[5]), 
  .D(DINA[25]), .SPO(pre_douta_0[25]), .DPRA0(ADDRB[0]), .DPRA1(ADDRB[1]), .DPRA2(ADDRB[2]), 
  .DPRA3(ADDRB[3]), .DPRA4(ADDRB[4]), .DPRA5(ADDRB[5]), .DPO(pre_doutb_0[25]));
// Memory array element: COL: 0, ROW: 26
//  Initialization of Dual Port LUT ram now done through explicit
// parameter setting.
RAM64X1D#(.INIT(64'h0000000000000000))RAM64X1D_instance_3546(.WCLK(CLK), .WE(wea_0), 
  .A0(ADDRA[0]), .A1(ADDRA[1]), .A2(ADDRA[2]), .A3(ADDRA[3]), .A4(ADDRA[4]), .A5(ADDRA[5]), 
  .D(DINA[26]), .SPO(pre_douta_0[26]), .DPRA0(ADDRB[0]), .DPRA1(ADDRB[1]), .DPRA2(ADDRB[2]), 
  .DPRA3(ADDRB[3]), .DPRA4(ADDRB[4]), .DPRA5(ADDRB[5]), .DPO(pre_doutb_0[26]));
// Memory array element: COL: 0, ROW: 27
//  Initialization of Dual Port LUT ram now done through explicit
// parameter setting.
RAM64X1D#(.INIT(64'h0000000000000000))RAM64X1D_instance_3547(.WCLK(CLK), .WE(wea_0), 
  .A0(ADDRA[0]), .A1(ADDRA[1]), .A2(ADDRA[2]), .A3(ADDRA[3]), .A4(ADDRA[4]), .A5(ADDRA[5]), 
  .D(DINA[27]), .SPO(pre_douta_0[27]), .DPRA0(ADDRB[0]), .DPRA1(ADDRB[1]), .DPRA2(ADDRB[2]), 
  .DPRA3(ADDRB[3]), .DPRA4(ADDRB[4]), .DPRA5(ADDRB[5]), .DPO(pre_doutb_0[27]));
// Memory array element: COL: 0, ROW: 28
//  Initialization of Dual Port LUT ram now done through explicit
// parameter setting.
RAM64X1D#(.INIT(64'h0000000000000000))RAM64X1D_instance_3548(.WCLK(CLK), .WE(wea_0), 
  .A0(ADDRA[0]), .A1(ADDRA[1]), .A2(ADDRA[2]), .A3(ADDRA[3]), .A4(ADDRA[4]), .A5(ADDRA[5]), 
  .D(DINA[28]), .SPO(pre_douta_0[28]), .DPRA0(ADDRB[0]), .DPRA1(ADDRB[1]), .DPRA2(ADDRB[2]), 
  .DPRA3(ADDRB[3]), .DPRA4(ADDRB[4]), .DPRA5(ADDRB[5]), .DPO(pre_doutb_0[28]));
// Memory array element: COL: 0, ROW: 29
//  Initialization of Dual Port LUT ram now done through explicit
// parameter setting.
RAM64X1D#(.INIT(64'h0000000000000000))RAM64X1D_instance_3549(.WCLK(CLK), .WE(wea_0), 
  .A0(ADDRA[0]), .A1(ADDRA[1]), .A2(ADDRA[2]), .A3(ADDRA[3]), .A4(ADDRA[4]), .A5(ADDRA[5]), 
  .D(DINA[29]), .SPO(pre_douta_0[29]), .DPRA0(ADDRB[0]), .DPRA1(ADDRB[1]), .DPRA2(ADDRB[2]), 
  .DPRA3(ADDRB[3]), .DPRA4(ADDRB[4]), .DPRA5(ADDRB[5]), .DPO(pre_doutb_0[29]));
// Memory array element: COL: 0, ROW: 30
//  Initialization of Dual Port LUT ram now done through explicit
// parameter setting.
RAM64X1D#(.INIT(64'h0000000000000000))RAM64X1D_instance_3550(.WCLK(CLK), .WE(wea_0), 
  .A0(ADDRA[0]), .A1(ADDRA[1]), .A2(ADDRA[2]), .A3(ADDRA[3]), .A4(ADDRA[4]), .A5(ADDRA[5]), 
  .D(DINA[30]), .SPO(pre_douta_0[30]), .DPRA0(ADDRB[0]), .DPRA1(ADDRB[1]), .DPRA2(ADDRB[2]), 
  .DPRA3(ADDRB[3]), .DPRA4(ADDRB[4]), .DPRA5(ADDRB[5]), .DPO(pre_doutb_0[30]));
// Memory array element: COL: 0, ROW: 31
//  Initialization of Dual Port LUT ram now done through explicit
// parameter setting.
RAM64X1D#(.INIT(64'h0000000000000000))RAM64X1D_instance_3551(.WCLK(CLK), .WE(wea_0), 
  .A0(ADDRA[0]), .A1(ADDRA[1]), .A2(ADDRA[2]), .A3(ADDRA[3]), .A4(ADDRA[4]), .A5(ADDRA[5]), 
  .D(DINA[31]), .SPO(pre_douta_0[31]), .DPRA0(ADDRB[0]), .DPRA1(ADDRB[1]), .DPRA2(ADDRB[2]), 
  .DPRA3(ADDRB[3]), .DPRA4(ADDRB[4]), .DPRA5(ADDRB[5]), .DPO(pre_doutb_0[31]));
// Memory array element: COL: 1, ROW: 0
//  Initialization of Dual Port LUT ram now done through explicit
// parameter setting.
RAM64X1D#(.INIT(64'h0000000000000000))RAM64X1D_instance_3552(.WCLK(CLK), .WE(wea_1), 
  .A0(ADDRA[0]), .A1(ADDRA[1]), .A2(ADDRA[2]), .A3(ADDRA[3]), .A4(ADDRA[4]), .A5(ADDRA[5]), 
  .D(DINA[0]), .SPO(pre_douta_1[0]), .DPRA0(ADDRB[0]), .DPRA1(ADDRB[1]), .DPRA2(ADDRB[2]), 
  .DPRA3(ADDRB[3]), .DPRA4(ADDRB[4]), .DPRA5(ADDRB[5]), .DPO(pre_doutb_1[0]));
// Memory array element: COL: 1, ROW: 1
//  Initialization of Dual Port LUT ram now done through explicit
// parameter setting.
RAM64X1D#(.INIT(64'h0000000000000000))RAM64X1D_instance_3553(.WCLK(CLK), .WE(wea_1), 
  .A0(ADDRA[0]), .A1(ADDRA[1]), .A2(ADDRA[2]), .A3(ADDRA[3]), .A4(ADDRA[4]), .A5(ADDRA[5]), 
  .D(DINA[1]), .SPO(pre_douta_1[1]), .DPRA0(ADDRB[0]), .DPRA1(ADDRB[1]), .DPRA2(ADDRB[2]), 
  .DPRA3(ADDRB[3]), .DPRA4(ADDRB[4]), .DPRA5(ADDRB[5]), .DPO(pre_doutb_1[1]));
// Memory array element: COL: 1, ROW: 2
//  Initialization of Dual Port LUT ram now done through explicit
// parameter setting.
RAM64X1D#(.INIT(64'h0000000000000000))RAM64X1D_instance_3554(.WCLK(CLK), .WE(wea_1), 
  .A0(ADDRA[0]), .A1(ADDRA[1]), .A2(ADDRA[2]), .A3(ADDRA[3]), .A4(ADDRA[4]), .A5(ADDRA[5]), 
  .D(DINA[2]), .SPO(pre_douta_1[2]), .DPRA0(ADDRB[0]), .DPRA1(ADDRB[1]), .DPRA2(ADDRB[2]), 
  .DPRA3(ADDRB[3]), .DPRA4(ADDRB[4]), .DPRA5(ADDRB[5]), .DPO(pre_doutb_1[2]));
// Memory array element: COL: 1, ROW: 3
//  Initialization of Dual Port LUT ram now done through explicit
// parameter setting.
RAM64X1D#(.INIT(64'h0000000000000000))RAM64X1D_instance_3555(.WCLK(CLK), .WE(wea_1), 
  .A0(ADDRA[0]), .A1(ADDRA[1]), .A2(ADDRA[2]), .A3(ADDRA[3]), .A4(ADDRA[4]), .A5(ADDRA[5]), 
  .D(DINA[3]), .SPO(pre_douta_1[3]), .DPRA0(ADDRB[0]), .DPRA1(ADDRB[1]), .DPRA2(ADDRB[2]), 
  .DPRA3(ADDRB[3]), .DPRA4(ADDRB[4]), .DPRA5(ADDRB[5]), .DPO(pre_doutb_1[3]));
// Memory array element: COL: 1, ROW: 4
//  Initialization of Dual Port LUT ram now done through explicit
// parameter setting.
RAM64X1D#(.INIT(64'h0000000000000000))RAM64X1D_instance_3556(.WCLK(CLK), .WE(wea_1), 
  .A0(ADDRA[0]), .A1(ADDRA[1]), .A2(ADDRA[2]), .A3(ADDRA[3]), .A4(ADDRA[4]), .A5(ADDRA[5]), 
  .D(DINA[4]), .SPO(pre_douta_1[4]), .DPRA0(ADDRB[0]), .DPRA1(ADDRB[1]), .DPRA2(ADDRB[2]), 
  .DPRA3(ADDRB[3]), .DPRA4(ADDRB[4]), .DPRA5(ADDRB[5]), .DPO(pre_doutb_1[4]));
// Memory array element: COL: 1, ROW: 5
//  Initialization of Dual Port LUT ram now done through explicit
// parameter setting.
RAM64X1D#(.INIT(64'h0000000000000000))RAM64X1D_instance_3557(.WCLK(CLK), .WE(wea_1), 
  .A0(ADDRA[0]), .A1(ADDRA[1]), .A2(ADDRA[2]), .A3(ADDRA[3]), .A4(ADDRA[4]), .A5(ADDRA[5]), 
  .D(DINA[5]), .SPO(pre_douta_1[5]), .DPRA0(ADDRB[0]), .DPRA1(ADDRB[1]), .DPRA2(ADDRB[2]), 
  .DPRA3(ADDRB[3]), .DPRA4(ADDRB[4]), .DPRA5(ADDRB[5]), .DPO(pre_doutb_1[5]));
// Memory array element: COL: 1, ROW: 6
//  Initialization of Dual Port LUT ram now done through explicit
// parameter setting.
RAM64X1D#(.INIT(64'h0000000000000000))RAM64X1D_instance_3558(.WCLK(CLK), .WE(wea_1), 
  .A0(ADDRA[0]), .A1(ADDRA[1]), .A2(ADDRA[2]), .A3(ADDRA[3]), .A4(ADDRA[4]), .A5(ADDRA[5]), 
  .D(DINA[6]), .SPO(pre_douta_1[6]), .DPRA0(ADDRB[0]), .DPRA1(ADDRB[1]), .DPRA2(ADDRB[2]), 
  .DPRA3(ADDRB[3]), .DPRA4(ADDRB[4]), .DPRA5(ADDRB[5]), .DPO(pre_doutb_1[6]));
// Memory array element: COL: 1, ROW: 7
//  Initialization of Dual Port LUT ram now done through explicit
// parameter setting.
RAM64X1D#(.INIT(64'h0000000000000000))RAM64X1D_instance_3559(.WCLK(CLK), .WE(wea_1), 
  .A0(ADDRA[0]), .A1(ADDRA[1]), .A2(ADDRA[2]), .A3(ADDRA[3]), .A4(ADDRA[4]), .A5(ADDRA[5]), 
  .D(DINA[7]), .SPO(pre_douta_1[7]), .DPRA0(ADDRB[0]), .DPRA1(ADDRB[1]), .DPRA2(ADDRB[2]), 
  .DPRA3(ADDRB[3]), .DPRA4(ADDRB[4]), .DPRA5(ADDRB[5]), .DPO(pre_doutb_1[7]));
// Memory array element: COL: 1, ROW: 8
//  Initialization of Dual Port LUT ram now done through explicit
// parameter setting.
RAM64X1D#(.INIT(64'h0000000000000000))RAM64X1D_instance_3560(.WCLK(CLK), .WE(wea_1), 
  .A0(ADDRA[0]), .A1(ADDRA[1]), .A2(ADDRA[2]), .A3(ADDRA[3]), .A4(ADDRA[4]), .A5(ADDRA[5]), 
  .D(DINA[8]), .SPO(pre_douta_1[8]), .DPRA0(ADDRB[0]), .DPRA1(ADDRB[1]), .DPRA2(ADDRB[2]), 
  .DPRA3(ADDRB[3]), .DPRA4(ADDRB[4]), .DPRA5(ADDRB[5]), .DPO(pre_doutb_1[8]));
// Memory array element: COL: 1, ROW: 9
//  Initialization of Dual Port LUT ram now done through explicit
// parameter setting.
RAM64X1D#(.INIT(64'h0000000000000000))RAM64X1D_instance_3561(.WCLK(CLK), .WE(wea_1), 
  .A0(ADDRA[0]), .A1(ADDRA[1]), .A2(ADDRA[2]), .A3(ADDRA[3]), .A4(ADDRA[4]), .A5(ADDRA[5]), 
  .D(DINA[9]), .SPO(pre_douta_1[9]), .DPRA0(ADDRB[0]), .DPRA1(ADDRB[1]), .DPRA2(ADDRB[2]), 
  .DPRA3(ADDRB[3]), .DPRA4(ADDRB[4]), .DPRA5(ADDRB[5]), .DPO(pre_doutb_1[9]));
// Memory array element: COL: 1, ROW: 10
//  Initialization of Dual Port LUT ram now done through explicit
// parameter setting.
RAM64X1D#(.INIT(64'h0000000000000000))RAM64X1D_instance_3562(.WCLK(CLK), .WE(wea_1), 
  .A0(ADDRA[0]), .A1(ADDRA[1]), .A2(ADDRA[2]), .A3(ADDRA[3]), .A4(ADDRA[4]), .A5(ADDRA[5]), 
  .D(DINA[10]), .SPO(pre_douta_1[10]), .DPRA0(ADDRB[0]), .DPRA1(ADDRB[1]), .DPRA2(ADDRB[2]), 
  .DPRA3(ADDRB[3]), .DPRA4(ADDRB[4]), .DPRA5(ADDRB[5]), .DPO(pre_doutb_1[10]));
// Memory array element: COL: 1, ROW: 11
//  Initialization of Dual Port LUT ram now done through explicit
// parameter setting.
RAM64X1D#(.INIT(64'h0000000000000000))RAM64X1D_instance_3563(.WCLK(CLK), .WE(wea_1), 
  .A0(ADDRA[0]), .A1(ADDRA[1]), .A2(ADDRA[2]), .A3(ADDRA[3]), .A4(ADDRA[4]), .A5(ADDRA[5]), 
  .D(DINA[11]), .SPO(pre_douta_1[11]), .DPRA0(ADDRB[0]), .DPRA1(ADDRB[1]), .DPRA2(ADDRB[2]), 
  .DPRA3(ADDRB[3]), .DPRA4(ADDRB[4]), .DPRA5(ADDRB[5]), .DPO(pre_doutb_1[11]));
// Memory array element: COL: 1, ROW: 12
//  Initialization of Dual Port LUT ram now done through explicit
// parameter setting.
RAM64X1D#(.INIT(64'h0000000000000000))RAM64X1D_instance_3564(.WCLK(CLK), .WE(wea_1), 
  .A0(ADDRA[0]), .A1(ADDRA[1]), .A2(ADDRA[2]), .A3(ADDRA[3]), .A4(ADDRA[4]), .A5(ADDRA[5]), 
  .D(DINA[12]), .SPO(pre_douta_1[12]), .DPRA0(ADDRB[0]), .DPRA1(ADDRB[1]), .DPRA2(ADDRB[2]), 
  .DPRA3(ADDRB[3]), .DPRA4(ADDRB[4]), .DPRA5(ADDRB[5]), .DPO(pre_doutb_1[12]));
// Memory array element: COL: 1, ROW: 13
//  Initialization of Dual Port LUT ram now done through explicit
// parameter setting.
RAM64X1D#(.INIT(64'h0000000000000000))RAM64X1D_instance_3565(.WCLK(CLK), .WE(wea_1), 
  .A0(ADDRA[0]), .A1(ADDRA[1]), .A2(ADDRA[2]), .A3(ADDRA[3]), .A4(ADDRA[4]), .A5(ADDRA[5]), 
  .D(DINA[13]), .SPO(pre_douta_1[13]), .DPRA0(ADDRB[0]), .DPRA1(ADDRB[1]), .DPRA2(ADDRB[2]), 
  .DPRA3(ADDRB[3]), .DPRA4(ADDRB[4]), .DPRA5(ADDRB[5]), .DPO(pre_doutb_1[13]));
// Memory array element: COL: 1, ROW: 14
//  Initialization of Dual Port LUT ram now done through explicit
// parameter setting.
RAM64X1D#(.INIT(64'h0000000000000000))RAM64X1D_instance_3566(.WCLK(CLK), .WE(wea_1), 
  .A0(ADDRA[0]), .A1(ADDRA[1]), .A2(ADDRA[2]), .A3(ADDRA[3]), .A4(ADDRA[4]), .A5(ADDRA[5]), 
  .D(DINA[14]), .SPO(pre_douta_1[14]), .DPRA0(ADDRB[0]), .DPRA1(ADDRB[1]), .DPRA2(ADDRB[2]), 
  .DPRA3(ADDRB[3]), .DPRA4(ADDRB[4]), .DPRA5(ADDRB[5]), .DPO(pre_doutb_1[14]));
// Memory array element: COL: 1, ROW: 15
//  Initialization of Dual Port LUT ram now done through explicit
// parameter setting.
RAM64X1D#(.INIT(64'h0000000000000000))RAM64X1D_instance_3567(.WCLK(CLK), .WE(wea_1), 
  .A0(ADDRA[0]), .A1(ADDRA[1]), .A2(ADDRA[2]), .A3(ADDRA[3]), .A4(ADDRA[4]), .A5(ADDRA[5]), 
  .D(DINA[15]), .SPO(pre_douta_1[15]), .DPRA0(ADDRB[0]), .DPRA1(ADDRB[1]), .DPRA2(ADDRB[2]), 
  .DPRA3(ADDRB[3]), .DPRA4(ADDRB[4]), .DPRA5(ADDRB[5]), .DPO(pre_doutb_1[15]));
// Memory array element: COL: 1, ROW: 16
//  Initialization of Dual Port LUT ram now done through explicit
// parameter setting.
RAM64X1D#(.INIT(64'h0000000000000000))RAM64X1D_instance_3568(.WCLK(CLK), .WE(wea_1), 
  .A0(ADDRA[0]), .A1(ADDRA[1]), .A2(ADDRA[2]), .A3(ADDRA[3]), .A4(ADDRA[4]), .A5(ADDRA[5]), 
  .D(DINA[16]), .SPO(pre_douta_1[16]), .DPRA0(ADDRB[0]), .DPRA1(ADDRB[1]), .DPRA2(ADDRB[2]), 
  .DPRA3(ADDRB[3]), .DPRA4(ADDRB[4]), .DPRA5(ADDRB[5]), .DPO(pre_doutb_1[16]));
// Memory array element: COL: 1, ROW: 17
//  Initialization of Dual Port LUT ram now done through explicit
// parameter setting.
RAM64X1D#(.INIT(64'h0000000000000000))RAM64X1D_instance_3569(.WCLK(CLK), .WE(wea_1), 
  .A0(ADDRA[0]), .A1(ADDRA[1]), .A2(ADDRA[2]), .A3(ADDRA[3]), .A4(ADDRA[4]), .A5(ADDRA[5]), 
  .D(DINA[17]), .SPO(pre_douta_1[17]), .DPRA0(ADDRB[0]), .DPRA1(ADDRB[1]), .DPRA2(ADDRB[2]), 
  .DPRA3(ADDRB[3]), .DPRA4(ADDRB[4]), .DPRA5(ADDRB[5]), .DPO(pre_doutb_1[17]));
// Memory array element: COL: 1, ROW: 18
//  Initialization of Dual Port LUT ram now done through explicit
// parameter setting.
RAM64X1D#(.INIT(64'h0000000000000000))RAM64X1D_instance_3570(.WCLK(CLK), .WE(wea_1), 
  .A0(ADDRA[0]), .A1(ADDRA[1]), .A2(ADDRA[2]), .A3(ADDRA[3]), .A4(ADDRA[4]), .A5(ADDRA[5]), 
  .D(DINA[18]), .SPO(pre_douta_1[18]), .DPRA0(ADDRB[0]), .DPRA1(ADDRB[1]), .DPRA2(ADDRB[2]), 
  .DPRA3(ADDRB[3]), .DPRA4(ADDRB[4]), .DPRA5(ADDRB[5]), .DPO(pre_doutb_1[18]));
// Memory array element: COL: 1, ROW: 19
//  Initialization of Dual Port LUT ram now done through explicit
// parameter setting.
RAM64X1D#(.INIT(64'h0000000000000000))RAM64X1D_instance_3571(.WCLK(CLK), .WE(wea_1), 
  .A0(ADDRA[0]), .A1(ADDRA[1]), .A2(ADDRA[2]), .A3(ADDRA[3]), .A4(ADDRA[4]), .A5(ADDRA[5]), 
  .D(DINA[19]), .SPO(pre_douta_1[19]), .DPRA0(ADDRB[0]), .DPRA1(ADDRB[1]), .DPRA2(ADDRB[2]), 
  .DPRA3(ADDRB[3]), .DPRA4(ADDRB[4]), .DPRA5(ADDRB[5]), .DPO(pre_doutb_1[19]));
// Memory array element: COL: 1, ROW: 20
//  Initialization of Dual Port LUT ram now done through explicit
// parameter setting.
RAM64X1D#(.INIT(64'h0000000000000000))RAM64X1D_instance_3572(.WCLK(CLK), .WE(wea_1), 
  .A0(ADDRA[0]), .A1(ADDRA[1]), .A2(ADDRA[2]), .A3(ADDRA[3]), .A4(ADDRA[4]), .A5(ADDRA[5]), 
  .D(DINA[20]), .SPO(pre_douta_1[20]), .DPRA0(ADDRB[0]), .DPRA1(ADDRB[1]), .DPRA2(ADDRB[2]), 
  .DPRA3(ADDRB[3]), .DPRA4(ADDRB[4]), .DPRA5(ADDRB[5]), .DPO(pre_doutb_1[20]));
// Memory array element: COL: 1, ROW: 21
//  Initialization of Dual Port LUT ram now done through explicit
// parameter setting.
RAM64X1D#(.INIT(64'h0000000000000000))RAM64X1D_instance_3573(.WCLK(CLK), .WE(wea_1), 
  .A0(ADDRA[0]), .A1(ADDRA[1]), .A2(ADDRA[2]), .A3(ADDRA[3]), .A4(ADDRA[4]), .A5(ADDRA[5]), 
  .D(DINA[21]), .SPO(pre_douta_1[21]), .DPRA0(ADDRB[0]), .DPRA1(ADDRB[1]), .DPRA2(ADDRB[2]), 
  .DPRA3(ADDRB[3]), .DPRA4(ADDRB[4]), .DPRA5(ADDRB[5]), .DPO(pre_doutb_1[21]));
// Memory array element: COL: 1, ROW: 22
//  Initialization of Dual Port LUT ram now done through explicit
// parameter setting.
RAM64X1D#(.INIT(64'h0000000000000000))RAM64X1D_instance_3574(.WCLK(CLK), .WE(wea_1), 
  .A0(ADDRA[0]), .A1(ADDRA[1]), .A2(ADDRA[2]), .A3(ADDRA[3]), .A4(ADDRA[4]), .A5(ADDRA[5]), 
  .D(DINA[22]), .SPO(pre_douta_1[22]), .DPRA0(ADDRB[0]), .DPRA1(ADDRB[1]), .DPRA2(ADDRB[2]), 
  .DPRA3(ADDRB[3]), .DPRA4(ADDRB[4]), .DPRA5(ADDRB[5]), .DPO(pre_doutb_1[22]));
// Memory array element: COL: 1, ROW: 23
//  Initialization of Dual Port LUT ram now done through explicit
// parameter setting.
RAM64X1D#(.INIT(64'h0000000000000000))RAM64X1D_instance_3575(.WCLK(CLK), .WE(wea_1), 
  .A0(ADDRA[0]), .A1(ADDRA[1]), .A2(ADDRA[2]), .A3(ADDRA[3]), .A4(ADDRA[4]), .A5(ADDRA[5]), 
  .D(DINA[23]), .SPO(pre_douta_1[23]), .DPRA0(ADDRB[0]), .DPRA1(ADDRB[1]), .DPRA2(ADDRB[2]), 
  .DPRA3(ADDRB[3]), .DPRA4(ADDRB[4]), .DPRA5(ADDRB[5]), .DPO(pre_doutb_1[23]));
// Memory array element: COL: 1, ROW: 24
//  Initialization of Dual Port LUT ram now done through explicit
// parameter setting.
RAM64X1D#(.INIT(64'h0000000000000000))RAM64X1D_instance_3576(.WCLK(CLK), .WE(wea_1), 
  .A0(ADDRA[0]), .A1(ADDRA[1]), .A2(ADDRA[2]), .A3(ADDRA[3]), .A4(ADDRA[4]), .A5(ADDRA[5]), 
  .D(DINA[24]), .SPO(pre_douta_1[24]), .DPRA0(ADDRB[0]), .DPRA1(ADDRB[1]), .DPRA2(ADDRB[2]), 
  .DPRA3(ADDRB[3]), .DPRA4(ADDRB[4]), .DPRA5(ADDRB[5]), .DPO(pre_doutb_1[24]));
// Memory array element: COL: 1, ROW: 25
//  Initialization of Dual Port LUT ram now done through explicit
// parameter setting.
RAM64X1D#(.INIT(64'h0000000000000000))RAM64X1D_instance_3577(.WCLK(CLK), .WE(wea_1), 
  .A0(ADDRA[0]), .A1(ADDRA[1]), .A2(ADDRA[2]), .A3(ADDRA[3]), .A4(ADDRA[4]), .A5(ADDRA[5]), 
  .D(DINA[25]), .SPO(pre_douta_1[25]), .DPRA0(ADDRB[0]), .DPRA1(ADDRB[1]), .DPRA2(ADDRB[2]), 
  .DPRA3(ADDRB[3]), .DPRA4(ADDRB[4]), .DPRA5(ADDRB[5]), .DPO(pre_doutb_1[25]));
// Memory array element: COL: 1, ROW: 26
//  Initialization of Dual Port LUT ram now done through explicit
// parameter setting.
RAM64X1D#(.INIT(64'h0000000000000000))RAM64X1D_instance_3578(.WCLK(CLK), .WE(wea_1), 
  .A0(ADDRA[0]), .A1(ADDRA[1]), .A2(ADDRA[2]), .A3(ADDRA[3]), .A4(ADDRA[4]), .A5(ADDRA[5]), 
  .D(DINA[26]), .SPO(pre_douta_1[26]), .DPRA0(ADDRB[0]), .DPRA1(ADDRB[1]), .DPRA2(ADDRB[2]), 
  .DPRA3(ADDRB[3]), .DPRA4(ADDRB[4]), .DPRA5(ADDRB[5]), .DPO(pre_doutb_1[26]));
// Memory array element: COL: 1, ROW: 27
//  Initialization of Dual Port LUT ram now done through explicit
// parameter setting.
RAM64X1D#(.INIT(64'h0000000000000000))RAM64X1D_instance_3579(.WCLK(CLK), .WE(wea_1), 
  .A0(ADDRA[0]), .A1(ADDRA[1]), .A2(ADDRA[2]), .A3(ADDRA[3]), .A4(ADDRA[4]), .A5(ADDRA[5]), 
  .D(DINA[27]), .SPO(pre_douta_1[27]), .DPRA0(ADDRB[0]), .DPRA1(ADDRB[1]), .DPRA2(ADDRB[2]), 
  .DPRA3(ADDRB[3]), .DPRA4(ADDRB[4]), .DPRA5(ADDRB[5]), .DPO(pre_doutb_1[27]));
// Memory array element: COL: 1, ROW: 28
//  Initialization of Dual Port LUT ram now done through explicit
// parameter setting.
RAM64X1D#(.INIT(64'h0000000000000000))RAM64X1D_instance_3580(.WCLK(CLK), .WE(wea_1), 
  .A0(ADDRA[0]), .A1(ADDRA[1]), .A2(ADDRA[2]), .A3(ADDRA[3]), .A4(ADDRA[4]), .A5(ADDRA[5]), 
  .D(DINA[28]), .SPO(pre_douta_1[28]), .DPRA0(ADDRB[0]), .DPRA1(ADDRB[1]), .DPRA2(ADDRB[2]), 
  .DPRA3(ADDRB[3]), .DPRA4(ADDRB[4]), .DPRA5(ADDRB[5]), .DPO(pre_doutb_1[28]));
// Memory array element: COL: 1, ROW: 29
//  Initialization of Dual Port LUT ram now done through explicit
// parameter setting.
RAM64X1D#(.INIT(64'h0000000000000000))RAM64X1D_instance_3581(.WCLK(CLK), .WE(wea_1), 
  .A0(ADDRA[0]), .A1(ADDRA[1]), .A2(ADDRA[2]), .A3(ADDRA[3]), .A4(ADDRA[4]), .A5(ADDRA[5]), 
  .D(DINA[29]), .SPO(pre_douta_1[29]), .DPRA0(ADDRB[0]), .DPRA1(ADDRB[1]), .DPRA2(ADDRB[2]), 
  .DPRA3(ADDRB[3]), .DPRA4(ADDRB[4]), .DPRA5(ADDRB[5]), .DPO(pre_doutb_1[29]));
// Memory array element: COL: 1, ROW: 30
//  Initialization of Dual Port LUT ram now done through explicit
// parameter setting.
RAM64X1D#(.INIT(64'h0000000000000000))RAM64X1D_instance_3582(.WCLK(CLK), .WE(wea_1), 
  .A0(ADDRA[0]), .A1(ADDRA[1]), .A2(ADDRA[2]), .A3(ADDRA[3]), .A4(ADDRA[4]), .A5(ADDRA[5]), 
  .D(DINA[30]), .SPO(pre_douta_1[30]), .DPRA0(ADDRB[0]), .DPRA1(ADDRB[1]), .DPRA2(ADDRB[2]), 
  .DPRA3(ADDRB[3]), .DPRA4(ADDRB[4]), .DPRA5(ADDRB[5]), .DPO(pre_doutb_1[30]));
// Memory array element: COL: 1, ROW: 31
//  Initialization of Dual Port LUT ram now done through explicit
// parameter setting.
RAM64X1D#(.INIT(64'h0000000000000000))RAM64X1D_instance_3583(.WCLK(CLK), .WE(wea_1), 
  .A0(ADDRA[0]), .A1(ADDRA[1]), .A2(ADDRA[2]), .A3(ADDRA[3]), .A4(ADDRA[4]), .A5(ADDRA[5]), 
  .D(DINA[31]), .SPO(pre_douta_1[31]), .DPRA0(ADDRB[0]), .DPRA1(ADDRB[1]), .DPRA2(ADDRB[2]), 
  .DPRA3(ADDRB[3]), .DPRA4(ADDRB[4]), .DPRA5(ADDRB[5]), .DPO(pre_doutb_1[31]));
endmodule



module medianRow7_structuralmemory_0d65dcc8_(CLK_u101, bus_11d9afd8_, bus_1b355fb8_, bus_139162df_, bus_7f5b5d16_, bus_1052d309_, bus_716701ce_, bus_06454314_, bus_5d904797_, bus_144c361d_, bus_79c19c10_, bus_3fbd0408_, bus_7014d17a_, bus_6a69f8cf_);
input		CLK_u101;
input		bus_11d9afd8_;
input	[31:0]	bus_1b355fb8_;
input	[2:0]	bus_139162df_;
input		bus_7f5b5d16_;
input	[31:0]	bus_1052d309_;
input	[2:0]	bus_716701ce_;
input		bus_06454314_;
input		bus_5d904797_;
input	[31:0]	bus_144c361d_;
output	[31:0]	bus_79c19c10_;
output		bus_3fbd0408_;
output	[31:0]	bus_7014d17a_;
output		bus_6a69f8cf_;
wire	[31:0]	bus_62fb88bf_;
wire	[31:0]	bus_48c98c5f_;
wire		or_1d6ab82b_u0;
wire		not_3fedeb29_u0;
wire		and_7b1dbeb6_u0;
wire		or_06e6c756_u0;
reg		logicalMem_6120fc59_we_delay0_u0=1'h0;
medianRow7_forge_memory_101x32_163 medianRow7_forge_memory_101x32_163_instance0(.CLK(CLK_u101), 
  .ENA(or_1d6ab82b_u0), .WEA(bus_5d904797_), .DINA(bus_144c361d_), .ADDRA(bus_1052d309_), 
  .DOUTA(bus_62fb88bf_), .DONEA(), .ENB(bus_7f5b5d16_), .ADDRB(bus_1b355fb8_), .DOUTB(bus_48c98c5f_), 
  .DONEB());
assign or_1d6ab82b_u0=bus_06454314_|bus_5d904797_;
assign not_3fedeb29_u0=~bus_5d904797_;
assign and_7b1dbeb6_u0=bus_06454314_&not_3fedeb29_u0;
assign or_06e6c756_u0=and_7b1dbeb6_u0|logicalMem_6120fc59_we_delay0_u0;
always @(posedge CLK_u101 or posedge bus_11d9afd8_)
begin
if (bus_11d9afd8_)
logicalMem_6120fc59_we_delay0_u0<=1'h0;
else logicalMem_6120fc59_we_delay0_u0<=bus_5d904797_;
end
assign bus_79c19c10_=bus_48c98c5f_;
assign bus_3fbd0408_=bus_7f5b5d16_;
assign bus_7014d17a_=bus_62fb88bf_;
assign bus_6a69f8cf_=or_06e6c756_u0;
endmodule



module medianRow7_compute_median(CLK, RESET, GO, port_003ab112_, port_336f8840_, port_1a1ed03d_, port_293a8b14_, port_3f171fb5_, RESULT, RESULT_u2435, RESULT_u2436, RESULT_u2437, RESULT_u2438, RESULT_u2439, RESULT_u2440, RESULT_u2441, RESULT_u2442, RESULT_u2443, RESULT_u2444, RESULT_u2445, RESULT_u2446, RESULT_u2447, RESULT_u2448, DONE);
input		CLK;
input		RESET;
input		GO;
input		port_003ab112_;
input	[31:0]	port_336f8840_;
input		port_1a1ed03d_;
input	[31:0]	port_293a8b14_;
input		port_3f171fb5_;
output		RESULT;
output	[31:0]	RESULT_u2435;
output		RESULT_u2436;
output	[31:0]	RESULT_u2437;
output	[2:0]	RESULT_u2438;
output		RESULT_u2439;
output	[31:0]	RESULT_u2440;
output	[2:0]	RESULT_u2441;
output		RESULT_u2442;
output	[31:0]	RESULT_u2443;
output	[31:0]	RESULT_u2444;
output	[2:0]	RESULT_u2445;
output	[15:0]	RESULT_u2446;
output		RESULT_u2447;
output	[7:0]	RESULT_u2448;
output		DONE;
wire		and_u4161_u0;
reg		syncEnable_u1608=1'h0;
wire		and_u4162_u0;
wire	[31:0]	add;
wire		and_u4163_u0;
wire	[31:0]	add_u772;
wire	[31:0]	add_u773;
wire		and_u4164_u0;
wire signed	[31:0]	greaterThan_a_signed;
wire signed	[31:0]	greaterThan_b_signed;
wire		greaterThan;
wire		and_u4165_u0;
wire		not_u963_u0;
wire		and_u4166_u0;
wire	[31:0]	add_u774;
wire		and_u4167_u0;
wire	[31:0]	add_u775;
wire	[31:0]	add_u776;
wire		and_u4168_u0;
wire	[31:0]	add_u777;
wire		and_u4169_u0;
wire		or_u1607_u0;
reg		reg_13dd0d32_u0=1'h0;
wire	[31:0]	add_u778;
wire	[31:0]	add_u779;
reg		reg_57e3cc46_u0=1'h0;
wire		and_u4170_u0;
wire		or_u1608_u0;
reg	[31:0]	syncEnable_u1609_u0=32'h0;
reg	[31:0]	syncEnable_u1610_u0=32'h0;
reg	[31:0]	syncEnable_u1611_u0=32'h0;
wire	[31:0]	mux_u2119;
wire		or_u1609_u0;
wire	[31:0]	mux_u2120_u0;
reg	[31:0]	syncEnable_u1612_u0=32'h0;
reg		reg_2885abee_u0=1'h0;
reg		block_GO_delayed_u116=1'h0;
reg	[31:0]	syncEnable_u1613_u0=32'h0;
reg	[31:0]	syncEnable_u1614_u0=32'h0;
reg		reg_106cd6ec_u0=1'h0;
wire		and_u4171_u0;
reg		and_delayed_u417=1'h0;
reg		reg_0a42d144_u0=1'h0;
wire	[31:0]	mux_u2121_u0;
wire		mux_u2122_u0;
reg	[31:0]	syncEnable_u1615_u0=32'h0;
reg	[31:0]	syncEnable_u1616_u0=32'h0;
wire		or_u1610_u0;
reg		reg_106cd6ec_result_delayed_u0=1'h0;
reg		syncEnable_u1617_u0=1'h0;
wire	[31:0]	mux_u2123_u0;
wire		and_u4172_u0;
reg		and_delayed_u418_u0=1'h0;
wire	[31:0]	add_u780;
reg	[31:0]	syncEnable_u1618_u0=32'h0;
reg	[31:0]	syncEnable_u1619_u0=32'h0;
wire	[31:0]	mux_u2124_u0;
wire		or_u1611_u0;
reg		block_GO_delayed_u117_u0=1'h0;
reg	[31:0]	syncEnable_u1620_u0=32'h0;
reg	[31:0]	syncEnable_u1621_u0=32'h0;
wire	[31:0]	mux_u2125_u0;
wire		or_u1612_u0;
reg	[31:0]	syncEnable_u1622_u0=32'h0;
reg	[31:0]	syncEnable_u1623_u0=32'h0;
reg		syncEnable_u1624_u0=1'h0;
wire		lessThan;
wire signed	[32:0]	lessThan_b_signed;
wire signed	[32:0]	lessThan_a_signed;
wire		and_u4173_u0;
wire		and_u4174_u0;
wire		not_u964_u0;
wire	[31:0]	mux_u2126_u0;
wire		or_u1613_u0;
wire	[31:0]	mux_u2127_u0;
wire		mux_u2128_u0;
wire	[31:0]	mux_u2129_u0;
wire	[31:0]	mux_u2130_u0;
wire	[31:0]	mux_u2131_u0;
wire	[15:0]	add_u781;
reg	[31:0]	latch_320088c7_reg=32'h0;
wire	[31:0]	latch_320088c7_out;
reg		reg_6300d2e9_u0=1'h0;
wire	[31:0]	latch_4e8bc0e7_out;
reg	[31:0]	latch_4e8bc0e7_reg=32'h0;
reg		latch_314ab7e5_reg=1'h0;
wire		latch_314ab7e5_out;
reg	[15:0]	syncEnable_u1625_u0=16'h0;
reg	[31:0]	latch_27312944_reg=32'h0;
wire	[31:0]	latch_27312944_out;
wire		bus_23873d86_;
wire		scoreboard_775edf89_resOr1;
reg		scoreboard_775edf89_reg0=1'h0;
wire		scoreboard_775edf89_resOr0;
reg		scoreboard_775edf89_reg1=1'h0;
wire		scoreboard_775edf89_and;
reg	[31:0]	latch_35062e5c_reg=32'h0;
wire	[31:0]	latch_35062e5c_out;
wire		and_u4175_u0;
wire		lessThan_u113;
wire	[15:0]	lessThan_u113_b_unsigned;
wire	[15:0]	lessThan_u113_a_unsigned;
wire		andOp;
wire		not_u965_u0;
wire		and_u4176_u0;
wire		and_u4177_u0;
wire	[31:0]	mux_u2132_u0;
wire		mux_u2133_u0;
reg	[31:0]	fbReg_tmp_u55=32'h0;
wire	[31:0]	mux_u2134_u0;
wire	[31:0]	mux_u2135_u0;
wire		or_u1614_u0;
reg		syncEnable_u1626_u0=1'h0;
reg		loopControl_u114=1'h0;
reg		fbReg_swapped_u55=1'h0;
reg	[31:0]	fbReg_tmp_row_u55=32'h0;
reg	[31:0]	fbReg_tmp_row0_u55=32'h0;
wire	[15:0]	mux_u2136_u0;
wire	[31:0]	mux_u2137_u0;
reg	[31:0]	fbReg_tmp_row1_u55=32'h0;
reg	[15:0]	fbReg_idx_u55=16'h0;
wire		and_u4178_u0;
wire		simplePinWrite;
wire	[7:0]	simplePinWrite_u658;
wire	[15:0]	simplePinWrite_u659;
wire		or_u1615_u0;
wire	[31:0]	mux_u2138_u0;
reg		reg_5b625fc1_u0=1'h0;
reg		reg_5b625fc1_result_delayed_u0=1'h0;
assign and_u4161_u0=and_u4177_u0&or_u1614_u0;
always @(posedge CLK)
begin
if (or_u1613_u0)
syncEnable_u1608<=mux_u2128_u0;
end
assign and_u4162_u0=and_u4173_u0&or_u1613_u0;
assign add=mux_u2126_u0+32'h0;
assign and_u4163_u0=and_u4162_u0&port_003ab112_;
assign add_u772=mux_u2126_u0+32'h1;
assign add_u773=add_u772+32'h0;
assign and_u4164_u0=and_u4162_u0&port_3f171fb5_;
assign greaterThan_a_signed=syncEnable_u1621_u0;
assign greaterThan_b_signed=syncEnable_u1618_u0;
assign greaterThan=greaterThan_a_signed>greaterThan_b_signed;
assign and_u4165_u0=block_GO_delayed_u117_u0&greaterThan;
assign not_u963_u0=~greaterThan;
assign and_u4166_u0=block_GO_delayed_u117_u0&not_u963_u0;
assign add_u774=syncEnable_u1620_u0+32'h0;
assign and_u4167_u0=and_u4172_u0&port_003ab112_;
assign add_u775=syncEnable_u1620_u0+32'h1;
assign add_u776=add_u775+32'h0;
assign and_u4168_u0=and_u4172_u0&port_3f171fb5_;
assign add_u777=syncEnable_u1620_u0+32'h0;
assign and_u4169_u0=reg_13dd0d32_u0&port_3f171fb5_;
assign or_u1607_u0=and_u4169_u0|RESET;
always @(posedge CLK or posedge block_GO_delayed_u116 or posedge or_u1607_u0)
begin
if (or_u1607_u0)
reg_13dd0d32_u0<=1'h0;
else if (block_GO_delayed_u116)
reg_13dd0d32_u0<=1'h1;
else reg_13dd0d32_u0<=reg_13dd0d32_u0;
end
assign add_u778=syncEnable_u1620_u0+32'h1;
assign add_u779=add_u778+32'h0;
always @(posedge CLK or posedge reg_2885abee_u0 or posedge or_u1608_u0)
begin
if (or_u1608_u0)
reg_57e3cc46_u0<=1'h0;
else if (reg_2885abee_u0)
reg_57e3cc46_u0<=1'h1;
else reg_57e3cc46_u0<=reg_57e3cc46_u0;
end
assign and_u4170_u0=reg_57e3cc46_u0&port_3f171fb5_;
assign or_u1608_u0=and_u4170_u0|RESET;
always @(posedge CLK)
begin
if (and_u4172_u0)
syncEnable_u1609_u0<=port_293a8b14_;
end
always @(posedge CLK)
begin
if (and_u4172_u0)
syncEnable_u1610_u0<=port_293a8b14_;
end
always @(posedge CLK)
begin
if (and_u4172_u0)
syncEnable_u1611_u0<=add_u779;
end
assign mux_u2119=({32{block_GO_delayed_u116}}&syncEnable_u1612_u0)|({32{reg_2885abee_u0}}&syncEnable_u1611_u0)|({32{and_u4172_u0}}&add_u776);
assign or_u1609_u0=block_GO_delayed_u116|reg_2885abee_u0;
assign mux_u2120_u0=(block_GO_delayed_u116)?syncEnable_u1610_u0:syncEnable_u1614_u0;
always @(posedge CLK)
begin
if (and_u4172_u0)
syncEnable_u1612_u0<=add_u777;
end
always @(posedge CLK or posedge RESET)
begin
if (RESET)
reg_2885abee_u0<=1'h0;
else reg_2885abee_u0<=block_GO_delayed_u116;
end
always @(posedge CLK or posedge RESET)
begin
if (RESET)
block_GO_delayed_u116<=1'h0;
else block_GO_delayed_u116<=and_u4172_u0;
end
always @(posedge CLK)
begin
if (and_u4172_u0)
syncEnable_u1613_u0<=port_336f8840_;
end
always @(posedge CLK)
begin
if (and_u4172_u0)
syncEnable_u1614_u0<=port_336f8840_;
end
always @(posedge CLK or posedge RESET)
begin
if (RESET)
reg_106cd6ec_u0<=1'h0;
else reg_106cd6ec_u0<=and_delayed_u418_u0;
end
assign and_u4171_u0=and_u4166_u0&block_GO_delayed_u117_u0;
always @(posedge CLK or posedge RESET)
begin
if (RESET)
and_delayed_u417<=1'h0;
else and_delayed_u417<=and_u4171_u0;
end
always @(posedge CLK or posedge RESET)
begin
if (RESET)
reg_0a42d144_u0<=1'h0;
else reg_0a42d144_u0<=reg_106cd6ec_result_delayed_u0;
end
assign mux_u2121_u0=(reg_0a42d144_u0)?syncEnable_u1613_u0:syncEnable_u1615_u0;
assign mux_u2122_u0=(reg_0a42d144_u0)?1'h1:syncEnable_u1617_u0;
always @(posedge CLK)
begin
if (block_GO_delayed_u117_u0)
syncEnable_u1615_u0<=syncEnable_u1623_u0;
end
always @(posedge CLK)
begin
if (block_GO_delayed_u117_u0)
syncEnable_u1616_u0<=syncEnable_u1622_u0;
end
assign or_u1610_u0=reg_0a42d144_u0|and_delayed_u417;
always @(posedge CLK or posedge RESET)
begin
if (RESET)
reg_106cd6ec_result_delayed_u0<=1'h0;
else reg_106cd6ec_result_delayed_u0<=reg_106cd6ec_u0;
end
always @(posedge CLK)
begin
if (block_GO_delayed_u117_u0)
syncEnable_u1617_u0<=syncEnable_u1624_u0;
end
assign mux_u2123_u0=(reg_0a42d144_u0)?syncEnable_u1609_u0:syncEnable_u1616_u0;
assign and_u4172_u0=and_u4165_u0&block_GO_delayed_u117_u0;
always @(posedge CLK or posedge RESET)
begin
if (RESET)
and_delayed_u418_u0<=1'h0;
else and_delayed_u418_u0<=and_u4172_u0;
end
assign add_u780=mux_u2126_u0+32'h1;
always @(posedge CLK)
begin
if (and_u4162_u0)
syncEnable_u1618_u0<=port_293a8b14_;
end
always @(posedge CLK)
begin
if (and_u4162_u0)
syncEnable_u1619_u0<=add_u780;
end
assign mux_u2124_u0=(and_u4162_u0)?add:add_u774;
assign or_u1611_u0=and_u4162_u0|and_u4172_u0;
always @(posedge CLK or posedge RESET)
begin
if (RESET)
block_GO_delayed_u117_u0<=1'h0;
else block_GO_delayed_u117_u0<=and_u4162_u0;
end
always @(posedge CLK)
begin
if (and_u4162_u0)
syncEnable_u1620_u0<=mux_u2126_u0;
end
always @(posedge CLK)
begin
if (and_u4162_u0)
syncEnable_u1621_u0<=port_336f8840_;
end
assign mux_u2125_u0=({32{or_u1609_u0}}&mux_u2119)|({32{and_u4162_u0}}&add_u773)|({32{and_u4172_u0}}&mux_u2119);
assign or_u1612_u0=and_u4162_u0|and_u4172_u0;
always @(posedge CLK)
begin
if (and_u4162_u0)
syncEnable_u1622_u0<=mux_u2127_u0;
end
always @(posedge CLK)
begin
if (and_u4162_u0)
syncEnable_u1623_u0<=mux_u2130_u0;
end
always @(posedge CLK)
begin
if (and_u4162_u0)
syncEnable_u1624_u0<=mux_u2128_u0;
end
assign lessThan_a_signed={1'b0, mux_u2126_u0};
assign lessThan_b_signed=33'h64;
assign lessThan=lessThan_a_signed<lessThan_b_signed;
assign and_u4173_u0=or_u1613_u0&lessThan;
assign and_u4174_u0=or_u1613_u0&not_u964_u0;
assign not_u964_u0=~lessThan;
assign mux_u2126_u0=(and_u4175_u0)?32'h0:syncEnable_u1619_u0;
assign or_u1613_u0=and_u4175_u0|or_u1610_u0;
assign mux_u2127_u0=(and_u4175_u0)?mux_u2137_u0:mux_u2123_u0;
assign mux_u2128_u0=(and_u4175_u0)?1'h0:mux_u2122_u0;
assign mux_u2129_u0=(and_u4175_u0)?mux_u2135_u0:syncEnable_u1618_u0;
assign mux_u2130_u0=(and_u4175_u0)?mux_u2134_u0:mux_u2121_u0;
assign mux_u2131_u0=(and_u4175_u0)?mux_u2132_u0:syncEnable_u1621_u0;
assign add_u781=mux_u2136_u0+16'h1;
always @(posedge CLK)
begin
if (or_u1610_u0)
latch_320088c7_reg<=syncEnable_u1618_u0;
end
assign latch_320088c7_out=(or_u1610_u0)?syncEnable_u1618_u0:latch_320088c7_reg;
always @(posedge CLK or posedge RESET)
begin
if (RESET)
reg_6300d2e9_u0<=1'h0;
else reg_6300d2e9_u0<=or_u1610_u0;
end
assign latch_4e8bc0e7_out=(or_u1610_u0)?mux_u2121_u0:latch_4e8bc0e7_reg;
always @(posedge CLK)
begin
if (or_u1610_u0)
latch_4e8bc0e7_reg<=mux_u2121_u0;
end
always @(posedge CLK)
begin
if (or_u1610_u0)
latch_314ab7e5_reg<=syncEnable_u1608;
end
assign latch_314ab7e5_out=(or_u1610_u0)?syncEnable_u1608:latch_314ab7e5_reg;
always @(posedge CLK)
begin
if (and_u4175_u0)
syncEnable_u1625_u0<=add_u781;
end
always @(posedge CLK)
begin
if (or_u1610_u0)
latch_27312944_reg<=mux_u2123_u0;
end
assign latch_27312944_out=(or_u1610_u0)?mux_u2123_u0:latch_27312944_reg;
assign bus_23873d86_=scoreboard_775edf89_and|RESET;
assign scoreboard_775edf89_resOr1=reg_6300d2e9_u0|scoreboard_775edf89_reg1;
always @(posedge CLK)
begin
if (bus_23873d86_)
scoreboard_775edf89_reg0<=1'h0;
else if (or_u1610_u0)
scoreboard_775edf89_reg0<=1'h1;
else scoreboard_775edf89_reg0<=scoreboard_775edf89_reg0;
end
assign scoreboard_775edf89_resOr0=or_u1610_u0|scoreboard_775edf89_reg0;
always @(posedge CLK)
begin
if (bus_23873d86_)
scoreboard_775edf89_reg1<=1'h0;
else if (reg_6300d2e9_u0)
scoreboard_775edf89_reg1<=1'h1;
else scoreboard_775edf89_reg1<=scoreboard_775edf89_reg1;
end
assign scoreboard_775edf89_and=scoreboard_775edf89_resOr0&scoreboard_775edf89_resOr1;
always @(posedge CLK)
begin
if (or_u1610_u0)
latch_35062e5c_reg<=syncEnable_u1621_u0;
end
assign latch_35062e5c_out=(or_u1610_u0)?syncEnable_u1621_u0:latch_35062e5c_reg;
assign and_u4175_u0=and_u4176_u0&or_u1614_u0;
assign lessThan_u113_a_unsigned=mux_u2136_u0;
assign lessThan_u113_b_unsigned=16'h65;
assign lessThan_u113=lessThan_u113_a_unsigned<lessThan_u113_b_unsigned;
assign andOp=lessThan_u113&mux_u2133_u0;
assign not_u965_u0=~andOp;
assign and_u4176_u0=or_u1614_u0&andOp;
assign and_u4177_u0=or_u1614_u0&not_u965_u0;
assign mux_u2132_u0=(loopControl_u114)?fbReg_tmp_row_u55:32'h0;
assign mux_u2133_u0=(loopControl_u114)?fbReg_swapped_u55:1'h0;
always @(posedge CLK)
begin
if (scoreboard_775edf89_and)
fbReg_tmp_u55<=latch_4e8bc0e7_out;
end
assign mux_u2134_u0=(loopControl_u114)?fbReg_tmp_u55:32'h0;
assign mux_u2135_u0=(loopControl_u114)?fbReg_tmp_row0_u55:32'h0;
assign or_u1614_u0=loopControl_u114|GO;
always @(posedge CLK)
begin
if (GO)
syncEnable_u1626_u0<=RESET;
end
always @(posedge CLK or posedge syncEnable_u1626_u0)
begin
if (syncEnable_u1626_u0)
loopControl_u114<=1'h0;
else loopControl_u114<=scoreboard_775edf89_and;
end
always @(posedge CLK)
begin
if (scoreboard_775edf89_and)
fbReg_swapped_u55<=latch_314ab7e5_out;
end
always @(posedge CLK)
begin
if (scoreboard_775edf89_and)
fbReg_tmp_row_u55<=latch_35062e5c_out;
end
always @(posedge CLK)
begin
if (scoreboard_775edf89_and)
fbReg_tmp_row0_u55<=latch_320088c7_out;
end
assign mux_u2136_u0=(loopControl_u114)?fbReg_idx_u55:16'h0;
assign mux_u2137_u0=(loopControl_u114)?fbReg_tmp_row1_u55:32'h0;
always @(posedge CLK)
begin
if (scoreboard_775edf89_and)
fbReg_tmp_row1_u55<=latch_27312944_out;
end
always @(posedge CLK)
begin
if (scoreboard_775edf89_and)
fbReg_idx_u55<=syncEnable_u1625_u0;
end
assign and_u4178_u0=reg_5b625fc1_u0&port_003ab112_;
assign simplePinWrite=reg_5b625fc1_u0&{1{reg_5b625fc1_u0}};
assign simplePinWrite_u658=port_336f8840_[7:0];
assign simplePinWrite_u659=16'h1&{16{1'h1}};
assign or_u1615_u0=or_u1611_u0|reg_5b625fc1_u0;
assign mux_u2138_u0=(or_u1611_u0)?mux_u2124_u0:32'h32;
always @(posedge CLK or posedge RESET)
begin
if (RESET)
reg_5b625fc1_u0<=1'h0;
else reg_5b625fc1_u0<=and_u4161_u0;
end
always @(posedge CLK or posedge RESET)
begin
if (RESET)
reg_5b625fc1_result_delayed_u0<=1'h0;
else reg_5b625fc1_result_delayed_u0<=reg_5b625fc1_u0;
end
assign RESULT=GO;
assign RESULT_u2435=32'h0;
assign RESULT_u2436=or_u1615_u0;
assign RESULT_u2437=mux_u2138_u0;
assign RESULT_u2438=3'h1;
assign RESULT_u2439=or_u1612_u0;
assign RESULT_u2440=mux_u2125_u0;
assign RESULT_u2441=3'h1;
assign RESULT_u2442=or_u1609_u0;
assign RESULT_u2443=mux_u2125_u0;
assign RESULT_u2444=mux_u2120_u0;
assign RESULT_u2445=3'h1;
assign RESULT_u2446=simplePinWrite_u659;
assign RESULT_u2447=simplePinWrite;
assign RESULT_u2448=simplePinWrite_u658;
assign DONE=reg_5b625fc1_result_delayed_u0;
endmodule



module medianRow7_scheduler(CLK, RESET, GO, port_6e4ae772_, port_282348bd_, port_47c2549e_, port_4a9db61a_, port_690a5e4b_, port_574be0de_, RESULT, RESULT_u2449, RESULT_u2450, RESULT_u2451, DONE);
input		CLK;
input		RESET;
input		GO;
input		port_6e4ae772_;
input	[31:0]	port_282348bd_;
input		port_47c2549e_;
input		port_4a9db61a_;
input		port_690a5e4b_;
input		port_574be0de_;
output		RESULT;
output		RESULT_u2449;
output		RESULT_u2450;
output		RESULT_u2451;
output		DONE;
wire signed	[31:0]	lessThan_b_signed;
wire		lessThan;
wire signed	[31:0]	lessThan_a_signed;
wire signed	[31:0]	equals_b_signed;
wire signed	[31:0]	equals_a_signed;
wire		equals;
wire signed	[31:0]	equals_u252_a_signed;
wire signed	[31:0]	equals_u252_b_signed;
wire		equals_u252;
wire		not_u966_u0;
wire		and_u4179_u0;
wire		and_u4180_u0;
wire		andOp;
wire		and_u4181_u0;
wire		and_u4182_u0;
wire		not_u967_u0;
wire		simplePinWrite;
wire		and_u4183_u0;
wire		and_u4184_u0;
wire signed	[31:0]	equals_u253_a_signed;
wire		equals_u253;
wire signed	[31:0]	equals_u253_b_signed;
wire		and_u4185_u0;
wire		and_u4186_u0;
wire		not_u968_u0;
wire		andOp_u89;
wire		and_u4187_u0;
wire		not_u969_u0;
wire		and_u4188_u0;
wire		simplePinWrite_u660;
wire		and_u4189_u0;
wire		not_u970_u0;
wire		and_u4190_u0;
wire		not_u971_u0;
wire		and_u4191_u0;
wire		and_u4192_u0;
wire		simplePinWrite_u661;
wire		or_u1616_u0;
reg		reg_139218c4_u0=1'h0;
wire		and_u4193_u0;
wire		and_u4194_u0;
wire		and_u4195_u0;
reg		reg_093922ae_u0=1'h0;
wire		and_u4196_u0;
wire		or_u1617_u0;
wire		and_u4197_u0;
wire		or_u1618_u0;
wire		mux_u2139;
wire		or_u1619_u0;
wire		and_u4198_u0;
reg		and_delayed_u419=1'h0;
reg		reg_3a1cbc19_u0=1'h0;
wire		or_u1620_u0;
wire		and_u4199_u0;
wire		and_u4200_u0;
wire		receive_go_merge;
wire		mux_u2140_u0;
wire		or_u1621_u0;
wire		and_u4201_u0;
reg		loopControl_u115=1'h0;
wire		or_u1622_u0;
reg		syncEnable_u1627=1'h0;
reg		reg_34c7399f_u0=1'h0;
reg		reg_34c7399f_result_delayed_u0=1'h0;
wire		or_u1623_u0;
wire		mux_u2141_u0;
assign lessThan_a_signed=port_282348bd_;
assign lessThan_b_signed=32'h65;
assign lessThan=lessThan_a_signed<lessThan_b_signed;
assign equals_a_signed=port_282348bd_;
assign equals_b_signed=32'h65;
assign equals=equals_a_signed==equals_b_signed;
assign equals_u252_a_signed={31'b0, port_6e4ae772_};
assign equals_u252_b_signed=32'h0;
assign equals_u252=equals_u252_a_signed==equals_u252_b_signed;
assign not_u966_u0=~equals_u252;
assign and_u4179_u0=and_u4201_u0&equals_u252;
assign and_u4180_u0=and_u4201_u0&not_u966_u0;
assign andOp=lessThan&port_47c2549e_;
assign and_u4181_u0=and_u4184_u0&not_u967_u0;
assign and_u4182_u0=and_u4184_u0&andOp;
assign not_u967_u0=~andOp;
assign simplePinWrite=and_u4183_u0&{1{and_u4183_u0}};
assign and_u4183_u0=and_u4182_u0&and_u4184_u0;
assign and_u4184_u0=and_u4179_u0&and_u4201_u0;
assign equals_u253_a_signed={31'b0, port_6e4ae772_};
assign equals_u253_b_signed=32'h1;
assign equals_u253=equals_u253_a_signed==equals_u253_b_signed;
assign and_u4185_u0=and_u4201_u0&not_u968_u0;
assign and_u4186_u0=and_u4201_u0&equals_u253;
assign not_u968_u0=~equals_u253;
assign andOp_u89=lessThan&port_47c2549e_;
assign and_u4187_u0=and_u4199_u0&andOp_u89;
assign not_u969_u0=~andOp_u89;
assign and_u4188_u0=and_u4199_u0&not_u969_u0;
assign simplePinWrite_u660=and_u4197_u0&{1{and_u4197_u0}};
assign and_u4189_u0=and_u4198_u0&not_u970_u0;
assign not_u970_u0=~equals;
assign and_u4190_u0=and_u4198_u0&equals;
assign not_u971_u0=~port_690a5e4b_;
assign and_u4191_u0=and_u4195_u0&port_690a5e4b_;
assign and_u4192_u0=and_u4195_u0&not_u971_u0;
assign simplePinWrite_u661=and_u4193_u0&{1{and_u4193_u0}};
assign or_u1616_u0=reg_139218c4_u0|port_574be0de_;
always @(posedge CLK or posedge RESET)
begin
if (RESET)
reg_139218c4_u0<=1'h0;
else reg_139218c4_u0<=and_u4194_u0;
end
assign and_u4193_u0=and_u4191_u0&and_u4195_u0;
assign and_u4194_u0=and_u4192_u0&and_u4195_u0;
assign and_u4195_u0=and_u4190_u0&and_u4198_u0;
always @(posedge CLK or posedge RESET)
begin
if (RESET)
reg_093922ae_u0<=1'h0;
else reg_093922ae_u0<=and_u4196_u0;
end
assign and_u4196_u0=and_u4189_u0&and_u4198_u0;
assign or_u1617_u0=reg_093922ae_u0|or_u1616_u0;
assign and_u4197_u0=and_u4187_u0&and_u4199_u0;
assign or_u1618_u0=and_delayed_u419|or_u1617_u0;
assign mux_u2139=(and_u4197_u0)?1'h1:1'h0;
assign or_u1619_u0=and_u4197_u0|and_u4193_u0;
assign and_u4198_u0=and_u4188_u0&and_u4199_u0;
always @(posedge CLK or posedge RESET)
begin
if (RESET)
and_delayed_u419<=1'h0;
else and_delayed_u419<=and_u4197_u0;
end
always @(posedge CLK or posedge RESET)
begin
if (RESET)
reg_3a1cbc19_u0<=1'h0;
else reg_3a1cbc19_u0<=and_u4200_u0;
end
assign or_u1620_u0=or_u1618_u0|reg_3a1cbc19_u0;
assign and_u4199_u0=and_u4186_u0&and_u4201_u0;
assign and_u4200_u0=and_u4185_u0&and_u4201_u0;
assign receive_go_merge=simplePinWrite|simplePinWrite_u660;
assign mux_u2140_u0=(and_u4183_u0)?1'h1:mux_u2139;
assign or_u1621_u0=and_u4183_u0|or_u1619_u0;
assign and_u4201_u0=or_u1622_u0&or_u1622_u0;
always @(posedge CLK or posedge syncEnable_u1627)
begin
if (syncEnable_u1627)
loopControl_u115<=1'h0;
else loopControl_u115<=or_u1620_u0;
end
assign or_u1622_u0=reg_34c7399f_result_delayed_u0|loopControl_u115;
always @(posedge CLK)
begin
if (reg_34c7399f_result_delayed_u0)
syncEnable_u1627<=RESET;
end
always @(posedge CLK or posedge RESET)
begin
if (RESET)
reg_34c7399f_u0<=1'h0;
else reg_34c7399f_u0<=GO;
end
always @(posedge CLK or posedge RESET)
begin
if (RESET)
reg_34c7399f_result_delayed_u0<=1'h0;
else reg_34c7399f_result_delayed_u0<=reg_34c7399f_u0;
end
assign or_u1623_u0=GO|or_u1621_u0;
assign mux_u2141_u0=(GO)?1'h0:mux_u2140_u0;
assign RESULT=or_u1623_u0;
assign RESULT_u2449=mux_u2141_u0;
assign RESULT_u2450=simplePinWrite_u661;
assign RESULT_u2451=receive_go_merge;
assign DONE=1'h0;
endmodule



module medianRow7_endianswapper_1fd10094_(endianswapper_1fd10094_in, endianswapper_1fd10094_out);
input		endianswapper_1fd10094_in;
output		endianswapper_1fd10094_out;
assign endianswapper_1fd10094_out=endianswapper_1fd10094_in;
endmodule



module medianRow7_endianswapper_16ac9bd2_(endianswapper_16ac9bd2_in, endianswapper_16ac9bd2_out);
input		endianswapper_16ac9bd2_in;
output		endianswapper_16ac9bd2_out;
assign endianswapper_16ac9bd2_out=endianswapper_16ac9bd2_in;
endmodule



module medianRow7_stateVar_fsmState_medianRow7(bus_76c75c5e_, bus_422b3039_, bus_6cb0eec1_, bus_3b682c6f_, bus_06d04b9d_);
input		bus_76c75c5e_;
input		bus_422b3039_;
input		bus_6cb0eec1_;
input		bus_3b682c6f_;
output		bus_06d04b9d_;
reg		stateVar_fsmState_medianRow7_u2=1'h0;
wire		endianswapper_1fd10094_out;
wire		endianswapper_16ac9bd2_out;
always @(posedge bus_76c75c5e_ or posedge bus_422b3039_)
begin
if (bus_422b3039_)
stateVar_fsmState_medianRow7_u2<=1'h0;
else if (bus_6cb0eec1_)
stateVar_fsmState_medianRow7_u2<=endianswapper_1fd10094_out;
end
medianRow7_endianswapper_1fd10094_ medianRow7_endianswapper_1fd10094__1(.endianswapper_1fd10094_in(bus_3b682c6f_), 
  .endianswapper_1fd10094_out(endianswapper_1fd10094_out));
assign bus_06d04b9d_=endianswapper_16ac9bd2_out;
medianRow7_endianswapper_16ac9bd2_ medianRow7_endianswapper_16ac9bd2__1(.endianswapper_16ac9bd2_in(stateVar_fsmState_medianRow7_u2), 
  .endianswapper_16ac9bd2_out(endianswapper_16ac9bd2_out));
endmodule



module medianRow7_globalreset_physical_3892246d_(bus_74c4d109_, bus_301e6f0d_, bus_4c9dfdd6_);
input		bus_74c4d109_;
input		bus_301e6f0d_;
output		bus_4c9dfdd6_;
reg		sample_u67=1'h0;
reg		glitch_u67=1'h0;
wire		not_54127048_u0;
reg		cross_u67=1'h0;
wire		and_324299a9_u0;
reg		final_u67=1'h1;
wire		or_344c30d0_u0;
always @(posedge bus_74c4d109_)
begin
sample_u67<=1'h1;
end
always @(posedge bus_74c4d109_)
begin
glitch_u67<=cross_u67;
end
assign not_54127048_u0=~and_324299a9_u0;
assign bus_4c9dfdd6_=or_344c30d0_u0;
always @(posedge bus_74c4d109_)
begin
cross_u67<=sample_u67;
end
assign and_324299a9_u0=cross_u67&glitch_u67;
always @(posedge bus_74c4d109_)
begin
final_u67<=not_54127048_u0;
end
assign or_344c30d0_u0=bus_301e6f0d_|final_u67;
endmodule


