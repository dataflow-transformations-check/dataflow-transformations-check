// __  ___ __ ___  _ __   ___  ___ 
// \ \/ / '__/ _ \| '_ \ / _ \/ __|
//  >  <| | | (_) | | | | (_) \__ \
// /_/\_\_|  \___/|_| |_|\___/|___/
// 
// Xronos synthesizer version
// Run date: Fri 23 Feb 2018 14:28:48 +0000
// 

module medianRow8(in1_SEND, median_DATA, median_SEND, in1_DATA, median_RDY, in1_ACK, in1_COUNT, RESET, median_COUNT, median_ACK, CLK);
input		in1_SEND;
output	[7:0]	median_DATA;
output		median_SEND;
wire		compute_median_go;
input	[7:0]	in1_DATA;
wire		receive_go;
wire		compute_median_done;
input		median_RDY;
output		in1_ACK;
input	[15:0]	in1_COUNT;
input		RESET;
output	[15:0]	median_COUNT;
wire		receive_done;
input		median_ACK;
input		CLK;
wire	[2:0]	bus_313b3687_;
wire		bus_64f01736_;
wire	[31:0]	bus_6acd6137_;
wire		bus_056d7d79_;
wire	[31:0]	bus_14c378f1_;
wire		bus_18b5b2b5_;
wire	[31:0]	bus_0e8fe0db_;
wire		bus_3682f818_;
wire		bus_4d969a0d_;
wire	[31:0]	bus_7f59bbbb_;
wire		bus_7089966d_;
wire	[31:0]	bus_21978536_;
wire		compute_median_u761;
wire		compute_median;
wire	[31:0]	compute_median_u765;
wire	[7:0]	compute_median_u768;
wire	[31:0]	compute_median_u758;
wire	[2:0]	compute_median_u760;
wire	[15:0]	compute_median_u767;
wire		compute_median_u764;
wire	[2:0]	compute_median_u766;
wire		compute_median_u757;
wire	[31:0]	compute_median_u762;
wire	[31:0]	compute_median_u756;
wire	[31:0]	compute_median_u759;
wire		compute_median_u769;
wire		medianRow8_compute_median_instance_DONE;
wire	[2:0]	compute_median_u763;
wire		receive_u329;
wire	[31:0]	receive_u324;
wire	[2:0]	receive_u328;
wire		receive_u325;
wire		receive;
wire		medianRow8_receive_instance_DONE;
wire	[31:0]	receive_u326;
wire	[31:0]	receive_u327;
wire		bus_0e8ca0a7_;
wire		bus_531ba8cc_;
wire	[31:0]	bus_6395f3b8_;
wire		bus_080ff39c_;
wire		bus_1d6f7ade_;
wire	[31:0]	bus_0a4f40f6_;
wire	[31:0]	bus_3908a74f_;
wire	[2:0]	bus_0d2495e6_;
wire		bus_14a6e8e9_;
wire	[31:0]	bus_1e7dd4de_;
wire		bus_223b8fad_;
wire		bus_505a8057_;
wire		bus_0f715aea_;
wire		medianRow8_scheduler_instance_DONE;
wire		scheduler_u839;
wire		scheduler_u838;
wire		scheduler;
wire		scheduler_u840;
assign median_DATA=compute_median_u768;
assign median_SEND=compute_median_u769;
assign compute_median_go=scheduler_u840;
assign receive_go=scheduler_u839;
assign compute_median_done=bus_3682f818_;
assign in1_ACK=receive_u329;
assign median_COUNT=compute_median_u767;
assign receive_done=bus_0f715aea_;
medianRow8_simplememoryreferee_5d612a62_ medianRow8_simplememoryreferee_5d612a62__1(.bus_2cf9bc04_(CLK), 
  .bus_7dea4842_(bus_0e8ca0a7_), .bus_7c50484d_(bus_4d969a0d_), .bus_532a5c09_(bus_7f59bbbb_), 
  .bus_022ae0fb_(compute_median_u761), .bus_10e3f4c5_(compute_median_u762), .bus_02f9251e_(3'h1), 
  .bus_0e8fe0db_(bus_0e8fe0db_), .bus_14c378f1_(bus_14c378f1_), .bus_64f01736_(bus_64f01736_), 
  .bus_056d7d79_(bus_056d7d79_), .bus_313b3687_(bus_313b3687_), .bus_6acd6137_(bus_6acd6137_), 
  .bus_18b5b2b5_(bus_18b5b2b5_));
assign bus_3682f818_=medianRow8_compute_median_instance_DONE&{1{medianRow8_compute_median_instance_DONE}};
medianRow8_structuralmemory_7819129f_ medianRow8_structuralmemory_7819129f__1(.CLK_u100(CLK), 
  .bus_2107881a_(bus_0e8ca0a7_), .bus_227bfe91_(bus_14c378f1_), .bus_26e4824a_(3'h1), 
  .bus_2e0e8d76_(bus_056d7d79_), .bus_7ad1fd75_(bus_3908a74f_), .bus_21a665a0_(3'h1), 
  .bus_2d808cfb_(bus_223b8fad_), .bus_3d6e29b1_(bus_14a6e8e9_), .bus_607d26b0_(bus_1e7dd4de_), 
  .bus_7f59bbbb_(bus_7f59bbbb_), .bus_4d969a0d_(bus_4d969a0d_), .bus_21978536_(bus_21978536_), 
  .bus_7089966d_(bus_7089966d_));
medianRow8_compute_median medianRow8_compute_median_instance(.CLK(CLK), .RESET(bus_0e8ca0a7_), 
  .GO(compute_median_go), .port_3c26653e_(bus_080ff39c_), .port_32a99403_(bus_18b5b2b5_), 
  .port_0b55d7e9_(bus_6acd6137_), .port_6f4d0b7d_(bus_080ff39c_), .port_649b4f89_(bus_0a4f40f6_), 
  .DONE(medianRow8_compute_median_instance_DONE), .RESULT(compute_median), .RESULT_u2406(compute_median_u756), 
  .RESULT_u2413(compute_median_u757), .RESULT_u2414(compute_median_u758), .RESULT_u2415(compute_median_u759), 
  .RESULT_u2416(compute_median_u760), .RESULT_u2407(compute_median_u761), .RESULT_u2408(compute_median_u762), 
  .RESULT_u2409(compute_median_u763), .RESULT_u2410(compute_median_u764), .RESULT_u2411(compute_median_u765), 
  .RESULT_u2412(compute_median_u766), .RESULT_u2417(compute_median_u767), .RESULT_u2418(compute_median_u768), 
  .RESULT_u2419(compute_median_u769));
medianRow8_receive medianRow8_receive_instance(.CLK(CLK), .RESET(bus_0e8ca0a7_), 
  .GO(receive_go), .port_114acc47_(bus_6395f3b8_), .port_61aea8ac_(bus_1d6f7ade_), 
  .port_1908964f_(in1_DATA), .DONE(medianRow8_receive_instance_DONE), .RESULT(receive), 
  .RESULT_u2420(receive_u324), .RESULT_u2421(receive_u325), .RESULT_u2422(receive_u326), 
  .RESULT_u2423(receive_u327), .RESULT_u2424(receive_u328), .RESULT_u2425(receive_u329));
medianRow8_globalreset_physical_6d3a41e3_ medianRow8_globalreset_physical_6d3a41e3__1(.bus_06b986b5_(CLK), 
  .bus_04b068fb_(RESET), .bus_0e8ca0a7_(bus_0e8ca0a7_));
medianRow8_Kicker_66 medianRow8_Kicker_66_1(.CLK(CLK), .RESET(bus_0e8ca0a7_), .bus_531ba8cc_(bus_531ba8cc_));
medianRow8_stateVar_i medianRow8_stateVar_i_1(.bus_3dc9d8cd_(CLK), .bus_72e7b464_(bus_0e8ca0a7_), 
  .bus_560d84ec_(receive), .bus_6afae50e_(receive_u324), .bus_1063f444_(compute_median), 
  .bus_01dbecb4_(32'h0), .bus_6395f3b8_(bus_6395f3b8_));
medianRow8_simplememoryreferee_47adb244_ medianRow8_simplememoryreferee_47adb244__1(.bus_6ea61510_(CLK), 
  .bus_2de8de13_(bus_0e8ca0a7_), .bus_4a6e6ce1_(bus_7089966d_), .bus_7d041b8f_(bus_21978536_), 
  .bus_1e26d567_(receive_u325), .bus_1133200f_({24'b0, receive_u327[7:0]}), .bus_2450d173_(receive_u326), 
  .bus_3c388326_(3'h1), .bus_6c107e20_(compute_median_u764), .bus_36e68cd5_(compute_median_u757), 
  .bus_19f8d507_(compute_median_u759), .bus_02cef7ce_(compute_median_u758), .bus_6a38488e_(3'h1), 
  .bus_1e7dd4de_(bus_1e7dd4de_), .bus_3908a74f_(bus_3908a74f_), .bus_14a6e8e9_(bus_14a6e8e9_), 
  .bus_223b8fad_(bus_223b8fad_), .bus_0d2495e6_(bus_0d2495e6_), .bus_1d6f7ade_(bus_1d6f7ade_), 
  .bus_0a4f40f6_(bus_0a4f40f6_), .bus_080ff39c_(bus_080ff39c_));
medianRow8_stateVar_fsmState_medianRow8 medianRow8_stateVar_fsmState_medianRow8_1(.bus_1d52540d_(CLK), 
  .bus_18af39b0_(bus_0e8ca0a7_), .bus_1eb2543a_(scheduler), .bus_70cc50ed_(scheduler_u838), 
  .bus_505a8057_(bus_505a8057_));
assign bus_0f715aea_=medianRow8_receive_instance_DONE&{1{medianRow8_receive_instance_DONE}};
medianRow8_scheduler medianRow8_scheduler_instance(.CLK(CLK), .RESET(bus_0e8ca0a7_), 
  .GO(bus_531ba8cc_), .port_6cce8b17_(bus_505a8057_), .port_7751b6af_(bus_6395f3b8_), 
  .port_5dda934d_(in1_SEND), .port_329263d0_(receive_done), .port_3418d83a_(compute_median_done), 
  .port_621bade7_(median_RDY), .DONE(medianRow8_scheduler_instance_DONE), .RESULT(scheduler), 
  .RESULT_u2426(scheduler_u838), .RESULT_u2427(scheduler_u839), .RESULT_u2428(scheduler_u840));
endmodule



module medianRow8_simplememoryreferee_5d612a62_(bus_2cf9bc04_, bus_7dea4842_, bus_7c50484d_, bus_532a5c09_, bus_022ae0fb_, bus_10e3f4c5_, bus_02f9251e_, bus_0e8fe0db_, bus_14c378f1_, bus_64f01736_, bus_056d7d79_, bus_313b3687_, bus_6acd6137_, bus_18b5b2b5_);
input		bus_2cf9bc04_;
input		bus_7dea4842_;
input		bus_7c50484d_;
input	[31:0]	bus_532a5c09_;
input		bus_022ae0fb_;
input	[31:0]	bus_10e3f4c5_;
input	[2:0]	bus_02f9251e_;
output	[31:0]	bus_0e8fe0db_;
output	[31:0]	bus_14c378f1_;
output		bus_64f01736_;
output		bus_056d7d79_;
output	[2:0]	bus_313b3687_;
output	[31:0]	bus_6acd6137_;
output		bus_18b5b2b5_;
assign bus_0e8fe0db_=32'h0;
assign bus_14c378f1_=bus_10e3f4c5_;
assign bus_64f01736_=1'h0;
assign bus_056d7d79_=bus_022ae0fb_;
assign bus_313b3687_=3'h1;
assign bus_6acd6137_=bus_532a5c09_;
assign bus_18b5b2b5_=bus_7c50484d_;
endmodule



module medianRow8_forge_memory_101x32_161(CLK, ENA, WEA, DINA, ENB, ADDRA, ADDRB, DOUTA, DOUTB, DONEA, DONEB);
input		CLK;
input		ENA;
input		WEA;
input	[31:0]	DINA;
input		ENB;
input	[31:0]	ADDRA;
input	[31:0]	ADDRB;
output	[31:0]	DOUTA;
output	[31:0]	DOUTB;
output		DONEA;
output		DONEB;
wire		wea_0;
wire	[31:0]	pre_douta_0;
wire	[31:0]	pre_doutb_0;
wire		wea_1;
wire	[31:0]	pre_douta_1;
wire	[31:0]	pre_doutb_1;
reg		wea_done;
reg		rea_done;
reg	[31:0]	mux_outa;
reg	[31:0]	mux_outb;
assign wea_0=WEA&(ADDRA[31:6]==26'h0);
assign wea_1=WEA&(ADDRA[31:6]==26'h1);
always @(posedge CLK)
begin
wea_done<=WEA;
end
assign DONEA=rea_done|wea_done;
always @(posedge CLK)
begin
end
assign DONEB=ENB;
always @(pre_douta_0 or pre_douta_1 or ADDRA)
begin
case (ADDRA[31:6])26'd0:mux_outa=pre_douta_0;
26'd1:mux_outa=pre_douta_1;
default:mux_outa=32'h0;
endcase end
assign DOUTA=mux_outa;
always @(pre_doutb_0 or pre_doutb_1 or ADDRB)
begin
case (ADDRB[31:6])26'd0:mux_outb=pre_doutb_0;
26'd1:mux_outb=pre_doutb_1;
default:mux_outb=32'h0;
endcase end
assign DOUTB=mux_outb;
// Memory array element: COL: 0, ROW: 0
//  Initialization of Dual Port LUT ram now done through explicit
// parameter setting.
RAM64X1D#(.INIT(64'h0000000000000000))RAM64X1D_instance_3456(.WCLK(CLK), .WE(wea_0), 
  .A0(ADDRA[0]), .A1(ADDRA[1]), .A2(ADDRA[2]), .A3(ADDRA[3]), .A4(ADDRA[4]), .A5(ADDRA[5]), 
  .D(DINA[0]), .SPO(pre_douta_0[0]), .DPRA0(ADDRB[0]), .DPRA1(ADDRB[1]), .DPRA2(ADDRB[2]), 
  .DPRA3(ADDRB[3]), .DPRA4(ADDRB[4]), .DPRA5(ADDRB[5]), .DPO(pre_doutb_0[0]));
// Memory array element: COL: 0, ROW: 1
//  Initialization of Dual Port LUT ram now done through explicit
// parameter setting.
RAM64X1D#(.INIT(64'h0000000000000000))RAM64X1D_instance_3457(.WCLK(CLK), .WE(wea_0), 
  .A0(ADDRA[0]), .A1(ADDRA[1]), .A2(ADDRA[2]), .A3(ADDRA[3]), .A4(ADDRA[4]), .A5(ADDRA[5]), 
  .D(DINA[1]), .SPO(pre_douta_0[1]), .DPRA0(ADDRB[0]), .DPRA1(ADDRB[1]), .DPRA2(ADDRB[2]), 
  .DPRA3(ADDRB[3]), .DPRA4(ADDRB[4]), .DPRA5(ADDRB[5]), .DPO(pre_doutb_0[1]));
// Memory array element: COL: 0, ROW: 2
//  Initialization of Dual Port LUT ram now done through explicit
// parameter setting.
RAM64X1D#(.INIT(64'h0000000000000000))RAM64X1D_instance_3458(.WCLK(CLK), .WE(wea_0), 
  .A0(ADDRA[0]), .A1(ADDRA[1]), .A2(ADDRA[2]), .A3(ADDRA[3]), .A4(ADDRA[4]), .A5(ADDRA[5]), 
  .D(DINA[2]), .SPO(pre_douta_0[2]), .DPRA0(ADDRB[0]), .DPRA1(ADDRB[1]), .DPRA2(ADDRB[2]), 
  .DPRA3(ADDRB[3]), .DPRA4(ADDRB[4]), .DPRA5(ADDRB[5]), .DPO(pre_doutb_0[2]));
// Memory array element: COL: 0, ROW: 3
//  Initialization of Dual Port LUT ram now done through explicit
// parameter setting.
RAM64X1D#(.INIT(64'h0000000000000000))RAM64X1D_instance_3459(.WCLK(CLK), .WE(wea_0), 
  .A0(ADDRA[0]), .A1(ADDRA[1]), .A2(ADDRA[2]), .A3(ADDRA[3]), .A4(ADDRA[4]), .A5(ADDRA[5]), 
  .D(DINA[3]), .SPO(pre_douta_0[3]), .DPRA0(ADDRB[0]), .DPRA1(ADDRB[1]), .DPRA2(ADDRB[2]), 
  .DPRA3(ADDRB[3]), .DPRA4(ADDRB[4]), .DPRA5(ADDRB[5]), .DPO(pre_doutb_0[3]));
// Memory array element: COL: 0, ROW: 4
//  Initialization of Dual Port LUT ram now done through explicit
// parameter setting.
RAM64X1D#(.INIT(64'h0000000000000000))RAM64X1D_instance_3460(.WCLK(CLK), .WE(wea_0), 
  .A0(ADDRA[0]), .A1(ADDRA[1]), .A2(ADDRA[2]), .A3(ADDRA[3]), .A4(ADDRA[4]), .A5(ADDRA[5]), 
  .D(DINA[4]), .SPO(pre_douta_0[4]), .DPRA0(ADDRB[0]), .DPRA1(ADDRB[1]), .DPRA2(ADDRB[2]), 
  .DPRA3(ADDRB[3]), .DPRA4(ADDRB[4]), .DPRA5(ADDRB[5]), .DPO(pre_doutb_0[4]));
// Memory array element: COL: 0, ROW: 5
//  Initialization of Dual Port LUT ram now done through explicit
// parameter setting.
RAM64X1D#(.INIT(64'h0000000000000000))RAM64X1D_instance_3461(.WCLK(CLK), .WE(wea_0), 
  .A0(ADDRA[0]), .A1(ADDRA[1]), .A2(ADDRA[2]), .A3(ADDRA[3]), .A4(ADDRA[4]), .A5(ADDRA[5]), 
  .D(DINA[5]), .SPO(pre_douta_0[5]), .DPRA0(ADDRB[0]), .DPRA1(ADDRB[1]), .DPRA2(ADDRB[2]), 
  .DPRA3(ADDRB[3]), .DPRA4(ADDRB[4]), .DPRA5(ADDRB[5]), .DPO(pre_doutb_0[5]));
// Memory array element: COL: 0, ROW: 6
//  Initialization of Dual Port LUT ram now done through explicit
// parameter setting.
RAM64X1D#(.INIT(64'h0000000000000000))RAM64X1D_instance_3462(.WCLK(CLK), .WE(wea_0), 
  .A0(ADDRA[0]), .A1(ADDRA[1]), .A2(ADDRA[2]), .A3(ADDRA[3]), .A4(ADDRA[4]), .A5(ADDRA[5]), 
  .D(DINA[6]), .SPO(pre_douta_0[6]), .DPRA0(ADDRB[0]), .DPRA1(ADDRB[1]), .DPRA2(ADDRB[2]), 
  .DPRA3(ADDRB[3]), .DPRA4(ADDRB[4]), .DPRA5(ADDRB[5]), .DPO(pre_doutb_0[6]));
// Memory array element: COL: 0, ROW: 7
//  Initialization of Dual Port LUT ram now done through explicit
// parameter setting.
RAM64X1D#(.INIT(64'h0000000000000000))RAM64X1D_instance_3463(.WCLK(CLK), .WE(wea_0), 
  .A0(ADDRA[0]), .A1(ADDRA[1]), .A2(ADDRA[2]), .A3(ADDRA[3]), .A4(ADDRA[4]), .A5(ADDRA[5]), 
  .D(DINA[7]), .SPO(pre_douta_0[7]), .DPRA0(ADDRB[0]), .DPRA1(ADDRB[1]), .DPRA2(ADDRB[2]), 
  .DPRA3(ADDRB[3]), .DPRA4(ADDRB[4]), .DPRA5(ADDRB[5]), .DPO(pre_doutb_0[7]));
// Memory array element: COL: 0, ROW: 8
//  Initialization of Dual Port LUT ram now done through explicit
// parameter setting.
RAM64X1D#(.INIT(64'h0000000000000000))RAM64X1D_instance_3464(.WCLK(CLK), .WE(wea_0), 
  .A0(ADDRA[0]), .A1(ADDRA[1]), .A2(ADDRA[2]), .A3(ADDRA[3]), .A4(ADDRA[4]), .A5(ADDRA[5]), 
  .D(DINA[8]), .SPO(pre_douta_0[8]), .DPRA0(ADDRB[0]), .DPRA1(ADDRB[1]), .DPRA2(ADDRB[2]), 
  .DPRA3(ADDRB[3]), .DPRA4(ADDRB[4]), .DPRA5(ADDRB[5]), .DPO(pre_doutb_0[8]));
// Memory array element: COL: 0, ROW: 9
//  Initialization of Dual Port LUT ram now done through explicit
// parameter setting.
RAM64X1D#(.INIT(64'h0000000000000000))RAM64X1D_instance_3465(.WCLK(CLK), .WE(wea_0), 
  .A0(ADDRA[0]), .A1(ADDRA[1]), .A2(ADDRA[2]), .A3(ADDRA[3]), .A4(ADDRA[4]), .A5(ADDRA[5]), 
  .D(DINA[9]), .SPO(pre_douta_0[9]), .DPRA0(ADDRB[0]), .DPRA1(ADDRB[1]), .DPRA2(ADDRB[2]), 
  .DPRA3(ADDRB[3]), .DPRA4(ADDRB[4]), .DPRA5(ADDRB[5]), .DPO(pre_doutb_0[9]));
// Memory array element: COL: 0, ROW: 10
//  Initialization of Dual Port LUT ram now done through explicit
// parameter setting.
RAM64X1D#(.INIT(64'h0000000000000000))RAM64X1D_instance_3466(.WCLK(CLK), .WE(wea_0), 
  .A0(ADDRA[0]), .A1(ADDRA[1]), .A2(ADDRA[2]), .A3(ADDRA[3]), .A4(ADDRA[4]), .A5(ADDRA[5]), 
  .D(DINA[10]), .SPO(pre_douta_0[10]), .DPRA0(ADDRB[0]), .DPRA1(ADDRB[1]), .DPRA2(ADDRB[2]), 
  .DPRA3(ADDRB[3]), .DPRA4(ADDRB[4]), .DPRA5(ADDRB[5]), .DPO(pre_doutb_0[10]));
// Memory array element: COL: 0, ROW: 11
//  Initialization of Dual Port LUT ram now done through explicit
// parameter setting.
RAM64X1D#(.INIT(64'h0000000000000000))RAM64X1D_instance_3467(.WCLK(CLK), .WE(wea_0), 
  .A0(ADDRA[0]), .A1(ADDRA[1]), .A2(ADDRA[2]), .A3(ADDRA[3]), .A4(ADDRA[4]), .A5(ADDRA[5]), 
  .D(DINA[11]), .SPO(pre_douta_0[11]), .DPRA0(ADDRB[0]), .DPRA1(ADDRB[1]), .DPRA2(ADDRB[2]), 
  .DPRA3(ADDRB[3]), .DPRA4(ADDRB[4]), .DPRA5(ADDRB[5]), .DPO(pre_doutb_0[11]));
// Memory array element: COL: 0, ROW: 12
//  Initialization of Dual Port LUT ram now done through explicit
// parameter setting.
RAM64X1D#(.INIT(64'h0000000000000000))RAM64X1D_instance_3468(.WCLK(CLK), .WE(wea_0), 
  .A0(ADDRA[0]), .A1(ADDRA[1]), .A2(ADDRA[2]), .A3(ADDRA[3]), .A4(ADDRA[4]), .A5(ADDRA[5]), 
  .D(DINA[12]), .SPO(pre_douta_0[12]), .DPRA0(ADDRB[0]), .DPRA1(ADDRB[1]), .DPRA2(ADDRB[2]), 
  .DPRA3(ADDRB[3]), .DPRA4(ADDRB[4]), .DPRA5(ADDRB[5]), .DPO(pre_doutb_0[12]));
// Memory array element: COL: 0, ROW: 13
//  Initialization of Dual Port LUT ram now done through explicit
// parameter setting.
RAM64X1D#(.INIT(64'h0000000000000000))RAM64X1D_instance_3469(.WCLK(CLK), .WE(wea_0), 
  .A0(ADDRA[0]), .A1(ADDRA[1]), .A2(ADDRA[2]), .A3(ADDRA[3]), .A4(ADDRA[4]), .A5(ADDRA[5]), 
  .D(DINA[13]), .SPO(pre_douta_0[13]), .DPRA0(ADDRB[0]), .DPRA1(ADDRB[1]), .DPRA2(ADDRB[2]), 
  .DPRA3(ADDRB[3]), .DPRA4(ADDRB[4]), .DPRA5(ADDRB[5]), .DPO(pre_doutb_0[13]));
// Memory array element: COL: 0, ROW: 14
//  Initialization of Dual Port LUT ram now done through explicit
// parameter setting.
RAM64X1D#(.INIT(64'h0000000000000000))RAM64X1D_instance_3470(.WCLK(CLK), .WE(wea_0), 
  .A0(ADDRA[0]), .A1(ADDRA[1]), .A2(ADDRA[2]), .A3(ADDRA[3]), .A4(ADDRA[4]), .A5(ADDRA[5]), 
  .D(DINA[14]), .SPO(pre_douta_0[14]), .DPRA0(ADDRB[0]), .DPRA1(ADDRB[1]), .DPRA2(ADDRB[2]), 
  .DPRA3(ADDRB[3]), .DPRA4(ADDRB[4]), .DPRA5(ADDRB[5]), .DPO(pre_doutb_0[14]));
// Memory array element: COL: 0, ROW: 15
//  Initialization of Dual Port LUT ram now done through explicit
// parameter setting.
RAM64X1D#(.INIT(64'h0000000000000000))RAM64X1D_instance_3471(.WCLK(CLK), .WE(wea_0), 
  .A0(ADDRA[0]), .A1(ADDRA[1]), .A2(ADDRA[2]), .A3(ADDRA[3]), .A4(ADDRA[4]), .A5(ADDRA[5]), 
  .D(DINA[15]), .SPO(pre_douta_0[15]), .DPRA0(ADDRB[0]), .DPRA1(ADDRB[1]), .DPRA2(ADDRB[2]), 
  .DPRA3(ADDRB[3]), .DPRA4(ADDRB[4]), .DPRA5(ADDRB[5]), .DPO(pre_doutb_0[15]));
// Memory array element: COL: 0, ROW: 16
//  Initialization of Dual Port LUT ram now done through explicit
// parameter setting.
RAM64X1D#(.INIT(64'h0000000000000000))RAM64X1D_instance_3472(.WCLK(CLK), .WE(wea_0), 
  .A0(ADDRA[0]), .A1(ADDRA[1]), .A2(ADDRA[2]), .A3(ADDRA[3]), .A4(ADDRA[4]), .A5(ADDRA[5]), 
  .D(DINA[16]), .SPO(pre_douta_0[16]), .DPRA0(ADDRB[0]), .DPRA1(ADDRB[1]), .DPRA2(ADDRB[2]), 
  .DPRA3(ADDRB[3]), .DPRA4(ADDRB[4]), .DPRA5(ADDRB[5]), .DPO(pre_doutb_0[16]));
// Memory array element: COL: 0, ROW: 17
//  Initialization of Dual Port LUT ram now done through explicit
// parameter setting.
RAM64X1D#(.INIT(64'h0000000000000000))RAM64X1D_instance_3473(.WCLK(CLK), .WE(wea_0), 
  .A0(ADDRA[0]), .A1(ADDRA[1]), .A2(ADDRA[2]), .A3(ADDRA[3]), .A4(ADDRA[4]), .A5(ADDRA[5]), 
  .D(DINA[17]), .SPO(pre_douta_0[17]), .DPRA0(ADDRB[0]), .DPRA1(ADDRB[1]), .DPRA2(ADDRB[2]), 
  .DPRA3(ADDRB[3]), .DPRA4(ADDRB[4]), .DPRA5(ADDRB[5]), .DPO(pre_doutb_0[17]));
// Memory array element: COL: 0, ROW: 18
//  Initialization of Dual Port LUT ram now done through explicit
// parameter setting.
RAM64X1D#(.INIT(64'h0000000000000000))RAM64X1D_instance_3474(.WCLK(CLK), .WE(wea_0), 
  .A0(ADDRA[0]), .A1(ADDRA[1]), .A2(ADDRA[2]), .A3(ADDRA[3]), .A4(ADDRA[4]), .A5(ADDRA[5]), 
  .D(DINA[18]), .SPO(pre_douta_0[18]), .DPRA0(ADDRB[0]), .DPRA1(ADDRB[1]), .DPRA2(ADDRB[2]), 
  .DPRA3(ADDRB[3]), .DPRA4(ADDRB[4]), .DPRA5(ADDRB[5]), .DPO(pre_doutb_0[18]));
// Memory array element: COL: 0, ROW: 19
//  Initialization of Dual Port LUT ram now done through explicit
// parameter setting.
RAM64X1D#(.INIT(64'h0000000000000000))RAM64X1D_instance_3475(.WCLK(CLK), .WE(wea_0), 
  .A0(ADDRA[0]), .A1(ADDRA[1]), .A2(ADDRA[2]), .A3(ADDRA[3]), .A4(ADDRA[4]), .A5(ADDRA[5]), 
  .D(DINA[19]), .SPO(pre_douta_0[19]), .DPRA0(ADDRB[0]), .DPRA1(ADDRB[1]), .DPRA2(ADDRB[2]), 
  .DPRA3(ADDRB[3]), .DPRA4(ADDRB[4]), .DPRA5(ADDRB[5]), .DPO(pre_doutb_0[19]));
// Memory array element: COL: 0, ROW: 20
//  Initialization of Dual Port LUT ram now done through explicit
// parameter setting.
RAM64X1D#(.INIT(64'h0000000000000000))RAM64X1D_instance_3476(.WCLK(CLK), .WE(wea_0), 
  .A0(ADDRA[0]), .A1(ADDRA[1]), .A2(ADDRA[2]), .A3(ADDRA[3]), .A4(ADDRA[4]), .A5(ADDRA[5]), 
  .D(DINA[20]), .SPO(pre_douta_0[20]), .DPRA0(ADDRB[0]), .DPRA1(ADDRB[1]), .DPRA2(ADDRB[2]), 
  .DPRA3(ADDRB[3]), .DPRA4(ADDRB[4]), .DPRA5(ADDRB[5]), .DPO(pre_doutb_0[20]));
// Memory array element: COL: 0, ROW: 21
//  Initialization of Dual Port LUT ram now done through explicit
// parameter setting.
RAM64X1D#(.INIT(64'h0000000000000000))RAM64X1D_instance_3477(.WCLK(CLK), .WE(wea_0), 
  .A0(ADDRA[0]), .A1(ADDRA[1]), .A2(ADDRA[2]), .A3(ADDRA[3]), .A4(ADDRA[4]), .A5(ADDRA[5]), 
  .D(DINA[21]), .SPO(pre_douta_0[21]), .DPRA0(ADDRB[0]), .DPRA1(ADDRB[1]), .DPRA2(ADDRB[2]), 
  .DPRA3(ADDRB[3]), .DPRA4(ADDRB[4]), .DPRA5(ADDRB[5]), .DPO(pre_doutb_0[21]));
// Memory array element: COL: 0, ROW: 22
//  Initialization of Dual Port LUT ram now done through explicit
// parameter setting.
RAM64X1D#(.INIT(64'h0000000000000000))RAM64X1D_instance_3478(.WCLK(CLK), .WE(wea_0), 
  .A0(ADDRA[0]), .A1(ADDRA[1]), .A2(ADDRA[2]), .A3(ADDRA[3]), .A4(ADDRA[4]), .A5(ADDRA[5]), 
  .D(DINA[22]), .SPO(pre_douta_0[22]), .DPRA0(ADDRB[0]), .DPRA1(ADDRB[1]), .DPRA2(ADDRB[2]), 
  .DPRA3(ADDRB[3]), .DPRA4(ADDRB[4]), .DPRA5(ADDRB[5]), .DPO(pre_doutb_0[22]));
// Memory array element: COL: 0, ROW: 23
//  Initialization of Dual Port LUT ram now done through explicit
// parameter setting.
RAM64X1D#(.INIT(64'h0000000000000000))RAM64X1D_instance_3479(.WCLK(CLK), .WE(wea_0), 
  .A0(ADDRA[0]), .A1(ADDRA[1]), .A2(ADDRA[2]), .A3(ADDRA[3]), .A4(ADDRA[4]), .A5(ADDRA[5]), 
  .D(DINA[23]), .SPO(pre_douta_0[23]), .DPRA0(ADDRB[0]), .DPRA1(ADDRB[1]), .DPRA2(ADDRB[2]), 
  .DPRA3(ADDRB[3]), .DPRA4(ADDRB[4]), .DPRA5(ADDRB[5]), .DPO(pre_doutb_0[23]));
// Memory array element: COL: 0, ROW: 24
//  Initialization of Dual Port LUT ram now done through explicit
// parameter setting.
RAM64X1D#(.INIT(64'h0000000000000000))RAM64X1D_instance_3480(.WCLK(CLK), .WE(wea_0), 
  .A0(ADDRA[0]), .A1(ADDRA[1]), .A2(ADDRA[2]), .A3(ADDRA[3]), .A4(ADDRA[4]), .A5(ADDRA[5]), 
  .D(DINA[24]), .SPO(pre_douta_0[24]), .DPRA0(ADDRB[0]), .DPRA1(ADDRB[1]), .DPRA2(ADDRB[2]), 
  .DPRA3(ADDRB[3]), .DPRA4(ADDRB[4]), .DPRA5(ADDRB[5]), .DPO(pre_doutb_0[24]));
// Memory array element: COL: 0, ROW: 25
//  Initialization of Dual Port LUT ram now done through explicit
// parameter setting.
RAM64X1D#(.INIT(64'h0000000000000000))RAM64X1D_instance_3481(.WCLK(CLK), .WE(wea_0), 
  .A0(ADDRA[0]), .A1(ADDRA[1]), .A2(ADDRA[2]), .A3(ADDRA[3]), .A4(ADDRA[4]), .A5(ADDRA[5]), 
  .D(DINA[25]), .SPO(pre_douta_0[25]), .DPRA0(ADDRB[0]), .DPRA1(ADDRB[1]), .DPRA2(ADDRB[2]), 
  .DPRA3(ADDRB[3]), .DPRA4(ADDRB[4]), .DPRA5(ADDRB[5]), .DPO(pre_doutb_0[25]));
// Memory array element: COL: 0, ROW: 26
//  Initialization of Dual Port LUT ram now done through explicit
// parameter setting.
RAM64X1D#(.INIT(64'h0000000000000000))RAM64X1D_instance_3482(.WCLK(CLK), .WE(wea_0), 
  .A0(ADDRA[0]), .A1(ADDRA[1]), .A2(ADDRA[2]), .A3(ADDRA[3]), .A4(ADDRA[4]), .A5(ADDRA[5]), 
  .D(DINA[26]), .SPO(pre_douta_0[26]), .DPRA0(ADDRB[0]), .DPRA1(ADDRB[1]), .DPRA2(ADDRB[2]), 
  .DPRA3(ADDRB[3]), .DPRA4(ADDRB[4]), .DPRA5(ADDRB[5]), .DPO(pre_doutb_0[26]));
// Memory array element: COL: 0, ROW: 27
//  Initialization of Dual Port LUT ram now done through explicit
// parameter setting.
RAM64X1D#(.INIT(64'h0000000000000000))RAM64X1D_instance_3483(.WCLK(CLK), .WE(wea_0), 
  .A0(ADDRA[0]), .A1(ADDRA[1]), .A2(ADDRA[2]), .A3(ADDRA[3]), .A4(ADDRA[4]), .A5(ADDRA[5]), 
  .D(DINA[27]), .SPO(pre_douta_0[27]), .DPRA0(ADDRB[0]), .DPRA1(ADDRB[1]), .DPRA2(ADDRB[2]), 
  .DPRA3(ADDRB[3]), .DPRA4(ADDRB[4]), .DPRA5(ADDRB[5]), .DPO(pre_doutb_0[27]));
// Memory array element: COL: 0, ROW: 28
//  Initialization of Dual Port LUT ram now done through explicit
// parameter setting.
RAM64X1D#(.INIT(64'h0000000000000000))RAM64X1D_instance_3484(.WCLK(CLK), .WE(wea_0), 
  .A0(ADDRA[0]), .A1(ADDRA[1]), .A2(ADDRA[2]), .A3(ADDRA[3]), .A4(ADDRA[4]), .A5(ADDRA[5]), 
  .D(DINA[28]), .SPO(pre_douta_0[28]), .DPRA0(ADDRB[0]), .DPRA1(ADDRB[1]), .DPRA2(ADDRB[2]), 
  .DPRA3(ADDRB[3]), .DPRA4(ADDRB[4]), .DPRA5(ADDRB[5]), .DPO(pre_doutb_0[28]));
// Memory array element: COL: 0, ROW: 29
//  Initialization of Dual Port LUT ram now done through explicit
// parameter setting.
RAM64X1D#(.INIT(64'h0000000000000000))RAM64X1D_instance_3485(.WCLK(CLK), .WE(wea_0), 
  .A0(ADDRA[0]), .A1(ADDRA[1]), .A2(ADDRA[2]), .A3(ADDRA[3]), .A4(ADDRA[4]), .A5(ADDRA[5]), 
  .D(DINA[29]), .SPO(pre_douta_0[29]), .DPRA0(ADDRB[0]), .DPRA1(ADDRB[1]), .DPRA2(ADDRB[2]), 
  .DPRA3(ADDRB[3]), .DPRA4(ADDRB[4]), .DPRA5(ADDRB[5]), .DPO(pre_doutb_0[29]));
// Memory array element: COL: 0, ROW: 30
//  Initialization of Dual Port LUT ram now done through explicit
// parameter setting.
RAM64X1D#(.INIT(64'h0000000000000000))RAM64X1D_instance_3486(.WCLK(CLK), .WE(wea_0), 
  .A0(ADDRA[0]), .A1(ADDRA[1]), .A2(ADDRA[2]), .A3(ADDRA[3]), .A4(ADDRA[4]), .A5(ADDRA[5]), 
  .D(DINA[30]), .SPO(pre_douta_0[30]), .DPRA0(ADDRB[0]), .DPRA1(ADDRB[1]), .DPRA2(ADDRB[2]), 
  .DPRA3(ADDRB[3]), .DPRA4(ADDRB[4]), .DPRA5(ADDRB[5]), .DPO(pre_doutb_0[30]));
// Memory array element: COL: 0, ROW: 31
//  Initialization of Dual Port LUT ram now done through explicit
// parameter setting.
RAM64X1D#(.INIT(64'h0000000000000000))RAM64X1D_instance_3487(.WCLK(CLK), .WE(wea_0), 
  .A0(ADDRA[0]), .A1(ADDRA[1]), .A2(ADDRA[2]), .A3(ADDRA[3]), .A4(ADDRA[4]), .A5(ADDRA[5]), 
  .D(DINA[31]), .SPO(pre_douta_0[31]), .DPRA0(ADDRB[0]), .DPRA1(ADDRB[1]), .DPRA2(ADDRB[2]), 
  .DPRA3(ADDRB[3]), .DPRA4(ADDRB[4]), .DPRA5(ADDRB[5]), .DPO(pre_doutb_0[31]));
// Memory array element: COL: 1, ROW: 0
//  Initialization of Dual Port LUT ram now done through explicit
// parameter setting.
RAM64X1D#(.INIT(64'h0000000000000000))RAM64X1D_instance_3488(.WCLK(CLK), .WE(wea_1), 
  .A0(ADDRA[0]), .A1(ADDRA[1]), .A2(ADDRA[2]), .A3(ADDRA[3]), .A4(ADDRA[4]), .A5(ADDRA[5]), 
  .D(DINA[0]), .SPO(pre_douta_1[0]), .DPRA0(ADDRB[0]), .DPRA1(ADDRB[1]), .DPRA2(ADDRB[2]), 
  .DPRA3(ADDRB[3]), .DPRA4(ADDRB[4]), .DPRA5(ADDRB[5]), .DPO(pre_doutb_1[0]));
// Memory array element: COL: 1, ROW: 1
//  Initialization of Dual Port LUT ram now done through explicit
// parameter setting.
RAM64X1D#(.INIT(64'h0000000000000000))RAM64X1D_instance_3489(.WCLK(CLK), .WE(wea_1), 
  .A0(ADDRA[0]), .A1(ADDRA[1]), .A2(ADDRA[2]), .A3(ADDRA[3]), .A4(ADDRA[4]), .A5(ADDRA[5]), 
  .D(DINA[1]), .SPO(pre_douta_1[1]), .DPRA0(ADDRB[0]), .DPRA1(ADDRB[1]), .DPRA2(ADDRB[2]), 
  .DPRA3(ADDRB[3]), .DPRA4(ADDRB[4]), .DPRA5(ADDRB[5]), .DPO(pre_doutb_1[1]));
// Memory array element: COL: 1, ROW: 2
//  Initialization of Dual Port LUT ram now done through explicit
// parameter setting.
RAM64X1D#(.INIT(64'h0000000000000000))RAM64X1D_instance_3490(.WCLK(CLK), .WE(wea_1), 
  .A0(ADDRA[0]), .A1(ADDRA[1]), .A2(ADDRA[2]), .A3(ADDRA[3]), .A4(ADDRA[4]), .A5(ADDRA[5]), 
  .D(DINA[2]), .SPO(pre_douta_1[2]), .DPRA0(ADDRB[0]), .DPRA1(ADDRB[1]), .DPRA2(ADDRB[2]), 
  .DPRA3(ADDRB[3]), .DPRA4(ADDRB[4]), .DPRA5(ADDRB[5]), .DPO(pre_doutb_1[2]));
// Memory array element: COL: 1, ROW: 3
//  Initialization of Dual Port LUT ram now done through explicit
// parameter setting.
RAM64X1D#(.INIT(64'h0000000000000000))RAM64X1D_instance_3491(.WCLK(CLK), .WE(wea_1), 
  .A0(ADDRA[0]), .A1(ADDRA[1]), .A2(ADDRA[2]), .A3(ADDRA[3]), .A4(ADDRA[4]), .A5(ADDRA[5]), 
  .D(DINA[3]), .SPO(pre_douta_1[3]), .DPRA0(ADDRB[0]), .DPRA1(ADDRB[1]), .DPRA2(ADDRB[2]), 
  .DPRA3(ADDRB[3]), .DPRA4(ADDRB[4]), .DPRA5(ADDRB[5]), .DPO(pre_doutb_1[3]));
// Memory array element: COL: 1, ROW: 4
//  Initialization of Dual Port LUT ram now done through explicit
// parameter setting.
RAM64X1D#(.INIT(64'h0000000000000000))RAM64X1D_instance_3492(.WCLK(CLK), .WE(wea_1), 
  .A0(ADDRA[0]), .A1(ADDRA[1]), .A2(ADDRA[2]), .A3(ADDRA[3]), .A4(ADDRA[4]), .A5(ADDRA[5]), 
  .D(DINA[4]), .SPO(pre_douta_1[4]), .DPRA0(ADDRB[0]), .DPRA1(ADDRB[1]), .DPRA2(ADDRB[2]), 
  .DPRA3(ADDRB[3]), .DPRA4(ADDRB[4]), .DPRA5(ADDRB[5]), .DPO(pre_doutb_1[4]));
// Memory array element: COL: 1, ROW: 5
//  Initialization of Dual Port LUT ram now done through explicit
// parameter setting.
RAM64X1D#(.INIT(64'h0000000000000000))RAM64X1D_instance_3493(.WCLK(CLK), .WE(wea_1), 
  .A0(ADDRA[0]), .A1(ADDRA[1]), .A2(ADDRA[2]), .A3(ADDRA[3]), .A4(ADDRA[4]), .A5(ADDRA[5]), 
  .D(DINA[5]), .SPO(pre_douta_1[5]), .DPRA0(ADDRB[0]), .DPRA1(ADDRB[1]), .DPRA2(ADDRB[2]), 
  .DPRA3(ADDRB[3]), .DPRA4(ADDRB[4]), .DPRA5(ADDRB[5]), .DPO(pre_doutb_1[5]));
// Memory array element: COL: 1, ROW: 6
//  Initialization of Dual Port LUT ram now done through explicit
// parameter setting.
RAM64X1D#(.INIT(64'h0000000000000000))RAM64X1D_instance_3494(.WCLK(CLK), .WE(wea_1), 
  .A0(ADDRA[0]), .A1(ADDRA[1]), .A2(ADDRA[2]), .A3(ADDRA[3]), .A4(ADDRA[4]), .A5(ADDRA[5]), 
  .D(DINA[6]), .SPO(pre_douta_1[6]), .DPRA0(ADDRB[0]), .DPRA1(ADDRB[1]), .DPRA2(ADDRB[2]), 
  .DPRA3(ADDRB[3]), .DPRA4(ADDRB[4]), .DPRA5(ADDRB[5]), .DPO(pre_doutb_1[6]));
// Memory array element: COL: 1, ROW: 7
//  Initialization of Dual Port LUT ram now done through explicit
// parameter setting.
RAM64X1D#(.INIT(64'h0000000000000000))RAM64X1D_instance_3495(.WCLK(CLK), .WE(wea_1), 
  .A0(ADDRA[0]), .A1(ADDRA[1]), .A2(ADDRA[2]), .A3(ADDRA[3]), .A4(ADDRA[4]), .A5(ADDRA[5]), 
  .D(DINA[7]), .SPO(pre_douta_1[7]), .DPRA0(ADDRB[0]), .DPRA1(ADDRB[1]), .DPRA2(ADDRB[2]), 
  .DPRA3(ADDRB[3]), .DPRA4(ADDRB[4]), .DPRA5(ADDRB[5]), .DPO(pre_doutb_1[7]));
// Memory array element: COL: 1, ROW: 8
//  Initialization of Dual Port LUT ram now done through explicit
// parameter setting.
RAM64X1D#(.INIT(64'h0000000000000000))RAM64X1D_instance_3496(.WCLK(CLK), .WE(wea_1), 
  .A0(ADDRA[0]), .A1(ADDRA[1]), .A2(ADDRA[2]), .A3(ADDRA[3]), .A4(ADDRA[4]), .A5(ADDRA[5]), 
  .D(DINA[8]), .SPO(pre_douta_1[8]), .DPRA0(ADDRB[0]), .DPRA1(ADDRB[1]), .DPRA2(ADDRB[2]), 
  .DPRA3(ADDRB[3]), .DPRA4(ADDRB[4]), .DPRA5(ADDRB[5]), .DPO(pre_doutb_1[8]));
// Memory array element: COL: 1, ROW: 9
//  Initialization of Dual Port LUT ram now done through explicit
// parameter setting.
RAM64X1D#(.INIT(64'h0000000000000000))RAM64X1D_instance_3497(.WCLK(CLK), .WE(wea_1), 
  .A0(ADDRA[0]), .A1(ADDRA[1]), .A2(ADDRA[2]), .A3(ADDRA[3]), .A4(ADDRA[4]), .A5(ADDRA[5]), 
  .D(DINA[9]), .SPO(pre_douta_1[9]), .DPRA0(ADDRB[0]), .DPRA1(ADDRB[1]), .DPRA2(ADDRB[2]), 
  .DPRA3(ADDRB[3]), .DPRA4(ADDRB[4]), .DPRA5(ADDRB[5]), .DPO(pre_doutb_1[9]));
// Memory array element: COL: 1, ROW: 10
//  Initialization of Dual Port LUT ram now done through explicit
// parameter setting.
RAM64X1D#(.INIT(64'h0000000000000000))RAM64X1D_instance_3498(.WCLK(CLK), .WE(wea_1), 
  .A0(ADDRA[0]), .A1(ADDRA[1]), .A2(ADDRA[2]), .A3(ADDRA[3]), .A4(ADDRA[4]), .A5(ADDRA[5]), 
  .D(DINA[10]), .SPO(pre_douta_1[10]), .DPRA0(ADDRB[0]), .DPRA1(ADDRB[1]), .DPRA2(ADDRB[2]), 
  .DPRA3(ADDRB[3]), .DPRA4(ADDRB[4]), .DPRA5(ADDRB[5]), .DPO(pre_doutb_1[10]));
// Memory array element: COL: 1, ROW: 11
//  Initialization of Dual Port LUT ram now done through explicit
// parameter setting.
RAM64X1D#(.INIT(64'h0000000000000000))RAM64X1D_instance_3499(.WCLK(CLK), .WE(wea_1), 
  .A0(ADDRA[0]), .A1(ADDRA[1]), .A2(ADDRA[2]), .A3(ADDRA[3]), .A4(ADDRA[4]), .A5(ADDRA[5]), 
  .D(DINA[11]), .SPO(pre_douta_1[11]), .DPRA0(ADDRB[0]), .DPRA1(ADDRB[1]), .DPRA2(ADDRB[2]), 
  .DPRA3(ADDRB[3]), .DPRA4(ADDRB[4]), .DPRA5(ADDRB[5]), .DPO(pre_doutb_1[11]));
// Memory array element: COL: 1, ROW: 12
//  Initialization of Dual Port LUT ram now done through explicit
// parameter setting.
RAM64X1D#(.INIT(64'h0000000000000000))RAM64X1D_instance_3500(.WCLK(CLK), .WE(wea_1), 
  .A0(ADDRA[0]), .A1(ADDRA[1]), .A2(ADDRA[2]), .A3(ADDRA[3]), .A4(ADDRA[4]), .A5(ADDRA[5]), 
  .D(DINA[12]), .SPO(pre_douta_1[12]), .DPRA0(ADDRB[0]), .DPRA1(ADDRB[1]), .DPRA2(ADDRB[2]), 
  .DPRA3(ADDRB[3]), .DPRA4(ADDRB[4]), .DPRA5(ADDRB[5]), .DPO(pre_doutb_1[12]));
// Memory array element: COL: 1, ROW: 13
//  Initialization of Dual Port LUT ram now done through explicit
// parameter setting.
RAM64X1D#(.INIT(64'h0000000000000000))RAM64X1D_instance_3501(.WCLK(CLK), .WE(wea_1), 
  .A0(ADDRA[0]), .A1(ADDRA[1]), .A2(ADDRA[2]), .A3(ADDRA[3]), .A4(ADDRA[4]), .A5(ADDRA[5]), 
  .D(DINA[13]), .SPO(pre_douta_1[13]), .DPRA0(ADDRB[0]), .DPRA1(ADDRB[1]), .DPRA2(ADDRB[2]), 
  .DPRA3(ADDRB[3]), .DPRA4(ADDRB[4]), .DPRA5(ADDRB[5]), .DPO(pre_doutb_1[13]));
// Memory array element: COL: 1, ROW: 14
//  Initialization of Dual Port LUT ram now done through explicit
// parameter setting.
RAM64X1D#(.INIT(64'h0000000000000000))RAM64X1D_instance_3502(.WCLK(CLK), .WE(wea_1), 
  .A0(ADDRA[0]), .A1(ADDRA[1]), .A2(ADDRA[2]), .A3(ADDRA[3]), .A4(ADDRA[4]), .A5(ADDRA[5]), 
  .D(DINA[14]), .SPO(pre_douta_1[14]), .DPRA0(ADDRB[0]), .DPRA1(ADDRB[1]), .DPRA2(ADDRB[2]), 
  .DPRA3(ADDRB[3]), .DPRA4(ADDRB[4]), .DPRA5(ADDRB[5]), .DPO(pre_doutb_1[14]));
// Memory array element: COL: 1, ROW: 15
//  Initialization of Dual Port LUT ram now done through explicit
// parameter setting.
RAM64X1D#(.INIT(64'h0000000000000000))RAM64X1D_instance_3503(.WCLK(CLK), .WE(wea_1), 
  .A0(ADDRA[0]), .A1(ADDRA[1]), .A2(ADDRA[2]), .A3(ADDRA[3]), .A4(ADDRA[4]), .A5(ADDRA[5]), 
  .D(DINA[15]), .SPO(pre_douta_1[15]), .DPRA0(ADDRB[0]), .DPRA1(ADDRB[1]), .DPRA2(ADDRB[2]), 
  .DPRA3(ADDRB[3]), .DPRA4(ADDRB[4]), .DPRA5(ADDRB[5]), .DPO(pre_doutb_1[15]));
// Memory array element: COL: 1, ROW: 16
//  Initialization of Dual Port LUT ram now done through explicit
// parameter setting.
RAM64X1D#(.INIT(64'h0000000000000000))RAM64X1D_instance_3504(.WCLK(CLK), .WE(wea_1), 
  .A0(ADDRA[0]), .A1(ADDRA[1]), .A2(ADDRA[2]), .A3(ADDRA[3]), .A4(ADDRA[4]), .A5(ADDRA[5]), 
  .D(DINA[16]), .SPO(pre_douta_1[16]), .DPRA0(ADDRB[0]), .DPRA1(ADDRB[1]), .DPRA2(ADDRB[2]), 
  .DPRA3(ADDRB[3]), .DPRA4(ADDRB[4]), .DPRA5(ADDRB[5]), .DPO(pre_doutb_1[16]));
// Memory array element: COL: 1, ROW: 17
//  Initialization of Dual Port LUT ram now done through explicit
// parameter setting.
RAM64X1D#(.INIT(64'h0000000000000000))RAM64X1D_instance_3505(.WCLK(CLK), .WE(wea_1), 
  .A0(ADDRA[0]), .A1(ADDRA[1]), .A2(ADDRA[2]), .A3(ADDRA[3]), .A4(ADDRA[4]), .A5(ADDRA[5]), 
  .D(DINA[17]), .SPO(pre_douta_1[17]), .DPRA0(ADDRB[0]), .DPRA1(ADDRB[1]), .DPRA2(ADDRB[2]), 
  .DPRA3(ADDRB[3]), .DPRA4(ADDRB[4]), .DPRA5(ADDRB[5]), .DPO(pre_doutb_1[17]));
// Memory array element: COL: 1, ROW: 18
//  Initialization of Dual Port LUT ram now done through explicit
// parameter setting.
RAM64X1D#(.INIT(64'h0000000000000000))RAM64X1D_instance_3506(.WCLK(CLK), .WE(wea_1), 
  .A0(ADDRA[0]), .A1(ADDRA[1]), .A2(ADDRA[2]), .A3(ADDRA[3]), .A4(ADDRA[4]), .A5(ADDRA[5]), 
  .D(DINA[18]), .SPO(pre_douta_1[18]), .DPRA0(ADDRB[0]), .DPRA1(ADDRB[1]), .DPRA2(ADDRB[2]), 
  .DPRA3(ADDRB[3]), .DPRA4(ADDRB[4]), .DPRA5(ADDRB[5]), .DPO(pre_doutb_1[18]));
// Memory array element: COL: 1, ROW: 19
//  Initialization of Dual Port LUT ram now done through explicit
// parameter setting.
RAM64X1D#(.INIT(64'h0000000000000000))RAM64X1D_instance_3507(.WCLK(CLK), .WE(wea_1), 
  .A0(ADDRA[0]), .A1(ADDRA[1]), .A2(ADDRA[2]), .A3(ADDRA[3]), .A4(ADDRA[4]), .A5(ADDRA[5]), 
  .D(DINA[19]), .SPO(pre_douta_1[19]), .DPRA0(ADDRB[0]), .DPRA1(ADDRB[1]), .DPRA2(ADDRB[2]), 
  .DPRA3(ADDRB[3]), .DPRA4(ADDRB[4]), .DPRA5(ADDRB[5]), .DPO(pre_doutb_1[19]));
// Memory array element: COL: 1, ROW: 20
//  Initialization of Dual Port LUT ram now done through explicit
// parameter setting.
RAM64X1D#(.INIT(64'h0000000000000000))RAM64X1D_instance_3508(.WCLK(CLK), .WE(wea_1), 
  .A0(ADDRA[0]), .A1(ADDRA[1]), .A2(ADDRA[2]), .A3(ADDRA[3]), .A4(ADDRA[4]), .A5(ADDRA[5]), 
  .D(DINA[20]), .SPO(pre_douta_1[20]), .DPRA0(ADDRB[0]), .DPRA1(ADDRB[1]), .DPRA2(ADDRB[2]), 
  .DPRA3(ADDRB[3]), .DPRA4(ADDRB[4]), .DPRA5(ADDRB[5]), .DPO(pre_doutb_1[20]));
// Memory array element: COL: 1, ROW: 21
//  Initialization of Dual Port LUT ram now done through explicit
// parameter setting.
RAM64X1D#(.INIT(64'h0000000000000000))RAM64X1D_instance_3509(.WCLK(CLK), .WE(wea_1), 
  .A0(ADDRA[0]), .A1(ADDRA[1]), .A2(ADDRA[2]), .A3(ADDRA[3]), .A4(ADDRA[4]), .A5(ADDRA[5]), 
  .D(DINA[21]), .SPO(pre_douta_1[21]), .DPRA0(ADDRB[0]), .DPRA1(ADDRB[1]), .DPRA2(ADDRB[2]), 
  .DPRA3(ADDRB[3]), .DPRA4(ADDRB[4]), .DPRA5(ADDRB[5]), .DPO(pre_doutb_1[21]));
// Memory array element: COL: 1, ROW: 22
//  Initialization of Dual Port LUT ram now done through explicit
// parameter setting.
RAM64X1D#(.INIT(64'h0000000000000000))RAM64X1D_instance_3510(.WCLK(CLK), .WE(wea_1), 
  .A0(ADDRA[0]), .A1(ADDRA[1]), .A2(ADDRA[2]), .A3(ADDRA[3]), .A4(ADDRA[4]), .A5(ADDRA[5]), 
  .D(DINA[22]), .SPO(pre_douta_1[22]), .DPRA0(ADDRB[0]), .DPRA1(ADDRB[1]), .DPRA2(ADDRB[2]), 
  .DPRA3(ADDRB[3]), .DPRA4(ADDRB[4]), .DPRA5(ADDRB[5]), .DPO(pre_doutb_1[22]));
// Memory array element: COL: 1, ROW: 23
//  Initialization of Dual Port LUT ram now done through explicit
// parameter setting.
RAM64X1D#(.INIT(64'h0000000000000000))RAM64X1D_instance_3511(.WCLK(CLK), .WE(wea_1), 
  .A0(ADDRA[0]), .A1(ADDRA[1]), .A2(ADDRA[2]), .A3(ADDRA[3]), .A4(ADDRA[4]), .A5(ADDRA[5]), 
  .D(DINA[23]), .SPO(pre_douta_1[23]), .DPRA0(ADDRB[0]), .DPRA1(ADDRB[1]), .DPRA2(ADDRB[2]), 
  .DPRA3(ADDRB[3]), .DPRA4(ADDRB[4]), .DPRA5(ADDRB[5]), .DPO(pre_doutb_1[23]));
// Memory array element: COL: 1, ROW: 24
//  Initialization of Dual Port LUT ram now done through explicit
// parameter setting.
RAM64X1D#(.INIT(64'h0000000000000000))RAM64X1D_instance_3512(.WCLK(CLK), .WE(wea_1), 
  .A0(ADDRA[0]), .A1(ADDRA[1]), .A2(ADDRA[2]), .A3(ADDRA[3]), .A4(ADDRA[4]), .A5(ADDRA[5]), 
  .D(DINA[24]), .SPO(pre_douta_1[24]), .DPRA0(ADDRB[0]), .DPRA1(ADDRB[1]), .DPRA2(ADDRB[2]), 
  .DPRA3(ADDRB[3]), .DPRA4(ADDRB[4]), .DPRA5(ADDRB[5]), .DPO(pre_doutb_1[24]));
// Memory array element: COL: 1, ROW: 25
//  Initialization of Dual Port LUT ram now done through explicit
// parameter setting.
RAM64X1D#(.INIT(64'h0000000000000000))RAM64X1D_instance_3513(.WCLK(CLK), .WE(wea_1), 
  .A0(ADDRA[0]), .A1(ADDRA[1]), .A2(ADDRA[2]), .A3(ADDRA[3]), .A4(ADDRA[4]), .A5(ADDRA[5]), 
  .D(DINA[25]), .SPO(pre_douta_1[25]), .DPRA0(ADDRB[0]), .DPRA1(ADDRB[1]), .DPRA2(ADDRB[2]), 
  .DPRA3(ADDRB[3]), .DPRA4(ADDRB[4]), .DPRA5(ADDRB[5]), .DPO(pre_doutb_1[25]));
// Memory array element: COL: 1, ROW: 26
//  Initialization of Dual Port LUT ram now done through explicit
// parameter setting.
RAM64X1D#(.INIT(64'h0000000000000000))RAM64X1D_instance_3514(.WCLK(CLK), .WE(wea_1), 
  .A0(ADDRA[0]), .A1(ADDRA[1]), .A2(ADDRA[2]), .A3(ADDRA[3]), .A4(ADDRA[4]), .A5(ADDRA[5]), 
  .D(DINA[26]), .SPO(pre_douta_1[26]), .DPRA0(ADDRB[0]), .DPRA1(ADDRB[1]), .DPRA2(ADDRB[2]), 
  .DPRA3(ADDRB[3]), .DPRA4(ADDRB[4]), .DPRA5(ADDRB[5]), .DPO(pre_doutb_1[26]));
// Memory array element: COL: 1, ROW: 27
//  Initialization of Dual Port LUT ram now done through explicit
// parameter setting.
RAM64X1D#(.INIT(64'h0000000000000000))RAM64X1D_instance_3515(.WCLK(CLK), .WE(wea_1), 
  .A0(ADDRA[0]), .A1(ADDRA[1]), .A2(ADDRA[2]), .A3(ADDRA[3]), .A4(ADDRA[4]), .A5(ADDRA[5]), 
  .D(DINA[27]), .SPO(pre_douta_1[27]), .DPRA0(ADDRB[0]), .DPRA1(ADDRB[1]), .DPRA2(ADDRB[2]), 
  .DPRA3(ADDRB[3]), .DPRA4(ADDRB[4]), .DPRA5(ADDRB[5]), .DPO(pre_doutb_1[27]));
// Memory array element: COL: 1, ROW: 28
//  Initialization of Dual Port LUT ram now done through explicit
// parameter setting.
RAM64X1D#(.INIT(64'h0000000000000000))RAM64X1D_instance_3516(.WCLK(CLK), .WE(wea_1), 
  .A0(ADDRA[0]), .A1(ADDRA[1]), .A2(ADDRA[2]), .A3(ADDRA[3]), .A4(ADDRA[4]), .A5(ADDRA[5]), 
  .D(DINA[28]), .SPO(pre_douta_1[28]), .DPRA0(ADDRB[0]), .DPRA1(ADDRB[1]), .DPRA2(ADDRB[2]), 
  .DPRA3(ADDRB[3]), .DPRA4(ADDRB[4]), .DPRA5(ADDRB[5]), .DPO(pre_doutb_1[28]));
// Memory array element: COL: 1, ROW: 29
//  Initialization of Dual Port LUT ram now done through explicit
// parameter setting.
RAM64X1D#(.INIT(64'h0000000000000000))RAM64X1D_instance_3517(.WCLK(CLK), .WE(wea_1), 
  .A0(ADDRA[0]), .A1(ADDRA[1]), .A2(ADDRA[2]), .A3(ADDRA[3]), .A4(ADDRA[4]), .A5(ADDRA[5]), 
  .D(DINA[29]), .SPO(pre_douta_1[29]), .DPRA0(ADDRB[0]), .DPRA1(ADDRB[1]), .DPRA2(ADDRB[2]), 
  .DPRA3(ADDRB[3]), .DPRA4(ADDRB[4]), .DPRA5(ADDRB[5]), .DPO(pre_doutb_1[29]));
// Memory array element: COL: 1, ROW: 30
//  Initialization of Dual Port LUT ram now done through explicit
// parameter setting.
RAM64X1D#(.INIT(64'h0000000000000000))RAM64X1D_instance_3518(.WCLK(CLK), .WE(wea_1), 
  .A0(ADDRA[0]), .A1(ADDRA[1]), .A2(ADDRA[2]), .A3(ADDRA[3]), .A4(ADDRA[4]), .A5(ADDRA[5]), 
  .D(DINA[30]), .SPO(pre_douta_1[30]), .DPRA0(ADDRB[0]), .DPRA1(ADDRB[1]), .DPRA2(ADDRB[2]), 
  .DPRA3(ADDRB[3]), .DPRA4(ADDRB[4]), .DPRA5(ADDRB[5]), .DPO(pre_doutb_1[30]));
// Memory array element: COL: 1, ROW: 31
//  Initialization of Dual Port LUT ram now done through explicit
// parameter setting.
RAM64X1D#(.INIT(64'h0000000000000000))RAM64X1D_instance_3519(.WCLK(CLK), .WE(wea_1), 
  .A0(ADDRA[0]), .A1(ADDRA[1]), .A2(ADDRA[2]), .A3(ADDRA[3]), .A4(ADDRA[4]), .A5(ADDRA[5]), 
  .D(DINA[31]), .SPO(pre_douta_1[31]), .DPRA0(ADDRB[0]), .DPRA1(ADDRB[1]), .DPRA2(ADDRB[2]), 
  .DPRA3(ADDRB[3]), .DPRA4(ADDRB[4]), .DPRA5(ADDRB[5]), .DPO(pre_doutb_1[31]));
endmodule



module medianRow8_structuralmemory_7819129f_(CLK_u100, bus_2107881a_, bus_227bfe91_, bus_26e4824a_, bus_2e0e8d76_, bus_7ad1fd75_, bus_21a665a0_, bus_2d808cfb_, bus_3d6e29b1_, bus_607d26b0_, bus_7f59bbbb_, bus_4d969a0d_, bus_21978536_, bus_7089966d_);
input		CLK_u100;
input		bus_2107881a_;
input	[31:0]	bus_227bfe91_;
input	[2:0]	bus_26e4824a_;
input		bus_2e0e8d76_;
input	[31:0]	bus_7ad1fd75_;
input	[2:0]	bus_21a665a0_;
input		bus_2d808cfb_;
input		bus_3d6e29b1_;
input	[31:0]	bus_607d26b0_;
output	[31:0]	bus_7f59bbbb_;
output		bus_4d969a0d_;
output	[31:0]	bus_21978536_;
output		bus_7089966d_;
wire		or_1695e1f7_u0;
reg		logicalMem_5abeaa0b_we_delay0_u0=1'h0;
wire		and_11096f82_u0;
wire	[31:0]	bus_375bc218_;
wire	[31:0]	bus_0cd09b07_;
wire		not_34449bf3_u0;
wire		or_1c6c23f0_u0;
assign or_1695e1f7_u0=and_11096f82_u0|logicalMem_5abeaa0b_we_delay0_u0;
always @(posedge CLK_u100 or posedge bus_2107881a_)
begin
if (bus_2107881a_)
logicalMem_5abeaa0b_we_delay0_u0<=1'h0;
else logicalMem_5abeaa0b_we_delay0_u0<=bus_3d6e29b1_;
end
assign and_11096f82_u0=bus_2d808cfb_&not_34449bf3_u0;
assign bus_7f59bbbb_=bus_0cd09b07_;
assign bus_4d969a0d_=bus_2e0e8d76_;
assign bus_21978536_=bus_375bc218_;
assign bus_7089966d_=or_1695e1f7_u0;
medianRow8_forge_memory_101x32_161 medianRow8_forge_memory_101x32_161_instance0(.CLK(CLK_u100), 
  .ENA(or_1c6c23f0_u0), .WEA(bus_3d6e29b1_), .DINA(bus_607d26b0_), .ADDRA(bus_7ad1fd75_), 
  .DOUTA(bus_375bc218_), .DONEA(), .ENB(bus_2e0e8d76_), .ADDRB(bus_227bfe91_), .DOUTB(bus_0cd09b07_), 
  .DONEB());
assign not_34449bf3_u0=~bus_3d6e29b1_;
assign or_1c6c23f0_u0=bus_2d808cfb_|bus_3d6e29b1_;
endmodule



module medianRow8_compute_median(CLK, RESET, GO, port_32a99403_, port_0b55d7e9_, port_6f4d0b7d_, port_649b4f89_, port_3c26653e_, RESULT, RESULT_u2406, RESULT_u2407, RESULT_u2408, RESULT_u2409, RESULT_u2410, RESULT_u2411, RESULT_u2412, RESULT_u2413, RESULT_u2414, RESULT_u2415, RESULT_u2416, RESULT_u2417, RESULT_u2418, RESULT_u2419, DONE);
input		CLK;
input		RESET;
input		GO;
input		port_32a99403_;
input	[31:0]	port_0b55d7e9_;
input		port_6f4d0b7d_;
input	[31:0]	port_649b4f89_;
input		port_3c26653e_;
output		RESULT;
output	[31:0]	RESULT_u2406;
output		RESULT_u2407;
output	[31:0]	RESULT_u2408;
output	[2:0]	RESULT_u2409;
output		RESULT_u2410;
output	[31:0]	RESULT_u2411;
output	[2:0]	RESULT_u2412;
output		RESULT_u2413;
output	[31:0]	RESULT_u2414;
output	[31:0]	RESULT_u2415;
output	[2:0]	RESULT_u2416;
output	[15:0]	RESULT_u2417;
output	[7:0]	RESULT_u2418;
output		RESULT_u2419;
output		DONE;
reg	[31:0]	syncEnable_u1588=32'h0;
wire signed	[32:0]	lessThan_a_signed;
wire signed	[32:0]	lessThan_b_signed;
wire		lessThan;
wire		and_u4118_u0;
wire		and_u4119_u0;
wire		not_u954_u0;
wire	[31:0]	add;
wire		and_u4120_u0;
wire	[31:0]	add_u760;
wire	[31:0]	add_u761;
wire		and_u4121_u0;
wire		greaterThan;
wire signed	[31:0]	greaterThan_a_signed;
wire signed	[31:0]	greaterThan_b_signed;
wire		not_u955_u0;
wire		and_u4122_u0;
wire		and_u4123_u0;
wire	[31:0]	add_u762;
wire		and_u4124_u0;
wire	[31:0]	add_u763;
wire	[31:0]	add_u764;
wire		and_u4125_u0;
wire	[31:0]	add_u765;
wire		and_u4126_u0;
reg		reg_7b53c12f_u0=1'h0;
wire		or_u1588_u0;
wire	[31:0]	add_u766;
wire	[31:0]	add_u767;
wire		or_u1589_u0;
wire		and_u4127_u0;
reg		reg_351ead71_u0=1'h0;
reg		block_GO_delayed_u114=1'h0;
reg	[31:0]	syncEnable_u1589_u0=32'h0;
reg	[31:0]	syncEnable_u1590_u0=32'h0;
wire	[31:0]	mux_u2096;
wire		or_u1590_u0;
wire	[31:0]	mux_u2097_u0;
reg	[31:0]	syncEnable_u1591_u0=32'h0;
reg		block_GO_delayed_result_delayed_u22=1'h0;
reg	[31:0]	syncEnable_u1592_u0=32'h0;
reg	[31:0]	syncEnable_u1593_u0=32'h0;
reg	[31:0]	syncEnable_u1594_u0=32'h0;
reg		syncEnable_u1595_u0=1'h0;
wire		and_u4128_u0;
wire	[31:0]	mux_u2098_u0;
reg		reg_7932a2d5_u0=1'h0;
reg		and_delayed_u414=1'h0;
wire		mux_u2099_u0;
reg		and_delayed_result_delayed_u16=1'h0;
wire	[31:0]	mux_u2100_u0;
wire		and_u4129_u0;
reg		and_delayed_result_delayed_result_delayed_u4=1'h0;
wire		or_u1591_u0;
reg	[31:0]	syncEnable_u1596_u0=32'h0;
reg		and_delayed_result_delayed_result_delayed_result_delayed_u2=1'h0;
reg	[31:0]	syncEnable_u1597_u0=32'h0;
wire	[31:0]	add_u768;
wire		or_u1592_u0;
wire	[31:0]	mux_u2101_u0;
reg	[31:0]	syncEnable_u1598_u0=32'h0;
reg	[31:0]	syncEnable_u1599_u0=32'h0;
reg	[31:0]	syncEnable_u1600_u0=32'h0;
reg		syncEnable_u1601_u0=1'h0;
reg	[31:0]	syncEnable_u1602_u0=32'h0;
reg	[31:0]	syncEnable_u1603_u0=32'h0;
wire	[31:0]	mux_u2102_u0;
wire		or_u1593_u0;
reg		block_GO_delayed_u115_u0=1'h0;
reg	[31:0]	syncEnable_u1604_u0=32'h0;
wire		and_u4130_u0;
wire	[31:0]	mux_u2103_u0;
wire	[31:0]	mux_u2104_u0;
wire	[31:0]	mux_u2105_u0;
wire		or_u1594_u0;
wire		mux_u2106_u0;
wire	[31:0]	mux_u2107_u0;
wire	[31:0]	mux_u2108_u0;
wire	[15:0]	add_u769;
wire		scoreboard_7af40216_resOr1;
reg		scoreboard_7af40216_reg1=1'h0;
wire		bus_4691741d_;
wire		scoreboard_7af40216_resOr0;
wire		scoreboard_7af40216_and;
reg		scoreboard_7af40216_reg0=1'h0;
wire	[31:0]	latch_6f3052f6_out;
reg	[31:0]	latch_6f3052f6_reg=32'h0;
reg		reg_1b7b6111_u0=1'h0;
reg	[15:0]	syncEnable_u1605_u0=16'h0;
reg	[31:0]	latch_43ae263a_reg=32'h0;
wire	[31:0]	latch_43ae263a_out;
wire	[31:0]	latch_75530ac5_out;
reg	[31:0]	latch_75530ac5_reg=32'h0;
wire	[31:0]	latch_05b589a3_out;
reg	[31:0]	latch_05b589a3_reg=32'h0;
wire		latch_6ec9c2b1_out;
reg		latch_6ec9c2b1_reg=1'h0;
wire	[15:0]	lessThan_u112_a_unsigned;
wire	[15:0]	lessThan_u112_b_unsigned;
wire		lessThan_u112;
wire		andOp;
wire		and_u4131_u0;
wire		not_u956_u0;
wire		and_u4132_u0;
wire		and_u4133_u0;
wire		and_u4134_u0;
reg	[31:0]	fbReg_tmp_u54=32'h0;
wire	[31:0]	mux_u2109_u0;
reg		syncEnable_u1606_u0=1'h0;
wire		mux_u2110_u0;
reg	[31:0]	fbReg_tmp_row1_u54=32'h0;
wire	[31:0]	mux_u2111_u0;
reg		fbReg_swapped_u54=1'h0;
wire	[31:0]	mux_u2112_u0;
reg	[31:0]	fbReg_tmp_row_u54=32'h0;
reg	[15:0]	fbReg_idx_u54=16'h0;
wire		or_u1595_u0;
wire	[31:0]	mux_u2113_u0;
wire	[15:0]	mux_u2114_u0;
reg		loopControl_u112=1'h0;
reg	[31:0]	fbReg_tmp_row0_u54=32'h0;
wire		and_u4135_u0;
wire	[7:0]	simplePinWrite;
wire		simplePinWrite_u654;
wire	[15:0]	simplePinWrite_u655;
wire		or_u1596_u0;
wire	[31:0]	mux_u2115_u0;
reg		reg_61ec2f87_u0=1'h0;
reg		reg_61ec2f87_result_delayed_u0=1'h0;
always @(posedge CLK)
begin
if (or_u1594_u0)
syncEnable_u1588<=mux_u2107_u0;
end
assign lessThan_a_signed={1'b0, mux_u2105_u0};
assign lessThan_b_signed=33'h64;
assign lessThan=lessThan_a_signed<lessThan_b_signed;
assign and_u4118_u0=or_u1594_u0&not_u954_u0;
assign and_u4119_u0=or_u1594_u0&lessThan;
assign not_u954_u0=~lessThan;
assign add=mux_u2105_u0+32'h0;
assign and_u4120_u0=and_u4130_u0&port_32a99403_;
assign add_u760=mux_u2105_u0+32'h1;
assign add_u761=add_u760+32'h0;
assign and_u4121_u0=and_u4130_u0&port_3c26653e_;
assign greaterThan_a_signed=syncEnable_u1604_u0;
assign greaterThan_b_signed=syncEnable_u1600_u0;
assign greaterThan=greaterThan_a_signed>greaterThan_b_signed;
assign not_u955_u0=~greaterThan;
assign and_u4122_u0=block_GO_delayed_u115_u0&greaterThan;
assign and_u4123_u0=block_GO_delayed_u115_u0&not_u955_u0;
assign add_u762=syncEnable_u1603_u0+32'h0;
assign and_u4124_u0=and_u4128_u0&port_32a99403_;
assign add_u763=syncEnable_u1603_u0+32'h1;
assign add_u764=add_u763+32'h0;
assign and_u4125_u0=and_u4128_u0&port_3c26653e_;
assign add_u765=syncEnable_u1603_u0+32'h0;
assign and_u4126_u0=reg_7b53c12f_u0&port_3c26653e_;
always @(posedge CLK or posedge block_GO_delayed_u114 or posedge or_u1588_u0)
begin
if (or_u1588_u0)
reg_7b53c12f_u0<=1'h0;
else if (block_GO_delayed_u114)
reg_7b53c12f_u0<=1'h1;
else reg_7b53c12f_u0<=reg_7b53c12f_u0;
end
assign or_u1588_u0=and_u4126_u0|RESET;
assign add_u766=syncEnable_u1603_u0+32'h1;
assign add_u767=add_u766+32'h0;
assign or_u1589_u0=and_u4127_u0|RESET;
assign and_u4127_u0=reg_351ead71_u0&port_3c26653e_;
always @(posedge CLK or posedge block_GO_delayed_result_delayed_u22 or posedge or_u1589_u0)
begin
if (or_u1589_u0)
reg_351ead71_u0<=1'h0;
else if (block_GO_delayed_result_delayed_u22)
reg_351ead71_u0<=1'h1;
else reg_351ead71_u0<=reg_351ead71_u0;
end
always @(posedge CLK or posedge RESET)
begin
if (RESET)
block_GO_delayed_u114<=1'h0;
else block_GO_delayed_u114<=and_u4128_u0;
end
always @(posedge CLK)
begin
if (and_u4128_u0)
syncEnable_u1589_u0<=port_649b4f89_;
end
always @(posedge CLK)
begin
if (and_u4128_u0)
syncEnable_u1590_u0<=add_u767;
end
assign mux_u2096=({32{block_GO_delayed_u114}}&syncEnable_u1591_u0)|({32{block_GO_delayed_result_delayed_u22}}&syncEnable_u1590_u0)|({32{and_u4128_u0}}&add_u764);
assign or_u1590_u0=block_GO_delayed_u114|block_GO_delayed_result_delayed_u22;
assign mux_u2097_u0=(block_GO_delayed_u114)?syncEnable_u1589_u0:syncEnable_u1593_u0;
always @(posedge CLK)
begin
if (and_u4128_u0)
syncEnable_u1591_u0<=add_u765;
end
always @(posedge CLK or posedge RESET)
begin
if (RESET)
block_GO_delayed_result_delayed_u22<=1'h0;
else block_GO_delayed_result_delayed_u22<=block_GO_delayed_u114;
end
always @(posedge CLK)
begin
if (and_u4128_u0)
syncEnable_u1592_u0<=port_649b4f89_;
end
always @(posedge CLK)
begin
if (and_u4128_u0)
syncEnable_u1593_u0<=port_0b55d7e9_;
end
always @(posedge CLK)
begin
if (and_u4128_u0)
syncEnable_u1594_u0<=port_0b55d7e9_;
end
always @(posedge CLK)
begin
if (block_GO_delayed_u115_u0)
syncEnable_u1595_u0<=syncEnable_u1601_u0;
end
assign and_u4128_u0=and_u4122_u0&block_GO_delayed_u115_u0;
assign mux_u2098_u0=(reg_7932a2d5_u0)?syncEnable_u1596_u0:syncEnable_u1594_u0;
always @(posedge CLK or posedge RESET)
begin
if (RESET)
reg_7932a2d5_u0<=1'h0;
else reg_7932a2d5_u0<=and_u4129_u0;
end
always @(posedge CLK or posedge RESET)
begin
if (RESET)
and_delayed_u414<=1'h0;
else and_delayed_u414<=and_u4128_u0;
end
assign mux_u2099_u0=(reg_7932a2d5_u0)?syncEnable_u1595_u0:1'h1;
always @(posedge CLK or posedge RESET)
begin
if (RESET)
and_delayed_result_delayed_u16<=1'h0;
else and_delayed_result_delayed_u16<=and_delayed_u414;
end
assign mux_u2100_u0=(reg_7932a2d5_u0)?syncEnable_u1597_u0:syncEnable_u1592_u0;
assign and_u4129_u0=and_u4123_u0&block_GO_delayed_u115_u0;
always @(posedge CLK or posedge RESET)
begin
if (RESET)
and_delayed_result_delayed_result_delayed_u4<=1'h0;
else and_delayed_result_delayed_result_delayed_u4<=and_delayed_result_delayed_u16;
end
assign or_u1591_u0=reg_7932a2d5_u0|and_delayed_result_delayed_result_delayed_result_delayed_u2;
always @(posedge CLK)
begin
if (block_GO_delayed_u115_u0)
syncEnable_u1596_u0<=syncEnable_u1602_u0;
end
always @(posedge CLK or posedge RESET)
begin
if (RESET)
and_delayed_result_delayed_result_delayed_result_delayed_u2<=1'h0;
else and_delayed_result_delayed_result_delayed_result_delayed_u2<=and_delayed_result_delayed_result_delayed_u4;
end
always @(posedge CLK)
begin
if (block_GO_delayed_u115_u0)
syncEnable_u1597_u0<=syncEnable_u1598_u0;
end
assign add_u768=mux_u2105_u0+32'h1;
assign or_u1592_u0=and_u4130_u0|and_u4128_u0;
assign mux_u2101_u0=(and_u4130_u0)?add:add_u762;
always @(posedge CLK)
begin
if (and_u4130_u0)
syncEnable_u1598_u0<=mux_u2107_u0;
end
always @(posedge CLK)
begin
if (and_u4130_u0)
syncEnable_u1599_u0<=add_u768;
end
always @(posedge CLK)
begin
if (and_u4130_u0)
syncEnable_u1600_u0<=port_649b4f89_;
end
always @(posedge CLK)
begin
if (and_u4130_u0)
syncEnable_u1601_u0<=mux_u2106_u0;
end
always @(posedge CLK)
begin
if (and_u4130_u0)
syncEnable_u1602_u0<=mux_u2104_u0;
end
always @(posedge CLK)
begin
if (and_u4130_u0)
syncEnable_u1603_u0<=mux_u2105_u0;
end
assign mux_u2102_u0=({32{or_u1590_u0}}&mux_u2096)|({32{and_u4130_u0}}&add_u761)|({32{and_u4128_u0}}&mux_u2096);
assign or_u1593_u0=and_u4130_u0|and_u4128_u0;
always @(posedge CLK or posedge RESET)
begin
if (RESET)
block_GO_delayed_u115_u0<=1'h0;
else block_GO_delayed_u115_u0<=and_u4130_u0;
end
always @(posedge CLK)
begin
if (and_u4130_u0)
syncEnable_u1604_u0<=port_0b55d7e9_;
end
assign and_u4130_u0=and_u4119_u0&or_u1594_u0;
assign mux_u2103_u0=(or_u1591_u0)?syncEnable_u1600_u0:mux_u2112_u0;
assign mux_u2104_u0=(or_u1591_u0)?mux_u2098_u0:mux_u2113_u0;
assign mux_u2105_u0=(or_u1591_u0)?syncEnable_u1599_u0:32'h0;
assign or_u1594_u0=or_u1591_u0|and_u4134_u0;
assign mux_u2106_u0=(or_u1591_u0)?mux_u2099_u0:1'h0;
assign mux_u2107_u0=(or_u1591_u0)?mux_u2100_u0:mux_u2109_u0;
assign mux_u2108_u0=(or_u1591_u0)?syncEnable_u1604_u0:mux_u2111_u0;
assign add_u769=mux_u2114_u0+16'h1;
assign scoreboard_7af40216_resOr1=or_u1591_u0|scoreboard_7af40216_reg1;
always @(posedge CLK)
begin
if (bus_4691741d_)
scoreboard_7af40216_reg1<=1'h0;
else if (or_u1591_u0)
scoreboard_7af40216_reg1<=1'h1;
else scoreboard_7af40216_reg1<=scoreboard_7af40216_reg1;
end
assign bus_4691741d_=scoreboard_7af40216_and|RESET;
assign scoreboard_7af40216_resOr0=reg_1b7b6111_u0|scoreboard_7af40216_reg0;
assign scoreboard_7af40216_and=scoreboard_7af40216_resOr0&scoreboard_7af40216_resOr1;
always @(posedge CLK)
begin
if (bus_4691741d_)
scoreboard_7af40216_reg0<=1'h0;
else if (reg_1b7b6111_u0)
scoreboard_7af40216_reg0<=1'h1;
else scoreboard_7af40216_reg0<=scoreboard_7af40216_reg0;
end
assign latch_6f3052f6_out=(or_u1591_u0)?syncEnable_u1604_u0:latch_6f3052f6_reg;
always @(posedge CLK)
begin
if (or_u1591_u0)
latch_6f3052f6_reg<=syncEnable_u1604_u0;
end
always @(posedge CLK or posedge RESET)
begin
if (RESET)
reg_1b7b6111_u0<=1'h0;
else reg_1b7b6111_u0<=or_u1591_u0;
end
always @(posedge CLK)
begin
if (and_u4134_u0)
syncEnable_u1605_u0<=add_u769;
end
always @(posedge CLK)
begin
if (or_u1591_u0)
latch_43ae263a_reg<=syncEnable_u1588;
end
assign latch_43ae263a_out=(or_u1591_u0)?syncEnable_u1588:latch_43ae263a_reg;
assign latch_75530ac5_out=(or_u1591_u0)?mux_u2098_u0:latch_75530ac5_reg;
always @(posedge CLK)
begin
if (or_u1591_u0)
latch_75530ac5_reg<=mux_u2098_u0;
end
assign latch_05b589a3_out=(or_u1591_u0)?syncEnable_u1600_u0:latch_05b589a3_reg;
always @(posedge CLK)
begin
if (or_u1591_u0)
latch_05b589a3_reg<=syncEnable_u1600_u0;
end
assign latch_6ec9c2b1_out=(or_u1591_u0)?mux_u2099_u0:latch_6ec9c2b1_reg;
always @(posedge CLK)
begin
if (or_u1591_u0)
latch_6ec9c2b1_reg<=mux_u2099_u0;
end
assign lessThan_u112_a_unsigned=mux_u2114_u0;
assign lessThan_u112_b_unsigned=16'h65;
assign lessThan_u112=lessThan_u112_a_unsigned<lessThan_u112_b_unsigned;
assign andOp=lessThan_u112&mux_u2110_u0;
assign and_u4131_u0=or_u1595_u0&andOp;
assign not_u956_u0=~andOp;
assign and_u4132_u0=or_u1595_u0&not_u956_u0;
assign and_u4133_u0=and_u4132_u0&or_u1595_u0;
assign and_u4134_u0=and_u4131_u0&or_u1595_u0;
always @(posedge CLK)
begin
if (scoreboard_7af40216_and)
fbReg_tmp_u54<=latch_75530ac5_out;
end
assign mux_u2109_u0=(loopControl_u112)?fbReg_tmp_row1_u54:32'h0;
always @(posedge CLK)
begin
if (GO)
syncEnable_u1606_u0<=RESET;
end
assign mux_u2110_u0=(loopControl_u112)?fbReg_swapped_u54:1'h0;
always @(posedge CLK)
begin
if (scoreboard_7af40216_and)
fbReg_tmp_row1_u54<=latch_43ae263a_out;
end
assign mux_u2111_u0=(loopControl_u112)?fbReg_tmp_row_u54:32'h0;
always @(posedge CLK)
begin
if (scoreboard_7af40216_and)
fbReg_swapped_u54<=latch_6ec9c2b1_out;
end
assign mux_u2112_u0=(loopControl_u112)?fbReg_tmp_row0_u54:32'h0;
always @(posedge CLK)
begin
if (scoreboard_7af40216_and)
fbReg_tmp_row_u54<=latch_6f3052f6_out;
end
always @(posedge CLK)
begin
if (scoreboard_7af40216_and)
fbReg_idx_u54<=syncEnable_u1605_u0;
end
assign or_u1595_u0=loopControl_u112|GO;
assign mux_u2113_u0=(loopControl_u112)?fbReg_tmp_u54:32'h0;
assign mux_u2114_u0=(loopControl_u112)?fbReg_idx_u54:16'h0;
always @(posedge CLK or posedge syncEnable_u1606_u0)
begin
if (syncEnable_u1606_u0)
loopControl_u112<=1'h0;
else loopControl_u112<=scoreboard_7af40216_and;
end
always @(posedge CLK)
begin
if (scoreboard_7af40216_and)
fbReg_tmp_row0_u54<=latch_05b589a3_out;
end
assign and_u4135_u0=reg_61ec2f87_u0&port_32a99403_;
assign simplePinWrite=port_0b55d7e9_[7:0];
assign simplePinWrite_u654=reg_61ec2f87_u0&{1{reg_61ec2f87_u0}};
assign simplePinWrite_u655=16'h1&{16{1'h1}};
assign or_u1596_u0=or_u1592_u0|reg_61ec2f87_u0;
assign mux_u2115_u0=(or_u1592_u0)?mux_u2101_u0:32'h32;
always @(posedge CLK or posedge RESET)
begin
if (RESET)
reg_61ec2f87_u0<=1'h0;
else reg_61ec2f87_u0<=and_u4133_u0;
end
always @(posedge CLK or posedge RESET)
begin
if (RESET)
reg_61ec2f87_result_delayed_u0<=1'h0;
else reg_61ec2f87_result_delayed_u0<=reg_61ec2f87_u0;
end
assign RESULT=GO;
assign RESULT_u2406=32'h0;
assign RESULT_u2407=or_u1596_u0;
assign RESULT_u2408=mux_u2115_u0;
assign RESULT_u2409=3'h1;
assign RESULT_u2410=or_u1593_u0;
assign RESULT_u2411=mux_u2102_u0;
assign RESULT_u2412=3'h1;
assign RESULT_u2413=or_u1590_u0;
assign RESULT_u2414=mux_u2102_u0;
assign RESULT_u2415=mux_u2097_u0;
assign RESULT_u2416=3'h1;
assign RESULT_u2417=simplePinWrite_u655;
assign RESULT_u2418=simplePinWrite;
assign RESULT_u2419=simplePinWrite_u654;
assign DONE=reg_61ec2f87_result_delayed_u0;
endmodule



module medianRow8_receive(CLK, RESET, GO, port_114acc47_, port_61aea8ac_, port_1908964f_, RESULT, RESULT_u2420, RESULT_u2421, RESULT_u2422, RESULT_u2423, RESULT_u2424, RESULT_u2425, DONE);
input		CLK;
input		RESET;
input		GO;
input	[31:0]	port_114acc47_;
input		port_61aea8ac_;
input	[7:0]	port_1908964f_;
output		RESULT;
output	[31:0]	RESULT_u2420;
output		RESULT_u2421;
output	[31:0]	RESULT_u2422;
output	[31:0]	RESULT_u2423;
output	[2:0]	RESULT_u2424;
output		RESULT_u2425;
output		DONE;
wire		simplePinWrite;
wire	[31:0]	add;
reg		reg_45efc65f_u0=1'h0;
wire		or_u1597_u0;
wire		and_u4136_u0;
wire	[31:0]	add_u770;
reg		reg_151f6982_u0=1'h0;
assign simplePinWrite=GO&{1{GO}};
assign add=port_114acc47_+32'h0;
always @(posedge CLK or posedge GO or posedge or_u1597_u0)
begin
if (or_u1597_u0)
reg_45efc65f_u0<=1'h0;
else if (GO)
reg_45efc65f_u0<=1'h1;
else reg_45efc65f_u0<=reg_45efc65f_u0;
end
assign or_u1597_u0=and_u4136_u0|RESET;
assign and_u4136_u0=reg_45efc65f_u0&port_61aea8ac_;
assign add_u770=port_114acc47_+32'h1;
always @(posedge CLK or posedge RESET)
begin
if (RESET)
reg_151f6982_u0<=1'h0;
else reg_151f6982_u0<=GO;
end
assign RESULT=GO;
assign RESULT_u2420=add_u770;
assign RESULT_u2421=GO;
assign RESULT_u2422=add;
assign RESULT_u2423={24'b0, port_1908964f_};
assign RESULT_u2424=3'h1;
assign RESULT_u2425=simplePinWrite;
assign DONE=reg_151f6982_u0;
endmodule



module medianRow8_globalreset_physical_6d3a41e3_(bus_06b986b5_, bus_04b068fb_, bus_0e8ca0a7_);
input		bus_06b986b5_;
input		bus_04b068fb_;
output		bus_0e8ca0a7_;
reg		sample_u66=1'h0;
reg		glitch_u66=1'h0;
wire		not_0c1dbe6c_u0;
wire		and_55263d25_u0;
reg		cross_u66=1'h0;
wire		or_6f44c08c_u0;
reg		final_u66=1'h1;
always @(posedge bus_06b986b5_)
begin
sample_u66<=1'h1;
end
always @(posedge bus_06b986b5_)
begin
glitch_u66<=cross_u66;
end
assign not_0c1dbe6c_u0=~and_55263d25_u0;
assign and_55263d25_u0=cross_u66&glitch_u66;
always @(posedge bus_06b986b5_)
begin
cross_u66<=sample_u66;
end
assign or_6f44c08c_u0=bus_04b068fb_|final_u66;
always @(posedge bus_06b986b5_)
begin
final_u66<=not_0c1dbe6c_u0;
end
assign bus_0e8ca0a7_=or_6f44c08c_u0;
endmodule



module medianRow8_Kicker_66(CLK, RESET, bus_531ba8cc_);
input		CLK;
input		RESET;
output		bus_531ba8cc_;
reg		kicker_1=1'h0;
wire		bus_18f4339b_;
wire		bus_605d0772_;
wire		bus_21fdff1c_;
reg		kicker_res=1'h0;
wire		bus_7ba7f1ff_;
reg		kicker_2=1'h0;
assign bus_531ba8cc_=kicker_res;
always @(posedge CLK)
begin
kicker_1<=bus_18f4339b_;
end
assign bus_18f4339b_=~RESET;
assign bus_605d0772_=~kicker_2;
assign bus_21fdff1c_=kicker_1&bus_18f4339b_&bus_605d0772_;
always @(posedge CLK)
begin
kicker_res<=bus_21fdff1c_;
end
assign bus_7ba7f1ff_=bus_18f4339b_&kicker_1;
always @(posedge CLK)
begin
kicker_2<=bus_7ba7f1ff_;
end
endmodule



module medianRow8_endianswapper_2611c5d4_(endianswapper_2611c5d4_in, endianswapper_2611c5d4_out);
input	[31:0]	endianswapper_2611c5d4_in;
output	[31:0]	endianswapper_2611c5d4_out;
assign endianswapper_2611c5d4_out=endianswapper_2611c5d4_in;
endmodule



module medianRow8_endianswapper_22b5ee60_(endianswapper_22b5ee60_in, endianswapper_22b5ee60_out);
input	[31:0]	endianswapper_22b5ee60_in;
output	[31:0]	endianswapper_22b5ee60_out;
assign endianswapper_22b5ee60_out=endianswapper_22b5ee60_in;
endmodule



module medianRow8_stateVar_i(bus_3dc9d8cd_, bus_72e7b464_, bus_560d84ec_, bus_6afae50e_, bus_1063f444_, bus_01dbecb4_, bus_6395f3b8_);
input		bus_3dc9d8cd_;
input		bus_72e7b464_;
input		bus_560d84ec_;
input	[31:0]	bus_6afae50e_;
input		bus_1063f444_;
input	[31:0]	bus_01dbecb4_;
output	[31:0]	bus_6395f3b8_;
wire	[31:0]	endianswapper_2611c5d4_out;
wire	[31:0]	endianswapper_22b5ee60_out;
wire		or_4c85aac2_u0;
reg	[31:0]	stateVar_i_u56=32'h0;
wire	[31:0]	mux_2af4873a_u0;
medianRow8_endianswapper_2611c5d4_ medianRow8_endianswapper_2611c5d4__1(.endianswapper_2611c5d4_in(mux_2af4873a_u0), 
  .endianswapper_2611c5d4_out(endianswapper_2611c5d4_out));
medianRow8_endianswapper_22b5ee60_ medianRow8_endianswapper_22b5ee60__1(.endianswapper_22b5ee60_in(stateVar_i_u56), 
  .endianswapper_22b5ee60_out(endianswapper_22b5ee60_out));
assign or_4c85aac2_u0=bus_560d84ec_|bus_1063f444_;
always @(posedge bus_3dc9d8cd_ or posedge bus_72e7b464_)
begin
if (bus_72e7b464_)
stateVar_i_u56<=32'h0;
else if (or_4c85aac2_u0)
stateVar_i_u56<=endianswapper_2611c5d4_out;
end
assign bus_6395f3b8_=endianswapper_22b5ee60_out;
assign mux_2af4873a_u0=(bus_560d84ec_)?bus_6afae50e_:32'h0;
endmodule



module medianRow8_simplememoryreferee_47adb244_(bus_6ea61510_, bus_2de8de13_, bus_4a6e6ce1_, bus_7d041b8f_, bus_1e26d567_, bus_1133200f_, bus_2450d173_, bus_3c388326_, bus_6c107e20_, bus_36e68cd5_, bus_19f8d507_, bus_02cef7ce_, bus_6a38488e_, bus_1e7dd4de_, bus_3908a74f_, bus_14a6e8e9_, bus_223b8fad_, bus_0d2495e6_, bus_1d6f7ade_, bus_0a4f40f6_, bus_080ff39c_);
input		bus_6ea61510_;
input		bus_2de8de13_;
input		bus_4a6e6ce1_;
input	[31:0]	bus_7d041b8f_;
input		bus_1e26d567_;
input	[31:0]	bus_1133200f_;
input	[31:0]	bus_2450d173_;
input	[2:0]	bus_3c388326_;
input		bus_6c107e20_;
input		bus_36e68cd5_;
input	[31:0]	bus_19f8d507_;
input	[31:0]	bus_02cef7ce_;
input	[2:0]	bus_6a38488e_;
output	[31:0]	bus_1e7dd4de_;
output	[31:0]	bus_3908a74f_;
output		bus_14a6e8e9_;
output		bus_223b8fad_;
output	[2:0]	bus_0d2495e6_;
output		bus_1d6f7ade_;
output	[31:0]	bus_0a4f40f6_;
output		bus_080ff39c_;
wire	[31:0]	mux_3b69eba9_u0;
reg		done_qual_u236=1'h0;
wire		and_082c7768_u0;
wire		or_60fdc6f7_u0;
wire		not_29c8e7c3_u0;
wire	[31:0]	mux_7729d044_u0;
wire		not_1d074c95_u0;
wire		or_29059ef3_u0;
reg		done_qual_u237_u0=1'h0;
wire		or_1ffc1ea5_u0;
wire		or_006b0152_u0;
wire		or_5ce3f2bf_u0;
wire		and_7a9d5e83_u0;
assign mux_3b69eba9_u0=(bus_1e26d567_)?bus_2450d173_:bus_02cef7ce_;
always @(posedge bus_6ea61510_)
begin
if (bus_2de8de13_)
done_qual_u236<=1'h0;
else done_qual_u236<=bus_1e26d567_;
end
assign and_082c7768_u0=or_006b0152_u0&bus_4a6e6ce1_;
assign or_60fdc6f7_u0=bus_1e26d567_|bus_36e68cd5_;
assign not_29c8e7c3_u0=~bus_4a6e6ce1_;
assign mux_7729d044_u0=(bus_1e26d567_)?{24'b0, bus_1133200f_[7:0]}:bus_19f8d507_;
assign bus_1e7dd4de_=mux_7729d044_u0;
assign bus_3908a74f_=mux_3b69eba9_u0;
assign bus_14a6e8e9_=or_60fdc6f7_u0;
assign bus_223b8fad_=or_1ffc1ea5_u0;
assign bus_0d2495e6_=3'h1;
assign bus_1d6f7ade_=and_7a9d5e83_u0;
assign bus_0a4f40f6_=bus_7d041b8f_;
assign bus_080ff39c_=and_082c7768_u0;
assign not_1d074c95_u0=~bus_4a6e6ce1_;
assign or_29059ef3_u0=bus_1e26d567_|done_qual_u236;
always @(posedge bus_6ea61510_)
begin
if (bus_2de8de13_)
done_qual_u237_u0<=1'h0;
else done_qual_u237_u0<=or_5ce3f2bf_u0;
end
assign or_1ffc1ea5_u0=bus_1e26d567_|or_5ce3f2bf_u0;
assign or_006b0152_u0=or_5ce3f2bf_u0|done_qual_u237_u0;
assign or_5ce3f2bf_u0=bus_6c107e20_|bus_36e68cd5_;
assign and_7a9d5e83_u0=or_29059ef3_u0&bus_4a6e6ce1_;
endmodule



module medianRow8_endianswapper_00fdd41e_(endianswapper_00fdd41e_in, endianswapper_00fdd41e_out);
input		endianswapper_00fdd41e_in;
output		endianswapper_00fdd41e_out;
assign endianswapper_00fdd41e_out=endianswapper_00fdd41e_in;
endmodule



module medianRow8_endianswapper_66939bdf_(endianswapper_66939bdf_in, endianswapper_66939bdf_out);
input		endianswapper_66939bdf_in;
output		endianswapper_66939bdf_out;
assign endianswapper_66939bdf_out=endianswapper_66939bdf_in;
endmodule



module medianRow8_stateVar_fsmState_medianRow8(bus_1d52540d_, bus_18af39b0_, bus_1eb2543a_, bus_70cc50ed_, bus_505a8057_);
input		bus_1d52540d_;
input		bus_18af39b0_;
input		bus_1eb2543a_;
input		bus_70cc50ed_;
output		bus_505a8057_;
reg		stateVar_fsmState_medianRow8_u2=1'h0;
wire		endianswapper_00fdd41e_out;
wire		endianswapper_66939bdf_out;
assign bus_505a8057_=endianswapper_66939bdf_out;
always @(posedge bus_1d52540d_ or posedge bus_18af39b0_)
begin
if (bus_18af39b0_)
stateVar_fsmState_medianRow8_u2<=1'h0;
else if (bus_1eb2543a_)
stateVar_fsmState_medianRow8_u2<=endianswapper_00fdd41e_out;
end
medianRow8_endianswapper_00fdd41e_ medianRow8_endianswapper_00fdd41e__1(.endianswapper_00fdd41e_in(bus_70cc50ed_), 
  .endianswapper_00fdd41e_out(endianswapper_00fdd41e_out));
medianRow8_endianswapper_66939bdf_ medianRow8_endianswapper_66939bdf__1(.endianswapper_66939bdf_in(stateVar_fsmState_medianRow8_u2), 
  .endianswapper_66939bdf_out(endianswapper_66939bdf_out));
endmodule



module medianRow8_scheduler(CLK, RESET, GO, port_6cce8b17_, port_7751b6af_, port_5dda934d_, port_329263d0_, port_3418d83a_, port_621bade7_, RESULT, RESULT_u2426, RESULT_u2427, RESULT_u2428, DONE);
input		CLK;
input		RESET;
input		GO;
input		port_6cce8b17_;
input	[31:0]	port_7751b6af_;
input		port_5dda934d_;
input		port_329263d0_;
input		port_3418d83a_;
input		port_621bade7_;
output		RESULT;
output		RESULT_u2426;
output		RESULT_u2427;
output		RESULT_u2428;
output		DONE;
wire		and_u4137_u0;
wire		lessThan;
wire signed	[31:0]	lessThan_b_signed;
wire signed	[31:0]	lessThan_a_signed;
wire signed	[31:0]	equals_a_signed;
wire signed	[31:0]	equals_b_signed;
wire		equals;
wire signed	[31:0]	equals_u250_a_signed;
wire signed	[31:0]	equals_u250_b_signed;
wire		equals_u250;
wire		not_u957_u0;
wire		and_u4138_u0;
wire		and_u4139_u0;
wire		andOp;
wire		and_u4140_u0;
wire		and_u4141_u0;
wire		not_u958_u0;
wire		simplePinWrite;
wire		and_u4142_u0;
wire		and_u4143_u0;
wire signed	[31:0]	equals_u251_b_signed;
wire		equals_u251;
wire signed	[31:0]	equals_u251_a_signed;
wire		and_u4144_u0;
wire		not_u959_u0;
wire		and_u4145_u0;
wire		andOp_u88;
wire		and_u4146_u0;
wire		and_u4147_u0;
wire		not_u960_u0;
wire		simplePinWrite_u656;
wire		not_u961_u0;
wire		and_u4148_u0;
wire		and_u4149_u0;
wire		not_u962_u0;
wire		and_u4150_u0;
wire		and_u4151_u0;
wire		simplePinWrite_u657;
reg		reg_0f878065_u0=1'h0;
wire		or_u1598_u0;
wire		and_u4152_u0;
wire		and_u4153_u0;
wire		and_u4154_u0;
wire		and_u4155_u0;
wire		or_u1599_u0;
reg		and_delayed_u415=1'h0;
wire		mux_u2116;
wire		or_u1600_u0;
wire		and_u4156_u0;
reg		reg_74273560_u0=1'h0;
wire		and_u4157_u0;
wire		or_u1601_u0;
wire		or_u1602_u0;
wire		and_u4158_u0;
wire		and_u4159_u0;
reg		and_delayed_u416_u0=1'h0;
wire		mux_u2117_u0;
wire		or_u1603_u0;
wire		receive_go_merge;
wire		or_u1604_u0;
reg		loopControl_u113=1'h0;
reg		syncEnable_u1607=1'h0;
reg		reg_0e3df0bc_u0=1'h0;
reg		reg_0e3df0bc_result_delayed_u0=1'h0;
wire		mux_u2118_u0;
wire		or_u1605_u0;
assign and_u4137_u0=or_u1604_u0&or_u1604_u0;
assign lessThan_a_signed=port_7751b6af_;
assign lessThan_b_signed=32'h65;
assign lessThan=lessThan_a_signed<lessThan_b_signed;
assign equals_a_signed=port_7751b6af_;
assign equals_b_signed=32'h65;
assign equals=equals_a_signed==equals_b_signed;
assign equals_u250_a_signed={31'b0, port_6cce8b17_};
assign equals_u250_b_signed=32'h0;
assign equals_u250=equals_u250_a_signed==equals_u250_b_signed;
assign not_u957_u0=~equals_u250;
assign and_u4138_u0=and_u4137_u0&equals_u250;
assign and_u4139_u0=and_u4137_u0&not_u957_u0;
assign andOp=lessThan&port_5dda934d_;
assign and_u4140_u0=and_u4143_u0&not_u958_u0;
assign and_u4141_u0=and_u4143_u0&andOp;
assign not_u958_u0=~andOp;
assign simplePinWrite=and_u4142_u0&{1{and_u4142_u0}};
assign and_u4142_u0=and_u4141_u0&and_u4143_u0;
assign and_u4143_u0=and_u4138_u0&and_u4137_u0;
assign equals_u251_a_signed={31'b0, port_6cce8b17_};
assign equals_u251_b_signed=32'h1;
assign equals_u251=equals_u251_a_signed==equals_u251_b_signed;
assign and_u4144_u0=and_u4137_u0&equals_u251;
assign not_u959_u0=~equals_u251;
assign and_u4145_u0=and_u4137_u0&not_u959_u0;
assign andOp_u88=lessThan&port_5dda934d_;
assign and_u4146_u0=and_u4158_u0&not_u960_u0;
assign and_u4147_u0=and_u4158_u0&andOp_u88;
assign not_u960_u0=~andOp_u88;
assign simplePinWrite_u656=and_u4157_u0&{1{and_u4157_u0}};
assign not_u961_u0=~equals;
assign and_u4148_u0=and_u4156_u0&equals;
assign and_u4149_u0=and_u4156_u0&not_u961_u0;
assign not_u962_u0=~port_621bade7_;
assign and_u4150_u0=and_u4154_u0&port_621bade7_;
assign and_u4151_u0=and_u4154_u0&not_u962_u0;
assign simplePinWrite_u657=and_u4152_u0&{1{and_u4152_u0}};
always @(posedge CLK or posedge RESET)
begin
if (RESET)
reg_0f878065_u0<=1'h0;
else reg_0f878065_u0<=and_u4153_u0;
end
assign or_u1598_u0=reg_0f878065_u0|port_3418d83a_;
assign and_u4152_u0=and_u4150_u0&and_u4154_u0;
assign and_u4153_u0=and_u4151_u0&and_u4154_u0;
assign and_u4154_u0=and_u4148_u0&and_u4156_u0;
assign and_u4155_u0=and_u4149_u0&and_u4156_u0;
assign or_u1599_u0=or_u1598_u0|and_delayed_u415;
always @(posedge CLK or posedge RESET)
begin
if (RESET)
and_delayed_u415<=1'h0;
else and_delayed_u415<=and_u4155_u0;
end
assign mux_u2116=(and_u4157_u0)?1'h1:1'h0;
assign or_u1600_u0=and_u4157_u0|and_u4152_u0;
assign and_u4156_u0=and_u4146_u0&and_u4158_u0;
always @(posedge CLK or posedge RESET)
begin
if (RESET)
reg_74273560_u0<=1'h0;
else reg_74273560_u0<=and_u4157_u0;
end
assign and_u4157_u0=and_u4147_u0&and_u4158_u0;
assign or_u1601_u0=reg_74273560_u0|or_u1599_u0;
assign or_u1602_u0=and_delayed_u416_u0|or_u1601_u0;
assign and_u4158_u0=and_u4144_u0&and_u4137_u0;
assign and_u4159_u0=and_u4145_u0&and_u4137_u0;
always @(posedge CLK or posedge RESET)
begin
if (RESET)
and_delayed_u416_u0<=1'h0;
else and_delayed_u416_u0<=and_u4159_u0;
end
assign mux_u2117_u0=(and_u4142_u0)?1'h1:mux_u2116;
assign or_u1603_u0=and_u4142_u0|or_u1600_u0;
assign receive_go_merge=simplePinWrite|simplePinWrite_u656;
assign or_u1604_u0=reg_0e3df0bc_result_delayed_u0|loopControl_u113;
always @(posedge CLK or posedge syncEnable_u1607)
begin
if (syncEnable_u1607)
loopControl_u113<=1'h0;
else loopControl_u113<=or_u1602_u0;
end
always @(posedge CLK)
begin
if (reg_0e3df0bc_result_delayed_u0)
syncEnable_u1607<=RESET;
end
always @(posedge CLK or posedge RESET)
begin
if (RESET)
reg_0e3df0bc_u0<=1'h0;
else reg_0e3df0bc_u0<=GO;
end
always @(posedge CLK or posedge RESET)
begin
if (RESET)
reg_0e3df0bc_result_delayed_u0<=1'h0;
else reg_0e3df0bc_result_delayed_u0<=reg_0e3df0bc_u0;
end
assign mux_u2118_u0=(GO)?1'h0:mux_u2117_u0;
assign or_u1605_u0=GO|or_u1603_u0;
assign RESULT=or_u1605_u0;
assign RESULT_u2426=mux_u2118_u0;
assign RESULT_u2427=receive_go_merge;
assign RESULT_u2428=simplePinWrite_u657;
assign DONE=1'h0;
endmodule


