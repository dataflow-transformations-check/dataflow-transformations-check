// __  ___ __ ___  _ __   ___  ___ 
// \ \/ / '__/ _ \| '_ \ / _ \/ __|
//  >  <| | | (_) | | | | (_) \__ \
// /_/\_\_|  \___/|_| |_|\___/|___/
// 
// Xronos synthesizer version
// Run date: Fri 23 Feb 2018 14:28:47 +0000
// 

module medianRow9(median_SEND, median_COUNT, median_DATA, median_RDY, in1_ACK, in1_COUNT, in1_DATA, CLK, RESET, median_ACK, in1_SEND);
output		median_SEND;
output	[15:0]	median_COUNT;
output	[7:0]	median_DATA;
input		median_RDY;
output		in1_ACK;
input	[15:0]	in1_COUNT;
wire		receive_go;
wire		compute_median_go;
wire		compute_median_done;
input	[7:0]	in1_DATA;
input		CLK;
input		RESET;
input		median_ACK;
wire		receive_done;
input		in1_SEND;
wire	[2:0]	receive_u322;
wire	[31:0]	receive_u321;
wire	[31:0]	receive_u320;
wire		receive;
wire	[31:0]	receive_u318;
wire		receive_u319;
wire		medianRow9_receive_instance_DONE;
wire		receive_u323;
wire	[2:0]	bus_1b632b3a_;
wire		bus_7b0bce57_;
wire		bus_7a5cc14e_;
wire	[31:0]	bus_665874a7_;
wire		bus_745e05cc_;
wire	[31:0]	bus_25fff9cf_;
wire	[31:0]	bus_029f727b_;
wire		bus_2f22b733_;
wire	[31:0]	bus_54d3b841_;
wire	[31:0]	bus_22edb834_;
wire		bus_2e403d2a_;
wire		bus_1ef60b57_;
wire	[31:0]	compute_median_u748;
wire	[31:0]	compute_median_u744;
wire		compute_median_u747;
wire	[7:0]	compute_median_u754;
wire		compute_median;
wire		compute_median_u755;
wire	[2:0]	compute_median_u752;
wire	[31:0]	compute_median_u745;
wire		compute_median_u750;
wire		compute_median_u743;
wire	[2:0]	compute_median_u749;
wire	[31:0]	compute_median_u742;
wire	[2:0]	compute_median_u746;
wire	[31:0]	compute_median_u751;
wire		medianRow9_compute_median_instance_DONE;
wire	[15:0]	compute_median_u753;
wire	[31:0]	bus_6d3d38a5_;
wire		bus_7891141e_;
wire		scheduler_u835;
wire		scheduler;
wire		scheduler_u836;
wire		medianRow9_scheduler_instance_DONE;
wire		scheduler_u837;
wire		bus_0e437ee2_;
wire		bus_23d907af_;
wire		bus_43856b8d_;
wire		bus_66285afd_;
wire	[31:0]	bus_319970ff_;
wire	[31:0]	bus_75536eaf_;
wire		bus_40f85ec8_;
wire	[2:0]	bus_6f64520a_;
wire	[31:0]	bus_1a42dfe8_;
wire		bus_100f7b95_;
wire		bus_75e12fdb_;
assign median_SEND=compute_median_u755;
assign median_COUNT=compute_median_u753;
assign median_DATA=compute_median_u754;
assign in1_ACK=receive_u323;
assign receive_go=scheduler_u836;
assign compute_median_go=scheduler_u837;
assign compute_median_done=bus_7891141e_;
assign receive_done=bus_43856b8d_;
medianRow9_receive medianRow9_receive_instance(.CLK(CLK), .RESET(bus_75e12fdb_), 
  .GO(receive_go), .port_00298f16_(bus_6d3d38a5_), .port_3c18d9b5_(bus_745e05cc_), 
  .port_09122f6a_(in1_DATA), .DONE(medianRow9_receive_instance_DONE), .RESULT(receive), 
  .RESULT_u2383(receive_u318), .RESULT_u2384(receive_u319), .RESULT_u2385(receive_u320), 
  .RESULT_u2386(receive_u321), .RESULT_u2387(receive_u322), .RESULT_u2388(receive_u323));
medianRow9_simplememoryreferee_27984d5e_ medianRow9_simplememoryreferee_27984d5e__1(.bus_5f55b704_(CLK), 
  .bus_3fa20a73_(bus_75e12fdb_), .bus_039b1493_(bus_2e403d2a_), .bus_1e3c87a2_(bus_22edb834_), 
  .bus_552dc4c7_(receive_u319), .bus_6c07ef23_({24'b0, receive_u321[7:0]}), .bus_29fc91dc_(receive_u320), 
  .bus_05c6e84e_(3'h1), .bus_06eb670f_(compute_median_u747), .bus_316e11c8_(compute_median_u743), 
  .bus_3f000861_(compute_median_u745), .bus_14a9260d_(compute_median_u744), .bus_3ed264e9_(3'h1), 
  .bus_665874a7_(bus_665874a7_), .bus_25fff9cf_(bus_25fff9cf_), .bus_7a5cc14e_(bus_7a5cc14e_), 
  .bus_7b0bce57_(bus_7b0bce57_), .bus_1b632b3a_(bus_1b632b3a_), .bus_745e05cc_(bus_745e05cc_), 
  .bus_029f727b_(bus_029f727b_), .bus_2f22b733_(bus_2f22b733_));
medianRow9_structuralmemory_4279926b_ medianRow9_structuralmemory_4279926b__1(.CLK_u99(CLK), 
  .bus_232a4242_(bus_75e12fdb_), .bus_4e922c35_(bus_319970ff_), .bus_5c215bc8_(3'h1), 
  .bus_1696dbcb_(bus_40f85ec8_), .bus_69f1a679_(bus_25fff9cf_), .bus_5387a8fe_(3'h1), 
  .bus_0004c004_(bus_7b0bce57_), .bus_1db0b09e_(bus_7a5cc14e_), .bus_07e334cd_(bus_665874a7_), 
  .bus_54d3b841_(bus_54d3b841_), .bus_1ef60b57_(bus_1ef60b57_), .bus_22edb834_(bus_22edb834_), 
  .bus_2e403d2a_(bus_2e403d2a_));
medianRow9_compute_median medianRow9_compute_median_instance(.CLK(CLK), .RESET(bus_75e12fdb_), 
  .GO(compute_median_go), .port_44a742d5_(bus_2f22b733_), .port_122ec10d_(bus_2f22b733_), 
  .port_2c990e5f_(bus_029f727b_), .port_257aeac3_(bus_66285afd_), .port_7bc1f42b_(bus_1a42dfe8_), 
  .DONE(medianRow9_compute_median_instance_DONE), .RESULT(compute_median), .RESULT_u2389(compute_median_u742), 
  .RESULT_u2396(compute_median_u743), .RESULT_u2397(compute_median_u744), .RESULT_u2398(compute_median_u745), 
  .RESULT_u2399(compute_median_u746), .RESULT_u2393(compute_median_u747), .RESULT_u2394(compute_median_u748), 
  .RESULT_u2395(compute_median_u749), .RESULT_u2390(compute_median_u750), .RESULT_u2391(compute_median_u751), 
  .RESULT_u2392(compute_median_u752), .RESULT_u2400(compute_median_u753), .RESULT_u2401(compute_median_u754), 
  .RESULT_u2402(compute_median_u755));
medianRow9_stateVar_i medianRow9_stateVar_i_1(.bus_6751732d_(CLK), .bus_4d770ef3_(bus_75e12fdb_), 
  .bus_1073d54e_(receive), .bus_09229d89_(receive_u318), .bus_6c4c219b_(compute_median), 
  .bus_352f6283_(32'h0), .bus_6d3d38a5_(bus_6d3d38a5_));
assign bus_7891141e_=medianRow9_compute_median_instance_DONE&{1{medianRow9_compute_median_instance_DONE}};
medianRow9_scheduler medianRow9_scheduler_instance(.CLK(CLK), .RESET(bus_75e12fdb_), 
  .GO(bus_0e437ee2_), .port_09c60b28_(bus_23d907af_), .port_7870cd79_(bus_6d3d38a5_), 
  .port_51f84618_(compute_median_done), .port_7f70966f_(median_RDY), .port_2d426ac7_(receive_done), 
  .port_1b421d5a_(in1_SEND), .DONE(medianRow9_scheduler_instance_DONE), .RESULT(scheduler), 
  .RESULT_u2403(scheduler_u835), .RESULT_u2404(scheduler_u836), .RESULT_u2405(scheduler_u837));
medianRow9_Kicker_65 medianRow9_Kicker_65_1(.CLK(CLK), .RESET(bus_75e12fdb_), .bus_0e437ee2_(bus_0e437ee2_));
medianRow9_stateVar_fsmState_medianRow9 medianRow9_stateVar_fsmState_medianRow9_1(.bus_676ea960_(CLK), 
  .bus_0cf2d95d_(bus_75e12fdb_), .bus_54b80ca4_(scheduler), .bus_5c2d8b47_(scheduler_u835), 
  .bus_23d907af_(bus_23d907af_));
assign bus_43856b8d_=medianRow9_receive_instance_DONE&{1{medianRow9_receive_instance_DONE}};
medianRow9_simplememoryreferee_09491173_ medianRow9_simplememoryreferee_09491173__1(.bus_36ebb0ea_(CLK), 
  .bus_7cd53092_(bus_75e12fdb_), .bus_3f7eacae_(bus_1ef60b57_), .bus_00c9fb1b_(bus_54d3b841_), 
  .bus_57fb331f_(compute_median_u750), .bus_76743299_(compute_median_u751), .bus_522f8603_(3'h1), 
  .bus_75536eaf_(bus_75536eaf_), .bus_319970ff_(bus_319970ff_), .bus_100f7b95_(bus_100f7b95_), 
  .bus_40f85ec8_(bus_40f85ec8_), .bus_6f64520a_(bus_6f64520a_), .bus_1a42dfe8_(bus_1a42dfe8_), 
  .bus_66285afd_(bus_66285afd_));
medianRow9_globalreset_physical_7f441d78_ medianRow9_globalreset_physical_7f441d78__1(.bus_373d06c2_(CLK), 
  .bus_7a39ab19_(RESET), .bus_75e12fdb_(bus_75e12fdb_));
endmodule



module medianRow9_receive(CLK, RESET, GO, port_00298f16_, port_3c18d9b5_, port_09122f6a_, RESULT, RESULT_u2383, RESULT_u2384, RESULT_u2385, RESULT_u2386, RESULT_u2387, RESULT_u2388, DONE);
input		CLK;
input		RESET;
input		GO;
input	[31:0]	port_00298f16_;
input		port_3c18d9b5_;
input	[7:0]	port_09122f6a_;
output		RESULT;
output	[31:0]	RESULT_u2383;
output		RESULT_u2384;
output	[31:0]	RESULT_u2385;
output	[31:0]	RESULT_u2386;
output	[2:0]	RESULT_u2387;
output		RESULT_u2388;
output		DONE;
wire		simplePinWrite;
wire	[31:0]	add;
wire		and_u4076_u0;
wire		or_u1570_u0;
reg		reg_69164222_u0=1'h0;
wire	[31:0]	add_u749;
reg		reg_6a824449_u0=1'h0;
assign simplePinWrite=GO&{1{GO}};
assign add=port_00298f16_+32'h0;
assign and_u4076_u0=reg_69164222_u0&port_3c18d9b5_;
assign or_u1570_u0=and_u4076_u0|RESET;
always @(posedge CLK or posedge GO or posedge or_u1570_u0)
begin
if (or_u1570_u0)
reg_69164222_u0<=1'h0;
else if (GO)
reg_69164222_u0<=1'h1;
else reg_69164222_u0<=reg_69164222_u0;
end
assign add_u749=port_00298f16_+32'h1;
always @(posedge CLK or posedge RESET)
begin
if (RESET)
reg_6a824449_u0<=1'h0;
else reg_6a824449_u0<=GO;
end
assign RESULT=GO;
assign RESULT_u2383=add_u749;
assign RESULT_u2384=GO;
assign RESULT_u2385=add;
assign RESULT_u2386={24'b0, port_09122f6a_};
assign RESULT_u2387=3'h1;
assign RESULT_u2388=simplePinWrite;
assign DONE=reg_6a824449_u0;
endmodule



module medianRow9_simplememoryreferee_27984d5e_(bus_5f55b704_, bus_3fa20a73_, bus_039b1493_, bus_1e3c87a2_, bus_552dc4c7_, bus_6c07ef23_, bus_29fc91dc_, bus_05c6e84e_, bus_06eb670f_, bus_316e11c8_, bus_3f000861_, bus_14a9260d_, bus_3ed264e9_, bus_665874a7_, bus_25fff9cf_, bus_7a5cc14e_, bus_7b0bce57_, bus_1b632b3a_, bus_745e05cc_, bus_029f727b_, bus_2f22b733_);
input		bus_5f55b704_;
input		bus_3fa20a73_;
input		bus_039b1493_;
input	[31:0]	bus_1e3c87a2_;
input		bus_552dc4c7_;
input	[31:0]	bus_6c07ef23_;
input	[31:0]	bus_29fc91dc_;
input	[2:0]	bus_05c6e84e_;
input		bus_06eb670f_;
input		bus_316e11c8_;
input	[31:0]	bus_3f000861_;
input	[31:0]	bus_14a9260d_;
input	[2:0]	bus_3ed264e9_;
output	[31:0]	bus_665874a7_;
output	[31:0]	bus_25fff9cf_;
output		bus_7a5cc14e_;
output		bus_7b0bce57_;
output	[2:0]	bus_1b632b3a_;
output		bus_745e05cc_;
output	[31:0]	bus_029f727b_;
output		bus_2f22b733_;
wire		or_6a35f355_u0;
reg		done_qual_u234=1'h0;
reg		done_qual_u235_u0=1'h0;
wire		or_7773f036_u0;
wire		or_2dde42d8_u0;
wire		not_00c67d56_u0;
wire		or_1dadca3a_u0;
wire		and_12aa2e12_u0;
wire		not_2c44983d_u0;
wire		or_2e8aee70_u0;
wire	[31:0]	mux_625e07a0_u0;
wire	[31:0]	mux_020537ba_u0;
wire		and_1ea4531b_u0;
assign or_6a35f355_u0=or_2e8aee70_u0|done_qual_u235_u0;
always @(posedge bus_5f55b704_)
begin
if (bus_3fa20a73_)
done_qual_u234<=1'h0;
else done_qual_u234<=bus_552dc4c7_;
end
always @(posedge bus_5f55b704_)
begin
if (bus_3fa20a73_)
done_qual_u235_u0<=1'h0;
else done_qual_u235_u0<=or_2e8aee70_u0;
end
assign or_7773f036_u0=bus_552dc4c7_|or_2e8aee70_u0;
assign or_2dde42d8_u0=bus_552dc4c7_|bus_316e11c8_;
assign not_00c67d56_u0=~bus_039b1493_;
assign or_1dadca3a_u0=bus_552dc4c7_|done_qual_u234;
assign and_12aa2e12_u0=or_1dadca3a_u0&bus_039b1493_;
assign not_2c44983d_u0=~bus_039b1493_;
assign or_2e8aee70_u0=bus_06eb670f_|bus_316e11c8_;
assign bus_665874a7_=mux_020537ba_u0;
assign bus_25fff9cf_=mux_625e07a0_u0;
assign bus_7a5cc14e_=or_2dde42d8_u0;
assign bus_7b0bce57_=or_7773f036_u0;
assign bus_1b632b3a_=3'h1;
assign bus_745e05cc_=and_12aa2e12_u0;
assign bus_029f727b_=bus_1e3c87a2_;
assign bus_2f22b733_=and_1ea4531b_u0;
assign mux_625e07a0_u0=(bus_552dc4c7_)?bus_29fc91dc_:bus_14a9260d_;
assign mux_020537ba_u0=(bus_552dc4c7_)?{24'b0, bus_6c07ef23_[7:0]}:bus_3f000861_;
assign and_1ea4531b_u0=or_6a35f355_u0&bus_039b1493_;
endmodule



module medianRow9_forge_memory_101x32_159(CLK, ENA, WEA, DINA, ENB, ADDRA, ADDRB, DOUTA, DOUTB, DONEA, DONEB);
input		CLK;
input		ENA;
input		WEA;
input	[31:0]	DINA;
input		ENB;
input	[31:0]	ADDRA;
input	[31:0]	ADDRB;
output	[31:0]	DOUTA;
output	[31:0]	DOUTB;
output		DONEA;
output		DONEB;
wire		wea_0;
wire	[31:0]	pre_douta_0;
wire	[31:0]	pre_doutb_0;
wire		wea_1;
wire	[31:0]	pre_douta_1;
wire	[31:0]	pre_doutb_1;
reg		wea_done;
reg		rea_done;
reg	[31:0]	mux_outa;
reg	[31:0]	mux_outb;
assign wea_0=WEA&(ADDRA[31:6]==26'h0);
assign wea_1=WEA&(ADDRA[31:6]==26'h1);
always @(posedge CLK)
begin
wea_done<=WEA;
end
assign DONEA=rea_done|wea_done;
always @(posedge CLK)
begin
end
assign DONEB=ENB;
always @(pre_douta_0 or pre_douta_1 or ADDRA)
begin
case (ADDRA[31:6])26'd0:mux_outa=pre_douta_0;
26'd1:mux_outa=pre_douta_1;
default:mux_outa=32'h0;
endcase end
assign DOUTA=mux_outa;
always @(pre_doutb_0 or pre_doutb_1 or ADDRB)
begin
case (ADDRB[31:6])26'd0:mux_outb=pre_doutb_0;
26'd1:mux_outb=pre_doutb_1;
default:mux_outb=32'h0;
endcase end
assign DOUTB=mux_outb;
// Memory array element: COL: 0, ROW: 0
//  Initialization of Dual Port LUT ram now done through explicit
// parameter setting.
RAM64X1D#(.INIT(64'h0000000000000000))RAM64X1D_instance_3392(.WCLK(CLK), .WE(wea_0), 
  .A0(ADDRA[0]), .A1(ADDRA[1]), .A2(ADDRA[2]), .A3(ADDRA[3]), .A4(ADDRA[4]), .A5(ADDRA[5]), 
  .D(DINA[0]), .SPO(pre_douta_0[0]), .DPRA0(ADDRB[0]), .DPRA1(ADDRB[1]), .DPRA2(ADDRB[2]), 
  .DPRA3(ADDRB[3]), .DPRA4(ADDRB[4]), .DPRA5(ADDRB[5]), .DPO(pre_doutb_0[0]));
// Memory array element: COL: 0, ROW: 1
//  Initialization of Dual Port LUT ram now done through explicit
// parameter setting.
RAM64X1D#(.INIT(64'h0000000000000000))RAM64X1D_instance_3393(.WCLK(CLK), .WE(wea_0), 
  .A0(ADDRA[0]), .A1(ADDRA[1]), .A2(ADDRA[2]), .A3(ADDRA[3]), .A4(ADDRA[4]), .A5(ADDRA[5]), 
  .D(DINA[1]), .SPO(pre_douta_0[1]), .DPRA0(ADDRB[0]), .DPRA1(ADDRB[1]), .DPRA2(ADDRB[2]), 
  .DPRA3(ADDRB[3]), .DPRA4(ADDRB[4]), .DPRA5(ADDRB[5]), .DPO(pre_doutb_0[1]));
// Memory array element: COL: 0, ROW: 2
//  Initialization of Dual Port LUT ram now done through explicit
// parameter setting.
RAM64X1D#(.INIT(64'h0000000000000000))RAM64X1D_instance_3394(.WCLK(CLK), .WE(wea_0), 
  .A0(ADDRA[0]), .A1(ADDRA[1]), .A2(ADDRA[2]), .A3(ADDRA[3]), .A4(ADDRA[4]), .A5(ADDRA[5]), 
  .D(DINA[2]), .SPO(pre_douta_0[2]), .DPRA0(ADDRB[0]), .DPRA1(ADDRB[1]), .DPRA2(ADDRB[2]), 
  .DPRA3(ADDRB[3]), .DPRA4(ADDRB[4]), .DPRA5(ADDRB[5]), .DPO(pre_doutb_0[2]));
// Memory array element: COL: 0, ROW: 3
//  Initialization of Dual Port LUT ram now done through explicit
// parameter setting.
RAM64X1D#(.INIT(64'h0000000000000000))RAM64X1D_instance_3395(.WCLK(CLK), .WE(wea_0), 
  .A0(ADDRA[0]), .A1(ADDRA[1]), .A2(ADDRA[2]), .A3(ADDRA[3]), .A4(ADDRA[4]), .A5(ADDRA[5]), 
  .D(DINA[3]), .SPO(pre_douta_0[3]), .DPRA0(ADDRB[0]), .DPRA1(ADDRB[1]), .DPRA2(ADDRB[2]), 
  .DPRA3(ADDRB[3]), .DPRA4(ADDRB[4]), .DPRA5(ADDRB[5]), .DPO(pre_doutb_0[3]));
// Memory array element: COL: 0, ROW: 4
//  Initialization of Dual Port LUT ram now done through explicit
// parameter setting.
RAM64X1D#(.INIT(64'h0000000000000000))RAM64X1D_instance_3396(.WCLK(CLK), .WE(wea_0), 
  .A0(ADDRA[0]), .A1(ADDRA[1]), .A2(ADDRA[2]), .A3(ADDRA[3]), .A4(ADDRA[4]), .A5(ADDRA[5]), 
  .D(DINA[4]), .SPO(pre_douta_0[4]), .DPRA0(ADDRB[0]), .DPRA1(ADDRB[1]), .DPRA2(ADDRB[2]), 
  .DPRA3(ADDRB[3]), .DPRA4(ADDRB[4]), .DPRA5(ADDRB[5]), .DPO(pre_doutb_0[4]));
// Memory array element: COL: 0, ROW: 5
//  Initialization of Dual Port LUT ram now done through explicit
// parameter setting.
RAM64X1D#(.INIT(64'h0000000000000000))RAM64X1D_instance_3397(.WCLK(CLK), .WE(wea_0), 
  .A0(ADDRA[0]), .A1(ADDRA[1]), .A2(ADDRA[2]), .A3(ADDRA[3]), .A4(ADDRA[4]), .A5(ADDRA[5]), 
  .D(DINA[5]), .SPO(pre_douta_0[5]), .DPRA0(ADDRB[0]), .DPRA1(ADDRB[1]), .DPRA2(ADDRB[2]), 
  .DPRA3(ADDRB[3]), .DPRA4(ADDRB[4]), .DPRA5(ADDRB[5]), .DPO(pre_doutb_0[5]));
// Memory array element: COL: 0, ROW: 6
//  Initialization of Dual Port LUT ram now done through explicit
// parameter setting.
RAM64X1D#(.INIT(64'h0000000000000000))RAM64X1D_instance_3398(.WCLK(CLK), .WE(wea_0), 
  .A0(ADDRA[0]), .A1(ADDRA[1]), .A2(ADDRA[2]), .A3(ADDRA[3]), .A4(ADDRA[4]), .A5(ADDRA[5]), 
  .D(DINA[6]), .SPO(pre_douta_0[6]), .DPRA0(ADDRB[0]), .DPRA1(ADDRB[1]), .DPRA2(ADDRB[2]), 
  .DPRA3(ADDRB[3]), .DPRA4(ADDRB[4]), .DPRA5(ADDRB[5]), .DPO(pre_doutb_0[6]));
// Memory array element: COL: 0, ROW: 7
//  Initialization of Dual Port LUT ram now done through explicit
// parameter setting.
RAM64X1D#(.INIT(64'h0000000000000000))RAM64X1D_instance_3399(.WCLK(CLK), .WE(wea_0), 
  .A0(ADDRA[0]), .A1(ADDRA[1]), .A2(ADDRA[2]), .A3(ADDRA[3]), .A4(ADDRA[4]), .A5(ADDRA[5]), 
  .D(DINA[7]), .SPO(pre_douta_0[7]), .DPRA0(ADDRB[0]), .DPRA1(ADDRB[1]), .DPRA2(ADDRB[2]), 
  .DPRA3(ADDRB[3]), .DPRA4(ADDRB[4]), .DPRA5(ADDRB[5]), .DPO(pre_doutb_0[7]));
// Memory array element: COL: 0, ROW: 8
//  Initialization of Dual Port LUT ram now done through explicit
// parameter setting.
RAM64X1D#(.INIT(64'h0000000000000000))RAM64X1D_instance_3400(.WCLK(CLK), .WE(wea_0), 
  .A0(ADDRA[0]), .A1(ADDRA[1]), .A2(ADDRA[2]), .A3(ADDRA[3]), .A4(ADDRA[4]), .A5(ADDRA[5]), 
  .D(DINA[8]), .SPO(pre_douta_0[8]), .DPRA0(ADDRB[0]), .DPRA1(ADDRB[1]), .DPRA2(ADDRB[2]), 
  .DPRA3(ADDRB[3]), .DPRA4(ADDRB[4]), .DPRA5(ADDRB[5]), .DPO(pre_doutb_0[8]));
// Memory array element: COL: 0, ROW: 9
//  Initialization of Dual Port LUT ram now done through explicit
// parameter setting.
RAM64X1D#(.INIT(64'h0000000000000000))RAM64X1D_instance_3401(.WCLK(CLK), .WE(wea_0), 
  .A0(ADDRA[0]), .A1(ADDRA[1]), .A2(ADDRA[2]), .A3(ADDRA[3]), .A4(ADDRA[4]), .A5(ADDRA[5]), 
  .D(DINA[9]), .SPO(pre_douta_0[9]), .DPRA0(ADDRB[0]), .DPRA1(ADDRB[1]), .DPRA2(ADDRB[2]), 
  .DPRA3(ADDRB[3]), .DPRA4(ADDRB[4]), .DPRA5(ADDRB[5]), .DPO(pre_doutb_0[9]));
// Memory array element: COL: 0, ROW: 10
//  Initialization of Dual Port LUT ram now done through explicit
// parameter setting.
RAM64X1D#(.INIT(64'h0000000000000000))RAM64X1D_instance_3402(.WCLK(CLK), .WE(wea_0), 
  .A0(ADDRA[0]), .A1(ADDRA[1]), .A2(ADDRA[2]), .A3(ADDRA[3]), .A4(ADDRA[4]), .A5(ADDRA[5]), 
  .D(DINA[10]), .SPO(pre_douta_0[10]), .DPRA0(ADDRB[0]), .DPRA1(ADDRB[1]), .DPRA2(ADDRB[2]), 
  .DPRA3(ADDRB[3]), .DPRA4(ADDRB[4]), .DPRA5(ADDRB[5]), .DPO(pre_doutb_0[10]));
// Memory array element: COL: 0, ROW: 11
//  Initialization of Dual Port LUT ram now done through explicit
// parameter setting.
RAM64X1D#(.INIT(64'h0000000000000000))RAM64X1D_instance_3403(.WCLK(CLK), .WE(wea_0), 
  .A0(ADDRA[0]), .A1(ADDRA[1]), .A2(ADDRA[2]), .A3(ADDRA[3]), .A4(ADDRA[4]), .A5(ADDRA[5]), 
  .D(DINA[11]), .SPO(pre_douta_0[11]), .DPRA0(ADDRB[0]), .DPRA1(ADDRB[1]), .DPRA2(ADDRB[2]), 
  .DPRA3(ADDRB[3]), .DPRA4(ADDRB[4]), .DPRA5(ADDRB[5]), .DPO(pre_doutb_0[11]));
// Memory array element: COL: 0, ROW: 12
//  Initialization of Dual Port LUT ram now done through explicit
// parameter setting.
RAM64X1D#(.INIT(64'h0000000000000000))RAM64X1D_instance_3404(.WCLK(CLK), .WE(wea_0), 
  .A0(ADDRA[0]), .A1(ADDRA[1]), .A2(ADDRA[2]), .A3(ADDRA[3]), .A4(ADDRA[4]), .A5(ADDRA[5]), 
  .D(DINA[12]), .SPO(pre_douta_0[12]), .DPRA0(ADDRB[0]), .DPRA1(ADDRB[1]), .DPRA2(ADDRB[2]), 
  .DPRA3(ADDRB[3]), .DPRA4(ADDRB[4]), .DPRA5(ADDRB[5]), .DPO(pre_doutb_0[12]));
// Memory array element: COL: 0, ROW: 13
//  Initialization of Dual Port LUT ram now done through explicit
// parameter setting.
RAM64X1D#(.INIT(64'h0000000000000000))RAM64X1D_instance_3405(.WCLK(CLK), .WE(wea_0), 
  .A0(ADDRA[0]), .A1(ADDRA[1]), .A2(ADDRA[2]), .A3(ADDRA[3]), .A4(ADDRA[4]), .A5(ADDRA[5]), 
  .D(DINA[13]), .SPO(pre_douta_0[13]), .DPRA0(ADDRB[0]), .DPRA1(ADDRB[1]), .DPRA2(ADDRB[2]), 
  .DPRA3(ADDRB[3]), .DPRA4(ADDRB[4]), .DPRA5(ADDRB[5]), .DPO(pre_doutb_0[13]));
// Memory array element: COL: 0, ROW: 14
//  Initialization of Dual Port LUT ram now done through explicit
// parameter setting.
RAM64X1D#(.INIT(64'h0000000000000000))RAM64X1D_instance_3406(.WCLK(CLK), .WE(wea_0), 
  .A0(ADDRA[0]), .A1(ADDRA[1]), .A2(ADDRA[2]), .A3(ADDRA[3]), .A4(ADDRA[4]), .A5(ADDRA[5]), 
  .D(DINA[14]), .SPO(pre_douta_0[14]), .DPRA0(ADDRB[0]), .DPRA1(ADDRB[1]), .DPRA2(ADDRB[2]), 
  .DPRA3(ADDRB[3]), .DPRA4(ADDRB[4]), .DPRA5(ADDRB[5]), .DPO(pre_doutb_0[14]));
// Memory array element: COL: 0, ROW: 15
//  Initialization of Dual Port LUT ram now done through explicit
// parameter setting.
RAM64X1D#(.INIT(64'h0000000000000000))RAM64X1D_instance_3407(.WCLK(CLK), .WE(wea_0), 
  .A0(ADDRA[0]), .A1(ADDRA[1]), .A2(ADDRA[2]), .A3(ADDRA[3]), .A4(ADDRA[4]), .A5(ADDRA[5]), 
  .D(DINA[15]), .SPO(pre_douta_0[15]), .DPRA0(ADDRB[0]), .DPRA1(ADDRB[1]), .DPRA2(ADDRB[2]), 
  .DPRA3(ADDRB[3]), .DPRA4(ADDRB[4]), .DPRA5(ADDRB[5]), .DPO(pre_doutb_0[15]));
// Memory array element: COL: 0, ROW: 16
//  Initialization of Dual Port LUT ram now done through explicit
// parameter setting.
RAM64X1D#(.INIT(64'h0000000000000000))RAM64X1D_instance_3408(.WCLK(CLK), .WE(wea_0), 
  .A0(ADDRA[0]), .A1(ADDRA[1]), .A2(ADDRA[2]), .A3(ADDRA[3]), .A4(ADDRA[4]), .A5(ADDRA[5]), 
  .D(DINA[16]), .SPO(pre_douta_0[16]), .DPRA0(ADDRB[0]), .DPRA1(ADDRB[1]), .DPRA2(ADDRB[2]), 
  .DPRA3(ADDRB[3]), .DPRA4(ADDRB[4]), .DPRA5(ADDRB[5]), .DPO(pre_doutb_0[16]));
// Memory array element: COL: 0, ROW: 17
//  Initialization of Dual Port LUT ram now done through explicit
// parameter setting.
RAM64X1D#(.INIT(64'h0000000000000000))RAM64X1D_instance_3409(.WCLK(CLK), .WE(wea_0), 
  .A0(ADDRA[0]), .A1(ADDRA[1]), .A2(ADDRA[2]), .A3(ADDRA[3]), .A4(ADDRA[4]), .A5(ADDRA[5]), 
  .D(DINA[17]), .SPO(pre_douta_0[17]), .DPRA0(ADDRB[0]), .DPRA1(ADDRB[1]), .DPRA2(ADDRB[2]), 
  .DPRA3(ADDRB[3]), .DPRA4(ADDRB[4]), .DPRA5(ADDRB[5]), .DPO(pre_doutb_0[17]));
// Memory array element: COL: 0, ROW: 18
//  Initialization of Dual Port LUT ram now done through explicit
// parameter setting.
RAM64X1D#(.INIT(64'h0000000000000000))RAM64X1D_instance_3410(.WCLK(CLK), .WE(wea_0), 
  .A0(ADDRA[0]), .A1(ADDRA[1]), .A2(ADDRA[2]), .A3(ADDRA[3]), .A4(ADDRA[4]), .A5(ADDRA[5]), 
  .D(DINA[18]), .SPO(pre_douta_0[18]), .DPRA0(ADDRB[0]), .DPRA1(ADDRB[1]), .DPRA2(ADDRB[2]), 
  .DPRA3(ADDRB[3]), .DPRA4(ADDRB[4]), .DPRA5(ADDRB[5]), .DPO(pre_doutb_0[18]));
// Memory array element: COL: 0, ROW: 19
//  Initialization of Dual Port LUT ram now done through explicit
// parameter setting.
RAM64X1D#(.INIT(64'h0000000000000000))RAM64X1D_instance_3411(.WCLK(CLK), .WE(wea_0), 
  .A0(ADDRA[0]), .A1(ADDRA[1]), .A2(ADDRA[2]), .A3(ADDRA[3]), .A4(ADDRA[4]), .A5(ADDRA[5]), 
  .D(DINA[19]), .SPO(pre_douta_0[19]), .DPRA0(ADDRB[0]), .DPRA1(ADDRB[1]), .DPRA2(ADDRB[2]), 
  .DPRA3(ADDRB[3]), .DPRA4(ADDRB[4]), .DPRA5(ADDRB[5]), .DPO(pre_doutb_0[19]));
// Memory array element: COL: 0, ROW: 20
//  Initialization of Dual Port LUT ram now done through explicit
// parameter setting.
RAM64X1D#(.INIT(64'h0000000000000000))RAM64X1D_instance_3412(.WCLK(CLK), .WE(wea_0), 
  .A0(ADDRA[0]), .A1(ADDRA[1]), .A2(ADDRA[2]), .A3(ADDRA[3]), .A4(ADDRA[4]), .A5(ADDRA[5]), 
  .D(DINA[20]), .SPO(pre_douta_0[20]), .DPRA0(ADDRB[0]), .DPRA1(ADDRB[1]), .DPRA2(ADDRB[2]), 
  .DPRA3(ADDRB[3]), .DPRA4(ADDRB[4]), .DPRA5(ADDRB[5]), .DPO(pre_doutb_0[20]));
// Memory array element: COL: 0, ROW: 21
//  Initialization of Dual Port LUT ram now done through explicit
// parameter setting.
RAM64X1D#(.INIT(64'h0000000000000000))RAM64X1D_instance_3413(.WCLK(CLK), .WE(wea_0), 
  .A0(ADDRA[0]), .A1(ADDRA[1]), .A2(ADDRA[2]), .A3(ADDRA[3]), .A4(ADDRA[4]), .A5(ADDRA[5]), 
  .D(DINA[21]), .SPO(pre_douta_0[21]), .DPRA0(ADDRB[0]), .DPRA1(ADDRB[1]), .DPRA2(ADDRB[2]), 
  .DPRA3(ADDRB[3]), .DPRA4(ADDRB[4]), .DPRA5(ADDRB[5]), .DPO(pre_doutb_0[21]));
// Memory array element: COL: 0, ROW: 22
//  Initialization of Dual Port LUT ram now done through explicit
// parameter setting.
RAM64X1D#(.INIT(64'h0000000000000000))RAM64X1D_instance_3414(.WCLK(CLK), .WE(wea_0), 
  .A0(ADDRA[0]), .A1(ADDRA[1]), .A2(ADDRA[2]), .A3(ADDRA[3]), .A4(ADDRA[4]), .A5(ADDRA[5]), 
  .D(DINA[22]), .SPO(pre_douta_0[22]), .DPRA0(ADDRB[0]), .DPRA1(ADDRB[1]), .DPRA2(ADDRB[2]), 
  .DPRA3(ADDRB[3]), .DPRA4(ADDRB[4]), .DPRA5(ADDRB[5]), .DPO(pre_doutb_0[22]));
// Memory array element: COL: 0, ROW: 23
//  Initialization of Dual Port LUT ram now done through explicit
// parameter setting.
RAM64X1D#(.INIT(64'h0000000000000000))RAM64X1D_instance_3415(.WCLK(CLK), .WE(wea_0), 
  .A0(ADDRA[0]), .A1(ADDRA[1]), .A2(ADDRA[2]), .A3(ADDRA[3]), .A4(ADDRA[4]), .A5(ADDRA[5]), 
  .D(DINA[23]), .SPO(pre_douta_0[23]), .DPRA0(ADDRB[0]), .DPRA1(ADDRB[1]), .DPRA2(ADDRB[2]), 
  .DPRA3(ADDRB[3]), .DPRA4(ADDRB[4]), .DPRA5(ADDRB[5]), .DPO(pre_doutb_0[23]));
// Memory array element: COL: 0, ROW: 24
//  Initialization of Dual Port LUT ram now done through explicit
// parameter setting.
RAM64X1D#(.INIT(64'h0000000000000000))RAM64X1D_instance_3416(.WCLK(CLK), .WE(wea_0), 
  .A0(ADDRA[0]), .A1(ADDRA[1]), .A2(ADDRA[2]), .A3(ADDRA[3]), .A4(ADDRA[4]), .A5(ADDRA[5]), 
  .D(DINA[24]), .SPO(pre_douta_0[24]), .DPRA0(ADDRB[0]), .DPRA1(ADDRB[1]), .DPRA2(ADDRB[2]), 
  .DPRA3(ADDRB[3]), .DPRA4(ADDRB[4]), .DPRA5(ADDRB[5]), .DPO(pre_doutb_0[24]));
// Memory array element: COL: 0, ROW: 25
//  Initialization of Dual Port LUT ram now done through explicit
// parameter setting.
RAM64X1D#(.INIT(64'h0000000000000000))RAM64X1D_instance_3417(.WCLK(CLK), .WE(wea_0), 
  .A0(ADDRA[0]), .A1(ADDRA[1]), .A2(ADDRA[2]), .A3(ADDRA[3]), .A4(ADDRA[4]), .A5(ADDRA[5]), 
  .D(DINA[25]), .SPO(pre_douta_0[25]), .DPRA0(ADDRB[0]), .DPRA1(ADDRB[1]), .DPRA2(ADDRB[2]), 
  .DPRA3(ADDRB[3]), .DPRA4(ADDRB[4]), .DPRA5(ADDRB[5]), .DPO(pre_doutb_0[25]));
// Memory array element: COL: 0, ROW: 26
//  Initialization of Dual Port LUT ram now done through explicit
// parameter setting.
RAM64X1D#(.INIT(64'h0000000000000000))RAM64X1D_instance_3418(.WCLK(CLK), .WE(wea_0), 
  .A0(ADDRA[0]), .A1(ADDRA[1]), .A2(ADDRA[2]), .A3(ADDRA[3]), .A4(ADDRA[4]), .A5(ADDRA[5]), 
  .D(DINA[26]), .SPO(pre_douta_0[26]), .DPRA0(ADDRB[0]), .DPRA1(ADDRB[1]), .DPRA2(ADDRB[2]), 
  .DPRA3(ADDRB[3]), .DPRA4(ADDRB[4]), .DPRA5(ADDRB[5]), .DPO(pre_doutb_0[26]));
// Memory array element: COL: 0, ROW: 27
//  Initialization of Dual Port LUT ram now done through explicit
// parameter setting.
RAM64X1D#(.INIT(64'h0000000000000000))RAM64X1D_instance_3419(.WCLK(CLK), .WE(wea_0), 
  .A0(ADDRA[0]), .A1(ADDRA[1]), .A2(ADDRA[2]), .A3(ADDRA[3]), .A4(ADDRA[4]), .A5(ADDRA[5]), 
  .D(DINA[27]), .SPO(pre_douta_0[27]), .DPRA0(ADDRB[0]), .DPRA1(ADDRB[1]), .DPRA2(ADDRB[2]), 
  .DPRA3(ADDRB[3]), .DPRA4(ADDRB[4]), .DPRA5(ADDRB[5]), .DPO(pre_doutb_0[27]));
// Memory array element: COL: 0, ROW: 28
//  Initialization of Dual Port LUT ram now done through explicit
// parameter setting.
RAM64X1D#(.INIT(64'h0000000000000000))RAM64X1D_instance_3420(.WCLK(CLK), .WE(wea_0), 
  .A0(ADDRA[0]), .A1(ADDRA[1]), .A2(ADDRA[2]), .A3(ADDRA[3]), .A4(ADDRA[4]), .A5(ADDRA[5]), 
  .D(DINA[28]), .SPO(pre_douta_0[28]), .DPRA0(ADDRB[0]), .DPRA1(ADDRB[1]), .DPRA2(ADDRB[2]), 
  .DPRA3(ADDRB[3]), .DPRA4(ADDRB[4]), .DPRA5(ADDRB[5]), .DPO(pre_doutb_0[28]));
// Memory array element: COL: 0, ROW: 29
//  Initialization of Dual Port LUT ram now done through explicit
// parameter setting.
RAM64X1D#(.INIT(64'h0000000000000000))RAM64X1D_instance_3421(.WCLK(CLK), .WE(wea_0), 
  .A0(ADDRA[0]), .A1(ADDRA[1]), .A2(ADDRA[2]), .A3(ADDRA[3]), .A4(ADDRA[4]), .A5(ADDRA[5]), 
  .D(DINA[29]), .SPO(pre_douta_0[29]), .DPRA0(ADDRB[0]), .DPRA1(ADDRB[1]), .DPRA2(ADDRB[2]), 
  .DPRA3(ADDRB[3]), .DPRA4(ADDRB[4]), .DPRA5(ADDRB[5]), .DPO(pre_doutb_0[29]));
// Memory array element: COL: 0, ROW: 30
//  Initialization of Dual Port LUT ram now done through explicit
// parameter setting.
RAM64X1D#(.INIT(64'h0000000000000000))RAM64X1D_instance_3422(.WCLK(CLK), .WE(wea_0), 
  .A0(ADDRA[0]), .A1(ADDRA[1]), .A2(ADDRA[2]), .A3(ADDRA[3]), .A4(ADDRA[4]), .A5(ADDRA[5]), 
  .D(DINA[30]), .SPO(pre_douta_0[30]), .DPRA0(ADDRB[0]), .DPRA1(ADDRB[1]), .DPRA2(ADDRB[2]), 
  .DPRA3(ADDRB[3]), .DPRA4(ADDRB[4]), .DPRA5(ADDRB[5]), .DPO(pre_doutb_0[30]));
// Memory array element: COL: 0, ROW: 31
//  Initialization of Dual Port LUT ram now done through explicit
// parameter setting.
RAM64X1D#(.INIT(64'h0000000000000000))RAM64X1D_instance_3423(.WCLK(CLK), .WE(wea_0), 
  .A0(ADDRA[0]), .A1(ADDRA[1]), .A2(ADDRA[2]), .A3(ADDRA[3]), .A4(ADDRA[4]), .A5(ADDRA[5]), 
  .D(DINA[31]), .SPO(pre_douta_0[31]), .DPRA0(ADDRB[0]), .DPRA1(ADDRB[1]), .DPRA2(ADDRB[2]), 
  .DPRA3(ADDRB[3]), .DPRA4(ADDRB[4]), .DPRA5(ADDRB[5]), .DPO(pre_doutb_0[31]));
// Memory array element: COL: 1, ROW: 0
//  Initialization of Dual Port LUT ram now done through explicit
// parameter setting.
RAM64X1D#(.INIT(64'h0000000000000000))RAM64X1D_instance_3424(.WCLK(CLK), .WE(wea_1), 
  .A0(ADDRA[0]), .A1(ADDRA[1]), .A2(ADDRA[2]), .A3(ADDRA[3]), .A4(ADDRA[4]), .A5(ADDRA[5]), 
  .D(DINA[0]), .SPO(pre_douta_1[0]), .DPRA0(ADDRB[0]), .DPRA1(ADDRB[1]), .DPRA2(ADDRB[2]), 
  .DPRA3(ADDRB[3]), .DPRA4(ADDRB[4]), .DPRA5(ADDRB[5]), .DPO(pre_doutb_1[0]));
// Memory array element: COL: 1, ROW: 1
//  Initialization of Dual Port LUT ram now done through explicit
// parameter setting.
RAM64X1D#(.INIT(64'h0000000000000000))RAM64X1D_instance_3425(.WCLK(CLK), .WE(wea_1), 
  .A0(ADDRA[0]), .A1(ADDRA[1]), .A2(ADDRA[2]), .A3(ADDRA[3]), .A4(ADDRA[4]), .A5(ADDRA[5]), 
  .D(DINA[1]), .SPO(pre_douta_1[1]), .DPRA0(ADDRB[0]), .DPRA1(ADDRB[1]), .DPRA2(ADDRB[2]), 
  .DPRA3(ADDRB[3]), .DPRA4(ADDRB[4]), .DPRA5(ADDRB[5]), .DPO(pre_doutb_1[1]));
// Memory array element: COL: 1, ROW: 2
//  Initialization of Dual Port LUT ram now done through explicit
// parameter setting.
RAM64X1D#(.INIT(64'h0000000000000000))RAM64X1D_instance_3426(.WCLK(CLK), .WE(wea_1), 
  .A0(ADDRA[0]), .A1(ADDRA[1]), .A2(ADDRA[2]), .A3(ADDRA[3]), .A4(ADDRA[4]), .A5(ADDRA[5]), 
  .D(DINA[2]), .SPO(pre_douta_1[2]), .DPRA0(ADDRB[0]), .DPRA1(ADDRB[1]), .DPRA2(ADDRB[2]), 
  .DPRA3(ADDRB[3]), .DPRA4(ADDRB[4]), .DPRA5(ADDRB[5]), .DPO(pre_doutb_1[2]));
// Memory array element: COL: 1, ROW: 3
//  Initialization of Dual Port LUT ram now done through explicit
// parameter setting.
RAM64X1D#(.INIT(64'h0000000000000000))RAM64X1D_instance_3427(.WCLK(CLK), .WE(wea_1), 
  .A0(ADDRA[0]), .A1(ADDRA[1]), .A2(ADDRA[2]), .A3(ADDRA[3]), .A4(ADDRA[4]), .A5(ADDRA[5]), 
  .D(DINA[3]), .SPO(pre_douta_1[3]), .DPRA0(ADDRB[0]), .DPRA1(ADDRB[1]), .DPRA2(ADDRB[2]), 
  .DPRA3(ADDRB[3]), .DPRA4(ADDRB[4]), .DPRA5(ADDRB[5]), .DPO(pre_doutb_1[3]));
// Memory array element: COL: 1, ROW: 4
//  Initialization of Dual Port LUT ram now done through explicit
// parameter setting.
RAM64X1D#(.INIT(64'h0000000000000000))RAM64X1D_instance_3428(.WCLK(CLK), .WE(wea_1), 
  .A0(ADDRA[0]), .A1(ADDRA[1]), .A2(ADDRA[2]), .A3(ADDRA[3]), .A4(ADDRA[4]), .A5(ADDRA[5]), 
  .D(DINA[4]), .SPO(pre_douta_1[4]), .DPRA0(ADDRB[0]), .DPRA1(ADDRB[1]), .DPRA2(ADDRB[2]), 
  .DPRA3(ADDRB[3]), .DPRA4(ADDRB[4]), .DPRA5(ADDRB[5]), .DPO(pre_doutb_1[4]));
// Memory array element: COL: 1, ROW: 5
//  Initialization of Dual Port LUT ram now done through explicit
// parameter setting.
RAM64X1D#(.INIT(64'h0000000000000000))RAM64X1D_instance_3429(.WCLK(CLK), .WE(wea_1), 
  .A0(ADDRA[0]), .A1(ADDRA[1]), .A2(ADDRA[2]), .A3(ADDRA[3]), .A4(ADDRA[4]), .A5(ADDRA[5]), 
  .D(DINA[5]), .SPO(pre_douta_1[5]), .DPRA0(ADDRB[0]), .DPRA1(ADDRB[1]), .DPRA2(ADDRB[2]), 
  .DPRA3(ADDRB[3]), .DPRA4(ADDRB[4]), .DPRA5(ADDRB[5]), .DPO(pre_doutb_1[5]));
// Memory array element: COL: 1, ROW: 6
//  Initialization of Dual Port LUT ram now done through explicit
// parameter setting.
RAM64X1D#(.INIT(64'h0000000000000000))RAM64X1D_instance_3430(.WCLK(CLK), .WE(wea_1), 
  .A0(ADDRA[0]), .A1(ADDRA[1]), .A2(ADDRA[2]), .A3(ADDRA[3]), .A4(ADDRA[4]), .A5(ADDRA[5]), 
  .D(DINA[6]), .SPO(pre_douta_1[6]), .DPRA0(ADDRB[0]), .DPRA1(ADDRB[1]), .DPRA2(ADDRB[2]), 
  .DPRA3(ADDRB[3]), .DPRA4(ADDRB[4]), .DPRA5(ADDRB[5]), .DPO(pre_doutb_1[6]));
// Memory array element: COL: 1, ROW: 7
//  Initialization of Dual Port LUT ram now done through explicit
// parameter setting.
RAM64X1D#(.INIT(64'h0000000000000000))RAM64X1D_instance_3431(.WCLK(CLK), .WE(wea_1), 
  .A0(ADDRA[0]), .A1(ADDRA[1]), .A2(ADDRA[2]), .A3(ADDRA[3]), .A4(ADDRA[4]), .A5(ADDRA[5]), 
  .D(DINA[7]), .SPO(pre_douta_1[7]), .DPRA0(ADDRB[0]), .DPRA1(ADDRB[1]), .DPRA2(ADDRB[2]), 
  .DPRA3(ADDRB[3]), .DPRA4(ADDRB[4]), .DPRA5(ADDRB[5]), .DPO(pre_doutb_1[7]));
// Memory array element: COL: 1, ROW: 8
//  Initialization of Dual Port LUT ram now done through explicit
// parameter setting.
RAM64X1D#(.INIT(64'h0000000000000000))RAM64X1D_instance_3432(.WCLK(CLK), .WE(wea_1), 
  .A0(ADDRA[0]), .A1(ADDRA[1]), .A2(ADDRA[2]), .A3(ADDRA[3]), .A4(ADDRA[4]), .A5(ADDRA[5]), 
  .D(DINA[8]), .SPO(pre_douta_1[8]), .DPRA0(ADDRB[0]), .DPRA1(ADDRB[1]), .DPRA2(ADDRB[2]), 
  .DPRA3(ADDRB[3]), .DPRA4(ADDRB[4]), .DPRA5(ADDRB[5]), .DPO(pre_doutb_1[8]));
// Memory array element: COL: 1, ROW: 9
//  Initialization of Dual Port LUT ram now done through explicit
// parameter setting.
RAM64X1D#(.INIT(64'h0000000000000000))RAM64X1D_instance_3433(.WCLK(CLK), .WE(wea_1), 
  .A0(ADDRA[0]), .A1(ADDRA[1]), .A2(ADDRA[2]), .A3(ADDRA[3]), .A4(ADDRA[4]), .A5(ADDRA[5]), 
  .D(DINA[9]), .SPO(pre_douta_1[9]), .DPRA0(ADDRB[0]), .DPRA1(ADDRB[1]), .DPRA2(ADDRB[2]), 
  .DPRA3(ADDRB[3]), .DPRA4(ADDRB[4]), .DPRA5(ADDRB[5]), .DPO(pre_doutb_1[9]));
// Memory array element: COL: 1, ROW: 10
//  Initialization of Dual Port LUT ram now done through explicit
// parameter setting.
RAM64X1D#(.INIT(64'h0000000000000000))RAM64X1D_instance_3434(.WCLK(CLK), .WE(wea_1), 
  .A0(ADDRA[0]), .A1(ADDRA[1]), .A2(ADDRA[2]), .A3(ADDRA[3]), .A4(ADDRA[4]), .A5(ADDRA[5]), 
  .D(DINA[10]), .SPO(pre_douta_1[10]), .DPRA0(ADDRB[0]), .DPRA1(ADDRB[1]), .DPRA2(ADDRB[2]), 
  .DPRA3(ADDRB[3]), .DPRA4(ADDRB[4]), .DPRA5(ADDRB[5]), .DPO(pre_doutb_1[10]));
// Memory array element: COL: 1, ROW: 11
//  Initialization of Dual Port LUT ram now done through explicit
// parameter setting.
RAM64X1D#(.INIT(64'h0000000000000000))RAM64X1D_instance_3435(.WCLK(CLK), .WE(wea_1), 
  .A0(ADDRA[0]), .A1(ADDRA[1]), .A2(ADDRA[2]), .A3(ADDRA[3]), .A4(ADDRA[4]), .A5(ADDRA[5]), 
  .D(DINA[11]), .SPO(pre_douta_1[11]), .DPRA0(ADDRB[0]), .DPRA1(ADDRB[1]), .DPRA2(ADDRB[2]), 
  .DPRA3(ADDRB[3]), .DPRA4(ADDRB[4]), .DPRA5(ADDRB[5]), .DPO(pre_doutb_1[11]));
// Memory array element: COL: 1, ROW: 12
//  Initialization of Dual Port LUT ram now done through explicit
// parameter setting.
RAM64X1D#(.INIT(64'h0000000000000000))RAM64X1D_instance_3436(.WCLK(CLK), .WE(wea_1), 
  .A0(ADDRA[0]), .A1(ADDRA[1]), .A2(ADDRA[2]), .A3(ADDRA[3]), .A4(ADDRA[4]), .A5(ADDRA[5]), 
  .D(DINA[12]), .SPO(pre_douta_1[12]), .DPRA0(ADDRB[0]), .DPRA1(ADDRB[1]), .DPRA2(ADDRB[2]), 
  .DPRA3(ADDRB[3]), .DPRA4(ADDRB[4]), .DPRA5(ADDRB[5]), .DPO(pre_doutb_1[12]));
// Memory array element: COL: 1, ROW: 13
//  Initialization of Dual Port LUT ram now done through explicit
// parameter setting.
RAM64X1D#(.INIT(64'h0000000000000000))RAM64X1D_instance_3437(.WCLK(CLK), .WE(wea_1), 
  .A0(ADDRA[0]), .A1(ADDRA[1]), .A2(ADDRA[2]), .A3(ADDRA[3]), .A4(ADDRA[4]), .A5(ADDRA[5]), 
  .D(DINA[13]), .SPO(pre_douta_1[13]), .DPRA0(ADDRB[0]), .DPRA1(ADDRB[1]), .DPRA2(ADDRB[2]), 
  .DPRA3(ADDRB[3]), .DPRA4(ADDRB[4]), .DPRA5(ADDRB[5]), .DPO(pre_doutb_1[13]));
// Memory array element: COL: 1, ROW: 14
//  Initialization of Dual Port LUT ram now done through explicit
// parameter setting.
RAM64X1D#(.INIT(64'h0000000000000000))RAM64X1D_instance_3438(.WCLK(CLK), .WE(wea_1), 
  .A0(ADDRA[0]), .A1(ADDRA[1]), .A2(ADDRA[2]), .A3(ADDRA[3]), .A4(ADDRA[4]), .A5(ADDRA[5]), 
  .D(DINA[14]), .SPO(pre_douta_1[14]), .DPRA0(ADDRB[0]), .DPRA1(ADDRB[1]), .DPRA2(ADDRB[2]), 
  .DPRA3(ADDRB[3]), .DPRA4(ADDRB[4]), .DPRA5(ADDRB[5]), .DPO(pre_doutb_1[14]));
// Memory array element: COL: 1, ROW: 15
//  Initialization of Dual Port LUT ram now done through explicit
// parameter setting.
RAM64X1D#(.INIT(64'h0000000000000000))RAM64X1D_instance_3439(.WCLK(CLK), .WE(wea_1), 
  .A0(ADDRA[0]), .A1(ADDRA[1]), .A2(ADDRA[2]), .A3(ADDRA[3]), .A4(ADDRA[4]), .A5(ADDRA[5]), 
  .D(DINA[15]), .SPO(pre_douta_1[15]), .DPRA0(ADDRB[0]), .DPRA1(ADDRB[1]), .DPRA2(ADDRB[2]), 
  .DPRA3(ADDRB[3]), .DPRA4(ADDRB[4]), .DPRA5(ADDRB[5]), .DPO(pre_doutb_1[15]));
// Memory array element: COL: 1, ROW: 16
//  Initialization of Dual Port LUT ram now done through explicit
// parameter setting.
RAM64X1D#(.INIT(64'h0000000000000000))RAM64X1D_instance_3440(.WCLK(CLK), .WE(wea_1), 
  .A0(ADDRA[0]), .A1(ADDRA[1]), .A2(ADDRA[2]), .A3(ADDRA[3]), .A4(ADDRA[4]), .A5(ADDRA[5]), 
  .D(DINA[16]), .SPO(pre_douta_1[16]), .DPRA0(ADDRB[0]), .DPRA1(ADDRB[1]), .DPRA2(ADDRB[2]), 
  .DPRA3(ADDRB[3]), .DPRA4(ADDRB[4]), .DPRA5(ADDRB[5]), .DPO(pre_doutb_1[16]));
// Memory array element: COL: 1, ROW: 17
//  Initialization of Dual Port LUT ram now done through explicit
// parameter setting.
RAM64X1D#(.INIT(64'h0000000000000000))RAM64X1D_instance_3441(.WCLK(CLK), .WE(wea_1), 
  .A0(ADDRA[0]), .A1(ADDRA[1]), .A2(ADDRA[2]), .A3(ADDRA[3]), .A4(ADDRA[4]), .A5(ADDRA[5]), 
  .D(DINA[17]), .SPO(pre_douta_1[17]), .DPRA0(ADDRB[0]), .DPRA1(ADDRB[1]), .DPRA2(ADDRB[2]), 
  .DPRA3(ADDRB[3]), .DPRA4(ADDRB[4]), .DPRA5(ADDRB[5]), .DPO(pre_doutb_1[17]));
// Memory array element: COL: 1, ROW: 18
//  Initialization of Dual Port LUT ram now done through explicit
// parameter setting.
RAM64X1D#(.INIT(64'h0000000000000000))RAM64X1D_instance_3442(.WCLK(CLK), .WE(wea_1), 
  .A0(ADDRA[0]), .A1(ADDRA[1]), .A2(ADDRA[2]), .A3(ADDRA[3]), .A4(ADDRA[4]), .A5(ADDRA[5]), 
  .D(DINA[18]), .SPO(pre_douta_1[18]), .DPRA0(ADDRB[0]), .DPRA1(ADDRB[1]), .DPRA2(ADDRB[2]), 
  .DPRA3(ADDRB[3]), .DPRA4(ADDRB[4]), .DPRA5(ADDRB[5]), .DPO(pre_doutb_1[18]));
// Memory array element: COL: 1, ROW: 19
//  Initialization of Dual Port LUT ram now done through explicit
// parameter setting.
RAM64X1D#(.INIT(64'h0000000000000000))RAM64X1D_instance_3443(.WCLK(CLK), .WE(wea_1), 
  .A0(ADDRA[0]), .A1(ADDRA[1]), .A2(ADDRA[2]), .A3(ADDRA[3]), .A4(ADDRA[4]), .A5(ADDRA[5]), 
  .D(DINA[19]), .SPO(pre_douta_1[19]), .DPRA0(ADDRB[0]), .DPRA1(ADDRB[1]), .DPRA2(ADDRB[2]), 
  .DPRA3(ADDRB[3]), .DPRA4(ADDRB[4]), .DPRA5(ADDRB[5]), .DPO(pre_doutb_1[19]));
// Memory array element: COL: 1, ROW: 20
//  Initialization of Dual Port LUT ram now done through explicit
// parameter setting.
RAM64X1D#(.INIT(64'h0000000000000000))RAM64X1D_instance_3444(.WCLK(CLK), .WE(wea_1), 
  .A0(ADDRA[0]), .A1(ADDRA[1]), .A2(ADDRA[2]), .A3(ADDRA[3]), .A4(ADDRA[4]), .A5(ADDRA[5]), 
  .D(DINA[20]), .SPO(pre_douta_1[20]), .DPRA0(ADDRB[0]), .DPRA1(ADDRB[1]), .DPRA2(ADDRB[2]), 
  .DPRA3(ADDRB[3]), .DPRA4(ADDRB[4]), .DPRA5(ADDRB[5]), .DPO(pre_doutb_1[20]));
// Memory array element: COL: 1, ROW: 21
//  Initialization of Dual Port LUT ram now done through explicit
// parameter setting.
RAM64X1D#(.INIT(64'h0000000000000000))RAM64X1D_instance_3445(.WCLK(CLK), .WE(wea_1), 
  .A0(ADDRA[0]), .A1(ADDRA[1]), .A2(ADDRA[2]), .A3(ADDRA[3]), .A4(ADDRA[4]), .A5(ADDRA[5]), 
  .D(DINA[21]), .SPO(pre_douta_1[21]), .DPRA0(ADDRB[0]), .DPRA1(ADDRB[1]), .DPRA2(ADDRB[2]), 
  .DPRA3(ADDRB[3]), .DPRA4(ADDRB[4]), .DPRA5(ADDRB[5]), .DPO(pre_doutb_1[21]));
// Memory array element: COL: 1, ROW: 22
//  Initialization of Dual Port LUT ram now done through explicit
// parameter setting.
RAM64X1D#(.INIT(64'h0000000000000000))RAM64X1D_instance_3446(.WCLK(CLK), .WE(wea_1), 
  .A0(ADDRA[0]), .A1(ADDRA[1]), .A2(ADDRA[2]), .A3(ADDRA[3]), .A4(ADDRA[4]), .A5(ADDRA[5]), 
  .D(DINA[22]), .SPO(pre_douta_1[22]), .DPRA0(ADDRB[0]), .DPRA1(ADDRB[1]), .DPRA2(ADDRB[2]), 
  .DPRA3(ADDRB[3]), .DPRA4(ADDRB[4]), .DPRA5(ADDRB[5]), .DPO(pre_doutb_1[22]));
// Memory array element: COL: 1, ROW: 23
//  Initialization of Dual Port LUT ram now done through explicit
// parameter setting.
RAM64X1D#(.INIT(64'h0000000000000000))RAM64X1D_instance_3447(.WCLK(CLK), .WE(wea_1), 
  .A0(ADDRA[0]), .A1(ADDRA[1]), .A2(ADDRA[2]), .A3(ADDRA[3]), .A4(ADDRA[4]), .A5(ADDRA[5]), 
  .D(DINA[23]), .SPO(pre_douta_1[23]), .DPRA0(ADDRB[0]), .DPRA1(ADDRB[1]), .DPRA2(ADDRB[2]), 
  .DPRA3(ADDRB[3]), .DPRA4(ADDRB[4]), .DPRA5(ADDRB[5]), .DPO(pre_doutb_1[23]));
// Memory array element: COL: 1, ROW: 24
//  Initialization of Dual Port LUT ram now done through explicit
// parameter setting.
RAM64X1D#(.INIT(64'h0000000000000000))RAM64X1D_instance_3448(.WCLK(CLK), .WE(wea_1), 
  .A0(ADDRA[0]), .A1(ADDRA[1]), .A2(ADDRA[2]), .A3(ADDRA[3]), .A4(ADDRA[4]), .A5(ADDRA[5]), 
  .D(DINA[24]), .SPO(pre_douta_1[24]), .DPRA0(ADDRB[0]), .DPRA1(ADDRB[1]), .DPRA2(ADDRB[2]), 
  .DPRA3(ADDRB[3]), .DPRA4(ADDRB[4]), .DPRA5(ADDRB[5]), .DPO(pre_doutb_1[24]));
// Memory array element: COL: 1, ROW: 25
//  Initialization of Dual Port LUT ram now done through explicit
// parameter setting.
RAM64X1D#(.INIT(64'h0000000000000000))RAM64X1D_instance_3449(.WCLK(CLK), .WE(wea_1), 
  .A0(ADDRA[0]), .A1(ADDRA[1]), .A2(ADDRA[2]), .A3(ADDRA[3]), .A4(ADDRA[4]), .A5(ADDRA[5]), 
  .D(DINA[25]), .SPO(pre_douta_1[25]), .DPRA0(ADDRB[0]), .DPRA1(ADDRB[1]), .DPRA2(ADDRB[2]), 
  .DPRA3(ADDRB[3]), .DPRA4(ADDRB[4]), .DPRA5(ADDRB[5]), .DPO(pre_doutb_1[25]));
// Memory array element: COL: 1, ROW: 26
//  Initialization of Dual Port LUT ram now done through explicit
// parameter setting.
RAM64X1D#(.INIT(64'h0000000000000000))RAM64X1D_instance_3450(.WCLK(CLK), .WE(wea_1), 
  .A0(ADDRA[0]), .A1(ADDRA[1]), .A2(ADDRA[2]), .A3(ADDRA[3]), .A4(ADDRA[4]), .A5(ADDRA[5]), 
  .D(DINA[26]), .SPO(pre_douta_1[26]), .DPRA0(ADDRB[0]), .DPRA1(ADDRB[1]), .DPRA2(ADDRB[2]), 
  .DPRA3(ADDRB[3]), .DPRA4(ADDRB[4]), .DPRA5(ADDRB[5]), .DPO(pre_doutb_1[26]));
// Memory array element: COL: 1, ROW: 27
//  Initialization of Dual Port LUT ram now done through explicit
// parameter setting.
RAM64X1D#(.INIT(64'h0000000000000000))RAM64X1D_instance_3451(.WCLK(CLK), .WE(wea_1), 
  .A0(ADDRA[0]), .A1(ADDRA[1]), .A2(ADDRA[2]), .A3(ADDRA[3]), .A4(ADDRA[4]), .A5(ADDRA[5]), 
  .D(DINA[27]), .SPO(pre_douta_1[27]), .DPRA0(ADDRB[0]), .DPRA1(ADDRB[1]), .DPRA2(ADDRB[2]), 
  .DPRA3(ADDRB[3]), .DPRA4(ADDRB[4]), .DPRA5(ADDRB[5]), .DPO(pre_doutb_1[27]));
// Memory array element: COL: 1, ROW: 28
//  Initialization of Dual Port LUT ram now done through explicit
// parameter setting.
RAM64X1D#(.INIT(64'h0000000000000000))RAM64X1D_instance_3452(.WCLK(CLK), .WE(wea_1), 
  .A0(ADDRA[0]), .A1(ADDRA[1]), .A2(ADDRA[2]), .A3(ADDRA[3]), .A4(ADDRA[4]), .A5(ADDRA[5]), 
  .D(DINA[28]), .SPO(pre_douta_1[28]), .DPRA0(ADDRB[0]), .DPRA1(ADDRB[1]), .DPRA2(ADDRB[2]), 
  .DPRA3(ADDRB[3]), .DPRA4(ADDRB[4]), .DPRA5(ADDRB[5]), .DPO(pre_doutb_1[28]));
// Memory array element: COL: 1, ROW: 29
//  Initialization of Dual Port LUT ram now done through explicit
// parameter setting.
RAM64X1D#(.INIT(64'h0000000000000000))RAM64X1D_instance_3453(.WCLK(CLK), .WE(wea_1), 
  .A0(ADDRA[0]), .A1(ADDRA[1]), .A2(ADDRA[2]), .A3(ADDRA[3]), .A4(ADDRA[4]), .A5(ADDRA[5]), 
  .D(DINA[29]), .SPO(pre_douta_1[29]), .DPRA0(ADDRB[0]), .DPRA1(ADDRB[1]), .DPRA2(ADDRB[2]), 
  .DPRA3(ADDRB[3]), .DPRA4(ADDRB[4]), .DPRA5(ADDRB[5]), .DPO(pre_doutb_1[29]));
// Memory array element: COL: 1, ROW: 30
//  Initialization of Dual Port LUT ram now done through explicit
// parameter setting.
RAM64X1D#(.INIT(64'h0000000000000000))RAM64X1D_instance_3454(.WCLK(CLK), .WE(wea_1), 
  .A0(ADDRA[0]), .A1(ADDRA[1]), .A2(ADDRA[2]), .A3(ADDRA[3]), .A4(ADDRA[4]), .A5(ADDRA[5]), 
  .D(DINA[30]), .SPO(pre_douta_1[30]), .DPRA0(ADDRB[0]), .DPRA1(ADDRB[1]), .DPRA2(ADDRB[2]), 
  .DPRA3(ADDRB[3]), .DPRA4(ADDRB[4]), .DPRA5(ADDRB[5]), .DPO(pre_doutb_1[30]));
// Memory array element: COL: 1, ROW: 31
//  Initialization of Dual Port LUT ram now done through explicit
// parameter setting.
RAM64X1D#(.INIT(64'h0000000000000000))RAM64X1D_instance_3455(.WCLK(CLK), .WE(wea_1), 
  .A0(ADDRA[0]), .A1(ADDRA[1]), .A2(ADDRA[2]), .A3(ADDRA[3]), .A4(ADDRA[4]), .A5(ADDRA[5]), 
  .D(DINA[31]), .SPO(pre_douta_1[31]), .DPRA0(ADDRB[0]), .DPRA1(ADDRB[1]), .DPRA2(ADDRB[2]), 
  .DPRA3(ADDRB[3]), .DPRA4(ADDRB[4]), .DPRA5(ADDRB[5]), .DPO(pre_doutb_1[31]));
endmodule



module medianRow9_structuralmemory_4279926b_(CLK_u99, bus_232a4242_, bus_4e922c35_, bus_5c215bc8_, bus_1696dbcb_, bus_69f1a679_, bus_5387a8fe_, bus_0004c004_, bus_1db0b09e_, bus_07e334cd_, bus_54d3b841_, bus_1ef60b57_, bus_22edb834_, bus_2e403d2a_);
input		CLK_u99;
input		bus_232a4242_;
input	[31:0]	bus_4e922c35_;
input	[2:0]	bus_5c215bc8_;
input		bus_1696dbcb_;
input	[31:0]	bus_69f1a679_;
input	[2:0]	bus_5387a8fe_;
input		bus_0004c004_;
input		bus_1db0b09e_;
input	[31:0]	bus_07e334cd_;
output	[31:0]	bus_54d3b841_;
output		bus_1ef60b57_;
output	[31:0]	bus_22edb834_;
output		bus_2e403d2a_;
wire		or_6fea3482_u0;
reg		logicalMem_307b6f2_we_delay0_u0=1'h0;
wire		not_6e0c7b3c_u0;
wire		or_515d7d28_u0;
wire	[31:0]	bus_0ce30e90_;
wire	[31:0]	bus_640fbbef_;
wire		and_5ca3913e_u0;
assign or_6fea3482_u0=bus_0004c004_|bus_1db0b09e_;
always @(posedge CLK_u99 or posedge bus_232a4242_)
begin
if (bus_232a4242_)
logicalMem_307b6f2_we_delay0_u0<=1'h0;
else logicalMem_307b6f2_we_delay0_u0<=bus_1db0b09e_;
end
assign not_6e0c7b3c_u0=~bus_1db0b09e_;
assign or_515d7d28_u0=and_5ca3913e_u0|logicalMem_307b6f2_we_delay0_u0;
medianRow9_forge_memory_101x32_159 medianRow9_forge_memory_101x32_159_instance0(.CLK(CLK_u99), 
  .ENA(or_6fea3482_u0), .WEA(bus_1db0b09e_), .DINA(bus_07e334cd_), .ADDRA(bus_69f1a679_), 
  .DOUTA(bus_640fbbef_), .DONEA(), .ENB(bus_1696dbcb_), .ADDRB(bus_4e922c35_), .DOUTB(bus_0ce30e90_), 
  .DONEB());
assign and_5ca3913e_u0=bus_0004c004_&not_6e0c7b3c_u0;
assign bus_54d3b841_=bus_0ce30e90_;
assign bus_1ef60b57_=bus_1696dbcb_;
assign bus_22edb834_=bus_640fbbef_;
assign bus_2e403d2a_=or_515d7d28_u0;
endmodule



module medianRow9_compute_median(CLK, RESET, GO, port_257aeac3_, port_7bc1f42b_, port_122ec10d_, port_2c990e5f_, port_44a742d5_, RESULT, RESULT_u2389, RESULT_u2390, RESULT_u2391, RESULT_u2392, RESULT_u2393, RESULT_u2394, RESULT_u2395, RESULT_u2396, RESULT_u2397, RESULT_u2398, RESULT_u2399, RESULT_u2400, RESULT_u2401, RESULT_u2402, DONE);
input		CLK;
input		RESET;
input		GO;
input		port_257aeac3_;
input	[31:0]	port_7bc1f42b_;
input		port_122ec10d_;
input	[31:0]	port_2c990e5f_;
input		port_44a742d5_;
output		RESULT;
output	[31:0]	RESULT_u2389;
output		RESULT_u2390;
output	[31:0]	RESULT_u2391;
output	[2:0]	RESULT_u2392;
output		RESULT_u2393;
output	[31:0]	RESULT_u2394;
output	[2:0]	RESULT_u2395;
output		RESULT_u2396;
output	[31:0]	RESULT_u2397;
output	[31:0]	RESULT_u2398;
output	[2:0]	RESULT_u2399;
output	[15:0]	RESULT_u2400;
output	[7:0]	RESULT_u2401;
output		RESULT_u2402;
output		DONE;
wire	[15:0]	lessThan_b_unsigned;
wire	[15:0]	lessThan_a_unsigned;
wire		lessThan;
wire		andOp;
wire		not_u945_u0;
wire		and_u4077_u0;
wire		and_u4078_u0;
wire	[31:0]	add;
wire		and_u4079_u0;
wire	[31:0]	add_u750;
wire	[31:0]	add_u751;
wire		and_u4080_u0;
wire signed	[31:0]	greaterThan_a_signed;
wire		greaterThan;
wire signed	[31:0]	greaterThan_b_signed;
wire		not_u946_u0;
wire		and_u4081_u0;
wire		and_u4082_u0;
wire	[31:0]	add_u752;
wire		and_u4083_u0;
wire	[31:0]	add_u753;
wire	[31:0]	add_u754;
wire		and_u4084_u0;
wire	[31:0]	add_u755;
wire		and_u4085_u0;
reg		reg_64f9e0df_u0=1'h0;
wire		or_u1571_u0;
wire	[31:0]	add_u756;
wire	[31:0]	add_u757;
reg		reg_735f5a29_u0=1'h0;
wire		or_u1572_u0;
wire		and_u4086_u0;
reg	[31:0]	syncEnable_u1569=32'h0;
reg		reg_2f355b7f_u0=1'h0;
wire	[31:0]	mux_u2073;
wire	[31:0]	mux_u2074_u0;
wire		or_u1573_u0;
reg	[31:0]	syncEnable_u1570_u0=32'h0;
reg	[31:0]	syncEnable_u1571_u0=32'h0;
reg		block_GO_delayed_u112=1'h0;
reg	[31:0]	syncEnable_u1572_u0=32'h0;
reg	[31:0]	syncEnable_u1573_u0=32'h0;
reg	[31:0]	syncEnable_u1574_u0=32'h0;
reg		reg_3e30c2b0_u0=1'h0;
reg	[31:0]	syncEnable_u1575_u0=32'h0;
wire	[31:0]	mux_u2075_u0;
wire	[31:0]	mux_u2076_u0;
reg		reg_3e30c2b0_result_delayed_u0=1'h0;
wire		or_u1574_u0;
wire		and_u4087_u0;
reg		reg_3e30c2b0_result_delayed_result_delayed_u0=1'h0;
wire		mux_u2077_u0;
reg		reg_3e30c2b0_result_delayed_result_delayed_result_delayed_u0=1'h0;
reg		reg_57c74cf2_u0=1'h0;
wire		and_u4088_u0;
reg	[31:0]	syncEnable_u1576_u0=32'h0;
reg		syncEnable_u1577_u0=1'h0;
wire	[31:0]	add_u758;
reg		syncEnable_u1578_u0=1'h0;
wire	[31:0]	mux_u2078_u0;
wire		or_u1575_u0;
reg	[31:0]	syncEnable_u1579_u0=32'h0;
reg	[31:0]	syncEnable_u1580_u0=32'h0;
reg	[31:0]	syncEnable_u1581_u0=32'h0;
reg		block_GO_delayed_u113_u0=1'h0;
reg	[31:0]	syncEnable_u1582_u0=32'h0;
reg	[31:0]	syncEnable_u1583_u0=32'h0;
reg	[31:0]	syncEnable_u1584_u0=32'h0;
wire		or_u1576_u0;
wire	[31:0]	mux_u2079_u0;
wire		and_u4089_u0;
wire		lessThan_u111;
wire signed	[32:0]	lessThan_u111_b_signed;
wire signed	[32:0]	lessThan_u111_a_signed;
wire		not_u947_u0;
wire		and_u4090_u0;
wire		and_u4091_u0;
wire	[31:0]	mux_u2080_u0;
wire	[31:0]	mux_u2081_u0;
wire		or_u1577_u0;
wire	[31:0]	mux_u2082_u0;
wire	[31:0]	mux_u2083_u0;
wire		mux_u2084_u0;
wire	[31:0]	mux_u2085_u0;
wire	[15:0]	add_u759;
reg	[31:0]	latch_32ad558e_reg=32'h0;
wire	[31:0]	latch_32ad558e_out;
reg		latch_6cf9a3f0_reg=1'h0;
wire		latch_6cf9a3f0_out;
wire	[31:0]	latch_7a4f0f82_out;
reg	[31:0]	latch_7a4f0f82_reg=32'h0;
wire		bus_643393a6_;
wire		scoreboard_78e65568_resOr1;
wire		scoreboard_78e65568_and;
reg		scoreboard_78e65568_reg1=1'h0;
wire		scoreboard_78e65568_resOr0;
reg		scoreboard_78e65568_reg0=1'h0;
wire	[31:0]	latch_38d0f861_out;
reg	[31:0]	latch_38d0f861_reg=32'h0;
reg		reg_3bedda5b_u0=1'h0;
reg	[31:0]	latch_0d09fe73_reg=32'h0;
wire	[31:0]	latch_0d09fe73_out;
reg	[15:0]	syncEnable_u1585_u0=16'h0;
wire		and_u4092_u0;
wire		and_u4093_u0;
reg		fbReg_swapped_u53=1'h0;
wire	[31:0]	mux_u2086_u0;
reg		loopControl_u110=1'h0;
wire		or_u1578_u0;
wire	[15:0]	mux_u2087_u0;
reg	[31:0]	fbReg_tmp_row0_u53=32'h0;
reg		syncEnable_u1586_u0=1'h0;
reg	[31:0]	fbReg_tmp_row_u53=32'h0;
wire		mux_u2088_u0;
reg	[31:0]	fbReg_tmp_u53=32'h0;
wire	[31:0]	mux_u2089_u0;
wire	[31:0]	mux_u2090_u0;
wire	[31:0]	mux_u2091_u0;
reg	[15:0]	fbReg_idx_u53=16'h0;
reg	[31:0]	fbReg_tmp_row1_u53=32'h0;
wire		and_u4094_u0;
wire	[7:0]	simplePinWrite;
wire	[15:0]	simplePinWrite_u650;
wire		simplePinWrite_u651;
reg		reg_53115892_u0=1'h0;
reg		reg_53115892_result_delayed_u0=1'h0;
wire		or_u1579_u0;
wire	[31:0]	mux_u2092_u0;
assign lessThan_a_unsigned=mux_u2087_u0;
assign lessThan_b_unsigned=16'h65;
assign lessThan=lessThan_a_unsigned<lessThan_b_unsigned;
assign andOp=lessThan&mux_u2088_u0;
assign not_u945_u0=~andOp;
assign and_u4077_u0=or_u1578_u0&andOp;
assign and_u4078_u0=or_u1578_u0&not_u945_u0;
assign add=mux_u2081_u0+32'h0;
assign and_u4079_u0=and_u4089_u0&port_257aeac3_;
assign add_u750=mux_u2081_u0+32'h1;
assign add_u751=add_u750+32'h0;
assign and_u4080_u0=and_u4089_u0&port_44a742d5_;
assign greaterThan_a_signed=syncEnable_u1582_u0;
assign greaterThan_b_signed=syncEnable_u1579_u0;
assign greaterThan=greaterThan_a_signed>greaterThan_b_signed;
assign not_u946_u0=~greaterThan;
assign and_u4081_u0=block_GO_delayed_u113_u0&greaterThan;
assign and_u4082_u0=block_GO_delayed_u113_u0&not_u946_u0;
assign add_u752=syncEnable_u1580_u0+32'h0;
assign and_u4083_u0=and_u4087_u0&port_257aeac3_;
assign add_u753=syncEnable_u1580_u0+32'h1;
assign add_u754=add_u753+32'h0;
assign and_u4084_u0=and_u4087_u0&port_44a742d5_;
assign add_u755=syncEnable_u1580_u0+32'h0;
assign and_u4085_u0=reg_64f9e0df_u0&port_44a742d5_;
always @(posedge CLK or posedge block_GO_delayed_u112 or posedge or_u1571_u0)
begin
if (or_u1571_u0)
reg_64f9e0df_u0<=1'h0;
else if (block_GO_delayed_u112)
reg_64f9e0df_u0<=1'h1;
else reg_64f9e0df_u0<=reg_64f9e0df_u0;
end
assign or_u1571_u0=and_u4085_u0|RESET;
assign add_u756=syncEnable_u1580_u0+32'h1;
assign add_u757=add_u756+32'h0;
always @(posedge CLK or posedge reg_2f355b7f_u0 or posedge or_u1572_u0)
begin
if (or_u1572_u0)
reg_735f5a29_u0<=1'h0;
else if (reg_2f355b7f_u0)
reg_735f5a29_u0<=1'h1;
else reg_735f5a29_u0<=reg_735f5a29_u0;
end
assign or_u1572_u0=and_u4086_u0|RESET;
assign and_u4086_u0=reg_735f5a29_u0&port_44a742d5_;
always @(posedge CLK)
begin
if (and_u4087_u0)
syncEnable_u1569<=add_u757;
end
always @(posedge CLK or posedge RESET)
begin
if (RESET)
reg_2f355b7f_u0<=1'h0;
else reg_2f355b7f_u0<=block_GO_delayed_u112;
end
assign mux_u2073=(block_GO_delayed_u112)?syncEnable_u1570_u0:syncEnable_u1574_u0;
assign mux_u2074_u0=({32{block_GO_delayed_u112}}&syncEnable_u1573_u0)|({32{reg_2f355b7f_u0}}&syncEnable_u1569)|({32{and_u4087_u0}}&add_u754);
assign or_u1573_u0=block_GO_delayed_u112|reg_2f355b7f_u0;
always @(posedge CLK)
begin
if (and_u4087_u0)
syncEnable_u1570_u0<=port_2c990e5f_;
end
always @(posedge CLK)
begin
if (and_u4087_u0)
syncEnable_u1571_u0<=port_7bc1f42b_;
end
always @(posedge CLK or posedge RESET)
begin
if (RESET)
block_GO_delayed_u112<=1'h0;
else block_GO_delayed_u112<=and_u4087_u0;
end
always @(posedge CLK)
begin
if (and_u4087_u0)
syncEnable_u1572_u0<=port_2c990e5f_;
end
always @(posedge CLK)
begin
if (and_u4087_u0)
syncEnable_u1573_u0<=add_u755;
end
always @(posedge CLK)
begin
if (and_u4087_u0)
syncEnable_u1574_u0<=port_7bc1f42b_;
end
always @(posedge CLK or posedge RESET)
begin
if (RESET)
reg_3e30c2b0_u0<=1'h0;
else reg_3e30c2b0_u0<=and_u4087_u0;
end
always @(posedge CLK)
begin
if (block_GO_delayed_u113_u0)
syncEnable_u1575_u0<=syncEnable_u1584_u0;
end
assign mux_u2075_u0=(reg_57c74cf2_u0)?syncEnable_u1576_u0:syncEnable_u1572_u0;
assign mux_u2076_u0=(reg_57c74cf2_u0)?syncEnable_u1575_u0:syncEnable_u1571_u0;
always @(posedge CLK or posedge RESET)
begin
if (RESET)
reg_3e30c2b0_result_delayed_u0<=1'h0;
else reg_3e30c2b0_result_delayed_u0<=reg_3e30c2b0_u0;
end
assign or_u1574_u0=reg_57c74cf2_u0|reg_3e30c2b0_result_delayed_result_delayed_result_delayed_u0;
assign and_u4087_u0=and_u4081_u0&block_GO_delayed_u113_u0;
always @(posedge CLK or posedge RESET)
begin
if (RESET)
reg_3e30c2b0_result_delayed_result_delayed_u0<=1'h0;
else reg_3e30c2b0_result_delayed_result_delayed_u0<=reg_3e30c2b0_result_delayed_u0;
end
assign mux_u2077_u0=(reg_57c74cf2_u0)?syncEnable_u1577_u0:1'h1;
always @(posedge CLK or posedge RESET)
begin
if (RESET)
reg_3e30c2b0_result_delayed_result_delayed_result_delayed_u0<=1'h0;
else reg_3e30c2b0_result_delayed_result_delayed_result_delayed_u0<=reg_3e30c2b0_result_delayed_result_delayed_u0;
end
always @(posedge CLK or posedge RESET)
begin
if (RESET)
reg_57c74cf2_u0<=1'h0;
else reg_57c74cf2_u0<=and_u4088_u0;
end
assign and_u4088_u0=and_u4082_u0&block_GO_delayed_u113_u0;
always @(posedge CLK)
begin
if (block_GO_delayed_u113_u0)
syncEnable_u1576_u0<=syncEnable_u1581_u0;
end
always @(posedge CLK)
begin
if (block_GO_delayed_u113_u0)
syncEnable_u1577_u0<=syncEnable_u1578_u0;
end
assign add_u758=mux_u2081_u0+32'h1;
always @(posedge CLK)
begin
if (and_u4089_u0)
syncEnable_u1578_u0<=mux_u2084_u0;
end
assign mux_u2078_u0=({32{or_u1573_u0}}&mux_u2074_u0)|({32{and_u4089_u0}}&add_u751)|({32{and_u4087_u0}}&mux_u2074_u0);
assign or_u1575_u0=and_u4089_u0|and_u4087_u0;
always @(posedge CLK)
begin
if (and_u4089_u0)
syncEnable_u1579_u0<=port_2c990e5f_;
end
always @(posedge CLK)
begin
if (and_u4089_u0)
syncEnable_u1580_u0<=mux_u2081_u0;
end
always @(posedge CLK)
begin
if (and_u4089_u0)
syncEnable_u1581_u0<=mux_u2080_u0;
end
always @(posedge CLK or posedge RESET)
begin
if (RESET)
block_GO_delayed_u113_u0<=1'h0;
else block_GO_delayed_u113_u0<=and_u4089_u0;
end
always @(posedge CLK)
begin
if (and_u4089_u0)
syncEnable_u1582_u0<=port_7bc1f42b_;
end
always @(posedge CLK)
begin
if (and_u4089_u0)
syncEnable_u1583_u0<=add_u758;
end
always @(posedge CLK)
begin
if (and_u4089_u0)
syncEnable_u1584_u0<=mux_u2083_u0;
end
assign or_u1576_u0=and_u4089_u0|and_u4087_u0;
assign mux_u2079_u0=(and_u4089_u0)?add:add_u752;
assign and_u4089_u0=and_u4090_u0&or_u1577_u0;
assign lessThan_u111_a_signed={1'b0, mux_u2081_u0};
assign lessThan_u111_b_signed=33'h64;
assign lessThan_u111=lessThan_u111_a_signed<lessThan_u111_b_signed;
assign not_u947_u0=~lessThan_u111;
assign and_u4090_u0=or_u1577_u0&lessThan_u111;
assign and_u4091_u0=or_u1577_u0&not_u947_u0;
assign mux_u2080_u0=(and_u4093_u0)?mux_u2091_u0:mux_u2075_u0;
assign mux_u2081_u0=(and_u4093_u0)?32'h0:syncEnable_u1583_u0;
assign or_u1577_u0=and_u4093_u0|or_u1574_u0;
assign mux_u2082_u0=(and_u4093_u0)?mux_u2086_u0:syncEnable_u1582_u0;
assign mux_u2083_u0=(and_u4093_u0)?mux_u2090_u0:mux_u2076_u0;
assign mux_u2084_u0=(and_u4093_u0)?1'h0:mux_u2077_u0;
assign mux_u2085_u0=(and_u4093_u0)?mux_u2089_u0:syncEnable_u1579_u0;
assign add_u759=mux_u2087_u0+16'h1;
always @(posedge CLK)
begin
if (or_u1574_u0)
latch_32ad558e_reg<=mux_u2075_u0;
end
assign latch_32ad558e_out=(or_u1574_u0)?mux_u2075_u0:latch_32ad558e_reg;
always @(posedge CLK)
begin
if (or_u1574_u0)
latch_6cf9a3f0_reg<=mux_u2077_u0;
end
assign latch_6cf9a3f0_out=(or_u1574_u0)?mux_u2077_u0:latch_6cf9a3f0_reg;
assign latch_7a4f0f82_out=(or_u1574_u0)?syncEnable_u1579_u0:latch_7a4f0f82_reg;
always @(posedge CLK)
begin
if (or_u1574_u0)
latch_7a4f0f82_reg<=syncEnable_u1579_u0;
end
assign bus_643393a6_=scoreboard_78e65568_and|RESET;
assign scoreboard_78e65568_resOr1=reg_3bedda5b_u0|scoreboard_78e65568_reg1;
assign scoreboard_78e65568_and=scoreboard_78e65568_resOr0&scoreboard_78e65568_resOr1;
always @(posedge CLK)
begin
if (bus_643393a6_)
scoreboard_78e65568_reg1<=1'h0;
else if (reg_3bedda5b_u0)
scoreboard_78e65568_reg1<=1'h1;
else scoreboard_78e65568_reg1<=scoreboard_78e65568_reg1;
end
assign scoreboard_78e65568_resOr0=or_u1574_u0|scoreboard_78e65568_reg0;
always @(posedge CLK)
begin
if (bus_643393a6_)
scoreboard_78e65568_reg0<=1'h0;
else if (or_u1574_u0)
scoreboard_78e65568_reg0<=1'h1;
else scoreboard_78e65568_reg0<=scoreboard_78e65568_reg0;
end
assign latch_38d0f861_out=(or_u1574_u0)?mux_u2076_u0:latch_38d0f861_reg;
always @(posedge CLK)
begin
if (or_u1574_u0)
latch_38d0f861_reg<=mux_u2076_u0;
end
always @(posedge CLK or posedge RESET)
begin
if (RESET)
reg_3bedda5b_u0<=1'h0;
else reg_3bedda5b_u0<=or_u1574_u0;
end
always @(posedge CLK)
begin
if (or_u1574_u0)
latch_0d09fe73_reg<=syncEnable_u1582_u0;
end
assign latch_0d09fe73_out=(or_u1574_u0)?syncEnable_u1582_u0:latch_0d09fe73_reg;
always @(posedge CLK)
begin
if (and_u4093_u0)
syncEnable_u1585_u0<=add_u759;
end
assign and_u4092_u0=and_u4078_u0&or_u1578_u0;
assign and_u4093_u0=and_u4077_u0&or_u1578_u0;
always @(posedge CLK)
begin
if (scoreboard_78e65568_and)
fbReg_swapped_u53<=latch_6cf9a3f0_out;
end
assign mux_u2086_u0=(GO)?32'h0:fbReg_tmp_row_u53;
always @(posedge CLK or posedge syncEnable_u1586_u0)
begin
if (syncEnable_u1586_u0)
loopControl_u110<=1'h0;
else loopControl_u110<=scoreboard_78e65568_and;
end
assign or_u1578_u0=GO|loopControl_u110;
assign mux_u2087_u0=(GO)?16'h0:fbReg_idx_u53;
always @(posedge CLK)
begin
if (scoreboard_78e65568_and)
fbReg_tmp_row0_u53<=latch_7a4f0f82_out;
end
always @(posedge CLK)
begin
if (GO)
syncEnable_u1586_u0<=RESET;
end
always @(posedge CLK)
begin
if (scoreboard_78e65568_and)
fbReg_tmp_row_u53<=latch_0d09fe73_out;
end
assign mux_u2088_u0=(GO)?1'h0:fbReg_swapped_u53;
always @(posedge CLK)
begin
if (scoreboard_78e65568_and)
fbReg_tmp_u53<=latch_38d0f861_out;
end
assign mux_u2089_u0=(GO)?32'h0:fbReg_tmp_row0_u53;
assign mux_u2090_u0=(GO)?32'h0:fbReg_tmp_u53;
assign mux_u2091_u0=(GO)?32'h0:fbReg_tmp_row1_u53;
always @(posedge CLK)
begin
if (scoreboard_78e65568_and)
fbReg_idx_u53<=syncEnable_u1585_u0;
end
always @(posedge CLK)
begin
if (scoreboard_78e65568_and)
fbReg_tmp_row1_u53<=latch_32ad558e_out;
end
assign and_u4094_u0=reg_53115892_u0&port_257aeac3_;
assign simplePinWrite=port_7bc1f42b_[7:0];
assign simplePinWrite_u650=16'h1&{16{1'h1}};
assign simplePinWrite_u651=reg_53115892_u0&{1{reg_53115892_u0}};
always @(posedge CLK or posedge RESET)
begin
if (RESET)
reg_53115892_u0<=1'h0;
else reg_53115892_u0<=and_u4092_u0;
end
always @(posedge CLK or posedge RESET)
begin
if (RESET)
reg_53115892_result_delayed_u0<=1'h0;
else reg_53115892_result_delayed_u0<=reg_53115892_u0;
end
assign or_u1579_u0=or_u1576_u0|reg_53115892_u0;
assign mux_u2092_u0=(or_u1576_u0)?mux_u2079_u0:32'h32;
assign RESULT=GO;
assign RESULT_u2389=32'h0;
assign RESULT_u2390=or_u1579_u0;
assign RESULT_u2391=mux_u2092_u0;
assign RESULT_u2392=3'h1;
assign RESULT_u2393=or_u1575_u0;
assign RESULT_u2394=mux_u2078_u0;
assign RESULT_u2395=3'h1;
assign RESULT_u2396=or_u1573_u0;
assign RESULT_u2397=mux_u2078_u0;
assign RESULT_u2398=mux_u2073;
assign RESULT_u2399=3'h1;
assign RESULT_u2400=simplePinWrite_u650;
assign RESULT_u2401=simplePinWrite;
assign RESULT_u2402=simplePinWrite_u651;
assign DONE=reg_53115892_result_delayed_u0;
endmodule



module medianRow9_endianswapper_1e17aca1_(endianswapper_1e17aca1_in, endianswapper_1e17aca1_out);
input	[31:0]	endianswapper_1e17aca1_in;
output	[31:0]	endianswapper_1e17aca1_out;
assign endianswapper_1e17aca1_out=endianswapper_1e17aca1_in;
endmodule



module medianRow9_endianswapper_256b1902_(endianswapper_256b1902_in, endianswapper_256b1902_out);
input	[31:0]	endianswapper_256b1902_in;
output	[31:0]	endianswapper_256b1902_out;
assign endianswapper_256b1902_out=endianswapper_256b1902_in;
endmodule



module medianRow9_stateVar_i(bus_6751732d_, bus_4d770ef3_, bus_1073d54e_, bus_09229d89_, bus_6c4c219b_, bus_352f6283_, bus_6d3d38a5_);
input		bus_6751732d_;
input		bus_4d770ef3_;
input		bus_1073d54e_;
input	[31:0]	bus_09229d89_;
input		bus_6c4c219b_;
input	[31:0]	bus_352f6283_;
output	[31:0]	bus_6d3d38a5_;
reg	[31:0]	stateVar_i_u55=32'h0;
wire	[31:0]	mux_2a0be35e_u0;
wire	[31:0]	endianswapper_1e17aca1_out;
wire	[31:0]	endianswapper_256b1902_out;
wire		or_022002aa_u0;
always @(posedge bus_6751732d_ or posedge bus_4d770ef3_)
begin
if (bus_4d770ef3_)
stateVar_i_u55<=32'h0;
else if (or_022002aa_u0)
stateVar_i_u55<=endianswapper_256b1902_out;
end
assign mux_2a0be35e_u0=(bus_1073d54e_)?bus_09229d89_:32'h0;
medianRow9_endianswapper_1e17aca1_ medianRow9_endianswapper_1e17aca1__1(.endianswapper_1e17aca1_in(stateVar_i_u55), 
  .endianswapper_1e17aca1_out(endianswapper_1e17aca1_out));
medianRow9_endianswapper_256b1902_ medianRow9_endianswapper_256b1902__1(.endianswapper_256b1902_in(mux_2a0be35e_u0), 
  .endianswapper_256b1902_out(endianswapper_256b1902_out));
assign or_022002aa_u0=bus_1073d54e_|bus_6c4c219b_;
assign bus_6d3d38a5_=endianswapper_1e17aca1_out;
endmodule



module medianRow9_scheduler(CLK, RESET, GO, port_09c60b28_, port_7870cd79_, port_51f84618_, port_7f70966f_, port_2d426ac7_, port_1b421d5a_, RESULT, RESULT_u2403, RESULT_u2404, RESULT_u2405, DONE);
input		CLK;
input		RESET;
input		GO;
input		port_09c60b28_;
input	[31:0]	port_7870cd79_;
input		port_51f84618_;
input		port_7f70966f_;
input		port_2d426ac7_;
input		port_1b421d5a_;
output		RESULT;
output		RESULT_u2403;
output		RESULT_u2404;
output		RESULT_u2405;
output		DONE;
wire		and_u4095_u0;
wire signed	[31:0]	lessThan_b_signed;
wire signed	[31:0]	lessThan_a_signed;
wire		lessThan;
wire signed	[31:0]	equals_a_signed;
wire		equals;
wire signed	[31:0]	equals_b_signed;
wire signed	[31:0]	equals_u248_a_signed;
wire signed	[31:0]	equals_u248_b_signed;
wire		equals_u248;
wire		not_u948_u0;
wire		and_u4096_u0;
wire		and_u4097_u0;
wire		andOp;
wire		and_u4098_u0;
wire		and_u4099_u0;
wire		not_u949_u0;
wire		simplePinWrite;
wire		and_u4100_u0;
wire		and_u4101_u0;
wire		equals_u249;
wire signed	[31:0]	equals_u249_a_signed;
wire signed	[31:0]	equals_u249_b_signed;
wire		not_u950_u0;
wire		and_u4102_u0;
wire		and_u4103_u0;
wire		andOp_u87;
wire		and_u4104_u0;
wire		not_u951_u0;
wire		and_u4105_u0;
wire		simplePinWrite_u652;
wire		not_u952_u0;
wire		and_u4106_u0;
wire		and_u4107_u0;
wire		and_u4108_u0;
wire		not_u953_u0;
wire		and_u4109_u0;
wire		simplePinWrite_u653;
wire		or_u1580_u0;
wire		and_u4110_u0;
wire		and_u4111_u0;
reg		and_delayed_u411=1'h0;
wire		and_u4112_u0;
wire		or_u1581_u0;
reg		reg_0655796e_u0=1'h0;
wire		and_u4113_u0;
wire		and_u4114_u0;
reg		and_delayed_u412_u0=1'h0;
wire		or_u1582_u0;
wire		and_u4115_u0;
wire		or_u1583_u0;
wire		mux_u2093;
wire		and_u4116_u0;
reg		and_delayed_u413_u0=1'h0;
wire		and_u4117_u0;
wire		or_u1584_u0;
wire		or_u1585_u0;
wire		mux_u2094_u0;
wire		receive_go_merge;
reg		syncEnable_u1587=1'h0;
wire		or_u1586_u0;
reg		loopControl_u111=1'h0;
reg		reg_3ed1fa81_u0=1'h0;
wire		mux_u2095_u0;
wire		or_u1587_u0;
reg		reg_42625815_u0=1'h0;
assign and_u4095_u0=or_u1586_u0&or_u1586_u0;
assign lessThan_a_signed=port_7870cd79_;
assign lessThan_b_signed=32'h65;
assign lessThan=lessThan_a_signed<lessThan_b_signed;
assign equals_a_signed=port_7870cd79_;
assign equals_b_signed=32'h65;
assign equals=equals_a_signed==equals_b_signed;
assign equals_u248_a_signed={31'b0, port_09c60b28_};
assign equals_u248_b_signed=32'h0;
assign equals_u248=equals_u248_a_signed==equals_u248_b_signed;
assign not_u948_u0=~equals_u248;
assign and_u4096_u0=and_u4095_u0&equals_u248;
assign and_u4097_u0=and_u4095_u0&not_u948_u0;
assign andOp=lessThan&port_1b421d5a_;
assign and_u4098_u0=and_u4101_u0&not_u949_u0;
assign and_u4099_u0=and_u4101_u0&andOp;
assign not_u949_u0=~andOp;
assign simplePinWrite=and_u4100_u0&{1{and_u4100_u0}};
assign and_u4100_u0=and_u4099_u0&and_u4101_u0;
assign and_u4101_u0=and_u4096_u0&and_u4095_u0;
assign equals_u249_a_signed={31'b0, port_09c60b28_};
assign equals_u249_b_signed=32'h1;
assign equals_u249=equals_u249_a_signed==equals_u249_b_signed;
assign not_u950_u0=~equals_u249;
assign and_u4102_u0=and_u4095_u0&equals_u249;
assign and_u4103_u0=and_u4095_u0&not_u950_u0;
assign andOp_u87=lessThan&port_1b421d5a_;
assign and_u4104_u0=and_u4117_u0&andOp_u87;
assign not_u951_u0=~andOp_u87;
assign and_u4105_u0=and_u4117_u0&not_u951_u0;
assign simplePinWrite_u652=and_u4114_u0&{1{and_u4114_u0}};
assign not_u952_u0=~equals;
assign and_u4106_u0=and_u4115_u0&not_u952_u0;
assign and_u4107_u0=and_u4115_u0&equals;
assign and_u4108_u0=and_u4112_u0&not_u953_u0;
assign not_u953_u0=~port_7f70966f_;
assign and_u4109_u0=and_u4112_u0&port_7f70966f_;
assign simplePinWrite_u653=and_u4110_u0&{1{and_u4110_u0}};
assign or_u1580_u0=and_delayed_u411|port_51f84618_;
assign and_u4110_u0=and_u4109_u0&and_u4112_u0;
assign and_u4111_u0=and_u4108_u0&and_u4112_u0;
always @(posedge CLK or posedge RESET)
begin
if (RESET)
and_delayed_u411<=1'h0;
else and_delayed_u411<=and_u4111_u0;
end
assign and_u4112_u0=and_u4107_u0&and_u4115_u0;
assign or_u1581_u0=or_u1580_u0|reg_0655796e_u0;
always @(posedge CLK or posedge RESET)
begin
if (RESET)
reg_0655796e_u0<=1'h0;
else reg_0655796e_u0<=and_u4113_u0;
end
assign and_u4113_u0=and_u4106_u0&and_u4115_u0;
assign and_u4114_u0=and_u4104_u0&and_u4117_u0;
always @(posedge CLK or posedge RESET)
begin
if (RESET)
and_delayed_u412_u0<=1'h0;
else and_delayed_u412_u0<=and_u4114_u0;
end
assign or_u1582_u0=and_delayed_u412_u0|or_u1581_u0;
assign and_u4115_u0=and_u4105_u0&and_u4117_u0;
assign or_u1583_u0=and_u4114_u0|and_u4110_u0;
assign mux_u2093=(and_u4114_u0)?1'h1:1'h0;
assign and_u4116_u0=and_u4103_u0&and_u4095_u0;
always @(posedge CLK or posedge RESET)
begin
if (RESET)
and_delayed_u413_u0<=1'h0;
else and_delayed_u413_u0<=and_u4116_u0;
end
assign and_u4117_u0=and_u4102_u0&and_u4095_u0;
assign or_u1584_u0=or_u1582_u0|and_delayed_u413_u0;
assign or_u1585_u0=and_u4100_u0|or_u1583_u0;
assign mux_u2094_u0=(and_u4100_u0)?1'h1:mux_u2093;
assign receive_go_merge=simplePinWrite|simplePinWrite_u652;
always @(posedge CLK)
begin
if (reg_3ed1fa81_u0)
syncEnable_u1587<=RESET;
end
assign or_u1586_u0=loopControl_u111|reg_3ed1fa81_u0;
always @(posedge CLK or posedge syncEnable_u1587)
begin
if (syncEnable_u1587)
loopControl_u111<=1'h0;
else loopControl_u111<=or_u1584_u0;
end
always @(posedge CLK or posedge RESET)
begin
if (RESET)
reg_3ed1fa81_u0<=1'h0;
else reg_3ed1fa81_u0<=reg_42625815_u0;
end
assign mux_u2095_u0=(GO)?1'h0:mux_u2094_u0;
assign or_u1587_u0=GO|or_u1585_u0;
always @(posedge CLK or posedge RESET)
begin
if (RESET)
reg_42625815_u0<=1'h0;
else reg_42625815_u0<=GO;
end
assign RESULT=or_u1587_u0;
assign RESULT_u2403=mux_u2095_u0;
assign RESULT_u2404=receive_go_merge;
assign RESULT_u2405=simplePinWrite_u653;
assign DONE=1'h0;
endmodule



module medianRow9_Kicker_65(CLK, RESET, bus_0e437ee2_);
input		CLK;
input		RESET;
output		bus_0e437ee2_;
wire		bus_155b4e77_;
reg		kicker_2=1'h0;
wire		bus_2dc6de0c_;
wire		bus_48c63e76_;
reg		kicker_1=1'h0;
reg		kicker_res=1'h0;
wire		bus_1baeccb5_;
assign bus_0e437ee2_=kicker_res;
assign bus_155b4e77_=kicker_1&bus_2dc6de0c_&bus_1baeccb5_;
always @(posedge CLK)
begin
kicker_2<=bus_48c63e76_;
end
assign bus_2dc6de0c_=~RESET;
assign bus_48c63e76_=bus_2dc6de0c_&kicker_1;
always @(posedge CLK)
begin
kicker_1<=bus_2dc6de0c_;
end
always @(posedge CLK)
begin
kicker_res<=bus_155b4e77_;
end
assign bus_1baeccb5_=~kicker_2;
endmodule



module medianRow9_endianswapper_35f6b19d_(endianswapper_35f6b19d_in, endianswapper_35f6b19d_out);
input		endianswapper_35f6b19d_in;
output		endianswapper_35f6b19d_out;
assign endianswapper_35f6b19d_out=endianswapper_35f6b19d_in;
endmodule



module medianRow9_endianswapper_0d8a5331_(endianswapper_0d8a5331_in, endianswapper_0d8a5331_out);
input		endianswapper_0d8a5331_in;
output		endianswapper_0d8a5331_out;
assign endianswapper_0d8a5331_out=endianswapper_0d8a5331_in;
endmodule



module medianRow9_stateVar_fsmState_medianRow9(bus_676ea960_, bus_0cf2d95d_, bus_54b80ca4_, bus_5c2d8b47_, bus_23d907af_);
input		bus_676ea960_;
input		bus_0cf2d95d_;
input		bus_54b80ca4_;
input		bus_5c2d8b47_;
output		bus_23d907af_;
reg		stateVar_fsmState_medianRow9_u2=1'h0;
wire		endianswapper_35f6b19d_out;
wire		endianswapper_0d8a5331_out;
assign bus_23d907af_=endianswapper_0d8a5331_out;
always @(posedge bus_676ea960_ or posedge bus_0cf2d95d_)
begin
if (bus_0cf2d95d_)
stateVar_fsmState_medianRow9_u2<=1'h0;
else if (bus_54b80ca4_)
stateVar_fsmState_medianRow9_u2<=endianswapper_35f6b19d_out;
end
medianRow9_endianswapper_35f6b19d_ medianRow9_endianswapper_35f6b19d__1(.endianswapper_35f6b19d_in(bus_5c2d8b47_), 
  .endianswapper_35f6b19d_out(endianswapper_35f6b19d_out));
medianRow9_endianswapper_0d8a5331_ medianRow9_endianswapper_0d8a5331__1(.endianswapper_0d8a5331_in(stateVar_fsmState_medianRow9_u2), 
  .endianswapper_0d8a5331_out(endianswapper_0d8a5331_out));
endmodule



module medianRow9_simplememoryreferee_09491173_(bus_36ebb0ea_, bus_7cd53092_, bus_3f7eacae_, bus_00c9fb1b_, bus_57fb331f_, bus_76743299_, bus_522f8603_, bus_75536eaf_, bus_319970ff_, bus_100f7b95_, bus_40f85ec8_, bus_6f64520a_, bus_1a42dfe8_, bus_66285afd_);
input		bus_36ebb0ea_;
input		bus_7cd53092_;
input		bus_3f7eacae_;
input	[31:0]	bus_00c9fb1b_;
input		bus_57fb331f_;
input	[31:0]	bus_76743299_;
input	[2:0]	bus_522f8603_;
output	[31:0]	bus_75536eaf_;
output	[31:0]	bus_319970ff_;
output		bus_100f7b95_;
output		bus_40f85ec8_;
output	[2:0]	bus_6f64520a_;
output	[31:0]	bus_1a42dfe8_;
output		bus_66285afd_;
assign bus_75536eaf_=32'h0;
assign bus_319970ff_=bus_76743299_;
assign bus_100f7b95_=1'h0;
assign bus_40f85ec8_=bus_57fb331f_;
assign bus_6f64520a_=3'h1;
assign bus_1a42dfe8_=bus_00c9fb1b_;
assign bus_66285afd_=bus_3f7eacae_;
endmodule



module medianRow9_globalreset_physical_7f441d78_(bus_373d06c2_, bus_7a39ab19_, bus_75e12fdb_);
input		bus_373d06c2_;
input		bus_7a39ab19_;
output		bus_75e12fdb_;
wire		or_27ba24cb_u0;
reg		sample_u65=1'h0;
reg		final_u65=1'h1;
wire		and_28908b76_u0;
reg		cross_u65=1'h0;
reg		glitch_u65=1'h0;
wire		not_2f93551d_u0;
assign or_27ba24cb_u0=bus_7a39ab19_|final_u65;
always @(posedge bus_373d06c2_)
begin
sample_u65<=1'h1;
end
always @(posedge bus_373d06c2_)
begin
final_u65<=not_2f93551d_u0;
end
assign and_28908b76_u0=cross_u65&glitch_u65;
always @(posedge bus_373d06c2_)
begin
cross_u65<=sample_u65;
end
always @(posedge bus_373d06c2_)
begin
glitch_u65<=cross_u65;
end
assign bus_75e12fdb_=or_27ba24cb_u0;
assign not_2f93551d_u0=~and_28908b76_u0;
endmodule


