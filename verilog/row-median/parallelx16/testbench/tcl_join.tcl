## ############################################################################
## __  ___ __ ___  _ __   ___  ___ 
## \ \/ / '__/ _ \| '_ \ / _ \/ __|
##  >  <| | | (_) | | | | (_) \__ \
## /_/\_\_|  \___/|_| |_|\___/|___/
## ############################################################################
## Xronos synthesizer
## Testbench TCL Script file for Actor: join 
## Date: 2018/02/23 14:28:39
## ############################################################################

## Set paths
set Lib "../lib/"
set LibSim "../lib/simulation"
set Rtl "../rtl"
set RtlGoDone "../rtl/rtlGoDone"
set Testbench "vhd"

## Create SystemBuilder design library
vlib SystemBuilder
vmap SystemBuilder SystemBuilder

## Compile the SystemBuilder Library from sources
vcom -reportprogress 300 -work SystemBuilder $Lib/systemBuilder/vhdl/sbtypes.vhdl
vcom -reportprogress 300 -work SystemBuilder $Lib/systemBuilder/vhdl/sbfifo.vhdl
vcom -reportprogress 300 -work SystemBuilder $Lib/systemBuilder/vhdl/sbfifo_behavioral.vhdl

## Create the work design library
if {[file exist work_join]} {vdel -all -lib work_join}
vlib work_join
vmap work_join work_join

## Compile the glbl constans given by Xilinx 
vlog -work work_join ../lib/simulation/glbl.v


# Compile sim package
vcom -93 -reportprogress 30 -work work_join $LibSim/sim_package.vhd
## Compile network instances and add them to work library	
vlog -work work_join $Rtl/join.v


## Compile the Testbench VHD
vcom -93 -check_synthesis -quiet -work work_join $Testbench/join_tb.vhd

## Start VSIM
vsim -voptargs="+acc" -L unisims_ver -L simprims_ver -t ns work_join.glbl work_join.join_tb
	
## Add clock(s) and reset signal
add wave -noupdate -divider -height 20 "CLK & RESET"

add wave sim:/join_tb/CLK
	add wave sim:/join_tb/RESET
	
	
	## Change radix to decimal
	radix -decimal

add wave -noupdate -divider -height 20  "Inputs: i_join"
add wave -label in1_DATA sim:/join_tb/i_join/in1_DATA
add wave -label in1_ACK sim:/join_tb/i_join/in1_ACK 
add wave -label in1_SEND sim:/join_tb/i_join/in1_SEND 

add wave -label in2_DATA sim:/join_tb/i_join/in2_DATA
add wave -label in2_ACK sim:/join_tb/i_join/in2_ACK 
add wave -label in2_SEND sim:/join_tb/i_join/in2_SEND 

add wave -label in3_DATA sim:/join_tb/i_join/in3_DATA
add wave -label in3_ACK sim:/join_tb/i_join/in3_ACK 
add wave -label in3_SEND sim:/join_tb/i_join/in3_SEND 

add wave -label in4_DATA sim:/join_tb/i_join/in4_DATA
add wave -label in4_ACK sim:/join_tb/i_join/in4_ACK 
add wave -label in4_SEND sim:/join_tb/i_join/in4_SEND 

add wave -label in5_DATA sim:/join_tb/i_join/in5_DATA
add wave -label in5_ACK sim:/join_tb/i_join/in5_ACK 
add wave -label in5_SEND sim:/join_tb/i_join/in5_SEND 

add wave -label in6_DATA sim:/join_tb/i_join/in6_DATA
add wave -label in6_ACK sim:/join_tb/i_join/in6_ACK 
add wave -label in6_SEND sim:/join_tb/i_join/in6_SEND 

add wave -label in7_DATA sim:/join_tb/i_join/in7_DATA
add wave -label in7_ACK sim:/join_tb/i_join/in7_ACK 
add wave -label in7_SEND sim:/join_tb/i_join/in7_SEND 

add wave -label in8_DATA sim:/join_tb/i_join/in8_DATA
add wave -label in8_ACK sim:/join_tb/i_join/in8_ACK 
add wave -label in8_SEND sim:/join_tb/i_join/in8_SEND 

add wave -label in9_DATA sim:/join_tb/i_join/in9_DATA
add wave -label in9_ACK sim:/join_tb/i_join/in9_ACK 
add wave -label in9_SEND sim:/join_tb/i_join/in9_SEND 

add wave -label in10_DATA sim:/join_tb/i_join/in10_DATA
add wave -label in10_ACK sim:/join_tb/i_join/in10_ACK 
add wave -label in10_SEND sim:/join_tb/i_join/in10_SEND 

add wave -label in11_DATA sim:/join_tb/i_join/in11_DATA
add wave -label in11_ACK sim:/join_tb/i_join/in11_ACK 
add wave -label in11_SEND sim:/join_tb/i_join/in11_SEND 

add wave -label in12_DATA sim:/join_tb/i_join/in12_DATA
add wave -label in12_ACK sim:/join_tb/i_join/in12_ACK 
add wave -label in12_SEND sim:/join_tb/i_join/in12_SEND 

add wave -label in13_DATA sim:/join_tb/i_join/in13_DATA
add wave -label in13_ACK sim:/join_tb/i_join/in13_ACK 
add wave -label in13_SEND sim:/join_tb/i_join/in13_SEND 

add wave -label in14_DATA sim:/join_tb/i_join/in14_DATA
add wave -label in14_ACK sim:/join_tb/i_join/in14_ACK 
add wave -label in14_SEND sim:/join_tb/i_join/in14_SEND 

add wave -label in15_DATA sim:/join_tb/i_join/in15_DATA
add wave -label in15_ACK sim:/join_tb/i_join/in15_ACK 
add wave -label in15_SEND sim:/join_tb/i_join/in15_SEND 

add wave -label in16_DATA sim:/join_tb/i_join/in16_DATA
add wave -label in16_ACK sim:/join_tb/i_join/in16_ACK 
add wave -label in16_SEND sim:/join_tb/i_join/in16_SEND 
add wave -noupdate -divider -height 20 "Outputs: i_join"
add wave -label out1_DATA sim:/join_tb/i_join/out1_DATA 
add wave -label out1_SEND sim:/join_tb/i_join/out1_ACK
add wave -label out1_SEND sim:/join_tb/i_join/out1_SEND
add wave -label out1_RDY sim:/join_tb/i_join/out1_RDY
add wave -noupdate -divider -height 20 "Go & Done" 
