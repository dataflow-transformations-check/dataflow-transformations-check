## ############################################################################
## __  ___ __ ___  _ __   ___  ___ 
## \ \/ / '__/ _ \| '_ \ / _ \/ __|
##  >  <| | | (_) | | | | (_) \__ \
## /_/\_\_|  \___/|_| |_|\___/|___/
## ############################################################################
## Xronos synthesizer
## Testbench TCL Script file for Network: medianParallel16 
## Date: 2018/02/23 14:28:55
## ############################################################################

## Set paths
set Lib "../lib/"
set LibSim "../lib/simulation"
set Rtl "../rtl"
set RtlGoDone "../rtl/rtlGoDone"
set Testbench "vhd"

## Create SystemBuilder design library
vlib SystemBuilder
vmap SystemBuilder SystemBuilder

## Compile the SystemBuilder Library from sources
vcom -reportprogress 300 -work SystemBuilder $Lib/systemBuilder/vhdl/sbtypes.vhdl
vcom -reportprogress 300 -work SystemBuilder $Lib/systemBuilder/vhdl/sbfifo.vhdl
vcom -reportprogress 300 -work SystemBuilder $Lib/systemBuilder/vhdl/sbfifo_behavioral.vhdl

## Create the work design library
if {[file exist work_medianParallel16]} {vdel -all -lib work_medianParallel16}
vlib work_medianParallel16
vmap work_medianParallel16 work_medianParallel16

## Compile the glbl constans given by Xilinx 
vlog -work work_medianParallel16 ../lib/simulation/glbl.v


# Compile sim package
vcom -93 -reportprogress 30 -work work_medianParallel16 $LibSim/sim_package.vhd
## Compile network instances and add them to work library	
vlog -work work_medianParallel16 $Rtl/split.v
vlog -work work_medianParallel16 $Rtl/join.v
vlog -work work_medianParallel16 $Rtl/medianRow1.v
vlog -work work_medianParallel16 $Rtl/medianRow2.v
vlog -work work_medianParallel16 $Rtl/medianRow16.v
vlog -work work_medianParallel16 $Rtl/medianRow15.v
vlog -work work_medianParallel16 $Rtl/medianRow14.v
vlog -work work_medianParallel16 $Rtl/medianRow13.v
vlog -work work_medianParallel16 $Rtl/medianRow12.v
vlog -work work_medianParallel16 $Rtl/medianRow11.v
vlog -work work_medianParallel16 $Rtl/medianRow10.v
vlog -work work_medianParallel16 $Rtl/medianRow9.v
vlog -work work_medianParallel16 $Rtl/medianRow8.v
vlog -work work_medianParallel16 $Rtl/medianRow7.v
vlog -work work_medianParallel16 $Rtl/medianRow6.v
vlog -work work_medianParallel16 $Rtl/medianRow5.v
vlog -work work_medianParallel16 $Rtl/medianRow4.v
vlog -work work_medianParallel16 $Rtl/medianRow3.v

## Compile the Top Network
vcom -93 -check_synthesis -quiet -work work_medianParallel16 $Rtl/medianParallel16.vhd

## Compile the Testbench VHD
vcom -93 -check_synthesis -quiet -work work_medianParallel16 $Testbench/medianParallel16_tb.vhd

## Start VSIM
vsim -voptargs="+acc" -L unisims_ver -L simprims_ver -t ns work_medianParallel16.glbl work_medianParallel16.medianParallel16_tb
	
## Add clock(s) and reset signal
add wave -noupdate -divider -height 20 "CLK & RESET"

add wave sim:/medianParallel16_tb/CLK
	add wave sim:/medianParallel16_tb/RESET
	
	
	## Change radix to decimal
	radix -decimal

add wave -noupdate -divider -height 20 ni_inPort
add wave sim:/medianParallel16_tb/inPort_DATA
add wave sim:/medianParallel16_tb/inPort_ACK
add wave sim:/medianParallel16_tb/inPort_SEND

add wave -noupdate -divider -height 20 no_outPort
add wave sim:/medianParallel16_tb/outPort_DATA
add wave sim:/medianParallel16_tb/outPort_SEND
add wave sim:/medianParallel16_tb/outPort_ACK
add wave sim:/medianParallel16_tb/outPort_RDY



add wave -noupdate -divider -height 20 i_split
add wave sim:/medianParallel16_tb/i_medianParallel16/i_split/CLK
add wave sim:/medianParallel16_tb/i_medianParallel16/i_split/in1_DATA
add wave sim:/medianParallel16_tb/i_medianParallel16/i_split/in1_ACK
add wave sim:/medianParallel16_tb/i_medianParallel16/i_split/in1_SEND
add wave sim:/medianParallel16_tb/i_medianParallel16/i_split/out1_DATA
add wave sim:/medianParallel16_tb/i_medianParallel16/i_split/out1_ACK
add wave sim:/medianParallel16_tb/i_medianParallel16/i_split/out1_SEND
add wave sim:/medianParallel16_tb/i_medianParallel16/i_split/out1_RDY

add wave sim:/medianParallel16_tb/i_medianParallel16/i_split/out2_DATA
add wave sim:/medianParallel16_tb/i_medianParallel16/i_split/out2_ACK
add wave sim:/medianParallel16_tb/i_medianParallel16/i_split/out2_SEND
add wave sim:/medianParallel16_tb/i_medianParallel16/i_split/out2_RDY

add wave sim:/medianParallel16_tb/i_medianParallel16/i_split/out3_DATA
add wave sim:/medianParallel16_tb/i_medianParallel16/i_split/out3_ACK
add wave sim:/medianParallel16_tb/i_medianParallel16/i_split/out3_SEND
add wave sim:/medianParallel16_tb/i_medianParallel16/i_split/out3_RDY

add wave sim:/medianParallel16_tb/i_medianParallel16/i_split/out4_DATA
add wave sim:/medianParallel16_tb/i_medianParallel16/i_split/out4_ACK
add wave sim:/medianParallel16_tb/i_medianParallel16/i_split/out4_SEND
add wave sim:/medianParallel16_tb/i_medianParallel16/i_split/out4_RDY

add wave sim:/medianParallel16_tb/i_medianParallel16/i_split/out5_DATA
add wave sim:/medianParallel16_tb/i_medianParallel16/i_split/out5_ACK
add wave sim:/medianParallel16_tb/i_medianParallel16/i_split/out5_SEND
add wave sim:/medianParallel16_tb/i_medianParallel16/i_split/out5_RDY

add wave sim:/medianParallel16_tb/i_medianParallel16/i_split/out6_DATA
add wave sim:/medianParallel16_tb/i_medianParallel16/i_split/out6_ACK
add wave sim:/medianParallel16_tb/i_medianParallel16/i_split/out6_SEND
add wave sim:/medianParallel16_tb/i_medianParallel16/i_split/out6_RDY

add wave sim:/medianParallel16_tb/i_medianParallel16/i_split/out7_DATA
add wave sim:/medianParallel16_tb/i_medianParallel16/i_split/out7_ACK
add wave sim:/medianParallel16_tb/i_medianParallel16/i_split/out7_SEND
add wave sim:/medianParallel16_tb/i_medianParallel16/i_split/out7_RDY

add wave sim:/medianParallel16_tb/i_medianParallel16/i_split/out8_DATA
add wave sim:/medianParallel16_tb/i_medianParallel16/i_split/out8_ACK
add wave sim:/medianParallel16_tb/i_medianParallel16/i_split/out8_SEND
add wave sim:/medianParallel16_tb/i_medianParallel16/i_split/out8_RDY

add wave sim:/medianParallel16_tb/i_medianParallel16/i_split/out9_DATA
add wave sim:/medianParallel16_tb/i_medianParallel16/i_split/out9_ACK
add wave sim:/medianParallel16_tb/i_medianParallel16/i_split/out9_SEND
add wave sim:/medianParallel16_tb/i_medianParallel16/i_split/out9_RDY

add wave sim:/medianParallel16_tb/i_medianParallel16/i_split/out10_DATA
add wave sim:/medianParallel16_tb/i_medianParallel16/i_split/out10_ACK
add wave sim:/medianParallel16_tb/i_medianParallel16/i_split/out10_SEND
add wave sim:/medianParallel16_tb/i_medianParallel16/i_split/out10_RDY

add wave sim:/medianParallel16_tb/i_medianParallel16/i_split/out11_DATA
add wave sim:/medianParallel16_tb/i_medianParallel16/i_split/out11_ACK
add wave sim:/medianParallel16_tb/i_medianParallel16/i_split/out11_SEND
add wave sim:/medianParallel16_tb/i_medianParallel16/i_split/out11_RDY

add wave sim:/medianParallel16_tb/i_medianParallel16/i_split/out12_DATA
add wave sim:/medianParallel16_tb/i_medianParallel16/i_split/out12_ACK
add wave sim:/medianParallel16_tb/i_medianParallel16/i_split/out12_SEND
add wave sim:/medianParallel16_tb/i_medianParallel16/i_split/out12_RDY

add wave sim:/medianParallel16_tb/i_medianParallel16/i_split/out13_DATA
add wave sim:/medianParallel16_tb/i_medianParallel16/i_split/out13_ACK
add wave sim:/medianParallel16_tb/i_medianParallel16/i_split/out13_SEND
add wave sim:/medianParallel16_tb/i_medianParallel16/i_split/out13_RDY

add wave sim:/medianParallel16_tb/i_medianParallel16/i_split/out14_DATA
add wave sim:/medianParallel16_tb/i_medianParallel16/i_split/out14_ACK
add wave sim:/medianParallel16_tb/i_medianParallel16/i_split/out14_SEND
add wave sim:/medianParallel16_tb/i_medianParallel16/i_split/out14_RDY

add wave sim:/medianParallel16_tb/i_medianParallel16/i_split/out15_DATA
add wave sim:/medianParallel16_tb/i_medianParallel16/i_split/out15_ACK
add wave sim:/medianParallel16_tb/i_medianParallel16/i_split/out15_SEND
add wave sim:/medianParallel16_tb/i_medianParallel16/i_split/out15_RDY

add wave sim:/medianParallel16_tb/i_medianParallel16/i_split/out16_DATA
add wave sim:/medianParallel16_tb/i_medianParallel16/i_split/out16_ACK
add wave sim:/medianParallel16_tb/i_medianParallel16/i_split/out16_SEND
add wave sim:/medianParallel16_tb/i_medianParallel16/i_split/out16_RDY

add wave -noupdate -divider -height 20 i_join
add wave sim:/medianParallel16_tb/i_medianParallel16/i_join/CLK
add wave sim:/medianParallel16_tb/i_medianParallel16/i_join/in1_DATA
add wave sim:/medianParallel16_tb/i_medianParallel16/i_join/in1_ACK
add wave sim:/medianParallel16_tb/i_medianParallel16/i_join/in1_SEND

add wave sim:/medianParallel16_tb/i_medianParallel16/i_join/in2_DATA
add wave sim:/medianParallel16_tb/i_medianParallel16/i_join/in2_ACK
add wave sim:/medianParallel16_tb/i_medianParallel16/i_join/in2_SEND

add wave sim:/medianParallel16_tb/i_medianParallel16/i_join/in3_DATA
add wave sim:/medianParallel16_tb/i_medianParallel16/i_join/in3_ACK
add wave sim:/medianParallel16_tb/i_medianParallel16/i_join/in3_SEND

add wave sim:/medianParallel16_tb/i_medianParallel16/i_join/in4_DATA
add wave sim:/medianParallel16_tb/i_medianParallel16/i_join/in4_ACK
add wave sim:/medianParallel16_tb/i_medianParallel16/i_join/in4_SEND

add wave sim:/medianParallel16_tb/i_medianParallel16/i_join/in5_DATA
add wave sim:/medianParallel16_tb/i_medianParallel16/i_join/in5_ACK
add wave sim:/medianParallel16_tb/i_medianParallel16/i_join/in5_SEND

add wave sim:/medianParallel16_tb/i_medianParallel16/i_join/in6_DATA
add wave sim:/medianParallel16_tb/i_medianParallel16/i_join/in6_ACK
add wave sim:/medianParallel16_tb/i_medianParallel16/i_join/in6_SEND

add wave sim:/medianParallel16_tb/i_medianParallel16/i_join/in7_DATA
add wave sim:/medianParallel16_tb/i_medianParallel16/i_join/in7_ACK
add wave sim:/medianParallel16_tb/i_medianParallel16/i_join/in7_SEND

add wave sim:/medianParallel16_tb/i_medianParallel16/i_join/in8_DATA
add wave sim:/medianParallel16_tb/i_medianParallel16/i_join/in8_ACK
add wave sim:/medianParallel16_tb/i_medianParallel16/i_join/in8_SEND

add wave sim:/medianParallel16_tb/i_medianParallel16/i_join/in9_DATA
add wave sim:/medianParallel16_tb/i_medianParallel16/i_join/in9_ACK
add wave sim:/medianParallel16_tb/i_medianParallel16/i_join/in9_SEND

add wave sim:/medianParallel16_tb/i_medianParallel16/i_join/in10_DATA
add wave sim:/medianParallel16_tb/i_medianParallel16/i_join/in10_ACK
add wave sim:/medianParallel16_tb/i_medianParallel16/i_join/in10_SEND

add wave sim:/medianParallel16_tb/i_medianParallel16/i_join/in11_DATA
add wave sim:/medianParallel16_tb/i_medianParallel16/i_join/in11_ACK
add wave sim:/medianParallel16_tb/i_medianParallel16/i_join/in11_SEND

add wave sim:/medianParallel16_tb/i_medianParallel16/i_join/in12_DATA
add wave sim:/medianParallel16_tb/i_medianParallel16/i_join/in12_ACK
add wave sim:/medianParallel16_tb/i_medianParallel16/i_join/in12_SEND

add wave sim:/medianParallel16_tb/i_medianParallel16/i_join/in13_DATA
add wave sim:/medianParallel16_tb/i_medianParallel16/i_join/in13_ACK
add wave sim:/medianParallel16_tb/i_medianParallel16/i_join/in13_SEND

add wave sim:/medianParallel16_tb/i_medianParallel16/i_join/in14_DATA
add wave sim:/medianParallel16_tb/i_medianParallel16/i_join/in14_ACK
add wave sim:/medianParallel16_tb/i_medianParallel16/i_join/in14_SEND

add wave sim:/medianParallel16_tb/i_medianParallel16/i_join/in15_DATA
add wave sim:/medianParallel16_tb/i_medianParallel16/i_join/in15_ACK
add wave sim:/medianParallel16_tb/i_medianParallel16/i_join/in15_SEND

add wave sim:/medianParallel16_tb/i_medianParallel16/i_join/in16_DATA
add wave sim:/medianParallel16_tb/i_medianParallel16/i_join/in16_ACK
add wave sim:/medianParallel16_tb/i_medianParallel16/i_join/in16_SEND
add wave sim:/medianParallel16_tb/i_medianParallel16/i_join/out1_DATA
add wave sim:/medianParallel16_tb/i_medianParallel16/i_join/out1_ACK
add wave sim:/medianParallel16_tb/i_medianParallel16/i_join/out1_SEND
add wave sim:/medianParallel16_tb/i_medianParallel16/i_join/out1_RDY

add wave -noupdate -divider -height 20 i_medianRow1
add wave sim:/medianParallel16_tb/i_medianParallel16/i_medianRow1/CLK
add wave sim:/medianParallel16_tb/i_medianParallel16/i_medianRow1/in1_DATA
add wave sim:/medianParallel16_tb/i_medianParallel16/i_medianRow1/in1_ACK
add wave sim:/medianParallel16_tb/i_medianParallel16/i_medianRow1/in1_SEND
add wave sim:/medianParallel16_tb/i_medianParallel16/i_medianRow1/median_DATA
add wave sim:/medianParallel16_tb/i_medianParallel16/i_medianRow1/median_ACK
add wave sim:/medianParallel16_tb/i_medianParallel16/i_medianRow1/median_SEND
add wave sim:/medianParallel16_tb/i_medianParallel16/i_medianRow1/median_RDY

add wave -noupdate -divider -height 20 i_medianRow2
add wave sim:/medianParallel16_tb/i_medianParallel16/i_medianRow2/CLK
add wave sim:/medianParallel16_tb/i_medianParallel16/i_medianRow2/in1_DATA
add wave sim:/medianParallel16_tb/i_medianParallel16/i_medianRow2/in1_ACK
add wave sim:/medianParallel16_tb/i_medianParallel16/i_medianRow2/in1_SEND
add wave sim:/medianParallel16_tb/i_medianParallel16/i_medianRow2/median_DATA
add wave sim:/medianParallel16_tb/i_medianParallel16/i_medianRow2/median_ACK
add wave sim:/medianParallel16_tb/i_medianParallel16/i_medianRow2/median_SEND
add wave sim:/medianParallel16_tb/i_medianParallel16/i_medianRow2/median_RDY

add wave -noupdate -divider -height 20 i_medianRow16
add wave sim:/medianParallel16_tb/i_medianParallel16/i_medianRow16/CLK
add wave sim:/medianParallel16_tb/i_medianParallel16/i_medianRow16/in1_DATA
add wave sim:/medianParallel16_tb/i_medianParallel16/i_medianRow16/in1_ACK
add wave sim:/medianParallel16_tb/i_medianParallel16/i_medianRow16/in1_SEND
add wave sim:/medianParallel16_tb/i_medianParallel16/i_medianRow16/median_DATA
add wave sim:/medianParallel16_tb/i_medianParallel16/i_medianRow16/median_ACK
add wave sim:/medianParallel16_tb/i_medianParallel16/i_medianRow16/median_SEND
add wave sim:/medianParallel16_tb/i_medianParallel16/i_medianRow16/median_RDY

add wave -noupdate -divider -height 20 i_medianRow15
add wave sim:/medianParallel16_tb/i_medianParallel16/i_medianRow15/CLK
add wave sim:/medianParallel16_tb/i_medianParallel16/i_medianRow15/in1_DATA
add wave sim:/medianParallel16_tb/i_medianParallel16/i_medianRow15/in1_ACK
add wave sim:/medianParallel16_tb/i_medianParallel16/i_medianRow15/in1_SEND
add wave sim:/medianParallel16_tb/i_medianParallel16/i_medianRow15/median_DATA
add wave sim:/medianParallel16_tb/i_medianParallel16/i_medianRow15/median_ACK
add wave sim:/medianParallel16_tb/i_medianParallel16/i_medianRow15/median_SEND
add wave sim:/medianParallel16_tb/i_medianParallel16/i_medianRow15/median_RDY

add wave -noupdate -divider -height 20 i_medianRow14
add wave sim:/medianParallel16_tb/i_medianParallel16/i_medianRow14/CLK
add wave sim:/medianParallel16_tb/i_medianParallel16/i_medianRow14/in1_DATA
add wave sim:/medianParallel16_tb/i_medianParallel16/i_medianRow14/in1_ACK
add wave sim:/medianParallel16_tb/i_medianParallel16/i_medianRow14/in1_SEND
add wave sim:/medianParallel16_tb/i_medianParallel16/i_medianRow14/median_DATA
add wave sim:/medianParallel16_tb/i_medianParallel16/i_medianRow14/median_ACK
add wave sim:/medianParallel16_tb/i_medianParallel16/i_medianRow14/median_SEND
add wave sim:/medianParallel16_tb/i_medianParallel16/i_medianRow14/median_RDY

add wave -noupdate -divider -height 20 i_medianRow13
add wave sim:/medianParallel16_tb/i_medianParallel16/i_medianRow13/CLK
add wave sim:/medianParallel16_tb/i_medianParallel16/i_medianRow13/in1_DATA
add wave sim:/medianParallel16_tb/i_medianParallel16/i_medianRow13/in1_ACK
add wave sim:/medianParallel16_tb/i_medianParallel16/i_medianRow13/in1_SEND
add wave sim:/medianParallel16_tb/i_medianParallel16/i_medianRow13/median_DATA
add wave sim:/medianParallel16_tb/i_medianParallel16/i_medianRow13/median_ACK
add wave sim:/medianParallel16_tb/i_medianParallel16/i_medianRow13/median_SEND
add wave sim:/medianParallel16_tb/i_medianParallel16/i_medianRow13/median_RDY

add wave -noupdate -divider -height 20 i_medianRow12
add wave sim:/medianParallel16_tb/i_medianParallel16/i_medianRow12/CLK
add wave sim:/medianParallel16_tb/i_medianParallel16/i_medianRow12/in1_DATA
add wave sim:/medianParallel16_tb/i_medianParallel16/i_medianRow12/in1_ACK
add wave sim:/medianParallel16_tb/i_medianParallel16/i_medianRow12/in1_SEND
add wave sim:/medianParallel16_tb/i_medianParallel16/i_medianRow12/median_DATA
add wave sim:/medianParallel16_tb/i_medianParallel16/i_medianRow12/median_ACK
add wave sim:/medianParallel16_tb/i_medianParallel16/i_medianRow12/median_SEND
add wave sim:/medianParallel16_tb/i_medianParallel16/i_medianRow12/median_RDY

add wave -noupdate -divider -height 20 i_medianRow11
add wave sim:/medianParallel16_tb/i_medianParallel16/i_medianRow11/CLK
add wave sim:/medianParallel16_tb/i_medianParallel16/i_medianRow11/in1_DATA
add wave sim:/medianParallel16_tb/i_medianParallel16/i_medianRow11/in1_ACK
add wave sim:/medianParallel16_tb/i_medianParallel16/i_medianRow11/in1_SEND
add wave sim:/medianParallel16_tb/i_medianParallel16/i_medianRow11/median_DATA
add wave sim:/medianParallel16_tb/i_medianParallel16/i_medianRow11/median_ACK
add wave sim:/medianParallel16_tb/i_medianParallel16/i_medianRow11/median_SEND
add wave sim:/medianParallel16_tb/i_medianParallel16/i_medianRow11/median_RDY

add wave -noupdate -divider -height 20 i_medianRow10
add wave sim:/medianParallel16_tb/i_medianParallel16/i_medianRow10/CLK
add wave sim:/medianParallel16_tb/i_medianParallel16/i_medianRow10/in1_DATA
add wave sim:/medianParallel16_tb/i_medianParallel16/i_medianRow10/in1_ACK
add wave sim:/medianParallel16_tb/i_medianParallel16/i_medianRow10/in1_SEND
add wave sim:/medianParallel16_tb/i_medianParallel16/i_medianRow10/median_DATA
add wave sim:/medianParallel16_tb/i_medianParallel16/i_medianRow10/median_ACK
add wave sim:/medianParallel16_tb/i_medianParallel16/i_medianRow10/median_SEND
add wave sim:/medianParallel16_tb/i_medianParallel16/i_medianRow10/median_RDY

add wave -noupdate -divider -height 20 i_medianRow9
add wave sim:/medianParallel16_tb/i_medianParallel16/i_medianRow9/CLK
add wave sim:/medianParallel16_tb/i_medianParallel16/i_medianRow9/in1_DATA
add wave sim:/medianParallel16_tb/i_medianParallel16/i_medianRow9/in1_ACK
add wave sim:/medianParallel16_tb/i_medianParallel16/i_medianRow9/in1_SEND
add wave sim:/medianParallel16_tb/i_medianParallel16/i_medianRow9/median_DATA
add wave sim:/medianParallel16_tb/i_medianParallel16/i_medianRow9/median_ACK
add wave sim:/medianParallel16_tb/i_medianParallel16/i_medianRow9/median_SEND
add wave sim:/medianParallel16_tb/i_medianParallel16/i_medianRow9/median_RDY

add wave -noupdate -divider -height 20 i_medianRow8
add wave sim:/medianParallel16_tb/i_medianParallel16/i_medianRow8/CLK
add wave sim:/medianParallel16_tb/i_medianParallel16/i_medianRow8/in1_DATA
add wave sim:/medianParallel16_tb/i_medianParallel16/i_medianRow8/in1_ACK
add wave sim:/medianParallel16_tb/i_medianParallel16/i_medianRow8/in1_SEND
add wave sim:/medianParallel16_tb/i_medianParallel16/i_medianRow8/median_DATA
add wave sim:/medianParallel16_tb/i_medianParallel16/i_medianRow8/median_ACK
add wave sim:/medianParallel16_tb/i_medianParallel16/i_medianRow8/median_SEND
add wave sim:/medianParallel16_tb/i_medianParallel16/i_medianRow8/median_RDY

add wave -noupdate -divider -height 20 i_medianRow7
add wave sim:/medianParallel16_tb/i_medianParallel16/i_medianRow7/CLK
add wave sim:/medianParallel16_tb/i_medianParallel16/i_medianRow7/in1_DATA
add wave sim:/medianParallel16_tb/i_medianParallel16/i_medianRow7/in1_ACK
add wave sim:/medianParallel16_tb/i_medianParallel16/i_medianRow7/in1_SEND
add wave sim:/medianParallel16_tb/i_medianParallel16/i_medianRow7/median_DATA
add wave sim:/medianParallel16_tb/i_medianParallel16/i_medianRow7/median_ACK
add wave sim:/medianParallel16_tb/i_medianParallel16/i_medianRow7/median_SEND
add wave sim:/medianParallel16_tb/i_medianParallel16/i_medianRow7/median_RDY

add wave -noupdate -divider -height 20 i_medianRow6
add wave sim:/medianParallel16_tb/i_medianParallel16/i_medianRow6/CLK
add wave sim:/medianParallel16_tb/i_medianParallel16/i_medianRow6/in1_DATA
add wave sim:/medianParallel16_tb/i_medianParallel16/i_medianRow6/in1_ACK
add wave sim:/medianParallel16_tb/i_medianParallel16/i_medianRow6/in1_SEND
add wave sim:/medianParallel16_tb/i_medianParallel16/i_medianRow6/median_DATA
add wave sim:/medianParallel16_tb/i_medianParallel16/i_medianRow6/median_ACK
add wave sim:/medianParallel16_tb/i_medianParallel16/i_medianRow6/median_SEND
add wave sim:/medianParallel16_tb/i_medianParallel16/i_medianRow6/median_RDY

add wave -noupdate -divider -height 20 i_medianRow5
add wave sim:/medianParallel16_tb/i_medianParallel16/i_medianRow5/CLK
add wave sim:/medianParallel16_tb/i_medianParallel16/i_medianRow5/in1_DATA
add wave sim:/medianParallel16_tb/i_medianParallel16/i_medianRow5/in1_ACK
add wave sim:/medianParallel16_tb/i_medianParallel16/i_medianRow5/in1_SEND
add wave sim:/medianParallel16_tb/i_medianParallel16/i_medianRow5/median_DATA
add wave sim:/medianParallel16_tb/i_medianParallel16/i_medianRow5/median_ACK
add wave sim:/medianParallel16_tb/i_medianParallel16/i_medianRow5/median_SEND
add wave sim:/medianParallel16_tb/i_medianParallel16/i_medianRow5/median_RDY

add wave -noupdate -divider -height 20 i_medianRow4
add wave sim:/medianParallel16_tb/i_medianParallel16/i_medianRow4/CLK
add wave sim:/medianParallel16_tb/i_medianParallel16/i_medianRow4/in1_DATA
add wave sim:/medianParallel16_tb/i_medianParallel16/i_medianRow4/in1_ACK
add wave sim:/medianParallel16_tb/i_medianParallel16/i_medianRow4/in1_SEND
add wave sim:/medianParallel16_tb/i_medianParallel16/i_medianRow4/median_DATA
add wave sim:/medianParallel16_tb/i_medianParallel16/i_medianRow4/median_ACK
add wave sim:/medianParallel16_tb/i_medianParallel16/i_medianRow4/median_SEND
add wave sim:/medianParallel16_tb/i_medianParallel16/i_medianRow4/median_RDY

add wave -noupdate -divider -height 20 i_medianRow3
add wave sim:/medianParallel16_tb/i_medianParallel16/i_medianRow3/CLK
add wave sim:/medianParallel16_tb/i_medianParallel16/i_medianRow3/in1_DATA
add wave sim:/medianParallel16_tb/i_medianParallel16/i_medianRow3/in1_ACK
add wave sim:/medianParallel16_tb/i_medianParallel16/i_medianRow3/in1_SEND
add wave sim:/medianParallel16_tb/i_medianParallel16/i_medianRow3/median_DATA
add wave sim:/medianParallel16_tb/i_medianParallel16/i_medianRow3/median_ACK
add wave sim:/medianParallel16_tb/i_medianParallel16/i_medianRow3/median_SEND
add wave sim:/medianParallel16_tb/i_medianParallel16/i_medianRow3/median_RDY

## FIFO FULL
add wave -noupdate -divider -height 20 "FIFO FULL"
add wave -label split_in1_full sim:/medianParallel16_tb/i_medianParallel16/q_ai_split_in1/full
add wave -label split_in1_almost_full sim:/medianParallel16_tb/i_medianParallel16/q_ai_split_in1/almost_full
add wave -label join_in1_full sim:/medianParallel16_tb/i_medianParallel16/q_ai_join_in1/full
add wave -label join_in1_almost_full sim:/medianParallel16_tb/i_medianParallel16/q_ai_join_in1/almost_full
add wave -label join_in2_full sim:/medianParallel16_tb/i_medianParallel16/q_ai_join_in2/full
add wave -label join_in2_almost_full sim:/medianParallel16_tb/i_medianParallel16/q_ai_join_in2/almost_full
add wave -label join_in3_full sim:/medianParallel16_tb/i_medianParallel16/q_ai_join_in3/full
add wave -label join_in3_almost_full sim:/medianParallel16_tb/i_medianParallel16/q_ai_join_in3/almost_full
add wave -label join_in4_full sim:/medianParallel16_tb/i_medianParallel16/q_ai_join_in4/full
add wave -label join_in4_almost_full sim:/medianParallel16_tb/i_medianParallel16/q_ai_join_in4/almost_full
add wave -label join_in5_full sim:/medianParallel16_tb/i_medianParallel16/q_ai_join_in5/full
add wave -label join_in5_almost_full sim:/medianParallel16_tb/i_medianParallel16/q_ai_join_in5/almost_full
add wave -label join_in6_full sim:/medianParallel16_tb/i_medianParallel16/q_ai_join_in6/full
add wave -label join_in6_almost_full sim:/medianParallel16_tb/i_medianParallel16/q_ai_join_in6/almost_full
add wave -label join_in7_full sim:/medianParallel16_tb/i_medianParallel16/q_ai_join_in7/full
add wave -label join_in7_almost_full sim:/medianParallel16_tb/i_medianParallel16/q_ai_join_in7/almost_full
add wave -label join_in8_full sim:/medianParallel16_tb/i_medianParallel16/q_ai_join_in8/full
add wave -label join_in8_almost_full sim:/medianParallel16_tb/i_medianParallel16/q_ai_join_in8/almost_full
add wave -label join_in9_full sim:/medianParallel16_tb/i_medianParallel16/q_ai_join_in9/full
add wave -label join_in9_almost_full sim:/medianParallel16_tb/i_medianParallel16/q_ai_join_in9/almost_full
add wave -label join_in10_full sim:/medianParallel16_tb/i_medianParallel16/q_ai_join_in10/full
add wave -label join_in10_almost_full sim:/medianParallel16_tb/i_medianParallel16/q_ai_join_in10/almost_full
add wave -label join_in11_full sim:/medianParallel16_tb/i_medianParallel16/q_ai_join_in11/full
add wave -label join_in11_almost_full sim:/medianParallel16_tb/i_medianParallel16/q_ai_join_in11/almost_full
add wave -label join_in12_full sim:/medianParallel16_tb/i_medianParallel16/q_ai_join_in12/full
add wave -label join_in12_almost_full sim:/medianParallel16_tb/i_medianParallel16/q_ai_join_in12/almost_full
add wave -label join_in13_full sim:/medianParallel16_tb/i_medianParallel16/q_ai_join_in13/full
add wave -label join_in13_almost_full sim:/medianParallel16_tb/i_medianParallel16/q_ai_join_in13/almost_full
add wave -label join_in14_full sim:/medianParallel16_tb/i_medianParallel16/q_ai_join_in14/full
add wave -label join_in14_almost_full sim:/medianParallel16_tb/i_medianParallel16/q_ai_join_in14/almost_full
add wave -label join_in15_full sim:/medianParallel16_tb/i_medianParallel16/q_ai_join_in15/full
add wave -label join_in15_almost_full sim:/medianParallel16_tb/i_medianParallel16/q_ai_join_in15/almost_full
add wave -label join_in16_full sim:/medianParallel16_tb/i_medianParallel16/q_ai_join_in16/full
add wave -label join_in16_almost_full sim:/medianParallel16_tb/i_medianParallel16/q_ai_join_in16/almost_full
add wave -label medianRow1_in1_full sim:/medianParallel16_tb/i_medianParallel16/q_ai_medianRow1_in1/full
add wave -label medianRow1_in1_almost_full sim:/medianParallel16_tb/i_medianParallel16/q_ai_medianRow1_in1/almost_full
add wave -label medianRow2_in1_full sim:/medianParallel16_tb/i_medianParallel16/q_ai_medianRow2_in1/full
add wave -label medianRow2_in1_almost_full sim:/medianParallel16_tb/i_medianParallel16/q_ai_medianRow2_in1/almost_full
add wave -label medianRow16_in1_full sim:/medianParallel16_tb/i_medianParallel16/q_ai_medianRow16_in1/full
add wave -label medianRow16_in1_almost_full sim:/medianParallel16_tb/i_medianParallel16/q_ai_medianRow16_in1/almost_full
add wave -label medianRow15_in1_full sim:/medianParallel16_tb/i_medianParallel16/q_ai_medianRow15_in1/full
add wave -label medianRow15_in1_almost_full sim:/medianParallel16_tb/i_medianParallel16/q_ai_medianRow15_in1/almost_full
add wave -label medianRow14_in1_full sim:/medianParallel16_tb/i_medianParallel16/q_ai_medianRow14_in1/full
add wave -label medianRow14_in1_almost_full sim:/medianParallel16_tb/i_medianParallel16/q_ai_medianRow14_in1/almost_full
add wave -label medianRow13_in1_full sim:/medianParallel16_tb/i_medianParallel16/q_ai_medianRow13_in1/full
add wave -label medianRow13_in1_almost_full sim:/medianParallel16_tb/i_medianParallel16/q_ai_medianRow13_in1/almost_full
add wave -label medianRow12_in1_full sim:/medianParallel16_tb/i_medianParallel16/q_ai_medianRow12_in1/full
add wave -label medianRow12_in1_almost_full sim:/medianParallel16_tb/i_medianParallel16/q_ai_medianRow12_in1/almost_full
add wave -label medianRow11_in1_full sim:/medianParallel16_tb/i_medianParallel16/q_ai_medianRow11_in1/full
add wave -label medianRow11_in1_almost_full sim:/medianParallel16_tb/i_medianParallel16/q_ai_medianRow11_in1/almost_full
add wave -label medianRow10_in1_full sim:/medianParallel16_tb/i_medianParallel16/q_ai_medianRow10_in1/full
add wave -label medianRow10_in1_almost_full sim:/medianParallel16_tb/i_medianParallel16/q_ai_medianRow10_in1/almost_full
add wave -label medianRow9_in1_full sim:/medianParallel16_tb/i_medianParallel16/q_ai_medianRow9_in1/full
add wave -label medianRow9_in1_almost_full sim:/medianParallel16_tb/i_medianParallel16/q_ai_medianRow9_in1/almost_full
add wave -label medianRow8_in1_full sim:/medianParallel16_tb/i_medianParallel16/q_ai_medianRow8_in1/full
add wave -label medianRow8_in1_almost_full sim:/medianParallel16_tb/i_medianParallel16/q_ai_medianRow8_in1/almost_full
add wave -label medianRow7_in1_full sim:/medianParallel16_tb/i_medianParallel16/q_ai_medianRow7_in1/full
add wave -label medianRow7_in1_almost_full sim:/medianParallel16_tb/i_medianParallel16/q_ai_medianRow7_in1/almost_full
add wave -label medianRow6_in1_full sim:/medianParallel16_tb/i_medianParallel16/q_ai_medianRow6_in1/full
add wave -label medianRow6_in1_almost_full sim:/medianParallel16_tb/i_medianParallel16/q_ai_medianRow6_in1/almost_full
add wave -label medianRow5_in1_full sim:/medianParallel16_tb/i_medianParallel16/q_ai_medianRow5_in1/full
add wave -label medianRow5_in1_almost_full sim:/medianParallel16_tb/i_medianParallel16/q_ai_medianRow5_in1/almost_full
add wave -label medianRow4_in1_full sim:/medianParallel16_tb/i_medianParallel16/q_ai_medianRow4_in1/full
add wave -label medianRow4_in1_almost_full sim:/medianParallel16_tb/i_medianParallel16/q_ai_medianRow4_in1/almost_full
add wave -label medianRow3_in1_full sim:/medianParallel16_tb/i_medianParallel16/q_ai_medianRow3_in1/full
add wave -label medianRow3_in1_almost_full sim:/medianParallel16_tb/i_medianParallel16/q_ai_medianRow3_in1/almost_full

