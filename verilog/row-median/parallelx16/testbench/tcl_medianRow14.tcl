## ############################################################################
## __  ___ __ ___  _ __   ___  ___ 
## \ \/ / '__/ _ \| '_ \ / _ \/ __|
##  >  <| | | (_) | | | | (_) \__ \
## /_/\_\_|  \___/|_| |_|\___/|___/
## ############################################################################
## Xronos synthesizer
## Testbench TCL Script file for Actor: medianRow14 
## Date: 2018/02/23 14:28:43
## ############################################################################

## Set paths
set Lib "../lib/"
set LibSim "../lib/simulation"
set Rtl "../rtl"
set RtlGoDone "../rtl/rtlGoDone"
set Testbench "vhd"

## Create SystemBuilder design library
vlib SystemBuilder
vmap SystemBuilder SystemBuilder

## Compile the SystemBuilder Library from sources
vcom -reportprogress 300 -work SystemBuilder $Lib/systemBuilder/vhdl/sbtypes.vhdl
vcom -reportprogress 300 -work SystemBuilder $Lib/systemBuilder/vhdl/sbfifo.vhdl
vcom -reportprogress 300 -work SystemBuilder $Lib/systemBuilder/vhdl/sbfifo_behavioral.vhdl

## Create the work design library
if {[file exist work_medianRow14]} {vdel -all -lib work_medianRow14}
vlib work_medianRow14
vmap work_medianRow14 work_medianRow14

## Compile the glbl constans given by Xilinx 
vlog -work work_medianRow14 ../lib/simulation/glbl.v


# Compile sim package
vcom -93 -reportprogress 30 -work work_medianRow14 $LibSim/sim_package.vhd
## Compile network instances and add them to work library	
vlog -work work_medianRow14 $Rtl/medianRow14.v


## Compile the Testbench VHD
vcom -93 -check_synthesis -quiet -work work_medianRow14 $Testbench/medianRow14_tb.vhd

## Start VSIM
vsim -voptargs="+acc" -L unisims_ver -L simprims_ver -t ns work_medianRow14.glbl work_medianRow14.medianRow14_tb
	
## Add clock(s) and reset signal
add wave -noupdate -divider -height 20 "CLK & RESET"

add wave sim:/medianRow14_tb/CLK
	add wave sim:/medianRow14_tb/RESET
	
	
	## Change radix to decimal
	radix -decimal

add wave -noupdate -divider -height 20  "Inputs: i_medianRow14"
add wave -label in1_DATA sim:/medianRow14_tb/i_medianRow14/in1_DATA
add wave -label in1_ACK sim:/medianRow14_tb/i_medianRow14/in1_ACK 
add wave -label in1_SEND sim:/medianRow14_tb/i_medianRow14/in1_SEND 
add wave -noupdate -divider -height 20 "Outputs: i_medianRow14"
add wave -label median_DATA sim:/medianRow14_tb/i_medianRow14/median_DATA 
add wave -label median_SEND sim:/medianRow14_tb/i_medianRow14/median_ACK
add wave -label median_SEND sim:/medianRow14_tb/i_medianRow14/median_SEND
add wave -label median_RDY sim:/medianRow14_tb/i_medianRow14/median_RDY
add wave -noupdate -divider -height 20 "Go & Done" 
