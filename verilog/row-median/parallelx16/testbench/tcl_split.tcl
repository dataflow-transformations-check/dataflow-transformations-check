## ############################################################################
## __  ___ __ ___  _ __   ___  ___ 
## \ \/ / '__/ _ \| '_ \ / _ \/ __|
##  >  <| | | (_) | | | | (_) \__ \
## /_/\_\_|  \___/|_| |_|\___/|___/
## ############################################################################
## Xronos synthesizer
## Testbench TCL Script file for Actor: split 
## Date: 2018/02/23 14:28:35
## ############################################################################

## Set paths
set Lib "../lib/"
set LibSim "../lib/simulation"
set Rtl "../rtl"
set RtlGoDone "../rtl/rtlGoDone"
set Testbench "vhd"

## Create SystemBuilder design library
vlib SystemBuilder
vmap SystemBuilder SystemBuilder

## Compile the SystemBuilder Library from sources
vcom -reportprogress 300 -work SystemBuilder $Lib/systemBuilder/vhdl/sbtypes.vhdl
vcom -reportprogress 300 -work SystemBuilder $Lib/systemBuilder/vhdl/sbfifo.vhdl
vcom -reportprogress 300 -work SystemBuilder $Lib/systemBuilder/vhdl/sbfifo_behavioral.vhdl

## Create the work design library
if {[file exist work_split]} {vdel -all -lib work_split}
vlib work_split
vmap work_split work_split

## Compile the glbl constans given by Xilinx 
vlog -work work_split ../lib/simulation/glbl.v


# Compile sim package
vcom -93 -reportprogress 30 -work work_split $LibSim/sim_package.vhd
## Compile network instances and add them to work library	
vlog -work work_split $Rtl/split.v


## Compile the Testbench VHD
vcom -93 -check_synthesis -quiet -work work_split $Testbench/split_tb.vhd

## Start VSIM
vsim -voptargs="+acc" -L unisims_ver -L simprims_ver -t ns work_split.glbl work_split.split_tb
	
## Add clock(s) and reset signal
add wave -noupdate -divider -height 20 "CLK & RESET"

add wave sim:/split_tb/CLK
	add wave sim:/split_tb/RESET
	
	
	## Change radix to decimal
	radix -decimal

add wave -noupdate -divider -height 20  "Inputs: i_split"
add wave -label in1_DATA sim:/split_tb/i_split/in1_DATA
add wave -label in1_ACK sim:/split_tb/i_split/in1_ACK 
add wave -label in1_SEND sim:/split_tb/i_split/in1_SEND 
add wave -noupdate -divider -height 20 "Outputs: i_split"
add wave -label out1_DATA sim:/split_tb/i_split/out1_DATA 
add wave -label out1_SEND sim:/split_tb/i_split/out1_ACK
add wave -label out1_SEND sim:/split_tb/i_split/out1_SEND
add wave -label out1_RDY sim:/split_tb/i_split/out1_RDY

add wave -label out2_DATA sim:/split_tb/i_split/out2_DATA 
add wave -label out2_SEND sim:/split_tb/i_split/out2_ACK
add wave -label out2_SEND sim:/split_tb/i_split/out2_SEND
add wave -label out2_RDY sim:/split_tb/i_split/out2_RDY

add wave -label out3_DATA sim:/split_tb/i_split/out3_DATA 
add wave -label out3_SEND sim:/split_tb/i_split/out3_ACK
add wave -label out3_SEND sim:/split_tb/i_split/out3_SEND
add wave -label out3_RDY sim:/split_tb/i_split/out3_RDY

add wave -label out4_DATA sim:/split_tb/i_split/out4_DATA 
add wave -label out4_SEND sim:/split_tb/i_split/out4_ACK
add wave -label out4_SEND sim:/split_tb/i_split/out4_SEND
add wave -label out4_RDY sim:/split_tb/i_split/out4_RDY

add wave -label out5_DATA sim:/split_tb/i_split/out5_DATA 
add wave -label out5_SEND sim:/split_tb/i_split/out5_ACK
add wave -label out5_SEND sim:/split_tb/i_split/out5_SEND
add wave -label out5_RDY sim:/split_tb/i_split/out5_RDY

add wave -label out6_DATA sim:/split_tb/i_split/out6_DATA 
add wave -label out6_SEND sim:/split_tb/i_split/out6_ACK
add wave -label out6_SEND sim:/split_tb/i_split/out6_SEND
add wave -label out6_RDY sim:/split_tb/i_split/out6_RDY

add wave -label out7_DATA sim:/split_tb/i_split/out7_DATA 
add wave -label out7_SEND sim:/split_tb/i_split/out7_ACK
add wave -label out7_SEND sim:/split_tb/i_split/out7_SEND
add wave -label out7_RDY sim:/split_tb/i_split/out7_RDY

add wave -label out8_DATA sim:/split_tb/i_split/out8_DATA 
add wave -label out8_SEND sim:/split_tb/i_split/out8_ACK
add wave -label out8_SEND sim:/split_tb/i_split/out8_SEND
add wave -label out8_RDY sim:/split_tb/i_split/out8_RDY

add wave -label out9_DATA sim:/split_tb/i_split/out9_DATA 
add wave -label out9_SEND sim:/split_tb/i_split/out9_ACK
add wave -label out9_SEND sim:/split_tb/i_split/out9_SEND
add wave -label out9_RDY sim:/split_tb/i_split/out9_RDY

add wave -label out10_DATA sim:/split_tb/i_split/out10_DATA 
add wave -label out10_SEND sim:/split_tb/i_split/out10_ACK
add wave -label out10_SEND sim:/split_tb/i_split/out10_SEND
add wave -label out10_RDY sim:/split_tb/i_split/out10_RDY

add wave -label out11_DATA sim:/split_tb/i_split/out11_DATA 
add wave -label out11_SEND sim:/split_tb/i_split/out11_ACK
add wave -label out11_SEND sim:/split_tb/i_split/out11_SEND
add wave -label out11_RDY sim:/split_tb/i_split/out11_RDY

add wave -label out12_DATA sim:/split_tb/i_split/out12_DATA 
add wave -label out12_SEND sim:/split_tb/i_split/out12_ACK
add wave -label out12_SEND sim:/split_tb/i_split/out12_SEND
add wave -label out12_RDY sim:/split_tb/i_split/out12_RDY

add wave -label out13_DATA sim:/split_tb/i_split/out13_DATA 
add wave -label out13_SEND sim:/split_tb/i_split/out13_ACK
add wave -label out13_SEND sim:/split_tb/i_split/out13_SEND
add wave -label out13_RDY sim:/split_tb/i_split/out13_RDY

add wave -label out14_DATA sim:/split_tb/i_split/out14_DATA 
add wave -label out14_SEND sim:/split_tb/i_split/out14_ACK
add wave -label out14_SEND sim:/split_tb/i_split/out14_SEND
add wave -label out14_RDY sim:/split_tb/i_split/out14_RDY

add wave -label out15_DATA sim:/split_tb/i_split/out15_DATA 
add wave -label out15_SEND sim:/split_tb/i_split/out15_ACK
add wave -label out15_SEND sim:/split_tb/i_split/out15_SEND
add wave -label out15_RDY sim:/split_tb/i_split/out15_RDY

add wave -label out16_DATA sim:/split_tb/i_split/out16_DATA 
add wave -label out16_SEND sim:/split_tb/i_split/out16_ACK
add wave -label out16_SEND sim:/split_tb/i_split/out16_SEND
add wave -label out16_RDY sim:/split_tb/i_split/out16_RDY
add wave -noupdate -divider -height 20 "Go & Done" 
