-- ----------------------------------------------------------------------------
-- __  ___ __ ___  _ __   ___  ___ 
-- \ \/ / '__/ _ \| '_ \ / _ \/ __|
--  >  <| | | (_) | | | | (_) \__ \
-- /_/\_\_|  \___/|_| |_|\___/|___/
-- ----------------------------------------------------------------------------
-- Xronos synthesizer
-- Testbench for Instance: join 
-- Date: 2018/02/23 14:28:39
-- ----------------------------------------------------------------------------

library ieee, SystemBuilder;
use ieee.std_logic_1164.all;
use ieee.std_logic_unsigned.all;
use ieee.numeric_std.all;
use std.textio.all;

library work;
use work.sim_package.all;

entity join_tb is
end join_tb;

architecture arch_join_tb of join_tb is
	-----------------------------------------------------------------------
	-- Component declaration
	-----------------------------------------------------------------------
	component join
	port(
	    in1_data : IN std_logic_vector(7 downto 0);
	    in1_send : IN std_logic;
	    in1_ack : OUT std_logic;
	    in1_count : IN std_logic_vector(15 downto 0);
	    in2_data : IN std_logic_vector(7 downto 0);
	    in2_send : IN std_logic;
	    in2_ack : OUT std_logic;
	    in2_count : IN std_logic_vector(15 downto 0);
	    in3_data : IN std_logic_vector(7 downto 0);
	    in3_send : IN std_logic;
	    in3_ack : OUT std_logic;
	    in3_count : IN std_logic_vector(15 downto 0);
	    in4_data : IN std_logic_vector(7 downto 0);
	    in4_send : IN std_logic;
	    in4_ack : OUT std_logic;
	    in4_count : IN std_logic_vector(15 downto 0);
	    in5_data : IN std_logic_vector(7 downto 0);
	    in5_send : IN std_logic;
	    in5_ack : OUT std_logic;
	    in5_count : IN std_logic_vector(15 downto 0);
	    in6_data : IN std_logic_vector(7 downto 0);
	    in6_send : IN std_logic;
	    in6_ack : OUT std_logic;
	    in6_count : IN std_logic_vector(15 downto 0);
	    in7_data : IN std_logic_vector(7 downto 0);
	    in7_send : IN std_logic;
	    in7_ack : OUT std_logic;
	    in7_count : IN std_logic_vector(15 downto 0);
	    in8_data : IN std_logic_vector(7 downto 0);
	    in8_send : IN std_logic;
	    in8_ack : OUT std_logic;
	    in8_count : IN std_logic_vector(15 downto 0);
	    in9_data : IN std_logic_vector(7 downto 0);
	    in9_send : IN std_logic;
	    in9_ack : OUT std_logic;
	    in9_count : IN std_logic_vector(15 downto 0);
	    in10_data : IN std_logic_vector(7 downto 0);
	    in10_send : IN std_logic;
	    in10_ack : OUT std_logic;
	    in10_count : IN std_logic_vector(15 downto 0);
	    in11_data : IN std_logic_vector(7 downto 0);
	    in11_send : IN std_logic;
	    in11_ack : OUT std_logic;
	    in11_count : IN std_logic_vector(15 downto 0);
	    in12_data : IN std_logic_vector(7 downto 0);
	    in12_send : IN std_logic;
	    in12_ack : OUT std_logic;
	    in12_count : IN std_logic_vector(15 downto 0);
	    in13_data : IN std_logic_vector(7 downto 0);
	    in13_send : IN std_logic;
	    in13_ack : OUT std_logic;
	    in13_count : IN std_logic_vector(15 downto 0);
	    in14_data : IN std_logic_vector(7 downto 0);
	    in14_send : IN std_logic;
	    in14_ack : OUT std_logic;
	    in14_count : IN std_logic_vector(15 downto 0);
	    in15_data : IN std_logic_vector(7 downto 0);
	    in15_send : IN std_logic;
	    in15_ack : OUT std_logic;
	    in15_count : IN std_logic_vector(15 downto 0);
	    in16_data : IN std_logic_vector(7 downto 0);
	    in16_send : IN std_logic;
	    in16_ack : OUT std_logic;
	    in16_count : IN std_logic_vector(15 downto 0);
	    out1_data : OUT std_logic_vector(7 downto 0);
	    out1_send : OUT std_logic;
	    out1_ack : IN std_logic;
	    out1_rdy : IN std_logic;
	    out1_count : OUT std_logic_vector(15 downto 0);
	    CLK: IN std_logic;
	    RESET: IN std_logic);
	end component join;
	
		-----------------------------------------------------------------------
		-- Achitecure signals & constants
		-----------------------------------------------------------------------
		constant PERIOD : time := 100 ns;
		constant DUTY_CYCLE : real := 0.5;
		constant OFFSET : time := 100 ns;
		-- Severity level and testbench type types
		type severity_level is (note, warning, error, failure);
		type tb_type is (after_reset, read_file, CheckRead);
		
		-- Component input(s) signals
		signal tb_FSM_in1 : tb_type;
		file sim_file_join_in1 : text is "fifoTraces/join_in1.txt";
		signal in1_data : std_logic_vector(7 downto 0) := (others => '0');
		signal in1_send : std_logic := '0';
		signal in1_ack : std_logic;
		signal in1_rdy : std_logic;
		signal in1_count : std_logic_vector(15 downto 0) := (others => '0');
		-- Input component queue
		signal q_in1_data : std_logic_vector(7 downto 0) := (others => '0');
		signal q_in1_send : std_logic := '0';
		signal q_in1_ack : std_logic;
		signal q_in1_rdy : std_logic;
		signal q_in1_count : std_logic_vector(15 downto 0) := (others => '0');
		signal tb_FSM_in2 : tb_type;
		file sim_file_join_in2 : text is "fifoTraces/join_in2.txt";
		signal in2_data : std_logic_vector(7 downto 0) := (others => '0');
		signal in2_send : std_logic := '0';
		signal in2_ack : std_logic;
		signal in2_rdy : std_logic;
		signal in2_count : std_logic_vector(15 downto 0) := (others => '0');
		-- Input component queue
		signal q_in2_data : std_logic_vector(7 downto 0) := (others => '0');
		signal q_in2_send : std_logic := '0';
		signal q_in2_ack : std_logic;
		signal q_in2_rdy : std_logic;
		signal q_in2_count : std_logic_vector(15 downto 0) := (others => '0');
		signal tb_FSM_in3 : tb_type;
		file sim_file_join_in3 : text is "fifoTraces/join_in3.txt";
		signal in3_data : std_logic_vector(7 downto 0) := (others => '0');
		signal in3_send : std_logic := '0';
		signal in3_ack : std_logic;
		signal in3_rdy : std_logic;
		signal in3_count : std_logic_vector(15 downto 0) := (others => '0');
		-- Input component queue
		signal q_in3_data : std_logic_vector(7 downto 0) := (others => '0');
		signal q_in3_send : std_logic := '0';
		signal q_in3_ack : std_logic;
		signal q_in3_rdy : std_logic;
		signal q_in3_count : std_logic_vector(15 downto 0) := (others => '0');
		signal tb_FSM_in4 : tb_type;
		file sim_file_join_in4 : text is "fifoTraces/join_in4.txt";
		signal in4_data : std_logic_vector(7 downto 0) := (others => '0');
		signal in4_send : std_logic := '0';
		signal in4_ack : std_logic;
		signal in4_rdy : std_logic;
		signal in4_count : std_logic_vector(15 downto 0) := (others => '0');
		-- Input component queue
		signal q_in4_data : std_logic_vector(7 downto 0) := (others => '0');
		signal q_in4_send : std_logic := '0';
		signal q_in4_ack : std_logic;
		signal q_in4_rdy : std_logic;
		signal q_in4_count : std_logic_vector(15 downto 0) := (others => '0');
		signal tb_FSM_in5 : tb_type;
		file sim_file_join_in5 : text is "fifoTraces/join_in5.txt";
		signal in5_data : std_logic_vector(7 downto 0) := (others => '0');
		signal in5_send : std_logic := '0';
		signal in5_ack : std_logic;
		signal in5_rdy : std_logic;
		signal in5_count : std_logic_vector(15 downto 0) := (others => '0');
		-- Input component queue
		signal q_in5_data : std_logic_vector(7 downto 0) := (others => '0');
		signal q_in5_send : std_logic := '0';
		signal q_in5_ack : std_logic;
		signal q_in5_rdy : std_logic;
		signal q_in5_count : std_logic_vector(15 downto 0) := (others => '0');
		signal tb_FSM_in6 : tb_type;
		file sim_file_join_in6 : text is "fifoTraces/join_in6.txt";
		signal in6_data : std_logic_vector(7 downto 0) := (others => '0');
		signal in6_send : std_logic := '0';
		signal in6_ack : std_logic;
		signal in6_rdy : std_logic;
		signal in6_count : std_logic_vector(15 downto 0) := (others => '0');
		-- Input component queue
		signal q_in6_data : std_logic_vector(7 downto 0) := (others => '0');
		signal q_in6_send : std_logic := '0';
		signal q_in6_ack : std_logic;
		signal q_in6_rdy : std_logic;
		signal q_in6_count : std_logic_vector(15 downto 0) := (others => '0');
		signal tb_FSM_in7 : tb_type;
		file sim_file_join_in7 : text is "fifoTraces/join_in7.txt";
		signal in7_data : std_logic_vector(7 downto 0) := (others => '0');
		signal in7_send : std_logic := '0';
		signal in7_ack : std_logic;
		signal in7_rdy : std_logic;
		signal in7_count : std_logic_vector(15 downto 0) := (others => '0');
		-- Input component queue
		signal q_in7_data : std_logic_vector(7 downto 0) := (others => '0');
		signal q_in7_send : std_logic := '0';
		signal q_in7_ack : std_logic;
		signal q_in7_rdy : std_logic;
		signal q_in7_count : std_logic_vector(15 downto 0) := (others => '0');
		signal tb_FSM_in8 : tb_type;
		file sim_file_join_in8 : text is "fifoTraces/join_in8.txt";
		signal in8_data : std_logic_vector(7 downto 0) := (others => '0');
		signal in8_send : std_logic := '0';
		signal in8_ack : std_logic;
		signal in8_rdy : std_logic;
		signal in8_count : std_logic_vector(15 downto 0) := (others => '0');
		-- Input component queue
		signal q_in8_data : std_logic_vector(7 downto 0) := (others => '0');
		signal q_in8_send : std_logic := '0';
		signal q_in8_ack : std_logic;
		signal q_in8_rdy : std_logic;
		signal q_in8_count : std_logic_vector(15 downto 0) := (others => '0');
		signal tb_FSM_in9 : tb_type;
		file sim_file_join_in9 : text is "fifoTraces/join_in9.txt";
		signal in9_data : std_logic_vector(7 downto 0) := (others => '0');
		signal in9_send : std_logic := '0';
		signal in9_ack : std_logic;
		signal in9_rdy : std_logic;
		signal in9_count : std_logic_vector(15 downto 0) := (others => '0');
		-- Input component queue
		signal q_in9_data : std_logic_vector(7 downto 0) := (others => '0');
		signal q_in9_send : std_logic := '0';
		signal q_in9_ack : std_logic;
		signal q_in9_rdy : std_logic;
		signal q_in9_count : std_logic_vector(15 downto 0) := (others => '0');
		signal tb_FSM_in10 : tb_type;
		file sim_file_join_in10 : text is "fifoTraces/join_in10.txt";
		signal in10_data : std_logic_vector(7 downto 0) := (others => '0');
		signal in10_send : std_logic := '0';
		signal in10_ack : std_logic;
		signal in10_rdy : std_logic;
		signal in10_count : std_logic_vector(15 downto 0) := (others => '0');
		-- Input component queue
		signal q_in10_data : std_logic_vector(7 downto 0) := (others => '0');
		signal q_in10_send : std_logic := '0';
		signal q_in10_ack : std_logic;
		signal q_in10_rdy : std_logic;
		signal q_in10_count : std_logic_vector(15 downto 0) := (others => '0');
		signal tb_FSM_in11 : tb_type;
		file sim_file_join_in11 : text is "fifoTraces/join_in11.txt";
		signal in11_data : std_logic_vector(7 downto 0) := (others => '0');
		signal in11_send : std_logic := '0';
		signal in11_ack : std_logic;
		signal in11_rdy : std_logic;
		signal in11_count : std_logic_vector(15 downto 0) := (others => '0');
		-- Input component queue
		signal q_in11_data : std_logic_vector(7 downto 0) := (others => '0');
		signal q_in11_send : std_logic := '0';
		signal q_in11_ack : std_logic;
		signal q_in11_rdy : std_logic;
		signal q_in11_count : std_logic_vector(15 downto 0) := (others => '0');
		signal tb_FSM_in12 : tb_type;
		file sim_file_join_in12 : text is "fifoTraces/join_in12.txt";
		signal in12_data : std_logic_vector(7 downto 0) := (others => '0');
		signal in12_send : std_logic := '0';
		signal in12_ack : std_logic;
		signal in12_rdy : std_logic;
		signal in12_count : std_logic_vector(15 downto 0) := (others => '0');
		-- Input component queue
		signal q_in12_data : std_logic_vector(7 downto 0) := (others => '0');
		signal q_in12_send : std_logic := '0';
		signal q_in12_ack : std_logic;
		signal q_in12_rdy : std_logic;
		signal q_in12_count : std_logic_vector(15 downto 0) := (others => '0');
		signal tb_FSM_in13 : tb_type;
		file sim_file_join_in13 : text is "fifoTraces/join_in13.txt";
		signal in13_data : std_logic_vector(7 downto 0) := (others => '0');
		signal in13_send : std_logic := '0';
		signal in13_ack : std_logic;
		signal in13_rdy : std_logic;
		signal in13_count : std_logic_vector(15 downto 0) := (others => '0');
		-- Input component queue
		signal q_in13_data : std_logic_vector(7 downto 0) := (others => '0');
		signal q_in13_send : std_logic := '0';
		signal q_in13_ack : std_logic;
		signal q_in13_rdy : std_logic;
		signal q_in13_count : std_logic_vector(15 downto 0) := (others => '0');
		signal tb_FSM_in14 : tb_type;
		file sim_file_join_in14 : text is "fifoTraces/join_in14.txt";
		signal in14_data : std_logic_vector(7 downto 0) := (others => '0');
		signal in14_send : std_logic := '0';
		signal in14_ack : std_logic;
		signal in14_rdy : std_logic;
		signal in14_count : std_logic_vector(15 downto 0) := (others => '0');
		-- Input component queue
		signal q_in14_data : std_logic_vector(7 downto 0) := (others => '0');
		signal q_in14_send : std_logic := '0';
		signal q_in14_ack : std_logic;
		signal q_in14_rdy : std_logic;
		signal q_in14_count : std_logic_vector(15 downto 0) := (others => '0');
		signal tb_FSM_in15 : tb_type;
		file sim_file_join_in15 : text is "fifoTraces/join_in15.txt";
		signal in15_data : std_logic_vector(7 downto 0) := (others => '0');
		signal in15_send : std_logic := '0';
		signal in15_ack : std_logic;
		signal in15_rdy : std_logic;
		signal in15_count : std_logic_vector(15 downto 0) := (others => '0');
		-- Input component queue
		signal q_in15_data : std_logic_vector(7 downto 0) := (others => '0');
		signal q_in15_send : std_logic := '0';
		signal q_in15_ack : std_logic;
		signal q_in15_rdy : std_logic;
		signal q_in15_count : std_logic_vector(15 downto 0) := (others => '0');
		signal tb_FSM_in16 : tb_type;
		file sim_file_join_in16 : text is "fifoTraces/join_in16.txt";
		signal in16_data : std_logic_vector(7 downto 0) := (others => '0');
		signal in16_send : std_logic := '0';
		signal in16_ack : std_logic;
		signal in16_rdy : std_logic;
		signal in16_count : std_logic_vector(15 downto 0) := (others => '0');
		-- Input component queue
		signal q_in16_data : std_logic_vector(7 downto 0) := (others => '0');
		signal q_in16_send : std_logic := '0';
		signal q_in16_ack : std_logic;
		signal q_in16_rdy : std_logic;
		signal q_in16_count : std_logic_vector(15 downto 0) := (others => '0');
		
		-- Component Output(s) signals
		signal tb_FSM_out1 : tb_type;
		file sim_file_join_out1 : text is "fifoTraces/join_out1.txt";
		signal out1_data : std_logic_vector(7 downto 0) := (others => '0');
		signal out1_send : std_logic;
		signal out1_ack : std_logic := '0';
		signal out1_rdy : std_logic := '0';
		signal out1_count : std_logic_vector(15 downto 0) := (others => '0');
		
	
		-- GoDone Weights Output Files
		
		signal count : integer range 255 downto 0 := 0;
		signal CLK : std_logic := '0';
		signal reset : std_logic := '0';
		
begin
	
	i_join : join 
	port map(
		in1_data => q_in1_data,
		in1_send => q_in1_send,
		in1_ack => q_in1_ack,
		in1_count => q_in1_count,
		
		in2_data => q_in2_data,
		in2_send => q_in2_send,
		in2_ack => q_in2_ack,
		in2_count => q_in2_count,
		
		in3_data => q_in3_data,
		in3_send => q_in3_send,
		in3_ack => q_in3_ack,
		in3_count => q_in3_count,
		
		in4_data => q_in4_data,
		in4_send => q_in4_send,
		in4_ack => q_in4_ack,
		in4_count => q_in4_count,
		
		in5_data => q_in5_data,
		in5_send => q_in5_send,
		in5_ack => q_in5_ack,
		in5_count => q_in5_count,
		
		in6_data => q_in6_data,
		in6_send => q_in6_send,
		in6_ack => q_in6_ack,
		in6_count => q_in6_count,
		
		in7_data => q_in7_data,
		in7_send => q_in7_send,
		in7_ack => q_in7_ack,
		in7_count => q_in7_count,
		
		in8_data => q_in8_data,
		in8_send => q_in8_send,
		in8_ack => q_in8_ack,
		in8_count => q_in8_count,
		
		in9_data => q_in9_data,
		in9_send => q_in9_send,
		in9_ack => q_in9_ack,
		in9_count => q_in9_count,
		
		in10_data => q_in10_data,
		in10_send => q_in10_send,
		in10_ack => q_in10_ack,
		in10_count => q_in10_count,
		
		in11_data => q_in11_data,
		in11_send => q_in11_send,
		in11_ack => q_in11_ack,
		in11_count => q_in11_count,
		
		in12_data => q_in12_data,
		in12_send => q_in12_send,
		in12_ack => q_in12_ack,
		in12_count => q_in12_count,
		
		in13_data => q_in13_data,
		in13_send => q_in13_send,
		in13_ack => q_in13_ack,
		in13_count => q_in13_count,
		
		in14_data => q_in14_data,
		in14_send => q_in14_send,
		in14_ack => q_in14_ack,
		in14_count => q_in14_count,
		
		in15_data => q_in15_data,
		in15_send => q_in15_send,
		in15_ack => q_in15_ack,
		in15_count => q_in15_count,
		
		in16_data => q_in16_data,
		in16_send => q_in16_send,
		in16_ack => q_in16_ack,
		in16_count => q_in16_count,
		
		out1_data => out1_data,
		out1_send => out1_send,
		out1_ack => out1_ack,
		out1_rdy => out1_rdy,
		out1_count => out1_count,
		CLK => CLK,
		reset => reset);
	
	-- Input(s) queues
	q_in1 : entity systemBuilder.Queue(behavioral)
	generic map(length => 512, width => 8)
	port map(
		OUT_DATA => q_in1_data,
		OUT_SEND => q_in1_send,
		OUT_ACK => q_in1_ack,
		OUT_COUNT => q_in1_count,
	
		IN_DATA => in1_data,
		IN_SEND => in1_send,
		IN_ACK => in1_ack,
		IN_RDY => in1_rdy,
		IN_COUNT => in1_count,

		CLK => CLK,
		reset => reset);
	
	q_in2 : entity systemBuilder.Queue(behavioral)
	generic map(length => 512, width => 8)
	port map(
		OUT_DATA => q_in2_data,
		OUT_SEND => q_in2_send,
		OUT_ACK => q_in2_ack,
		OUT_COUNT => q_in2_count,
	
		IN_DATA => in2_data,
		IN_SEND => in2_send,
		IN_ACK => in2_ack,
		IN_RDY => in2_rdy,
		IN_COUNT => in2_count,

		CLK => CLK,
		reset => reset);
	
	q_in3 : entity systemBuilder.Queue(behavioral)
	generic map(length => 512, width => 8)
	port map(
		OUT_DATA => q_in3_data,
		OUT_SEND => q_in3_send,
		OUT_ACK => q_in3_ack,
		OUT_COUNT => q_in3_count,
	
		IN_DATA => in3_data,
		IN_SEND => in3_send,
		IN_ACK => in3_ack,
		IN_RDY => in3_rdy,
		IN_COUNT => in3_count,

		CLK => CLK,
		reset => reset);
	
	q_in4 : entity systemBuilder.Queue(behavioral)
	generic map(length => 512, width => 8)
	port map(
		OUT_DATA => q_in4_data,
		OUT_SEND => q_in4_send,
		OUT_ACK => q_in4_ack,
		OUT_COUNT => q_in4_count,
	
		IN_DATA => in4_data,
		IN_SEND => in4_send,
		IN_ACK => in4_ack,
		IN_RDY => in4_rdy,
		IN_COUNT => in4_count,

		CLK => CLK,
		reset => reset);
	
	q_in5 : entity systemBuilder.Queue(behavioral)
	generic map(length => 512, width => 8)
	port map(
		OUT_DATA => q_in5_data,
		OUT_SEND => q_in5_send,
		OUT_ACK => q_in5_ack,
		OUT_COUNT => q_in5_count,
	
		IN_DATA => in5_data,
		IN_SEND => in5_send,
		IN_ACK => in5_ack,
		IN_RDY => in5_rdy,
		IN_COUNT => in5_count,

		CLK => CLK,
		reset => reset);
	
	q_in6 : entity systemBuilder.Queue(behavioral)
	generic map(length => 512, width => 8)
	port map(
		OUT_DATA => q_in6_data,
		OUT_SEND => q_in6_send,
		OUT_ACK => q_in6_ack,
		OUT_COUNT => q_in6_count,
	
		IN_DATA => in6_data,
		IN_SEND => in6_send,
		IN_ACK => in6_ack,
		IN_RDY => in6_rdy,
		IN_COUNT => in6_count,

		CLK => CLK,
		reset => reset);
	
	q_in7 : entity systemBuilder.Queue(behavioral)
	generic map(length => 512, width => 8)
	port map(
		OUT_DATA => q_in7_data,
		OUT_SEND => q_in7_send,
		OUT_ACK => q_in7_ack,
		OUT_COUNT => q_in7_count,
	
		IN_DATA => in7_data,
		IN_SEND => in7_send,
		IN_ACK => in7_ack,
		IN_RDY => in7_rdy,
		IN_COUNT => in7_count,

		CLK => CLK,
		reset => reset);
	
	q_in8 : entity systemBuilder.Queue(behavioral)
	generic map(length => 512, width => 8)
	port map(
		OUT_DATA => q_in8_data,
		OUT_SEND => q_in8_send,
		OUT_ACK => q_in8_ack,
		OUT_COUNT => q_in8_count,
	
		IN_DATA => in8_data,
		IN_SEND => in8_send,
		IN_ACK => in8_ack,
		IN_RDY => in8_rdy,
		IN_COUNT => in8_count,

		CLK => CLK,
		reset => reset);
	
	q_in9 : entity systemBuilder.Queue(behavioral)
	generic map(length => 512, width => 8)
	port map(
		OUT_DATA => q_in9_data,
		OUT_SEND => q_in9_send,
		OUT_ACK => q_in9_ack,
		OUT_COUNT => q_in9_count,
	
		IN_DATA => in9_data,
		IN_SEND => in9_send,
		IN_ACK => in9_ack,
		IN_RDY => in9_rdy,
		IN_COUNT => in9_count,

		CLK => CLK,
		reset => reset);
	
	q_in10 : entity systemBuilder.Queue(behavioral)
	generic map(length => 512, width => 8)
	port map(
		OUT_DATA => q_in10_data,
		OUT_SEND => q_in10_send,
		OUT_ACK => q_in10_ack,
		OUT_COUNT => q_in10_count,
	
		IN_DATA => in10_data,
		IN_SEND => in10_send,
		IN_ACK => in10_ack,
		IN_RDY => in10_rdy,
		IN_COUNT => in10_count,

		CLK => CLK,
		reset => reset);
	
	q_in11 : entity systemBuilder.Queue(behavioral)
	generic map(length => 512, width => 8)
	port map(
		OUT_DATA => q_in11_data,
		OUT_SEND => q_in11_send,
		OUT_ACK => q_in11_ack,
		OUT_COUNT => q_in11_count,
	
		IN_DATA => in11_data,
		IN_SEND => in11_send,
		IN_ACK => in11_ack,
		IN_RDY => in11_rdy,
		IN_COUNT => in11_count,

		CLK => CLK,
		reset => reset);
	
	q_in12 : entity systemBuilder.Queue(behavioral)
	generic map(length => 512, width => 8)
	port map(
		OUT_DATA => q_in12_data,
		OUT_SEND => q_in12_send,
		OUT_ACK => q_in12_ack,
		OUT_COUNT => q_in12_count,
	
		IN_DATA => in12_data,
		IN_SEND => in12_send,
		IN_ACK => in12_ack,
		IN_RDY => in12_rdy,
		IN_COUNT => in12_count,

		CLK => CLK,
		reset => reset);
	
	q_in13 : entity systemBuilder.Queue(behavioral)
	generic map(length => 512, width => 8)
	port map(
		OUT_DATA => q_in13_data,
		OUT_SEND => q_in13_send,
		OUT_ACK => q_in13_ack,
		OUT_COUNT => q_in13_count,
	
		IN_DATA => in13_data,
		IN_SEND => in13_send,
		IN_ACK => in13_ack,
		IN_RDY => in13_rdy,
		IN_COUNT => in13_count,

		CLK => CLK,
		reset => reset);
	
	q_in14 : entity systemBuilder.Queue(behavioral)
	generic map(length => 512, width => 8)
	port map(
		OUT_DATA => q_in14_data,
		OUT_SEND => q_in14_send,
		OUT_ACK => q_in14_ack,
		OUT_COUNT => q_in14_count,
	
		IN_DATA => in14_data,
		IN_SEND => in14_send,
		IN_ACK => in14_ack,
		IN_RDY => in14_rdy,
		IN_COUNT => in14_count,

		CLK => CLK,
		reset => reset);
	
	q_in15 : entity systemBuilder.Queue(behavioral)
	generic map(length => 512, width => 8)
	port map(
		OUT_DATA => q_in15_data,
		OUT_SEND => q_in15_send,
		OUT_ACK => q_in15_ack,
		OUT_COUNT => q_in15_count,
	
		IN_DATA => in15_data,
		IN_SEND => in15_send,
		IN_ACK => in15_ack,
		IN_RDY => in15_rdy,
		IN_COUNT => in15_count,

		CLK => CLK,
		reset => reset);
	
	q_in16 : entity systemBuilder.Queue(behavioral)
	generic map(length => 512, width => 8)
	port map(
		OUT_DATA => q_in16_data,
		OUT_SEND => q_in16_send,
		OUT_ACK => q_in16_ack,
		OUT_COUNT => q_in16_count,
	
		IN_DATA => in16_data,
		IN_SEND => in16_send,
		IN_ACK => in16_ack,
		IN_RDY => in16_rdy,
		IN_COUNT => in16_count,

		CLK => CLK,
		reset => reset);

	-- Clock process
	
	clockProcess : process
	begin
	wait for OFFSET;
		clockLOOP : loop
			CLK <= '0';
			wait for (PERIOD - (PERIOD * DUTY_CYCLE));
			CLK <= '1';
			wait for (PERIOD * DUTY_CYCLE);
		end loop clockLOOP;
	end process;
	
	-- Reset process
	resetProcess : process
	begin
		wait for OFFSET;
		-- reset state for 100 ns.
		RESET <= '1';
		wait for 100 ns;
		RESET <= '0';
		wait;
	end process;

	
	-- Input(s) Waveform Generation
	WaveGen_Proc_In : process (CLK)
		variable Input_bit : integer range 2147483647 downto - 2147483648;
		variable line_number : line;
	begin
		if rising_edge(CLK) then
		-- Input port: in1 Waveform Generation
			case tb_FSM_in1 is
				when after_reset =>
					count <= count + 1;
					if (count = 15) then
						tb_FSM_in1 <= read_file;
						count <= 0;
					end if;
				when read_file =>
					if (not endfile (sim_file_join_in1)) then
						readline(sim_file_join_in1, line_number);
						if (line_number'length > 0 and line_number(1) /= '/') then
							read(line_number, input_bit);
							in1_data <= std_logic_vector(to_unsigned(input_bit, 8));
							in1_send <= '1';
							tb_FSM_in1 <= CheckRead;
						end if;
					end if;
				when CheckRead =>
					if (not endfile (sim_file_join_in1)) and in1_ack = '1' then
						readline(sim_file_join_in1, line_number);
						if (line_number'length > 0 and line_number(1) /= '/') then
							read(line_number, input_bit);
							in1_data <= std_logic_vector(to_unsigned(input_bit, 8));
							in1_send <= '1';
						end if;
					elsif (endfile (sim_file_join_in1)) then
						in1_send <= '0';
					end if;
				when others => null;
			end case;
		
		-- Input port: in2 Waveform Generation
			case tb_FSM_in2 is
				when after_reset =>
					count <= count + 1;
					if (count = 15) then
						tb_FSM_in2 <= read_file;
						count <= 0;
					end if;
				when read_file =>
					if (not endfile (sim_file_join_in2)) then
						readline(sim_file_join_in2, line_number);
						if (line_number'length > 0 and line_number(1) /= '/') then
							read(line_number, input_bit);
							in2_data <= std_logic_vector(to_unsigned(input_bit, 8));
							in2_send <= '1';
							tb_FSM_in2 <= CheckRead;
						end if;
					end if;
				when CheckRead =>
					if (not endfile (sim_file_join_in2)) and in2_ack = '1' then
						readline(sim_file_join_in2, line_number);
						if (line_number'length > 0 and line_number(1) /= '/') then
							read(line_number, input_bit);
							in2_data <= std_logic_vector(to_unsigned(input_bit, 8));
							in2_send <= '1';
						end if;
					elsif (endfile (sim_file_join_in2)) then
						in2_send <= '0';
					end if;
				when others => null;
			end case;
		
		-- Input port: in3 Waveform Generation
			case tb_FSM_in3 is
				when after_reset =>
					count <= count + 1;
					if (count = 15) then
						tb_FSM_in3 <= read_file;
						count <= 0;
					end if;
				when read_file =>
					if (not endfile (sim_file_join_in3)) then
						readline(sim_file_join_in3, line_number);
						if (line_number'length > 0 and line_number(1) /= '/') then
							read(line_number, input_bit);
							in3_data <= std_logic_vector(to_unsigned(input_bit, 8));
							in3_send <= '1';
							tb_FSM_in3 <= CheckRead;
						end if;
					end if;
				when CheckRead =>
					if (not endfile (sim_file_join_in3)) and in3_ack = '1' then
						readline(sim_file_join_in3, line_number);
						if (line_number'length > 0 and line_number(1) /= '/') then
							read(line_number, input_bit);
							in3_data <= std_logic_vector(to_unsigned(input_bit, 8));
							in3_send <= '1';
						end if;
					elsif (endfile (sim_file_join_in3)) then
						in3_send <= '0';
					end if;
				when others => null;
			end case;
		
		-- Input port: in4 Waveform Generation
			case tb_FSM_in4 is
				when after_reset =>
					count <= count + 1;
					if (count = 15) then
						tb_FSM_in4 <= read_file;
						count <= 0;
					end if;
				when read_file =>
					if (not endfile (sim_file_join_in4)) then
						readline(sim_file_join_in4, line_number);
						if (line_number'length > 0 and line_number(1) /= '/') then
							read(line_number, input_bit);
							in4_data <= std_logic_vector(to_unsigned(input_bit, 8));
							in4_send <= '1';
							tb_FSM_in4 <= CheckRead;
						end if;
					end if;
				when CheckRead =>
					if (not endfile (sim_file_join_in4)) and in4_ack = '1' then
						readline(sim_file_join_in4, line_number);
						if (line_number'length > 0 and line_number(1) /= '/') then
							read(line_number, input_bit);
							in4_data <= std_logic_vector(to_unsigned(input_bit, 8));
							in4_send <= '1';
						end if;
					elsif (endfile (sim_file_join_in4)) then
						in4_send <= '0';
					end if;
				when others => null;
			end case;
		
		-- Input port: in5 Waveform Generation
			case tb_FSM_in5 is
				when after_reset =>
					count <= count + 1;
					if (count = 15) then
						tb_FSM_in5 <= read_file;
						count <= 0;
					end if;
				when read_file =>
					if (not endfile (sim_file_join_in5)) then
						readline(sim_file_join_in5, line_number);
						if (line_number'length > 0 and line_number(1) /= '/') then
							read(line_number, input_bit);
							in5_data <= std_logic_vector(to_unsigned(input_bit, 8));
							in5_send <= '1';
							tb_FSM_in5 <= CheckRead;
						end if;
					end if;
				when CheckRead =>
					if (not endfile (sim_file_join_in5)) and in5_ack = '1' then
						readline(sim_file_join_in5, line_number);
						if (line_number'length > 0 and line_number(1) /= '/') then
							read(line_number, input_bit);
							in5_data <= std_logic_vector(to_unsigned(input_bit, 8));
							in5_send <= '1';
						end if;
					elsif (endfile (sim_file_join_in5)) then
						in5_send <= '0';
					end if;
				when others => null;
			end case;
		
		-- Input port: in6 Waveform Generation
			case tb_FSM_in6 is
				when after_reset =>
					count <= count + 1;
					if (count = 15) then
						tb_FSM_in6 <= read_file;
						count <= 0;
					end if;
				when read_file =>
					if (not endfile (sim_file_join_in6)) then
						readline(sim_file_join_in6, line_number);
						if (line_number'length > 0 and line_number(1) /= '/') then
							read(line_number, input_bit);
							in6_data <= std_logic_vector(to_unsigned(input_bit, 8));
							in6_send <= '1';
							tb_FSM_in6 <= CheckRead;
						end if;
					end if;
				when CheckRead =>
					if (not endfile (sim_file_join_in6)) and in6_ack = '1' then
						readline(sim_file_join_in6, line_number);
						if (line_number'length > 0 and line_number(1) /= '/') then
							read(line_number, input_bit);
							in6_data <= std_logic_vector(to_unsigned(input_bit, 8));
							in6_send <= '1';
						end if;
					elsif (endfile (sim_file_join_in6)) then
						in6_send <= '0';
					end if;
				when others => null;
			end case;
		
		-- Input port: in7 Waveform Generation
			case tb_FSM_in7 is
				when after_reset =>
					count <= count + 1;
					if (count = 15) then
						tb_FSM_in7 <= read_file;
						count <= 0;
					end if;
				when read_file =>
					if (not endfile (sim_file_join_in7)) then
						readline(sim_file_join_in7, line_number);
						if (line_number'length > 0 and line_number(1) /= '/') then
							read(line_number, input_bit);
							in7_data <= std_logic_vector(to_unsigned(input_bit, 8));
							in7_send <= '1';
							tb_FSM_in7 <= CheckRead;
						end if;
					end if;
				when CheckRead =>
					if (not endfile (sim_file_join_in7)) and in7_ack = '1' then
						readline(sim_file_join_in7, line_number);
						if (line_number'length > 0 and line_number(1) /= '/') then
							read(line_number, input_bit);
							in7_data <= std_logic_vector(to_unsigned(input_bit, 8));
							in7_send <= '1';
						end if;
					elsif (endfile (sim_file_join_in7)) then
						in7_send <= '0';
					end if;
				when others => null;
			end case;
		
		-- Input port: in8 Waveform Generation
			case tb_FSM_in8 is
				when after_reset =>
					count <= count + 1;
					if (count = 15) then
						tb_FSM_in8 <= read_file;
						count <= 0;
					end if;
				when read_file =>
					if (not endfile (sim_file_join_in8)) then
						readline(sim_file_join_in8, line_number);
						if (line_number'length > 0 and line_number(1) /= '/') then
							read(line_number, input_bit);
							in8_data <= std_logic_vector(to_unsigned(input_bit, 8));
							in8_send <= '1';
							tb_FSM_in8 <= CheckRead;
						end if;
					end if;
				when CheckRead =>
					if (not endfile (sim_file_join_in8)) and in8_ack = '1' then
						readline(sim_file_join_in8, line_number);
						if (line_number'length > 0 and line_number(1) /= '/') then
							read(line_number, input_bit);
							in8_data <= std_logic_vector(to_unsigned(input_bit, 8));
							in8_send <= '1';
						end if;
					elsif (endfile (sim_file_join_in8)) then
						in8_send <= '0';
					end if;
				when others => null;
			end case;
		
		-- Input port: in9 Waveform Generation
			case tb_FSM_in9 is
				when after_reset =>
					count <= count + 1;
					if (count = 15) then
						tb_FSM_in9 <= read_file;
						count <= 0;
					end if;
				when read_file =>
					if (not endfile (sim_file_join_in9)) then
						readline(sim_file_join_in9, line_number);
						if (line_number'length > 0 and line_number(1) /= '/') then
							read(line_number, input_bit);
							in9_data <= std_logic_vector(to_unsigned(input_bit, 8));
							in9_send <= '1';
							tb_FSM_in9 <= CheckRead;
						end if;
					end if;
				when CheckRead =>
					if (not endfile (sim_file_join_in9)) and in9_ack = '1' then
						readline(sim_file_join_in9, line_number);
						if (line_number'length > 0 and line_number(1) /= '/') then
							read(line_number, input_bit);
							in9_data <= std_logic_vector(to_unsigned(input_bit, 8));
							in9_send <= '1';
						end if;
					elsif (endfile (sim_file_join_in9)) then
						in9_send <= '0';
					end if;
				when others => null;
			end case;
		
		-- Input port: in10 Waveform Generation
			case tb_FSM_in10 is
				when after_reset =>
					count <= count + 1;
					if (count = 15) then
						tb_FSM_in10 <= read_file;
						count <= 0;
					end if;
				when read_file =>
					if (not endfile (sim_file_join_in10)) then
						readline(sim_file_join_in10, line_number);
						if (line_number'length > 0 and line_number(1) /= '/') then
							read(line_number, input_bit);
							in10_data <= std_logic_vector(to_unsigned(input_bit, 8));
							in10_send <= '1';
							tb_FSM_in10 <= CheckRead;
						end if;
					end if;
				when CheckRead =>
					if (not endfile (sim_file_join_in10)) and in10_ack = '1' then
						readline(sim_file_join_in10, line_number);
						if (line_number'length > 0 and line_number(1) /= '/') then
							read(line_number, input_bit);
							in10_data <= std_logic_vector(to_unsigned(input_bit, 8));
							in10_send <= '1';
						end if;
					elsif (endfile (sim_file_join_in10)) then
						in10_send <= '0';
					end if;
				when others => null;
			end case;
		
		-- Input port: in11 Waveform Generation
			case tb_FSM_in11 is
				when after_reset =>
					count <= count + 1;
					if (count = 15) then
						tb_FSM_in11 <= read_file;
						count <= 0;
					end if;
				when read_file =>
					if (not endfile (sim_file_join_in11)) then
						readline(sim_file_join_in11, line_number);
						if (line_number'length > 0 and line_number(1) /= '/') then
							read(line_number, input_bit);
							in11_data <= std_logic_vector(to_unsigned(input_bit, 8));
							in11_send <= '1';
							tb_FSM_in11 <= CheckRead;
						end if;
					end if;
				when CheckRead =>
					if (not endfile (sim_file_join_in11)) and in11_ack = '1' then
						readline(sim_file_join_in11, line_number);
						if (line_number'length > 0 and line_number(1) /= '/') then
							read(line_number, input_bit);
							in11_data <= std_logic_vector(to_unsigned(input_bit, 8));
							in11_send <= '1';
						end if;
					elsif (endfile (sim_file_join_in11)) then
						in11_send <= '0';
					end if;
				when others => null;
			end case;
		
		-- Input port: in12 Waveform Generation
			case tb_FSM_in12 is
				when after_reset =>
					count <= count + 1;
					if (count = 15) then
						tb_FSM_in12 <= read_file;
						count <= 0;
					end if;
				when read_file =>
					if (not endfile (sim_file_join_in12)) then
						readline(sim_file_join_in12, line_number);
						if (line_number'length > 0 and line_number(1) /= '/') then
							read(line_number, input_bit);
							in12_data <= std_logic_vector(to_unsigned(input_bit, 8));
							in12_send <= '1';
							tb_FSM_in12 <= CheckRead;
						end if;
					end if;
				when CheckRead =>
					if (not endfile (sim_file_join_in12)) and in12_ack = '1' then
						readline(sim_file_join_in12, line_number);
						if (line_number'length > 0 and line_number(1) /= '/') then
							read(line_number, input_bit);
							in12_data <= std_logic_vector(to_unsigned(input_bit, 8));
							in12_send <= '1';
						end if;
					elsif (endfile (sim_file_join_in12)) then
						in12_send <= '0';
					end if;
				when others => null;
			end case;
		
		-- Input port: in13 Waveform Generation
			case tb_FSM_in13 is
				when after_reset =>
					count <= count + 1;
					if (count = 15) then
						tb_FSM_in13 <= read_file;
						count <= 0;
					end if;
				when read_file =>
					if (not endfile (sim_file_join_in13)) then
						readline(sim_file_join_in13, line_number);
						if (line_number'length > 0 and line_number(1) /= '/') then
							read(line_number, input_bit);
							in13_data <= std_logic_vector(to_unsigned(input_bit, 8));
							in13_send <= '1';
							tb_FSM_in13 <= CheckRead;
						end if;
					end if;
				when CheckRead =>
					if (not endfile (sim_file_join_in13)) and in13_ack = '1' then
						readline(sim_file_join_in13, line_number);
						if (line_number'length > 0 and line_number(1) /= '/') then
							read(line_number, input_bit);
							in13_data <= std_logic_vector(to_unsigned(input_bit, 8));
							in13_send <= '1';
						end if;
					elsif (endfile (sim_file_join_in13)) then
						in13_send <= '0';
					end if;
				when others => null;
			end case;
		
		-- Input port: in14 Waveform Generation
			case tb_FSM_in14 is
				when after_reset =>
					count <= count + 1;
					if (count = 15) then
						tb_FSM_in14 <= read_file;
						count <= 0;
					end if;
				when read_file =>
					if (not endfile (sim_file_join_in14)) then
						readline(sim_file_join_in14, line_number);
						if (line_number'length > 0 and line_number(1) /= '/') then
							read(line_number, input_bit);
							in14_data <= std_logic_vector(to_unsigned(input_bit, 8));
							in14_send <= '1';
							tb_FSM_in14 <= CheckRead;
						end if;
					end if;
				when CheckRead =>
					if (not endfile (sim_file_join_in14)) and in14_ack = '1' then
						readline(sim_file_join_in14, line_number);
						if (line_number'length > 0 and line_number(1) /= '/') then
							read(line_number, input_bit);
							in14_data <= std_logic_vector(to_unsigned(input_bit, 8));
							in14_send <= '1';
						end if;
					elsif (endfile (sim_file_join_in14)) then
						in14_send <= '0';
					end if;
				when others => null;
			end case;
		
		-- Input port: in15 Waveform Generation
			case tb_FSM_in15 is
				when after_reset =>
					count <= count + 1;
					if (count = 15) then
						tb_FSM_in15 <= read_file;
						count <= 0;
					end if;
				when read_file =>
					if (not endfile (sim_file_join_in15)) then
						readline(sim_file_join_in15, line_number);
						if (line_number'length > 0 and line_number(1) /= '/') then
							read(line_number, input_bit);
							in15_data <= std_logic_vector(to_unsigned(input_bit, 8));
							in15_send <= '1';
							tb_FSM_in15 <= CheckRead;
						end if;
					end if;
				when CheckRead =>
					if (not endfile (sim_file_join_in15)) and in15_ack = '1' then
						readline(sim_file_join_in15, line_number);
						if (line_number'length > 0 and line_number(1) /= '/') then
							read(line_number, input_bit);
							in15_data <= std_logic_vector(to_unsigned(input_bit, 8));
							in15_send <= '1';
						end if;
					elsif (endfile (sim_file_join_in15)) then
						in15_send <= '0';
					end if;
				when others => null;
			end case;
		
		-- Input port: in16 Waveform Generation
			case tb_FSM_in16 is
				when after_reset =>
					count <= count + 1;
					if (count = 15) then
						tb_FSM_in16 <= read_file;
						count <= 0;
					end if;
				when read_file =>
					if (not endfile (sim_file_join_in16)) then
						readline(sim_file_join_in16, line_number);
						if (line_number'length > 0 and line_number(1) /= '/') then
							read(line_number, input_bit);
							in16_data <= std_logic_vector(to_unsigned(input_bit, 8));
							in16_send <= '1';
							tb_FSM_in16 <= CheckRead;
						end if;
					end if;
				when CheckRead =>
					if (not endfile (sim_file_join_in16)) and in16_ack = '1' then
						readline(sim_file_join_in16, line_number);
						if (line_number'length > 0 and line_number(1) /= '/') then
							read(line_number, input_bit);
							in16_data <= std_logic_vector(to_unsigned(input_bit, 8));
							in16_send <= '1';
						end if;
					elsif (endfile (sim_file_join_in16)) then
						in16_send <= '0';
					end if;
				when others => null;
			end case;
		end if;
	end process WaveGen_Proc_In;
	
	-- Output(s) waveform Generation
	out1_ack <= out1_send;
	out1_rdy <= '1';
	
	WaveGen_Proc_Out : process (CLK)
		variable Input_bit   : integer range 2147483647 downto - 2147483648;
		variable line_number : line;
		variable sequence_out1 : integer := 0;
	begin
		if (rising_edge(CLK)) then
		-- Output port: out1 Waveform Generation
			if (not endfile (sim_file_join_out1) and out1_send = '1') then
				readline(sim_file_join_out1, line_number);
					if (line_number'length > 0 and line_number(1) /= '/') then
						read(line_number, input_bit);
						assert (out1_data  = std_logic_vector(to_unsigned(input_bit, 8)))
						report "on port out1 incorrect value computed : " & str(to_integer(unsigned(out1_data))) & " instead of : " & str(input_bit) & " sequence " & str(sequence_out1)
						severity failure;
						
						assert (out1_data /= std_logic_vector(to_unsigned(input_bit, 8)))
						report "on port out1 correct value computed : " & str(to_integer(unsigned(out1_data))) & " equals : " & str(input_bit) & " sequence " & str(sequence_out1)
						severity note;
						sequence_out1 := sequence_out1 + 1;
					end if;
			end if;
		end if;			
	end process WaveGen_Proc_Out;
	
end architecture arch_join_tb; 
