-- ----------------------------------------------------------------------------
-- __  ___ __ ___  _ __   ___  ___ 
-- \ \/ / '__/ _ \| '_ \ / _ \/ __|
--  >  <| | | (_) | | | | (_) \__ \
-- /_/\_\_|  \___/|_| |_|\___/|___/
-- ----------------------------------------------------------------------------
-- Xronos synthesizer
-- Testbench for Network: medianParallel16 
-- Date: 2018/02/23 14:28:55
-- ----------------------------------------------------------------------------

library ieee, SystemBuilder;
use ieee.std_logic_1164.all;
use ieee.std_logic_unsigned.all;
use ieee.numeric_std.all;
use std.textio.all;

library work;
use work.sim_package.all;

entity medianParallel16_tb is
end medianParallel16_tb;

architecture arch_medianParallel16_tb of medianParallel16_tb is
	-----------------------------------------------------------------------
	-- Component declaration
	-----------------------------------------------------------------------
	component medianParallel16
	port(
	    inPort_data : IN std_logic_vector(7 downto 0);
	    inPort_send : IN std_logic;
	    inPort_ack : OUT std_logic;
	    inPort_count : IN std_logic_vector(15 downto 0);
	    outPort_data : OUT std_logic_vector(7 downto 0);
	    outPort_send : OUT std_logic;
	    outPort_ack : IN std_logic;
	    outPort_rdy : IN std_logic;
	    outPort_count : OUT std_logic_vector(15 downto 0);
	    CLK : in std_logic;
	    RESET: IN std_logic);
	end component medianParallel16;
	
		-----------------------------------------------------------------------
		-- Achitecure signals & constants
		-----------------------------------------------------------------------
		constant CLK_PERIOD : time := 100 ns;
		constant CLK_DUTY_CYCLE : real := 0.5;
		constant OFFSET : time := 100 ns;
		-- Severity level and testbench type types
		type severity_level is (note, warning, error, failure);
		type tb_type is (after_reset, read_file, CheckRead);
		
		-- Component input(s) signals
		signal tb_FSM_inPort : tb_type;
		file sim_file_medianParallel16_inPort : text is "fifoTraces/medianParallel16_inPort.txt";
		signal inPort_data : std_logic_vector(7 downto 0) := (others => '0');
		signal inPort_send : std_logic := '0';
		signal inPort_ack : std_logic;
		signal inPort_rdy : std_logic;
		signal inPort_count : std_logic_vector(15 downto 0) := (others => '0');
		-- Input component queue
		signal q_inPort_data : std_logic_vector(7 downto 0) := (others => '0');
		signal q_inPort_send : std_logic := '0';
		signal q_inPort_ack : std_logic;
		signal q_inPort_rdy : std_logic;
		signal q_inPort_count : std_logic_vector(15 downto 0) := (others => '0');
		
		-- Component Output(s) signals
		signal tb_FSM_outPort : tb_type;
		file sim_file_medianParallel16_outPort : text is "fifoTraces/medianParallel16_outPort.txt";
		signal outPort_data : std_logic_vector(7 downto 0) := (others => '0');
		signal outPort_send : std_logic;
		signal outPort_ack : std_logic := '0';
		signal outPort_rdy : std_logic := '0';
		signal outPort_count : std_logic_vector(15 downto 0) := (others => '0');
		
	
		-- GoDone Weights Output Files
		
		signal count : integer range 255 downto 0 := 0;
		signal CLK : std_logic := '0';
		signal reset : std_logic := '0';
		
begin
	
	i_medianParallel16 : medianParallel16 
	port map(
		inPort_data => q_inPort_data,
		inPort_send => q_inPort_send,
		inPort_ack => q_inPort_ack,
		inPort_count => q_inPort_count,
		
		outPort_data => outPort_data,
		outPort_send => outPort_send,
		outPort_ack => outPort_ack,
		outPort_rdy => outPort_rdy,
		outPort_count => outPort_count,
		CLK => CLK,
		reset => reset);
	
	-- Input(s) queues
	q_inPort : entity systemBuilder.Queue(behavioral)
	generic map(length => 512, width => 8)
	port map(
		OUT_DATA => q_inPort_data,
		OUT_SEND => q_inPort_send,
		OUT_ACK => q_inPort_ack,
		OUT_COUNT => q_inPort_count,
	
		IN_DATA => inPort_data,
		IN_SEND => inPort_send,
		IN_ACK => inPort_ack,
		IN_RDY => inPort_rdy,
		IN_COUNT => inPort_count,

		CLK => CLK,
		reset => reset);

	-- Clock process
	
	CLK_clockProcess : process
		begin
		wait for OFFSET;
			clockLOOP : loop
				CLK <= '0';
				wait for (CLK_PERIOD - (CLK_PERIOD * CLK_DUTY_CYCLE));
				CLK <= '1';
				wait for (CLK_PERIOD * CLK_DUTY_CYCLE);
			end loop clockLOOP;
	end process;
	
	-- Reset process
	resetProcess : process
	begin
		wait for OFFSET;
		-- reset state for 100 ns.
		RESET <= '1';
		wait for 100 ns;
		RESET <= '0';
		wait;
	end process;

	
	-- Input(s) Waveform Generation
	WaveGen_Proc_In : process (CLK)
		variable Input_bit : integer range 2147483647 downto - 2147483648;
		variable line_number : line;
	begin
		if rising_edge(CLK) then
		-- Input port: inPort Waveform Generation
			case tb_FSM_inPort is
				when after_reset =>
					count <= count + 1;
					if (count = 15) then
						tb_FSM_inPort <= read_file;
						count <= 0;
					end if;
				when read_file =>
					if (not endfile (sim_file_medianParallel16_inPort)) then
						readline(sim_file_medianParallel16_inPort, line_number);
						if (line_number'length > 0 and line_number(1) /= '/') then
							read(line_number, input_bit);
							inPort_data <= std_logic_vector(to_unsigned(input_bit, 8));
							inPort_send <= '1';
							tb_FSM_inPort <= CheckRead;
						end if;
					end if;
				when CheckRead =>
					if (not endfile (sim_file_medianParallel16_inPort)) and inPort_ack = '1' then
						readline(sim_file_medianParallel16_inPort, line_number);
						if (line_number'length > 0 and line_number(1) /= '/') then
							read(line_number, input_bit);
							inPort_data <= std_logic_vector(to_unsigned(input_bit, 8));
							inPort_send <= '1';
						end if;
					elsif (endfile (sim_file_medianParallel16_inPort)) then
						inPort_send <= '0';
					end if;
				when others => null;
			end case;
		end if;
	end process WaveGen_Proc_In;
	
	-- Output(s) waveform Generation
	outPort_ack <= outPort_send;
	outPort_rdy <= '1';
	
	WaveGen_Proc_Out : process (CLK)
		variable Input_bit   : integer range 2147483647 downto - 2147483648;
		variable line_number : line;
		variable sequence_outPort : integer := 0;
	begin
		if (rising_edge(CLK)) then
		-- Output port: outPort Waveform Generation
			if (not endfile (sim_file_medianParallel16_outPort) and outPort_send = '1') then
				readline(sim_file_medianParallel16_outPort, line_number);
					if (line_number'length > 0 and line_number(1) /= '/') then
						read(line_number, input_bit);
						assert (outPort_data  = std_logic_vector(to_unsigned(input_bit, 8)))
						report "on port outPort incorrect value computed : " & str(to_integer(unsigned(outPort_data))) & " instead of : " & str(input_bit) & " sequence " & str(sequence_outPort)
						severity failure;
						
						assert (outPort_data /= std_logic_vector(to_unsigned(input_bit, 8)))
						report "on port outPort correct value computed : " & str(to_integer(unsigned(outPort_data))) & " equals : " & str(input_bit) & " sequence " & str(sequence_outPort)
						severity note;
						sequence_outPort := sequence_outPort + 1;
					end if;
			end if;
		end if;			
	end process WaveGen_Proc_Out;
	
end architecture arch_medianParallel16_tb; 
