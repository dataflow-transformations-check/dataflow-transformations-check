-- ----------------------------------------------------------------------------
-- __  ___ __ ___  _ __   ___  ___ 
-- \ \/ / '__/ _ \| '_ \ / _ \/ __|
--  >  <| | | (_) | | | | (_) \__ \
-- /_/\_\_|  \___/|_| |_|\___/|___/
-- ----------------------------------------------------------------------------
-- Xronos synthesizer
-- Testbench for Instance: medianRow15 
-- Date: 2018/02/23 14:28:42
-- ----------------------------------------------------------------------------

library ieee, SystemBuilder;
use ieee.std_logic_1164.all;
use ieee.std_logic_unsigned.all;
use ieee.numeric_std.all;
use std.textio.all;

library work;
use work.sim_package.all;

entity medianRow15_tb is
end medianRow15_tb;

architecture arch_medianRow15_tb of medianRow15_tb is
	-----------------------------------------------------------------------
	-- Component declaration
	-----------------------------------------------------------------------
	component medianRow15
	port(
	    in1_data : IN std_logic_vector(7 downto 0);
	    in1_send : IN std_logic;
	    in1_ack : OUT std_logic;
	    in1_count : IN std_logic_vector(15 downto 0);
	    median_data : OUT std_logic_vector(7 downto 0);
	    median_send : OUT std_logic;
	    median_ack : IN std_logic;
	    median_rdy : IN std_logic;
	    median_count : OUT std_logic_vector(15 downto 0);
	    CLK: IN std_logic;
	    RESET: IN std_logic);
	end component medianRow15;
	
		-----------------------------------------------------------------------
		-- Achitecure signals & constants
		-----------------------------------------------------------------------
		constant PERIOD : time := 100 ns;
		constant DUTY_CYCLE : real := 0.5;
		constant OFFSET : time := 100 ns;
		-- Severity level and testbench type types
		type severity_level is (note, warning, error, failure);
		type tb_type is (after_reset, read_file, CheckRead);
		
		-- Component input(s) signals
		signal tb_FSM_in1 : tb_type;
		file sim_file_medianRow15_in1 : text is "fifoTraces/medianRow15_in1.txt";
		signal in1_data : std_logic_vector(7 downto 0) := (others => '0');
		signal in1_send : std_logic := '0';
		signal in1_ack : std_logic;
		signal in1_rdy : std_logic;
		signal in1_count : std_logic_vector(15 downto 0) := (others => '0');
		-- Input component queue
		signal q_in1_data : std_logic_vector(7 downto 0) := (others => '0');
		signal q_in1_send : std_logic := '0';
		signal q_in1_ack : std_logic;
		signal q_in1_rdy : std_logic;
		signal q_in1_count : std_logic_vector(15 downto 0) := (others => '0');
		
		-- Component Output(s) signals
		signal tb_FSM_median : tb_type;
		file sim_file_medianRow15_median : text is "fifoTraces/medianRow15_median.txt";
		signal median_data : std_logic_vector(7 downto 0) := (others => '0');
		signal median_send : std_logic;
		signal median_ack : std_logic := '0';
		signal median_rdy : std_logic := '0';
		signal median_count : std_logic_vector(15 downto 0) := (others => '0');
		
	
		-- GoDone Weights Output Files
		
		signal count : integer range 255 downto 0 := 0;
		signal CLK : std_logic := '0';
		signal reset : std_logic := '0';
		
begin
	
	i_medianRow15 : medianRow15 
	port map(
		in1_data => q_in1_data,
		in1_send => q_in1_send,
		in1_ack => q_in1_ack,
		in1_count => q_in1_count,
		
		median_data => median_data,
		median_send => median_send,
		median_ack => median_ack,
		median_rdy => median_rdy,
		median_count => median_count,
		CLK => CLK,
		reset => reset);
	
	-- Input(s) queues
	q_in1 : entity systemBuilder.Queue(behavioral)
	generic map(length => 512, width => 8)
	port map(
		OUT_DATA => q_in1_data,
		OUT_SEND => q_in1_send,
		OUT_ACK => q_in1_ack,
		OUT_COUNT => q_in1_count,
	
		IN_DATA => in1_data,
		IN_SEND => in1_send,
		IN_ACK => in1_ack,
		IN_RDY => in1_rdy,
		IN_COUNT => in1_count,

		CLK => CLK,
		reset => reset);

	-- Clock process
	
	clockProcess : process
	begin
	wait for OFFSET;
		clockLOOP : loop
			CLK <= '0';
			wait for (PERIOD - (PERIOD * DUTY_CYCLE));
			CLK <= '1';
			wait for (PERIOD * DUTY_CYCLE);
		end loop clockLOOP;
	end process;
	
	-- Reset process
	resetProcess : process
	begin
		wait for OFFSET;
		-- reset state for 100 ns.
		RESET <= '1';
		wait for 100 ns;
		RESET <= '0';
		wait;
	end process;

	
	-- Input(s) Waveform Generation
	WaveGen_Proc_In : process (CLK)
		variable Input_bit : integer range 2147483647 downto - 2147483648;
		variable line_number : line;
	begin
		if rising_edge(CLK) then
		-- Input port: in1 Waveform Generation
			case tb_FSM_in1 is
				when after_reset =>
					count <= count + 1;
					if (count = 15) then
						tb_FSM_in1 <= read_file;
						count <= 0;
					end if;
				when read_file =>
					if (not endfile (sim_file_medianRow15_in1)) then
						readline(sim_file_medianRow15_in1, line_number);
						if (line_number'length > 0 and line_number(1) /= '/') then
							read(line_number, input_bit);
							in1_data <= std_logic_vector(to_unsigned(input_bit, 8));
							in1_send <= '1';
							tb_FSM_in1 <= CheckRead;
						end if;
					end if;
				when CheckRead =>
					if (not endfile (sim_file_medianRow15_in1)) and in1_ack = '1' then
						readline(sim_file_medianRow15_in1, line_number);
						if (line_number'length > 0 and line_number(1) /= '/') then
							read(line_number, input_bit);
							in1_data <= std_logic_vector(to_unsigned(input_bit, 8));
							in1_send <= '1';
						end if;
					elsif (endfile (sim_file_medianRow15_in1)) then
						in1_send <= '0';
					end if;
				when others => null;
			end case;
		end if;
	end process WaveGen_Proc_In;
	
	-- Output(s) waveform Generation
	median_ack <= median_send;
	median_rdy <= '1';
	
	WaveGen_Proc_Out : process (CLK)
		variable Input_bit   : integer range 2147483647 downto - 2147483648;
		variable line_number : line;
		variable sequence_median : integer := 0;
	begin
		if (rising_edge(CLK)) then
		-- Output port: median Waveform Generation
			if (not endfile (sim_file_medianRow15_median) and median_send = '1') then
				readline(sim_file_medianRow15_median, line_number);
					if (line_number'length > 0 and line_number(1) /= '/') then
						read(line_number, input_bit);
						assert (median_data  = std_logic_vector(to_unsigned(input_bit, 8)))
						report "on port median incorrect value computed : " & str(to_integer(unsigned(median_data))) & " instead of : " & str(input_bit) & " sequence " & str(sequence_median)
						severity failure;
						
						assert (median_data /= std_logic_vector(to_unsigned(input_bit, 8)))
						report "on port median correct value computed : " & str(to_integer(unsigned(median_data))) & " equals : " & str(input_bit) & " sequence " & str(sequence_median)
						severity note;
						sequence_median := sequence_median + 1;
					end if;
			end if;
		end if;			
	end process WaveGen_Proc_Out;
	
end architecture arch_medianRow15_tb; 
