-- ----------------------------------------------------------------------------
-- __  ___ __ ___  _ __   ___  ___ 
-- \ \/ / '__/ _ \| '_ \ / _ \/ __|
--  >  <| | | (_) | | | | (_) \__ \
-- /_/\_\_|  \___/|_| |_|\___/|___/
-- ----------------------------------------------------------------------------
-- Xronos synthesizer
-- Testbench for Instance: split 
-- Date: 2018/02/23 14:28:35
-- ----------------------------------------------------------------------------

library ieee, SystemBuilder;
use ieee.std_logic_1164.all;
use ieee.std_logic_unsigned.all;
use ieee.numeric_std.all;
use std.textio.all;

library work;
use work.sim_package.all;

entity split_tb is
end split_tb;

architecture arch_split_tb of split_tb is
	-----------------------------------------------------------------------
	-- Component declaration
	-----------------------------------------------------------------------
	component split
	port(
	    in1_data : IN std_logic_vector(7 downto 0);
	    in1_send : IN std_logic;
	    in1_ack : OUT std_logic;
	    in1_count : IN std_logic_vector(15 downto 0);
	    out1_data : OUT std_logic_vector(7 downto 0);
	    out1_send : OUT std_logic;
	    out1_ack : IN std_logic;
	    out1_rdy : IN std_logic;
	    out1_count : OUT std_logic_vector(15 downto 0);
	    out2_data : OUT std_logic_vector(7 downto 0);
	    out2_send : OUT std_logic;
	    out2_ack : IN std_logic;
	    out2_rdy : IN std_logic;
	    out2_count : OUT std_logic_vector(15 downto 0);
	    out3_data : OUT std_logic_vector(7 downto 0);
	    out3_send : OUT std_logic;
	    out3_ack : IN std_logic;
	    out3_rdy : IN std_logic;
	    out3_count : OUT std_logic_vector(15 downto 0);
	    out4_data : OUT std_logic_vector(7 downto 0);
	    out4_send : OUT std_logic;
	    out4_ack : IN std_logic;
	    out4_rdy : IN std_logic;
	    out4_count : OUT std_logic_vector(15 downto 0);
	    out5_data : OUT std_logic_vector(7 downto 0);
	    out5_send : OUT std_logic;
	    out5_ack : IN std_logic;
	    out5_rdy : IN std_logic;
	    out5_count : OUT std_logic_vector(15 downto 0);
	    out6_data : OUT std_logic_vector(7 downto 0);
	    out6_send : OUT std_logic;
	    out6_ack : IN std_logic;
	    out6_rdy : IN std_logic;
	    out6_count : OUT std_logic_vector(15 downto 0);
	    out7_data : OUT std_logic_vector(7 downto 0);
	    out7_send : OUT std_logic;
	    out7_ack : IN std_logic;
	    out7_rdy : IN std_logic;
	    out7_count : OUT std_logic_vector(15 downto 0);
	    out8_data : OUT std_logic_vector(7 downto 0);
	    out8_send : OUT std_logic;
	    out8_ack : IN std_logic;
	    out8_rdy : IN std_logic;
	    out8_count : OUT std_logic_vector(15 downto 0);
	    out9_data : OUT std_logic_vector(7 downto 0);
	    out9_send : OUT std_logic;
	    out9_ack : IN std_logic;
	    out9_rdy : IN std_logic;
	    out9_count : OUT std_logic_vector(15 downto 0);
	    out10_data : OUT std_logic_vector(7 downto 0);
	    out10_send : OUT std_logic;
	    out10_ack : IN std_logic;
	    out10_rdy : IN std_logic;
	    out10_count : OUT std_logic_vector(15 downto 0);
	    out11_data : OUT std_logic_vector(7 downto 0);
	    out11_send : OUT std_logic;
	    out11_ack : IN std_logic;
	    out11_rdy : IN std_logic;
	    out11_count : OUT std_logic_vector(15 downto 0);
	    out12_data : OUT std_logic_vector(7 downto 0);
	    out12_send : OUT std_logic;
	    out12_ack : IN std_logic;
	    out12_rdy : IN std_logic;
	    out12_count : OUT std_logic_vector(15 downto 0);
	    out13_data : OUT std_logic_vector(7 downto 0);
	    out13_send : OUT std_logic;
	    out13_ack : IN std_logic;
	    out13_rdy : IN std_logic;
	    out13_count : OUT std_logic_vector(15 downto 0);
	    out14_data : OUT std_logic_vector(7 downto 0);
	    out14_send : OUT std_logic;
	    out14_ack : IN std_logic;
	    out14_rdy : IN std_logic;
	    out14_count : OUT std_logic_vector(15 downto 0);
	    out15_data : OUT std_logic_vector(7 downto 0);
	    out15_send : OUT std_logic;
	    out15_ack : IN std_logic;
	    out15_rdy : IN std_logic;
	    out15_count : OUT std_logic_vector(15 downto 0);
	    out16_data : OUT std_logic_vector(7 downto 0);
	    out16_send : OUT std_logic;
	    out16_ack : IN std_logic;
	    out16_rdy : IN std_logic;
	    out16_count : OUT std_logic_vector(15 downto 0);
	    CLK: IN std_logic;
	    RESET: IN std_logic);
	end component split;
	
		-----------------------------------------------------------------------
		-- Achitecure signals & constants
		-----------------------------------------------------------------------
		constant PERIOD : time := 100 ns;
		constant DUTY_CYCLE : real := 0.5;
		constant OFFSET : time := 100 ns;
		-- Severity level and testbench type types
		type severity_level is (note, warning, error, failure);
		type tb_type is (after_reset, read_file, CheckRead);
		
		-- Component input(s) signals
		signal tb_FSM_in1 : tb_type;
		file sim_file_split_in1 : text is "fifoTraces/split_in1.txt";
		signal in1_data : std_logic_vector(7 downto 0) := (others => '0');
		signal in1_send : std_logic := '0';
		signal in1_ack : std_logic;
		signal in1_rdy : std_logic;
		signal in1_count : std_logic_vector(15 downto 0) := (others => '0');
		-- Input component queue
		signal q_in1_data : std_logic_vector(7 downto 0) := (others => '0');
		signal q_in1_send : std_logic := '0';
		signal q_in1_ack : std_logic;
		signal q_in1_rdy : std_logic;
		signal q_in1_count : std_logic_vector(15 downto 0) := (others => '0');
		
		-- Component Output(s) signals
		signal tb_FSM_out1 : tb_type;
		file sim_file_split_out1 : text is "fifoTraces/split_out1.txt";
		signal out1_data : std_logic_vector(7 downto 0) := (others => '0');
		signal out1_send : std_logic;
		signal out1_ack : std_logic := '0';
		signal out1_rdy : std_logic := '0';
		signal out1_count : std_logic_vector(15 downto 0) := (others => '0');
		signal tb_FSM_out2 : tb_type;
		file sim_file_split_out2 : text is "fifoTraces/split_out2.txt";
		signal out2_data : std_logic_vector(7 downto 0) := (others => '0');
		signal out2_send : std_logic;
		signal out2_ack : std_logic := '0';
		signal out2_rdy : std_logic := '0';
		signal out2_count : std_logic_vector(15 downto 0) := (others => '0');
		signal tb_FSM_out3 : tb_type;
		file sim_file_split_out3 : text is "fifoTraces/split_out3.txt";
		signal out3_data : std_logic_vector(7 downto 0) := (others => '0');
		signal out3_send : std_logic;
		signal out3_ack : std_logic := '0';
		signal out3_rdy : std_logic := '0';
		signal out3_count : std_logic_vector(15 downto 0) := (others => '0');
		signal tb_FSM_out4 : tb_type;
		file sim_file_split_out4 : text is "fifoTraces/split_out4.txt";
		signal out4_data : std_logic_vector(7 downto 0) := (others => '0');
		signal out4_send : std_logic;
		signal out4_ack : std_logic := '0';
		signal out4_rdy : std_logic := '0';
		signal out4_count : std_logic_vector(15 downto 0) := (others => '0');
		signal tb_FSM_out5 : tb_type;
		file sim_file_split_out5 : text is "fifoTraces/split_out5.txt";
		signal out5_data : std_logic_vector(7 downto 0) := (others => '0');
		signal out5_send : std_logic;
		signal out5_ack : std_logic := '0';
		signal out5_rdy : std_logic := '0';
		signal out5_count : std_logic_vector(15 downto 0) := (others => '0');
		signal tb_FSM_out6 : tb_type;
		file sim_file_split_out6 : text is "fifoTraces/split_out6.txt";
		signal out6_data : std_logic_vector(7 downto 0) := (others => '0');
		signal out6_send : std_logic;
		signal out6_ack : std_logic := '0';
		signal out6_rdy : std_logic := '0';
		signal out6_count : std_logic_vector(15 downto 0) := (others => '0');
		signal tb_FSM_out7 : tb_type;
		file sim_file_split_out7 : text is "fifoTraces/split_out7.txt";
		signal out7_data : std_logic_vector(7 downto 0) := (others => '0');
		signal out7_send : std_logic;
		signal out7_ack : std_logic := '0';
		signal out7_rdy : std_logic := '0';
		signal out7_count : std_logic_vector(15 downto 0) := (others => '0');
		signal tb_FSM_out8 : tb_type;
		file sim_file_split_out8 : text is "fifoTraces/split_out8.txt";
		signal out8_data : std_logic_vector(7 downto 0) := (others => '0');
		signal out8_send : std_logic;
		signal out8_ack : std_logic := '0';
		signal out8_rdy : std_logic := '0';
		signal out8_count : std_logic_vector(15 downto 0) := (others => '0');
		signal tb_FSM_out9 : tb_type;
		file sim_file_split_out9 : text is "fifoTraces/split_out9.txt";
		signal out9_data : std_logic_vector(7 downto 0) := (others => '0');
		signal out9_send : std_logic;
		signal out9_ack : std_logic := '0';
		signal out9_rdy : std_logic := '0';
		signal out9_count : std_logic_vector(15 downto 0) := (others => '0');
		signal tb_FSM_out10 : tb_type;
		file sim_file_split_out10 : text is "fifoTraces/split_out10.txt";
		signal out10_data : std_logic_vector(7 downto 0) := (others => '0');
		signal out10_send : std_logic;
		signal out10_ack : std_logic := '0';
		signal out10_rdy : std_logic := '0';
		signal out10_count : std_logic_vector(15 downto 0) := (others => '0');
		signal tb_FSM_out11 : tb_type;
		file sim_file_split_out11 : text is "fifoTraces/split_out11.txt";
		signal out11_data : std_logic_vector(7 downto 0) := (others => '0');
		signal out11_send : std_logic;
		signal out11_ack : std_logic := '0';
		signal out11_rdy : std_logic := '0';
		signal out11_count : std_logic_vector(15 downto 0) := (others => '0');
		signal tb_FSM_out12 : tb_type;
		file sim_file_split_out12 : text is "fifoTraces/split_out12.txt";
		signal out12_data : std_logic_vector(7 downto 0) := (others => '0');
		signal out12_send : std_logic;
		signal out12_ack : std_logic := '0';
		signal out12_rdy : std_logic := '0';
		signal out12_count : std_logic_vector(15 downto 0) := (others => '0');
		signal tb_FSM_out13 : tb_type;
		file sim_file_split_out13 : text is "fifoTraces/split_out13.txt";
		signal out13_data : std_logic_vector(7 downto 0) := (others => '0');
		signal out13_send : std_logic;
		signal out13_ack : std_logic := '0';
		signal out13_rdy : std_logic := '0';
		signal out13_count : std_logic_vector(15 downto 0) := (others => '0');
		signal tb_FSM_out14 : tb_type;
		file sim_file_split_out14 : text is "fifoTraces/split_out14.txt";
		signal out14_data : std_logic_vector(7 downto 0) := (others => '0');
		signal out14_send : std_logic;
		signal out14_ack : std_logic := '0';
		signal out14_rdy : std_logic := '0';
		signal out14_count : std_logic_vector(15 downto 0) := (others => '0');
		signal tb_FSM_out15 : tb_type;
		file sim_file_split_out15 : text is "fifoTraces/split_out15.txt";
		signal out15_data : std_logic_vector(7 downto 0) := (others => '0');
		signal out15_send : std_logic;
		signal out15_ack : std_logic := '0';
		signal out15_rdy : std_logic := '0';
		signal out15_count : std_logic_vector(15 downto 0) := (others => '0');
		signal tb_FSM_out16 : tb_type;
		file sim_file_split_out16 : text is "fifoTraces/split_out16.txt";
		signal out16_data : std_logic_vector(7 downto 0) := (others => '0');
		signal out16_send : std_logic;
		signal out16_ack : std_logic := '0';
		signal out16_rdy : std_logic := '0';
		signal out16_count : std_logic_vector(15 downto 0) := (others => '0');
		
	
		-- GoDone Weights Output Files
		
		signal count : integer range 255 downto 0 := 0;
		signal CLK : std_logic := '0';
		signal reset : std_logic := '0';
		
begin
	
	i_split : split 
	port map(
		in1_data => q_in1_data,
		in1_send => q_in1_send,
		in1_ack => q_in1_ack,
		in1_count => q_in1_count,
		
		out1_data => out1_data,
		out1_send => out1_send,
		out1_ack => out1_ack,
		out1_rdy => out1_rdy,
		out1_count => out1_count,
		
		out2_data => out2_data,
		out2_send => out2_send,
		out2_ack => out2_ack,
		out2_rdy => out2_rdy,
		out2_count => out2_count,
		
		out3_data => out3_data,
		out3_send => out3_send,
		out3_ack => out3_ack,
		out3_rdy => out3_rdy,
		out3_count => out3_count,
		
		out4_data => out4_data,
		out4_send => out4_send,
		out4_ack => out4_ack,
		out4_rdy => out4_rdy,
		out4_count => out4_count,
		
		out5_data => out5_data,
		out5_send => out5_send,
		out5_ack => out5_ack,
		out5_rdy => out5_rdy,
		out5_count => out5_count,
		
		out6_data => out6_data,
		out6_send => out6_send,
		out6_ack => out6_ack,
		out6_rdy => out6_rdy,
		out6_count => out6_count,
		
		out7_data => out7_data,
		out7_send => out7_send,
		out7_ack => out7_ack,
		out7_rdy => out7_rdy,
		out7_count => out7_count,
		
		out8_data => out8_data,
		out8_send => out8_send,
		out8_ack => out8_ack,
		out8_rdy => out8_rdy,
		out8_count => out8_count,
		
		out9_data => out9_data,
		out9_send => out9_send,
		out9_ack => out9_ack,
		out9_rdy => out9_rdy,
		out9_count => out9_count,
		
		out10_data => out10_data,
		out10_send => out10_send,
		out10_ack => out10_ack,
		out10_rdy => out10_rdy,
		out10_count => out10_count,
		
		out11_data => out11_data,
		out11_send => out11_send,
		out11_ack => out11_ack,
		out11_rdy => out11_rdy,
		out11_count => out11_count,
		
		out12_data => out12_data,
		out12_send => out12_send,
		out12_ack => out12_ack,
		out12_rdy => out12_rdy,
		out12_count => out12_count,
		
		out13_data => out13_data,
		out13_send => out13_send,
		out13_ack => out13_ack,
		out13_rdy => out13_rdy,
		out13_count => out13_count,
		
		out14_data => out14_data,
		out14_send => out14_send,
		out14_ack => out14_ack,
		out14_rdy => out14_rdy,
		out14_count => out14_count,
		
		out15_data => out15_data,
		out15_send => out15_send,
		out15_ack => out15_ack,
		out15_rdy => out15_rdy,
		out15_count => out15_count,
		
		out16_data => out16_data,
		out16_send => out16_send,
		out16_ack => out16_ack,
		out16_rdy => out16_rdy,
		out16_count => out16_count,
		CLK => CLK,
		reset => reset);
	
	-- Input(s) queues
	q_in1 : entity systemBuilder.Queue(behavioral)
	generic map(length => 512, width => 8)
	port map(
		OUT_DATA => q_in1_data,
		OUT_SEND => q_in1_send,
		OUT_ACK => q_in1_ack,
		OUT_COUNT => q_in1_count,
	
		IN_DATA => in1_data,
		IN_SEND => in1_send,
		IN_ACK => in1_ack,
		IN_RDY => in1_rdy,
		IN_COUNT => in1_count,

		CLK => CLK,
		reset => reset);

	-- Clock process
	
	clockProcess : process
	begin
	wait for OFFSET;
		clockLOOP : loop
			CLK <= '0';
			wait for (PERIOD - (PERIOD * DUTY_CYCLE));
			CLK <= '1';
			wait for (PERIOD * DUTY_CYCLE);
		end loop clockLOOP;
	end process;
	
	-- Reset process
	resetProcess : process
	begin
		wait for OFFSET;
		-- reset state for 100 ns.
		RESET <= '1';
		wait for 100 ns;
		RESET <= '0';
		wait;
	end process;

	
	-- Input(s) Waveform Generation
	WaveGen_Proc_In : process (CLK)
		variable Input_bit : integer range 2147483647 downto - 2147483648;
		variable line_number : line;
	begin
		if rising_edge(CLK) then
		-- Input port: in1 Waveform Generation
			case tb_FSM_in1 is
				when after_reset =>
					count <= count + 1;
					if (count = 15) then
						tb_FSM_in1 <= read_file;
						count <= 0;
					end if;
				when read_file =>
					if (not endfile (sim_file_split_in1)) then
						readline(sim_file_split_in1, line_number);
						if (line_number'length > 0 and line_number(1) /= '/') then
							read(line_number, input_bit);
							in1_data <= std_logic_vector(to_unsigned(input_bit, 8));
							in1_send <= '1';
							tb_FSM_in1 <= CheckRead;
						end if;
					end if;
				when CheckRead =>
					if (not endfile (sim_file_split_in1)) and in1_ack = '1' then
						readline(sim_file_split_in1, line_number);
						if (line_number'length > 0 and line_number(1) /= '/') then
							read(line_number, input_bit);
							in1_data <= std_logic_vector(to_unsigned(input_bit, 8));
							in1_send <= '1';
						end if;
					elsif (endfile (sim_file_split_in1)) then
						in1_send <= '0';
					end if;
				when others => null;
			end case;
		end if;
	end process WaveGen_Proc_In;
	
	-- Output(s) waveform Generation
	out1_ack <= out1_send;
	out1_rdy <= '1';
	
	out2_ack <= out2_send;
	out2_rdy <= '1';
	
	out3_ack <= out3_send;
	out3_rdy <= '1';
	
	out4_ack <= out4_send;
	out4_rdy <= '1';
	
	out5_ack <= out5_send;
	out5_rdy <= '1';
	
	out6_ack <= out6_send;
	out6_rdy <= '1';
	
	out7_ack <= out7_send;
	out7_rdy <= '1';
	
	out8_ack <= out8_send;
	out8_rdy <= '1';
	
	out9_ack <= out9_send;
	out9_rdy <= '1';
	
	out10_ack <= out10_send;
	out10_rdy <= '1';
	
	out11_ack <= out11_send;
	out11_rdy <= '1';
	
	out12_ack <= out12_send;
	out12_rdy <= '1';
	
	out13_ack <= out13_send;
	out13_rdy <= '1';
	
	out14_ack <= out14_send;
	out14_rdy <= '1';
	
	out15_ack <= out15_send;
	out15_rdy <= '1';
	
	out16_ack <= out16_send;
	out16_rdy <= '1';
	
	WaveGen_Proc_Out : process (CLK)
		variable Input_bit   : integer range 2147483647 downto - 2147483648;
		variable line_number : line;
		variable sequence_out1 : integer := 0;
		
		variable sequence_out2 : integer := 0;
		
		variable sequence_out3 : integer := 0;
		
		variable sequence_out4 : integer := 0;
		
		variable sequence_out5 : integer := 0;
		
		variable sequence_out6 : integer := 0;
		
		variable sequence_out7 : integer := 0;
		
		variable sequence_out8 : integer := 0;
		
		variable sequence_out9 : integer := 0;
		
		variable sequence_out10 : integer := 0;
		
		variable sequence_out11 : integer := 0;
		
		variable sequence_out12 : integer := 0;
		
		variable sequence_out13 : integer := 0;
		
		variable sequence_out14 : integer := 0;
		
		variable sequence_out15 : integer := 0;
		
		variable sequence_out16 : integer := 0;
	begin
		if (rising_edge(CLK)) then
		-- Output port: out1 Waveform Generation
			if (not endfile (sim_file_split_out1) and out1_send = '1') then
				readline(sim_file_split_out1, line_number);
					if (line_number'length > 0 and line_number(1) /= '/') then
						read(line_number, input_bit);
						assert (out1_data  = std_logic_vector(to_unsigned(input_bit, 8)))
						report "on port out1 incorrect value computed : " & str(to_integer(unsigned(out1_data))) & " instead of : " & str(input_bit) & " sequence " & str(sequence_out1)
						severity failure;
						
						assert (out1_data /= std_logic_vector(to_unsigned(input_bit, 8)))
						report "on port out1 correct value computed : " & str(to_integer(unsigned(out1_data))) & " equals : " & str(input_bit) & " sequence " & str(sequence_out1)
						severity note;
						sequence_out1 := sequence_out1 + 1;
					end if;
			end if;
		
		-- Output port: out2 Waveform Generation
			if (not endfile (sim_file_split_out2) and out2_send = '1') then
				readline(sim_file_split_out2, line_number);
					if (line_number'length > 0 and line_number(1) /= '/') then
						read(line_number, input_bit);
						assert (out2_data  = std_logic_vector(to_unsigned(input_bit, 8)))
						report "on port out2 incorrect value computed : " & str(to_integer(unsigned(out2_data))) & " instead of : " & str(input_bit) & " sequence " & str(sequence_out2)
						severity failure;
						
						assert (out2_data /= std_logic_vector(to_unsigned(input_bit, 8)))
						report "on port out2 correct value computed : " & str(to_integer(unsigned(out2_data))) & " equals : " & str(input_bit) & " sequence " & str(sequence_out2)
						severity note;
						sequence_out2 := sequence_out2 + 1;
					end if;
			end if;
		
		-- Output port: out3 Waveform Generation
			if (not endfile (sim_file_split_out3) and out3_send = '1') then
				readline(sim_file_split_out3, line_number);
					if (line_number'length > 0 and line_number(1) /= '/') then
						read(line_number, input_bit);
						assert (out3_data  = std_logic_vector(to_unsigned(input_bit, 8)))
						report "on port out3 incorrect value computed : " & str(to_integer(unsigned(out3_data))) & " instead of : " & str(input_bit) & " sequence " & str(sequence_out3)
						severity failure;
						
						assert (out3_data /= std_logic_vector(to_unsigned(input_bit, 8)))
						report "on port out3 correct value computed : " & str(to_integer(unsigned(out3_data))) & " equals : " & str(input_bit) & " sequence " & str(sequence_out3)
						severity note;
						sequence_out3 := sequence_out3 + 1;
					end if;
			end if;
		
		-- Output port: out4 Waveform Generation
			if (not endfile (sim_file_split_out4) and out4_send = '1') then
				readline(sim_file_split_out4, line_number);
					if (line_number'length > 0 and line_number(1) /= '/') then
						read(line_number, input_bit);
						assert (out4_data  = std_logic_vector(to_unsigned(input_bit, 8)))
						report "on port out4 incorrect value computed : " & str(to_integer(unsigned(out4_data))) & " instead of : " & str(input_bit) & " sequence " & str(sequence_out4)
						severity failure;
						
						assert (out4_data /= std_logic_vector(to_unsigned(input_bit, 8)))
						report "on port out4 correct value computed : " & str(to_integer(unsigned(out4_data))) & " equals : " & str(input_bit) & " sequence " & str(sequence_out4)
						severity note;
						sequence_out4 := sequence_out4 + 1;
					end if;
			end if;
		
		-- Output port: out5 Waveform Generation
			if (not endfile (sim_file_split_out5) and out5_send = '1') then
				readline(sim_file_split_out5, line_number);
					if (line_number'length > 0 and line_number(1) /= '/') then
						read(line_number, input_bit);
						assert (out5_data  = std_logic_vector(to_unsigned(input_bit, 8)))
						report "on port out5 incorrect value computed : " & str(to_integer(unsigned(out5_data))) & " instead of : " & str(input_bit) & " sequence " & str(sequence_out5)
						severity failure;
						
						assert (out5_data /= std_logic_vector(to_unsigned(input_bit, 8)))
						report "on port out5 correct value computed : " & str(to_integer(unsigned(out5_data))) & " equals : " & str(input_bit) & " sequence " & str(sequence_out5)
						severity note;
						sequence_out5 := sequence_out5 + 1;
					end if;
			end if;
		
		-- Output port: out6 Waveform Generation
			if (not endfile (sim_file_split_out6) and out6_send = '1') then
				readline(sim_file_split_out6, line_number);
					if (line_number'length > 0 and line_number(1) /= '/') then
						read(line_number, input_bit);
						assert (out6_data  = std_logic_vector(to_unsigned(input_bit, 8)))
						report "on port out6 incorrect value computed : " & str(to_integer(unsigned(out6_data))) & " instead of : " & str(input_bit) & " sequence " & str(sequence_out6)
						severity failure;
						
						assert (out6_data /= std_logic_vector(to_unsigned(input_bit, 8)))
						report "on port out6 correct value computed : " & str(to_integer(unsigned(out6_data))) & " equals : " & str(input_bit) & " sequence " & str(sequence_out6)
						severity note;
						sequence_out6 := sequence_out6 + 1;
					end if;
			end if;
		
		-- Output port: out7 Waveform Generation
			if (not endfile (sim_file_split_out7) and out7_send = '1') then
				readline(sim_file_split_out7, line_number);
					if (line_number'length > 0 and line_number(1) /= '/') then
						read(line_number, input_bit);
						assert (out7_data  = std_logic_vector(to_unsigned(input_bit, 8)))
						report "on port out7 incorrect value computed : " & str(to_integer(unsigned(out7_data))) & " instead of : " & str(input_bit) & " sequence " & str(sequence_out7)
						severity failure;
						
						assert (out7_data /= std_logic_vector(to_unsigned(input_bit, 8)))
						report "on port out7 correct value computed : " & str(to_integer(unsigned(out7_data))) & " equals : " & str(input_bit) & " sequence " & str(sequence_out7)
						severity note;
						sequence_out7 := sequence_out7 + 1;
					end if;
			end if;
		
		-- Output port: out8 Waveform Generation
			if (not endfile (sim_file_split_out8) and out8_send = '1') then
				readline(sim_file_split_out8, line_number);
					if (line_number'length > 0 and line_number(1) /= '/') then
						read(line_number, input_bit);
						assert (out8_data  = std_logic_vector(to_unsigned(input_bit, 8)))
						report "on port out8 incorrect value computed : " & str(to_integer(unsigned(out8_data))) & " instead of : " & str(input_bit) & " sequence " & str(sequence_out8)
						severity failure;
						
						assert (out8_data /= std_logic_vector(to_unsigned(input_bit, 8)))
						report "on port out8 correct value computed : " & str(to_integer(unsigned(out8_data))) & " equals : " & str(input_bit) & " sequence " & str(sequence_out8)
						severity note;
						sequence_out8 := sequence_out8 + 1;
					end if;
			end if;
		
		-- Output port: out9 Waveform Generation
			if (not endfile (sim_file_split_out9) and out9_send = '1') then
				readline(sim_file_split_out9, line_number);
					if (line_number'length > 0 and line_number(1) /= '/') then
						read(line_number, input_bit);
						assert (out9_data  = std_logic_vector(to_unsigned(input_bit, 8)))
						report "on port out9 incorrect value computed : " & str(to_integer(unsigned(out9_data))) & " instead of : " & str(input_bit) & " sequence " & str(sequence_out9)
						severity failure;
						
						assert (out9_data /= std_logic_vector(to_unsigned(input_bit, 8)))
						report "on port out9 correct value computed : " & str(to_integer(unsigned(out9_data))) & " equals : " & str(input_bit) & " sequence " & str(sequence_out9)
						severity note;
						sequence_out9 := sequence_out9 + 1;
					end if;
			end if;
		
		-- Output port: out10 Waveform Generation
			if (not endfile (sim_file_split_out10) and out10_send = '1') then
				readline(sim_file_split_out10, line_number);
					if (line_number'length > 0 and line_number(1) /= '/') then
						read(line_number, input_bit);
						assert (out10_data  = std_logic_vector(to_unsigned(input_bit, 8)))
						report "on port out10 incorrect value computed : " & str(to_integer(unsigned(out10_data))) & " instead of : " & str(input_bit) & " sequence " & str(sequence_out10)
						severity failure;
						
						assert (out10_data /= std_logic_vector(to_unsigned(input_bit, 8)))
						report "on port out10 correct value computed : " & str(to_integer(unsigned(out10_data))) & " equals : " & str(input_bit) & " sequence " & str(sequence_out10)
						severity note;
						sequence_out10 := sequence_out10 + 1;
					end if;
			end if;
		
		-- Output port: out11 Waveform Generation
			if (not endfile (sim_file_split_out11) and out11_send = '1') then
				readline(sim_file_split_out11, line_number);
					if (line_number'length > 0 and line_number(1) /= '/') then
						read(line_number, input_bit);
						assert (out11_data  = std_logic_vector(to_unsigned(input_bit, 8)))
						report "on port out11 incorrect value computed : " & str(to_integer(unsigned(out11_data))) & " instead of : " & str(input_bit) & " sequence " & str(sequence_out11)
						severity failure;
						
						assert (out11_data /= std_logic_vector(to_unsigned(input_bit, 8)))
						report "on port out11 correct value computed : " & str(to_integer(unsigned(out11_data))) & " equals : " & str(input_bit) & " sequence " & str(sequence_out11)
						severity note;
						sequence_out11 := sequence_out11 + 1;
					end if;
			end if;
		
		-- Output port: out12 Waveform Generation
			if (not endfile (sim_file_split_out12) and out12_send = '1') then
				readline(sim_file_split_out12, line_number);
					if (line_number'length > 0 and line_number(1) /= '/') then
						read(line_number, input_bit);
						assert (out12_data  = std_logic_vector(to_unsigned(input_bit, 8)))
						report "on port out12 incorrect value computed : " & str(to_integer(unsigned(out12_data))) & " instead of : " & str(input_bit) & " sequence " & str(sequence_out12)
						severity failure;
						
						assert (out12_data /= std_logic_vector(to_unsigned(input_bit, 8)))
						report "on port out12 correct value computed : " & str(to_integer(unsigned(out12_data))) & " equals : " & str(input_bit) & " sequence " & str(sequence_out12)
						severity note;
						sequence_out12 := sequence_out12 + 1;
					end if;
			end if;
		
		-- Output port: out13 Waveform Generation
			if (not endfile (sim_file_split_out13) and out13_send = '1') then
				readline(sim_file_split_out13, line_number);
					if (line_number'length > 0 and line_number(1) /= '/') then
						read(line_number, input_bit);
						assert (out13_data  = std_logic_vector(to_unsigned(input_bit, 8)))
						report "on port out13 incorrect value computed : " & str(to_integer(unsigned(out13_data))) & " instead of : " & str(input_bit) & " sequence " & str(sequence_out13)
						severity failure;
						
						assert (out13_data /= std_logic_vector(to_unsigned(input_bit, 8)))
						report "on port out13 correct value computed : " & str(to_integer(unsigned(out13_data))) & " equals : " & str(input_bit) & " sequence " & str(sequence_out13)
						severity note;
						sequence_out13 := sequence_out13 + 1;
					end if;
			end if;
		
		-- Output port: out14 Waveform Generation
			if (not endfile (sim_file_split_out14) and out14_send = '1') then
				readline(sim_file_split_out14, line_number);
					if (line_number'length > 0 and line_number(1) /= '/') then
						read(line_number, input_bit);
						assert (out14_data  = std_logic_vector(to_unsigned(input_bit, 8)))
						report "on port out14 incorrect value computed : " & str(to_integer(unsigned(out14_data))) & " instead of : " & str(input_bit) & " sequence " & str(sequence_out14)
						severity failure;
						
						assert (out14_data /= std_logic_vector(to_unsigned(input_bit, 8)))
						report "on port out14 correct value computed : " & str(to_integer(unsigned(out14_data))) & " equals : " & str(input_bit) & " sequence " & str(sequence_out14)
						severity note;
						sequence_out14 := sequence_out14 + 1;
					end if;
			end if;
		
		-- Output port: out15 Waveform Generation
			if (not endfile (sim_file_split_out15) and out15_send = '1') then
				readline(sim_file_split_out15, line_number);
					if (line_number'length > 0 and line_number(1) /= '/') then
						read(line_number, input_bit);
						assert (out15_data  = std_logic_vector(to_unsigned(input_bit, 8)))
						report "on port out15 incorrect value computed : " & str(to_integer(unsigned(out15_data))) & " instead of : " & str(input_bit) & " sequence " & str(sequence_out15)
						severity failure;
						
						assert (out15_data /= std_logic_vector(to_unsigned(input_bit, 8)))
						report "on port out15 correct value computed : " & str(to_integer(unsigned(out15_data))) & " equals : " & str(input_bit) & " sequence " & str(sequence_out15)
						severity note;
						sequence_out15 := sequence_out15 + 1;
					end if;
			end if;
		
		-- Output port: out16 Waveform Generation
			if (not endfile (sim_file_split_out16) and out16_send = '1') then
				readline(sim_file_split_out16, line_number);
					if (line_number'length > 0 and line_number(1) /= '/') then
						read(line_number, input_bit);
						assert (out16_data  = std_logic_vector(to_unsigned(input_bit, 8)))
						report "on port out16 incorrect value computed : " & str(to_integer(unsigned(out16_data))) & " instead of : " & str(input_bit) & " sequence " & str(sequence_out16)
						severity failure;
						
						assert (out16_data /= std_logic_vector(to_unsigned(input_bit, 8)))
						report "on port out16 correct value computed : " & str(to_integer(unsigned(out16_data))) & " equals : " & str(input_bit) & " sequence " & str(sequence_out16)
						severity note;
						sequence_out16 := sequence_out16 + 1;
					end if;
			end if;
		end if;			
	end process WaveGen_Proc_Out;
	
end architecture arch_split_tb; 
