// __  ___ __ ___  _ __   ___  ___ 
// \ \/ / '__/ _ \| '_ \ / _ \/ __|
//  >  <| | | (_) | | | | (_) \__ \
// /_/\_\_|  \___/|_| |_|\___/|___/
// 
// Xronos synthesizer version
// Run date: Fri 23 Feb 2018 14:27:58 +0000
// 

module join_u4(in4_SEND, in2_COUNT, in4_ACK, in2_DATA, in3_COUNT, out1_COUNT, in4_DATA, in3_SEND, in2_ACK, out1_RDY, CLK, in1_SEND, RESET, in1_COUNT, out1_ACK, in3_ACK, in3_DATA, out1_SEND, in1_DATA, in4_COUNT, in1_ACK, in2_SEND, out1_DATA);
input		in4_SEND;
input	[15:0]	in2_COUNT;
output		in4_ACK;
input	[7:0]	in2_DATA;
input	[15:0]	in3_COUNT;
output	[15:0]	out1_COUNT;
input	[7:0]	in4_DATA;
input		in3_SEND;
output		in2_ACK;
input		out1_RDY;
input		CLK;
input		in1_SEND;
wire		fanIn3_go;
input		RESET;
input	[15:0]	in1_COUNT;
input		out1_ACK;
wire		fanIn4_done;
output		in3_ACK;
input	[7:0]	in3_DATA;
wire		fanIn1_done;
wire		fanIn1_go;
wire		fanIn3_done;
output		out1_SEND;
wire		fanIn4_go;
input	[7:0]	in1_DATA;
wire		fanIn2_done;
input	[15:0]	in4_COUNT;
output		in1_ACK;
input		in2_SEND;
output	[7:0]	out1_DATA;
wire		fanIn2_go;
wire		scheduler;
wire		scheduler_u758;
wire		scheduler_u757;
wire	[1:0]	scheduler_u756;
wire		scheduler_u760;
wire		scheduler_u759;
wire		join_scheduler_instance_DONE;
wire		fanIn1_u7;
wire		join_fanIn1_instance_DONE;
wire	[7:0]	fanIn1_u8;
wire		fanIn1;
wire	[15:0]	fanIn1_u6;
wire	[7:0]	or_68c3fed8_u0;
wire	[1:0]	bus_34771cb5_;
wire		bus_3188018e_;
wire		bus_6589b446_;
wire		bus_0c3aceab_;
wire		bus_17235d01_;
wire	[15:0]	fanIn2_u6;
wire		fanIn2;
wire	[7:0]	fanIn2_u8;
wire		fanIn2_u7;
wire		join_fanIn2_instance_DONE;
wire		bus_22382ffe_;
wire	[15:0]	or_59d1768e_u0;
wire		or_0f2079c5_u0;
wire		fanIn3;
wire		fanIn3_u7;
wire	[7:0]	fanIn3_u8;
wire		join_fanIn3_instance_DONE;
wire	[15:0]	fanIn3_u6;
wire		bus_31136193_;
wire		join_fanIn4_instance_DONE;
wire	[7:0]	fanIn4_u7;
wire	[15:0]	fanIn4;
wire		fanIn4_u6;
wire		fanIn4_u8;
assign in4_ACK=fanIn4_u8;
assign out1_COUNT=or_59d1768e_u0;
assign in2_ACK=fanIn2;
assign fanIn3_go=scheduler_u757;
assign fanIn4_done=bus_17235d01_;
assign in3_ACK=fanIn3;
assign fanIn1_done=bus_31136193_;
assign fanIn1_go=scheduler_u758;
assign fanIn3_done=bus_6589b446_;
assign out1_SEND=or_0f2079c5_u0;
assign fanIn4_go=scheduler_u760;
assign fanIn2_done=bus_22382ffe_;
assign in1_ACK=fanIn1;
assign out1_DATA=or_68c3fed8_u0;
assign fanIn2_go=scheduler_u759;
join_scheduler join_scheduler_instance(.CLK(CLK), .RESET(bus_0c3aceab_), .GO(bus_3188018e_), 
  .port_6ec93744_(bus_34771cb5_), .port_2fc98b7e_(fanIn4_done), .port_412dc729_(in2_SEND), 
  .port_7d4b86b3_(in3_SEND), .port_21ea565f_(in4_SEND), .port_4da192d5_(out1_RDY), 
  .port_3eba1055_(in1_SEND), .port_6456c869_(fanIn1_done), .port_48abe608_(fanIn2_done), 
  .port_39668ff3_(fanIn3_done), .DONE(join_scheduler_instance_DONE), .RESULT(scheduler), 
  .RESULT_u1903(scheduler_u756), .RESULT_u1904(scheduler_u757), .RESULT_u1905(scheduler_u758), 
  .RESULT_u1906(scheduler_u759), .RESULT_u1907(scheduler_u760));
join_fanIn1 join_fanIn1_instance(.CLK(CLK), .GO(fanIn1_go), .port_60bf99a0_(in1_DATA), 
  .DONE(join_fanIn1_instance_DONE), .RESULT(fanIn1), .RESULT_u1908(fanIn1_u6), 
  .RESULT_u1909(fanIn1_u7), .RESULT_u1910(fanIn1_u8));
assign or_68c3fed8_u0=fanIn1_u8|fanIn2_u8|fanIn3_u8|fanIn4_u7;
join_stateVar_fsmState_join join_stateVar_fsmState_join_1(.bus_288040b8_(CLK), 
  .bus_0e7e6bc9_(bus_0c3aceab_), .bus_39b58bcf_(scheduler), .bus_6e479858_(scheduler_u756), 
  .bus_34771cb5_(bus_34771cb5_));
join_Kicker_49 join_Kicker_49_1(.CLK(CLK), .RESET(bus_0c3aceab_), .bus_3188018e_(bus_3188018e_));
assign bus_6589b446_=join_fanIn3_instance_DONE&{1{join_fanIn3_instance_DONE}};
join_globalreset_physical_1e8de1b6_ join_globalreset_physical_1e8de1b6__1(.bus_0f80bd30_(CLK), 
  .bus_73229404_(RESET), .bus_0c3aceab_(bus_0c3aceab_));
assign bus_17235d01_=join_fanIn4_instance_DONE&{1{join_fanIn4_instance_DONE}};
join_fanIn2 join_fanIn2_instance(.CLK(CLK), .GO(fanIn2_go), .port_07e5d029_(in2_DATA), 
  .DONE(join_fanIn2_instance_DONE), .RESULT(fanIn2), .RESULT_u1911(fanIn2_u6), 
  .RESULT_u1912(fanIn2_u7), .RESULT_u1913(fanIn2_u8));
assign bus_22382ffe_=join_fanIn2_instance_DONE&{1{join_fanIn2_instance_DONE}};
assign or_59d1768e_u0=fanIn1_u6|fanIn2_u6|fanIn3_u6|fanIn4;
assign or_0f2079c5_u0=fanIn1_u7|fanIn2_u7|fanIn3_u7|fanIn4_u6;
join_fanIn3 join_fanIn3_instance(.CLK(CLK), .GO(fanIn3_go), .port_72108e99_(in3_DATA), 
  .DONE(join_fanIn3_instance_DONE), .RESULT(fanIn3), .RESULT_u1914(fanIn3_u6), 
  .RESULT_u1915(fanIn3_u7), .RESULT_u1916(fanIn3_u8));
assign bus_31136193_=join_fanIn1_instance_DONE&{1{join_fanIn1_instance_DONE}};
join_fanIn4 join_fanIn4_instance(.CLK(CLK), .GO(fanIn4_go), .port_1940228d_(in4_DATA), 
  .DONE(join_fanIn4_instance_DONE), .RESULT(fanIn4), .RESULT_u1917(fanIn4_u6), 
  .RESULT_u1918(fanIn4_u7), .RESULT_u1919(fanIn4_u8));
endmodule



module join_scheduler(CLK, RESET, GO, port_6ec93744_, port_2fc98b7e_, port_412dc729_, port_7d4b86b3_, port_21ea565f_, port_4da192d5_, port_3eba1055_, port_6456c869_, port_48abe608_, port_39668ff3_, RESULT, RESULT_u1903, RESULT_u1904, RESULT_u1905, RESULT_u1906, RESULT_u1907, DONE);
input		CLK;
input		RESET;
input		GO;
input	[1:0]	port_6ec93744_;
input		port_2fc98b7e_;
input		port_412dc729_;
input		port_7d4b86b3_;
input		port_21ea565f_;
input		port_4da192d5_;
input		port_3eba1055_;
input		port_6456c869_;
input		port_48abe608_;
input		port_39668ff3_;
output		RESULT;
output	[1:0]	RESULT_u1903;
output		RESULT_u1904;
output		RESULT_u1905;
output		RESULT_u1906;
output		RESULT_u1907;
output		DONE;
reg		reg_4f9a981c_u0=1'h0;
wire		and_u3139_u0;
wire		equals;
wire signed	[31:0]	equals_a_signed;
wire signed	[31:0]	equals_b_signed;
wire		and_u3140_u0;
wire		and_u3141_u0;
wire		not_u704_u0;
wire		not_u705_u0;
wire		and_u3142_u0;
wire		and_u3143_u0;
wire		not_u706_u0;
wire		and_u3144_u0;
wire		and_u3145_u0;
wire		simplePinWrite;
wire		and_u3146_u0;
wire		and_u3147_u0;
wire		and_u3148_u0;
wire signed	[31:0]	equals_u188_a_signed;
wire signed	[31:0]	equals_u188_b_signed;
wire		equals_u188;
wire		and_u3149_u0;
wire		and_u3150_u0;
wire		not_u707_u0;
wire		not_u708_u0;
wire		and_u3151_u0;
wire		and_u3152_u0;
wire		and_u3153_u0;
wire		not_u709_u0;
wire		and_u3154_u0;
wire		simplePinWrite_u441;
wire		and_u3155_u0;
wire		and_u3156_u0;
wire		and_u3157_u0;
wire signed	[31:0]	equals_u189_a_signed;
wire signed	[31:0]	equals_u189_b_signed;
wire		equals_u189;
wire		and_u3158_u0;
wire		and_u3159_u0;
wire		not_u710_u0;
wire		not_u711_u0;
wire		and_u3160_u0;
wire		and_u3161_u0;
wire		and_u3162_u0;
wire		not_u712_u0;
wire		and_u3163_u0;
wire		simplePinWrite_u442;
wire		and_u3164_u0;
wire		and_u3165_u0;
wire		and_u3166_u0;
wire signed	[31:0]	equals_u190_a_signed;
wire		equals_u190;
wire signed	[31:0]	equals_u190_b_signed;
wire		and_u3167_u0;
wire		and_u3168_u0;
wire		not_u713_u0;
wire		and_u3169_u0;
wire		and_u3170_u0;
wire		not_u714_u0;
wire		not_u715_u0;
wire		and_u3171_u0;
wire		and_u3172_u0;
wire		simplePinWrite_u443;
wire		and_u3173_u0;
wire		and_u3174_u0;
wire		and_u3175_u0;
wire		or_u1311_u0;
wire	[1:0]	mux_u1752;
wire		or_u1312_u0;
reg		reg_136c45ed_u0=1'h0;
reg		reg_53a4a288_u0=1'h0;
wire		or_u1313_u0;
wire	[1:0]	mux_u1753_u0;
always @(posedge CLK or posedge RESET)
begin
if (RESET)
reg_4f9a981c_u0<=1'h0;
else reg_4f9a981c_u0<=and_u3139_u0;
end
assign and_u3139_u0=or_u1312_u0&or_u1312_u0;
assign equals_a_signed={30'b0, port_6ec93744_};
assign equals_b_signed=32'h0;
assign equals=equals_a_signed==equals_b_signed;
assign and_u3140_u0=and_u3139_u0&equals;
assign and_u3141_u0=and_u3139_u0&not_u704_u0;
assign not_u704_u0=~equals;
assign not_u705_u0=~port_3eba1055_;
assign and_u3142_u0=and_u3148_u0&port_3eba1055_;
assign and_u3143_u0=and_u3148_u0&not_u705_u0;
assign not_u706_u0=~port_4da192d5_;
assign and_u3144_u0=and_u3147_u0&port_4da192d5_;
assign and_u3145_u0=and_u3147_u0&not_u706_u0;
assign simplePinWrite=and_u3146_u0&{1{and_u3146_u0}};
assign and_u3146_u0=and_u3144_u0&and_u3147_u0;
assign and_u3147_u0=and_u3142_u0&and_u3148_u0;
assign and_u3148_u0=and_u3140_u0&and_u3139_u0;
assign equals_u188_a_signed={30'b0, port_6ec93744_};
assign equals_u188_b_signed=32'h1;
assign equals_u188=equals_u188_a_signed==equals_u188_b_signed;
assign and_u3149_u0=and_u3139_u0&not_u707_u0;
assign and_u3150_u0=and_u3139_u0&equals_u188;
assign not_u707_u0=~equals_u188;
assign not_u708_u0=~port_412dc729_;
assign and_u3151_u0=and_u3157_u0&port_412dc729_;
assign and_u3152_u0=and_u3157_u0&not_u708_u0;
assign and_u3153_u0=and_u3156_u0&not_u709_u0;
assign not_u709_u0=~port_4da192d5_;
assign and_u3154_u0=and_u3156_u0&port_4da192d5_;
assign simplePinWrite_u441=and_u3155_u0&{1{and_u3155_u0}};
assign and_u3155_u0=and_u3154_u0&and_u3156_u0;
assign and_u3156_u0=and_u3151_u0&and_u3157_u0;
assign and_u3157_u0=and_u3150_u0&and_u3139_u0;
assign equals_u189_a_signed={30'b0, port_6ec93744_};
assign equals_u189_b_signed=32'h2;
assign equals_u189=equals_u189_a_signed==equals_u189_b_signed;
assign and_u3158_u0=and_u3139_u0&equals_u189;
assign and_u3159_u0=and_u3139_u0&not_u710_u0;
assign not_u710_u0=~equals_u189;
assign not_u711_u0=~port_7d4b86b3_;
assign and_u3160_u0=and_u3166_u0&not_u711_u0;
assign and_u3161_u0=and_u3166_u0&port_7d4b86b3_;
assign and_u3162_u0=and_u3165_u0&not_u712_u0;
assign not_u712_u0=~port_4da192d5_;
assign and_u3163_u0=and_u3165_u0&port_4da192d5_;
assign simplePinWrite_u442=and_u3164_u0&{1{and_u3164_u0}};
assign and_u3164_u0=and_u3163_u0&and_u3165_u0;
assign and_u3165_u0=and_u3161_u0&and_u3166_u0;
assign and_u3166_u0=and_u3158_u0&and_u3139_u0;
assign equals_u190_a_signed={30'b0, port_6ec93744_};
assign equals_u190_b_signed=32'h3;
assign equals_u190=equals_u190_a_signed==equals_u190_b_signed;
assign and_u3167_u0=and_u3139_u0&equals_u190;
assign and_u3168_u0=and_u3139_u0&not_u713_u0;
assign not_u713_u0=~equals_u190;
assign and_u3169_u0=and_u3175_u0&not_u714_u0;
assign and_u3170_u0=and_u3175_u0&port_21ea565f_;
assign not_u714_u0=~port_21ea565f_;
assign not_u715_u0=~port_4da192d5_;
assign and_u3171_u0=and_u3174_u0&port_4da192d5_;
assign and_u3172_u0=and_u3174_u0&not_u715_u0;
assign simplePinWrite_u443=and_u3173_u0&{1{and_u3173_u0}};
assign and_u3173_u0=and_u3171_u0&and_u3174_u0;
assign and_u3174_u0=and_u3170_u0&and_u3175_u0;
assign and_u3175_u0=and_u3167_u0&and_u3139_u0;
assign or_u1311_u0=and_u3146_u0|and_u3155_u0|and_u3164_u0|and_u3173_u0;
assign mux_u1752=({2{and_u3146_u0}}&2'h1)|({2{and_u3155_u0}}&2'h2)|({2{and_u3164_u0}}&2'h3)|({2{and_u3173_u0}}&2'h0);
assign or_u1312_u0=reg_4f9a981c_u0|reg_136c45ed_u0;
always @(posedge CLK or posedge RESET)
begin
if (RESET)
reg_136c45ed_u0<=1'h0;
else reg_136c45ed_u0<=reg_53a4a288_u0;
end
always @(posedge CLK or posedge RESET)
begin
if (RESET)
reg_53a4a288_u0<=1'h0;
else reg_53a4a288_u0<=GO;
end
assign or_u1313_u0=GO|or_u1311_u0;
assign mux_u1753_u0=(GO)?2'h0:mux_u1752;
assign RESULT=or_u1313_u0;
assign RESULT_u1903=mux_u1753_u0;
assign RESULT_u1904=simplePinWrite_u442;
assign RESULT_u1905=simplePinWrite;
assign RESULT_u1906=simplePinWrite_u441;
assign RESULT_u1907=simplePinWrite_u443;
assign DONE=1'h0;
endmodule



module join_fanIn1(CLK, GO, port_60bf99a0_, RESULT, RESULT_u1908, RESULT_u1909, RESULT_u1910, DONE);
input		CLK;
input		GO;
input	[7:0]	port_60bf99a0_;
output		RESULT;
output	[15:0]	RESULT_u1908;
output		RESULT_u1909;
output	[7:0]	RESULT_u1910;
output		DONE;
wire		simplePinWrite;
wire		simplePinWrite_u444;
wire	[7:0]	simplePinWrite_u445;
wire	[15:0]	simplePinWrite_u446;
assign simplePinWrite=GO&{1{GO}};
assign simplePinWrite_u444=GO&{1{GO}};
assign simplePinWrite_u445=port_60bf99a0_&{8{GO}};
assign simplePinWrite_u446=16'h1&{16{1'h1}};
assign RESULT=simplePinWrite;
assign RESULT_u1908=simplePinWrite_u446;
assign RESULT_u1909=simplePinWrite_u444;
assign RESULT_u1910=simplePinWrite_u445;
assign DONE=GO;
endmodule



module join_endianswapper_7685a3f7_(endianswapper_7685a3f7_in, endianswapper_7685a3f7_out);
input	[1:0]	endianswapper_7685a3f7_in;
output	[1:0]	endianswapper_7685a3f7_out;
assign endianswapper_7685a3f7_out=endianswapper_7685a3f7_in;
endmodule



module join_endianswapper_3be7db53_(endianswapper_3be7db53_in, endianswapper_3be7db53_out);
input	[1:0]	endianswapper_3be7db53_in;
output	[1:0]	endianswapper_3be7db53_out;
assign endianswapper_3be7db53_out=endianswapper_3be7db53_in;
endmodule



module join_stateVar_fsmState_join(bus_288040b8_, bus_0e7e6bc9_, bus_39b58bcf_, bus_6e479858_, bus_34771cb5_);
input		bus_288040b8_;
input		bus_0e7e6bc9_;
input		bus_39b58bcf_;
input	[1:0]	bus_6e479858_;
output	[1:0]	bus_34771cb5_;
wire	[1:0]	endianswapper_7685a3f7_out;
wire	[1:0]	endianswapper_3be7db53_out;
reg	[1:0]	stateVar_fsmState_join_u4=2'h0;
join_endianswapper_7685a3f7_ join_endianswapper_7685a3f7__1(.endianswapper_7685a3f7_in(stateVar_fsmState_join_u4), 
  .endianswapper_7685a3f7_out(endianswapper_7685a3f7_out));
join_endianswapper_3be7db53_ join_endianswapper_3be7db53__1(.endianswapper_3be7db53_in(bus_6e479858_), 
  .endianswapper_3be7db53_out(endianswapper_3be7db53_out));
assign bus_34771cb5_=endianswapper_7685a3f7_out;
always @(posedge bus_288040b8_ or posedge bus_0e7e6bc9_)
begin
if (bus_0e7e6bc9_)
stateVar_fsmState_join_u4<=2'h0;
else if (bus_39b58bcf_)
stateVar_fsmState_join_u4<=endianswapper_3be7db53_out;
end
endmodule



module join_Kicker_49(CLK, RESET, bus_3188018e_);
input		CLK;
input		RESET;
output		bus_3188018e_;
wire		bus_766d1fbe_;
wire		bus_7bd6c3bc_;
reg		kicker_2=1'h0;
wire		bus_2a5ee09e_;
reg		kicker_res=1'h0;
wire		bus_2f1736ad_;
reg		kicker_1=1'h0;
assign bus_766d1fbe_=bus_7bd6c3bc_&kicker_1;
assign bus_3188018e_=kicker_res;
assign bus_7bd6c3bc_=~RESET;
always @(posedge CLK)
begin
kicker_2<=bus_766d1fbe_;
end
assign bus_2a5ee09e_=~kicker_2;
always @(posedge CLK)
begin
kicker_res<=bus_2f1736ad_;
end
assign bus_2f1736ad_=kicker_1&bus_7bd6c3bc_&bus_2a5ee09e_;
always @(posedge CLK)
begin
kicker_1<=bus_7bd6c3bc_;
end
endmodule



module join_globalreset_physical_1e8de1b6_(bus_0f80bd30_, bus_73229404_, bus_0c3aceab_);
input		bus_0f80bd30_;
input		bus_73229404_;
output		bus_0c3aceab_;
wire		not_3cfb5cc8_u0;
reg		sample_u49=1'h0;
wire		or_040bc8ec_u0;
wire		and_0b339e3f_u0;
reg		cross_u49=1'h0;
reg		glitch_u49=1'h0;
reg		final_u49=1'h1;
assign bus_0c3aceab_=or_040bc8ec_u0;
assign not_3cfb5cc8_u0=~and_0b339e3f_u0;
always @(posedge bus_0f80bd30_)
begin
sample_u49<=1'h1;
end
assign or_040bc8ec_u0=bus_73229404_|final_u49;
assign and_0b339e3f_u0=cross_u49&glitch_u49;
always @(posedge bus_0f80bd30_)
begin
cross_u49<=sample_u49;
end
always @(posedge bus_0f80bd30_)
begin
glitch_u49<=cross_u49;
end
always @(posedge bus_0f80bd30_)
begin
final_u49<=not_3cfb5cc8_u0;
end
endmodule



module join_fanIn2(CLK, GO, port_07e5d029_, RESULT, RESULT_u1911, RESULT_u1912, RESULT_u1913, DONE);
input		CLK;
input		GO;
input	[7:0]	port_07e5d029_;
output		RESULT;
output	[15:0]	RESULT_u1911;
output		RESULT_u1912;
output	[7:0]	RESULT_u1913;
output		DONE;
wire		simplePinWrite;
wire		simplePinWrite_u447;
wire	[7:0]	simplePinWrite_u448;
wire	[15:0]	simplePinWrite_u449;
assign simplePinWrite=GO&{1{GO}};
assign simplePinWrite_u447=GO&{1{GO}};
assign simplePinWrite_u448=port_07e5d029_&{8{GO}};
assign simplePinWrite_u449=16'h1&{16{1'h1}};
assign RESULT=simplePinWrite;
assign RESULT_u1911=simplePinWrite_u449;
assign RESULT_u1912=simplePinWrite_u447;
assign RESULT_u1913=simplePinWrite_u448;
assign DONE=GO;
endmodule



module join_fanIn3(CLK, GO, port_72108e99_, RESULT, RESULT_u1914, RESULT_u1915, RESULT_u1916, DONE);
input		CLK;
input		GO;
input	[7:0]	port_72108e99_;
output		RESULT;
output	[15:0]	RESULT_u1914;
output		RESULT_u1915;
output	[7:0]	RESULT_u1916;
output		DONE;
wire		simplePinWrite;
wire	[7:0]	simplePinWrite_u450;
wire		simplePinWrite_u451;
wire	[15:0]	simplePinWrite_u452;
assign simplePinWrite=GO&{1{GO}};
assign simplePinWrite_u450=port_72108e99_&{8{GO}};
assign simplePinWrite_u451=GO&{1{GO}};
assign simplePinWrite_u452=16'h1&{16{1'h1}};
assign RESULT=simplePinWrite;
assign RESULT_u1914=simplePinWrite_u452;
assign RESULT_u1915=simplePinWrite_u451;
assign RESULT_u1916=simplePinWrite_u450;
assign DONE=GO;
endmodule



module join_fanIn4(CLK, GO, port_1940228d_, RESULT, RESULT_u1917, RESULT_u1918, RESULT_u1919, DONE);
input		CLK;
input		GO;
input	[7:0]	port_1940228d_;
output	[15:0]	RESULT;
output		RESULT_u1917;
output	[7:0]	RESULT_u1918;
output		RESULT_u1919;
output		DONE;
wire		simplePinWrite;
wire	[7:0]	simplePinWrite_u453;
wire		simplePinWrite_u454;
wire	[15:0]	simplePinWrite_u455;
assign simplePinWrite=GO&{1{GO}};
assign simplePinWrite_u453=port_1940228d_&{8{GO}};
assign simplePinWrite_u454=GO&{1{GO}};
assign simplePinWrite_u455=16'h1&{16{1'h1}};
assign RESULT=simplePinWrite_u455;
assign RESULT_u1917=simplePinWrite_u454;
assign RESULT_u1918=simplePinWrite_u453;
assign RESULT_u1919=simplePinWrite;
assign DONE=GO;
endmodule


