-- ----------------------------------------------------------------------------
-- __  ___ __ ___  _ __   ___  ___ 
-- \ \/ / '__/ _ \| '_ \ / _ \/ __|
--  >  <| | | (_) | | | | (_) \__ \
-- /_/\_\_|  \___/|_| |_|\___/|___/
-- ----------------------------------------------------------------------------
-- Xronos synthesizer
-- Top level Network: medianParallel4 
-- Date: 2018/02/23 14:28:04
-- ----------------------------------------------------------------------------

-- ----------------------------------------------------------------------------
-- Clock Domain(s) Information on the Network "medianParallel4"
--
-- Network input port(s) clock domain:
--	inPort --> CLK
-- Network output port(s) clock domain:
-- 	outPort --> CLK
-- Actor(s) clock domains:
--	split (split) --> CLK
--	join (join) --> CLK
--	medianRow2 (medianRow2) --> CLK
--	medianRow1 (medianRow1) --> CLK
--	medianRow3 (medianRow3) --> CLK
--	medianRow4 (medianRow4) --> CLK

library ieee;
library SystemBuilder;

use ieee.std_logic_1164.all;

-- ----------------------------------------------------------------------------
-- Entity Declaration
-- ----------------------------------------------------------------------------
entity medianParallel4 is
port(
	 -- XDF Network Input(s)
	 inPort_data : in std_logic_vector(7 downto 0);
	 inPort_send : in std_logic;
	 inPort_ack : out std_logic;
	 inPort_rdy : out std_logic;
	 inPort_count : in std_logic_vector(15 downto 0);
	 -- XDF Network Output(s)
	 outPort_data : out std_logic_vector(7 downto 0);
	 outPort_send : out std_logic;
	 outPort_ack : in std_logic;
	 outPort_rdy : in std_logic;
	 outPort_count : out std_logic_vector(15 downto 0);
	 -- Clock(s) and Reset
	 CLK : in std_logic;
	 RESET : in std_logic);
end entity medianParallel4;

-- ----------------------------------------------------------------------------
-- Architecture Declaration
-- ----------------------------------------------------------------------------
architecture rtl of medianParallel4 is
	-- --------------------------------------------------------------------------
	-- Internal Signals
	-- --------------------------------------------------------------------------

	-- Clock(s) and Reset signal
	signal clocks, resets: std_logic_vector(0 downto 0);

	-- Network Input Port(s)
	signal ni_inPort_data : std_logic_vector(7 downto 0);
	signal ni_inPort_send : std_logic;
	signal ni_inPort_ack : std_logic;
	signal ni_inPort_rdy : std_logic;
	signal ni_inPort_count : std_logic_vector(15 downto 0);
	
	-- Network Input Port Fanout(s)
	signal nif_inPort_data : std_logic_vector(7 downto 0);
	signal nif_inPort_send : std_logic_vector(0 downto 0);
	signal nif_inPort_ack : std_logic_vector(0 downto 0);
	signal nif_inPort_rdy : std_logic_vector(0 downto 0);
	signal nif_inPort_count : std_logic_vector(15 downto 0);
	
	-- Network Output Port(s) 
	signal no_outPort_data : std_logic_vector(7 downto 0);
	signal no_outPort_send : std_logic;
	signal no_outPort_ack : std_logic;
	signal no_outPort_rdy : std_logic;
	signal no_outPort_count : std_logic_vector(15 downto 0);
	
	-- Actors Input/Output and Output fanout signals
	signal ai_split_in1_data : std_logic_vector(7 downto 0);
	signal ai_split_in1_send : std_logic;
	signal ai_split_in1_ack : std_logic;
	signal ai_split_in1_count : std_logic_vector(15 downto 0);
	
	signal ao_split_out1_data : std_logic_vector(7 downto 0);
	signal ao_split_out1_send : std_logic;
	signal ao_split_out1_ack : std_logic;
	signal ao_split_out1_rdy : std_logic;
	signal ao_split_out1_count : std_logic_vector(15 downto 0);
	
	signal aof_split_out1_data : std_logic_vector(7 downto 0);
	signal aof_split_out1_send : std_logic_vector(0 downto 0);
	signal aof_split_out1_ack : std_logic_vector(0 downto 0);
	signal aof_split_out1_rdy : std_logic_vector(0 downto 0);
	signal aof_split_out1_count : std_logic_vector(15 downto 0);
	
	signal ao_split_out2_data : std_logic_vector(7 downto 0);
	signal ao_split_out2_send : std_logic;
	signal ao_split_out2_ack : std_logic;
	signal ao_split_out2_rdy : std_logic;
	signal ao_split_out2_count : std_logic_vector(15 downto 0);
	
	signal aof_split_out2_data : std_logic_vector(7 downto 0);
	signal aof_split_out2_send : std_logic_vector(0 downto 0);
	signal aof_split_out2_ack : std_logic_vector(0 downto 0);
	signal aof_split_out2_rdy : std_logic_vector(0 downto 0);
	signal aof_split_out2_count : std_logic_vector(15 downto 0);
	
	signal ao_split_out3_data : std_logic_vector(7 downto 0);
	signal ao_split_out3_send : std_logic;
	signal ao_split_out3_ack : std_logic;
	signal ao_split_out3_rdy : std_logic;
	signal ao_split_out3_count : std_logic_vector(15 downto 0);
	
	signal aof_split_out3_data : std_logic_vector(7 downto 0);
	signal aof_split_out3_send : std_logic_vector(0 downto 0);
	signal aof_split_out3_ack : std_logic_vector(0 downto 0);
	signal aof_split_out3_rdy : std_logic_vector(0 downto 0);
	signal aof_split_out3_count : std_logic_vector(15 downto 0);
	
	signal ao_split_out4_data : std_logic_vector(7 downto 0);
	signal ao_split_out4_send : std_logic;
	signal ao_split_out4_ack : std_logic;
	signal ao_split_out4_rdy : std_logic;
	signal ao_split_out4_count : std_logic_vector(15 downto 0);
	
	signal aof_split_out4_data : std_logic_vector(7 downto 0);
	signal aof_split_out4_send : std_logic_vector(0 downto 0);
	signal aof_split_out4_ack : std_logic_vector(0 downto 0);
	signal aof_split_out4_rdy : std_logic_vector(0 downto 0);
	signal aof_split_out4_count : std_logic_vector(15 downto 0);
	
	signal ai_join_in1_data : std_logic_vector(7 downto 0);
	signal ai_join_in1_send : std_logic;
	signal ai_join_in1_ack : std_logic;
	signal ai_join_in1_count : std_logic_vector(15 downto 0);
	
	signal ai_join_in2_data : std_logic_vector(7 downto 0);
	signal ai_join_in2_send : std_logic;
	signal ai_join_in2_ack : std_logic;
	signal ai_join_in2_count : std_logic_vector(15 downto 0);
	
	signal ai_join_in3_data : std_logic_vector(7 downto 0);
	signal ai_join_in3_send : std_logic;
	signal ai_join_in3_ack : std_logic;
	signal ai_join_in3_count : std_logic_vector(15 downto 0);
	
	signal ai_join_in4_data : std_logic_vector(7 downto 0);
	signal ai_join_in4_send : std_logic;
	signal ai_join_in4_ack : std_logic;
	signal ai_join_in4_count : std_logic_vector(15 downto 0);
	
	signal ao_join_out1_data : std_logic_vector(7 downto 0);
	signal ao_join_out1_send : std_logic;
	signal ao_join_out1_ack : std_logic;
	signal ao_join_out1_rdy : std_logic;
	signal ao_join_out1_count : std_logic_vector(15 downto 0);
	
	signal aof_join_out1_data : std_logic_vector(7 downto 0);
	signal aof_join_out1_send : std_logic_vector(0 downto 0);
	signal aof_join_out1_ack : std_logic_vector(0 downto 0);
	signal aof_join_out1_rdy : std_logic_vector(0 downto 0);
	signal aof_join_out1_count : std_logic_vector(15 downto 0);
	
	signal ai_medianRow2_in1_data : std_logic_vector(7 downto 0);
	signal ai_medianRow2_in1_send : std_logic;
	signal ai_medianRow2_in1_ack : std_logic;
	signal ai_medianRow2_in1_count : std_logic_vector(15 downto 0);
	
	signal ao_medianRow2_median_data : std_logic_vector(7 downto 0);
	signal ao_medianRow2_median_send : std_logic;
	signal ao_medianRow2_median_ack : std_logic;
	signal ao_medianRow2_median_rdy : std_logic;
	signal ao_medianRow2_median_count : std_logic_vector(15 downto 0);
	
	signal aof_medianRow2_median_data : std_logic_vector(7 downto 0);
	signal aof_medianRow2_median_send : std_logic_vector(0 downto 0);
	signal aof_medianRow2_median_ack : std_logic_vector(0 downto 0);
	signal aof_medianRow2_median_rdy : std_logic_vector(0 downto 0);
	signal aof_medianRow2_median_count : std_logic_vector(15 downto 0);
	
	signal ai_medianRow1_in1_data : std_logic_vector(7 downto 0);
	signal ai_medianRow1_in1_send : std_logic;
	signal ai_medianRow1_in1_ack : std_logic;
	signal ai_medianRow1_in1_count : std_logic_vector(15 downto 0);
	
	signal ao_medianRow1_median_data : std_logic_vector(7 downto 0);
	signal ao_medianRow1_median_send : std_logic;
	signal ao_medianRow1_median_ack : std_logic;
	signal ao_medianRow1_median_rdy : std_logic;
	signal ao_medianRow1_median_count : std_logic_vector(15 downto 0);
	
	signal aof_medianRow1_median_data : std_logic_vector(7 downto 0);
	signal aof_medianRow1_median_send : std_logic_vector(0 downto 0);
	signal aof_medianRow1_median_ack : std_logic_vector(0 downto 0);
	signal aof_medianRow1_median_rdy : std_logic_vector(0 downto 0);
	signal aof_medianRow1_median_count : std_logic_vector(15 downto 0);
	
	signal ai_medianRow3_in1_data : std_logic_vector(7 downto 0);
	signal ai_medianRow3_in1_send : std_logic;
	signal ai_medianRow3_in1_ack : std_logic;
	signal ai_medianRow3_in1_count : std_logic_vector(15 downto 0);
	
	signal ao_medianRow3_median_data : std_logic_vector(7 downto 0);
	signal ao_medianRow3_median_send : std_logic;
	signal ao_medianRow3_median_ack : std_logic;
	signal ao_medianRow3_median_rdy : std_logic;
	signal ao_medianRow3_median_count : std_logic_vector(15 downto 0);
	
	signal aof_medianRow3_median_data : std_logic_vector(7 downto 0);
	signal aof_medianRow3_median_send : std_logic_vector(0 downto 0);
	signal aof_medianRow3_median_ack : std_logic_vector(0 downto 0);
	signal aof_medianRow3_median_rdy : std_logic_vector(0 downto 0);
	signal aof_medianRow3_median_count : std_logic_vector(15 downto 0);
	
	signal ai_medianRow4_in1_data : std_logic_vector(7 downto 0);
	signal ai_medianRow4_in1_send : std_logic;
	signal ai_medianRow4_in1_ack : std_logic;
	signal ai_medianRow4_in1_count : std_logic_vector(15 downto 0);
	
	signal ao_medianRow4_median_data : std_logic_vector(7 downto 0);
	signal ao_medianRow4_median_send : std_logic;
	signal ao_medianRow4_median_ack : std_logic;
	signal ao_medianRow4_median_rdy : std_logic;
	signal ao_medianRow4_median_count : std_logic_vector(15 downto 0);
	
	signal aof_medianRow4_median_data : std_logic_vector(7 downto 0);
	signal aof_medianRow4_median_send : std_logic_vector(0 downto 0);
	signal aof_medianRow4_median_ack : std_logic_vector(0 downto 0);
	signal aof_medianRow4_median_rdy : std_logic_vector(0 downto 0);
	signal aof_medianRow4_median_count : std_logic_vector(15 downto 0);

	-- --------------------------------------------------------------------------
	-- Network Instances
	-- --------------------------------------------------------------------------
	component split is
	port(
	     -- Instance split Input(s)
	     in1_data : in std_logic_vector(7 downto 0);
	     in1_send : in std_logic;
	     in1_ack : out std_logic;
	     in1_count : in std_logic_vector(15 downto 0);
	     -- Instance split Output(s)
	     out1_data : out std_logic_vector(7 downto 0);
	     out1_send : out std_logic;
	     out1_ack : in std_logic;
	     out1_rdy : in std_logic;
	     out1_count : out std_logic_vector(15 downto 0);
	     out2_data : out std_logic_vector(7 downto 0);
	     out2_send : out std_logic;
	     out2_ack : in std_logic;
	     out2_rdy : in std_logic;
	     out2_count : out std_logic_vector(15 downto 0);
	     out3_data : out std_logic_vector(7 downto 0);
	     out3_send : out std_logic;
	     out3_ack : in std_logic;
	     out3_rdy : in std_logic;
	     out3_count : out std_logic_vector(15 downto 0);
	     out4_data : out std_logic_vector(7 downto 0);
	     out4_send : out std_logic;
	     out4_ack : in std_logic;
	     out4_rdy : in std_logic;
	     out4_count : out std_logic_vector(15 downto 0);
	     clk: in std_logic;
	     reset: in std_logic);
	end component split;
	
	component join is
	port(
	     -- Instance join Input(s)
	     in1_data : in std_logic_vector(7 downto 0);
	     in1_send : in std_logic;
	     in1_ack : out std_logic;
	     in1_count : in std_logic_vector(15 downto 0);
	     in2_data : in std_logic_vector(7 downto 0);
	     in2_send : in std_logic;
	     in2_ack : out std_logic;
	     in2_count : in std_logic_vector(15 downto 0);
	     in3_data : in std_logic_vector(7 downto 0);
	     in3_send : in std_logic;
	     in3_ack : out std_logic;
	     in3_count : in std_logic_vector(15 downto 0);
	     in4_data : in std_logic_vector(7 downto 0);
	     in4_send : in std_logic;
	     in4_ack : out std_logic;
	     in4_count : in std_logic_vector(15 downto 0);
	     -- Instance join Output(s)
	     out1_data : out std_logic_vector(7 downto 0);
	     out1_send : out std_logic;
	     out1_ack : in std_logic;
	     out1_rdy : in std_logic;
	     out1_count : out std_logic_vector(15 downto 0);
	     clk: in std_logic;
	     reset: in std_logic);
	end component join;
	
	component medianRow2 is
	port(
	     -- Instance medianRow2 Input(s)
	     in1_data : in std_logic_vector(7 downto 0);
	     in1_send : in std_logic;
	     in1_ack : out std_logic;
	     in1_count : in std_logic_vector(15 downto 0);
	     -- Instance medianRow2 Output(s)
	     median_data : out std_logic_vector(7 downto 0);
	     median_send : out std_logic;
	     median_ack : in std_logic;
	     median_rdy : in std_logic;
	     median_count : out std_logic_vector(15 downto 0);
	     clk: in std_logic;
	     reset: in std_logic);
	end component medianRow2;
	
	component medianRow1 is
	port(
	     -- Instance medianRow1 Input(s)
	     in1_data : in std_logic_vector(7 downto 0);
	     in1_send : in std_logic;
	     in1_ack : out std_logic;
	     in1_count : in std_logic_vector(15 downto 0);
	     -- Instance medianRow1 Output(s)
	     median_data : out std_logic_vector(7 downto 0);
	     median_send : out std_logic;
	     median_ack : in std_logic;
	     median_rdy : in std_logic;
	     median_count : out std_logic_vector(15 downto 0);
	     clk: in std_logic;
	     reset: in std_logic);
	end component medianRow1;
	
	component medianRow3 is
	port(
	     -- Instance medianRow3 Input(s)
	     in1_data : in std_logic_vector(7 downto 0);
	     in1_send : in std_logic;
	     in1_ack : out std_logic;
	     in1_count : in std_logic_vector(15 downto 0);
	     -- Instance medianRow3 Output(s)
	     median_data : out std_logic_vector(7 downto 0);
	     median_send : out std_logic;
	     median_ack : in std_logic;
	     median_rdy : in std_logic;
	     median_count : out std_logic_vector(15 downto 0);
	     clk: in std_logic;
	     reset: in std_logic);
	end component medianRow3;
	
	component medianRow4 is
	port(
	     -- Instance medianRow4 Input(s)
	     in1_data : in std_logic_vector(7 downto 0);
	     in1_send : in std_logic;
	     in1_ack : out std_logic;
	     in1_count : in std_logic_vector(15 downto 0);
	     -- Instance medianRow4 Output(s)
	     median_data : out std_logic_vector(7 downto 0);
	     median_send : out std_logic;
	     median_ack : in std_logic;
	     median_rdy : in std_logic;
	     median_count : out std_logic_vector(15 downto 0);
	     clk: in std_logic;
	     reset: in std_logic);
	end component medianRow4;
	

begin
	-- Reset Controller
	rcon: entity SystemBuilder.resetController(behavioral)
	generic map(count => 1)
	port map( 
	         clocks => clocks, 
	         reset_in => reset, 
	         resets => resets);
	
	clocks(0) <= CLK;

	-- --------------------------------------------------------------------------
	-- Actor instances
	-- --------------------------------------------------------------------------
	i_split : component split
	port map(
		-- Instance split Input(s)
		in1_data => ai_split_in1_data,
		in1_send => ai_split_in1_send,
		in1_ack => ai_split_in1_ack,
		in1_count => ai_split_in1_count,
		-- Instance split Output(s)
		out1_data => ao_split_out1_data,
		out1_send => ao_split_out1_send,
		out1_ack => ao_split_out1_ack,
		out1_rdy => ao_split_out1_rdy,
		out1_count => ao_split_out1_count,
		
		out2_data => ao_split_out2_data,
		out2_send => ao_split_out2_send,
		out2_ack => ao_split_out2_ack,
		out2_rdy => ao_split_out2_rdy,
		out2_count => ao_split_out2_count,
		
		out3_data => ao_split_out3_data,
		out3_send => ao_split_out3_send,
		out3_ack => ao_split_out3_ack,
		out3_rdy => ao_split_out3_rdy,
		out3_count => ao_split_out3_count,
		
		out4_data => ao_split_out4_data,
		out4_send => ao_split_out4_send,
		out4_ack => ao_split_out4_ack,
		out4_rdy => ao_split_out4_rdy,
		out4_count => ao_split_out4_count,
		-- Clock and Reset
	clk => clocks(0),
	--clk => clocks(0),
	reset => resets(0));
	
	i_join : component join
	port map(
		-- Instance join Input(s)
		in1_data => ai_join_in1_data,
		in1_send => ai_join_in1_send,
		in1_ack => ai_join_in1_ack,
		in1_count => ai_join_in1_count,
		
		in2_data => ai_join_in2_data,
		in2_send => ai_join_in2_send,
		in2_ack => ai_join_in2_ack,
		in2_count => ai_join_in2_count,
		
		in3_data => ai_join_in3_data,
		in3_send => ai_join_in3_send,
		in3_ack => ai_join_in3_ack,
		in3_count => ai_join_in3_count,
		
		in4_data => ai_join_in4_data,
		in4_send => ai_join_in4_send,
		in4_ack => ai_join_in4_ack,
		in4_count => ai_join_in4_count,
		-- Instance join Output(s)
		out1_data => ao_join_out1_data,
		out1_send => ao_join_out1_send,
		out1_ack => ao_join_out1_ack,
		out1_rdy => ao_join_out1_rdy,
		out1_count => ao_join_out1_count,
		-- Clock and Reset
	clk => clocks(0),
	--clk => clocks(0),
	reset => resets(0));
	
	i_medianRow2 : component medianRow2
	port map(
		-- Instance medianRow2 Input(s)
		in1_data => ai_medianRow2_in1_data,
		in1_send => ai_medianRow2_in1_send,
		in1_ack => ai_medianRow2_in1_ack,
		in1_count => ai_medianRow2_in1_count,
		-- Instance medianRow2 Output(s)
		median_data => ao_medianRow2_median_data,
		median_send => ao_medianRow2_median_send,
		median_ack => ao_medianRow2_median_ack,
		median_rdy => ao_medianRow2_median_rdy,
		median_count => ao_medianRow2_median_count,
		-- Clock and Reset
	clk => clocks(0),
	--clk => clocks(0),
	reset => resets(0));
	
	i_medianRow1 : component medianRow1
	port map(
		-- Instance medianRow1 Input(s)
		in1_data => ai_medianRow1_in1_data,
		in1_send => ai_medianRow1_in1_send,
		in1_ack => ai_medianRow1_in1_ack,
		in1_count => ai_medianRow1_in1_count,
		-- Instance medianRow1 Output(s)
		median_data => ao_medianRow1_median_data,
		median_send => ao_medianRow1_median_send,
		median_ack => ao_medianRow1_median_ack,
		median_rdy => ao_medianRow1_median_rdy,
		median_count => ao_medianRow1_median_count,
		-- Clock and Reset
	clk => clocks(0),
	--clk => clocks(0),
	reset => resets(0));
	
	i_medianRow3 : component medianRow3
	port map(
		-- Instance medianRow3 Input(s)
		in1_data => ai_medianRow3_in1_data,
		in1_send => ai_medianRow3_in1_send,
		in1_ack => ai_medianRow3_in1_ack,
		in1_count => ai_medianRow3_in1_count,
		-- Instance medianRow3 Output(s)
		median_data => ao_medianRow3_median_data,
		median_send => ao_medianRow3_median_send,
		median_ack => ao_medianRow3_median_ack,
		median_rdy => ao_medianRow3_median_rdy,
		median_count => ao_medianRow3_median_count,
		-- Clock and Reset
	clk => clocks(0),
	--clk => clocks(0),
	reset => resets(0));
	
	i_medianRow4 : component medianRow4
	port map(
		-- Instance medianRow4 Input(s)
		in1_data => ai_medianRow4_in1_data,
		in1_send => ai_medianRow4_in1_send,
		in1_ack => ai_medianRow4_in1_ack,
		in1_count => ai_medianRow4_in1_count,
		-- Instance medianRow4 Output(s)
		median_data => ao_medianRow4_median_data,
		median_send => ao_medianRow4_median_send,
		median_ack => ao_medianRow4_median_ack,
		median_rdy => ao_medianRow4_median_rdy,
		median_count => ao_medianRow4_median_count,
		-- Clock and Reset
	clk => clocks(0),
	--clk => clocks(0),
	reset => resets(0));
	
	-- --------------------------------------------------------------------------
	-- Nework Input Fanouts
	-- --------------------------------------------------------------------------
	f_ni_inPort : entity SystemBuilder.Fanout(behavioral)
	generic map (fanout => 1, width => 8)
	port map(
		-- Fanout In
		In_data => ni_inPort_data,
		In_send => ni_inPort_send,
		In_ack => ni_inPort_ack,
		In_rdy => ni_inPort_rdy,
		In_count => ni_inPort_count,
		-- Fanout Out
		Out_data => nif_inPort_data,
		Out_send => nif_inPort_send,
		Out_ack => nif_inPort_ack,
		Out_rdy => nif_inPort_rdy,
		Out_count => nif_inPort_count,
		-- Clock & Reset
		clk => clocks(0),
		reset => resets(0));

	-- --------------------------------------------------------------------------
	-- Actor Output Fanouts
	-- --------------------------------------------------------------------------
	f_ao_split_out1 : entity SystemBuilder.Fanout(behavioral)
	generic map (fanout => 1, width => 8)
	port map(
		-- Fanout In
		In_data => ao_split_out1_data,
		In_send => ao_split_out1_send,
		In_ack => ao_split_out1_ack,
		In_rdy => ao_split_out1_rdy,
		In_count => ao_split_out1_count,
		-- Fanout Out
		Out_data => aof_split_out1_data,
		Out_send => aof_split_out1_send,
		Out_ack => aof_split_out1_ack,
		Out_rdy => aof_split_out1_rdy,
		Out_count => aof_split_out1_count,
		-- Clock & Reset
		clk => clocks(0),
		reset => resets(0));
	f_ao_split_out2 : entity SystemBuilder.Fanout(behavioral)
	generic map (fanout => 1, width => 8)
	port map(
		-- Fanout In
		In_data => ao_split_out2_data,
		In_send => ao_split_out2_send,
		In_ack => ao_split_out2_ack,
		In_rdy => ao_split_out2_rdy,
		In_count => ao_split_out2_count,
		-- Fanout Out
		Out_data => aof_split_out2_data,
		Out_send => aof_split_out2_send,
		Out_ack => aof_split_out2_ack,
		Out_rdy => aof_split_out2_rdy,
		Out_count => aof_split_out2_count,
		-- Clock & Reset
		clk => clocks(0),
		reset => resets(0));
	f_ao_split_out3 : entity SystemBuilder.Fanout(behavioral)
	generic map (fanout => 1, width => 8)
	port map(
		-- Fanout In
		In_data => ao_split_out3_data,
		In_send => ao_split_out3_send,
		In_ack => ao_split_out3_ack,
		In_rdy => ao_split_out3_rdy,
		In_count => ao_split_out3_count,
		-- Fanout Out
		Out_data => aof_split_out3_data,
		Out_send => aof_split_out3_send,
		Out_ack => aof_split_out3_ack,
		Out_rdy => aof_split_out3_rdy,
		Out_count => aof_split_out3_count,
		-- Clock & Reset
		clk => clocks(0),
		reset => resets(0));
	f_ao_split_out4 : entity SystemBuilder.Fanout(behavioral)
	generic map (fanout => 1, width => 8)
	port map(
		-- Fanout In
		In_data => ao_split_out4_data,
		In_send => ao_split_out4_send,
		In_ack => ao_split_out4_ack,
		In_rdy => ao_split_out4_rdy,
		In_count => ao_split_out4_count,
		-- Fanout Out
		Out_data => aof_split_out4_data,
		Out_send => aof_split_out4_send,
		Out_ack => aof_split_out4_ack,
		Out_rdy => aof_split_out4_rdy,
		Out_count => aof_split_out4_count,
		-- Clock & Reset
		clk => clocks(0),
		reset => resets(0));
	
	f_ao_join_out1 : entity SystemBuilder.Fanout(behavioral)
	generic map (fanout => 1, width => 8)
	port map(
		-- Fanout In
		In_data => ao_join_out1_data,
		In_send => ao_join_out1_send,
		In_ack => ao_join_out1_ack,
		In_rdy => ao_join_out1_rdy,
		In_count => ao_join_out1_count,
		-- Fanout Out
		Out_data => aof_join_out1_data,
		Out_send => aof_join_out1_send,
		Out_ack => aof_join_out1_ack,
		Out_rdy => aof_join_out1_rdy,
		Out_count => aof_join_out1_count,
		-- Clock & Reset
		clk => clocks(0),
		reset => resets(0));
	
	f_ao_medianRow2_median : entity SystemBuilder.Fanout(behavioral)
	generic map (fanout => 1, width => 8)
	port map(
		-- Fanout In
		In_data => ao_medianRow2_median_data,
		In_send => ao_medianRow2_median_send,
		In_ack => ao_medianRow2_median_ack,
		In_rdy => ao_medianRow2_median_rdy,
		In_count => ao_medianRow2_median_count,
		-- Fanout Out
		Out_data => aof_medianRow2_median_data,
		Out_send => aof_medianRow2_median_send,
		Out_ack => aof_medianRow2_median_ack,
		Out_rdy => aof_medianRow2_median_rdy,
		Out_count => aof_medianRow2_median_count,
		-- Clock & Reset
		clk => clocks(0),
		reset => resets(0));
	
	f_ao_medianRow1_median : entity SystemBuilder.Fanout(behavioral)
	generic map (fanout => 1, width => 8)
	port map(
		-- Fanout In
		In_data => ao_medianRow1_median_data,
		In_send => ao_medianRow1_median_send,
		In_ack => ao_medianRow1_median_ack,
		In_rdy => ao_medianRow1_median_rdy,
		In_count => ao_medianRow1_median_count,
		-- Fanout Out
		Out_data => aof_medianRow1_median_data,
		Out_send => aof_medianRow1_median_send,
		Out_ack => aof_medianRow1_median_ack,
		Out_rdy => aof_medianRow1_median_rdy,
		Out_count => aof_medianRow1_median_count,
		-- Clock & Reset
		clk => clocks(0),
		reset => resets(0));
	
	f_ao_medianRow3_median : entity SystemBuilder.Fanout(behavioral)
	generic map (fanout => 1, width => 8)
	port map(
		-- Fanout In
		In_data => ao_medianRow3_median_data,
		In_send => ao_medianRow3_median_send,
		In_ack => ao_medianRow3_median_ack,
		In_rdy => ao_medianRow3_median_rdy,
		In_count => ao_medianRow3_median_count,
		-- Fanout Out
		Out_data => aof_medianRow3_median_data,
		Out_send => aof_medianRow3_median_send,
		Out_ack => aof_medianRow3_median_ack,
		Out_rdy => aof_medianRow3_median_rdy,
		Out_count => aof_medianRow3_median_count,
		-- Clock & Reset
		clk => clocks(0),
		reset => resets(0));
	
	f_ao_medianRow4_median : entity SystemBuilder.Fanout(behavioral)
	generic map (fanout => 1, width => 8)
	port map(
		-- Fanout In
		In_data => ao_medianRow4_median_data,
		In_send => ao_medianRow4_median_send,
		In_ack => ao_medianRow4_median_ack,
		In_rdy => ao_medianRow4_median_rdy,
		In_count => ao_medianRow4_median_count,
		-- Fanout Out
		Out_data => aof_medianRow4_median_data,
		Out_send => aof_medianRow4_median_send,
		Out_ack => aof_medianRow4_median_ack,
		Out_rdy => aof_medianRow4_median_rdy,
		Out_count => aof_medianRow4_median_count,
		-- Clock & Reset
		clk => clocks(0),
		reset => resets(0));

	-- --------------------------------------------------------------------------
	-- Queues
	-- --------------------------------------------------------------------------
	q_ai_split_in1 : entity SystemBuilder.Queue(behavioral)
	generic map (length => 4096, width => 8)
	port map(
		-- Queue Out
		Out_data => ai_split_in1_data,
		Out_send => ai_split_in1_send,
		Out_ack => ai_split_in1_ack,
		Out_count => ai_split_in1_count,
		-- Queue In
		In_data => nif_inPort_data,
		In_send => nif_inPort_send(0),
		In_ack => nif_inPort_ack(0),
		In_rdy => nif_inPort_rdy(0),
		In_count => nif_inPort_count,
		-- Clock & Reset
		clk => clocks(0),
		reset => resets(0)
	);
	
	q_no_outPort : entity SystemBuilder.Queue(behavioral)
	generic map (length => 64, width => 8)
	port map(
		-- Queue Out
		Out_data => no_outPort_data,
		Out_send => no_outPort_send,
		Out_ack => no_outPort_ack,
		Out_count => no_outPort_count,
		-- Queue In
		In_data => aof_join_out1_data,
		In_send => aof_join_out1_send(0),
		In_ack => aof_join_out1_ack(0),
		In_rdy => aof_join_out1_rdy(0),
		In_count => aof_join_out1_count,
		-- Clock & Reset
		clk => clocks(0),
		reset => resets(0)
	);
	
	q_ai_medianRow1_in1 : entity SystemBuilder.Queue(behavioral)
	generic map (length => 4096, width => 8)
	port map(
		-- Queue Out
		Out_data => ai_medianRow1_in1_data,
		Out_send => ai_medianRow1_in1_send,
		Out_ack => ai_medianRow1_in1_ack,
		Out_count => ai_medianRow1_in1_count,
		-- Queue In
		In_data => aof_split_out1_data,
		In_send => aof_split_out1_send(0),
		In_ack => aof_split_out1_ack(0),
		In_rdy => aof_split_out1_rdy(0),
		In_count => aof_split_out1_count,
		-- Clock & Reset
		clk => clocks(0),
		reset => resets(0)
	);
	
	q_ai_join_in1 : entity SystemBuilder.Queue(behavioral)
	generic map (length => 4096, width => 8)
	port map(
		-- Queue Out
		Out_data => ai_join_in1_data,
		Out_send => ai_join_in1_send,
		Out_ack => ai_join_in1_ack,
		Out_count => ai_join_in1_count,
		-- Queue In
		In_data => aof_medianRow1_median_data,
		In_send => aof_medianRow1_median_send(0),
		In_ack => aof_medianRow1_median_ack(0),
		In_rdy => aof_medianRow1_median_rdy(0),
		In_count => aof_medianRow1_median_count,
		-- Clock & Reset
		clk => clocks(0),
		reset => resets(0)
	);
	
	q_ai_medianRow2_in1 : entity SystemBuilder.Queue(behavioral)
	generic map (length => 4096, width => 8)
	port map(
		-- Queue Out
		Out_data => ai_medianRow2_in1_data,
		Out_send => ai_medianRow2_in1_send,
		Out_ack => ai_medianRow2_in1_ack,
		Out_count => ai_medianRow2_in1_count,
		-- Queue In
		In_data => aof_split_out2_data,
		In_send => aof_split_out2_send(0),
		In_ack => aof_split_out2_ack(0),
		In_rdy => aof_split_out2_rdy(0),
		In_count => aof_split_out2_count,
		-- Clock & Reset
		clk => clocks(0),
		reset => resets(0)
	);
	
	q_ai_join_in2 : entity SystemBuilder.Queue(behavioral)
	generic map (length => 4096, width => 8)
	port map(
		-- Queue Out
		Out_data => ai_join_in2_data,
		Out_send => ai_join_in2_send,
		Out_ack => ai_join_in2_ack,
		Out_count => ai_join_in2_count,
		-- Queue In
		In_data => aof_medianRow2_median_data,
		In_send => aof_medianRow2_median_send(0),
		In_ack => aof_medianRow2_median_ack(0),
		In_rdy => aof_medianRow2_median_rdy(0),
		In_count => aof_medianRow2_median_count,
		-- Clock & Reset
		clk => clocks(0),
		reset => resets(0)
	);
	
	q_ai_medianRow3_in1 : entity SystemBuilder.Queue(behavioral)
	generic map (length => 4096, width => 8)
	port map(
		-- Queue Out
		Out_data => ai_medianRow3_in1_data,
		Out_send => ai_medianRow3_in1_send,
		Out_ack => ai_medianRow3_in1_ack,
		Out_count => ai_medianRow3_in1_count,
		-- Queue In
		In_data => aof_split_out3_data,
		In_send => aof_split_out3_send(0),
		In_ack => aof_split_out3_ack(0),
		In_rdy => aof_split_out3_rdy(0),
		In_count => aof_split_out3_count,
		-- Clock & Reset
		clk => clocks(0),
		reset => resets(0)
	);
	
	q_ai_join_in3 : entity SystemBuilder.Queue(behavioral)
	generic map (length => 4096, width => 8)
	port map(
		-- Queue Out
		Out_data => ai_join_in3_data,
		Out_send => ai_join_in3_send,
		Out_ack => ai_join_in3_ack,
		Out_count => ai_join_in3_count,
		-- Queue In
		In_data => aof_medianRow3_median_data,
		In_send => aof_medianRow3_median_send(0),
		In_ack => aof_medianRow3_median_ack(0),
		In_rdy => aof_medianRow3_median_rdy(0),
		In_count => aof_medianRow3_median_count,
		-- Clock & Reset
		clk => clocks(0),
		reset => resets(0)
	);
	
	q_ai_medianRow4_in1 : entity SystemBuilder.Queue(behavioral)
	generic map (length => 4096, width => 8)
	port map(
		-- Queue Out
		Out_data => ai_medianRow4_in1_data,
		Out_send => ai_medianRow4_in1_send,
		Out_ack => ai_medianRow4_in1_ack,
		Out_count => ai_medianRow4_in1_count,
		-- Queue In
		In_data => aof_split_out4_data,
		In_send => aof_split_out4_send(0),
		In_ack => aof_split_out4_ack(0),
		In_rdy => aof_split_out4_rdy(0),
		In_count => aof_split_out4_count,
		-- Clock & Reset
		clk => clocks(0),
		reset => resets(0)
	);
	
	q_ai_join_in4 : entity SystemBuilder.Queue(behavioral)
	generic map (length => 4096, width => 8)
	port map(
		-- Queue Out
		Out_data => ai_join_in4_data,
		Out_send => ai_join_in4_send,
		Out_ack => ai_join_in4_ack,
		Out_count => ai_join_in4_count,
		-- Queue In
		In_data => aof_medianRow4_median_data,
		In_send => aof_medianRow4_median_send(0),
		In_ack => aof_medianRow4_median_ack(0),
		In_rdy => aof_medianRow4_median_rdy(0),
		In_count => aof_medianRow4_median_count,
		-- Clock & Reset
		clk => clocks(0),
		reset => resets(0)
	);

	-- --------------------------------------------------------------------------
	-- Network port(s) instantiation
	-- --------------------------------------------------------------------------
	
	-- Output Port(s) Instantiation
	outPort_data <= no_outPort_data;
	outPort_send <= no_outPort_send;
	no_outPort_ack <= outPort_ack;
	no_outPort_rdy <= outPort_rdy;
	outPort_count <= no_outPort_count;
	
	-- Input Port(s) Instantiation
	ni_inPort_data <= inPort_data;
	ni_inPort_send <= inPort_send;
	inPort_ack <= ni_inPort_ack;
	inPort_rdy <= ni_inPort_rdy;
	ni_inPort_count <= inPort_count;
end architecture rtl;
-- ----------------------------------------------------------------------------
-- ----------------------------------------------------------------------------
-- ----------------------------------------------------------------------------
