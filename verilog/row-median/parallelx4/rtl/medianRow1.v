// __  ___ __ ___  _ __   ___  ___ 
// \ \/ / '__/ _ \| '_ \ / _ \/ __|
//  >  <| | | (_) | | | | (_) \__ \
// /_/\_\_|  \___/|_| |_|\___/|___/
// 
// Xronos synthesizer version
// Run date: Fri 23 Feb 2018 14:28:00 +0000
// 

module medianRow1(in1_SEND, median_ACK, in1_ACK, CLK, median_DATA, RESET, median_COUNT, in1_COUNT, in1_DATA, median_SEND, median_RDY);
input		in1_SEND;
input		median_ACK;
output		in1_ACK;
input		CLK;
wire		compute_median_done;
output	[7:0]	median_DATA;
input		RESET;
output	[15:0]	median_COUNT;
wire		receive_done;
wire		compute_median_go;
input	[15:0]	in1_COUNT;
wire		receive_go;
input	[7:0]	in1_DATA;
output		median_SEND;
input		median_RDY;
wire	[2:0]	bus_526385ac_;
wire	[31:0]	bus_24f4a4b8_;
wire		bus_600f8196_;
wire		bus_5c47816e_;
wire		bus_77a6e4fb_;
wire		bus_56efbe1c_;
wire	[31:0]	bus_4f47e3d7_;
wire	[31:0]	bus_29ca251b_;
wire	[31:0]	bus_2fdc550d_;
wire	[31:0]	bus_20d4651c_;
wire		bus_184441d7_;
wire		bus_3bd8fd6d_;
wire	[31:0]	bus_600ada8a_;
wire		bus_2d338235_;
wire		bus_7dd773e3_;
wire		bus_005e94ce_;
wire		bus_67ccda47_;
wire		bus_69180131_;
wire	[31:0]	bus_2cca9b16_;
wire	[31:0]	bus_44bf117c_;
wire		bus_42122957_;
wire	[31:0]	bus_06f4c3e3_;
wire	[2:0]	bus_3b6819e4_;
wire		bus_3bdd7ee2_;
wire		bus_1701b0e0_;
wire	[2:0]	receive_u250;
wire		receive;
wire	[31:0]	receive_u248;
wire		receive_u247;
wire		medianRow1_receive_instance_DONE;
wire	[31:0]	receive_u249;
wire		receive_u251;
wire	[31:0]	receive_u246;
wire	[31:0]	compute_median_u574;
wire		compute_median_u587;
wire		compute_median_u575;
wire	[15:0]	compute_median_u585;
wire	[31:0]	compute_median_u583;
wire		compute_median;
wire	[31:0]	compute_median_u576;
wire	[2:0]	compute_median_u581;
wire	[2:0]	compute_median_u584;
wire	[7:0]	compute_median_u586;
wire		compute_median_u582;
wire	[2:0]	compute_median_u577;
wire	[31:0]	compute_median_u579;
wire		medianRow1_compute_median_instance_DONE;
wire		compute_median_u578;
wire	[31:0]	compute_median_u580;
wire		medianRow1_scheduler_instance_DONE;
wire		scheduler_u764;
wire		scheduler_u765;
wire		scheduler;
wire		scheduler_u766;
assign in1_ACK=receive_u251;
assign compute_median_done=bus_3bdd7ee2_;
assign median_DATA=compute_median_u586;
assign median_COUNT=compute_median_u585;
assign receive_done=bus_005e94ce_;
assign compute_median_go=scheduler_u765;
assign receive_go=scheduler_u766;
assign median_SEND=compute_median_u587;
medianRow1_simplememoryreferee_3d10df12_ medianRow1_simplememoryreferee_3d10df12__1(.bus_593c68f3_(CLK), 
  .bus_4144ee68_(bus_1701b0e0_), .bus_49afd88a_(bus_3bd8fd6d_), .bus_0627f5fb_(bus_20d4651c_), 
  .bus_69b09619_(receive_u247), .bus_41bc3d23_({24'b0, receive_u249[7:0]}), .bus_43aba1fd_(receive_u248), 
  .bus_454be65f_(3'h1), .bus_496681a8_(compute_median_u575), .bus_012fe040_(compute_median_u578), 
  .bus_59b5e89a_(compute_median_u580), .bus_114412f5_(compute_median_u579), .bus_31dceff0_(3'h1), 
  .bus_24f4a4b8_(bus_24f4a4b8_), .bus_4f47e3d7_(bus_4f47e3d7_), .bus_77a6e4fb_(bus_77a6e4fb_), 
  .bus_56efbe1c_(bus_56efbe1c_), .bus_526385ac_(bus_526385ac_), .bus_5c47816e_(bus_5c47816e_), 
  .bus_29ca251b_(bus_29ca251b_), .bus_600f8196_(bus_600f8196_));
medianRow1_structuralmemory_315f90d8_ medianRow1_structuralmemory_315f90d8__1(.CLK_u87(CLK), 
  .bus_084457e9_(bus_1701b0e0_), .bus_6a58810d_(bus_44bf117c_), .bus_01e50d1e_(3'h1), 
  .bus_71c6914d_(bus_42122957_), .bus_1f4c50d7_(bus_4f47e3d7_), .bus_5739bb87_(3'h1), 
  .bus_117346ef_(bus_56efbe1c_), .bus_7ea553da_(bus_77a6e4fb_), .bus_1fab3e39_(bus_24f4a4b8_), 
  .bus_2fdc550d_(bus_2fdc550d_), .bus_184441d7_(bus_184441d7_), .bus_20d4651c_(bus_20d4651c_), 
  .bus_3bd8fd6d_(bus_3bd8fd6d_));
medianRow1_stateVar_i medianRow1_stateVar_i_1(.bus_61e233d8_(CLK), .bus_6b4c4e16_(bus_1701b0e0_), 
  .bus_0e0623eb_(receive), .bus_773f6029_(receive_u246), .bus_185bb10f_(compute_median), 
  .bus_3f20edda_(32'h0), .bus_600ada8a_(bus_600ada8a_));
medianRow1_stateVar_fsmState_medianRow1 medianRow1_stateVar_fsmState_medianRow1_1(.bus_4cf74991_(CLK), 
  .bus_6820df2f_(bus_1701b0e0_), .bus_08566f50_(scheduler), .bus_10ac6551_(scheduler_u764), 
  .bus_2d338235_(bus_2d338235_));
medianRow1_Kicker_51 medianRow1_Kicker_51_1(.CLK(CLK), .RESET(bus_1701b0e0_), .bus_7dd773e3_(bus_7dd773e3_));
assign bus_005e94ce_=medianRow1_receive_instance_DONE&{1{medianRow1_receive_instance_DONE}};
medianRow1_simplememoryreferee_65aacafe_ medianRow1_simplememoryreferee_65aacafe__1(.bus_744d7d6f_(CLK), 
  .bus_20e9aebb_(bus_1701b0e0_), .bus_661c8d09_(bus_184441d7_), .bus_21e59098_(bus_2fdc550d_), 
  .bus_57161f29_(compute_median_u582), .bus_46185a01_(compute_median_u583), .bus_41c86ead_(3'h1), 
  .bus_06f4c3e3_(bus_06f4c3e3_), .bus_44bf117c_(bus_44bf117c_), .bus_69180131_(bus_69180131_), 
  .bus_42122957_(bus_42122957_), .bus_3b6819e4_(bus_3b6819e4_), .bus_2cca9b16_(bus_2cca9b16_), 
  .bus_67ccda47_(bus_67ccda47_));
assign bus_3bdd7ee2_=medianRow1_compute_median_instance_DONE&{1{medianRow1_compute_median_instance_DONE}};
medianRow1_globalreset_physical_1ec5a0d8_ medianRow1_globalreset_physical_1ec5a0d8__1(.bus_62913fb0_(CLK), 
  .bus_78aca935_(RESET), .bus_1701b0e0_(bus_1701b0e0_));
medianRow1_receive medianRow1_receive_instance(.CLK(CLK), .RESET(bus_1701b0e0_), 
  .GO(receive_go), .port_0f2325b8_(bus_600ada8a_), .port_64a495de_(bus_5c47816e_), 
  .port_746d25a1_(in1_DATA), .DONE(medianRow1_receive_instance_DONE), .RESULT(receive), 
  .RESULT_u1943(receive_u246), .RESULT_u1944(receive_u247), .RESULT_u1945(receive_u248), 
  .RESULT_u1946(receive_u249), .RESULT_u1947(receive_u250), .RESULT_u1948(receive_u251));
medianRow1_compute_median medianRow1_compute_median_instance(.CLK(CLK), .RESET(bus_1701b0e0_), 
  .GO(compute_median_go), .port_27b7f1f6_(bus_600f8196_), .port_34368cc5_(bus_29ca251b_), 
  .port_43a1623f_(bus_600f8196_), .port_6829be08_(bus_67ccda47_), .port_152892fb_(bus_2cca9b16_), 
  .DONE(medianRow1_compute_median_instance_DONE), .RESULT(compute_median), .RESULT_u1949(compute_median_u574), 
  .RESULT_u1953(compute_median_u575), .RESULT_u1954(compute_median_u576), .RESULT_u1955(compute_median_u577), 
  .RESULT_u1956(compute_median_u578), .RESULT_u1957(compute_median_u579), .RESULT_u1958(compute_median_u580), 
  .RESULT_u1959(compute_median_u581), .RESULT_u1950(compute_median_u582), .RESULT_u1951(compute_median_u583), 
  .RESULT_u1952(compute_median_u584), .RESULT_u1960(compute_median_u585), .RESULT_u1961(compute_median_u586), 
  .RESULT_u1962(compute_median_u587));
medianRow1_scheduler medianRow1_scheduler_instance(.CLK(CLK), .RESET(bus_1701b0e0_), 
  .GO(bus_7dd773e3_), .port_6074fca8_(bus_2d338235_), .port_0eecd18d_(bus_600ada8a_), 
  .port_18f257dd_(receive_done), .port_00718719_(in1_SEND), .port_3e61550a_(compute_median_done), 
  .port_6c8d7d8b_(median_RDY), .DONE(medianRow1_scheduler_instance_DONE), .RESULT(scheduler), 
  .RESULT_u1963(scheduler_u764), .RESULT_u1964(scheduler_u765), .RESULT_u1965(scheduler_u766));
endmodule



module medianRow1_simplememoryreferee_3d10df12_(bus_593c68f3_, bus_4144ee68_, bus_49afd88a_, bus_0627f5fb_, bus_69b09619_, bus_41bc3d23_, bus_43aba1fd_, bus_454be65f_, bus_496681a8_, bus_012fe040_, bus_59b5e89a_, bus_114412f5_, bus_31dceff0_, bus_24f4a4b8_, bus_4f47e3d7_, bus_77a6e4fb_, bus_56efbe1c_, bus_526385ac_, bus_5c47816e_, bus_29ca251b_, bus_600f8196_);
input		bus_593c68f3_;
input		bus_4144ee68_;
input		bus_49afd88a_;
input	[31:0]	bus_0627f5fb_;
input		bus_69b09619_;
input	[31:0]	bus_41bc3d23_;
input	[31:0]	bus_43aba1fd_;
input	[2:0]	bus_454be65f_;
input		bus_496681a8_;
input		bus_012fe040_;
input	[31:0]	bus_59b5e89a_;
input	[31:0]	bus_114412f5_;
input	[2:0]	bus_31dceff0_;
output	[31:0]	bus_24f4a4b8_;
output	[31:0]	bus_4f47e3d7_;
output		bus_77a6e4fb_;
output		bus_56efbe1c_;
output	[2:0]	bus_526385ac_;
output		bus_5c47816e_;
output	[31:0]	bus_29ca251b_;
output		bus_600f8196_;
reg		done_qual_u210=1'h0;
wire		or_46756a73_u0;
wire		or_14f9f3b1_u0;
wire		and_2646c78d_u0;
wire		or_782cf082_u0;
wire		not_03dc3672_u0;
reg		done_qual_u211_u0=1'h0;
wire		or_2c58186c_u0;
wire	[31:0]	mux_4fdd262a_u0;
wire		not_17c2e57a_u0;
wire		or_0f03443a_u0;
wire		and_738adff6_u0;
wire	[31:0]	mux_7c4e63f3_u0;
always @(posedge bus_593c68f3_)
begin
if (bus_4144ee68_)
done_qual_u210<=1'h0;
else done_qual_u210<=bus_69b09619_;
end
assign or_46756a73_u0=bus_69b09619_|bus_012fe040_;
assign or_14f9f3b1_u0=bus_69b09619_|done_qual_u210;
assign and_2646c78d_u0=or_14f9f3b1_u0&bus_49afd88a_;
assign or_782cf082_u0=bus_496681a8_|bus_012fe040_;
assign not_03dc3672_u0=~bus_49afd88a_;
assign bus_24f4a4b8_=mux_4fdd262a_u0;
assign bus_4f47e3d7_=mux_7c4e63f3_u0;
assign bus_77a6e4fb_=or_46756a73_u0;
assign bus_56efbe1c_=or_0f03443a_u0;
assign bus_526385ac_=3'h1;
assign bus_5c47816e_=and_2646c78d_u0;
assign bus_29ca251b_=bus_0627f5fb_;
assign bus_600f8196_=and_738adff6_u0;
always @(posedge bus_593c68f3_)
begin
if (bus_4144ee68_)
done_qual_u211_u0<=1'h0;
else done_qual_u211_u0<=or_782cf082_u0;
end
assign or_2c58186c_u0=or_782cf082_u0|done_qual_u211_u0;
assign mux_4fdd262a_u0=(bus_69b09619_)?{24'b0, bus_41bc3d23_[7:0]}:bus_59b5e89a_;
assign not_17c2e57a_u0=~bus_49afd88a_;
assign or_0f03443a_u0=bus_69b09619_|or_782cf082_u0;
assign and_738adff6_u0=or_2c58186c_u0&bus_49afd88a_;
assign mux_7c4e63f3_u0=(bus_69b09619_)?bus_43aba1fd_:bus_114412f5_;
endmodule



module medianRow1_forge_memory_101x32_135(CLK, ENA, WEA, DINA, ENB, ADDRA, ADDRB, DOUTA, DOUTB, DONEA, DONEB);
input		CLK;
input		ENA;
input		WEA;
input	[31:0]	DINA;
input		ENB;
input	[31:0]	ADDRA;
input	[31:0]	ADDRB;
output	[31:0]	DOUTA;
output	[31:0]	DOUTB;
output		DONEA;
output		DONEB;
wire		wea_0;
wire	[31:0]	pre_douta_0;
wire	[31:0]	pre_doutb_0;
wire		wea_1;
wire	[31:0]	pre_douta_1;
wire	[31:0]	pre_doutb_1;
reg		wea_done;
reg		rea_done;
reg	[31:0]	mux_outa;
reg	[31:0]	mux_outb;
assign wea_0=WEA&(ADDRA[31:6]==26'h0);
assign wea_1=WEA&(ADDRA[31:6]==26'h1);
always @(posedge CLK)
begin
wea_done<=WEA;
end
assign DONEA=rea_done|wea_done;
always @(posedge CLK)
begin
end
assign DONEB=ENB;
always @(pre_douta_0 or pre_douta_1 or ADDRA)
begin
case (ADDRA[31:6])26'd0:mux_outa=pre_douta_0;
26'd1:mux_outa=pre_douta_1;
default:mux_outa=32'h0;
endcase end
assign DOUTA=mux_outa;
always @(pre_doutb_0 or pre_doutb_1 or ADDRB)
begin
case (ADDRB[31:6])26'd0:mux_outb=pre_doutb_0;
26'd1:mux_outb=pre_doutb_1;
default:mux_outb=32'h0;
endcase end
assign DOUTB=mux_outb;
// Memory array element: COL: 0, ROW: 0
//  Initialization of Dual Port LUT ram now done through explicit
// parameter setting.
RAM64X1D#(.INIT(64'h0000000000000000))RAM64X1D_instance_2624(.WCLK(CLK), .WE(wea_0), 
  .A0(ADDRA[0]), .A1(ADDRA[1]), .A2(ADDRA[2]), .A3(ADDRA[3]), .A4(ADDRA[4]), .A5(ADDRA[5]), 
  .D(DINA[0]), .SPO(pre_douta_0[0]), .DPRA0(ADDRB[0]), .DPRA1(ADDRB[1]), .DPRA2(ADDRB[2]), 
  .DPRA3(ADDRB[3]), .DPRA4(ADDRB[4]), .DPRA5(ADDRB[5]), .DPO(pre_doutb_0[0]));
// Memory array element: COL: 0, ROW: 1
//  Initialization of Dual Port LUT ram now done through explicit
// parameter setting.
RAM64X1D#(.INIT(64'h0000000000000000))RAM64X1D_instance_2625(.WCLK(CLK), .WE(wea_0), 
  .A0(ADDRA[0]), .A1(ADDRA[1]), .A2(ADDRA[2]), .A3(ADDRA[3]), .A4(ADDRA[4]), .A5(ADDRA[5]), 
  .D(DINA[1]), .SPO(pre_douta_0[1]), .DPRA0(ADDRB[0]), .DPRA1(ADDRB[1]), .DPRA2(ADDRB[2]), 
  .DPRA3(ADDRB[3]), .DPRA4(ADDRB[4]), .DPRA5(ADDRB[5]), .DPO(pre_doutb_0[1]));
// Memory array element: COL: 0, ROW: 2
//  Initialization of Dual Port LUT ram now done through explicit
// parameter setting.
RAM64X1D#(.INIT(64'h0000000000000000))RAM64X1D_instance_2626(.WCLK(CLK), .WE(wea_0), 
  .A0(ADDRA[0]), .A1(ADDRA[1]), .A2(ADDRA[2]), .A3(ADDRA[3]), .A4(ADDRA[4]), .A5(ADDRA[5]), 
  .D(DINA[2]), .SPO(pre_douta_0[2]), .DPRA0(ADDRB[0]), .DPRA1(ADDRB[1]), .DPRA2(ADDRB[2]), 
  .DPRA3(ADDRB[3]), .DPRA4(ADDRB[4]), .DPRA5(ADDRB[5]), .DPO(pre_doutb_0[2]));
// Memory array element: COL: 0, ROW: 3
//  Initialization of Dual Port LUT ram now done through explicit
// parameter setting.
RAM64X1D#(.INIT(64'h0000000000000000))RAM64X1D_instance_2627(.WCLK(CLK), .WE(wea_0), 
  .A0(ADDRA[0]), .A1(ADDRA[1]), .A2(ADDRA[2]), .A3(ADDRA[3]), .A4(ADDRA[4]), .A5(ADDRA[5]), 
  .D(DINA[3]), .SPO(pre_douta_0[3]), .DPRA0(ADDRB[0]), .DPRA1(ADDRB[1]), .DPRA2(ADDRB[2]), 
  .DPRA3(ADDRB[3]), .DPRA4(ADDRB[4]), .DPRA5(ADDRB[5]), .DPO(pre_doutb_0[3]));
// Memory array element: COL: 0, ROW: 4
//  Initialization of Dual Port LUT ram now done through explicit
// parameter setting.
RAM64X1D#(.INIT(64'h0000000000000000))RAM64X1D_instance_2628(.WCLK(CLK), .WE(wea_0), 
  .A0(ADDRA[0]), .A1(ADDRA[1]), .A2(ADDRA[2]), .A3(ADDRA[3]), .A4(ADDRA[4]), .A5(ADDRA[5]), 
  .D(DINA[4]), .SPO(pre_douta_0[4]), .DPRA0(ADDRB[0]), .DPRA1(ADDRB[1]), .DPRA2(ADDRB[2]), 
  .DPRA3(ADDRB[3]), .DPRA4(ADDRB[4]), .DPRA5(ADDRB[5]), .DPO(pre_doutb_0[4]));
// Memory array element: COL: 0, ROW: 5
//  Initialization of Dual Port LUT ram now done through explicit
// parameter setting.
RAM64X1D#(.INIT(64'h0000000000000000))RAM64X1D_instance_2629(.WCLK(CLK), .WE(wea_0), 
  .A0(ADDRA[0]), .A1(ADDRA[1]), .A2(ADDRA[2]), .A3(ADDRA[3]), .A4(ADDRA[4]), .A5(ADDRA[5]), 
  .D(DINA[5]), .SPO(pre_douta_0[5]), .DPRA0(ADDRB[0]), .DPRA1(ADDRB[1]), .DPRA2(ADDRB[2]), 
  .DPRA3(ADDRB[3]), .DPRA4(ADDRB[4]), .DPRA5(ADDRB[5]), .DPO(pre_doutb_0[5]));
// Memory array element: COL: 0, ROW: 6
//  Initialization of Dual Port LUT ram now done through explicit
// parameter setting.
RAM64X1D#(.INIT(64'h0000000000000000))RAM64X1D_instance_2630(.WCLK(CLK), .WE(wea_0), 
  .A0(ADDRA[0]), .A1(ADDRA[1]), .A2(ADDRA[2]), .A3(ADDRA[3]), .A4(ADDRA[4]), .A5(ADDRA[5]), 
  .D(DINA[6]), .SPO(pre_douta_0[6]), .DPRA0(ADDRB[0]), .DPRA1(ADDRB[1]), .DPRA2(ADDRB[2]), 
  .DPRA3(ADDRB[3]), .DPRA4(ADDRB[4]), .DPRA5(ADDRB[5]), .DPO(pre_doutb_0[6]));
// Memory array element: COL: 0, ROW: 7
//  Initialization of Dual Port LUT ram now done through explicit
// parameter setting.
RAM64X1D#(.INIT(64'h0000000000000000))RAM64X1D_instance_2631(.WCLK(CLK), .WE(wea_0), 
  .A0(ADDRA[0]), .A1(ADDRA[1]), .A2(ADDRA[2]), .A3(ADDRA[3]), .A4(ADDRA[4]), .A5(ADDRA[5]), 
  .D(DINA[7]), .SPO(pre_douta_0[7]), .DPRA0(ADDRB[0]), .DPRA1(ADDRB[1]), .DPRA2(ADDRB[2]), 
  .DPRA3(ADDRB[3]), .DPRA4(ADDRB[4]), .DPRA5(ADDRB[5]), .DPO(pre_doutb_0[7]));
// Memory array element: COL: 0, ROW: 8
//  Initialization of Dual Port LUT ram now done through explicit
// parameter setting.
RAM64X1D#(.INIT(64'h0000000000000000))RAM64X1D_instance_2632(.WCLK(CLK), .WE(wea_0), 
  .A0(ADDRA[0]), .A1(ADDRA[1]), .A2(ADDRA[2]), .A3(ADDRA[3]), .A4(ADDRA[4]), .A5(ADDRA[5]), 
  .D(DINA[8]), .SPO(pre_douta_0[8]), .DPRA0(ADDRB[0]), .DPRA1(ADDRB[1]), .DPRA2(ADDRB[2]), 
  .DPRA3(ADDRB[3]), .DPRA4(ADDRB[4]), .DPRA5(ADDRB[5]), .DPO(pre_doutb_0[8]));
// Memory array element: COL: 0, ROW: 9
//  Initialization of Dual Port LUT ram now done through explicit
// parameter setting.
RAM64X1D#(.INIT(64'h0000000000000000))RAM64X1D_instance_2633(.WCLK(CLK), .WE(wea_0), 
  .A0(ADDRA[0]), .A1(ADDRA[1]), .A2(ADDRA[2]), .A3(ADDRA[3]), .A4(ADDRA[4]), .A5(ADDRA[5]), 
  .D(DINA[9]), .SPO(pre_douta_0[9]), .DPRA0(ADDRB[0]), .DPRA1(ADDRB[1]), .DPRA2(ADDRB[2]), 
  .DPRA3(ADDRB[3]), .DPRA4(ADDRB[4]), .DPRA5(ADDRB[5]), .DPO(pre_doutb_0[9]));
// Memory array element: COL: 0, ROW: 10
//  Initialization of Dual Port LUT ram now done through explicit
// parameter setting.
RAM64X1D#(.INIT(64'h0000000000000000))RAM64X1D_instance_2634(.WCLK(CLK), .WE(wea_0), 
  .A0(ADDRA[0]), .A1(ADDRA[1]), .A2(ADDRA[2]), .A3(ADDRA[3]), .A4(ADDRA[4]), .A5(ADDRA[5]), 
  .D(DINA[10]), .SPO(pre_douta_0[10]), .DPRA0(ADDRB[0]), .DPRA1(ADDRB[1]), .DPRA2(ADDRB[2]), 
  .DPRA3(ADDRB[3]), .DPRA4(ADDRB[4]), .DPRA5(ADDRB[5]), .DPO(pre_doutb_0[10]));
// Memory array element: COL: 0, ROW: 11
//  Initialization of Dual Port LUT ram now done through explicit
// parameter setting.
RAM64X1D#(.INIT(64'h0000000000000000))RAM64X1D_instance_2635(.WCLK(CLK), .WE(wea_0), 
  .A0(ADDRA[0]), .A1(ADDRA[1]), .A2(ADDRA[2]), .A3(ADDRA[3]), .A4(ADDRA[4]), .A5(ADDRA[5]), 
  .D(DINA[11]), .SPO(pre_douta_0[11]), .DPRA0(ADDRB[0]), .DPRA1(ADDRB[1]), .DPRA2(ADDRB[2]), 
  .DPRA3(ADDRB[3]), .DPRA4(ADDRB[4]), .DPRA5(ADDRB[5]), .DPO(pre_doutb_0[11]));
// Memory array element: COL: 0, ROW: 12
//  Initialization of Dual Port LUT ram now done through explicit
// parameter setting.
RAM64X1D#(.INIT(64'h0000000000000000))RAM64X1D_instance_2636(.WCLK(CLK), .WE(wea_0), 
  .A0(ADDRA[0]), .A1(ADDRA[1]), .A2(ADDRA[2]), .A3(ADDRA[3]), .A4(ADDRA[4]), .A5(ADDRA[5]), 
  .D(DINA[12]), .SPO(pre_douta_0[12]), .DPRA0(ADDRB[0]), .DPRA1(ADDRB[1]), .DPRA2(ADDRB[2]), 
  .DPRA3(ADDRB[3]), .DPRA4(ADDRB[4]), .DPRA5(ADDRB[5]), .DPO(pre_doutb_0[12]));
// Memory array element: COL: 0, ROW: 13
//  Initialization of Dual Port LUT ram now done through explicit
// parameter setting.
RAM64X1D#(.INIT(64'h0000000000000000))RAM64X1D_instance_2637(.WCLK(CLK), .WE(wea_0), 
  .A0(ADDRA[0]), .A1(ADDRA[1]), .A2(ADDRA[2]), .A3(ADDRA[3]), .A4(ADDRA[4]), .A5(ADDRA[5]), 
  .D(DINA[13]), .SPO(pre_douta_0[13]), .DPRA0(ADDRB[0]), .DPRA1(ADDRB[1]), .DPRA2(ADDRB[2]), 
  .DPRA3(ADDRB[3]), .DPRA4(ADDRB[4]), .DPRA5(ADDRB[5]), .DPO(pre_doutb_0[13]));
// Memory array element: COL: 0, ROW: 14
//  Initialization of Dual Port LUT ram now done through explicit
// parameter setting.
RAM64X1D#(.INIT(64'h0000000000000000))RAM64X1D_instance_2638(.WCLK(CLK), .WE(wea_0), 
  .A0(ADDRA[0]), .A1(ADDRA[1]), .A2(ADDRA[2]), .A3(ADDRA[3]), .A4(ADDRA[4]), .A5(ADDRA[5]), 
  .D(DINA[14]), .SPO(pre_douta_0[14]), .DPRA0(ADDRB[0]), .DPRA1(ADDRB[1]), .DPRA2(ADDRB[2]), 
  .DPRA3(ADDRB[3]), .DPRA4(ADDRB[4]), .DPRA5(ADDRB[5]), .DPO(pre_doutb_0[14]));
// Memory array element: COL: 0, ROW: 15
//  Initialization of Dual Port LUT ram now done through explicit
// parameter setting.
RAM64X1D#(.INIT(64'h0000000000000000))RAM64X1D_instance_2639(.WCLK(CLK), .WE(wea_0), 
  .A0(ADDRA[0]), .A1(ADDRA[1]), .A2(ADDRA[2]), .A3(ADDRA[3]), .A4(ADDRA[4]), .A5(ADDRA[5]), 
  .D(DINA[15]), .SPO(pre_douta_0[15]), .DPRA0(ADDRB[0]), .DPRA1(ADDRB[1]), .DPRA2(ADDRB[2]), 
  .DPRA3(ADDRB[3]), .DPRA4(ADDRB[4]), .DPRA5(ADDRB[5]), .DPO(pre_doutb_0[15]));
// Memory array element: COL: 0, ROW: 16
//  Initialization of Dual Port LUT ram now done through explicit
// parameter setting.
RAM64X1D#(.INIT(64'h0000000000000000))RAM64X1D_instance_2640(.WCLK(CLK), .WE(wea_0), 
  .A0(ADDRA[0]), .A1(ADDRA[1]), .A2(ADDRA[2]), .A3(ADDRA[3]), .A4(ADDRA[4]), .A5(ADDRA[5]), 
  .D(DINA[16]), .SPO(pre_douta_0[16]), .DPRA0(ADDRB[0]), .DPRA1(ADDRB[1]), .DPRA2(ADDRB[2]), 
  .DPRA3(ADDRB[3]), .DPRA4(ADDRB[4]), .DPRA5(ADDRB[5]), .DPO(pre_doutb_0[16]));
// Memory array element: COL: 0, ROW: 17
//  Initialization of Dual Port LUT ram now done through explicit
// parameter setting.
RAM64X1D#(.INIT(64'h0000000000000000))RAM64X1D_instance_2641(.WCLK(CLK), .WE(wea_0), 
  .A0(ADDRA[0]), .A1(ADDRA[1]), .A2(ADDRA[2]), .A3(ADDRA[3]), .A4(ADDRA[4]), .A5(ADDRA[5]), 
  .D(DINA[17]), .SPO(pre_douta_0[17]), .DPRA0(ADDRB[0]), .DPRA1(ADDRB[1]), .DPRA2(ADDRB[2]), 
  .DPRA3(ADDRB[3]), .DPRA4(ADDRB[4]), .DPRA5(ADDRB[5]), .DPO(pre_doutb_0[17]));
// Memory array element: COL: 0, ROW: 18
//  Initialization of Dual Port LUT ram now done through explicit
// parameter setting.
RAM64X1D#(.INIT(64'h0000000000000000))RAM64X1D_instance_2642(.WCLK(CLK), .WE(wea_0), 
  .A0(ADDRA[0]), .A1(ADDRA[1]), .A2(ADDRA[2]), .A3(ADDRA[3]), .A4(ADDRA[4]), .A5(ADDRA[5]), 
  .D(DINA[18]), .SPO(pre_douta_0[18]), .DPRA0(ADDRB[0]), .DPRA1(ADDRB[1]), .DPRA2(ADDRB[2]), 
  .DPRA3(ADDRB[3]), .DPRA4(ADDRB[4]), .DPRA5(ADDRB[5]), .DPO(pre_doutb_0[18]));
// Memory array element: COL: 0, ROW: 19
//  Initialization of Dual Port LUT ram now done through explicit
// parameter setting.
RAM64X1D#(.INIT(64'h0000000000000000))RAM64X1D_instance_2643(.WCLK(CLK), .WE(wea_0), 
  .A0(ADDRA[0]), .A1(ADDRA[1]), .A2(ADDRA[2]), .A3(ADDRA[3]), .A4(ADDRA[4]), .A5(ADDRA[5]), 
  .D(DINA[19]), .SPO(pre_douta_0[19]), .DPRA0(ADDRB[0]), .DPRA1(ADDRB[1]), .DPRA2(ADDRB[2]), 
  .DPRA3(ADDRB[3]), .DPRA4(ADDRB[4]), .DPRA5(ADDRB[5]), .DPO(pre_doutb_0[19]));
// Memory array element: COL: 0, ROW: 20
//  Initialization of Dual Port LUT ram now done through explicit
// parameter setting.
RAM64X1D#(.INIT(64'h0000000000000000))RAM64X1D_instance_2644(.WCLK(CLK), .WE(wea_0), 
  .A0(ADDRA[0]), .A1(ADDRA[1]), .A2(ADDRA[2]), .A3(ADDRA[3]), .A4(ADDRA[4]), .A5(ADDRA[5]), 
  .D(DINA[20]), .SPO(pre_douta_0[20]), .DPRA0(ADDRB[0]), .DPRA1(ADDRB[1]), .DPRA2(ADDRB[2]), 
  .DPRA3(ADDRB[3]), .DPRA4(ADDRB[4]), .DPRA5(ADDRB[5]), .DPO(pre_doutb_0[20]));
// Memory array element: COL: 0, ROW: 21
//  Initialization of Dual Port LUT ram now done through explicit
// parameter setting.
RAM64X1D#(.INIT(64'h0000000000000000))RAM64X1D_instance_2645(.WCLK(CLK), .WE(wea_0), 
  .A0(ADDRA[0]), .A1(ADDRA[1]), .A2(ADDRA[2]), .A3(ADDRA[3]), .A4(ADDRA[4]), .A5(ADDRA[5]), 
  .D(DINA[21]), .SPO(pre_douta_0[21]), .DPRA0(ADDRB[0]), .DPRA1(ADDRB[1]), .DPRA2(ADDRB[2]), 
  .DPRA3(ADDRB[3]), .DPRA4(ADDRB[4]), .DPRA5(ADDRB[5]), .DPO(pre_doutb_0[21]));
// Memory array element: COL: 0, ROW: 22
//  Initialization of Dual Port LUT ram now done through explicit
// parameter setting.
RAM64X1D#(.INIT(64'h0000000000000000))RAM64X1D_instance_2646(.WCLK(CLK), .WE(wea_0), 
  .A0(ADDRA[0]), .A1(ADDRA[1]), .A2(ADDRA[2]), .A3(ADDRA[3]), .A4(ADDRA[4]), .A5(ADDRA[5]), 
  .D(DINA[22]), .SPO(pre_douta_0[22]), .DPRA0(ADDRB[0]), .DPRA1(ADDRB[1]), .DPRA2(ADDRB[2]), 
  .DPRA3(ADDRB[3]), .DPRA4(ADDRB[4]), .DPRA5(ADDRB[5]), .DPO(pre_doutb_0[22]));
// Memory array element: COL: 0, ROW: 23
//  Initialization of Dual Port LUT ram now done through explicit
// parameter setting.
RAM64X1D#(.INIT(64'h0000000000000000))RAM64X1D_instance_2647(.WCLK(CLK), .WE(wea_0), 
  .A0(ADDRA[0]), .A1(ADDRA[1]), .A2(ADDRA[2]), .A3(ADDRA[3]), .A4(ADDRA[4]), .A5(ADDRA[5]), 
  .D(DINA[23]), .SPO(pre_douta_0[23]), .DPRA0(ADDRB[0]), .DPRA1(ADDRB[1]), .DPRA2(ADDRB[2]), 
  .DPRA3(ADDRB[3]), .DPRA4(ADDRB[4]), .DPRA5(ADDRB[5]), .DPO(pre_doutb_0[23]));
// Memory array element: COL: 0, ROW: 24
//  Initialization of Dual Port LUT ram now done through explicit
// parameter setting.
RAM64X1D#(.INIT(64'h0000000000000000))RAM64X1D_instance_2648(.WCLK(CLK), .WE(wea_0), 
  .A0(ADDRA[0]), .A1(ADDRA[1]), .A2(ADDRA[2]), .A3(ADDRA[3]), .A4(ADDRA[4]), .A5(ADDRA[5]), 
  .D(DINA[24]), .SPO(pre_douta_0[24]), .DPRA0(ADDRB[0]), .DPRA1(ADDRB[1]), .DPRA2(ADDRB[2]), 
  .DPRA3(ADDRB[3]), .DPRA4(ADDRB[4]), .DPRA5(ADDRB[5]), .DPO(pre_doutb_0[24]));
// Memory array element: COL: 0, ROW: 25
//  Initialization of Dual Port LUT ram now done through explicit
// parameter setting.
RAM64X1D#(.INIT(64'h0000000000000000))RAM64X1D_instance_2649(.WCLK(CLK), .WE(wea_0), 
  .A0(ADDRA[0]), .A1(ADDRA[1]), .A2(ADDRA[2]), .A3(ADDRA[3]), .A4(ADDRA[4]), .A5(ADDRA[5]), 
  .D(DINA[25]), .SPO(pre_douta_0[25]), .DPRA0(ADDRB[0]), .DPRA1(ADDRB[1]), .DPRA2(ADDRB[2]), 
  .DPRA3(ADDRB[3]), .DPRA4(ADDRB[4]), .DPRA5(ADDRB[5]), .DPO(pre_doutb_0[25]));
// Memory array element: COL: 0, ROW: 26
//  Initialization of Dual Port LUT ram now done through explicit
// parameter setting.
RAM64X1D#(.INIT(64'h0000000000000000))RAM64X1D_instance_2650(.WCLK(CLK), .WE(wea_0), 
  .A0(ADDRA[0]), .A1(ADDRA[1]), .A2(ADDRA[2]), .A3(ADDRA[3]), .A4(ADDRA[4]), .A5(ADDRA[5]), 
  .D(DINA[26]), .SPO(pre_douta_0[26]), .DPRA0(ADDRB[0]), .DPRA1(ADDRB[1]), .DPRA2(ADDRB[2]), 
  .DPRA3(ADDRB[3]), .DPRA4(ADDRB[4]), .DPRA5(ADDRB[5]), .DPO(pre_doutb_0[26]));
// Memory array element: COL: 0, ROW: 27
//  Initialization of Dual Port LUT ram now done through explicit
// parameter setting.
RAM64X1D#(.INIT(64'h0000000000000000))RAM64X1D_instance_2651(.WCLK(CLK), .WE(wea_0), 
  .A0(ADDRA[0]), .A1(ADDRA[1]), .A2(ADDRA[2]), .A3(ADDRA[3]), .A4(ADDRA[4]), .A5(ADDRA[5]), 
  .D(DINA[27]), .SPO(pre_douta_0[27]), .DPRA0(ADDRB[0]), .DPRA1(ADDRB[1]), .DPRA2(ADDRB[2]), 
  .DPRA3(ADDRB[3]), .DPRA4(ADDRB[4]), .DPRA5(ADDRB[5]), .DPO(pre_doutb_0[27]));
// Memory array element: COL: 0, ROW: 28
//  Initialization of Dual Port LUT ram now done through explicit
// parameter setting.
RAM64X1D#(.INIT(64'h0000000000000000))RAM64X1D_instance_2652(.WCLK(CLK), .WE(wea_0), 
  .A0(ADDRA[0]), .A1(ADDRA[1]), .A2(ADDRA[2]), .A3(ADDRA[3]), .A4(ADDRA[4]), .A5(ADDRA[5]), 
  .D(DINA[28]), .SPO(pre_douta_0[28]), .DPRA0(ADDRB[0]), .DPRA1(ADDRB[1]), .DPRA2(ADDRB[2]), 
  .DPRA3(ADDRB[3]), .DPRA4(ADDRB[4]), .DPRA5(ADDRB[5]), .DPO(pre_doutb_0[28]));
// Memory array element: COL: 0, ROW: 29
//  Initialization of Dual Port LUT ram now done through explicit
// parameter setting.
RAM64X1D#(.INIT(64'h0000000000000000))RAM64X1D_instance_2653(.WCLK(CLK), .WE(wea_0), 
  .A0(ADDRA[0]), .A1(ADDRA[1]), .A2(ADDRA[2]), .A3(ADDRA[3]), .A4(ADDRA[4]), .A5(ADDRA[5]), 
  .D(DINA[29]), .SPO(pre_douta_0[29]), .DPRA0(ADDRB[0]), .DPRA1(ADDRB[1]), .DPRA2(ADDRB[2]), 
  .DPRA3(ADDRB[3]), .DPRA4(ADDRB[4]), .DPRA5(ADDRB[5]), .DPO(pre_doutb_0[29]));
// Memory array element: COL: 0, ROW: 30
//  Initialization of Dual Port LUT ram now done through explicit
// parameter setting.
RAM64X1D#(.INIT(64'h0000000000000000))RAM64X1D_instance_2654(.WCLK(CLK), .WE(wea_0), 
  .A0(ADDRA[0]), .A1(ADDRA[1]), .A2(ADDRA[2]), .A3(ADDRA[3]), .A4(ADDRA[4]), .A5(ADDRA[5]), 
  .D(DINA[30]), .SPO(pre_douta_0[30]), .DPRA0(ADDRB[0]), .DPRA1(ADDRB[1]), .DPRA2(ADDRB[2]), 
  .DPRA3(ADDRB[3]), .DPRA4(ADDRB[4]), .DPRA5(ADDRB[5]), .DPO(pre_doutb_0[30]));
// Memory array element: COL: 0, ROW: 31
//  Initialization of Dual Port LUT ram now done through explicit
// parameter setting.
RAM64X1D#(.INIT(64'h0000000000000000))RAM64X1D_instance_2655(.WCLK(CLK), .WE(wea_0), 
  .A0(ADDRA[0]), .A1(ADDRA[1]), .A2(ADDRA[2]), .A3(ADDRA[3]), .A4(ADDRA[4]), .A5(ADDRA[5]), 
  .D(DINA[31]), .SPO(pre_douta_0[31]), .DPRA0(ADDRB[0]), .DPRA1(ADDRB[1]), .DPRA2(ADDRB[2]), 
  .DPRA3(ADDRB[3]), .DPRA4(ADDRB[4]), .DPRA5(ADDRB[5]), .DPO(pre_doutb_0[31]));
// Memory array element: COL: 1, ROW: 0
//  Initialization of Dual Port LUT ram now done through explicit
// parameter setting.
RAM64X1D#(.INIT(64'h0000000000000000))RAM64X1D_instance_2656(.WCLK(CLK), .WE(wea_1), 
  .A0(ADDRA[0]), .A1(ADDRA[1]), .A2(ADDRA[2]), .A3(ADDRA[3]), .A4(ADDRA[4]), .A5(ADDRA[5]), 
  .D(DINA[0]), .SPO(pre_douta_1[0]), .DPRA0(ADDRB[0]), .DPRA1(ADDRB[1]), .DPRA2(ADDRB[2]), 
  .DPRA3(ADDRB[3]), .DPRA4(ADDRB[4]), .DPRA5(ADDRB[5]), .DPO(pre_doutb_1[0]));
// Memory array element: COL: 1, ROW: 1
//  Initialization of Dual Port LUT ram now done through explicit
// parameter setting.
RAM64X1D#(.INIT(64'h0000000000000000))RAM64X1D_instance_2657(.WCLK(CLK), .WE(wea_1), 
  .A0(ADDRA[0]), .A1(ADDRA[1]), .A2(ADDRA[2]), .A3(ADDRA[3]), .A4(ADDRA[4]), .A5(ADDRA[5]), 
  .D(DINA[1]), .SPO(pre_douta_1[1]), .DPRA0(ADDRB[0]), .DPRA1(ADDRB[1]), .DPRA2(ADDRB[2]), 
  .DPRA3(ADDRB[3]), .DPRA4(ADDRB[4]), .DPRA5(ADDRB[5]), .DPO(pre_doutb_1[1]));
// Memory array element: COL: 1, ROW: 2
//  Initialization of Dual Port LUT ram now done through explicit
// parameter setting.
RAM64X1D#(.INIT(64'h0000000000000000))RAM64X1D_instance_2658(.WCLK(CLK), .WE(wea_1), 
  .A0(ADDRA[0]), .A1(ADDRA[1]), .A2(ADDRA[2]), .A3(ADDRA[3]), .A4(ADDRA[4]), .A5(ADDRA[5]), 
  .D(DINA[2]), .SPO(pre_douta_1[2]), .DPRA0(ADDRB[0]), .DPRA1(ADDRB[1]), .DPRA2(ADDRB[2]), 
  .DPRA3(ADDRB[3]), .DPRA4(ADDRB[4]), .DPRA5(ADDRB[5]), .DPO(pre_doutb_1[2]));
// Memory array element: COL: 1, ROW: 3
//  Initialization of Dual Port LUT ram now done through explicit
// parameter setting.
RAM64X1D#(.INIT(64'h0000000000000000))RAM64X1D_instance_2659(.WCLK(CLK), .WE(wea_1), 
  .A0(ADDRA[0]), .A1(ADDRA[1]), .A2(ADDRA[2]), .A3(ADDRA[3]), .A4(ADDRA[4]), .A5(ADDRA[5]), 
  .D(DINA[3]), .SPO(pre_douta_1[3]), .DPRA0(ADDRB[0]), .DPRA1(ADDRB[1]), .DPRA2(ADDRB[2]), 
  .DPRA3(ADDRB[3]), .DPRA4(ADDRB[4]), .DPRA5(ADDRB[5]), .DPO(pre_doutb_1[3]));
// Memory array element: COL: 1, ROW: 4
//  Initialization of Dual Port LUT ram now done through explicit
// parameter setting.
RAM64X1D#(.INIT(64'h0000000000000000))RAM64X1D_instance_2660(.WCLK(CLK), .WE(wea_1), 
  .A0(ADDRA[0]), .A1(ADDRA[1]), .A2(ADDRA[2]), .A3(ADDRA[3]), .A4(ADDRA[4]), .A5(ADDRA[5]), 
  .D(DINA[4]), .SPO(pre_douta_1[4]), .DPRA0(ADDRB[0]), .DPRA1(ADDRB[1]), .DPRA2(ADDRB[2]), 
  .DPRA3(ADDRB[3]), .DPRA4(ADDRB[4]), .DPRA5(ADDRB[5]), .DPO(pre_doutb_1[4]));
// Memory array element: COL: 1, ROW: 5
//  Initialization of Dual Port LUT ram now done through explicit
// parameter setting.
RAM64X1D#(.INIT(64'h0000000000000000))RAM64X1D_instance_2661(.WCLK(CLK), .WE(wea_1), 
  .A0(ADDRA[0]), .A1(ADDRA[1]), .A2(ADDRA[2]), .A3(ADDRA[3]), .A4(ADDRA[4]), .A5(ADDRA[5]), 
  .D(DINA[5]), .SPO(pre_douta_1[5]), .DPRA0(ADDRB[0]), .DPRA1(ADDRB[1]), .DPRA2(ADDRB[2]), 
  .DPRA3(ADDRB[3]), .DPRA4(ADDRB[4]), .DPRA5(ADDRB[5]), .DPO(pre_doutb_1[5]));
// Memory array element: COL: 1, ROW: 6
//  Initialization of Dual Port LUT ram now done through explicit
// parameter setting.
RAM64X1D#(.INIT(64'h0000000000000000))RAM64X1D_instance_2662(.WCLK(CLK), .WE(wea_1), 
  .A0(ADDRA[0]), .A1(ADDRA[1]), .A2(ADDRA[2]), .A3(ADDRA[3]), .A4(ADDRA[4]), .A5(ADDRA[5]), 
  .D(DINA[6]), .SPO(pre_douta_1[6]), .DPRA0(ADDRB[0]), .DPRA1(ADDRB[1]), .DPRA2(ADDRB[2]), 
  .DPRA3(ADDRB[3]), .DPRA4(ADDRB[4]), .DPRA5(ADDRB[5]), .DPO(pre_doutb_1[6]));
// Memory array element: COL: 1, ROW: 7
//  Initialization of Dual Port LUT ram now done through explicit
// parameter setting.
RAM64X1D#(.INIT(64'h0000000000000000))RAM64X1D_instance_2663(.WCLK(CLK), .WE(wea_1), 
  .A0(ADDRA[0]), .A1(ADDRA[1]), .A2(ADDRA[2]), .A3(ADDRA[3]), .A4(ADDRA[4]), .A5(ADDRA[5]), 
  .D(DINA[7]), .SPO(pre_douta_1[7]), .DPRA0(ADDRB[0]), .DPRA1(ADDRB[1]), .DPRA2(ADDRB[2]), 
  .DPRA3(ADDRB[3]), .DPRA4(ADDRB[4]), .DPRA5(ADDRB[5]), .DPO(pre_doutb_1[7]));
// Memory array element: COL: 1, ROW: 8
//  Initialization of Dual Port LUT ram now done through explicit
// parameter setting.
RAM64X1D#(.INIT(64'h0000000000000000))RAM64X1D_instance_2664(.WCLK(CLK), .WE(wea_1), 
  .A0(ADDRA[0]), .A1(ADDRA[1]), .A2(ADDRA[2]), .A3(ADDRA[3]), .A4(ADDRA[4]), .A5(ADDRA[5]), 
  .D(DINA[8]), .SPO(pre_douta_1[8]), .DPRA0(ADDRB[0]), .DPRA1(ADDRB[1]), .DPRA2(ADDRB[2]), 
  .DPRA3(ADDRB[3]), .DPRA4(ADDRB[4]), .DPRA5(ADDRB[5]), .DPO(pre_doutb_1[8]));
// Memory array element: COL: 1, ROW: 9
//  Initialization of Dual Port LUT ram now done through explicit
// parameter setting.
RAM64X1D#(.INIT(64'h0000000000000000))RAM64X1D_instance_2665(.WCLK(CLK), .WE(wea_1), 
  .A0(ADDRA[0]), .A1(ADDRA[1]), .A2(ADDRA[2]), .A3(ADDRA[3]), .A4(ADDRA[4]), .A5(ADDRA[5]), 
  .D(DINA[9]), .SPO(pre_douta_1[9]), .DPRA0(ADDRB[0]), .DPRA1(ADDRB[1]), .DPRA2(ADDRB[2]), 
  .DPRA3(ADDRB[3]), .DPRA4(ADDRB[4]), .DPRA5(ADDRB[5]), .DPO(pre_doutb_1[9]));
// Memory array element: COL: 1, ROW: 10
//  Initialization of Dual Port LUT ram now done through explicit
// parameter setting.
RAM64X1D#(.INIT(64'h0000000000000000))RAM64X1D_instance_2666(.WCLK(CLK), .WE(wea_1), 
  .A0(ADDRA[0]), .A1(ADDRA[1]), .A2(ADDRA[2]), .A3(ADDRA[3]), .A4(ADDRA[4]), .A5(ADDRA[5]), 
  .D(DINA[10]), .SPO(pre_douta_1[10]), .DPRA0(ADDRB[0]), .DPRA1(ADDRB[1]), .DPRA2(ADDRB[2]), 
  .DPRA3(ADDRB[3]), .DPRA4(ADDRB[4]), .DPRA5(ADDRB[5]), .DPO(pre_doutb_1[10]));
// Memory array element: COL: 1, ROW: 11
//  Initialization of Dual Port LUT ram now done through explicit
// parameter setting.
RAM64X1D#(.INIT(64'h0000000000000000))RAM64X1D_instance_2667(.WCLK(CLK), .WE(wea_1), 
  .A0(ADDRA[0]), .A1(ADDRA[1]), .A2(ADDRA[2]), .A3(ADDRA[3]), .A4(ADDRA[4]), .A5(ADDRA[5]), 
  .D(DINA[11]), .SPO(pre_douta_1[11]), .DPRA0(ADDRB[0]), .DPRA1(ADDRB[1]), .DPRA2(ADDRB[2]), 
  .DPRA3(ADDRB[3]), .DPRA4(ADDRB[4]), .DPRA5(ADDRB[5]), .DPO(pre_doutb_1[11]));
// Memory array element: COL: 1, ROW: 12
//  Initialization of Dual Port LUT ram now done through explicit
// parameter setting.
RAM64X1D#(.INIT(64'h0000000000000000))RAM64X1D_instance_2668(.WCLK(CLK), .WE(wea_1), 
  .A0(ADDRA[0]), .A1(ADDRA[1]), .A2(ADDRA[2]), .A3(ADDRA[3]), .A4(ADDRA[4]), .A5(ADDRA[5]), 
  .D(DINA[12]), .SPO(pre_douta_1[12]), .DPRA0(ADDRB[0]), .DPRA1(ADDRB[1]), .DPRA2(ADDRB[2]), 
  .DPRA3(ADDRB[3]), .DPRA4(ADDRB[4]), .DPRA5(ADDRB[5]), .DPO(pre_doutb_1[12]));
// Memory array element: COL: 1, ROW: 13
//  Initialization of Dual Port LUT ram now done through explicit
// parameter setting.
RAM64X1D#(.INIT(64'h0000000000000000))RAM64X1D_instance_2669(.WCLK(CLK), .WE(wea_1), 
  .A0(ADDRA[0]), .A1(ADDRA[1]), .A2(ADDRA[2]), .A3(ADDRA[3]), .A4(ADDRA[4]), .A5(ADDRA[5]), 
  .D(DINA[13]), .SPO(pre_douta_1[13]), .DPRA0(ADDRB[0]), .DPRA1(ADDRB[1]), .DPRA2(ADDRB[2]), 
  .DPRA3(ADDRB[3]), .DPRA4(ADDRB[4]), .DPRA5(ADDRB[5]), .DPO(pre_doutb_1[13]));
// Memory array element: COL: 1, ROW: 14
//  Initialization of Dual Port LUT ram now done through explicit
// parameter setting.
RAM64X1D#(.INIT(64'h0000000000000000))RAM64X1D_instance_2670(.WCLK(CLK), .WE(wea_1), 
  .A0(ADDRA[0]), .A1(ADDRA[1]), .A2(ADDRA[2]), .A3(ADDRA[3]), .A4(ADDRA[4]), .A5(ADDRA[5]), 
  .D(DINA[14]), .SPO(pre_douta_1[14]), .DPRA0(ADDRB[0]), .DPRA1(ADDRB[1]), .DPRA2(ADDRB[2]), 
  .DPRA3(ADDRB[3]), .DPRA4(ADDRB[4]), .DPRA5(ADDRB[5]), .DPO(pre_doutb_1[14]));
// Memory array element: COL: 1, ROW: 15
//  Initialization of Dual Port LUT ram now done through explicit
// parameter setting.
RAM64X1D#(.INIT(64'h0000000000000000))RAM64X1D_instance_2671(.WCLK(CLK), .WE(wea_1), 
  .A0(ADDRA[0]), .A1(ADDRA[1]), .A2(ADDRA[2]), .A3(ADDRA[3]), .A4(ADDRA[4]), .A5(ADDRA[5]), 
  .D(DINA[15]), .SPO(pre_douta_1[15]), .DPRA0(ADDRB[0]), .DPRA1(ADDRB[1]), .DPRA2(ADDRB[2]), 
  .DPRA3(ADDRB[3]), .DPRA4(ADDRB[4]), .DPRA5(ADDRB[5]), .DPO(pre_doutb_1[15]));
// Memory array element: COL: 1, ROW: 16
//  Initialization of Dual Port LUT ram now done through explicit
// parameter setting.
RAM64X1D#(.INIT(64'h0000000000000000))RAM64X1D_instance_2672(.WCLK(CLK), .WE(wea_1), 
  .A0(ADDRA[0]), .A1(ADDRA[1]), .A2(ADDRA[2]), .A3(ADDRA[3]), .A4(ADDRA[4]), .A5(ADDRA[5]), 
  .D(DINA[16]), .SPO(pre_douta_1[16]), .DPRA0(ADDRB[0]), .DPRA1(ADDRB[1]), .DPRA2(ADDRB[2]), 
  .DPRA3(ADDRB[3]), .DPRA4(ADDRB[4]), .DPRA5(ADDRB[5]), .DPO(pre_doutb_1[16]));
// Memory array element: COL: 1, ROW: 17
//  Initialization of Dual Port LUT ram now done through explicit
// parameter setting.
RAM64X1D#(.INIT(64'h0000000000000000))RAM64X1D_instance_2673(.WCLK(CLK), .WE(wea_1), 
  .A0(ADDRA[0]), .A1(ADDRA[1]), .A2(ADDRA[2]), .A3(ADDRA[3]), .A4(ADDRA[4]), .A5(ADDRA[5]), 
  .D(DINA[17]), .SPO(pre_douta_1[17]), .DPRA0(ADDRB[0]), .DPRA1(ADDRB[1]), .DPRA2(ADDRB[2]), 
  .DPRA3(ADDRB[3]), .DPRA4(ADDRB[4]), .DPRA5(ADDRB[5]), .DPO(pre_doutb_1[17]));
// Memory array element: COL: 1, ROW: 18
//  Initialization of Dual Port LUT ram now done through explicit
// parameter setting.
RAM64X1D#(.INIT(64'h0000000000000000))RAM64X1D_instance_2674(.WCLK(CLK), .WE(wea_1), 
  .A0(ADDRA[0]), .A1(ADDRA[1]), .A2(ADDRA[2]), .A3(ADDRA[3]), .A4(ADDRA[4]), .A5(ADDRA[5]), 
  .D(DINA[18]), .SPO(pre_douta_1[18]), .DPRA0(ADDRB[0]), .DPRA1(ADDRB[1]), .DPRA2(ADDRB[2]), 
  .DPRA3(ADDRB[3]), .DPRA4(ADDRB[4]), .DPRA5(ADDRB[5]), .DPO(pre_doutb_1[18]));
// Memory array element: COL: 1, ROW: 19
//  Initialization of Dual Port LUT ram now done through explicit
// parameter setting.
RAM64X1D#(.INIT(64'h0000000000000000))RAM64X1D_instance_2675(.WCLK(CLK), .WE(wea_1), 
  .A0(ADDRA[0]), .A1(ADDRA[1]), .A2(ADDRA[2]), .A3(ADDRA[3]), .A4(ADDRA[4]), .A5(ADDRA[5]), 
  .D(DINA[19]), .SPO(pre_douta_1[19]), .DPRA0(ADDRB[0]), .DPRA1(ADDRB[1]), .DPRA2(ADDRB[2]), 
  .DPRA3(ADDRB[3]), .DPRA4(ADDRB[4]), .DPRA5(ADDRB[5]), .DPO(pre_doutb_1[19]));
// Memory array element: COL: 1, ROW: 20
//  Initialization of Dual Port LUT ram now done through explicit
// parameter setting.
RAM64X1D#(.INIT(64'h0000000000000000))RAM64X1D_instance_2676(.WCLK(CLK), .WE(wea_1), 
  .A0(ADDRA[0]), .A1(ADDRA[1]), .A2(ADDRA[2]), .A3(ADDRA[3]), .A4(ADDRA[4]), .A5(ADDRA[5]), 
  .D(DINA[20]), .SPO(pre_douta_1[20]), .DPRA0(ADDRB[0]), .DPRA1(ADDRB[1]), .DPRA2(ADDRB[2]), 
  .DPRA3(ADDRB[3]), .DPRA4(ADDRB[4]), .DPRA5(ADDRB[5]), .DPO(pre_doutb_1[20]));
// Memory array element: COL: 1, ROW: 21
//  Initialization of Dual Port LUT ram now done through explicit
// parameter setting.
RAM64X1D#(.INIT(64'h0000000000000000))RAM64X1D_instance_2677(.WCLK(CLK), .WE(wea_1), 
  .A0(ADDRA[0]), .A1(ADDRA[1]), .A2(ADDRA[2]), .A3(ADDRA[3]), .A4(ADDRA[4]), .A5(ADDRA[5]), 
  .D(DINA[21]), .SPO(pre_douta_1[21]), .DPRA0(ADDRB[0]), .DPRA1(ADDRB[1]), .DPRA2(ADDRB[2]), 
  .DPRA3(ADDRB[3]), .DPRA4(ADDRB[4]), .DPRA5(ADDRB[5]), .DPO(pre_doutb_1[21]));
// Memory array element: COL: 1, ROW: 22
//  Initialization of Dual Port LUT ram now done through explicit
// parameter setting.
RAM64X1D#(.INIT(64'h0000000000000000))RAM64X1D_instance_2678(.WCLK(CLK), .WE(wea_1), 
  .A0(ADDRA[0]), .A1(ADDRA[1]), .A2(ADDRA[2]), .A3(ADDRA[3]), .A4(ADDRA[4]), .A5(ADDRA[5]), 
  .D(DINA[22]), .SPO(pre_douta_1[22]), .DPRA0(ADDRB[0]), .DPRA1(ADDRB[1]), .DPRA2(ADDRB[2]), 
  .DPRA3(ADDRB[3]), .DPRA4(ADDRB[4]), .DPRA5(ADDRB[5]), .DPO(pre_doutb_1[22]));
// Memory array element: COL: 1, ROW: 23
//  Initialization of Dual Port LUT ram now done through explicit
// parameter setting.
RAM64X1D#(.INIT(64'h0000000000000000))RAM64X1D_instance_2679(.WCLK(CLK), .WE(wea_1), 
  .A0(ADDRA[0]), .A1(ADDRA[1]), .A2(ADDRA[2]), .A3(ADDRA[3]), .A4(ADDRA[4]), .A5(ADDRA[5]), 
  .D(DINA[23]), .SPO(pre_douta_1[23]), .DPRA0(ADDRB[0]), .DPRA1(ADDRB[1]), .DPRA2(ADDRB[2]), 
  .DPRA3(ADDRB[3]), .DPRA4(ADDRB[4]), .DPRA5(ADDRB[5]), .DPO(pre_doutb_1[23]));
// Memory array element: COL: 1, ROW: 24
//  Initialization of Dual Port LUT ram now done through explicit
// parameter setting.
RAM64X1D#(.INIT(64'h0000000000000000))RAM64X1D_instance_2680(.WCLK(CLK), .WE(wea_1), 
  .A0(ADDRA[0]), .A1(ADDRA[1]), .A2(ADDRA[2]), .A3(ADDRA[3]), .A4(ADDRA[4]), .A5(ADDRA[5]), 
  .D(DINA[24]), .SPO(pre_douta_1[24]), .DPRA0(ADDRB[0]), .DPRA1(ADDRB[1]), .DPRA2(ADDRB[2]), 
  .DPRA3(ADDRB[3]), .DPRA4(ADDRB[4]), .DPRA5(ADDRB[5]), .DPO(pre_doutb_1[24]));
// Memory array element: COL: 1, ROW: 25
//  Initialization of Dual Port LUT ram now done through explicit
// parameter setting.
RAM64X1D#(.INIT(64'h0000000000000000))RAM64X1D_instance_2681(.WCLK(CLK), .WE(wea_1), 
  .A0(ADDRA[0]), .A1(ADDRA[1]), .A2(ADDRA[2]), .A3(ADDRA[3]), .A4(ADDRA[4]), .A5(ADDRA[5]), 
  .D(DINA[25]), .SPO(pre_douta_1[25]), .DPRA0(ADDRB[0]), .DPRA1(ADDRB[1]), .DPRA2(ADDRB[2]), 
  .DPRA3(ADDRB[3]), .DPRA4(ADDRB[4]), .DPRA5(ADDRB[5]), .DPO(pre_doutb_1[25]));
// Memory array element: COL: 1, ROW: 26
//  Initialization of Dual Port LUT ram now done through explicit
// parameter setting.
RAM64X1D#(.INIT(64'h0000000000000000))RAM64X1D_instance_2682(.WCLK(CLK), .WE(wea_1), 
  .A0(ADDRA[0]), .A1(ADDRA[1]), .A2(ADDRA[2]), .A3(ADDRA[3]), .A4(ADDRA[4]), .A5(ADDRA[5]), 
  .D(DINA[26]), .SPO(pre_douta_1[26]), .DPRA0(ADDRB[0]), .DPRA1(ADDRB[1]), .DPRA2(ADDRB[2]), 
  .DPRA3(ADDRB[3]), .DPRA4(ADDRB[4]), .DPRA5(ADDRB[5]), .DPO(pre_doutb_1[26]));
// Memory array element: COL: 1, ROW: 27
//  Initialization of Dual Port LUT ram now done through explicit
// parameter setting.
RAM64X1D#(.INIT(64'h0000000000000000))RAM64X1D_instance_2683(.WCLK(CLK), .WE(wea_1), 
  .A0(ADDRA[0]), .A1(ADDRA[1]), .A2(ADDRA[2]), .A3(ADDRA[3]), .A4(ADDRA[4]), .A5(ADDRA[5]), 
  .D(DINA[27]), .SPO(pre_douta_1[27]), .DPRA0(ADDRB[0]), .DPRA1(ADDRB[1]), .DPRA2(ADDRB[2]), 
  .DPRA3(ADDRB[3]), .DPRA4(ADDRB[4]), .DPRA5(ADDRB[5]), .DPO(pre_doutb_1[27]));
// Memory array element: COL: 1, ROW: 28
//  Initialization of Dual Port LUT ram now done through explicit
// parameter setting.
RAM64X1D#(.INIT(64'h0000000000000000))RAM64X1D_instance_2684(.WCLK(CLK), .WE(wea_1), 
  .A0(ADDRA[0]), .A1(ADDRA[1]), .A2(ADDRA[2]), .A3(ADDRA[3]), .A4(ADDRA[4]), .A5(ADDRA[5]), 
  .D(DINA[28]), .SPO(pre_douta_1[28]), .DPRA0(ADDRB[0]), .DPRA1(ADDRB[1]), .DPRA2(ADDRB[2]), 
  .DPRA3(ADDRB[3]), .DPRA4(ADDRB[4]), .DPRA5(ADDRB[5]), .DPO(pre_doutb_1[28]));
// Memory array element: COL: 1, ROW: 29
//  Initialization of Dual Port LUT ram now done through explicit
// parameter setting.
RAM64X1D#(.INIT(64'h0000000000000000))RAM64X1D_instance_2685(.WCLK(CLK), .WE(wea_1), 
  .A0(ADDRA[0]), .A1(ADDRA[1]), .A2(ADDRA[2]), .A3(ADDRA[3]), .A4(ADDRA[4]), .A5(ADDRA[5]), 
  .D(DINA[29]), .SPO(pre_douta_1[29]), .DPRA0(ADDRB[0]), .DPRA1(ADDRB[1]), .DPRA2(ADDRB[2]), 
  .DPRA3(ADDRB[3]), .DPRA4(ADDRB[4]), .DPRA5(ADDRB[5]), .DPO(pre_doutb_1[29]));
// Memory array element: COL: 1, ROW: 30
//  Initialization of Dual Port LUT ram now done through explicit
// parameter setting.
RAM64X1D#(.INIT(64'h0000000000000000))RAM64X1D_instance_2686(.WCLK(CLK), .WE(wea_1), 
  .A0(ADDRA[0]), .A1(ADDRA[1]), .A2(ADDRA[2]), .A3(ADDRA[3]), .A4(ADDRA[4]), .A5(ADDRA[5]), 
  .D(DINA[30]), .SPO(pre_douta_1[30]), .DPRA0(ADDRB[0]), .DPRA1(ADDRB[1]), .DPRA2(ADDRB[2]), 
  .DPRA3(ADDRB[3]), .DPRA4(ADDRB[4]), .DPRA5(ADDRB[5]), .DPO(pre_doutb_1[30]));
// Memory array element: COL: 1, ROW: 31
//  Initialization of Dual Port LUT ram now done through explicit
// parameter setting.
RAM64X1D#(.INIT(64'h0000000000000000))RAM64X1D_instance_2687(.WCLK(CLK), .WE(wea_1), 
  .A0(ADDRA[0]), .A1(ADDRA[1]), .A2(ADDRA[2]), .A3(ADDRA[3]), .A4(ADDRA[4]), .A5(ADDRA[5]), 
  .D(DINA[31]), .SPO(pre_douta_1[31]), .DPRA0(ADDRB[0]), .DPRA1(ADDRB[1]), .DPRA2(ADDRB[2]), 
  .DPRA3(ADDRB[3]), .DPRA4(ADDRB[4]), .DPRA5(ADDRB[5]), .DPO(pre_doutb_1[31]));
endmodule



module medianRow1_structuralmemory_315f90d8_(CLK_u87, bus_084457e9_, bus_6a58810d_, bus_01e50d1e_, bus_71c6914d_, bus_1f4c50d7_, bus_5739bb87_, bus_117346ef_, bus_7ea553da_, bus_1fab3e39_, bus_2fdc550d_, bus_184441d7_, bus_20d4651c_, bus_3bd8fd6d_);
input		CLK_u87;
input		bus_084457e9_;
input	[31:0]	bus_6a58810d_;
input	[2:0]	bus_01e50d1e_;
input		bus_71c6914d_;
input	[31:0]	bus_1f4c50d7_;
input	[2:0]	bus_5739bb87_;
input		bus_117346ef_;
input		bus_7ea553da_;
input	[31:0]	bus_1fab3e39_;
output	[31:0]	bus_2fdc550d_;
output		bus_184441d7_;
output	[31:0]	bus_20d4651c_;
output		bus_3bd8fd6d_;
wire		not_723b61ff_u0;
reg		logicalMem_122c901e_we_delay0_u0=1'h0;
wire		or_65a45848_u0;
wire		and_35aa46fa_u0;
wire	[31:0]	bus_0e242461_;
wire	[31:0]	bus_4a1904ef_;
wire		or_545f2f2c_u0;
assign not_723b61ff_u0=~bus_7ea553da_;
assign bus_2fdc550d_=bus_4a1904ef_;
assign bus_184441d7_=bus_71c6914d_;
assign bus_20d4651c_=bus_0e242461_;
assign bus_3bd8fd6d_=or_65a45848_u0;
always @(posedge CLK_u87 or posedge bus_084457e9_)
begin
if (bus_084457e9_)
logicalMem_122c901e_we_delay0_u0<=1'h0;
else logicalMem_122c901e_we_delay0_u0<=bus_7ea553da_;
end
assign or_65a45848_u0=and_35aa46fa_u0|logicalMem_122c901e_we_delay0_u0;
assign and_35aa46fa_u0=bus_117346ef_&not_723b61ff_u0;
medianRow1_forge_memory_101x32_135 medianRow1_forge_memory_101x32_135_instance0(.CLK(CLK_u87), 
  .ENA(or_545f2f2c_u0), .WEA(bus_7ea553da_), .DINA(bus_1fab3e39_), .ADDRA(bus_1f4c50d7_), 
  .DOUTA(bus_0e242461_), .DONEA(), .ENB(bus_71c6914d_), .ADDRB(bus_6a58810d_), .DOUTB(bus_4a1904ef_), 
  .DONEB());
assign or_545f2f2c_u0=bus_117346ef_|bus_7ea553da_;
endmodule



module medianRow1_endianswapper_7ff52300_(endianswapper_7ff52300_in, endianswapper_7ff52300_out);
input	[31:0]	endianswapper_7ff52300_in;
output	[31:0]	endianswapper_7ff52300_out;
assign endianswapper_7ff52300_out=endianswapper_7ff52300_in;
endmodule



module medianRow1_endianswapper_4e1255f7_(endianswapper_4e1255f7_in, endianswapper_4e1255f7_out);
input	[31:0]	endianswapper_4e1255f7_in;
output	[31:0]	endianswapper_4e1255f7_out;
assign endianswapper_4e1255f7_out=endianswapper_4e1255f7_in;
endmodule



module medianRow1_stateVar_i(bus_61e233d8_, bus_6b4c4e16_, bus_0e0623eb_, bus_773f6029_, bus_185bb10f_, bus_3f20edda_, bus_600ada8a_);
input		bus_61e233d8_;
input		bus_6b4c4e16_;
input		bus_0e0623eb_;
input	[31:0]	bus_773f6029_;
input		bus_185bb10f_;
input	[31:0]	bus_3f20edda_;
output	[31:0]	bus_600ada8a_;
wire	[31:0]	endianswapper_7ff52300_out;
reg	[31:0]	stateVar_i_u42=32'h0;
wire	[31:0]	endianswapper_4e1255f7_out;
wire	[31:0]	mux_7e92b4af_u0;
wire		or_0fe057dd_u0;
medianRow1_endianswapper_7ff52300_ medianRow1_endianswapper_7ff52300__1(.endianswapper_7ff52300_in(mux_7e92b4af_u0), 
  .endianswapper_7ff52300_out(endianswapper_7ff52300_out));
always @(posedge bus_61e233d8_ or posedge bus_6b4c4e16_)
begin
if (bus_6b4c4e16_)
stateVar_i_u42<=32'h0;
else if (or_0fe057dd_u0)
stateVar_i_u42<=endianswapper_7ff52300_out;
end
medianRow1_endianswapper_4e1255f7_ medianRow1_endianswapper_4e1255f7__1(.endianswapper_4e1255f7_in(stateVar_i_u42), 
  .endianswapper_4e1255f7_out(endianswapper_4e1255f7_out));
assign bus_600ada8a_=endianswapper_4e1255f7_out;
assign mux_7e92b4af_u0=(bus_0e0623eb_)?bus_773f6029_:32'h0;
assign or_0fe057dd_u0=bus_0e0623eb_|bus_185bb10f_;
endmodule



module medianRow1_endianswapper_4f3a2ee6_(endianswapper_4f3a2ee6_in, endianswapper_4f3a2ee6_out);
input		endianswapper_4f3a2ee6_in;
output		endianswapper_4f3a2ee6_out;
assign endianswapper_4f3a2ee6_out=endianswapper_4f3a2ee6_in;
endmodule



module medianRow1_endianswapper_0c81530c_(endianswapper_0c81530c_in, endianswapper_0c81530c_out);
input		endianswapper_0c81530c_in;
output		endianswapper_0c81530c_out;
assign endianswapper_0c81530c_out=endianswapper_0c81530c_in;
endmodule



module medianRow1_stateVar_fsmState_medianRow1(bus_4cf74991_, bus_6820df2f_, bus_08566f50_, bus_10ac6551_, bus_2d338235_);
input		bus_4cf74991_;
input		bus_6820df2f_;
input		bus_08566f50_;
input		bus_10ac6551_;
output		bus_2d338235_;
reg		stateVar_fsmState_medianRow1_u4=1'h0;
wire		endianswapper_4f3a2ee6_out;
wire		endianswapper_0c81530c_out;
always @(posedge bus_4cf74991_ or posedge bus_6820df2f_)
begin
if (bus_6820df2f_)
stateVar_fsmState_medianRow1_u4<=1'h0;
else if (bus_08566f50_)
stateVar_fsmState_medianRow1_u4<=endianswapper_0c81530c_out;
end
medianRow1_endianswapper_4f3a2ee6_ medianRow1_endianswapper_4f3a2ee6__1(.endianswapper_4f3a2ee6_in(stateVar_fsmState_medianRow1_u4), 
  .endianswapper_4f3a2ee6_out(endianswapper_4f3a2ee6_out));
medianRow1_endianswapper_0c81530c_ medianRow1_endianswapper_0c81530c__1(.endianswapper_0c81530c_in(bus_10ac6551_), 
  .endianswapper_0c81530c_out(endianswapper_0c81530c_out));
assign bus_2d338235_=endianswapper_4f3a2ee6_out;
endmodule



module medianRow1_Kicker_51(CLK, RESET, bus_7dd773e3_);
input		CLK;
input		RESET;
output		bus_7dd773e3_;
reg		kicker_2=1'h0;
reg		kicker_res=1'h0;
wire		bus_20f76ea4_;
wire		bus_0bb6f679_;
wire		bus_554d4f8b_;
wire		bus_002b0bba_;
reg		kicker_1=1'h0;
always @(posedge CLK)
begin
kicker_2<=bus_554d4f8b_;
end
always @(posedge CLK)
begin
kicker_res<=bus_002b0bba_;
end
assign bus_7dd773e3_=kicker_res;
assign bus_20f76ea4_=~RESET;
assign bus_0bb6f679_=~kicker_2;
assign bus_554d4f8b_=bus_20f76ea4_&kicker_1;
assign bus_002b0bba_=kicker_1&bus_20f76ea4_&bus_0bb6f679_;
always @(posedge CLK)
begin
kicker_1<=bus_20f76ea4_;
end
endmodule



module medianRow1_simplememoryreferee_65aacafe_(bus_744d7d6f_, bus_20e9aebb_, bus_661c8d09_, bus_21e59098_, bus_57161f29_, bus_46185a01_, bus_41c86ead_, bus_06f4c3e3_, bus_44bf117c_, bus_69180131_, bus_42122957_, bus_3b6819e4_, bus_2cca9b16_, bus_67ccda47_);
input		bus_744d7d6f_;
input		bus_20e9aebb_;
input		bus_661c8d09_;
input	[31:0]	bus_21e59098_;
input		bus_57161f29_;
input	[31:0]	bus_46185a01_;
input	[2:0]	bus_41c86ead_;
output	[31:0]	bus_06f4c3e3_;
output	[31:0]	bus_44bf117c_;
output		bus_69180131_;
output		bus_42122957_;
output	[2:0]	bus_3b6819e4_;
output	[31:0]	bus_2cca9b16_;
output		bus_67ccda47_;
assign bus_06f4c3e3_=32'h0;
assign bus_44bf117c_=bus_46185a01_;
assign bus_69180131_=1'h0;
assign bus_42122957_=bus_57161f29_;
assign bus_3b6819e4_=3'h1;
assign bus_2cca9b16_=bus_21e59098_;
assign bus_67ccda47_=bus_661c8d09_;
endmodule



module medianRow1_globalreset_physical_1ec5a0d8_(bus_62913fb0_, bus_78aca935_, bus_1701b0e0_);
input		bus_62913fb0_;
input		bus_78aca935_;
output		bus_1701b0e0_;
wire		and_0e0d5aa5_u0;
wire		not_688df405_u0;
wire		or_2687c12f_u0;
reg		sample_u51=1'h0;
reg		glitch_u51=1'h0;
reg		final_u51=1'h1;
reg		cross_u51=1'h0;
assign bus_1701b0e0_=or_2687c12f_u0;
assign and_0e0d5aa5_u0=cross_u51&glitch_u51;
assign not_688df405_u0=~and_0e0d5aa5_u0;
assign or_2687c12f_u0=bus_78aca935_|final_u51;
always @(posedge bus_62913fb0_)
begin
sample_u51<=1'h1;
end
always @(posedge bus_62913fb0_)
begin
glitch_u51<=cross_u51;
end
always @(posedge bus_62913fb0_)
begin
final_u51<=not_688df405_u0;
end
always @(posedge bus_62913fb0_)
begin
cross_u51<=sample_u51;
end
endmodule



module medianRow1_receive(CLK, RESET, GO, port_0f2325b8_, port_64a495de_, port_746d25a1_, RESULT, RESULT_u1943, RESULT_u1944, RESULT_u1945, RESULT_u1946, RESULT_u1947, RESULT_u1948, DONE);
input		CLK;
input		RESET;
input		GO;
input	[31:0]	port_0f2325b8_;
input		port_64a495de_;
input	[7:0]	port_746d25a1_;
output		RESULT;
output	[31:0]	RESULT_u1943;
output		RESULT_u1944;
output	[31:0]	RESULT_u1945;
output	[31:0]	RESULT_u1946;
output	[2:0]	RESULT_u1947;
output		RESULT_u1948;
output		DONE;
wire		simplePinWrite;
wire	[31:0]	add;
reg		reg_26c76977_u0=1'h0;
wire		or_u1332_u0;
wire		and_u3218_u0;
wire	[31:0]	add_u617;
reg		reg_19563008_u0=1'h0;
assign simplePinWrite=GO&{1{GO}};
assign add=port_0f2325b8_+32'h0;
always @(posedge CLK or posedge GO or posedge or_u1332_u0)
begin
if (or_u1332_u0)
reg_26c76977_u0<=1'h0;
else if (GO)
reg_26c76977_u0<=1'h1;
else reg_26c76977_u0<=reg_26c76977_u0;
end
assign or_u1332_u0=and_u3218_u0|RESET;
assign and_u3218_u0=reg_26c76977_u0&port_64a495de_;
assign add_u617=port_0f2325b8_+32'h1;
always @(posedge CLK or posedge RESET)
begin
if (RESET)
reg_19563008_u0<=1'h0;
else reg_19563008_u0<=GO;
end
assign RESULT=GO;
assign RESULT_u1943=add_u617;
assign RESULT_u1944=GO;
assign RESULT_u1945=add;
assign RESULT_u1946={24'b0, port_746d25a1_};
assign RESULT_u1947=3'h1;
assign RESULT_u1948=simplePinWrite;
assign DONE=reg_19563008_u0;
endmodule



module medianRow1_compute_median(CLK, RESET, GO, port_6829be08_, port_152892fb_, port_27b7f1f6_, port_34368cc5_, port_43a1623f_, RESULT, RESULT_u1949, RESULT_u1950, RESULT_u1951, RESULT_u1952, RESULT_u1953, RESULT_u1954, RESULT_u1955, RESULT_u1956, RESULT_u1957, RESULT_u1958, RESULT_u1959, RESULT_u1960, RESULT_u1961, RESULT_u1962, DONE);
input		CLK;
input		RESET;
input		GO;
input		port_6829be08_;
input	[31:0]	port_152892fb_;
input		port_27b7f1f6_;
input	[31:0]	port_34368cc5_;
input		port_43a1623f_;
output		RESULT;
output	[31:0]	RESULT_u1949;
output		RESULT_u1950;
output	[31:0]	RESULT_u1951;
output	[2:0]	RESULT_u1952;
output		RESULT_u1953;
output	[31:0]	RESULT_u1954;
output	[2:0]	RESULT_u1955;
output		RESULT_u1956;
output	[31:0]	RESULT_u1957;
output	[31:0]	RESULT_u1958;
output	[2:0]	RESULT_u1959;
output	[15:0]	RESULT_u1960;
output	[7:0]	RESULT_u1961;
output		RESULT_u1962;
output		DONE;
wire	[15:0]	lessThan_a_unsigned;
wire	[15:0]	lessThan_b_unsigned;
wire		lessThan;
wire		andOp;
wire		and_u3219_u0;
wire		and_u3220_u0;
wire		not_u725_u0;
wire		and_u3221_u0;
reg	[31:0]	syncEnable_u1322=32'h0;
wire signed	[32:0]	lessThan_u84_a_signed;
wire signed	[32:0]	lessThan_u84_b_signed;
wire		lessThan_u84;
wire		and_u3222_u0;
wire		and_u3223_u0;
wire		not_u726_u0;
wire	[31:0]	add;
wire		and_u3224_u0;
wire	[31:0]	add_u618;
wire	[31:0]	add_u619;
wire		and_u3225_u0;
wire		greaterThan;
wire signed	[31:0]	greaterThan_a_signed;
wire signed	[31:0]	greaterThan_b_signed;
wire		not_u727_u0;
wire		and_u3226_u0;
wire		and_u3227_u0;
wire	[31:0]	add_u620;
wire		and_u3228_u0;
wire	[31:0]	add_u621;
wire	[31:0]	add_u622;
wire		and_u3229_u0;
wire	[31:0]	add_u623;
wire		and_u3230_u0;
wire		or_u1333_u0;
reg		reg_2ea1700e_u0=1'h0;
wire	[31:0]	add_u624;
wire	[31:0]	add_u625;
reg		reg_3d9faea6_u0=1'h0;
wire		or_u1334_u0;
wire		and_u3231_u0;
reg	[31:0]	syncEnable_u1323_u0=32'h0;
reg	[31:0]	syncEnable_u1324_u0=32'h0;
wire		or_u1335_u0;
wire	[31:0]	mux_u1777;
wire	[31:0]	mux_u1778_u0;
reg		reg_7713a0f6_u0=1'h0;
reg	[31:0]	syncEnable_u1325_u0=32'h0;
reg	[31:0]	syncEnable_u1326_u0=32'h0;
reg	[31:0]	syncEnable_u1327_u0=32'h0;
reg		block_GO_delayed_u88=1'h0;
reg	[31:0]	syncEnable_u1328_u0=32'h0;
reg		reg_0043c563_u0=1'h0;
reg		reg_0043c563_result_delayed_u0=1'h0;
reg		reg_4bbcffdd_u0=1'h0;
reg	[31:0]	syncEnable_u1329_u0=32'h0;
reg		reg_2b7fa5b8_u0=1'h0;
reg		syncEnable_u1330_u0=1'h0;
reg		reg_08747a40_u0=1'h0;
wire		and_u3232_u0;
wire		or_u1336_u0;
wire	[31:0]	mux_u1779_u0;
wire		mux_u1780_u0;
wire	[31:0]	mux_u1781_u0;
wire		and_u3233_u0;
reg	[31:0]	syncEnable_u1331_u0=32'h0;
wire	[31:0]	add_u626;
wire	[31:0]	mux_u1782_u0;
wire		or_u1337_u0;
reg	[31:0]	syncEnable_u1332_u0=32'h0;
reg	[31:0]	syncEnable_u1333_u0=32'h0;
reg		block_GO_delayed_u89_u0=1'h0;
wire		or_u1338_u0;
wire	[31:0]	mux_u1783_u0;
reg	[31:0]	syncEnable_u1334_u0=32'h0;
reg	[31:0]	syncEnable_u1335_u0=32'h0;
reg	[31:0]	syncEnable_u1336_u0=32'h0;
reg		syncEnable_u1337_u0=1'h0;
reg	[31:0]	syncEnable_u1338_u0=32'h0;
wire		and_u3234_u0;
wire	[31:0]	mux_u1784_u0;
wire	[31:0]	mux_u1785_u0;
wire	[31:0]	mux_u1786_u0;
wire		or_u1339_u0;
wire	[31:0]	mux_u1787_u0;
wire	[31:0]	mux_u1788_u0;
wire		mux_u1789_u0;
wire	[15:0]	add_u627;
reg	[31:0]	latch_27a357a2_reg=32'h0;
wire	[31:0]	latch_27a357a2_out;
wire	[31:0]	latch_673569d1_out;
reg	[31:0]	latch_673569d1_reg=32'h0;
wire		latch_5bb25e9a_out;
reg		latch_5bb25e9a_reg=1'h0;
wire	[31:0]	latch_198aa701_out;
reg	[31:0]	latch_198aa701_reg=32'h0;
reg	[31:0]	latch_0f89107b_reg=32'h0;
wire	[31:0]	latch_0f89107b_out;
reg		reg_5d98a68e_u0=1'h0;
reg	[15:0]	syncEnable_u1339_u0=16'h0;
wire		scoreboard_353468e9_resOr0;
wire		scoreboard_353468e9_resOr1;
reg		scoreboard_353468e9_reg0=1'h0;
wire		scoreboard_353468e9_and;
reg		scoreboard_353468e9_reg1=1'h0;
wire		bus_357f6630_;
wire		and_u3235_u0;
reg	[31:0]	fbReg_tmp_u41=32'h0;
wire		or_u1340_u0;
wire	[31:0]	mux_u1790_u0;
reg		loopControl_u86=1'h0;
reg	[15:0]	fbReg_idx_u41=16'h0;
wire	[31:0]	mux_u1791_u0;
reg	[31:0]	fbReg_tmp_row_u41=32'h0;
wire	[31:0]	mux_u1792_u0;
reg	[31:0]	fbReg_tmp_row0_u41=32'h0;
reg		fbReg_swapped_u41=1'h0;
wire	[15:0]	mux_u1793_u0;
reg	[31:0]	fbReg_tmp_row1_u41=32'h0;
wire	[31:0]	mux_u1794_u0;
reg		syncEnable_u1340_u0=1'h0;
wire		mux_u1795_u0;
wire		and_u3236_u0;
wire	[7:0]	simplePinWrite;
wire		simplePinWrite_u460;
wire	[15:0]	simplePinWrite_u461;
reg		reg_22b5ec37_u0=1'h0;
wire	[31:0]	mux_u1796_u0;
wire		or_u1341_u0;
reg		reg_22b5ec37_result_delayed_u0=1'h0;
assign lessThan_a_unsigned=mux_u1793_u0;
assign lessThan_b_unsigned=16'h65;
assign lessThan=lessThan_a_unsigned<lessThan_b_unsigned;
assign andOp=lessThan&mux_u1795_u0;
assign and_u3219_u0=or_u1340_u0&andOp;
assign and_u3220_u0=or_u1340_u0&not_u725_u0;
assign not_u725_u0=~andOp;
assign and_u3221_u0=and_u3220_u0&or_u1340_u0;
always @(posedge CLK)
begin
if (or_u1339_u0)
syncEnable_u1322<=mux_u1788_u0;
end
assign lessThan_u84_a_signed={1'b0, mux_u1787_u0};
assign lessThan_u84_b_signed=33'h64;
assign lessThan_u84=lessThan_u84_a_signed<lessThan_u84_b_signed;
assign and_u3222_u0=or_u1339_u0&not_u726_u0;
assign and_u3223_u0=or_u1339_u0&lessThan_u84;
assign not_u726_u0=~lessThan_u84;
assign add=mux_u1787_u0+32'h0;
assign and_u3224_u0=and_u3234_u0&port_6829be08_;
assign add_u618=mux_u1787_u0+32'h1;
assign add_u619=add_u618+32'h0;
assign and_u3225_u0=and_u3234_u0&port_43a1623f_;
assign greaterThan_a_signed=syncEnable_u1336_u0;
assign greaterThan_b_signed=syncEnable_u1335_u0;
assign greaterThan=greaterThan_a_signed>greaterThan_b_signed;
assign not_u727_u0=~greaterThan;
assign and_u3226_u0=block_GO_delayed_u89_u0&greaterThan;
assign and_u3227_u0=block_GO_delayed_u89_u0&not_u727_u0;
assign add_u620=syncEnable_u1334_u0+32'h0;
assign and_u3228_u0=and_u3232_u0&port_6829be08_;
assign add_u621=syncEnable_u1334_u0+32'h1;
assign add_u622=add_u621+32'h0;
assign and_u3229_u0=and_u3232_u0&port_43a1623f_;
assign add_u623=syncEnable_u1334_u0+32'h0;
assign and_u3230_u0=reg_2ea1700e_u0&port_43a1623f_;
assign or_u1333_u0=and_u3230_u0|RESET;
always @(posedge CLK or posedge block_GO_delayed_u88 or posedge or_u1333_u0)
begin
if (or_u1333_u0)
reg_2ea1700e_u0<=1'h0;
else if (block_GO_delayed_u88)
reg_2ea1700e_u0<=1'h1;
else reg_2ea1700e_u0<=reg_2ea1700e_u0;
end
assign add_u624=syncEnable_u1334_u0+32'h1;
assign add_u625=add_u624+32'h0;
always @(posedge CLK or posedge reg_7713a0f6_u0 or posedge or_u1334_u0)
begin
if (or_u1334_u0)
reg_3d9faea6_u0<=1'h0;
else if (reg_7713a0f6_u0)
reg_3d9faea6_u0<=1'h1;
else reg_3d9faea6_u0<=reg_3d9faea6_u0;
end
assign or_u1334_u0=and_u3231_u0|RESET;
assign and_u3231_u0=reg_3d9faea6_u0&port_43a1623f_;
always @(posedge CLK)
begin
if (and_u3232_u0)
syncEnable_u1323_u0<=add_u625;
end
always @(posedge CLK)
begin
if (and_u3232_u0)
syncEnable_u1324_u0<=port_34368cc5_;
end
assign or_u1335_u0=block_GO_delayed_u88|reg_7713a0f6_u0;
assign mux_u1777=({32{block_GO_delayed_u88}}&syncEnable_u1327_u0)|({32{reg_7713a0f6_u0}}&syncEnable_u1323_u0)|({32{and_u3232_u0}}&add_u622);
assign mux_u1778_u0=(block_GO_delayed_u88)?syncEnable_u1328_u0:syncEnable_u1325_u0;
always @(posedge CLK or posedge RESET)
begin
if (RESET)
reg_7713a0f6_u0<=1'h0;
else reg_7713a0f6_u0<=block_GO_delayed_u88;
end
always @(posedge CLK)
begin
if (and_u3232_u0)
syncEnable_u1325_u0<=port_152892fb_;
end
always @(posedge CLK)
begin
if (and_u3232_u0)
syncEnable_u1326_u0<=port_152892fb_;
end
always @(posedge CLK)
begin
if (and_u3232_u0)
syncEnable_u1327_u0<=add_u623;
end
always @(posedge CLK or posedge RESET)
begin
if (RESET)
block_GO_delayed_u88<=1'h0;
else block_GO_delayed_u88<=and_u3232_u0;
end
always @(posedge CLK)
begin
if (and_u3232_u0)
syncEnable_u1328_u0<=port_34368cc5_;
end
always @(posedge CLK or posedge RESET)
begin
if (RESET)
reg_0043c563_u0<=1'h0;
else reg_0043c563_u0<=reg_4bbcffdd_u0;
end
always @(posedge CLK or posedge RESET)
begin
if (RESET)
reg_0043c563_result_delayed_u0<=1'h0;
else reg_0043c563_result_delayed_u0<=reg_0043c563_u0;
end
always @(posedge CLK or posedge RESET)
begin
if (RESET)
reg_4bbcffdd_u0<=1'h0;
else reg_4bbcffdd_u0<=reg_08747a40_u0;
end
always @(posedge CLK)
begin
if (block_GO_delayed_u89_u0)
syncEnable_u1329_u0<=syncEnable_u1333_u0;
end
always @(posedge CLK or posedge RESET)
begin
if (RESET)
reg_2b7fa5b8_u0<=1'h0;
else reg_2b7fa5b8_u0<=and_u3233_u0;
end
always @(posedge CLK)
begin
if (block_GO_delayed_u89_u0)
syncEnable_u1330_u0<=syncEnable_u1337_u0;
end
always @(posedge CLK or posedge RESET)
begin
if (RESET)
reg_08747a40_u0<=1'h0;
else reg_08747a40_u0<=and_u3232_u0;
end
assign and_u3232_u0=and_u3226_u0&block_GO_delayed_u89_u0;
assign or_u1336_u0=reg_0043c563_result_delayed_u0|reg_2b7fa5b8_u0;
assign mux_u1779_u0=(reg_0043c563_result_delayed_u0)?syncEnable_u1326_u0:syncEnable_u1331_u0;
assign mux_u1780_u0=(reg_0043c563_result_delayed_u0)?1'h1:syncEnable_u1330_u0;
assign mux_u1781_u0=(reg_0043c563_result_delayed_u0)?syncEnable_u1324_u0:syncEnable_u1329_u0;
assign and_u3233_u0=and_u3227_u0&block_GO_delayed_u89_u0;
always @(posedge CLK)
begin
if (block_GO_delayed_u89_u0)
syncEnable_u1331_u0<=syncEnable_u1332_u0;
end
assign add_u626=mux_u1787_u0+32'h1;
assign mux_u1782_u0=(and_u3234_u0)?add:add_u620;
assign or_u1337_u0=and_u3234_u0|and_u3232_u0;
always @(posedge CLK)
begin
if (and_u3234_u0)
syncEnable_u1332_u0<=mux_u1785_u0;
end
always @(posedge CLK)
begin
if (and_u3234_u0)
syncEnable_u1333_u0<=mux_u1784_u0;
end
always @(posedge CLK or posedge RESET)
begin
if (RESET)
block_GO_delayed_u89_u0<=1'h0;
else block_GO_delayed_u89_u0<=and_u3234_u0;
end
assign or_u1338_u0=and_u3234_u0|and_u3232_u0;
assign mux_u1783_u0=({32{or_u1335_u0}}&mux_u1777)|({32{and_u3234_u0}}&add_u619)|({32{and_u3232_u0}}&mux_u1777);
always @(posedge CLK)
begin
if (and_u3234_u0)
syncEnable_u1334_u0<=mux_u1787_u0;
end
always @(posedge CLK)
begin
if (and_u3234_u0)
syncEnable_u1335_u0<=port_34368cc5_;
end
always @(posedge CLK)
begin
if (and_u3234_u0)
syncEnable_u1336_u0<=port_152892fb_;
end
always @(posedge CLK)
begin
if (and_u3234_u0)
syncEnable_u1337_u0<=mux_u1789_u0;
end
always @(posedge CLK)
begin
if (and_u3234_u0)
syncEnable_u1338_u0<=add_u626;
end
assign and_u3234_u0=and_u3223_u0&or_u1339_u0;
assign mux_u1784_u0=(or_u1336_u0)?mux_u1781_u0:mux_u1791_u0;
assign mux_u1785_u0=(or_u1336_u0)?mux_u1779_u0:mux_u1792_u0;
assign mux_u1786_u0=(or_u1336_u0)?syncEnable_u1335_u0:mux_u1794_u0;
assign or_u1339_u0=or_u1336_u0|and_u3235_u0;
assign mux_u1787_u0=(or_u1336_u0)?syncEnable_u1338_u0:32'h0;
assign mux_u1788_u0=(or_u1336_u0)?syncEnable_u1336_u0:mux_u1790_u0;
assign mux_u1789_u0=(or_u1336_u0)?mux_u1780_u0:1'h0;
assign add_u627=mux_u1793_u0+16'h1;
always @(posedge CLK)
begin
if (or_u1336_u0)
latch_27a357a2_reg<=mux_u1781_u0;
end
assign latch_27a357a2_out=(or_u1336_u0)?mux_u1781_u0:latch_27a357a2_reg;
assign latch_673569d1_out=(or_u1336_u0)?syncEnable_u1322:latch_673569d1_reg;
always @(posedge CLK)
begin
if (or_u1336_u0)
latch_673569d1_reg<=syncEnable_u1322;
end
assign latch_5bb25e9a_out=(or_u1336_u0)?mux_u1780_u0:latch_5bb25e9a_reg;
always @(posedge CLK)
begin
if (or_u1336_u0)
latch_5bb25e9a_reg<=mux_u1780_u0;
end
assign latch_198aa701_out=(or_u1336_u0)?mux_u1779_u0:latch_198aa701_reg;
always @(posedge CLK)
begin
if (or_u1336_u0)
latch_198aa701_reg<=mux_u1779_u0;
end
always @(posedge CLK)
begin
if (or_u1336_u0)
latch_0f89107b_reg<=syncEnable_u1335_u0;
end
assign latch_0f89107b_out=(or_u1336_u0)?syncEnable_u1335_u0:latch_0f89107b_reg;
always @(posedge CLK or posedge RESET)
begin
if (RESET)
reg_5d98a68e_u0<=1'h0;
else reg_5d98a68e_u0<=or_u1336_u0;
end
always @(posedge CLK)
begin
if (and_u3235_u0)
syncEnable_u1339_u0<=add_u627;
end
assign scoreboard_353468e9_resOr0=reg_5d98a68e_u0|scoreboard_353468e9_reg0;
assign scoreboard_353468e9_resOr1=or_u1336_u0|scoreboard_353468e9_reg1;
always @(posedge CLK)
begin
if (bus_357f6630_)
scoreboard_353468e9_reg0<=1'h0;
else if (reg_5d98a68e_u0)
scoreboard_353468e9_reg0<=1'h1;
else scoreboard_353468e9_reg0<=scoreboard_353468e9_reg0;
end
assign scoreboard_353468e9_and=scoreboard_353468e9_resOr0&scoreboard_353468e9_resOr1;
always @(posedge CLK)
begin
if (bus_357f6630_)
scoreboard_353468e9_reg1<=1'h0;
else if (or_u1336_u0)
scoreboard_353468e9_reg1<=1'h1;
else scoreboard_353468e9_reg1<=scoreboard_353468e9_reg1;
end
assign bus_357f6630_=scoreboard_353468e9_and|RESET;
assign and_u3235_u0=and_u3219_u0&or_u1340_u0;
always @(posedge CLK)
begin
if (scoreboard_353468e9_and)
fbReg_tmp_u41<=latch_198aa701_out;
end
assign or_u1340_u0=GO|loopControl_u86;
assign mux_u1790_u0=(GO)?32'h0:fbReg_tmp_row_u41;
always @(posedge CLK or posedge syncEnable_u1340_u0)
begin
if (syncEnable_u1340_u0)
loopControl_u86<=1'h0;
else loopControl_u86<=scoreboard_353468e9_and;
end
always @(posedge CLK)
begin
if (scoreboard_353468e9_and)
fbReg_idx_u41<=syncEnable_u1339_u0;
end
assign mux_u1791_u0=(GO)?32'h0:fbReg_tmp_row1_u41;
always @(posedge CLK)
begin
if (scoreboard_353468e9_and)
fbReg_tmp_row_u41<=latch_673569d1_out;
end
assign mux_u1792_u0=(GO)?32'h0:fbReg_tmp_u41;
always @(posedge CLK)
begin
if (scoreboard_353468e9_and)
fbReg_tmp_row0_u41<=latch_0f89107b_out;
end
always @(posedge CLK)
begin
if (scoreboard_353468e9_and)
fbReg_swapped_u41<=latch_5bb25e9a_out;
end
assign mux_u1793_u0=(GO)?16'h0:fbReg_idx_u41;
always @(posedge CLK)
begin
if (scoreboard_353468e9_and)
fbReg_tmp_row1_u41<=latch_27a357a2_out;
end
assign mux_u1794_u0=(GO)?32'h0:fbReg_tmp_row0_u41;
always @(posedge CLK)
begin
if (GO)
syncEnable_u1340_u0<=RESET;
end
assign mux_u1795_u0=(GO)?1'h0:fbReg_swapped_u41;
assign and_u3236_u0=reg_22b5ec37_u0&port_6829be08_;
assign simplePinWrite=port_152892fb_[7:0];
assign simplePinWrite_u460=reg_22b5ec37_u0&{1{reg_22b5ec37_u0}};
assign simplePinWrite_u461=16'h1&{16{1'h1}};
always @(posedge CLK or posedge RESET)
begin
if (RESET)
reg_22b5ec37_u0<=1'h0;
else reg_22b5ec37_u0<=and_u3221_u0;
end
assign mux_u1796_u0=(or_u1337_u0)?mux_u1782_u0:32'h32;
assign or_u1341_u0=or_u1337_u0|reg_22b5ec37_u0;
always @(posedge CLK or posedge RESET)
begin
if (RESET)
reg_22b5ec37_result_delayed_u0<=1'h0;
else reg_22b5ec37_result_delayed_u0<=reg_22b5ec37_u0;
end
assign RESULT=GO;
assign RESULT_u1949=32'h0;
assign RESULT_u1950=or_u1341_u0;
assign RESULT_u1951=mux_u1796_u0;
assign RESULT_u1952=3'h1;
assign RESULT_u1953=or_u1338_u0;
assign RESULT_u1954=mux_u1783_u0;
assign RESULT_u1955=3'h1;
assign RESULT_u1956=or_u1335_u0;
assign RESULT_u1957=mux_u1783_u0;
assign RESULT_u1958=mux_u1778_u0;
assign RESULT_u1959=3'h1;
assign RESULT_u1960=simplePinWrite_u461;
assign RESULT_u1961=simplePinWrite;
assign RESULT_u1962=simplePinWrite_u460;
assign DONE=reg_22b5ec37_result_delayed_u0;
endmodule



module medianRow1_scheduler(CLK, RESET, GO, port_6074fca8_, port_0eecd18d_, port_18f257dd_, port_00718719_, port_3e61550a_, port_6c8d7d8b_, RESULT, RESULT_u1963, RESULT_u1964, RESULT_u1965, DONE);
input		CLK;
input		RESET;
input		GO;
input		port_6074fca8_;
input	[31:0]	port_0eecd18d_;
input		port_18f257dd_;
input		port_00718719_;
input		port_3e61550a_;
input		port_6c8d7d8b_;
output		RESULT;
output		RESULT_u1963;
output		RESULT_u1964;
output		RESULT_u1965;
output		DONE;
wire		lessThan;
wire signed	[31:0]	lessThan_a_signed;
wire signed	[31:0]	lessThan_b_signed;
wire		equals;
wire signed	[31:0]	equals_a_signed;
wire signed	[31:0]	equals_b_signed;
wire signed	[31:0]	equals_u193_a_signed;
wire signed	[31:0]	equals_u193_b_signed;
wire		equals_u193;
wire		and_u3237_u0;
wire		not_u728_u0;
wire		and_u3238_u0;
wire		andOp;
wire		not_u729_u0;
wire		and_u3239_u0;
wire		and_u3240_u0;
wire		simplePinWrite;
wire		and_u3241_u0;
wire		and_u3242_u0;
wire signed	[31:0]	equals_u194_a_signed;
wire signed	[31:0]	equals_u194_b_signed;
wire		equals_u194;
wire		and_u3243_u0;
wire		and_u3244_u0;
wire		not_u730_u0;
wire		andOp_u60;
wire		not_u731_u0;
wire		and_u3245_u0;
wire		and_u3246_u0;
wire		simplePinWrite_u462;
wire		not_u732_u0;
wire		and_u3247_u0;
wire		and_u3248_u0;
wire		and_u3249_u0;
wire		not_u733_u0;
wire		and_u3250_u0;
wire		simplePinWrite_u463;
reg		reg_609fcb4f_u0=1'h0;
wire		or_u1342_u0;
wire		and_u3251_u0;
wire		and_u3252_u0;
wire		and_u3253_u0;
wire		and_u3254_u0;
wire		or_u1343_u0;
reg		and_delayed_u371=1'h0;
wire		and_u3255_u0;
wire		mux_u1797;
wire		or_u1344_u0;
wire		and_u3256_u0;
reg		and_delayed_u372_u0=1'h0;
wire		or_u1345_u0;
wire		or_u1346_u0;
wire		and_u3257_u0;
wire		and_u3258_u0;
reg		and_delayed_u373_u0=1'h0;
wire		mux_u1798_u0;
wire		or_u1347_u0;
wire		receive_go_merge;
wire		and_u3259_u0;
wire		or_u1348_u0;
reg		loopControl_u87=1'h0;
reg		syncEnable_u1341=1'h0;
wire		mux_u1799_u0;
wire		or_u1349_u0;
reg		reg_5454854e_u0=1'h0;
reg		reg_29ea6b90_u0=1'h0;
assign lessThan_a_signed=port_0eecd18d_;
assign lessThan_b_signed=32'h65;
assign lessThan=lessThan_a_signed<lessThan_b_signed;
assign equals_a_signed=port_0eecd18d_;
assign equals_b_signed=32'h65;
assign equals=equals_a_signed==equals_b_signed;
assign equals_u193_a_signed={31'b0, port_6074fca8_};
assign equals_u193_b_signed=32'h0;
assign equals_u193=equals_u193_a_signed==equals_u193_b_signed;
assign and_u3237_u0=and_u3259_u0&equals_u193;
assign not_u728_u0=~equals_u193;
assign and_u3238_u0=and_u3259_u0&not_u728_u0;
assign andOp=lessThan&port_00718719_;
assign not_u729_u0=~andOp;
assign and_u3239_u0=and_u3242_u0&andOp;
assign and_u3240_u0=and_u3242_u0&not_u729_u0;
assign simplePinWrite=and_u3241_u0&{1{and_u3241_u0}};
assign and_u3241_u0=and_u3239_u0&and_u3242_u0;
assign and_u3242_u0=and_u3237_u0&and_u3259_u0;
assign equals_u194_a_signed={31'b0, port_6074fca8_};
assign equals_u194_b_signed=32'h1;
assign equals_u194=equals_u194_a_signed==equals_u194_b_signed;
assign and_u3243_u0=and_u3259_u0&not_u730_u0;
assign and_u3244_u0=and_u3259_u0&equals_u194;
assign not_u730_u0=~equals_u194;
assign andOp_u60=lessThan&port_00718719_;
assign not_u731_u0=~andOp_u60;
assign and_u3245_u0=and_u3257_u0&not_u731_u0;
assign and_u3246_u0=and_u3257_u0&andOp_u60;
assign simplePinWrite_u462=and_u3256_u0&{1{and_u3256_u0}};
assign not_u732_u0=~equals;
assign and_u3247_u0=and_u3255_u0&equals;
assign and_u3248_u0=and_u3255_u0&not_u732_u0;
assign and_u3249_u0=and_u3253_u0&port_6c8d7d8b_;
assign not_u733_u0=~port_6c8d7d8b_;
assign and_u3250_u0=and_u3253_u0&not_u733_u0;
assign simplePinWrite_u463=and_u3252_u0&{1{and_u3252_u0}};
always @(posedge CLK or posedge RESET)
begin
if (RESET)
reg_609fcb4f_u0<=1'h0;
else reg_609fcb4f_u0<=and_u3251_u0;
end
assign or_u1342_u0=port_3e61550a_|reg_609fcb4f_u0;
assign and_u3251_u0=and_u3250_u0&and_u3253_u0;
assign and_u3252_u0=and_u3249_u0&and_u3253_u0;
assign and_u3253_u0=and_u3247_u0&and_u3255_u0;
assign and_u3254_u0=and_u3248_u0&and_u3255_u0;
assign or_u1343_u0=and_delayed_u371|or_u1342_u0;
always @(posedge CLK or posedge RESET)
begin
if (RESET)
and_delayed_u371<=1'h0;
else and_delayed_u371<=and_u3254_u0;
end
assign and_u3255_u0=and_u3245_u0&and_u3257_u0;
assign mux_u1797=(and_u3256_u0)?1'h1:1'h0;
assign or_u1344_u0=and_u3256_u0|and_u3252_u0;
assign and_u3256_u0=and_u3246_u0&and_u3257_u0;
always @(posedge CLK or posedge RESET)
begin
if (RESET)
and_delayed_u372_u0<=1'h0;
else and_delayed_u372_u0<=and_u3256_u0;
end
assign or_u1345_u0=or_u1343_u0|and_delayed_u372_u0;
assign or_u1346_u0=or_u1345_u0|and_delayed_u373_u0;
assign and_u3257_u0=and_u3244_u0&and_u3259_u0;
assign and_u3258_u0=and_u3243_u0&and_u3259_u0;
always @(posedge CLK or posedge RESET)
begin
if (RESET)
and_delayed_u373_u0<=1'h0;
else and_delayed_u373_u0<=and_u3258_u0;
end
assign mux_u1798_u0=(and_u3241_u0)?1'h1:mux_u1797;
assign or_u1347_u0=and_u3241_u0|or_u1344_u0;
assign receive_go_merge=simplePinWrite|simplePinWrite_u462;
assign and_u3259_u0=or_u1348_u0&or_u1348_u0;
assign or_u1348_u0=reg_5454854e_u0|loopControl_u87;
always @(posedge CLK or posedge syncEnable_u1341)
begin
if (syncEnable_u1341)
loopControl_u87<=1'h0;
else loopControl_u87<=or_u1346_u0;
end
always @(posedge CLK)
begin
if (reg_5454854e_u0)
syncEnable_u1341<=RESET;
end
assign mux_u1799_u0=(GO)?1'h0:mux_u1798_u0;
assign or_u1349_u0=GO|or_u1347_u0;
always @(posedge CLK or posedge RESET)
begin
if (RESET)
reg_5454854e_u0<=1'h0;
else reg_5454854e_u0<=reg_29ea6b90_u0;
end
always @(posedge CLK or posedge RESET)
begin
if (RESET)
reg_29ea6b90_u0<=1'h0;
else reg_29ea6b90_u0<=GO;
end
assign RESULT=or_u1349_u0;
assign RESULT_u1963=mux_u1799_u0;
assign RESULT_u1964=simplePinWrite_u463;
assign RESULT_u1965=receive_go_merge;
assign DONE=1'h0;
endmodule


