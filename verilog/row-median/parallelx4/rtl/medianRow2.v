// __  ___ __ ___  _ __   ___  ___ 
// \ \/ / '__/ _ \| '_ \ / _ \/ __|
//  >  <| | | (_) | | | | (_) \__ \
// /_/\_\_|  \___/|_| |_|\___/|___/
// 
// Xronos synthesizer version
// Run date: Fri 23 Feb 2018 14:27:59 +0000
// 

module medianRow2(RESET, in1_DATA, in1_SEND, in1_ACK, median_RDY, median_COUNT, median_SEND, CLK, in1_COUNT, median_ACK, median_DATA);
input		RESET;
input	[7:0]	in1_DATA;
input		in1_SEND;
output		in1_ACK;
wire		receive_done;
input		median_RDY;
output	[15:0]	median_COUNT;
output		median_SEND;
input		CLK;
input	[15:0]	in1_COUNT;
wire		compute_median_go;
input		median_ACK;
wire		receive_go;
output	[7:0]	median_DATA;
wire		compute_median_done;
wire		bus_52056a8b_;
wire		scheduler;
wire		scheduler_u762;
wire		scheduler_u763;
wire		scheduler_u761;
wire		medianRow2_scheduler_instance_DONE;
wire		bus_0667b835_;
wire		bus_3bc9d5ca_;
wire	[31:0]	bus_0e389e39_;
wire	[31:0]	bus_46bb48e9_;
wire		bus_1927a308_;
wire	[31:0]	bus_0e104710_;
wire	[31:0]	bus_2b6556c2_;
wire	[31:0]	bus_1fb0e3fe_;
wire		bus_6ac7c410_;
wire		bus_563c0487_;
wire	[2:0]	bus_770e037d_;
wire		bus_5235cae6_;
wire		bus_172ff744_;
wire	[31:0]	receive_u242;
wire		receive;
wire	[2:0]	receive_u244;
wire	[31:0]	receive_u240;
wire		receive_u245;
wire		medianRow2_receive_instance_DONE;
wire		receive_u241;
wire	[31:0]	receive_u243;
wire		bus_3f194dfb_;
wire	[2:0]	compute_median_u563;
wire	[2:0]	compute_median_u567;
wire		compute_median_u572;
wire	[31:0]	compute_median_u565;
wire	[2:0]	compute_median_u570;
wire		compute_median_u561;
wire	[31:0]	compute_median_u560;
wire	[31:0]	compute_median_u569;
wire	[31:0]	compute_median_u562;
wire		compute_median;
wire	[7:0]	compute_median_u573;
wire	[31:0]	compute_median_u566;
wire		compute_median_u564;
wire	[15:0]	compute_median_u571;
wire		medianRow2_compute_median_instance_DONE;
wire		compute_median_u568;
wire		bus_21646f5a_;
wire		bus_259878bd_;
wire	[31:0]	bus_7b03152e_;
wire		bus_0927b503_;
wire	[31:0]	bus_1ca57154_;
wire		bus_4a3de72a_;
wire	[2:0]	bus_667319c3_;
wire	[31:0]	bus_5f93a996_;
wire		bus_606cbacc_;
wire	[31:0]	bus_4b94181f_;
assign in1_ACK=receive_u245;
assign receive_done=bus_3f194dfb_;
assign median_COUNT=compute_median_u571;
assign median_SEND=compute_median_u572;
assign compute_median_go=scheduler_u762;
assign receive_go=scheduler_u763;
assign median_DATA=compute_median_u573;
assign compute_median_done=bus_21646f5a_;
medianRow2_stateVar_fsmState_medianRow2 medianRow2_stateVar_fsmState_medianRow2_1(.bus_1738412d_(CLK), 
  .bus_33a610d4_(bus_0667b835_), .bus_07a5083e_(scheduler), .bus_51629ae2_(scheduler_u761), 
  .bus_52056a8b_(bus_52056a8b_));
medianRow2_scheduler medianRow2_scheduler_instance(.CLK(CLK), .RESET(bus_0667b835_), 
  .GO(bus_259878bd_), .port_144e9a60_(bus_52056a8b_), .port_615a6e73_(bus_4b94181f_), 
  .port_13f24c41_(median_RDY), .port_200b215a_(compute_median_done), .port_49ecbcd2_(in1_SEND), 
  .port_3532fe61_(receive_done), .DONE(medianRow2_scheduler_instance_DONE), .RESULT(scheduler), 
  .RESULT_u1920(scheduler_u761), .RESULT_u1921(scheduler_u762), .RESULT_u1922(scheduler_u763));
medianRow2_globalreset_physical_20fb7b72_ medianRow2_globalreset_physical_20fb7b72__1(.bus_0578c53e_(CLK), 
  .bus_24c97a29_(RESET), .bus_0667b835_(bus_0667b835_));
medianRow2_structuralmemory_048501cb_ medianRow2_structuralmemory_048501cb__1(.CLK_u86(CLK), 
  .bus_5e0e7bd9_(bus_0667b835_), .bus_75fa6998_(bus_1ca57154_), .bus_1401863a_(3'h1), 
  .bus_64a9a75e_(bus_4a3de72a_), .bus_79590001_(bus_2b6556c2_), .bus_1617e17a_(3'h1), 
  .bus_38daf567_(bus_5235cae6_), .bus_1c943fe2_(bus_563c0487_), .bus_1c0d7037_(bus_1fb0e3fe_), 
  .bus_0e389e39_(bus_0e389e39_), .bus_1927a308_(bus_1927a308_), .bus_46bb48e9_(bus_46bb48e9_), 
  .bus_3bc9d5ca_(bus_3bc9d5ca_));
medianRow2_simplememoryreferee_75d84f17_ medianRow2_simplememoryreferee_75d84f17__1(.bus_3150bbf2_(CLK), 
  .bus_0affd4ba_(bus_0667b835_), .bus_04338566_(bus_3bc9d5ca_), .bus_414489ec_(bus_46bb48e9_), 
  .bus_22a90075_(receive_u241), .bus_7873a4e9_({24'b0, receive_u243[7:0]}), .bus_4733e576_(receive_u242), 
  .bus_5816e6cd_(3'h1), .bus_5a23ef00_(compute_median_u568), .bus_12a0e4a3_(compute_median_u564), 
  .bus_4f9cd513_(compute_median_u566), .bus_7f42d1dc_(compute_median_u565), .bus_68aceb65_(3'h1), 
  .bus_1fb0e3fe_(bus_1fb0e3fe_), .bus_2b6556c2_(bus_2b6556c2_), .bus_563c0487_(bus_563c0487_), 
  .bus_5235cae6_(bus_5235cae6_), .bus_770e037d_(bus_770e037d_), .bus_6ac7c410_(bus_6ac7c410_), 
  .bus_0e104710_(bus_0e104710_), .bus_172ff744_(bus_172ff744_));
medianRow2_receive medianRow2_receive_instance(.CLK(CLK), .RESET(bus_0667b835_), 
  .GO(receive_go), .port_3b4deb68_(bus_4b94181f_), .port_56ba8b92_(bus_6ac7c410_), 
  .port_018fb986_(in1_DATA), .DONE(medianRow2_receive_instance_DONE), .RESULT(receive), 
  .RESULT_u1923(receive_u240), .RESULT_u1924(receive_u241), .RESULT_u1925(receive_u242), 
  .RESULT_u1926(receive_u243), .RESULT_u1927(receive_u244), .RESULT_u1928(receive_u245));
assign bus_3f194dfb_=medianRow2_receive_instance_DONE&{1{medianRow2_receive_instance_DONE}};
medianRow2_compute_median medianRow2_compute_median_instance(.CLK(CLK), .RESET(bus_0667b835_), 
  .GO(compute_median_go), .port_2f0fcf3b_(bus_606cbacc_), .port_1a513609_(bus_7b03152e_), 
  .port_62c7b7a4_(bus_172ff744_), .port_0fd59e64_(bus_172ff744_), .port_60ba7fd2_(bus_0e104710_), 
  .DONE(medianRow2_compute_median_instance_DONE), .RESULT(compute_median), .RESULT_u1929(compute_median_u560), 
  .RESULT_u1930(compute_median_u561), .RESULT_u1931(compute_median_u562), .RESULT_u1932(compute_median_u563), 
  .RESULT_u1936(compute_median_u564), .RESULT_u1937(compute_median_u565), .RESULT_u1938(compute_median_u566), 
  .RESULT_u1939(compute_median_u567), .RESULT_u1933(compute_median_u568), .RESULT_u1934(compute_median_u569), 
  .RESULT_u1935(compute_median_u570), .RESULT_u1940(compute_median_u571), .RESULT_u1941(compute_median_u572), 
  .RESULT_u1942(compute_median_u573));
assign bus_21646f5a_=medianRow2_compute_median_instance_DONE&{1{medianRow2_compute_median_instance_DONE}};
medianRow2_Kicker_50 medianRow2_Kicker_50_1(.CLK(CLK), .RESET(bus_0667b835_), .bus_259878bd_(bus_259878bd_));
medianRow2_simplememoryreferee_11c18fee_ medianRow2_simplememoryreferee_11c18fee__1(.bus_712c2001_(CLK), 
  .bus_679456eb_(bus_0667b835_), .bus_7f1b8f4b_(bus_1927a308_), .bus_2b8f88eb_(bus_0e389e39_), 
  .bus_05c0a954_(compute_median_u561), .bus_532ffaac_(compute_median_u562), .bus_182d4b07_(3'h1), 
  .bus_5f93a996_(bus_5f93a996_), .bus_1ca57154_(bus_1ca57154_), .bus_0927b503_(bus_0927b503_), 
  .bus_4a3de72a_(bus_4a3de72a_), .bus_667319c3_(bus_667319c3_), .bus_7b03152e_(bus_7b03152e_), 
  .bus_606cbacc_(bus_606cbacc_));
medianRow2_stateVar_i medianRow2_stateVar_i_1(.bus_4ffa273d_(CLK), .bus_7a280464_(bus_0667b835_), 
  .bus_09b6d000_(receive), .bus_2ddae079_(receive_u240), .bus_642b7542_(compute_median), 
  .bus_57446513_(32'h0), .bus_4b94181f_(bus_4b94181f_));
endmodule



module medianRow2_endianswapper_6f84c217_(endianswapper_6f84c217_in, endianswapper_6f84c217_out);
input		endianswapper_6f84c217_in;
output		endianswapper_6f84c217_out;
assign endianswapper_6f84c217_out=endianswapper_6f84c217_in;
endmodule



module medianRow2_endianswapper_2026ad03_(endianswapper_2026ad03_in, endianswapper_2026ad03_out);
input		endianswapper_2026ad03_in;
output		endianswapper_2026ad03_out;
assign endianswapper_2026ad03_out=endianswapper_2026ad03_in;
endmodule



module medianRow2_stateVar_fsmState_medianRow2(bus_1738412d_, bus_33a610d4_, bus_07a5083e_, bus_51629ae2_, bus_52056a8b_);
input		bus_1738412d_;
input		bus_33a610d4_;
input		bus_07a5083e_;
input		bus_51629ae2_;
output		bus_52056a8b_;
wire		endianswapper_6f84c217_out;
wire		endianswapper_2026ad03_out;
reg		stateVar_fsmState_medianRow2_u4=1'h0;
medianRow2_endianswapper_6f84c217_ medianRow2_endianswapper_6f84c217__1(.endianswapper_6f84c217_in(bus_51629ae2_), 
  .endianswapper_6f84c217_out(endianswapper_6f84c217_out));
medianRow2_endianswapper_2026ad03_ medianRow2_endianswapper_2026ad03__1(.endianswapper_2026ad03_in(stateVar_fsmState_medianRow2_u4), 
  .endianswapper_2026ad03_out(endianswapper_2026ad03_out));
always @(posedge bus_1738412d_ or posedge bus_33a610d4_)
begin
if (bus_33a610d4_)
stateVar_fsmState_medianRow2_u4<=1'h0;
else if (bus_07a5083e_)
stateVar_fsmState_medianRow2_u4<=endianswapper_6f84c217_out;
end
assign bus_52056a8b_=endianswapper_2026ad03_out;
endmodule



module medianRow2_scheduler(CLK, RESET, GO, port_144e9a60_, port_615a6e73_, port_13f24c41_, port_200b215a_, port_49ecbcd2_, port_3532fe61_, RESULT, RESULT_u1920, RESULT_u1921, RESULT_u1922, DONE);
input		CLK;
input		RESET;
input		GO;
input		port_144e9a60_;
input	[31:0]	port_615a6e73_;
input		port_13f24c41_;
input		port_200b215a_;
input		port_49ecbcd2_;
input		port_3532fe61_;
output		RESULT;
output		RESULT_u1920;
output		RESULT_u1921;
output		RESULT_u1922;
output		DONE;
wire		and_u3176_u0;
wire		lessThan;
wire signed	[31:0]	lessThan_b_signed;
wire signed	[31:0]	lessThan_a_signed;
wire		equals;
wire signed	[31:0]	equals_b_signed;
wire signed	[31:0]	equals_a_signed;
wire signed	[31:0]	equals_u191_b_signed;
wire		equals_u191;
wire signed	[31:0]	equals_u191_a_signed;
wire		and_u3177_u0;
wire		not_u716_u0;
wire		and_u3178_u0;
wire		andOp;
wire		and_u3179_u0;
wire		and_u3180_u0;
wire		not_u717_u0;
wire		simplePinWrite;
wire		and_u3181_u0;
wire		and_u3182_u0;
wire signed	[31:0]	equals_u192_b_signed;
wire		equals_u192;
wire signed	[31:0]	equals_u192_a_signed;
wire		and_u3183_u0;
wire		and_u3184_u0;
wire		not_u718_u0;
wire		andOp_u59;
wire		and_u3185_u0;
wire		not_u719_u0;
wire		and_u3186_u0;
wire		simplePinWrite_u456;
wire		and_u3187_u0;
wire		not_u720_u0;
wire		and_u3188_u0;
wire		and_u3189_u0;
wire		not_u721_u0;
wire		and_u3190_u0;
wire		simplePinWrite_u457;
wire		and_u3191_u0;
wire		and_u3192_u0;
wire		or_u1314_u0;
reg		and_delayed_u367=1'h0;
wire		and_u3193_u0;
wire		or_u1315_u0;
reg		and_delayed_u368_u0=1'h0;
wire		and_u3194_u0;
wire		mux_u1754;
wire		or_u1316_u0;
wire		and_u3195_u0;
wire		and_u3196_u0;
wire		or_u1317_u0;
reg		and_delayed_u369_u0=1'h0;
reg		reg_6e15724d_u0=1'h0;
wire		or_u1318_u0;
wire		and_u3197_u0;
wire		and_u3198_u0;
wire		receive_go_merge;
wire		or_u1319_u0;
wire		mux_u1755_u0;
reg		loopControl_u84=1'h0;
wire		or_u1320_u0;
reg		syncEnable_u1301=1'h0;
reg		reg_2b525ac8_u0=1'h0;
reg		reg_44b79550_u0=1'h0;
wire		mux_u1756_u0;
wire		or_u1321_u0;
assign and_u3176_u0=or_u1320_u0&or_u1320_u0;
assign lessThan_a_signed=port_615a6e73_;
assign lessThan_b_signed=32'h65;
assign lessThan=lessThan_a_signed<lessThan_b_signed;
assign equals_a_signed=port_615a6e73_;
assign equals_b_signed=32'h65;
assign equals=equals_a_signed==equals_b_signed;
assign equals_u191_a_signed={31'b0, port_144e9a60_};
assign equals_u191_b_signed=32'h0;
assign equals_u191=equals_u191_a_signed==equals_u191_b_signed;
assign and_u3177_u0=and_u3176_u0&equals_u191;
assign not_u716_u0=~equals_u191;
assign and_u3178_u0=and_u3176_u0&not_u716_u0;
assign andOp=lessThan&port_49ecbcd2_;
assign and_u3179_u0=and_u3182_u0&andOp;
assign and_u3180_u0=and_u3182_u0&not_u717_u0;
assign not_u717_u0=~andOp;
assign simplePinWrite=and_u3181_u0&{1{and_u3181_u0}};
assign and_u3181_u0=and_u3179_u0&and_u3182_u0;
assign and_u3182_u0=and_u3177_u0&and_u3176_u0;
assign equals_u192_a_signed={31'b0, port_144e9a60_};
assign equals_u192_b_signed=32'h1;
assign equals_u192=equals_u192_a_signed==equals_u192_b_signed;
assign and_u3183_u0=and_u3176_u0&equals_u192;
assign and_u3184_u0=and_u3176_u0&not_u718_u0;
assign not_u718_u0=~equals_u192;
assign andOp_u59=lessThan&port_49ecbcd2_;
assign and_u3185_u0=and_u3197_u0&not_u719_u0;
assign not_u719_u0=~andOp_u59;
assign and_u3186_u0=and_u3197_u0&andOp_u59;
assign simplePinWrite_u456=and_u3196_u0&{1{and_u3196_u0}};
assign and_u3187_u0=and_u3195_u0&equals;
assign not_u720_u0=~equals;
assign and_u3188_u0=and_u3195_u0&not_u720_u0;
assign and_u3189_u0=and_u3194_u0&port_13f24c41_;
assign not_u721_u0=~port_13f24c41_;
assign and_u3190_u0=and_u3194_u0&not_u721_u0;
assign simplePinWrite_u457=and_u3192_u0&{1{and_u3192_u0}};
assign and_u3191_u0=and_u3190_u0&and_u3194_u0;
assign and_u3192_u0=and_u3189_u0&and_u3194_u0;
assign or_u1314_u0=port_200b215a_|and_delayed_u367;
always @(posedge CLK or posedge RESET)
begin
if (RESET)
and_delayed_u367<=1'h0;
else and_delayed_u367<=and_u3191_u0;
end
assign and_u3193_u0=and_u3188_u0&and_u3195_u0;
assign or_u1315_u0=or_u1314_u0|and_delayed_u368_u0;
always @(posedge CLK or posedge RESET)
begin
if (RESET)
and_delayed_u368_u0<=1'h0;
else and_delayed_u368_u0<=and_u3193_u0;
end
assign and_u3194_u0=and_u3187_u0&and_u3195_u0;
assign mux_u1754=(and_u3196_u0)?1'h1:1'h0;
assign or_u1316_u0=and_u3196_u0|and_u3192_u0;
assign and_u3195_u0=and_u3185_u0&and_u3197_u0;
assign and_u3196_u0=and_u3186_u0&and_u3197_u0;
assign or_u1317_u0=or_u1315_u0|and_delayed_u369_u0;
always @(posedge CLK or posedge RESET)
begin
if (RESET)
and_delayed_u369_u0<=1'h0;
else and_delayed_u369_u0<=and_u3196_u0;
end
always @(posedge CLK or posedge RESET)
begin
if (RESET)
reg_6e15724d_u0<=1'h0;
else reg_6e15724d_u0<=and_u3198_u0;
end
assign or_u1318_u0=or_u1317_u0|reg_6e15724d_u0;
assign and_u3197_u0=and_u3183_u0&and_u3176_u0;
assign and_u3198_u0=and_u3184_u0&and_u3176_u0;
assign receive_go_merge=simplePinWrite|simplePinWrite_u456;
assign or_u1319_u0=and_u3181_u0|or_u1316_u0;
assign mux_u1755_u0=(and_u3181_u0)?1'h1:mux_u1754;
always @(posedge CLK or posedge syncEnable_u1301)
begin
if (syncEnable_u1301)
loopControl_u84<=1'h0;
else loopControl_u84<=or_u1318_u0;
end
assign or_u1320_u0=reg_2b525ac8_u0|loopControl_u84;
always @(posedge CLK)
begin
if (reg_2b525ac8_u0)
syncEnable_u1301<=RESET;
end
always @(posedge CLK or posedge RESET)
begin
if (RESET)
reg_2b525ac8_u0<=1'h0;
else reg_2b525ac8_u0<=reg_44b79550_u0;
end
always @(posedge CLK or posedge RESET)
begin
if (RESET)
reg_44b79550_u0<=1'h0;
else reg_44b79550_u0<=GO;
end
assign mux_u1756_u0=(GO)?1'h0:mux_u1755_u0;
assign or_u1321_u0=GO|or_u1319_u0;
assign RESULT=or_u1321_u0;
assign RESULT_u1920=mux_u1756_u0;
assign RESULT_u1921=simplePinWrite_u457;
assign RESULT_u1922=receive_go_merge;
assign DONE=1'h0;
endmodule



module medianRow2_globalreset_physical_20fb7b72_(bus_0578c53e_, bus_24c97a29_, bus_0667b835_);
input		bus_0578c53e_;
input		bus_24c97a29_;
output		bus_0667b835_;
reg		sample_u50=1'h0;
reg		final_u50=1'h1;
wire		or_7a31f81d_u0;
wire		not_1dae1323_u0;
reg		cross_u50=1'h0;
reg		glitch_u50=1'h0;
wire		and_0e0418cb_u0;
always @(posedge bus_0578c53e_)
begin
sample_u50<=1'h1;
end
assign bus_0667b835_=or_7a31f81d_u0;
always @(posedge bus_0578c53e_)
begin
final_u50<=not_1dae1323_u0;
end
assign or_7a31f81d_u0=bus_24c97a29_|final_u50;
assign not_1dae1323_u0=~and_0e0418cb_u0;
always @(posedge bus_0578c53e_)
begin
cross_u50<=sample_u50;
end
always @(posedge bus_0578c53e_)
begin
glitch_u50<=cross_u50;
end
assign and_0e0418cb_u0=cross_u50&glitch_u50;
endmodule



module medianRow2_forge_memory_101x32_133(CLK, ENA, WEA, DINA, ENB, ADDRA, ADDRB, DOUTA, DOUTB, DONEA, DONEB);
input		CLK;
input		ENA;
input		WEA;
input	[31:0]	DINA;
input		ENB;
input	[31:0]	ADDRA;
input	[31:0]	ADDRB;
output	[31:0]	DOUTA;
output	[31:0]	DOUTB;
output		DONEA;
output		DONEB;
wire		wea_0;
wire	[31:0]	pre_douta_0;
wire	[31:0]	pre_doutb_0;
wire		wea_1;
wire	[31:0]	pre_douta_1;
wire	[31:0]	pre_doutb_1;
reg		wea_done;
reg		rea_done;
reg	[31:0]	mux_outa;
reg	[31:0]	mux_outb;
assign wea_0=WEA&(ADDRA[31:6]==26'h0);
assign wea_1=WEA&(ADDRA[31:6]==26'h1);
always @(posedge CLK)
begin
wea_done<=WEA;
end
assign DONEA=rea_done|wea_done;
always @(posedge CLK)
begin
end
assign DONEB=ENB;
always @(pre_douta_0 or pre_douta_1 or ADDRA)
begin
case (ADDRA[31:6])26'd0:mux_outa=pre_douta_0;
26'd1:mux_outa=pre_douta_1;
default:mux_outa=32'h0;
endcase end
assign DOUTA=mux_outa;
always @(pre_doutb_0 or pre_doutb_1 or ADDRB)
begin
case (ADDRB[31:6])26'd0:mux_outb=pre_doutb_0;
26'd1:mux_outb=pre_doutb_1;
default:mux_outb=32'h0;
endcase end
assign DOUTB=mux_outb;
// Memory array element: COL: 0, ROW: 0
//  Initialization of Dual Port LUT ram now done through explicit
// parameter setting.
RAM64X1D#(.INIT(64'h0000000000000000))RAM64X1D_instance_2560(.WCLK(CLK), .WE(wea_0), 
  .A0(ADDRA[0]), .A1(ADDRA[1]), .A2(ADDRA[2]), .A3(ADDRA[3]), .A4(ADDRA[4]), .A5(ADDRA[5]), 
  .D(DINA[0]), .SPO(pre_douta_0[0]), .DPRA0(ADDRB[0]), .DPRA1(ADDRB[1]), .DPRA2(ADDRB[2]), 
  .DPRA3(ADDRB[3]), .DPRA4(ADDRB[4]), .DPRA5(ADDRB[5]), .DPO(pre_doutb_0[0]));
// Memory array element: COL: 0, ROW: 1
//  Initialization of Dual Port LUT ram now done through explicit
// parameter setting.
RAM64X1D#(.INIT(64'h0000000000000000))RAM64X1D_instance_2561(.WCLK(CLK), .WE(wea_0), 
  .A0(ADDRA[0]), .A1(ADDRA[1]), .A2(ADDRA[2]), .A3(ADDRA[3]), .A4(ADDRA[4]), .A5(ADDRA[5]), 
  .D(DINA[1]), .SPO(pre_douta_0[1]), .DPRA0(ADDRB[0]), .DPRA1(ADDRB[1]), .DPRA2(ADDRB[2]), 
  .DPRA3(ADDRB[3]), .DPRA4(ADDRB[4]), .DPRA5(ADDRB[5]), .DPO(pre_doutb_0[1]));
// Memory array element: COL: 0, ROW: 2
//  Initialization of Dual Port LUT ram now done through explicit
// parameter setting.
RAM64X1D#(.INIT(64'h0000000000000000))RAM64X1D_instance_2562(.WCLK(CLK), .WE(wea_0), 
  .A0(ADDRA[0]), .A1(ADDRA[1]), .A2(ADDRA[2]), .A3(ADDRA[3]), .A4(ADDRA[4]), .A5(ADDRA[5]), 
  .D(DINA[2]), .SPO(pre_douta_0[2]), .DPRA0(ADDRB[0]), .DPRA1(ADDRB[1]), .DPRA2(ADDRB[2]), 
  .DPRA3(ADDRB[3]), .DPRA4(ADDRB[4]), .DPRA5(ADDRB[5]), .DPO(pre_doutb_0[2]));
// Memory array element: COL: 0, ROW: 3
//  Initialization of Dual Port LUT ram now done through explicit
// parameter setting.
RAM64X1D#(.INIT(64'h0000000000000000))RAM64X1D_instance_2563(.WCLK(CLK), .WE(wea_0), 
  .A0(ADDRA[0]), .A1(ADDRA[1]), .A2(ADDRA[2]), .A3(ADDRA[3]), .A4(ADDRA[4]), .A5(ADDRA[5]), 
  .D(DINA[3]), .SPO(pre_douta_0[3]), .DPRA0(ADDRB[0]), .DPRA1(ADDRB[1]), .DPRA2(ADDRB[2]), 
  .DPRA3(ADDRB[3]), .DPRA4(ADDRB[4]), .DPRA5(ADDRB[5]), .DPO(pre_doutb_0[3]));
// Memory array element: COL: 0, ROW: 4
//  Initialization of Dual Port LUT ram now done through explicit
// parameter setting.
RAM64X1D#(.INIT(64'h0000000000000000))RAM64X1D_instance_2564(.WCLK(CLK), .WE(wea_0), 
  .A0(ADDRA[0]), .A1(ADDRA[1]), .A2(ADDRA[2]), .A3(ADDRA[3]), .A4(ADDRA[4]), .A5(ADDRA[5]), 
  .D(DINA[4]), .SPO(pre_douta_0[4]), .DPRA0(ADDRB[0]), .DPRA1(ADDRB[1]), .DPRA2(ADDRB[2]), 
  .DPRA3(ADDRB[3]), .DPRA4(ADDRB[4]), .DPRA5(ADDRB[5]), .DPO(pre_doutb_0[4]));
// Memory array element: COL: 0, ROW: 5
//  Initialization of Dual Port LUT ram now done through explicit
// parameter setting.
RAM64X1D#(.INIT(64'h0000000000000000))RAM64X1D_instance_2565(.WCLK(CLK), .WE(wea_0), 
  .A0(ADDRA[0]), .A1(ADDRA[1]), .A2(ADDRA[2]), .A3(ADDRA[3]), .A4(ADDRA[4]), .A5(ADDRA[5]), 
  .D(DINA[5]), .SPO(pre_douta_0[5]), .DPRA0(ADDRB[0]), .DPRA1(ADDRB[1]), .DPRA2(ADDRB[2]), 
  .DPRA3(ADDRB[3]), .DPRA4(ADDRB[4]), .DPRA5(ADDRB[5]), .DPO(pre_doutb_0[5]));
// Memory array element: COL: 0, ROW: 6
//  Initialization of Dual Port LUT ram now done through explicit
// parameter setting.
RAM64X1D#(.INIT(64'h0000000000000000))RAM64X1D_instance_2566(.WCLK(CLK), .WE(wea_0), 
  .A0(ADDRA[0]), .A1(ADDRA[1]), .A2(ADDRA[2]), .A3(ADDRA[3]), .A4(ADDRA[4]), .A5(ADDRA[5]), 
  .D(DINA[6]), .SPO(pre_douta_0[6]), .DPRA0(ADDRB[0]), .DPRA1(ADDRB[1]), .DPRA2(ADDRB[2]), 
  .DPRA3(ADDRB[3]), .DPRA4(ADDRB[4]), .DPRA5(ADDRB[5]), .DPO(pre_doutb_0[6]));
// Memory array element: COL: 0, ROW: 7
//  Initialization of Dual Port LUT ram now done through explicit
// parameter setting.
RAM64X1D#(.INIT(64'h0000000000000000))RAM64X1D_instance_2567(.WCLK(CLK), .WE(wea_0), 
  .A0(ADDRA[0]), .A1(ADDRA[1]), .A2(ADDRA[2]), .A3(ADDRA[3]), .A4(ADDRA[4]), .A5(ADDRA[5]), 
  .D(DINA[7]), .SPO(pre_douta_0[7]), .DPRA0(ADDRB[0]), .DPRA1(ADDRB[1]), .DPRA2(ADDRB[2]), 
  .DPRA3(ADDRB[3]), .DPRA4(ADDRB[4]), .DPRA5(ADDRB[5]), .DPO(pre_doutb_0[7]));
// Memory array element: COL: 0, ROW: 8
//  Initialization of Dual Port LUT ram now done through explicit
// parameter setting.
RAM64X1D#(.INIT(64'h0000000000000000))RAM64X1D_instance_2568(.WCLK(CLK), .WE(wea_0), 
  .A0(ADDRA[0]), .A1(ADDRA[1]), .A2(ADDRA[2]), .A3(ADDRA[3]), .A4(ADDRA[4]), .A5(ADDRA[5]), 
  .D(DINA[8]), .SPO(pre_douta_0[8]), .DPRA0(ADDRB[0]), .DPRA1(ADDRB[1]), .DPRA2(ADDRB[2]), 
  .DPRA3(ADDRB[3]), .DPRA4(ADDRB[4]), .DPRA5(ADDRB[5]), .DPO(pre_doutb_0[8]));
// Memory array element: COL: 0, ROW: 9
//  Initialization of Dual Port LUT ram now done through explicit
// parameter setting.
RAM64X1D#(.INIT(64'h0000000000000000))RAM64X1D_instance_2569(.WCLK(CLK), .WE(wea_0), 
  .A0(ADDRA[0]), .A1(ADDRA[1]), .A2(ADDRA[2]), .A3(ADDRA[3]), .A4(ADDRA[4]), .A5(ADDRA[5]), 
  .D(DINA[9]), .SPO(pre_douta_0[9]), .DPRA0(ADDRB[0]), .DPRA1(ADDRB[1]), .DPRA2(ADDRB[2]), 
  .DPRA3(ADDRB[3]), .DPRA4(ADDRB[4]), .DPRA5(ADDRB[5]), .DPO(pre_doutb_0[9]));
// Memory array element: COL: 0, ROW: 10
//  Initialization of Dual Port LUT ram now done through explicit
// parameter setting.
RAM64X1D#(.INIT(64'h0000000000000000))RAM64X1D_instance_2570(.WCLK(CLK), .WE(wea_0), 
  .A0(ADDRA[0]), .A1(ADDRA[1]), .A2(ADDRA[2]), .A3(ADDRA[3]), .A4(ADDRA[4]), .A5(ADDRA[5]), 
  .D(DINA[10]), .SPO(pre_douta_0[10]), .DPRA0(ADDRB[0]), .DPRA1(ADDRB[1]), .DPRA2(ADDRB[2]), 
  .DPRA3(ADDRB[3]), .DPRA4(ADDRB[4]), .DPRA5(ADDRB[5]), .DPO(pre_doutb_0[10]));
// Memory array element: COL: 0, ROW: 11
//  Initialization of Dual Port LUT ram now done through explicit
// parameter setting.
RAM64X1D#(.INIT(64'h0000000000000000))RAM64X1D_instance_2571(.WCLK(CLK), .WE(wea_0), 
  .A0(ADDRA[0]), .A1(ADDRA[1]), .A2(ADDRA[2]), .A3(ADDRA[3]), .A4(ADDRA[4]), .A5(ADDRA[5]), 
  .D(DINA[11]), .SPO(pre_douta_0[11]), .DPRA0(ADDRB[0]), .DPRA1(ADDRB[1]), .DPRA2(ADDRB[2]), 
  .DPRA3(ADDRB[3]), .DPRA4(ADDRB[4]), .DPRA5(ADDRB[5]), .DPO(pre_doutb_0[11]));
// Memory array element: COL: 0, ROW: 12
//  Initialization of Dual Port LUT ram now done through explicit
// parameter setting.
RAM64X1D#(.INIT(64'h0000000000000000))RAM64X1D_instance_2572(.WCLK(CLK), .WE(wea_0), 
  .A0(ADDRA[0]), .A1(ADDRA[1]), .A2(ADDRA[2]), .A3(ADDRA[3]), .A4(ADDRA[4]), .A5(ADDRA[5]), 
  .D(DINA[12]), .SPO(pre_douta_0[12]), .DPRA0(ADDRB[0]), .DPRA1(ADDRB[1]), .DPRA2(ADDRB[2]), 
  .DPRA3(ADDRB[3]), .DPRA4(ADDRB[4]), .DPRA5(ADDRB[5]), .DPO(pre_doutb_0[12]));
// Memory array element: COL: 0, ROW: 13
//  Initialization of Dual Port LUT ram now done through explicit
// parameter setting.
RAM64X1D#(.INIT(64'h0000000000000000))RAM64X1D_instance_2573(.WCLK(CLK), .WE(wea_0), 
  .A0(ADDRA[0]), .A1(ADDRA[1]), .A2(ADDRA[2]), .A3(ADDRA[3]), .A4(ADDRA[4]), .A5(ADDRA[5]), 
  .D(DINA[13]), .SPO(pre_douta_0[13]), .DPRA0(ADDRB[0]), .DPRA1(ADDRB[1]), .DPRA2(ADDRB[2]), 
  .DPRA3(ADDRB[3]), .DPRA4(ADDRB[4]), .DPRA5(ADDRB[5]), .DPO(pre_doutb_0[13]));
// Memory array element: COL: 0, ROW: 14
//  Initialization of Dual Port LUT ram now done through explicit
// parameter setting.
RAM64X1D#(.INIT(64'h0000000000000000))RAM64X1D_instance_2574(.WCLK(CLK), .WE(wea_0), 
  .A0(ADDRA[0]), .A1(ADDRA[1]), .A2(ADDRA[2]), .A3(ADDRA[3]), .A4(ADDRA[4]), .A5(ADDRA[5]), 
  .D(DINA[14]), .SPO(pre_douta_0[14]), .DPRA0(ADDRB[0]), .DPRA1(ADDRB[1]), .DPRA2(ADDRB[2]), 
  .DPRA3(ADDRB[3]), .DPRA4(ADDRB[4]), .DPRA5(ADDRB[5]), .DPO(pre_doutb_0[14]));
// Memory array element: COL: 0, ROW: 15
//  Initialization of Dual Port LUT ram now done through explicit
// parameter setting.
RAM64X1D#(.INIT(64'h0000000000000000))RAM64X1D_instance_2575(.WCLK(CLK), .WE(wea_0), 
  .A0(ADDRA[0]), .A1(ADDRA[1]), .A2(ADDRA[2]), .A3(ADDRA[3]), .A4(ADDRA[4]), .A5(ADDRA[5]), 
  .D(DINA[15]), .SPO(pre_douta_0[15]), .DPRA0(ADDRB[0]), .DPRA1(ADDRB[1]), .DPRA2(ADDRB[2]), 
  .DPRA3(ADDRB[3]), .DPRA4(ADDRB[4]), .DPRA5(ADDRB[5]), .DPO(pre_doutb_0[15]));
// Memory array element: COL: 0, ROW: 16
//  Initialization of Dual Port LUT ram now done through explicit
// parameter setting.
RAM64X1D#(.INIT(64'h0000000000000000))RAM64X1D_instance_2576(.WCLK(CLK), .WE(wea_0), 
  .A0(ADDRA[0]), .A1(ADDRA[1]), .A2(ADDRA[2]), .A3(ADDRA[3]), .A4(ADDRA[4]), .A5(ADDRA[5]), 
  .D(DINA[16]), .SPO(pre_douta_0[16]), .DPRA0(ADDRB[0]), .DPRA1(ADDRB[1]), .DPRA2(ADDRB[2]), 
  .DPRA3(ADDRB[3]), .DPRA4(ADDRB[4]), .DPRA5(ADDRB[5]), .DPO(pre_doutb_0[16]));
// Memory array element: COL: 0, ROW: 17
//  Initialization of Dual Port LUT ram now done through explicit
// parameter setting.
RAM64X1D#(.INIT(64'h0000000000000000))RAM64X1D_instance_2577(.WCLK(CLK), .WE(wea_0), 
  .A0(ADDRA[0]), .A1(ADDRA[1]), .A2(ADDRA[2]), .A3(ADDRA[3]), .A4(ADDRA[4]), .A5(ADDRA[5]), 
  .D(DINA[17]), .SPO(pre_douta_0[17]), .DPRA0(ADDRB[0]), .DPRA1(ADDRB[1]), .DPRA2(ADDRB[2]), 
  .DPRA3(ADDRB[3]), .DPRA4(ADDRB[4]), .DPRA5(ADDRB[5]), .DPO(pre_doutb_0[17]));
// Memory array element: COL: 0, ROW: 18
//  Initialization of Dual Port LUT ram now done through explicit
// parameter setting.
RAM64X1D#(.INIT(64'h0000000000000000))RAM64X1D_instance_2578(.WCLK(CLK), .WE(wea_0), 
  .A0(ADDRA[0]), .A1(ADDRA[1]), .A2(ADDRA[2]), .A3(ADDRA[3]), .A4(ADDRA[4]), .A5(ADDRA[5]), 
  .D(DINA[18]), .SPO(pre_douta_0[18]), .DPRA0(ADDRB[0]), .DPRA1(ADDRB[1]), .DPRA2(ADDRB[2]), 
  .DPRA3(ADDRB[3]), .DPRA4(ADDRB[4]), .DPRA5(ADDRB[5]), .DPO(pre_doutb_0[18]));
// Memory array element: COL: 0, ROW: 19
//  Initialization of Dual Port LUT ram now done through explicit
// parameter setting.
RAM64X1D#(.INIT(64'h0000000000000000))RAM64X1D_instance_2579(.WCLK(CLK), .WE(wea_0), 
  .A0(ADDRA[0]), .A1(ADDRA[1]), .A2(ADDRA[2]), .A3(ADDRA[3]), .A4(ADDRA[4]), .A5(ADDRA[5]), 
  .D(DINA[19]), .SPO(pre_douta_0[19]), .DPRA0(ADDRB[0]), .DPRA1(ADDRB[1]), .DPRA2(ADDRB[2]), 
  .DPRA3(ADDRB[3]), .DPRA4(ADDRB[4]), .DPRA5(ADDRB[5]), .DPO(pre_doutb_0[19]));
// Memory array element: COL: 0, ROW: 20
//  Initialization of Dual Port LUT ram now done through explicit
// parameter setting.
RAM64X1D#(.INIT(64'h0000000000000000))RAM64X1D_instance_2580(.WCLK(CLK), .WE(wea_0), 
  .A0(ADDRA[0]), .A1(ADDRA[1]), .A2(ADDRA[2]), .A3(ADDRA[3]), .A4(ADDRA[4]), .A5(ADDRA[5]), 
  .D(DINA[20]), .SPO(pre_douta_0[20]), .DPRA0(ADDRB[0]), .DPRA1(ADDRB[1]), .DPRA2(ADDRB[2]), 
  .DPRA3(ADDRB[3]), .DPRA4(ADDRB[4]), .DPRA5(ADDRB[5]), .DPO(pre_doutb_0[20]));
// Memory array element: COL: 0, ROW: 21
//  Initialization of Dual Port LUT ram now done through explicit
// parameter setting.
RAM64X1D#(.INIT(64'h0000000000000000))RAM64X1D_instance_2581(.WCLK(CLK), .WE(wea_0), 
  .A0(ADDRA[0]), .A1(ADDRA[1]), .A2(ADDRA[2]), .A3(ADDRA[3]), .A4(ADDRA[4]), .A5(ADDRA[5]), 
  .D(DINA[21]), .SPO(pre_douta_0[21]), .DPRA0(ADDRB[0]), .DPRA1(ADDRB[1]), .DPRA2(ADDRB[2]), 
  .DPRA3(ADDRB[3]), .DPRA4(ADDRB[4]), .DPRA5(ADDRB[5]), .DPO(pre_doutb_0[21]));
// Memory array element: COL: 0, ROW: 22
//  Initialization of Dual Port LUT ram now done through explicit
// parameter setting.
RAM64X1D#(.INIT(64'h0000000000000000))RAM64X1D_instance_2582(.WCLK(CLK), .WE(wea_0), 
  .A0(ADDRA[0]), .A1(ADDRA[1]), .A2(ADDRA[2]), .A3(ADDRA[3]), .A4(ADDRA[4]), .A5(ADDRA[5]), 
  .D(DINA[22]), .SPO(pre_douta_0[22]), .DPRA0(ADDRB[0]), .DPRA1(ADDRB[1]), .DPRA2(ADDRB[2]), 
  .DPRA3(ADDRB[3]), .DPRA4(ADDRB[4]), .DPRA5(ADDRB[5]), .DPO(pre_doutb_0[22]));
// Memory array element: COL: 0, ROW: 23
//  Initialization of Dual Port LUT ram now done through explicit
// parameter setting.
RAM64X1D#(.INIT(64'h0000000000000000))RAM64X1D_instance_2583(.WCLK(CLK), .WE(wea_0), 
  .A0(ADDRA[0]), .A1(ADDRA[1]), .A2(ADDRA[2]), .A3(ADDRA[3]), .A4(ADDRA[4]), .A5(ADDRA[5]), 
  .D(DINA[23]), .SPO(pre_douta_0[23]), .DPRA0(ADDRB[0]), .DPRA1(ADDRB[1]), .DPRA2(ADDRB[2]), 
  .DPRA3(ADDRB[3]), .DPRA4(ADDRB[4]), .DPRA5(ADDRB[5]), .DPO(pre_doutb_0[23]));
// Memory array element: COL: 0, ROW: 24
//  Initialization of Dual Port LUT ram now done through explicit
// parameter setting.
RAM64X1D#(.INIT(64'h0000000000000000))RAM64X1D_instance_2584(.WCLK(CLK), .WE(wea_0), 
  .A0(ADDRA[0]), .A1(ADDRA[1]), .A2(ADDRA[2]), .A3(ADDRA[3]), .A4(ADDRA[4]), .A5(ADDRA[5]), 
  .D(DINA[24]), .SPO(pre_douta_0[24]), .DPRA0(ADDRB[0]), .DPRA1(ADDRB[1]), .DPRA2(ADDRB[2]), 
  .DPRA3(ADDRB[3]), .DPRA4(ADDRB[4]), .DPRA5(ADDRB[5]), .DPO(pre_doutb_0[24]));
// Memory array element: COL: 0, ROW: 25
//  Initialization of Dual Port LUT ram now done through explicit
// parameter setting.
RAM64X1D#(.INIT(64'h0000000000000000))RAM64X1D_instance_2585(.WCLK(CLK), .WE(wea_0), 
  .A0(ADDRA[0]), .A1(ADDRA[1]), .A2(ADDRA[2]), .A3(ADDRA[3]), .A4(ADDRA[4]), .A5(ADDRA[5]), 
  .D(DINA[25]), .SPO(pre_douta_0[25]), .DPRA0(ADDRB[0]), .DPRA1(ADDRB[1]), .DPRA2(ADDRB[2]), 
  .DPRA3(ADDRB[3]), .DPRA4(ADDRB[4]), .DPRA5(ADDRB[5]), .DPO(pre_doutb_0[25]));
// Memory array element: COL: 0, ROW: 26
//  Initialization of Dual Port LUT ram now done through explicit
// parameter setting.
RAM64X1D#(.INIT(64'h0000000000000000))RAM64X1D_instance_2586(.WCLK(CLK), .WE(wea_0), 
  .A0(ADDRA[0]), .A1(ADDRA[1]), .A2(ADDRA[2]), .A3(ADDRA[3]), .A4(ADDRA[4]), .A5(ADDRA[5]), 
  .D(DINA[26]), .SPO(pre_douta_0[26]), .DPRA0(ADDRB[0]), .DPRA1(ADDRB[1]), .DPRA2(ADDRB[2]), 
  .DPRA3(ADDRB[3]), .DPRA4(ADDRB[4]), .DPRA5(ADDRB[5]), .DPO(pre_doutb_0[26]));
// Memory array element: COL: 0, ROW: 27
//  Initialization of Dual Port LUT ram now done through explicit
// parameter setting.
RAM64X1D#(.INIT(64'h0000000000000000))RAM64X1D_instance_2587(.WCLK(CLK), .WE(wea_0), 
  .A0(ADDRA[0]), .A1(ADDRA[1]), .A2(ADDRA[2]), .A3(ADDRA[3]), .A4(ADDRA[4]), .A5(ADDRA[5]), 
  .D(DINA[27]), .SPO(pre_douta_0[27]), .DPRA0(ADDRB[0]), .DPRA1(ADDRB[1]), .DPRA2(ADDRB[2]), 
  .DPRA3(ADDRB[3]), .DPRA4(ADDRB[4]), .DPRA5(ADDRB[5]), .DPO(pre_doutb_0[27]));
// Memory array element: COL: 0, ROW: 28
//  Initialization of Dual Port LUT ram now done through explicit
// parameter setting.
RAM64X1D#(.INIT(64'h0000000000000000))RAM64X1D_instance_2588(.WCLK(CLK), .WE(wea_0), 
  .A0(ADDRA[0]), .A1(ADDRA[1]), .A2(ADDRA[2]), .A3(ADDRA[3]), .A4(ADDRA[4]), .A5(ADDRA[5]), 
  .D(DINA[28]), .SPO(pre_douta_0[28]), .DPRA0(ADDRB[0]), .DPRA1(ADDRB[1]), .DPRA2(ADDRB[2]), 
  .DPRA3(ADDRB[3]), .DPRA4(ADDRB[4]), .DPRA5(ADDRB[5]), .DPO(pre_doutb_0[28]));
// Memory array element: COL: 0, ROW: 29
//  Initialization of Dual Port LUT ram now done through explicit
// parameter setting.
RAM64X1D#(.INIT(64'h0000000000000000))RAM64X1D_instance_2589(.WCLK(CLK), .WE(wea_0), 
  .A0(ADDRA[0]), .A1(ADDRA[1]), .A2(ADDRA[2]), .A3(ADDRA[3]), .A4(ADDRA[4]), .A5(ADDRA[5]), 
  .D(DINA[29]), .SPO(pre_douta_0[29]), .DPRA0(ADDRB[0]), .DPRA1(ADDRB[1]), .DPRA2(ADDRB[2]), 
  .DPRA3(ADDRB[3]), .DPRA4(ADDRB[4]), .DPRA5(ADDRB[5]), .DPO(pre_doutb_0[29]));
// Memory array element: COL: 0, ROW: 30
//  Initialization of Dual Port LUT ram now done through explicit
// parameter setting.
RAM64X1D#(.INIT(64'h0000000000000000))RAM64X1D_instance_2590(.WCLK(CLK), .WE(wea_0), 
  .A0(ADDRA[0]), .A1(ADDRA[1]), .A2(ADDRA[2]), .A3(ADDRA[3]), .A4(ADDRA[4]), .A5(ADDRA[5]), 
  .D(DINA[30]), .SPO(pre_douta_0[30]), .DPRA0(ADDRB[0]), .DPRA1(ADDRB[1]), .DPRA2(ADDRB[2]), 
  .DPRA3(ADDRB[3]), .DPRA4(ADDRB[4]), .DPRA5(ADDRB[5]), .DPO(pre_doutb_0[30]));
// Memory array element: COL: 0, ROW: 31
//  Initialization of Dual Port LUT ram now done through explicit
// parameter setting.
RAM64X1D#(.INIT(64'h0000000000000000))RAM64X1D_instance_2591(.WCLK(CLK), .WE(wea_0), 
  .A0(ADDRA[0]), .A1(ADDRA[1]), .A2(ADDRA[2]), .A3(ADDRA[3]), .A4(ADDRA[4]), .A5(ADDRA[5]), 
  .D(DINA[31]), .SPO(pre_douta_0[31]), .DPRA0(ADDRB[0]), .DPRA1(ADDRB[1]), .DPRA2(ADDRB[2]), 
  .DPRA3(ADDRB[3]), .DPRA4(ADDRB[4]), .DPRA5(ADDRB[5]), .DPO(pre_doutb_0[31]));
// Memory array element: COL: 1, ROW: 0
//  Initialization of Dual Port LUT ram now done through explicit
// parameter setting.
RAM64X1D#(.INIT(64'h0000000000000000))RAM64X1D_instance_2592(.WCLK(CLK), .WE(wea_1), 
  .A0(ADDRA[0]), .A1(ADDRA[1]), .A2(ADDRA[2]), .A3(ADDRA[3]), .A4(ADDRA[4]), .A5(ADDRA[5]), 
  .D(DINA[0]), .SPO(pre_douta_1[0]), .DPRA0(ADDRB[0]), .DPRA1(ADDRB[1]), .DPRA2(ADDRB[2]), 
  .DPRA3(ADDRB[3]), .DPRA4(ADDRB[4]), .DPRA5(ADDRB[5]), .DPO(pre_doutb_1[0]));
// Memory array element: COL: 1, ROW: 1
//  Initialization of Dual Port LUT ram now done through explicit
// parameter setting.
RAM64X1D#(.INIT(64'h0000000000000000))RAM64X1D_instance_2593(.WCLK(CLK), .WE(wea_1), 
  .A0(ADDRA[0]), .A1(ADDRA[1]), .A2(ADDRA[2]), .A3(ADDRA[3]), .A4(ADDRA[4]), .A5(ADDRA[5]), 
  .D(DINA[1]), .SPO(pre_douta_1[1]), .DPRA0(ADDRB[0]), .DPRA1(ADDRB[1]), .DPRA2(ADDRB[2]), 
  .DPRA3(ADDRB[3]), .DPRA4(ADDRB[4]), .DPRA5(ADDRB[5]), .DPO(pre_doutb_1[1]));
// Memory array element: COL: 1, ROW: 2
//  Initialization of Dual Port LUT ram now done through explicit
// parameter setting.
RAM64X1D#(.INIT(64'h0000000000000000))RAM64X1D_instance_2594(.WCLK(CLK), .WE(wea_1), 
  .A0(ADDRA[0]), .A1(ADDRA[1]), .A2(ADDRA[2]), .A3(ADDRA[3]), .A4(ADDRA[4]), .A5(ADDRA[5]), 
  .D(DINA[2]), .SPO(pre_douta_1[2]), .DPRA0(ADDRB[0]), .DPRA1(ADDRB[1]), .DPRA2(ADDRB[2]), 
  .DPRA3(ADDRB[3]), .DPRA4(ADDRB[4]), .DPRA5(ADDRB[5]), .DPO(pre_doutb_1[2]));
// Memory array element: COL: 1, ROW: 3
//  Initialization of Dual Port LUT ram now done through explicit
// parameter setting.
RAM64X1D#(.INIT(64'h0000000000000000))RAM64X1D_instance_2595(.WCLK(CLK), .WE(wea_1), 
  .A0(ADDRA[0]), .A1(ADDRA[1]), .A2(ADDRA[2]), .A3(ADDRA[3]), .A4(ADDRA[4]), .A5(ADDRA[5]), 
  .D(DINA[3]), .SPO(pre_douta_1[3]), .DPRA0(ADDRB[0]), .DPRA1(ADDRB[1]), .DPRA2(ADDRB[2]), 
  .DPRA3(ADDRB[3]), .DPRA4(ADDRB[4]), .DPRA5(ADDRB[5]), .DPO(pre_doutb_1[3]));
// Memory array element: COL: 1, ROW: 4
//  Initialization of Dual Port LUT ram now done through explicit
// parameter setting.
RAM64X1D#(.INIT(64'h0000000000000000))RAM64X1D_instance_2596(.WCLK(CLK), .WE(wea_1), 
  .A0(ADDRA[0]), .A1(ADDRA[1]), .A2(ADDRA[2]), .A3(ADDRA[3]), .A4(ADDRA[4]), .A5(ADDRA[5]), 
  .D(DINA[4]), .SPO(pre_douta_1[4]), .DPRA0(ADDRB[0]), .DPRA1(ADDRB[1]), .DPRA2(ADDRB[2]), 
  .DPRA3(ADDRB[3]), .DPRA4(ADDRB[4]), .DPRA5(ADDRB[5]), .DPO(pre_doutb_1[4]));
// Memory array element: COL: 1, ROW: 5
//  Initialization of Dual Port LUT ram now done through explicit
// parameter setting.
RAM64X1D#(.INIT(64'h0000000000000000))RAM64X1D_instance_2597(.WCLK(CLK), .WE(wea_1), 
  .A0(ADDRA[0]), .A1(ADDRA[1]), .A2(ADDRA[2]), .A3(ADDRA[3]), .A4(ADDRA[4]), .A5(ADDRA[5]), 
  .D(DINA[5]), .SPO(pre_douta_1[5]), .DPRA0(ADDRB[0]), .DPRA1(ADDRB[1]), .DPRA2(ADDRB[2]), 
  .DPRA3(ADDRB[3]), .DPRA4(ADDRB[4]), .DPRA5(ADDRB[5]), .DPO(pre_doutb_1[5]));
// Memory array element: COL: 1, ROW: 6
//  Initialization of Dual Port LUT ram now done through explicit
// parameter setting.
RAM64X1D#(.INIT(64'h0000000000000000))RAM64X1D_instance_2598(.WCLK(CLK), .WE(wea_1), 
  .A0(ADDRA[0]), .A1(ADDRA[1]), .A2(ADDRA[2]), .A3(ADDRA[3]), .A4(ADDRA[4]), .A5(ADDRA[5]), 
  .D(DINA[6]), .SPO(pre_douta_1[6]), .DPRA0(ADDRB[0]), .DPRA1(ADDRB[1]), .DPRA2(ADDRB[2]), 
  .DPRA3(ADDRB[3]), .DPRA4(ADDRB[4]), .DPRA5(ADDRB[5]), .DPO(pre_doutb_1[6]));
// Memory array element: COL: 1, ROW: 7
//  Initialization of Dual Port LUT ram now done through explicit
// parameter setting.
RAM64X1D#(.INIT(64'h0000000000000000))RAM64X1D_instance_2599(.WCLK(CLK), .WE(wea_1), 
  .A0(ADDRA[0]), .A1(ADDRA[1]), .A2(ADDRA[2]), .A3(ADDRA[3]), .A4(ADDRA[4]), .A5(ADDRA[5]), 
  .D(DINA[7]), .SPO(pre_douta_1[7]), .DPRA0(ADDRB[0]), .DPRA1(ADDRB[1]), .DPRA2(ADDRB[2]), 
  .DPRA3(ADDRB[3]), .DPRA4(ADDRB[4]), .DPRA5(ADDRB[5]), .DPO(pre_doutb_1[7]));
// Memory array element: COL: 1, ROW: 8
//  Initialization of Dual Port LUT ram now done through explicit
// parameter setting.
RAM64X1D#(.INIT(64'h0000000000000000))RAM64X1D_instance_2600(.WCLK(CLK), .WE(wea_1), 
  .A0(ADDRA[0]), .A1(ADDRA[1]), .A2(ADDRA[2]), .A3(ADDRA[3]), .A4(ADDRA[4]), .A5(ADDRA[5]), 
  .D(DINA[8]), .SPO(pre_douta_1[8]), .DPRA0(ADDRB[0]), .DPRA1(ADDRB[1]), .DPRA2(ADDRB[2]), 
  .DPRA3(ADDRB[3]), .DPRA4(ADDRB[4]), .DPRA5(ADDRB[5]), .DPO(pre_doutb_1[8]));
// Memory array element: COL: 1, ROW: 9
//  Initialization of Dual Port LUT ram now done through explicit
// parameter setting.
RAM64X1D#(.INIT(64'h0000000000000000))RAM64X1D_instance_2601(.WCLK(CLK), .WE(wea_1), 
  .A0(ADDRA[0]), .A1(ADDRA[1]), .A2(ADDRA[2]), .A3(ADDRA[3]), .A4(ADDRA[4]), .A5(ADDRA[5]), 
  .D(DINA[9]), .SPO(pre_douta_1[9]), .DPRA0(ADDRB[0]), .DPRA1(ADDRB[1]), .DPRA2(ADDRB[2]), 
  .DPRA3(ADDRB[3]), .DPRA4(ADDRB[4]), .DPRA5(ADDRB[5]), .DPO(pre_doutb_1[9]));
// Memory array element: COL: 1, ROW: 10
//  Initialization of Dual Port LUT ram now done through explicit
// parameter setting.
RAM64X1D#(.INIT(64'h0000000000000000))RAM64X1D_instance_2602(.WCLK(CLK), .WE(wea_1), 
  .A0(ADDRA[0]), .A1(ADDRA[1]), .A2(ADDRA[2]), .A3(ADDRA[3]), .A4(ADDRA[4]), .A5(ADDRA[5]), 
  .D(DINA[10]), .SPO(pre_douta_1[10]), .DPRA0(ADDRB[0]), .DPRA1(ADDRB[1]), .DPRA2(ADDRB[2]), 
  .DPRA3(ADDRB[3]), .DPRA4(ADDRB[4]), .DPRA5(ADDRB[5]), .DPO(pre_doutb_1[10]));
// Memory array element: COL: 1, ROW: 11
//  Initialization of Dual Port LUT ram now done through explicit
// parameter setting.
RAM64X1D#(.INIT(64'h0000000000000000))RAM64X1D_instance_2603(.WCLK(CLK), .WE(wea_1), 
  .A0(ADDRA[0]), .A1(ADDRA[1]), .A2(ADDRA[2]), .A3(ADDRA[3]), .A4(ADDRA[4]), .A5(ADDRA[5]), 
  .D(DINA[11]), .SPO(pre_douta_1[11]), .DPRA0(ADDRB[0]), .DPRA1(ADDRB[1]), .DPRA2(ADDRB[2]), 
  .DPRA3(ADDRB[3]), .DPRA4(ADDRB[4]), .DPRA5(ADDRB[5]), .DPO(pre_doutb_1[11]));
// Memory array element: COL: 1, ROW: 12
//  Initialization of Dual Port LUT ram now done through explicit
// parameter setting.
RAM64X1D#(.INIT(64'h0000000000000000))RAM64X1D_instance_2604(.WCLK(CLK), .WE(wea_1), 
  .A0(ADDRA[0]), .A1(ADDRA[1]), .A2(ADDRA[2]), .A3(ADDRA[3]), .A4(ADDRA[4]), .A5(ADDRA[5]), 
  .D(DINA[12]), .SPO(pre_douta_1[12]), .DPRA0(ADDRB[0]), .DPRA1(ADDRB[1]), .DPRA2(ADDRB[2]), 
  .DPRA3(ADDRB[3]), .DPRA4(ADDRB[4]), .DPRA5(ADDRB[5]), .DPO(pre_doutb_1[12]));
// Memory array element: COL: 1, ROW: 13
//  Initialization of Dual Port LUT ram now done through explicit
// parameter setting.
RAM64X1D#(.INIT(64'h0000000000000000))RAM64X1D_instance_2605(.WCLK(CLK), .WE(wea_1), 
  .A0(ADDRA[0]), .A1(ADDRA[1]), .A2(ADDRA[2]), .A3(ADDRA[3]), .A4(ADDRA[4]), .A5(ADDRA[5]), 
  .D(DINA[13]), .SPO(pre_douta_1[13]), .DPRA0(ADDRB[0]), .DPRA1(ADDRB[1]), .DPRA2(ADDRB[2]), 
  .DPRA3(ADDRB[3]), .DPRA4(ADDRB[4]), .DPRA5(ADDRB[5]), .DPO(pre_doutb_1[13]));
// Memory array element: COL: 1, ROW: 14
//  Initialization of Dual Port LUT ram now done through explicit
// parameter setting.
RAM64X1D#(.INIT(64'h0000000000000000))RAM64X1D_instance_2606(.WCLK(CLK), .WE(wea_1), 
  .A0(ADDRA[0]), .A1(ADDRA[1]), .A2(ADDRA[2]), .A3(ADDRA[3]), .A4(ADDRA[4]), .A5(ADDRA[5]), 
  .D(DINA[14]), .SPO(pre_douta_1[14]), .DPRA0(ADDRB[0]), .DPRA1(ADDRB[1]), .DPRA2(ADDRB[2]), 
  .DPRA3(ADDRB[3]), .DPRA4(ADDRB[4]), .DPRA5(ADDRB[5]), .DPO(pre_doutb_1[14]));
// Memory array element: COL: 1, ROW: 15
//  Initialization of Dual Port LUT ram now done through explicit
// parameter setting.
RAM64X1D#(.INIT(64'h0000000000000000))RAM64X1D_instance_2607(.WCLK(CLK), .WE(wea_1), 
  .A0(ADDRA[0]), .A1(ADDRA[1]), .A2(ADDRA[2]), .A3(ADDRA[3]), .A4(ADDRA[4]), .A5(ADDRA[5]), 
  .D(DINA[15]), .SPO(pre_douta_1[15]), .DPRA0(ADDRB[0]), .DPRA1(ADDRB[1]), .DPRA2(ADDRB[2]), 
  .DPRA3(ADDRB[3]), .DPRA4(ADDRB[4]), .DPRA5(ADDRB[5]), .DPO(pre_doutb_1[15]));
// Memory array element: COL: 1, ROW: 16
//  Initialization of Dual Port LUT ram now done through explicit
// parameter setting.
RAM64X1D#(.INIT(64'h0000000000000000))RAM64X1D_instance_2608(.WCLK(CLK), .WE(wea_1), 
  .A0(ADDRA[0]), .A1(ADDRA[1]), .A2(ADDRA[2]), .A3(ADDRA[3]), .A4(ADDRA[4]), .A5(ADDRA[5]), 
  .D(DINA[16]), .SPO(pre_douta_1[16]), .DPRA0(ADDRB[0]), .DPRA1(ADDRB[1]), .DPRA2(ADDRB[2]), 
  .DPRA3(ADDRB[3]), .DPRA4(ADDRB[4]), .DPRA5(ADDRB[5]), .DPO(pre_doutb_1[16]));
// Memory array element: COL: 1, ROW: 17
//  Initialization of Dual Port LUT ram now done through explicit
// parameter setting.
RAM64X1D#(.INIT(64'h0000000000000000))RAM64X1D_instance_2609(.WCLK(CLK), .WE(wea_1), 
  .A0(ADDRA[0]), .A1(ADDRA[1]), .A2(ADDRA[2]), .A3(ADDRA[3]), .A4(ADDRA[4]), .A5(ADDRA[5]), 
  .D(DINA[17]), .SPO(pre_douta_1[17]), .DPRA0(ADDRB[0]), .DPRA1(ADDRB[1]), .DPRA2(ADDRB[2]), 
  .DPRA3(ADDRB[3]), .DPRA4(ADDRB[4]), .DPRA5(ADDRB[5]), .DPO(pre_doutb_1[17]));
// Memory array element: COL: 1, ROW: 18
//  Initialization of Dual Port LUT ram now done through explicit
// parameter setting.
RAM64X1D#(.INIT(64'h0000000000000000))RAM64X1D_instance_2610(.WCLK(CLK), .WE(wea_1), 
  .A0(ADDRA[0]), .A1(ADDRA[1]), .A2(ADDRA[2]), .A3(ADDRA[3]), .A4(ADDRA[4]), .A5(ADDRA[5]), 
  .D(DINA[18]), .SPO(pre_douta_1[18]), .DPRA0(ADDRB[0]), .DPRA1(ADDRB[1]), .DPRA2(ADDRB[2]), 
  .DPRA3(ADDRB[3]), .DPRA4(ADDRB[4]), .DPRA5(ADDRB[5]), .DPO(pre_doutb_1[18]));
// Memory array element: COL: 1, ROW: 19
//  Initialization of Dual Port LUT ram now done through explicit
// parameter setting.
RAM64X1D#(.INIT(64'h0000000000000000))RAM64X1D_instance_2611(.WCLK(CLK), .WE(wea_1), 
  .A0(ADDRA[0]), .A1(ADDRA[1]), .A2(ADDRA[2]), .A3(ADDRA[3]), .A4(ADDRA[4]), .A5(ADDRA[5]), 
  .D(DINA[19]), .SPO(pre_douta_1[19]), .DPRA0(ADDRB[0]), .DPRA1(ADDRB[1]), .DPRA2(ADDRB[2]), 
  .DPRA3(ADDRB[3]), .DPRA4(ADDRB[4]), .DPRA5(ADDRB[5]), .DPO(pre_doutb_1[19]));
// Memory array element: COL: 1, ROW: 20
//  Initialization of Dual Port LUT ram now done through explicit
// parameter setting.
RAM64X1D#(.INIT(64'h0000000000000000))RAM64X1D_instance_2612(.WCLK(CLK), .WE(wea_1), 
  .A0(ADDRA[0]), .A1(ADDRA[1]), .A2(ADDRA[2]), .A3(ADDRA[3]), .A4(ADDRA[4]), .A5(ADDRA[5]), 
  .D(DINA[20]), .SPO(pre_douta_1[20]), .DPRA0(ADDRB[0]), .DPRA1(ADDRB[1]), .DPRA2(ADDRB[2]), 
  .DPRA3(ADDRB[3]), .DPRA4(ADDRB[4]), .DPRA5(ADDRB[5]), .DPO(pre_doutb_1[20]));
// Memory array element: COL: 1, ROW: 21
//  Initialization of Dual Port LUT ram now done through explicit
// parameter setting.
RAM64X1D#(.INIT(64'h0000000000000000))RAM64X1D_instance_2613(.WCLK(CLK), .WE(wea_1), 
  .A0(ADDRA[0]), .A1(ADDRA[1]), .A2(ADDRA[2]), .A3(ADDRA[3]), .A4(ADDRA[4]), .A5(ADDRA[5]), 
  .D(DINA[21]), .SPO(pre_douta_1[21]), .DPRA0(ADDRB[0]), .DPRA1(ADDRB[1]), .DPRA2(ADDRB[2]), 
  .DPRA3(ADDRB[3]), .DPRA4(ADDRB[4]), .DPRA5(ADDRB[5]), .DPO(pre_doutb_1[21]));
// Memory array element: COL: 1, ROW: 22
//  Initialization of Dual Port LUT ram now done through explicit
// parameter setting.
RAM64X1D#(.INIT(64'h0000000000000000))RAM64X1D_instance_2614(.WCLK(CLK), .WE(wea_1), 
  .A0(ADDRA[0]), .A1(ADDRA[1]), .A2(ADDRA[2]), .A3(ADDRA[3]), .A4(ADDRA[4]), .A5(ADDRA[5]), 
  .D(DINA[22]), .SPO(pre_douta_1[22]), .DPRA0(ADDRB[0]), .DPRA1(ADDRB[1]), .DPRA2(ADDRB[2]), 
  .DPRA3(ADDRB[3]), .DPRA4(ADDRB[4]), .DPRA5(ADDRB[5]), .DPO(pre_doutb_1[22]));
// Memory array element: COL: 1, ROW: 23
//  Initialization of Dual Port LUT ram now done through explicit
// parameter setting.
RAM64X1D#(.INIT(64'h0000000000000000))RAM64X1D_instance_2615(.WCLK(CLK), .WE(wea_1), 
  .A0(ADDRA[0]), .A1(ADDRA[1]), .A2(ADDRA[2]), .A3(ADDRA[3]), .A4(ADDRA[4]), .A5(ADDRA[5]), 
  .D(DINA[23]), .SPO(pre_douta_1[23]), .DPRA0(ADDRB[0]), .DPRA1(ADDRB[1]), .DPRA2(ADDRB[2]), 
  .DPRA3(ADDRB[3]), .DPRA4(ADDRB[4]), .DPRA5(ADDRB[5]), .DPO(pre_doutb_1[23]));
// Memory array element: COL: 1, ROW: 24
//  Initialization of Dual Port LUT ram now done through explicit
// parameter setting.
RAM64X1D#(.INIT(64'h0000000000000000))RAM64X1D_instance_2616(.WCLK(CLK), .WE(wea_1), 
  .A0(ADDRA[0]), .A1(ADDRA[1]), .A2(ADDRA[2]), .A3(ADDRA[3]), .A4(ADDRA[4]), .A5(ADDRA[5]), 
  .D(DINA[24]), .SPO(pre_douta_1[24]), .DPRA0(ADDRB[0]), .DPRA1(ADDRB[1]), .DPRA2(ADDRB[2]), 
  .DPRA3(ADDRB[3]), .DPRA4(ADDRB[4]), .DPRA5(ADDRB[5]), .DPO(pre_doutb_1[24]));
// Memory array element: COL: 1, ROW: 25
//  Initialization of Dual Port LUT ram now done through explicit
// parameter setting.
RAM64X1D#(.INIT(64'h0000000000000000))RAM64X1D_instance_2617(.WCLK(CLK), .WE(wea_1), 
  .A0(ADDRA[0]), .A1(ADDRA[1]), .A2(ADDRA[2]), .A3(ADDRA[3]), .A4(ADDRA[4]), .A5(ADDRA[5]), 
  .D(DINA[25]), .SPO(pre_douta_1[25]), .DPRA0(ADDRB[0]), .DPRA1(ADDRB[1]), .DPRA2(ADDRB[2]), 
  .DPRA3(ADDRB[3]), .DPRA4(ADDRB[4]), .DPRA5(ADDRB[5]), .DPO(pre_doutb_1[25]));
// Memory array element: COL: 1, ROW: 26
//  Initialization of Dual Port LUT ram now done through explicit
// parameter setting.
RAM64X1D#(.INIT(64'h0000000000000000))RAM64X1D_instance_2618(.WCLK(CLK), .WE(wea_1), 
  .A0(ADDRA[0]), .A1(ADDRA[1]), .A2(ADDRA[2]), .A3(ADDRA[3]), .A4(ADDRA[4]), .A5(ADDRA[5]), 
  .D(DINA[26]), .SPO(pre_douta_1[26]), .DPRA0(ADDRB[0]), .DPRA1(ADDRB[1]), .DPRA2(ADDRB[2]), 
  .DPRA3(ADDRB[3]), .DPRA4(ADDRB[4]), .DPRA5(ADDRB[5]), .DPO(pre_doutb_1[26]));
// Memory array element: COL: 1, ROW: 27
//  Initialization of Dual Port LUT ram now done through explicit
// parameter setting.
RAM64X1D#(.INIT(64'h0000000000000000))RAM64X1D_instance_2619(.WCLK(CLK), .WE(wea_1), 
  .A0(ADDRA[0]), .A1(ADDRA[1]), .A2(ADDRA[2]), .A3(ADDRA[3]), .A4(ADDRA[4]), .A5(ADDRA[5]), 
  .D(DINA[27]), .SPO(pre_douta_1[27]), .DPRA0(ADDRB[0]), .DPRA1(ADDRB[1]), .DPRA2(ADDRB[2]), 
  .DPRA3(ADDRB[3]), .DPRA4(ADDRB[4]), .DPRA5(ADDRB[5]), .DPO(pre_doutb_1[27]));
// Memory array element: COL: 1, ROW: 28
//  Initialization of Dual Port LUT ram now done through explicit
// parameter setting.
RAM64X1D#(.INIT(64'h0000000000000000))RAM64X1D_instance_2620(.WCLK(CLK), .WE(wea_1), 
  .A0(ADDRA[0]), .A1(ADDRA[1]), .A2(ADDRA[2]), .A3(ADDRA[3]), .A4(ADDRA[4]), .A5(ADDRA[5]), 
  .D(DINA[28]), .SPO(pre_douta_1[28]), .DPRA0(ADDRB[0]), .DPRA1(ADDRB[1]), .DPRA2(ADDRB[2]), 
  .DPRA3(ADDRB[3]), .DPRA4(ADDRB[4]), .DPRA5(ADDRB[5]), .DPO(pre_doutb_1[28]));
// Memory array element: COL: 1, ROW: 29
//  Initialization of Dual Port LUT ram now done through explicit
// parameter setting.
RAM64X1D#(.INIT(64'h0000000000000000))RAM64X1D_instance_2621(.WCLK(CLK), .WE(wea_1), 
  .A0(ADDRA[0]), .A1(ADDRA[1]), .A2(ADDRA[2]), .A3(ADDRA[3]), .A4(ADDRA[4]), .A5(ADDRA[5]), 
  .D(DINA[29]), .SPO(pre_douta_1[29]), .DPRA0(ADDRB[0]), .DPRA1(ADDRB[1]), .DPRA2(ADDRB[2]), 
  .DPRA3(ADDRB[3]), .DPRA4(ADDRB[4]), .DPRA5(ADDRB[5]), .DPO(pre_doutb_1[29]));
// Memory array element: COL: 1, ROW: 30
//  Initialization of Dual Port LUT ram now done through explicit
// parameter setting.
RAM64X1D#(.INIT(64'h0000000000000000))RAM64X1D_instance_2622(.WCLK(CLK), .WE(wea_1), 
  .A0(ADDRA[0]), .A1(ADDRA[1]), .A2(ADDRA[2]), .A3(ADDRA[3]), .A4(ADDRA[4]), .A5(ADDRA[5]), 
  .D(DINA[30]), .SPO(pre_douta_1[30]), .DPRA0(ADDRB[0]), .DPRA1(ADDRB[1]), .DPRA2(ADDRB[2]), 
  .DPRA3(ADDRB[3]), .DPRA4(ADDRB[4]), .DPRA5(ADDRB[5]), .DPO(pre_doutb_1[30]));
// Memory array element: COL: 1, ROW: 31
//  Initialization of Dual Port LUT ram now done through explicit
// parameter setting.
RAM64X1D#(.INIT(64'h0000000000000000))RAM64X1D_instance_2623(.WCLK(CLK), .WE(wea_1), 
  .A0(ADDRA[0]), .A1(ADDRA[1]), .A2(ADDRA[2]), .A3(ADDRA[3]), .A4(ADDRA[4]), .A5(ADDRA[5]), 
  .D(DINA[31]), .SPO(pre_douta_1[31]), .DPRA0(ADDRB[0]), .DPRA1(ADDRB[1]), .DPRA2(ADDRB[2]), 
  .DPRA3(ADDRB[3]), .DPRA4(ADDRB[4]), .DPRA5(ADDRB[5]), .DPO(pre_doutb_1[31]));
endmodule



module medianRow2_structuralmemory_048501cb_(CLK_u86, bus_5e0e7bd9_, bus_75fa6998_, bus_1401863a_, bus_64a9a75e_, bus_79590001_, bus_1617e17a_, bus_38daf567_, bus_1c943fe2_, bus_1c0d7037_, bus_0e389e39_, bus_1927a308_, bus_46bb48e9_, bus_3bc9d5ca_);
input		CLK_u86;
input		bus_5e0e7bd9_;
input	[31:0]	bus_75fa6998_;
input	[2:0]	bus_1401863a_;
input		bus_64a9a75e_;
input	[31:0]	bus_79590001_;
input	[2:0]	bus_1617e17a_;
input		bus_38daf567_;
input		bus_1c943fe2_;
input	[31:0]	bus_1c0d7037_;
output	[31:0]	bus_0e389e39_;
output		bus_1927a308_;
output	[31:0]	bus_46bb48e9_;
output		bus_3bc9d5ca_;
wire		or_144bfa6f_u0;
reg		logicalMem_7bf4a0db_we_delay0_u0=1'h0;
wire		or_4a7de51a_u0;
wire	[31:0]	bus_78dbb50d_;
wire	[31:0]	bus_7dd40efe_;
wire		and_65d76bb8_u0;
wire		not_03d4b0e4_u0;
assign or_144bfa6f_u0=and_65d76bb8_u0|logicalMem_7bf4a0db_we_delay0_u0;
always @(posedge CLK_u86 or posedge bus_5e0e7bd9_)
begin
if (bus_5e0e7bd9_)
logicalMem_7bf4a0db_we_delay0_u0<=1'h0;
else logicalMem_7bf4a0db_we_delay0_u0<=bus_1c943fe2_;
end
assign or_4a7de51a_u0=bus_38daf567_|bus_1c943fe2_;
medianRow2_forge_memory_101x32_133 medianRow2_forge_memory_101x32_133_instance0(.CLK(CLK_u86), 
  .ENA(or_4a7de51a_u0), .WEA(bus_1c943fe2_), .DINA(bus_1c0d7037_), .ADDRA(bus_79590001_), 
  .DOUTA(bus_7dd40efe_), .DONEA(), .ENB(bus_64a9a75e_), .ADDRB(bus_75fa6998_), .DOUTB(bus_78dbb50d_), 
  .DONEB());
assign and_65d76bb8_u0=bus_38daf567_&not_03d4b0e4_u0;
assign not_03d4b0e4_u0=~bus_1c943fe2_;
assign bus_0e389e39_=bus_78dbb50d_;
assign bus_1927a308_=bus_64a9a75e_;
assign bus_46bb48e9_=bus_7dd40efe_;
assign bus_3bc9d5ca_=or_144bfa6f_u0;
endmodule



module medianRow2_simplememoryreferee_75d84f17_(bus_3150bbf2_, bus_0affd4ba_, bus_04338566_, bus_414489ec_, bus_22a90075_, bus_7873a4e9_, bus_4733e576_, bus_5816e6cd_, bus_5a23ef00_, bus_12a0e4a3_, bus_4f9cd513_, bus_7f42d1dc_, bus_68aceb65_, bus_1fb0e3fe_, bus_2b6556c2_, bus_563c0487_, bus_5235cae6_, bus_770e037d_, bus_6ac7c410_, bus_0e104710_, bus_172ff744_);
input		bus_3150bbf2_;
input		bus_0affd4ba_;
input		bus_04338566_;
input	[31:0]	bus_414489ec_;
input		bus_22a90075_;
input	[31:0]	bus_7873a4e9_;
input	[31:0]	bus_4733e576_;
input	[2:0]	bus_5816e6cd_;
input		bus_5a23ef00_;
input		bus_12a0e4a3_;
input	[31:0]	bus_4f9cd513_;
input	[31:0]	bus_7f42d1dc_;
input	[2:0]	bus_68aceb65_;
output	[31:0]	bus_1fb0e3fe_;
output	[31:0]	bus_2b6556c2_;
output		bus_563c0487_;
output		bus_5235cae6_;
output	[2:0]	bus_770e037d_;
output		bus_6ac7c410_;
output	[31:0]	bus_0e104710_;
output		bus_172ff744_;
wire		or_65b23652_u0;
reg		done_qual_u208=1'h0;
wire		and_73d475f3_u0;
wire	[31:0]	mux_65805a6c_u0;
reg		done_qual_u209_u0=1'h0;
wire		or_037c88cc_u0;
wire		or_216d2e9d_u0;
wire		or_223b290a_u0;
wire	[31:0]	mux_33260472_u0;
wire		not_35d9c76c_u0;
wire		not_44caea1f_u0;
wire		or_68bf8684_u0;
wire		and_4ff3e2ed_u0;
assign or_65b23652_u0=bus_5a23ef00_|bus_12a0e4a3_;
assign bus_1fb0e3fe_=mux_33260472_u0;
assign bus_2b6556c2_=mux_65805a6c_u0;
assign bus_563c0487_=or_68bf8684_u0;
assign bus_5235cae6_=or_037c88cc_u0;
assign bus_770e037d_=3'h1;
assign bus_6ac7c410_=and_73d475f3_u0;
assign bus_0e104710_=bus_414489ec_;
assign bus_172ff744_=and_4ff3e2ed_u0;
always @(posedge bus_3150bbf2_)
begin
if (bus_0affd4ba_)
done_qual_u208<=1'h0;
else done_qual_u208<=bus_22a90075_;
end
assign and_73d475f3_u0=or_216d2e9d_u0&bus_04338566_;
assign mux_65805a6c_u0=(bus_22a90075_)?bus_4733e576_:bus_7f42d1dc_;
always @(posedge bus_3150bbf2_)
begin
if (bus_0affd4ba_)
done_qual_u209_u0<=1'h0;
else done_qual_u209_u0<=or_65b23652_u0;
end
assign or_037c88cc_u0=bus_22a90075_|or_65b23652_u0;
assign or_216d2e9d_u0=bus_22a90075_|done_qual_u208;
assign or_223b290a_u0=or_65b23652_u0|done_qual_u209_u0;
assign mux_33260472_u0=(bus_22a90075_)?{24'b0, bus_7873a4e9_[7:0]}:bus_4f9cd513_;
assign not_35d9c76c_u0=~bus_04338566_;
assign not_44caea1f_u0=~bus_04338566_;
assign or_68bf8684_u0=bus_22a90075_|bus_12a0e4a3_;
assign and_4ff3e2ed_u0=or_223b290a_u0&bus_04338566_;
endmodule



module medianRow2_receive(CLK, RESET, GO, port_3b4deb68_, port_56ba8b92_, port_018fb986_, RESULT, RESULT_u1923, RESULT_u1924, RESULT_u1925, RESULT_u1926, RESULT_u1927, RESULT_u1928, DONE);
input		CLK;
input		RESET;
input		GO;
input	[31:0]	port_3b4deb68_;
input		port_56ba8b92_;
input	[7:0]	port_018fb986_;
output		RESULT;
output	[31:0]	RESULT_u1923;
output		RESULT_u1924;
output	[31:0]	RESULT_u1925;
output	[31:0]	RESULT_u1926;
output	[2:0]	RESULT_u1927;
output		RESULT_u1928;
output		DONE;
wire		simplePinWrite;
wire	[31:0]	add;
wire		and_u3199_u0;
wire		or_u1322_u0;
reg		reg_4a519417_u0=1'h0;
wire	[31:0]	add_u606;
reg		reg_0e879f3f_u0=1'h0;
assign simplePinWrite=GO&{1{GO}};
assign add=port_3b4deb68_+32'h0;
assign and_u3199_u0=reg_4a519417_u0&port_56ba8b92_;
assign or_u1322_u0=and_u3199_u0|RESET;
always @(posedge CLK or posedge GO or posedge or_u1322_u0)
begin
if (or_u1322_u0)
reg_4a519417_u0<=1'h0;
else if (GO)
reg_4a519417_u0<=1'h1;
else reg_4a519417_u0<=reg_4a519417_u0;
end
assign add_u606=port_3b4deb68_+32'h1;
always @(posedge CLK or posedge RESET)
begin
if (RESET)
reg_0e879f3f_u0<=1'h0;
else reg_0e879f3f_u0<=GO;
end
assign RESULT=GO;
assign RESULT_u1923=add_u606;
assign RESULT_u1924=GO;
assign RESULT_u1925=add;
assign RESULT_u1926={24'b0, port_018fb986_};
assign RESULT_u1927=3'h1;
assign RESULT_u1928=simplePinWrite;
assign DONE=reg_0e879f3f_u0;
endmodule



module medianRow2_compute_median(CLK, RESET, GO, port_2f0fcf3b_, port_1a513609_, port_0fd59e64_, port_60ba7fd2_, port_62c7b7a4_, RESULT, RESULT_u1929, RESULT_u1930, RESULT_u1931, RESULT_u1932, RESULT_u1933, RESULT_u1934, RESULT_u1935, RESULT_u1936, RESULT_u1937, RESULT_u1938, RESULT_u1939, RESULT_u1940, RESULT_u1941, RESULT_u1942, DONE);
input		CLK;
input		RESET;
input		GO;
input		port_2f0fcf3b_;
input	[31:0]	port_1a513609_;
input		port_0fd59e64_;
input	[31:0]	port_60ba7fd2_;
input		port_62c7b7a4_;
output		RESULT;
output	[31:0]	RESULT_u1929;
output		RESULT_u1930;
output	[31:0]	RESULT_u1931;
output	[2:0]	RESULT_u1932;
output		RESULT_u1933;
output	[31:0]	RESULT_u1934;
output	[2:0]	RESULT_u1935;
output		RESULT_u1936;
output	[31:0]	RESULT_u1937;
output	[31:0]	RESULT_u1938;
output	[2:0]	RESULT_u1939;
output	[15:0]	RESULT_u1940;
output		RESULT_u1941;
output	[7:0]	RESULT_u1942;
output		DONE;
wire	[15:0]	lessThan_b_unsigned;
wire	[15:0]	lessThan_a_unsigned;
wire		lessThan;
wire		andOp;
wire		and_u3200_u0;
wire		not_u722_u0;
wire		and_u3201_u0;
wire	[31:0]	add;
wire		and_u3202_u0;
wire	[31:0]	add_u607;
wire	[31:0]	add_u608;
wire		and_u3203_u0;
wire		greaterThan;
wire signed	[31:0]	greaterThan_a_signed;
wire signed	[31:0]	greaterThan_b_signed;
wire		and_u3204_u0;
wire		not_u723_u0;
wire		and_u3205_u0;
wire	[31:0]	add_u609;
wire		and_u3206_u0;
wire	[31:0]	add_u610;
wire	[31:0]	add_u611;
wire		and_u3207_u0;
wire	[31:0]	add_u612;
reg		reg_26326880_u0=1'h0;
wire		or_u1323_u0;
wire		and_u3208_u0;
wire	[31:0]	add_u613;
wire	[31:0]	add_u614;
wire		or_u1324_u0;
wire		and_u3209_u0;
reg		reg_31f25e6e_u0=1'h0;
reg	[31:0]	syncEnable_u1302=32'h0;
reg		reg_31541d9d_u0=1'h0;
reg	[31:0]	syncEnable_u1303_u0=32'h0;
wire	[31:0]	mux_u1757;
wire		or_u1325_u0;
wire	[31:0]	mux_u1758_u0;
reg		block_GO_delayed_u86=1'h0;
reg	[31:0]	syncEnable_u1304_u0=32'h0;
reg	[31:0]	syncEnable_u1305_u0=32'h0;
reg	[31:0]	syncEnable_u1306_u0=32'h0;
reg	[31:0]	syncEnable_u1307_u0=32'h0;
reg		reg_2c9cfebc_u0=1'h0;
wire		and_u3210_u0;
reg		reg_3a8de00c_u0=1'h0;
reg	[31:0]	syncEnable_u1308_u0=32'h0;
wire	[31:0]	mux_u1759_u0;
reg		reg_74e807a5_u0=1'h0;
wire		and_u3211_u0;
reg		and_delayed_u370=1'h0;
wire		or_u1326_u0;
wire		mux_u1760_u0;
reg	[31:0]	syncEnable_u1309_u0=32'h0;
reg		syncEnable_u1310_u0=1'h0;
reg		reg_2c9cfebc_result_delayed_u0=1'h0;
wire	[31:0]	mux_u1761_u0;
wire	[31:0]	add_u615;
reg	[31:0]	syncEnable_u1311_u0=32'h0;
wire		or_u1327_u0;
wire	[31:0]	mux_u1762_u0;
wire	[31:0]	mux_u1763_u0;
wire		or_u1328_u0;
reg	[31:0]	syncEnable_u1312_u0=32'h0;
reg		block_GO_delayed_u87_u0=1'h0;
reg	[31:0]	syncEnable_u1313_u0=32'h0;
reg	[31:0]	syncEnable_u1314_u0=32'h0;
reg	[31:0]	syncEnable_u1315_u0=32'h0;
reg	[31:0]	syncEnable_u1316_u0=32'h0;
reg		syncEnable_u1317_u0=1'h0;
wire		and_u3212_u0;
wire signed	[32:0]	lessThan_u83_b_signed;
wire		lessThan_u83;
wire signed	[32:0]	lessThan_u83_a_signed;
wire		and_u3213_u0;
wire		not_u724_u0;
wire		and_u3214_u0;
reg	[31:0]	syncEnable_u1318_u0=32'h0;
reg	[31:0]	syncEnable_u1319_u0=32'h0;
wire		mux_u1764_u0;
wire	[31:0]	mux_u1765_u0;
wire	[31:0]	mux_u1766_u0;
wire	[31:0]	mux_u1767_u0;
wire	[31:0]	mux_u1768_u0;
wire	[31:0]	mux_u1769_u0;
wire		or_u1329_u0;
wire	[15:0]	add_u616;
reg		latch_2580f220_reg=1'h0;
wire		latch_2580f220_out;
reg	[31:0]	latch_27e74ea8_reg=32'h0;
wire	[31:0]	latch_27e74ea8_out;
wire	[31:0]	latch_7b889667_out;
reg	[31:0]	latch_7b889667_reg=32'h0;
reg		reg_345ffaea_u0=1'h0;
reg	[15:0]	syncEnable_u1320_u0=16'h0;
wire	[31:0]	latch_43a4d61d_out;
reg	[31:0]	latch_43a4d61d_reg=32'h0;
wire		scoreboard_2ce15a9c_and;
reg		scoreboard_2ce15a9c_reg1=1'h0;
wire		scoreboard_2ce15a9c_resOr1;
reg		scoreboard_2ce15a9c_reg0=1'h0;
wire		bus_0d764672_;
wire		scoreboard_2ce15a9c_resOr0;
reg	[31:0]	latch_2f0eca30_reg=32'h0;
wire	[31:0]	latch_2f0eca30_out;
wire		and_u3215_u0;
wire		and_u3216_u0;
wire		mux_u1770_u0;
reg	[31:0]	fbReg_tmp_u40=32'h0;
wire	[31:0]	mux_u1771_u0;
reg	[15:0]	fbReg_idx_u40=16'h0;
wire	[31:0]	mux_u1772_u0;
wire	[31:0]	mux_u1773_u0;
reg	[31:0]	fbReg_tmp_row1_u40=32'h0;
reg		fbReg_swapped_u40=1'h0;
reg	[31:0]	fbReg_tmp_row_u40=32'h0;
reg		loopControl_u85=1'h0;
wire	[15:0]	mux_u1774_u0;
wire		or_u1330_u0;
reg	[31:0]	fbReg_tmp_row0_u40=32'h0;
wire	[31:0]	mux_u1775_u0;
reg		syncEnable_u1321_u0=1'h0;
wire		and_u3217_u0;
wire	[15:0]	simplePinWrite;
wire		simplePinWrite_u458;
wire	[7:0]	simplePinWrite_u459;
reg		reg_2c36b866_u0=1'h0;
reg		reg_2c36b866_result_delayed_u0=1'h0;
wire		or_u1331_u0;
wire	[31:0]	mux_u1776_u0;
assign lessThan_a_unsigned=mux_u1774_u0;
assign lessThan_b_unsigned=16'h65;
assign lessThan=lessThan_a_unsigned<lessThan_b_unsigned;
assign andOp=lessThan&mux_u1770_u0;
assign and_u3200_u0=or_u1330_u0&not_u722_u0;
assign not_u722_u0=~andOp;
assign and_u3201_u0=or_u1330_u0&andOp;
assign add=mux_u1765_u0+32'h0;
assign and_u3202_u0=and_u3212_u0&port_2f0fcf3b_;
assign add_u607=mux_u1765_u0+32'h1;
assign add_u608=add_u607+32'h0;
assign and_u3203_u0=and_u3212_u0&port_62c7b7a4_;
assign greaterThan_a_signed=syncEnable_u1313_u0;
assign greaterThan_b_signed=syncEnable_u1315_u0;
assign greaterThan=greaterThan_a_signed>greaterThan_b_signed;
assign and_u3204_u0=block_GO_delayed_u87_u0&not_u723_u0;
assign not_u723_u0=~greaterThan;
assign and_u3205_u0=block_GO_delayed_u87_u0&greaterThan;
assign add_u609=syncEnable_u1316_u0+32'h0;
assign and_u3206_u0=and_u3210_u0&port_2f0fcf3b_;
assign add_u610=syncEnable_u1316_u0+32'h1;
assign add_u611=add_u610+32'h0;
assign and_u3207_u0=and_u3210_u0&port_62c7b7a4_;
assign add_u612=syncEnable_u1316_u0+32'h0;
always @(posedge CLK or posedge block_GO_delayed_u86 or posedge or_u1323_u0)
begin
if (or_u1323_u0)
reg_26326880_u0<=1'h0;
else if (block_GO_delayed_u86)
reg_26326880_u0<=1'h1;
else reg_26326880_u0<=reg_26326880_u0;
end
assign or_u1323_u0=and_u3208_u0|RESET;
assign and_u3208_u0=reg_26326880_u0&port_62c7b7a4_;
assign add_u613=syncEnable_u1316_u0+32'h1;
assign add_u614=add_u613+32'h0;
assign or_u1324_u0=and_u3209_u0|RESET;
assign and_u3209_u0=reg_31f25e6e_u0&port_62c7b7a4_;
always @(posedge CLK or posedge reg_31541d9d_u0 or posedge or_u1324_u0)
begin
if (or_u1324_u0)
reg_31f25e6e_u0<=1'h0;
else if (reg_31541d9d_u0)
reg_31f25e6e_u0<=1'h1;
else reg_31f25e6e_u0<=reg_31f25e6e_u0;
end
always @(posedge CLK)
begin
if (and_u3210_u0)
syncEnable_u1302<=add_u614;
end
always @(posedge CLK or posedge RESET)
begin
if (RESET)
reg_31541d9d_u0<=1'h0;
else reg_31541d9d_u0<=block_GO_delayed_u86;
end
always @(posedge CLK)
begin
if (and_u3210_u0)
syncEnable_u1303_u0<=port_60ba7fd2_;
end
assign mux_u1757=(block_GO_delayed_u86)?syncEnable_u1306_u0:syncEnable_u1304_u0;
assign or_u1325_u0=block_GO_delayed_u86|reg_31541d9d_u0;
assign mux_u1758_u0=({32{block_GO_delayed_u86}}&syncEnable_u1307_u0)|({32{reg_31541d9d_u0}}&syncEnable_u1302)|({32{and_u3210_u0}}&add_u611);
always @(posedge CLK or posedge RESET)
begin
if (RESET)
block_GO_delayed_u86<=1'h0;
else block_GO_delayed_u86<=and_u3210_u0;
end
always @(posedge CLK)
begin
if (and_u3210_u0)
syncEnable_u1304_u0<=port_1a513609_;
end
always @(posedge CLK)
begin
if (and_u3210_u0)
syncEnable_u1305_u0<=port_1a513609_;
end
always @(posedge CLK)
begin
if (and_u3210_u0)
syncEnable_u1306_u0<=port_60ba7fd2_;
end
always @(posedge CLK)
begin
if (and_u3210_u0)
syncEnable_u1307_u0<=add_u612;
end
always @(posedge CLK or posedge RESET)
begin
if (RESET)
reg_2c9cfebc_u0<=1'h0;
else reg_2c9cfebc_u0<=reg_74e807a5_u0;
end
assign and_u3210_u0=and_u3205_u0&block_GO_delayed_u87_u0;
always @(posedge CLK or posedge RESET)
begin
if (RESET)
reg_3a8de00c_u0<=1'h0;
else reg_3a8de00c_u0<=and_u3211_u0;
end
always @(posedge CLK)
begin
if (block_GO_delayed_u87_u0)
syncEnable_u1308_u0<=syncEnable_u1312_u0;
end
assign mux_u1759_u0=(reg_2c9cfebc_result_delayed_u0)?syncEnable_u1303_u0:syncEnable_u1309_u0;
always @(posedge CLK or posedge RESET)
begin
if (RESET)
reg_74e807a5_u0<=1'h0;
else reg_74e807a5_u0<=and_delayed_u370;
end
assign and_u3211_u0=and_u3204_u0&block_GO_delayed_u87_u0;
always @(posedge CLK or posedge RESET)
begin
if (RESET)
and_delayed_u370<=1'h0;
else and_delayed_u370<=and_u3210_u0;
end
assign or_u1326_u0=reg_2c9cfebc_result_delayed_u0|reg_3a8de00c_u0;
assign mux_u1760_u0=(reg_2c9cfebc_result_delayed_u0)?1'h1:syncEnable_u1310_u0;
always @(posedge CLK)
begin
if (block_GO_delayed_u87_u0)
syncEnable_u1309_u0<=syncEnable_u1314_u0;
end
always @(posedge CLK)
begin
if (block_GO_delayed_u87_u0)
syncEnable_u1310_u0<=syncEnable_u1317_u0;
end
always @(posedge CLK or posedge RESET)
begin
if (RESET)
reg_2c9cfebc_result_delayed_u0<=1'h0;
else reg_2c9cfebc_result_delayed_u0<=reg_2c9cfebc_u0;
end
assign mux_u1761_u0=(reg_2c9cfebc_result_delayed_u0)?syncEnable_u1305_u0:syncEnable_u1308_u0;
assign add_u615=mux_u1765_u0+32'h1;
always @(posedge CLK)
begin
if (and_u3212_u0)
syncEnable_u1311_u0<=add_u615;
end
assign or_u1327_u0=and_u3212_u0|and_u3210_u0;
assign mux_u1762_u0=(and_u3212_u0)?add:add_u609;
assign mux_u1763_u0=({32{or_u1325_u0}}&mux_u1758_u0)|({32{and_u3212_u0}}&add_u608)|({32{and_u3210_u0}}&mux_u1758_u0);
assign or_u1328_u0=and_u3212_u0|and_u3210_u0;
always @(posedge CLK)
begin
if (and_u3212_u0)
syncEnable_u1312_u0<=mux_u1767_u0;
end
always @(posedge CLK or posedge RESET)
begin
if (RESET)
block_GO_delayed_u87_u0<=1'h0;
else block_GO_delayed_u87_u0<=and_u3212_u0;
end
always @(posedge CLK)
begin
if (and_u3212_u0)
syncEnable_u1313_u0<=port_1a513609_;
end
always @(posedge CLK)
begin
if (and_u3212_u0)
syncEnable_u1314_u0<=mux_u1769_u0;
end
always @(posedge CLK)
begin
if (and_u3212_u0)
syncEnable_u1315_u0<=port_60ba7fd2_;
end
always @(posedge CLK)
begin
if (and_u3212_u0)
syncEnable_u1316_u0<=mux_u1765_u0;
end
always @(posedge CLK)
begin
if (and_u3212_u0)
syncEnable_u1317_u0<=mux_u1764_u0;
end
assign and_u3212_u0=and_u3214_u0&or_u1329_u0;
assign lessThan_u83_a_signed={1'b0, mux_u1765_u0};
assign lessThan_u83_b_signed=33'h64;
assign lessThan_u83=lessThan_u83_a_signed<lessThan_u83_b_signed;
assign and_u3213_u0=or_u1329_u0&not_u724_u0;
assign not_u724_u0=~lessThan_u83;
assign and_u3214_u0=or_u1329_u0&lessThan_u83;
always @(posedge CLK)
begin
if (or_u1329_u0)
syncEnable_u1318_u0<=mux_u1766_u0;
end
always @(posedge CLK)
begin
if (or_u1329_u0)
syncEnable_u1319_u0<=mux_u1768_u0;
end
assign mux_u1764_u0=(or_u1326_u0)?mux_u1760_u0:1'h0;
assign mux_u1765_u0=(or_u1326_u0)?syncEnable_u1311_u0:32'h0;
assign mux_u1766_u0=(or_u1326_u0)?syncEnable_u1315_u0:mux_u1773_u0;
assign mux_u1767_u0=(or_u1326_u0)?mux_u1761_u0:mux_u1772_u0;
assign mux_u1768_u0=(or_u1326_u0)?syncEnable_u1313_u0:mux_u1775_u0;
assign mux_u1769_u0=(or_u1326_u0)?mux_u1759_u0:mux_u1771_u0;
assign or_u1329_u0=or_u1326_u0|and_u3215_u0;
assign add_u616=mux_u1774_u0+16'h1;
always @(posedge CLK)
begin
if (or_u1326_u0)
latch_2580f220_reg<=mux_u1760_u0;
end
assign latch_2580f220_out=(or_u1326_u0)?mux_u1760_u0:latch_2580f220_reg;
always @(posedge CLK)
begin
if (or_u1326_u0)
latch_27e74ea8_reg<=syncEnable_u1319_u0;
end
assign latch_27e74ea8_out=(or_u1326_u0)?syncEnable_u1319_u0:latch_27e74ea8_reg;
assign latch_7b889667_out=(or_u1326_u0)?mux_u1761_u0:latch_7b889667_reg;
always @(posedge CLK)
begin
if (or_u1326_u0)
latch_7b889667_reg<=mux_u1761_u0;
end
always @(posedge CLK or posedge RESET)
begin
if (RESET)
reg_345ffaea_u0<=1'h0;
else reg_345ffaea_u0<=or_u1326_u0;
end
always @(posedge CLK)
begin
if (and_u3215_u0)
syncEnable_u1320_u0<=add_u616;
end
assign latch_43a4d61d_out=(or_u1326_u0)?mux_u1759_u0:latch_43a4d61d_reg;
always @(posedge CLK)
begin
if (or_u1326_u0)
latch_43a4d61d_reg<=mux_u1759_u0;
end
assign scoreboard_2ce15a9c_and=scoreboard_2ce15a9c_resOr0&scoreboard_2ce15a9c_resOr1;
always @(posedge CLK)
begin
if (bus_0d764672_)
scoreboard_2ce15a9c_reg1<=1'h0;
else if (reg_345ffaea_u0)
scoreboard_2ce15a9c_reg1<=1'h1;
else scoreboard_2ce15a9c_reg1<=scoreboard_2ce15a9c_reg1;
end
assign scoreboard_2ce15a9c_resOr1=reg_345ffaea_u0|scoreboard_2ce15a9c_reg1;
always @(posedge CLK)
begin
if (bus_0d764672_)
scoreboard_2ce15a9c_reg0<=1'h0;
else if (or_u1326_u0)
scoreboard_2ce15a9c_reg0<=1'h1;
else scoreboard_2ce15a9c_reg0<=scoreboard_2ce15a9c_reg0;
end
assign bus_0d764672_=scoreboard_2ce15a9c_and|RESET;
assign scoreboard_2ce15a9c_resOr0=or_u1326_u0|scoreboard_2ce15a9c_reg0;
always @(posedge CLK)
begin
if (or_u1326_u0)
latch_2f0eca30_reg<=syncEnable_u1318_u0;
end
assign latch_2f0eca30_out=(or_u1326_u0)?syncEnable_u1318_u0:latch_2f0eca30_reg;
assign and_u3215_u0=and_u3201_u0&or_u1330_u0;
assign and_u3216_u0=and_u3200_u0&or_u1330_u0;
assign mux_u1770_u0=(loopControl_u85)?fbReg_swapped_u40:1'h0;
always @(posedge CLK)
begin
if (scoreboard_2ce15a9c_and)
fbReg_tmp_u40<=latch_7b889667_out;
end
assign mux_u1771_u0=(loopControl_u85)?fbReg_tmp_row1_u40:32'h0;
always @(posedge CLK)
begin
if (scoreboard_2ce15a9c_and)
fbReg_idx_u40<=syncEnable_u1320_u0;
end
assign mux_u1772_u0=(loopControl_u85)?fbReg_tmp_u40:32'h0;
assign mux_u1773_u0=(loopControl_u85)?fbReg_tmp_row0_u40:32'h0;
always @(posedge CLK)
begin
if (scoreboard_2ce15a9c_and)
fbReg_tmp_row1_u40<=latch_43a4d61d_out;
end
always @(posedge CLK)
begin
if (scoreboard_2ce15a9c_and)
fbReg_swapped_u40<=latch_2580f220_out;
end
always @(posedge CLK)
begin
if (scoreboard_2ce15a9c_and)
fbReg_tmp_row_u40<=latch_27e74ea8_out;
end
always @(posedge CLK or posedge syncEnable_u1321_u0)
begin
if (syncEnable_u1321_u0)
loopControl_u85<=1'h0;
else loopControl_u85<=scoreboard_2ce15a9c_and;
end
assign mux_u1774_u0=(loopControl_u85)?fbReg_idx_u40:16'h0;
assign or_u1330_u0=loopControl_u85|GO;
always @(posedge CLK)
begin
if (scoreboard_2ce15a9c_and)
fbReg_tmp_row0_u40<=latch_2f0eca30_out;
end
assign mux_u1775_u0=(loopControl_u85)?fbReg_tmp_row_u40:32'h0;
always @(posedge CLK)
begin
if (GO)
syncEnable_u1321_u0<=RESET;
end
assign and_u3217_u0=reg_2c36b866_u0&port_2f0fcf3b_;
assign simplePinWrite=16'h1&{16{1'h1}};
assign simplePinWrite_u458=reg_2c36b866_u0&{1{reg_2c36b866_u0}};
assign simplePinWrite_u459=port_1a513609_[7:0];
always @(posedge CLK or posedge RESET)
begin
if (RESET)
reg_2c36b866_u0<=1'h0;
else reg_2c36b866_u0<=and_u3216_u0;
end
always @(posedge CLK or posedge RESET)
begin
if (RESET)
reg_2c36b866_result_delayed_u0<=1'h0;
else reg_2c36b866_result_delayed_u0<=reg_2c36b866_u0;
end
assign or_u1331_u0=or_u1327_u0|reg_2c36b866_u0;
assign mux_u1776_u0=(or_u1327_u0)?mux_u1762_u0:32'h32;
assign RESULT=GO;
assign RESULT_u1929=32'h0;
assign RESULT_u1930=or_u1331_u0;
assign RESULT_u1931=mux_u1776_u0;
assign RESULT_u1932=3'h1;
assign RESULT_u1933=or_u1328_u0;
assign RESULT_u1934=mux_u1763_u0;
assign RESULT_u1935=3'h1;
assign RESULT_u1936=or_u1325_u0;
assign RESULT_u1937=mux_u1763_u0;
assign RESULT_u1938=mux_u1757;
assign RESULT_u1939=3'h1;
assign RESULT_u1940=simplePinWrite;
assign RESULT_u1941=simplePinWrite_u458;
assign RESULT_u1942=simplePinWrite_u459;
assign DONE=reg_2c36b866_result_delayed_u0;
endmodule



module medianRow2_Kicker_50(CLK, RESET, bus_259878bd_);
input		CLK;
input		RESET;
output		bus_259878bd_;
reg		kicker_2=1'h0;
wire		bus_6f06ab80_;
wire		bus_4eae7249_;
wire		bus_6f05c454_;
reg		kicker_res=1'h0;
wire		bus_73c5cd62_;
reg		kicker_1=1'h0;
always @(posedge CLK)
begin
kicker_2<=bus_73c5cd62_;
end
assign bus_6f06ab80_=~RESET;
assign bus_259878bd_=kicker_res;
assign bus_4eae7249_=~kicker_2;
assign bus_6f05c454_=kicker_1&bus_6f06ab80_&bus_4eae7249_;
always @(posedge CLK)
begin
kicker_res<=bus_6f05c454_;
end
assign bus_73c5cd62_=bus_6f06ab80_&kicker_1;
always @(posedge CLK)
begin
kicker_1<=bus_6f06ab80_;
end
endmodule



module medianRow2_simplememoryreferee_11c18fee_(bus_712c2001_, bus_679456eb_, bus_7f1b8f4b_, bus_2b8f88eb_, bus_05c0a954_, bus_532ffaac_, bus_182d4b07_, bus_5f93a996_, bus_1ca57154_, bus_0927b503_, bus_4a3de72a_, bus_667319c3_, bus_7b03152e_, bus_606cbacc_);
input		bus_712c2001_;
input		bus_679456eb_;
input		bus_7f1b8f4b_;
input	[31:0]	bus_2b8f88eb_;
input		bus_05c0a954_;
input	[31:0]	bus_532ffaac_;
input	[2:0]	bus_182d4b07_;
output	[31:0]	bus_5f93a996_;
output	[31:0]	bus_1ca57154_;
output		bus_0927b503_;
output		bus_4a3de72a_;
output	[2:0]	bus_667319c3_;
output	[31:0]	bus_7b03152e_;
output		bus_606cbacc_;
assign bus_5f93a996_=32'h0;
assign bus_1ca57154_=bus_532ffaac_;
assign bus_0927b503_=1'h0;
assign bus_4a3de72a_=bus_05c0a954_;
assign bus_667319c3_=3'h1;
assign bus_7b03152e_=bus_2b8f88eb_;
assign bus_606cbacc_=bus_7f1b8f4b_;
endmodule



module medianRow2_endianswapper_097be8b8_(endianswapper_097be8b8_in, endianswapper_097be8b8_out);
input	[31:0]	endianswapper_097be8b8_in;
output	[31:0]	endianswapper_097be8b8_out;
assign endianswapper_097be8b8_out=endianswapper_097be8b8_in;
endmodule



module medianRow2_endianswapper_282e0163_(endianswapper_282e0163_in, endianswapper_282e0163_out);
input	[31:0]	endianswapper_282e0163_in;
output	[31:0]	endianswapper_282e0163_out;
assign endianswapper_282e0163_out=endianswapper_282e0163_in;
endmodule



module medianRow2_stateVar_i(bus_4ffa273d_, bus_7a280464_, bus_09b6d000_, bus_2ddae079_, bus_642b7542_, bus_57446513_, bus_4b94181f_);
input		bus_4ffa273d_;
input		bus_7a280464_;
input		bus_09b6d000_;
input	[31:0]	bus_2ddae079_;
input		bus_642b7542_;
input	[31:0]	bus_57446513_;
output	[31:0]	bus_4b94181f_;
wire		or_69ddeb2e_u0;
wire	[31:0]	endianswapper_097be8b8_out;
wire	[31:0]	mux_3d2748f3_u0;
reg	[31:0]	stateVar_i_u41=32'h0;
wire	[31:0]	endianswapper_282e0163_out;
assign or_69ddeb2e_u0=bus_09b6d000_|bus_642b7542_;
medianRow2_endianswapper_097be8b8_ medianRow2_endianswapper_097be8b8__1(.endianswapper_097be8b8_in(stateVar_i_u41), 
  .endianswapper_097be8b8_out(endianswapper_097be8b8_out));
assign mux_3d2748f3_u0=(bus_09b6d000_)?bus_2ddae079_:32'h0;
always @(posedge bus_4ffa273d_ or posedge bus_7a280464_)
begin
if (bus_7a280464_)
stateVar_i_u41<=32'h0;
else if (or_69ddeb2e_u0)
stateVar_i_u41<=endianswapper_282e0163_out;
end
assign bus_4b94181f_=endianswapper_097be8b8_out;
medianRow2_endianswapper_282e0163_ medianRow2_endianswapper_282e0163__1(.endianswapper_282e0163_in(mux_3d2748f3_u0), 
  .endianswapper_282e0163_out(endianswapper_282e0163_out));
endmodule


