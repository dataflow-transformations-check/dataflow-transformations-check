// __  ___ __ ___  _ __   ___  ___ 
// \ \/ / '__/ _ \| '_ \ / _ \/ __|
//  >  <| | | (_) | | | | (_) \__ \
// /_/\_\_|  \___/|_| |_|\___/|___/
// 
// Xronos synthesizer version
// Run date: Fri 23 Feb 2018 14:28:01 +0000
// 

module medianRow3(CLK, RESET, in1_COUNT, median_COUNT, in1_SEND, median_ACK, in1_ACK, in1_DATA, median_DATA, median_SEND, median_RDY);
input		CLK;
input		RESET;
input	[15:0]	in1_COUNT;
wire		compute_median_done;
output	[15:0]	median_COUNT;
input		in1_SEND;
input		median_ACK;
wire		compute_median_go;
output		in1_ACK;
input	[7:0]	in1_DATA;
wire		receive_done;
output	[7:0]	median_DATA;
wire		receive_go;
output		median_SEND;
input		median_RDY;
wire		scheduler_u768;
wire		medianRow3_scheduler_instance_DONE;
wire		scheduler;
wire		scheduler_u769;
wire		scheduler_u767;
wire	[31:0]	bus_7e32c356_;
wire		bus_15dc3a72_;
wire	[31:0]	bus_299a3d99_;
wire	[31:0]	bus_47d92ba9_;
wire		bus_4a7ea9d3_;
wire		bus_276d2572_;
wire	[31:0]	bus_579f786c_;
wire	[31:0]	bus_208ce5dd_;
wire		bus_38d71f8c_;
wire		bus_58d62dee_;
wire		bus_0b970a93_;
wire		bus_50a24456_;
wire	[31:0]	bus_6d1ad724_;
wire	[2:0]	bus_16d696c1_;
wire		compute_median_u592;
wire	[2:0]	compute_median_u598;
wire	[7:0]	compute_median_u599;
wire	[31:0]	compute_median_u596;
wire	[2:0]	compute_median_u591;
wire	[15:0]	compute_median_u601;
wire	[31:0]	compute_median_u590;
wire		medianRow3_compute_median_instance_DONE;
wire		compute_median_u595;
wire		compute_median;
wire	[31:0]	compute_median_u597;
wire	[31:0]	compute_median_u588;
wire	[31:0]	compute_median_u593;
wire		compute_median_u589;
wire		compute_median_u600;
wire	[2:0]	compute_median_u594;
wire		bus_7fabd8a7_;
wire		bus_19d2b492_;
wire		bus_1e8a6260_;
wire	[31:0]	receive_u252;
wire		medianRow3_receive_instance_DONE;
wire		receive_u257;
wire	[2:0]	receive_u256;
wire	[31:0]	receive_u254;
wire		receive;
wire	[31:0]	receive_u255;
wire		receive_u253;
wire		bus_540b1976_;
wire	[31:0]	bus_2523f18a_;
wire	[2:0]	bus_57e9a613_;
wire	[31:0]	bus_01da4310_;
wire		bus_22e0ab4d_;
wire		bus_45b27ee7_;
wire	[31:0]	bus_259da384_;
wire		bus_17da45ba_;
assign compute_median_done=bus_19d2b492_;
assign median_COUNT=compute_median_u601;
assign compute_median_go=scheduler_u768;
assign in1_ACK=receive_u257;
assign receive_done=bus_7fabd8a7_;
assign median_DATA=compute_median_u599;
assign receive_go=scheduler_u769;
assign median_SEND=compute_median_u600;
medianRow3_scheduler medianRow3_scheduler_instance(.CLK(CLK), .RESET(bus_1e8a6260_), 
  .GO(bus_15dc3a72_), .port_305c0491_(bus_540b1976_), .port_70a109be_(bus_7e32c356_), 
  .port_6747ee78_(in1_SEND), .port_428cc5c9_(receive_done), .port_4179d856_(compute_median_done), 
  .port_3943a37c_(median_RDY), .DONE(medianRow3_scheduler_instance_DONE), .RESULT(scheduler), 
  .RESULT_u1966(scheduler_u767), .RESULT_u1967(scheduler_u768), .RESULT_u1968(scheduler_u769));
medianRow3_stateVar_i medianRow3_stateVar_i_1(.bus_09f2d119_(CLK), .bus_34206eb6_(bus_1e8a6260_), 
  .bus_016c8813_(receive), .bus_29eb6397_(receive_u252), .bus_4ce557ac_(compute_median), 
  .bus_6fe13ae5_(32'h0), .bus_7e32c356_(bus_7e32c356_));
medianRow3_Kicker_52 medianRow3_Kicker_52_1(.CLK(CLK), .RESET(bus_1e8a6260_), .bus_15dc3a72_(bus_15dc3a72_));
medianRow3_structuralmemory_15370eb9_ medianRow3_structuralmemory_15370eb9__1(.CLK_u88(CLK), 
  .bus_3fb75481_(bus_1e8a6260_), .bus_6c50ca49_(bus_259da384_), .bus_331ba1e4_(3'h1), 
  .bus_0e6811e7_(bus_45b27ee7_), .bus_0e7df011_(bus_208ce5dd_), .bus_49969a43_(3'h1), 
  .bus_2d013811_(bus_38d71f8c_), .bus_3f1ed616_(bus_50a24456_), .bus_0ce591f3_(bus_579f786c_), 
  .bus_299a3d99_(bus_299a3d99_), .bus_276d2572_(bus_276d2572_), .bus_47d92ba9_(bus_47d92ba9_), 
  .bus_4a7ea9d3_(bus_4a7ea9d3_));
medianRow3_simplememoryreferee_14849a54_ medianRow3_simplememoryreferee_14849a54__1(.bus_7a9073c9_(CLK), 
  .bus_078b3f3f_(bus_1e8a6260_), .bus_4b8c9ecf_(bus_4a7ea9d3_), .bus_4d623c08_(bus_47d92ba9_), 
  .bus_0ff8d3a9_(receive_u253), .bus_4c0eb824_({24'b0, receive_u255[7:0]}), .bus_3e3c13b1_(receive_u254), 
  .bus_7bec75e4_(3'h1), .bus_5e6a8de8_(compute_median_u589), .bus_3980d2b8_(compute_median_u595), 
  .bus_3de6add0_(compute_median_u597), .bus_496f379e_(compute_median_u596), .bus_07bb41ab_(3'h1), 
  .bus_579f786c_(bus_579f786c_), .bus_208ce5dd_(bus_208ce5dd_), .bus_50a24456_(bus_50a24456_), 
  .bus_38d71f8c_(bus_38d71f8c_), .bus_16d696c1_(bus_16d696c1_), .bus_0b970a93_(bus_0b970a93_), 
  .bus_6d1ad724_(bus_6d1ad724_), .bus_58d62dee_(bus_58d62dee_));
medianRow3_compute_median medianRow3_compute_median_instance(.CLK(CLK), .RESET(bus_1e8a6260_), 
  .GO(compute_median_go), .port_354097ba_(bus_58d62dee_), .port_0dfedef3_(bus_6d1ad724_), 
  .port_483b0649_(bus_22e0ab4d_), .port_52e16905_(bus_01da4310_), .port_4f7891d8_(bus_58d62dee_), 
  .DONE(medianRow3_compute_median_instance_DONE), .RESULT(compute_median), .RESULT_u1969(compute_median_u588), 
  .RESULT_u1973(compute_median_u589), .RESULT_u1974(compute_median_u590), .RESULT_u1975(compute_median_u591), 
  .RESULT_u1970(compute_median_u592), .RESULT_u1971(compute_median_u593), .RESULT_u1972(compute_median_u594), 
  .RESULT_u1976(compute_median_u595), .RESULT_u1977(compute_median_u596), .RESULT_u1978(compute_median_u597), 
  .RESULT_u1979(compute_median_u598), .RESULT_u1980(compute_median_u599), .RESULT_u1981(compute_median_u600), 
  .RESULT_u1982(compute_median_u601));
assign bus_7fabd8a7_=medianRow3_receive_instance_DONE&{1{medianRow3_receive_instance_DONE}};
assign bus_19d2b492_=medianRow3_compute_median_instance_DONE&{1{medianRow3_compute_median_instance_DONE}};
medianRow3_globalreset_physical_60677802_ medianRow3_globalreset_physical_60677802__1(.bus_18530ae3_(CLK), 
  .bus_2d088eaa_(RESET), .bus_1e8a6260_(bus_1e8a6260_));
medianRow3_receive medianRow3_receive_instance(.CLK(CLK), .RESET(bus_1e8a6260_), 
  .GO(receive_go), .port_48d875af_(bus_7e32c356_), .port_5f0d9a55_(bus_0b970a93_), 
  .port_53143199_(in1_DATA), .DONE(medianRow3_receive_instance_DONE), .RESULT(receive), 
  .RESULT_u1983(receive_u252), .RESULT_u1984(receive_u253), .RESULT_u1985(receive_u254), 
  .RESULT_u1986(receive_u255), .RESULT_u1987(receive_u256), .RESULT_u1988(receive_u257));
medianRow3_stateVar_fsmState_medianRow3 medianRow3_stateVar_fsmState_medianRow3_1(.bus_6a4d280b_(CLK), 
  .bus_78dd91e2_(bus_1e8a6260_), .bus_063b7a30_(scheduler), .bus_1173d449_(scheduler_u767), 
  .bus_540b1976_(bus_540b1976_));
medianRow3_simplememoryreferee_51b7974f_ medianRow3_simplememoryreferee_51b7974f__1(.bus_3ee70140_(CLK), 
  .bus_64afef21_(bus_1e8a6260_), .bus_10d604dd_(bus_276d2572_), .bus_36c7d3ac_(bus_299a3d99_), 
  .bus_1b19a75e_(compute_median_u592), .bus_19ee9cde_(compute_median_u593), .bus_3262fe74_(3'h1), 
  .bus_2523f18a_(bus_2523f18a_), .bus_259da384_(bus_259da384_), .bus_17da45ba_(bus_17da45ba_), 
  .bus_45b27ee7_(bus_45b27ee7_), .bus_57e9a613_(bus_57e9a613_), .bus_01da4310_(bus_01da4310_), 
  .bus_22e0ab4d_(bus_22e0ab4d_));
endmodule



module medianRow3_scheduler(CLK, RESET, GO, port_305c0491_, port_70a109be_, port_6747ee78_, port_428cc5c9_, port_4179d856_, port_3943a37c_, RESULT, RESULT_u1966, RESULT_u1967, RESULT_u1968, DONE);
input		CLK;
input		RESET;
input		GO;
input		port_305c0491_;
input	[31:0]	port_70a109be_;
input		port_6747ee78_;
input		port_428cc5c9_;
input		port_4179d856_;
input		port_3943a37c_;
output		RESULT;
output		RESULT_u1966;
output		RESULT_u1967;
output		RESULT_u1968;
output		DONE;
wire signed	[31:0]	lessThan_b_signed;
wire signed	[31:0]	lessThan_a_signed;
wire		lessThan;
wire signed	[31:0]	equals_b_signed;
wire signed	[31:0]	equals_a_signed;
wire		equals;
wire signed	[31:0]	equals_u195_b_signed;
wire signed	[31:0]	equals_u195_a_signed;
wire		equals_u195;
wire		and_u3260_u0;
wire		and_u3261_u0;
wire		not_u734_u0;
wire		andOp;
wire		and_u3262_u0;
wire		not_u735_u0;
wire		and_u3263_u0;
wire		simplePinWrite;
wire		and_u3264_u0;
wire		and_u3265_u0;
wire		equals_u196;
wire signed	[31:0]	equals_u196_a_signed;
wire signed	[31:0]	equals_u196_b_signed;
wire		and_u3266_u0;
wire		not_u736_u0;
wire		and_u3267_u0;
wire		andOp_u61;
wire		and_u3268_u0;
wire		not_u737_u0;
wire		and_u3269_u0;
wire		simplePinWrite_u464;
wire		and_u3270_u0;
wire		and_u3271_u0;
wire		not_u738_u0;
wire		not_u739_u0;
wire		and_u3272_u0;
wire		and_u3273_u0;
wire		simplePinWrite_u465;
wire		and_u3274_u0;
wire		or_u1350_u0;
reg		reg_5820df61_u0=1'h0;
wire		and_u3275_u0;
wire		and_u3276_u0;
reg		reg_2f7bd17d_u0=1'h0;
wire		or_u1351_u0;
wire		and_u3277_u0;
wire		and_u3278_u0;
reg		and_delayed_u374=1'h0;
wire		mux_u1800;
wire		or_u1352_u0;
wire		or_u1353_u0;
wire		and_u3279_u0;
wire		and_u3280_u0;
wire		or_u1354_u0;
wire		and_u3281_u0;
reg		and_delayed_u375_u0=1'h0;
wire		or_u1355_u0;
wire		mux_u1801_u0;
wire		receive_go_merge;
wire		and_u3282_u0;
reg		loopControl_u88=1'h0;
reg		syncEnable_u1342=1'h0;
wire		or_u1356_u0;
reg		reg_3a328eba_u0=1'h0;
wire		or_u1357_u0;
wire		mux_u1802_u0;
reg		reg_2a9903a1_u0=1'h0;
assign lessThan_a_signed=port_70a109be_;
assign lessThan_b_signed=32'h65;
assign lessThan=lessThan_a_signed<lessThan_b_signed;
assign equals_a_signed=port_70a109be_;
assign equals_b_signed=32'h65;
assign equals=equals_a_signed==equals_b_signed;
assign equals_u195_a_signed={31'b0, port_305c0491_};
assign equals_u195_b_signed=32'h0;
assign equals_u195=equals_u195_a_signed==equals_u195_b_signed;
assign and_u3260_u0=and_u3282_u0&not_u734_u0;
assign and_u3261_u0=and_u3282_u0&equals_u195;
assign not_u734_u0=~equals_u195;
assign andOp=lessThan&port_6747ee78_;
assign and_u3262_u0=and_u3265_u0&not_u735_u0;
assign not_u735_u0=~andOp;
assign and_u3263_u0=and_u3265_u0&andOp;
assign simplePinWrite=and_u3264_u0&{1{and_u3264_u0}};
assign and_u3264_u0=and_u3263_u0&and_u3265_u0;
assign and_u3265_u0=and_u3261_u0&and_u3282_u0;
assign equals_u196_a_signed={31'b0, port_305c0491_};
assign equals_u196_b_signed=32'h1;
assign equals_u196=equals_u196_a_signed==equals_u196_b_signed;
assign and_u3266_u0=and_u3282_u0&not_u736_u0;
assign not_u736_u0=~equals_u196;
assign and_u3267_u0=and_u3282_u0&equals_u196;
assign andOp_u61=lessThan&port_6747ee78_;
assign and_u3268_u0=and_u3281_u0&andOp_u61;
assign not_u737_u0=~andOp_u61;
assign and_u3269_u0=and_u3281_u0&not_u737_u0;
assign simplePinWrite_u464=and_u3278_u0&{1{and_u3278_u0}};
assign and_u3270_u0=and_u3279_u0&not_u738_u0;
assign and_u3271_u0=and_u3279_u0&equals;
assign not_u738_u0=~equals;
assign not_u739_u0=~port_3943a37c_;
assign and_u3272_u0=and_u3276_u0&port_3943a37c_;
assign and_u3273_u0=and_u3276_u0&not_u739_u0;
assign simplePinWrite_u465=and_u3274_u0&{1{and_u3274_u0}};
assign and_u3274_u0=and_u3272_u0&and_u3276_u0;
assign or_u1350_u0=reg_5820df61_u0|port_4179d856_;
always @(posedge CLK or posedge RESET)
begin
if (RESET)
reg_5820df61_u0<=1'h0;
else reg_5820df61_u0<=and_u3275_u0;
end
assign and_u3275_u0=and_u3273_u0&and_u3276_u0;
assign and_u3276_u0=and_u3271_u0&and_u3279_u0;
always @(posedge CLK or posedge RESET)
begin
if (RESET)
reg_2f7bd17d_u0<=1'h0;
else reg_2f7bd17d_u0<=and_u3277_u0;
end
assign or_u1351_u0=or_u1350_u0|reg_2f7bd17d_u0;
assign and_u3277_u0=and_u3270_u0&and_u3279_u0;
assign and_u3278_u0=and_u3268_u0&and_u3281_u0;
always @(posedge CLK or posedge RESET)
begin
if (RESET)
and_delayed_u374<=1'h0;
else and_delayed_u374<=and_u3278_u0;
end
assign mux_u1800=(and_u3278_u0)?1'h1:1'h0;
assign or_u1352_u0=and_u3278_u0|and_u3274_u0;
assign or_u1353_u0=and_delayed_u374|or_u1351_u0;
assign and_u3279_u0=and_u3269_u0&and_u3281_u0;
assign and_u3280_u0=and_u3266_u0&and_u3282_u0;
assign or_u1354_u0=or_u1353_u0|and_delayed_u375_u0;
assign and_u3281_u0=and_u3267_u0&and_u3282_u0;
always @(posedge CLK or posedge RESET)
begin
if (RESET)
and_delayed_u375_u0<=1'h0;
else and_delayed_u375_u0<=and_u3280_u0;
end
assign or_u1355_u0=and_u3264_u0|or_u1352_u0;
assign mux_u1801_u0=(and_u3264_u0)?1'h1:mux_u1800;
assign receive_go_merge=simplePinWrite|simplePinWrite_u464;
assign and_u3282_u0=or_u1356_u0&or_u1356_u0;
always @(posedge CLK or posedge syncEnable_u1342)
begin
if (syncEnable_u1342)
loopControl_u88<=1'h0;
else loopControl_u88<=or_u1354_u0;
end
always @(posedge CLK)
begin
if (reg_3a328eba_u0)
syncEnable_u1342<=RESET;
end
assign or_u1356_u0=reg_3a328eba_u0|loopControl_u88;
always @(posedge CLK or posedge RESET)
begin
if (RESET)
reg_3a328eba_u0<=1'h0;
else reg_3a328eba_u0<=reg_2a9903a1_u0;
end
assign or_u1357_u0=GO|or_u1355_u0;
assign mux_u1802_u0=(GO)?1'h0:mux_u1801_u0;
always @(posedge CLK or posedge RESET)
begin
if (RESET)
reg_2a9903a1_u0<=1'h0;
else reg_2a9903a1_u0<=GO;
end
assign RESULT=or_u1357_u0;
assign RESULT_u1966=mux_u1802_u0;
assign RESULT_u1967=simplePinWrite_u465;
assign RESULT_u1968=receive_go_merge;
assign DONE=1'h0;
endmodule



module medianRow3_endianswapper_456cb52c_(endianswapper_456cb52c_in, endianswapper_456cb52c_out);
input	[31:0]	endianswapper_456cb52c_in;
output	[31:0]	endianswapper_456cb52c_out;
assign endianswapper_456cb52c_out=endianswapper_456cb52c_in;
endmodule



module medianRow3_endianswapper_00e33247_(endianswapper_00e33247_in, endianswapper_00e33247_out);
input	[31:0]	endianswapper_00e33247_in;
output	[31:0]	endianswapper_00e33247_out;
assign endianswapper_00e33247_out=endianswapper_00e33247_in;
endmodule



module medianRow3_stateVar_i(bus_09f2d119_, bus_34206eb6_, bus_016c8813_, bus_29eb6397_, bus_4ce557ac_, bus_6fe13ae5_, bus_7e32c356_);
input		bus_09f2d119_;
input		bus_34206eb6_;
input		bus_016c8813_;
input	[31:0]	bus_29eb6397_;
input		bus_4ce557ac_;
input	[31:0]	bus_6fe13ae5_;
output	[31:0]	bus_7e32c356_;
wire	[31:0]	endianswapper_456cb52c_out;
wire	[31:0]	endianswapper_00e33247_out;
wire		or_62ac40c9_u0;
reg	[31:0]	stateVar_i_u43=32'h0;
wire	[31:0]	mux_5d5dfa33_u0;
medianRow3_endianswapper_456cb52c_ medianRow3_endianswapper_456cb52c__1(.endianswapper_456cb52c_in(stateVar_i_u43), 
  .endianswapper_456cb52c_out(endianswapper_456cb52c_out));
medianRow3_endianswapper_00e33247_ medianRow3_endianswapper_00e33247__1(.endianswapper_00e33247_in(mux_5d5dfa33_u0), 
  .endianswapper_00e33247_out(endianswapper_00e33247_out));
assign or_62ac40c9_u0=bus_016c8813_|bus_4ce557ac_;
assign bus_7e32c356_=endianswapper_456cb52c_out;
always @(posedge bus_09f2d119_ or posedge bus_34206eb6_)
begin
if (bus_34206eb6_)
stateVar_i_u43<=32'h0;
else if (or_62ac40c9_u0)
stateVar_i_u43<=endianswapper_00e33247_out;
end
assign mux_5d5dfa33_u0=(bus_016c8813_)?bus_29eb6397_:32'h0;
endmodule



module medianRow3_Kicker_52(CLK, RESET, bus_15dc3a72_);
input		CLK;
input		RESET;
output		bus_15dc3a72_;
reg		kicker_2=1'h0;
wire		bus_5c5aa4ad_;
reg		kicker_1=1'h0;
wire		bus_08e1aca1_;
wire		bus_03d36291_;
wire		bus_0366e3c9_;
reg		kicker_res=1'h0;
assign bus_15dc3a72_=kicker_res;
always @(posedge CLK)
begin
kicker_2<=bus_5c5aa4ad_;
end
assign bus_5c5aa4ad_=bus_08e1aca1_&kicker_1;
always @(posedge CLK)
begin
kicker_1<=bus_08e1aca1_;
end
assign bus_08e1aca1_=~RESET;
assign bus_03d36291_=kicker_1&bus_08e1aca1_&bus_0366e3c9_;
assign bus_0366e3c9_=~kicker_2;
always @(posedge CLK)
begin
kicker_res<=bus_03d36291_;
end
endmodule



module medianRow3_forge_memory_101x32_137(CLK, ENA, WEA, DINA, ENB, ADDRA, ADDRB, DOUTA, DOUTB, DONEA, DONEB);
input		CLK;
input		ENA;
input		WEA;
input	[31:0]	DINA;
input		ENB;
input	[31:0]	ADDRA;
input	[31:0]	ADDRB;
output	[31:0]	DOUTA;
output	[31:0]	DOUTB;
output		DONEA;
output		DONEB;
wire		wea_0;
wire	[31:0]	pre_douta_0;
wire	[31:0]	pre_doutb_0;
wire		wea_1;
wire	[31:0]	pre_douta_1;
wire	[31:0]	pre_doutb_1;
reg		wea_done;
reg		rea_done;
reg	[31:0]	mux_outa;
reg	[31:0]	mux_outb;
assign wea_0=WEA&(ADDRA[31:6]==26'h0);
assign wea_1=WEA&(ADDRA[31:6]==26'h1);
always @(posedge CLK)
begin
wea_done<=WEA;
end
assign DONEA=rea_done|wea_done;
always @(posedge CLK)
begin
end
assign DONEB=ENB;
always @(pre_douta_0 or pre_douta_1 or ADDRA)
begin
case (ADDRA[31:6])26'd0:mux_outa=pre_douta_0;
26'd1:mux_outa=pre_douta_1;
default:mux_outa=32'h0;
endcase end
assign DOUTA=mux_outa;
always @(pre_doutb_0 or pre_doutb_1 or ADDRB)
begin
case (ADDRB[31:6])26'd0:mux_outb=pre_doutb_0;
26'd1:mux_outb=pre_doutb_1;
default:mux_outb=32'h0;
endcase end
assign DOUTB=mux_outb;
// Memory array element: COL: 0, ROW: 0
//  Initialization of Dual Port LUT ram now done through explicit
// parameter setting.
RAM64X1D#(.INIT(64'h0000000000000000))RAM64X1D_instance_2688(.WCLK(CLK), .WE(wea_0), 
  .A0(ADDRA[0]), .A1(ADDRA[1]), .A2(ADDRA[2]), .A3(ADDRA[3]), .A4(ADDRA[4]), .A5(ADDRA[5]), 
  .D(DINA[0]), .SPO(pre_douta_0[0]), .DPRA0(ADDRB[0]), .DPRA1(ADDRB[1]), .DPRA2(ADDRB[2]), 
  .DPRA3(ADDRB[3]), .DPRA4(ADDRB[4]), .DPRA5(ADDRB[5]), .DPO(pre_doutb_0[0]));
// Memory array element: COL: 0, ROW: 1
//  Initialization of Dual Port LUT ram now done through explicit
// parameter setting.
RAM64X1D#(.INIT(64'h0000000000000000))RAM64X1D_instance_2689(.WCLK(CLK), .WE(wea_0), 
  .A0(ADDRA[0]), .A1(ADDRA[1]), .A2(ADDRA[2]), .A3(ADDRA[3]), .A4(ADDRA[4]), .A5(ADDRA[5]), 
  .D(DINA[1]), .SPO(pre_douta_0[1]), .DPRA0(ADDRB[0]), .DPRA1(ADDRB[1]), .DPRA2(ADDRB[2]), 
  .DPRA3(ADDRB[3]), .DPRA4(ADDRB[4]), .DPRA5(ADDRB[5]), .DPO(pre_doutb_0[1]));
// Memory array element: COL: 0, ROW: 2
//  Initialization of Dual Port LUT ram now done through explicit
// parameter setting.
RAM64X1D#(.INIT(64'h0000000000000000))RAM64X1D_instance_2690(.WCLK(CLK), .WE(wea_0), 
  .A0(ADDRA[0]), .A1(ADDRA[1]), .A2(ADDRA[2]), .A3(ADDRA[3]), .A4(ADDRA[4]), .A5(ADDRA[5]), 
  .D(DINA[2]), .SPO(pre_douta_0[2]), .DPRA0(ADDRB[0]), .DPRA1(ADDRB[1]), .DPRA2(ADDRB[2]), 
  .DPRA3(ADDRB[3]), .DPRA4(ADDRB[4]), .DPRA5(ADDRB[5]), .DPO(pre_doutb_0[2]));
// Memory array element: COL: 0, ROW: 3
//  Initialization of Dual Port LUT ram now done through explicit
// parameter setting.
RAM64X1D#(.INIT(64'h0000000000000000))RAM64X1D_instance_2691(.WCLK(CLK), .WE(wea_0), 
  .A0(ADDRA[0]), .A1(ADDRA[1]), .A2(ADDRA[2]), .A3(ADDRA[3]), .A4(ADDRA[4]), .A5(ADDRA[5]), 
  .D(DINA[3]), .SPO(pre_douta_0[3]), .DPRA0(ADDRB[0]), .DPRA1(ADDRB[1]), .DPRA2(ADDRB[2]), 
  .DPRA3(ADDRB[3]), .DPRA4(ADDRB[4]), .DPRA5(ADDRB[5]), .DPO(pre_doutb_0[3]));
// Memory array element: COL: 0, ROW: 4
//  Initialization of Dual Port LUT ram now done through explicit
// parameter setting.
RAM64X1D#(.INIT(64'h0000000000000000))RAM64X1D_instance_2692(.WCLK(CLK), .WE(wea_0), 
  .A0(ADDRA[0]), .A1(ADDRA[1]), .A2(ADDRA[2]), .A3(ADDRA[3]), .A4(ADDRA[4]), .A5(ADDRA[5]), 
  .D(DINA[4]), .SPO(pre_douta_0[4]), .DPRA0(ADDRB[0]), .DPRA1(ADDRB[1]), .DPRA2(ADDRB[2]), 
  .DPRA3(ADDRB[3]), .DPRA4(ADDRB[4]), .DPRA5(ADDRB[5]), .DPO(pre_doutb_0[4]));
// Memory array element: COL: 0, ROW: 5
//  Initialization of Dual Port LUT ram now done through explicit
// parameter setting.
RAM64X1D#(.INIT(64'h0000000000000000))RAM64X1D_instance_2693(.WCLK(CLK), .WE(wea_0), 
  .A0(ADDRA[0]), .A1(ADDRA[1]), .A2(ADDRA[2]), .A3(ADDRA[3]), .A4(ADDRA[4]), .A5(ADDRA[5]), 
  .D(DINA[5]), .SPO(pre_douta_0[5]), .DPRA0(ADDRB[0]), .DPRA1(ADDRB[1]), .DPRA2(ADDRB[2]), 
  .DPRA3(ADDRB[3]), .DPRA4(ADDRB[4]), .DPRA5(ADDRB[5]), .DPO(pre_doutb_0[5]));
// Memory array element: COL: 0, ROW: 6
//  Initialization of Dual Port LUT ram now done through explicit
// parameter setting.
RAM64X1D#(.INIT(64'h0000000000000000))RAM64X1D_instance_2694(.WCLK(CLK), .WE(wea_0), 
  .A0(ADDRA[0]), .A1(ADDRA[1]), .A2(ADDRA[2]), .A3(ADDRA[3]), .A4(ADDRA[4]), .A5(ADDRA[5]), 
  .D(DINA[6]), .SPO(pre_douta_0[6]), .DPRA0(ADDRB[0]), .DPRA1(ADDRB[1]), .DPRA2(ADDRB[2]), 
  .DPRA3(ADDRB[3]), .DPRA4(ADDRB[4]), .DPRA5(ADDRB[5]), .DPO(pre_doutb_0[6]));
// Memory array element: COL: 0, ROW: 7
//  Initialization of Dual Port LUT ram now done through explicit
// parameter setting.
RAM64X1D#(.INIT(64'h0000000000000000))RAM64X1D_instance_2695(.WCLK(CLK), .WE(wea_0), 
  .A0(ADDRA[0]), .A1(ADDRA[1]), .A2(ADDRA[2]), .A3(ADDRA[3]), .A4(ADDRA[4]), .A5(ADDRA[5]), 
  .D(DINA[7]), .SPO(pre_douta_0[7]), .DPRA0(ADDRB[0]), .DPRA1(ADDRB[1]), .DPRA2(ADDRB[2]), 
  .DPRA3(ADDRB[3]), .DPRA4(ADDRB[4]), .DPRA5(ADDRB[5]), .DPO(pre_doutb_0[7]));
// Memory array element: COL: 0, ROW: 8
//  Initialization of Dual Port LUT ram now done through explicit
// parameter setting.
RAM64X1D#(.INIT(64'h0000000000000000))RAM64X1D_instance_2696(.WCLK(CLK), .WE(wea_0), 
  .A0(ADDRA[0]), .A1(ADDRA[1]), .A2(ADDRA[2]), .A3(ADDRA[3]), .A4(ADDRA[4]), .A5(ADDRA[5]), 
  .D(DINA[8]), .SPO(pre_douta_0[8]), .DPRA0(ADDRB[0]), .DPRA1(ADDRB[1]), .DPRA2(ADDRB[2]), 
  .DPRA3(ADDRB[3]), .DPRA4(ADDRB[4]), .DPRA5(ADDRB[5]), .DPO(pre_doutb_0[8]));
// Memory array element: COL: 0, ROW: 9
//  Initialization of Dual Port LUT ram now done through explicit
// parameter setting.
RAM64X1D#(.INIT(64'h0000000000000000))RAM64X1D_instance_2697(.WCLK(CLK), .WE(wea_0), 
  .A0(ADDRA[0]), .A1(ADDRA[1]), .A2(ADDRA[2]), .A3(ADDRA[3]), .A4(ADDRA[4]), .A5(ADDRA[5]), 
  .D(DINA[9]), .SPO(pre_douta_0[9]), .DPRA0(ADDRB[0]), .DPRA1(ADDRB[1]), .DPRA2(ADDRB[2]), 
  .DPRA3(ADDRB[3]), .DPRA4(ADDRB[4]), .DPRA5(ADDRB[5]), .DPO(pre_doutb_0[9]));
// Memory array element: COL: 0, ROW: 10
//  Initialization of Dual Port LUT ram now done through explicit
// parameter setting.
RAM64X1D#(.INIT(64'h0000000000000000))RAM64X1D_instance_2698(.WCLK(CLK), .WE(wea_0), 
  .A0(ADDRA[0]), .A1(ADDRA[1]), .A2(ADDRA[2]), .A3(ADDRA[3]), .A4(ADDRA[4]), .A5(ADDRA[5]), 
  .D(DINA[10]), .SPO(pre_douta_0[10]), .DPRA0(ADDRB[0]), .DPRA1(ADDRB[1]), .DPRA2(ADDRB[2]), 
  .DPRA3(ADDRB[3]), .DPRA4(ADDRB[4]), .DPRA5(ADDRB[5]), .DPO(pre_doutb_0[10]));
// Memory array element: COL: 0, ROW: 11
//  Initialization of Dual Port LUT ram now done through explicit
// parameter setting.
RAM64X1D#(.INIT(64'h0000000000000000))RAM64X1D_instance_2699(.WCLK(CLK), .WE(wea_0), 
  .A0(ADDRA[0]), .A1(ADDRA[1]), .A2(ADDRA[2]), .A3(ADDRA[3]), .A4(ADDRA[4]), .A5(ADDRA[5]), 
  .D(DINA[11]), .SPO(pre_douta_0[11]), .DPRA0(ADDRB[0]), .DPRA1(ADDRB[1]), .DPRA2(ADDRB[2]), 
  .DPRA3(ADDRB[3]), .DPRA4(ADDRB[4]), .DPRA5(ADDRB[5]), .DPO(pre_doutb_0[11]));
// Memory array element: COL: 0, ROW: 12
//  Initialization of Dual Port LUT ram now done through explicit
// parameter setting.
RAM64X1D#(.INIT(64'h0000000000000000))RAM64X1D_instance_2700(.WCLK(CLK), .WE(wea_0), 
  .A0(ADDRA[0]), .A1(ADDRA[1]), .A2(ADDRA[2]), .A3(ADDRA[3]), .A4(ADDRA[4]), .A5(ADDRA[5]), 
  .D(DINA[12]), .SPO(pre_douta_0[12]), .DPRA0(ADDRB[0]), .DPRA1(ADDRB[1]), .DPRA2(ADDRB[2]), 
  .DPRA3(ADDRB[3]), .DPRA4(ADDRB[4]), .DPRA5(ADDRB[5]), .DPO(pre_doutb_0[12]));
// Memory array element: COL: 0, ROW: 13
//  Initialization of Dual Port LUT ram now done through explicit
// parameter setting.
RAM64X1D#(.INIT(64'h0000000000000000))RAM64X1D_instance_2701(.WCLK(CLK), .WE(wea_0), 
  .A0(ADDRA[0]), .A1(ADDRA[1]), .A2(ADDRA[2]), .A3(ADDRA[3]), .A4(ADDRA[4]), .A5(ADDRA[5]), 
  .D(DINA[13]), .SPO(pre_douta_0[13]), .DPRA0(ADDRB[0]), .DPRA1(ADDRB[1]), .DPRA2(ADDRB[2]), 
  .DPRA3(ADDRB[3]), .DPRA4(ADDRB[4]), .DPRA5(ADDRB[5]), .DPO(pre_doutb_0[13]));
// Memory array element: COL: 0, ROW: 14
//  Initialization of Dual Port LUT ram now done through explicit
// parameter setting.
RAM64X1D#(.INIT(64'h0000000000000000))RAM64X1D_instance_2702(.WCLK(CLK), .WE(wea_0), 
  .A0(ADDRA[0]), .A1(ADDRA[1]), .A2(ADDRA[2]), .A3(ADDRA[3]), .A4(ADDRA[4]), .A5(ADDRA[5]), 
  .D(DINA[14]), .SPO(pre_douta_0[14]), .DPRA0(ADDRB[0]), .DPRA1(ADDRB[1]), .DPRA2(ADDRB[2]), 
  .DPRA3(ADDRB[3]), .DPRA4(ADDRB[4]), .DPRA5(ADDRB[5]), .DPO(pre_doutb_0[14]));
// Memory array element: COL: 0, ROW: 15
//  Initialization of Dual Port LUT ram now done through explicit
// parameter setting.
RAM64X1D#(.INIT(64'h0000000000000000))RAM64X1D_instance_2703(.WCLK(CLK), .WE(wea_0), 
  .A0(ADDRA[0]), .A1(ADDRA[1]), .A2(ADDRA[2]), .A3(ADDRA[3]), .A4(ADDRA[4]), .A5(ADDRA[5]), 
  .D(DINA[15]), .SPO(pre_douta_0[15]), .DPRA0(ADDRB[0]), .DPRA1(ADDRB[1]), .DPRA2(ADDRB[2]), 
  .DPRA3(ADDRB[3]), .DPRA4(ADDRB[4]), .DPRA5(ADDRB[5]), .DPO(pre_doutb_0[15]));
// Memory array element: COL: 0, ROW: 16
//  Initialization of Dual Port LUT ram now done through explicit
// parameter setting.
RAM64X1D#(.INIT(64'h0000000000000000))RAM64X1D_instance_2704(.WCLK(CLK), .WE(wea_0), 
  .A0(ADDRA[0]), .A1(ADDRA[1]), .A2(ADDRA[2]), .A3(ADDRA[3]), .A4(ADDRA[4]), .A5(ADDRA[5]), 
  .D(DINA[16]), .SPO(pre_douta_0[16]), .DPRA0(ADDRB[0]), .DPRA1(ADDRB[1]), .DPRA2(ADDRB[2]), 
  .DPRA3(ADDRB[3]), .DPRA4(ADDRB[4]), .DPRA5(ADDRB[5]), .DPO(pre_doutb_0[16]));
// Memory array element: COL: 0, ROW: 17
//  Initialization of Dual Port LUT ram now done through explicit
// parameter setting.
RAM64X1D#(.INIT(64'h0000000000000000))RAM64X1D_instance_2705(.WCLK(CLK), .WE(wea_0), 
  .A0(ADDRA[0]), .A1(ADDRA[1]), .A2(ADDRA[2]), .A3(ADDRA[3]), .A4(ADDRA[4]), .A5(ADDRA[5]), 
  .D(DINA[17]), .SPO(pre_douta_0[17]), .DPRA0(ADDRB[0]), .DPRA1(ADDRB[1]), .DPRA2(ADDRB[2]), 
  .DPRA3(ADDRB[3]), .DPRA4(ADDRB[4]), .DPRA5(ADDRB[5]), .DPO(pre_doutb_0[17]));
// Memory array element: COL: 0, ROW: 18
//  Initialization of Dual Port LUT ram now done through explicit
// parameter setting.
RAM64X1D#(.INIT(64'h0000000000000000))RAM64X1D_instance_2706(.WCLK(CLK), .WE(wea_0), 
  .A0(ADDRA[0]), .A1(ADDRA[1]), .A2(ADDRA[2]), .A3(ADDRA[3]), .A4(ADDRA[4]), .A5(ADDRA[5]), 
  .D(DINA[18]), .SPO(pre_douta_0[18]), .DPRA0(ADDRB[0]), .DPRA1(ADDRB[1]), .DPRA2(ADDRB[2]), 
  .DPRA3(ADDRB[3]), .DPRA4(ADDRB[4]), .DPRA5(ADDRB[5]), .DPO(pre_doutb_0[18]));
// Memory array element: COL: 0, ROW: 19
//  Initialization of Dual Port LUT ram now done through explicit
// parameter setting.
RAM64X1D#(.INIT(64'h0000000000000000))RAM64X1D_instance_2707(.WCLK(CLK), .WE(wea_0), 
  .A0(ADDRA[0]), .A1(ADDRA[1]), .A2(ADDRA[2]), .A3(ADDRA[3]), .A4(ADDRA[4]), .A5(ADDRA[5]), 
  .D(DINA[19]), .SPO(pre_douta_0[19]), .DPRA0(ADDRB[0]), .DPRA1(ADDRB[1]), .DPRA2(ADDRB[2]), 
  .DPRA3(ADDRB[3]), .DPRA4(ADDRB[4]), .DPRA5(ADDRB[5]), .DPO(pre_doutb_0[19]));
// Memory array element: COL: 0, ROW: 20
//  Initialization of Dual Port LUT ram now done through explicit
// parameter setting.
RAM64X1D#(.INIT(64'h0000000000000000))RAM64X1D_instance_2708(.WCLK(CLK), .WE(wea_0), 
  .A0(ADDRA[0]), .A1(ADDRA[1]), .A2(ADDRA[2]), .A3(ADDRA[3]), .A4(ADDRA[4]), .A5(ADDRA[5]), 
  .D(DINA[20]), .SPO(pre_douta_0[20]), .DPRA0(ADDRB[0]), .DPRA1(ADDRB[1]), .DPRA2(ADDRB[2]), 
  .DPRA3(ADDRB[3]), .DPRA4(ADDRB[4]), .DPRA5(ADDRB[5]), .DPO(pre_doutb_0[20]));
// Memory array element: COL: 0, ROW: 21
//  Initialization of Dual Port LUT ram now done through explicit
// parameter setting.
RAM64X1D#(.INIT(64'h0000000000000000))RAM64X1D_instance_2709(.WCLK(CLK), .WE(wea_0), 
  .A0(ADDRA[0]), .A1(ADDRA[1]), .A2(ADDRA[2]), .A3(ADDRA[3]), .A4(ADDRA[4]), .A5(ADDRA[5]), 
  .D(DINA[21]), .SPO(pre_douta_0[21]), .DPRA0(ADDRB[0]), .DPRA1(ADDRB[1]), .DPRA2(ADDRB[2]), 
  .DPRA3(ADDRB[3]), .DPRA4(ADDRB[4]), .DPRA5(ADDRB[5]), .DPO(pre_doutb_0[21]));
// Memory array element: COL: 0, ROW: 22
//  Initialization of Dual Port LUT ram now done through explicit
// parameter setting.
RAM64X1D#(.INIT(64'h0000000000000000))RAM64X1D_instance_2710(.WCLK(CLK), .WE(wea_0), 
  .A0(ADDRA[0]), .A1(ADDRA[1]), .A2(ADDRA[2]), .A3(ADDRA[3]), .A4(ADDRA[4]), .A5(ADDRA[5]), 
  .D(DINA[22]), .SPO(pre_douta_0[22]), .DPRA0(ADDRB[0]), .DPRA1(ADDRB[1]), .DPRA2(ADDRB[2]), 
  .DPRA3(ADDRB[3]), .DPRA4(ADDRB[4]), .DPRA5(ADDRB[5]), .DPO(pre_doutb_0[22]));
// Memory array element: COL: 0, ROW: 23
//  Initialization of Dual Port LUT ram now done through explicit
// parameter setting.
RAM64X1D#(.INIT(64'h0000000000000000))RAM64X1D_instance_2711(.WCLK(CLK), .WE(wea_0), 
  .A0(ADDRA[0]), .A1(ADDRA[1]), .A2(ADDRA[2]), .A3(ADDRA[3]), .A4(ADDRA[4]), .A5(ADDRA[5]), 
  .D(DINA[23]), .SPO(pre_douta_0[23]), .DPRA0(ADDRB[0]), .DPRA1(ADDRB[1]), .DPRA2(ADDRB[2]), 
  .DPRA3(ADDRB[3]), .DPRA4(ADDRB[4]), .DPRA5(ADDRB[5]), .DPO(pre_doutb_0[23]));
// Memory array element: COL: 0, ROW: 24
//  Initialization of Dual Port LUT ram now done through explicit
// parameter setting.
RAM64X1D#(.INIT(64'h0000000000000000))RAM64X1D_instance_2712(.WCLK(CLK), .WE(wea_0), 
  .A0(ADDRA[0]), .A1(ADDRA[1]), .A2(ADDRA[2]), .A3(ADDRA[3]), .A4(ADDRA[4]), .A5(ADDRA[5]), 
  .D(DINA[24]), .SPO(pre_douta_0[24]), .DPRA0(ADDRB[0]), .DPRA1(ADDRB[1]), .DPRA2(ADDRB[2]), 
  .DPRA3(ADDRB[3]), .DPRA4(ADDRB[4]), .DPRA5(ADDRB[5]), .DPO(pre_doutb_0[24]));
// Memory array element: COL: 0, ROW: 25
//  Initialization of Dual Port LUT ram now done through explicit
// parameter setting.
RAM64X1D#(.INIT(64'h0000000000000000))RAM64X1D_instance_2713(.WCLK(CLK), .WE(wea_0), 
  .A0(ADDRA[0]), .A1(ADDRA[1]), .A2(ADDRA[2]), .A3(ADDRA[3]), .A4(ADDRA[4]), .A5(ADDRA[5]), 
  .D(DINA[25]), .SPO(pre_douta_0[25]), .DPRA0(ADDRB[0]), .DPRA1(ADDRB[1]), .DPRA2(ADDRB[2]), 
  .DPRA3(ADDRB[3]), .DPRA4(ADDRB[4]), .DPRA5(ADDRB[5]), .DPO(pre_doutb_0[25]));
// Memory array element: COL: 0, ROW: 26
//  Initialization of Dual Port LUT ram now done through explicit
// parameter setting.
RAM64X1D#(.INIT(64'h0000000000000000))RAM64X1D_instance_2714(.WCLK(CLK), .WE(wea_0), 
  .A0(ADDRA[0]), .A1(ADDRA[1]), .A2(ADDRA[2]), .A3(ADDRA[3]), .A4(ADDRA[4]), .A5(ADDRA[5]), 
  .D(DINA[26]), .SPO(pre_douta_0[26]), .DPRA0(ADDRB[0]), .DPRA1(ADDRB[1]), .DPRA2(ADDRB[2]), 
  .DPRA3(ADDRB[3]), .DPRA4(ADDRB[4]), .DPRA5(ADDRB[5]), .DPO(pre_doutb_0[26]));
// Memory array element: COL: 0, ROW: 27
//  Initialization of Dual Port LUT ram now done through explicit
// parameter setting.
RAM64X1D#(.INIT(64'h0000000000000000))RAM64X1D_instance_2715(.WCLK(CLK), .WE(wea_0), 
  .A0(ADDRA[0]), .A1(ADDRA[1]), .A2(ADDRA[2]), .A3(ADDRA[3]), .A4(ADDRA[4]), .A5(ADDRA[5]), 
  .D(DINA[27]), .SPO(pre_douta_0[27]), .DPRA0(ADDRB[0]), .DPRA1(ADDRB[1]), .DPRA2(ADDRB[2]), 
  .DPRA3(ADDRB[3]), .DPRA4(ADDRB[4]), .DPRA5(ADDRB[5]), .DPO(pre_doutb_0[27]));
// Memory array element: COL: 0, ROW: 28
//  Initialization of Dual Port LUT ram now done through explicit
// parameter setting.
RAM64X1D#(.INIT(64'h0000000000000000))RAM64X1D_instance_2716(.WCLK(CLK), .WE(wea_0), 
  .A0(ADDRA[0]), .A1(ADDRA[1]), .A2(ADDRA[2]), .A3(ADDRA[3]), .A4(ADDRA[4]), .A5(ADDRA[5]), 
  .D(DINA[28]), .SPO(pre_douta_0[28]), .DPRA0(ADDRB[0]), .DPRA1(ADDRB[1]), .DPRA2(ADDRB[2]), 
  .DPRA3(ADDRB[3]), .DPRA4(ADDRB[4]), .DPRA5(ADDRB[5]), .DPO(pre_doutb_0[28]));
// Memory array element: COL: 0, ROW: 29
//  Initialization of Dual Port LUT ram now done through explicit
// parameter setting.
RAM64X1D#(.INIT(64'h0000000000000000))RAM64X1D_instance_2717(.WCLK(CLK), .WE(wea_0), 
  .A0(ADDRA[0]), .A1(ADDRA[1]), .A2(ADDRA[2]), .A3(ADDRA[3]), .A4(ADDRA[4]), .A5(ADDRA[5]), 
  .D(DINA[29]), .SPO(pre_douta_0[29]), .DPRA0(ADDRB[0]), .DPRA1(ADDRB[1]), .DPRA2(ADDRB[2]), 
  .DPRA3(ADDRB[3]), .DPRA4(ADDRB[4]), .DPRA5(ADDRB[5]), .DPO(pre_doutb_0[29]));
// Memory array element: COL: 0, ROW: 30
//  Initialization of Dual Port LUT ram now done through explicit
// parameter setting.
RAM64X1D#(.INIT(64'h0000000000000000))RAM64X1D_instance_2718(.WCLK(CLK), .WE(wea_0), 
  .A0(ADDRA[0]), .A1(ADDRA[1]), .A2(ADDRA[2]), .A3(ADDRA[3]), .A4(ADDRA[4]), .A5(ADDRA[5]), 
  .D(DINA[30]), .SPO(pre_douta_0[30]), .DPRA0(ADDRB[0]), .DPRA1(ADDRB[1]), .DPRA2(ADDRB[2]), 
  .DPRA3(ADDRB[3]), .DPRA4(ADDRB[4]), .DPRA5(ADDRB[5]), .DPO(pre_doutb_0[30]));
// Memory array element: COL: 0, ROW: 31
//  Initialization of Dual Port LUT ram now done through explicit
// parameter setting.
RAM64X1D#(.INIT(64'h0000000000000000))RAM64X1D_instance_2719(.WCLK(CLK), .WE(wea_0), 
  .A0(ADDRA[0]), .A1(ADDRA[1]), .A2(ADDRA[2]), .A3(ADDRA[3]), .A4(ADDRA[4]), .A5(ADDRA[5]), 
  .D(DINA[31]), .SPO(pre_douta_0[31]), .DPRA0(ADDRB[0]), .DPRA1(ADDRB[1]), .DPRA2(ADDRB[2]), 
  .DPRA3(ADDRB[3]), .DPRA4(ADDRB[4]), .DPRA5(ADDRB[5]), .DPO(pre_doutb_0[31]));
// Memory array element: COL: 1, ROW: 0
//  Initialization of Dual Port LUT ram now done through explicit
// parameter setting.
RAM64X1D#(.INIT(64'h0000000000000000))RAM64X1D_instance_2720(.WCLK(CLK), .WE(wea_1), 
  .A0(ADDRA[0]), .A1(ADDRA[1]), .A2(ADDRA[2]), .A3(ADDRA[3]), .A4(ADDRA[4]), .A5(ADDRA[5]), 
  .D(DINA[0]), .SPO(pre_douta_1[0]), .DPRA0(ADDRB[0]), .DPRA1(ADDRB[1]), .DPRA2(ADDRB[2]), 
  .DPRA3(ADDRB[3]), .DPRA4(ADDRB[4]), .DPRA5(ADDRB[5]), .DPO(pre_doutb_1[0]));
// Memory array element: COL: 1, ROW: 1
//  Initialization of Dual Port LUT ram now done through explicit
// parameter setting.
RAM64X1D#(.INIT(64'h0000000000000000))RAM64X1D_instance_2721(.WCLK(CLK), .WE(wea_1), 
  .A0(ADDRA[0]), .A1(ADDRA[1]), .A2(ADDRA[2]), .A3(ADDRA[3]), .A4(ADDRA[4]), .A5(ADDRA[5]), 
  .D(DINA[1]), .SPO(pre_douta_1[1]), .DPRA0(ADDRB[0]), .DPRA1(ADDRB[1]), .DPRA2(ADDRB[2]), 
  .DPRA3(ADDRB[3]), .DPRA4(ADDRB[4]), .DPRA5(ADDRB[5]), .DPO(pre_doutb_1[1]));
// Memory array element: COL: 1, ROW: 2
//  Initialization of Dual Port LUT ram now done through explicit
// parameter setting.
RAM64X1D#(.INIT(64'h0000000000000000))RAM64X1D_instance_2722(.WCLK(CLK), .WE(wea_1), 
  .A0(ADDRA[0]), .A1(ADDRA[1]), .A2(ADDRA[2]), .A3(ADDRA[3]), .A4(ADDRA[4]), .A5(ADDRA[5]), 
  .D(DINA[2]), .SPO(pre_douta_1[2]), .DPRA0(ADDRB[0]), .DPRA1(ADDRB[1]), .DPRA2(ADDRB[2]), 
  .DPRA3(ADDRB[3]), .DPRA4(ADDRB[4]), .DPRA5(ADDRB[5]), .DPO(pre_doutb_1[2]));
// Memory array element: COL: 1, ROW: 3
//  Initialization of Dual Port LUT ram now done through explicit
// parameter setting.
RAM64X1D#(.INIT(64'h0000000000000000))RAM64X1D_instance_2723(.WCLK(CLK), .WE(wea_1), 
  .A0(ADDRA[0]), .A1(ADDRA[1]), .A2(ADDRA[2]), .A3(ADDRA[3]), .A4(ADDRA[4]), .A5(ADDRA[5]), 
  .D(DINA[3]), .SPO(pre_douta_1[3]), .DPRA0(ADDRB[0]), .DPRA1(ADDRB[1]), .DPRA2(ADDRB[2]), 
  .DPRA3(ADDRB[3]), .DPRA4(ADDRB[4]), .DPRA5(ADDRB[5]), .DPO(pre_doutb_1[3]));
// Memory array element: COL: 1, ROW: 4
//  Initialization of Dual Port LUT ram now done through explicit
// parameter setting.
RAM64X1D#(.INIT(64'h0000000000000000))RAM64X1D_instance_2724(.WCLK(CLK), .WE(wea_1), 
  .A0(ADDRA[0]), .A1(ADDRA[1]), .A2(ADDRA[2]), .A3(ADDRA[3]), .A4(ADDRA[4]), .A5(ADDRA[5]), 
  .D(DINA[4]), .SPO(pre_douta_1[4]), .DPRA0(ADDRB[0]), .DPRA1(ADDRB[1]), .DPRA2(ADDRB[2]), 
  .DPRA3(ADDRB[3]), .DPRA4(ADDRB[4]), .DPRA5(ADDRB[5]), .DPO(pre_doutb_1[4]));
// Memory array element: COL: 1, ROW: 5
//  Initialization of Dual Port LUT ram now done through explicit
// parameter setting.
RAM64X1D#(.INIT(64'h0000000000000000))RAM64X1D_instance_2725(.WCLK(CLK), .WE(wea_1), 
  .A0(ADDRA[0]), .A1(ADDRA[1]), .A2(ADDRA[2]), .A3(ADDRA[3]), .A4(ADDRA[4]), .A5(ADDRA[5]), 
  .D(DINA[5]), .SPO(pre_douta_1[5]), .DPRA0(ADDRB[0]), .DPRA1(ADDRB[1]), .DPRA2(ADDRB[2]), 
  .DPRA3(ADDRB[3]), .DPRA4(ADDRB[4]), .DPRA5(ADDRB[5]), .DPO(pre_doutb_1[5]));
// Memory array element: COL: 1, ROW: 6
//  Initialization of Dual Port LUT ram now done through explicit
// parameter setting.
RAM64X1D#(.INIT(64'h0000000000000000))RAM64X1D_instance_2726(.WCLK(CLK), .WE(wea_1), 
  .A0(ADDRA[0]), .A1(ADDRA[1]), .A2(ADDRA[2]), .A3(ADDRA[3]), .A4(ADDRA[4]), .A5(ADDRA[5]), 
  .D(DINA[6]), .SPO(pre_douta_1[6]), .DPRA0(ADDRB[0]), .DPRA1(ADDRB[1]), .DPRA2(ADDRB[2]), 
  .DPRA3(ADDRB[3]), .DPRA4(ADDRB[4]), .DPRA5(ADDRB[5]), .DPO(pre_doutb_1[6]));
// Memory array element: COL: 1, ROW: 7
//  Initialization of Dual Port LUT ram now done through explicit
// parameter setting.
RAM64X1D#(.INIT(64'h0000000000000000))RAM64X1D_instance_2727(.WCLK(CLK), .WE(wea_1), 
  .A0(ADDRA[0]), .A1(ADDRA[1]), .A2(ADDRA[2]), .A3(ADDRA[3]), .A4(ADDRA[4]), .A5(ADDRA[5]), 
  .D(DINA[7]), .SPO(pre_douta_1[7]), .DPRA0(ADDRB[0]), .DPRA1(ADDRB[1]), .DPRA2(ADDRB[2]), 
  .DPRA3(ADDRB[3]), .DPRA4(ADDRB[4]), .DPRA5(ADDRB[5]), .DPO(pre_doutb_1[7]));
// Memory array element: COL: 1, ROW: 8
//  Initialization of Dual Port LUT ram now done through explicit
// parameter setting.
RAM64X1D#(.INIT(64'h0000000000000000))RAM64X1D_instance_2728(.WCLK(CLK), .WE(wea_1), 
  .A0(ADDRA[0]), .A1(ADDRA[1]), .A2(ADDRA[2]), .A3(ADDRA[3]), .A4(ADDRA[4]), .A5(ADDRA[5]), 
  .D(DINA[8]), .SPO(pre_douta_1[8]), .DPRA0(ADDRB[0]), .DPRA1(ADDRB[1]), .DPRA2(ADDRB[2]), 
  .DPRA3(ADDRB[3]), .DPRA4(ADDRB[4]), .DPRA5(ADDRB[5]), .DPO(pre_doutb_1[8]));
// Memory array element: COL: 1, ROW: 9
//  Initialization of Dual Port LUT ram now done through explicit
// parameter setting.
RAM64X1D#(.INIT(64'h0000000000000000))RAM64X1D_instance_2729(.WCLK(CLK), .WE(wea_1), 
  .A0(ADDRA[0]), .A1(ADDRA[1]), .A2(ADDRA[2]), .A3(ADDRA[3]), .A4(ADDRA[4]), .A5(ADDRA[5]), 
  .D(DINA[9]), .SPO(pre_douta_1[9]), .DPRA0(ADDRB[0]), .DPRA1(ADDRB[1]), .DPRA2(ADDRB[2]), 
  .DPRA3(ADDRB[3]), .DPRA4(ADDRB[4]), .DPRA5(ADDRB[5]), .DPO(pre_doutb_1[9]));
// Memory array element: COL: 1, ROW: 10
//  Initialization of Dual Port LUT ram now done through explicit
// parameter setting.
RAM64X1D#(.INIT(64'h0000000000000000))RAM64X1D_instance_2730(.WCLK(CLK), .WE(wea_1), 
  .A0(ADDRA[0]), .A1(ADDRA[1]), .A2(ADDRA[2]), .A3(ADDRA[3]), .A4(ADDRA[4]), .A5(ADDRA[5]), 
  .D(DINA[10]), .SPO(pre_douta_1[10]), .DPRA0(ADDRB[0]), .DPRA1(ADDRB[1]), .DPRA2(ADDRB[2]), 
  .DPRA3(ADDRB[3]), .DPRA4(ADDRB[4]), .DPRA5(ADDRB[5]), .DPO(pre_doutb_1[10]));
// Memory array element: COL: 1, ROW: 11
//  Initialization of Dual Port LUT ram now done through explicit
// parameter setting.
RAM64X1D#(.INIT(64'h0000000000000000))RAM64X1D_instance_2731(.WCLK(CLK), .WE(wea_1), 
  .A0(ADDRA[0]), .A1(ADDRA[1]), .A2(ADDRA[2]), .A3(ADDRA[3]), .A4(ADDRA[4]), .A5(ADDRA[5]), 
  .D(DINA[11]), .SPO(pre_douta_1[11]), .DPRA0(ADDRB[0]), .DPRA1(ADDRB[1]), .DPRA2(ADDRB[2]), 
  .DPRA3(ADDRB[3]), .DPRA4(ADDRB[4]), .DPRA5(ADDRB[5]), .DPO(pre_doutb_1[11]));
// Memory array element: COL: 1, ROW: 12
//  Initialization of Dual Port LUT ram now done through explicit
// parameter setting.
RAM64X1D#(.INIT(64'h0000000000000000))RAM64X1D_instance_2732(.WCLK(CLK), .WE(wea_1), 
  .A0(ADDRA[0]), .A1(ADDRA[1]), .A2(ADDRA[2]), .A3(ADDRA[3]), .A4(ADDRA[4]), .A5(ADDRA[5]), 
  .D(DINA[12]), .SPO(pre_douta_1[12]), .DPRA0(ADDRB[0]), .DPRA1(ADDRB[1]), .DPRA2(ADDRB[2]), 
  .DPRA3(ADDRB[3]), .DPRA4(ADDRB[4]), .DPRA5(ADDRB[5]), .DPO(pre_doutb_1[12]));
// Memory array element: COL: 1, ROW: 13
//  Initialization of Dual Port LUT ram now done through explicit
// parameter setting.
RAM64X1D#(.INIT(64'h0000000000000000))RAM64X1D_instance_2733(.WCLK(CLK), .WE(wea_1), 
  .A0(ADDRA[0]), .A1(ADDRA[1]), .A2(ADDRA[2]), .A3(ADDRA[3]), .A4(ADDRA[4]), .A5(ADDRA[5]), 
  .D(DINA[13]), .SPO(pre_douta_1[13]), .DPRA0(ADDRB[0]), .DPRA1(ADDRB[1]), .DPRA2(ADDRB[2]), 
  .DPRA3(ADDRB[3]), .DPRA4(ADDRB[4]), .DPRA5(ADDRB[5]), .DPO(pre_doutb_1[13]));
// Memory array element: COL: 1, ROW: 14
//  Initialization of Dual Port LUT ram now done through explicit
// parameter setting.
RAM64X1D#(.INIT(64'h0000000000000000))RAM64X1D_instance_2734(.WCLK(CLK), .WE(wea_1), 
  .A0(ADDRA[0]), .A1(ADDRA[1]), .A2(ADDRA[2]), .A3(ADDRA[3]), .A4(ADDRA[4]), .A5(ADDRA[5]), 
  .D(DINA[14]), .SPO(pre_douta_1[14]), .DPRA0(ADDRB[0]), .DPRA1(ADDRB[1]), .DPRA2(ADDRB[2]), 
  .DPRA3(ADDRB[3]), .DPRA4(ADDRB[4]), .DPRA5(ADDRB[5]), .DPO(pre_doutb_1[14]));
// Memory array element: COL: 1, ROW: 15
//  Initialization of Dual Port LUT ram now done through explicit
// parameter setting.
RAM64X1D#(.INIT(64'h0000000000000000))RAM64X1D_instance_2735(.WCLK(CLK), .WE(wea_1), 
  .A0(ADDRA[0]), .A1(ADDRA[1]), .A2(ADDRA[2]), .A3(ADDRA[3]), .A4(ADDRA[4]), .A5(ADDRA[5]), 
  .D(DINA[15]), .SPO(pre_douta_1[15]), .DPRA0(ADDRB[0]), .DPRA1(ADDRB[1]), .DPRA2(ADDRB[2]), 
  .DPRA3(ADDRB[3]), .DPRA4(ADDRB[4]), .DPRA5(ADDRB[5]), .DPO(pre_doutb_1[15]));
// Memory array element: COL: 1, ROW: 16
//  Initialization of Dual Port LUT ram now done through explicit
// parameter setting.
RAM64X1D#(.INIT(64'h0000000000000000))RAM64X1D_instance_2736(.WCLK(CLK), .WE(wea_1), 
  .A0(ADDRA[0]), .A1(ADDRA[1]), .A2(ADDRA[2]), .A3(ADDRA[3]), .A4(ADDRA[4]), .A5(ADDRA[5]), 
  .D(DINA[16]), .SPO(pre_douta_1[16]), .DPRA0(ADDRB[0]), .DPRA1(ADDRB[1]), .DPRA2(ADDRB[2]), 
  .DPRA3(ADDRB[3]), .DPRA4(ADDRB[4]), .DPRA5(ADDRB[5]), .DPO(pre_doutb_1[16]));
// Memory array element: COL: 1, ROW: 17
//  Initialization of Dual Port LUT ram now done through explicit
// parameter setting.
RAM64X1D#(.INIT(64'h0000000000000000))RAM64X1D_instance_2737(.WCLK(CLK), .WE(wea_1), 
  .A0(ADDRA[0]), .A1(ADDRA[1]), .A2(ADDRA[2]), .A3(ADDRA[3]), .A4(ADDRA[4]), .A5(ADDRA[5]), 
  .D(DINA[17]), .SPO(pre_douta_1[17]), .DPRA0(ADDRB[0]), .DPRA1(ADDRB[1]), .DPRA2(ADDRB[2]), 
  .DPRA3(ADDRB[3]), .DPRA4(ADDRB[4]), .DPRA5(ADDRB[5]), .DPO(pre_doutb_1[17]));
// Memory array element: COL: 1, ROW: 18
//  Initialization of Dual Port LUT ram now done through explicit
// parameter setting.
RAM64X1D#(.INIT(64'h0000000000000000))RAM64X1D_instance_2738(.WCLK(CLK), .WE(wea_1), 
  .A0(ADDRA[0]), .A1(ADDRA[1]), .A2(ADDRA[2]), .A3(ADDRA[3]), .A4(ADDRA[4]), .A5(ADDRA[5]), 
  .D(DINA[18]), .SPO(pre_douta_1[18]), .DPRA0(ADDRB[0]), .DPRA1(ADDRB[1]), .DPRA2(ADDRB[2]), 
  .DPRA3(ADDRB[3]), .DPRA4(ADDRB[4]), .DPRA5(ADDRB[5]), .DPO(pre_doutb_1[18]));
// Memory array element: COL: 1, ROW: 19
//  Initialization of Dual Port LUT ram now done through explicit
// parameter setting.
RAM64X1D#(.INIT(64'h0000000000000000))RAM64X1D_instance_2739(.WCLK(CLK), .WE(wea_1), 
  .A0(ADDRA[0]), .A1(ADDRA[1]), .A2(ADDRA[2]), .A3(ADDRA[3]), .A4(ADDRA[4]), .A5(ADDRA[5]), 
  .D(DINA[19]), .SPO(pre_douta_1[19]), .DPRA0(ADDRB[0]), .DPRA1(ADDRB[1]), .DPRA2(ADDRB[2]), 
  .DPRA3(ADDRB[3]), .DPRA4(ADDRB[4]), .DPRA5(ADDRB[5]), .DPO(pre_doutb_1[19]));
// Memory array element: COL: 1, ROW: 20
//  Initialization of Dual Port LUT ram now done through explicit
// parameter setting.
RAM64X1D#(.INIT(64'h0000000000000000))RAM64X1D_instance_2740(.WCLK(CLK), .WE(wea_1), 
  .A0(ADDRA[0]), .A1(ADDRA[1]), .A2(ADDRA[2]), .A3(ADDRA[3]), .A4(ADDRA[4]), .A5(ADDRA[5]), 
  .D(DINA[20]), .SPO(pre_douta_1[20]), .DPRA0(ADDRB[0]), .DPRA1(ADDRB[1]), .DPRA2(ADDRB[2]), 
  .DPRA3(ADDRB[3]), .DPRA4(ADDRB[4]), .DPRA5(ADDRB[5]), .DPO(pre_doutb_1[20]));
// Memory array element: COL: 1, ROW: 21
//  Initialization of Dual Port LUT ram now done through explicit
// parameter setting.
RAM64X1D#(.INIT(64'h0000000000000000))RAM64X1D_instance_2741(.WCLK(CLK), .WE(wea_1), 
  .A0(ADDRA[0]), .A1(ADDRA[1]), .A2(ADDRA[2]), .A3(ADDRA[3]), .A4(ADDRA[4]), .A5(ADDRA[5]), 
  .D(DINA[21]), .SPO(pre_douta_1[21]), .DPRA0(ADDRB[0]), .DPRA1(ADDRB[1]), .DPRA2(ADDRB[2]), 
  .DPRA3(ADDRB[3]), .DPRA4(ADDRB[4]), .DPRA5(ADDRB[5]), .DPO(pre_doutb_1[21]));
// Memory array element: COL: 1, ROW: 22
//  Initialization of Dual Port LUT ram now done through explicit
// parameter setting.
RAM64X1D#(.INIT(64'h0000000000000000))RAM64X1D_instance_2742(.WCLK(CLK), .WE(wea_1), 
  .A0(ADDRA[0]), .A1(ADDRA[1]), .A2(ADDRA[2]), .A3(ADDRA[3]), .A4(ADDRA[4]), .A5(ADDRA[5]), 
  .D(DINA[22]), .SPO(pre_douta_1[22]), .DPRA0(ADDRB[0]), .DPRA1(ADDRB[1]), .DPRA2(ADDRB[2]), 
  .DPRA3(ADDRB[3]), .DPRA4(ADDRB[4]), .DPRA5(ADDRB[5]), .DPO(pre_doutb_1[22]));
// Memory array element: COL: 1, ROW: 23
//  Initialization of Dual Port LUT ram now done through explicit
// parameter setting.
RAM64X1D#(.INIT(64'h0000000000000000))RAM64X1D_instance_2743(.WCLK(CLK), .WE(wea_1), 
  .A0(ADDRA[0]), .A1(ADDRA[1]), .A2(ADDRA[2]), .A3(ADDRA[3]), .A4(ADDRA[4]), .A5(ADDRA[5]), 
  .D(DINA[23]), .SPO(pre_douta_1[23]), .DPRA0(ADDRB[0]), .DPRA1(ADDRB[1]), .DPRA2(ADDRB[2]), 
  .DPRA3(ADDRB[3]), .DPRA4(ADDRB[4]), .DPRA5(ADDRB[5]), .DPO(pre_doutb_1[23]));
// Memory array element: COL: 1, ROW: 24
//  Initialization of Dual Port LUT ram now done through explicit
// parameter setting.
RAM64X1D#(.INIT(64'h0000000000000000))RAM64X1D_instance_2744(.WCLK(CLK), .WE(wea_1), 
  .A0(ADDRA[0]), .A1(ADDRA[1]), .A2(ADDRA[2]), .A3(ADDRA[3]), .A4(ADDRA[4]), .A5(ADDRA[5]), 
  .D(DINA[24]), .SPO(pre_douta_1[24]), .DPRA0(ADDRB[0]), .DPRA1(ADDRB[1]), .DPRA2(ADDRB[2]), 
  .DPRA3(ADDRB[3]), .DPRA4(ADDRB[4]), .DPRA5(ADDRB[5]), .DPO(pre_doutb_1[24]));
// Memory array element: COL: 1, ROW: 25
//  Initialization of Dual Port LUT ram now done through explicit
// parameter setting.
RAM64X1D#(.INIT(64'h0000000000000000))RAM64X1D_instance_2745(.WCLK(CLK), .WE(wea_1), 
  .A0(ADDRA[0]), .A1(ADDRA[1]), .A2(ADDRA[2]), .A3(ADDRA[3]), .A4(ADDRA[4]), .A5(ADDRA[5]), 
  .D(DINA[25]), .SPO(pre_douta_1[25]), .DPRA0(ADDRB[0]), .DPRA1(ADDRB[1]), .DPRA2(ADDRB[2]), 
  .DPRA3(ADDRB[3]), .DPRA4(ADDRB[4]), .DPRA5(ADDRB[5]), .DPO(pre_doutb_1[25]));
// Memory array element: COL: 1, ROW: 26
//  Initialization of Dual Port LUT ram now done through explicit
// parameter setting.
RAM64X1D#(.INIT(64'h0000000000000000))RAM64X1D_instance_2746(.WCLK(CLK), .WE(wea_1), 
  .A0(ADDRA[0]), .A1(ADDRA[1]), .A2(ADDRA[2]), .A3(ADDRA[3]), .A4(ADDRA[4]), .A5(ADDRA[5]), 
  .D(DINA[26]), .SPO(pre_douta_1[26]), .DPRA0(ADDRB[0]), .DPRA1(ADDRB[1]), .DPRA2(ADDRB[2]), 
  .DPRA3(ADDRB[3]), .DPRA4(ADDRB[4]), .DPRA5(ADDRB[5]), .DPO(pre_doutb_1[26]));
// Memory array element: COL: 1, ROW: 27
//  Initialization of Dual Port LUT ram now done through explicit
// parameter setting.
RAM64X1D#(.INIT(64'h0000000000000000))RAM64X1D_instance_2747(.WCLK(CLK), .WE(wea_1), 
  .A0(ADDRA[0]), .A1(ADDRA[1]), .A2(ADDRA[2]), .A3(ADDRA[3]), .A4(ADDRA[4]), .A5(ADDRA[5]), 
  .D(DINA[27]), .SPO(pre_douta_1[27]), .DPRA0(ADDRB[0]), .DPRA1(ADDRB[1]), .DPRA2(ADDRB[2]), 
  .DPRA3(ADDRB[3]), .DPRA4(ADDRB[4]), .DPRA5(ADDRB[5]), .DPO(pre_doutb_1[27]));
// Memory array element: COL: 1, ROW: 28
//  Initialization of Dual Port LUT ram now done through explicit
// parameter setting.
RAM64X1D#(.INIT(64'h0000000000000000))RAM64X1D_instance_2748(.WCLK(CLK), .WE(wea_1), 
  .A0(ADDRA[0]), .A1(ADDRA[1]), .A2(ADDRA[2]), .A3(ADDRA[3]), .A4(ADDRA[4]), .A5(ADDRA[5]), 
  .D(DINA[28]), .SPO(pre_douta_1[28]), .DPRA0(ADDRB[0]), .DPRA1(ADDRB[1]), .DPRA2(ADDRB[2]), 
  .DPRA3(ADDRB[3]), .DPRA4(ADDRB[4]), .DPRA5(ADDRB[5]), .DPO(pre_doutb_1[28]));
// Memory array element: COL: 1, ROW: 29
//  Initialization of Dual Port LUT ram now done through explicit
// parameter setting.
RAM64X1D#(.INIT(64'h0000000000000000))RAM64X1D_instance_2749(.WCLK(CLK), .WE(wea_1), 
  .A0(ADDRA[0]), .A1(ADDRA[1]), .A2(ADDRA[2]), .A3(ADDRA[3]), .A4(ADDRA[4]), .A5(ADDRA[5]), 
  .D(DINA[29]), .SPO(pre_douta_1[29]), .DPRA0(ADDRB[0]), .DPRA1(ADDRB[1]), .DPRA2(ADDRB[2]), 
  .DPRA3(ADDRB[3]), .DPRA4(ADDRB[4]), .DPRA5(ADDRB[5]), .DPO(pre_doutb_1[29]));
// Memory array element: COL: 1, ROW: 30
//  Initialization of Dual Port LUT ram now done through explicit
// parameter setting.
RAM64X1D#(.INIT(64'h0000000000000000))RAM64X1D_instance_2750(.WCLK(CLK), .WE(wea_1), 
  .A0(ADDRA[0]), .A1(ADDRA[1]), .A2(ADDRA[2]), .A3(ADDRA[3]), .A4(ADDRA[4]), .A5(ADDRA[5]), 
  .D(DINA[30]), .SPO(pre_douta_1[30]), .DPRA0(ADDRB[0]), .DPRA1(ADDRB[1]), .DPRA2(ADDRB[2]), 
  .DPRA3(ADDRB[3]), .DPRA4(ADDRB[4]), .DPRA5(ADDRB[5]), .DPO(pre_doutb_1[30]));
// Memory array element: COL: 1, ROW: 31
//  Initialization of Dual Port LUT ram now done through explicit
// parameter setting.
RAM64X1D#(.INIT(64'h0000000000000000))RAM64X1D_instance_2751(.WCLK(CLK), .WE(wea_1), 
  .A0(ADDRA[0]), .A1(ADDRA[1]), .A2(ADDRA[2]), .A3(ADDRA[3]), .A4(ADDRA[4]), .A5(ADDRA[5]), 
  .D(DINA[31]), .SPO(pre_douta_1[31]), .DPRA0(ADDRB[0]), .DPRA1(ADDRB[1]), .DPRA2(ADDRB[2]), 
  .DPRA3(ADDRB[3]), .DPRA4(ADDRB[4]), .DPRA5(ADDRB[5]), .DPO(pre_doutb_1[31]));
endmodule



module medianRow3_structuralmemory_15370eb9_(CLK_u88, bus_3fb75481_, bus_6c50ca49_, bus_331ba1e4_, bus_0e6811e7_, bus_0e7df011_, bus_49969a43_, bus_2d013811_, bus_3f1ed616_, bus_0ce591f3_, bus_299a3d99_, bus_276d2572_, bus_47d92ba9_, bus_4a7ea9d3_);
input		CLK_u88;
input		bus_3fb75481_;
input	[31:0]	bus_6c50ca49_;
input	[2:0]	bus_331ba1e4_;
input		bus_0e6811e7_;
input	[31:0]	bus_0e7df011_;
input	[2:0]	bus_49969a43_;
input		bus_2d013811_;
input		bus_3f1ed616_;
input	[31:0]	bus_0ce591f3_;
output	[31:0]	bus_299a3d99_;
output		bus_276d2572_;
output	[31:0]	bus_47d92ba9_;
output		bus_4a7ea9d3_;
reg		logicalMem_6eced121_we_delay0_u0=1'h0;
wire		not_570e5a8d_u0;
wire		or_69d28357_u0;
wire	[31:0]	bus_03db8fe5_;
wire	[31:0]	bus_18c23a99_;
wire		and_0c553232_u0;
wire		or_386c3a15_u0;
always @(posedge CLK_u88 or posedge bus_3fb75481_)
begin
if (bus_3fb75481_)
logicalMem_6eced121_we_delay0_u0<=1'h0;
else logicalMem_6eced121_we_delay0_u0<=bus_3f1ed616_;
end
assign not_570e5a8d_u0=~bus_3f1ed616_;
assign or_69d28357_u0=bus_2d013811_|bus_3f1ed616_;
medianRow3_forge_memory_101x32_137 medianRow3_forge_memory_101x32_137_instance0(.CLK(CLK_u88), 
  .ENA(or_69d28357_u0), .WEA(bus_3f1ed616_), .DINA(bus_0ce591f3_), .ADDRA(bus_0e7df011_), 
  .DOUTA(bus_03db8fe5_), .DONEA(), .ENB(bus_0e6811e7_), .ADDRB(bus_6c50ca49_), .DOUTB(bus_18c23a99_), 
  .DONEB());
assign and_0c553232_u0=bus_2d013811_&not_570e5a8d_u0;
assign bus_299a3d99_=bus_18c23a99_;
assign bus_276d2572_=bus_0e6811e7_;
assign bus_47d92ba9_=bus_03db8fe5_;
assign bus_4a7ea9d3_=or_386c3a15_u0;
assign or_386c3a15_u0=and_0c553232_u0|logicalMem_6eced121_we_delay0_u0;
endmodule



module medianRow3_simplememoryreferee_14849a54_(bus_7a9073c9_, bus_078b3f3f_, bus_4b8c9ecf_, bus_4d623c08_, bus_0ff8d3a9_, bus_4c0eb824_, bus_3e3c13b1_, bus_7bec75e4_, bus_5e6a8de8_, bus_3980d2b8_, bus_3de6add0_, bus_496f379e_, bus_07bb41ab_, bus_579f786c_, bus_208ce5dd_, bus_50a24456_, bus_38d71f8c_, bus_16d696c1_, bus_0b970a93_, bus_6d1ad724_, bus_58d62dee_);
input		bus_7a9073c9_;
input		bus_078b3f3f_;
input		bus_4b8c9ecf_;
input	[31:0]	bus_4d623c08_;
input		bus_0ff8d3a9_;
input	[31:0]	bus_4c0eb824_;
input	[31:0]	bus_3e3c13b1_;
input	[2:0]	bus_7bec75e4_;
input		bus_5e6a8de8_;
input		bus_3980d2b8_;
input	[31:0]	bus_3de6add0_;
input	[31:0]	bus_496f379e_;
input	[2:0]	bus_07bb41ab_;
output	[31:0]	bus_579f786c_;
output	[31:0]	bus_208ce5dd_;
output		bus_50a24456_;
output		bus_38d71f8c_;
output	[2:0]	bus_16d696c1_;
output		bus_0b970a93_;
output	[31:0]	bus_6d1ad724_;
output		bus_58d62dee_;
wire		or_55094ec8_u0;
reg		done_qual_u212=1'h0;
wire		or_29fcfb74_u0;
wire		not_67cb6e81_u0;
wire		and_01896402_u0;
wire		or_24e9ce79_u0;
wire		not_158fbc5c_u0;
wire	[31:0]	mux_38f3c4e7_u0;
wire		and_2fb70d61_u0;
wire		or_370d4ad4_u0;
wire		or_5b9b74a1_u0;
wire	[31:0]	mux_18a87e13_u0;
reg		done_qual_u213_u0=1'h0;
assign or_55094ec8_u0=bus_0ff8d3a9_|bus_3980d2b8_;
always @(posedge bus_7a9073c9_)
begin
if (bus_078b3f3f_)
done_qual_u212<=1'h0;
else done_qual_u212<=bus_0ff8d3a9_;
end
assign or_29fcfb74_u0=bus_0ff8d3a9_|or_5b9b74a1_u0;
assign not_67cb6e81_u0=~bus_4b8c9ecf_;
assign and_01896402_u0=or_24e9ce79_u0&bus_4b8c9ecf_;
assign or_24e9ce79_u0=bus_0ff8d3a9_|done_qual_u212;
assign bus_579f786c_=mux_38f3c4e7_u0;
assign bus_208ce5dd_=mux_18a87e13_u0;
assign bus_50a24456_=or_55094ec8_u0;
assign bus_38d71f8c_=or_29fcfb74_u0;
assign bus_16d696c1_=3'h1;
assign bus_0b970a93_=and_01896402_u0;
assign bus_6d1ad724_=bus_4d623c08_;
assign bus_58d62dee_=and_2fb70d61_u0;
assign not_158fbc5c_u0=~bus_4b8c9ecf_;
assign mux_38f3c4e7_u0=(bus_0ff8d3a9_)?{24'b0, bus_4c0eb824_[7:0]}:bus_3de6add0_;
assign and_2fb70d61_u0=or_370d4ad4_u0&bus_4b8c9ecf_;
assign or_370d4ad4_u0=or_5b9b74a1_u0|done_qual_u213_u0;
assign or_5b9b74a1_u0=bus_5e6a8de8_|bus_3980d2b8_;
assign mux_18a87e13_u0=(bus_0ff8d3a9_)?bus_3e3c13b1_:bus_496f379e_;
always @(posedge bus_7a9073c9_)
begin
if (bus_078b3f3f_)
done_qual_u213_u0<=1'h0;
else done_qual_u213_u0<=or_5b9b74a1_u0;
end
endmodule



module medianRow3_compute_median(CLK, RESET, GO, port_483b0649_, port_52e16905_, port_354097ba_, port_0dfedef3_, port_4f7891d8_, RESULT, RESULT_u1969, RESULT_u1970, RESULT_u1971, RESULT_u1972, RESULT_u1973, RESULT_u1974, RESULT_u1975, RESULT_u1976, RESULT_u1977, RESULT_u1978, RESULT_u1979, RESULT_u1980, RESULT_u1981, RESULT_u1982, DONE);
input		CLK;
input		RESET;
input		GO;
input		port_483b0649_;
input	[31:0]	port_52e16905_;
input		port_354097ba_;
input	[31:0]	port_0dfedef3_;
input		port_4f7891d8_;
output		RESULT;
output	[31:0]	RESULT_u1969;
output		RESULT_u1970;
output	[31:0]	RESULT_u1971;
output	[2:0]	RESULT_u1972;
output		RESULT_u1973;
output	[31:0]	RESULT_u1974;
output	[2:0]	RESULT_u1975;
output		RESULT_u1976;
output	[31:0]	RESULT_u1977;
output	[31:0]	RESULT_u1978;
output	[2:0]	RESULT_u1979;
output	[7:0]	RESULT_u1980;
output		RESULT_u1981;
output	[15:0]	RESULT_u1982;
output		DONE;
wire		and_u3283_u0;
wire		and_u3284_u0;
wire	[31:0]	add;
wire		and_u3285_u0;
wire	[31:0]	add_u628;
wire	[31:0]	add_u629;
wire		and_u3286_u0;
wire		greaterThan;
wire signed	[31:0]	greaterThan_a_signed;
wire signed	[31:0]	greaterThan_b_signed;
wire		and_u3287_u0;
wire		and_u3288_u0;
wire		not_u740_u0;
wire	[31:0]	add_u630;
wire		and_u3289_u0;
wire	[31:0]	add_u631;
wire	[31:0]	add_u632;
wire		and_u3290_u0;
wire	[31:0]	add_u633;
wire		or_u1358_u0;
reg		reg_286d8967_u0=1'h0;
wire		and_u3291_u0;
wire	[31:0]	add_u634;
wire	[31:0]	add_u635;
reg		reg_7f26d385_u0=1'h0;
wire		and_u3292_u0;
wire		or_u1359_u0;
reg	[31:0]	syncEnable_u1343=32'h0;
reg	[31:0]	syncEnable_u1344_u0=32'h0;
reg	[31:0]	syncEnable_u1345_u0=32'h0;
reg	[31:0]	syncEnable_u1346_u0=32'h0;
reg		block_GO_delayed_u90=1'h0;
reg		block_GO_delayed_result_delayed_u16=1'h0;
reg	[31:0]	syncEnable_u1347_u0=32'h0;
wire		or_u1360_u0;
wire	[31:0]	mux_u1803;
wire	[31:0]	mux_u1804_u0;
reg	[31:0]	syncEnable_u1348_u0=32'h0;
wire		mux_u1805_u0;
reg		reg_0e9994dd_u0=1'h0;
reg		reg_0e9994dd_result_delayed_u0=1'h0;
reg		reg_0e9994dd_result_delayed_result_delayed_u0=1'h0;
wire		or_u1361_u0;
wire		and_u3293_u0;
wire	[31:0]	mux_u1806_u0;
reg	[31:0]	syncEnable_u1349_u0=32'h0;
wire		and_u3294_u0;
wire	[31:0]	mux_u1807_u0;
reg		and_delayed_u376=1'h0;
reg	[31:0]	syncEnable_u1350_u0=32'h0;
reg		syncEnable_u1351_u0=1'h0;
reg		and_delayed_u377_u0=1'h0;
wire	[31:0]	add_u636;
wire		or_u1362_u0;
wire	[31:0]	mux_u1808_u0;
reg	[31:0]	syncEnable_u1352_u0=32'h0;
reg		syncEnable_u1353_u0=1'h0;
reg	[31:0]	syncEnable_u1354_u0=32'h0;
reg		block_GO_delayed_u91_u0=1'h0;
reg	[31:0]	syncEnable_u1355_u0=32'h0;
reg	[31:0]	syncEnable_u1356_u0=32'h0;
wire	[31:0]	mux_u1809_u0;
wire		or_u1363_u0;
reg	[31:0]	syncEnable_u1357_u0=32'h0;
reg	[31:0]	syncEnable_u1358_u0=32'h0;
wire signed	[32:0]	lessThan_a_signed;
wire signed	[32:0]	lessThan_b_signed;
wire		lessThan;
wire		and_u3295_u0;
wire		not_u741_u0;
wire		and_u3296_u0;
reg	[31:0]	syncEnable_u1359_u0=32'h0;
wire		and_u3297_u0;
wire	[31:0]	mux_u1810_u0;
wire	[31:0]	mux_u1811_u0;
wire	[31:0]	mux_u1812_u0;
wire		mux_u1813_u0;
wire	[31:0]	mux_u1814_u0;
wire		or_u1364_u0;
wire	[31:0]	mux_u1815_u0;
wire	[15:0]	add_u637;
wire	[31:0]	latch_389d3bde_out;
reg	[31:0]	latch_389d3bde_reg=32'h0;
reg		reg_31b9425c_u0=1'h0;
reg		scoreboard_11404e06_reg0=1'h0;
wire		scoreboard_11404e06_resOr0;
wire		scoreboard_11404e06_and;
wire		bus_6feed8aa_;
reg		scoreboard_11404e06_reg1=1'h0;
wire		scoreboard_11404e06_resOr1;
wire	[31:0]	latch_6d66588e_out;
reg	[31:0]	latch_6d66588e_reg=32'h0;
reg	[31:0]	latch_08fd632f_reg=32'h0;
wire	[31:0]	latch_08fd632f_out;
wire		latch_6fe3daf6_out;
reg		latch_6fe3daf6_reg=1'h0;
reg	[15:0]	syncEnable_u1360_u0=16'h0;
reg	[31:0]	latch_481fda23_reg=32'h0;
wire	[31:0]	latch_481fda23_out;
wire	[15:0]	lessThan_u85_b_unsigned;
wire	[15:0]	lessThan_u85_a_unsigned;
wire		lessThan_u85;
wire		andOp;
wire		and_u3298_u0;
wire		and_u3299_u0;
wire		not_u742_u0;
wire	[15:0]	mux_u1816_u0;
reg	[31:0]	fbReg_tmp_row1_u42=32'h0;
wire	[31:0]	mux_u1817_u0;
wire		or_u1365_u0;
reg		fbReg_swapped_u42=1'h0;
reg	[31:0]	fbReg_tmp_row_u42=32'h0;
wire	[31:0]	mux_u1818_u0;
wire		mux_u1819_u0;
reg	[31:0]	fbReg_tmp_u42=32'h0;
reg		syncEnable_u1361_u0=1'h0;
reg	[31:0]	fbReg_tmp_row0_u42=32'h0;
wire	[31:0]	mux_u1820_u0;
reg	[15:0]	fbReg_idx_u42=16'h0;
reg		loopControl_u89=1'h0;
wire	[31:0]	mux_u1821_u0;
wire		and_u3300_u0;
wire	[7:0]	simplePinWrite;
wire	[15:0]	simplePinWrite_u466;
wire		simplePinWrite_u467;
wire	[31:0]	mux_u1822_u0;
wire		or_u1366_u0;
reg		reg_079be379_u0=1'h0;
reg		reg_079be379_result_delayed_u0=1'h0;
assign and_u3283_u0=and_u3298_u0&or_u1365_u0;
assign and_u3284_u0=and_u3299_u0&or_u1365_u0;
assign add=mux_u1814_u0+32'h0;
assign and_u3285_u0=and_u3297_u0&port_483b0649_;
assign add_u628=mux_u1814_u0+32'h1;
assign add_u629=add_u628+32'h0;
assign and_u3286_u0=and_u3297_u0&port_4f7891d8_;
assign greaterThan_a_signed=syncEnable_u1355_u0;
assign greaterThan_b_signed=syncEnable_u1352_u0;
assign greaterThan=greaterThan_a_signed>greaterThan_b_signed;
assign and_u3287_u0=block_GO_delayed_u91_u0&greaterThan;
assign and_u3288_u0=block_GO_delayed_u91_u0&not_u740_u0;
assign not_u740_u0=~greaterThan;
assign add_u630=syncEnable_u1354_u0+32'h0;
assign and_u3289_u0=and_u3293_u0&port_483b0649_;
assign add_u631=syncEnable_u1354_u0+32'h1;
assign add_u632=add_u631+32'h0;
assign and_u3290_u0=and_u3293_u0&port_4f7891d8_;
assign add_u633=syncEnable_u1354_u0+32'h0;
assign or_u1358_u0=and_u3291_u0|RESET;
always @(posedge CLK or posedge block_GO_delayed_u90 or posedge or_u1358_u0)
begin
if (or_u1358_u0)
reg_286d8967_u0<=1'h0;
else if (block_GO_delayed_u90)
reg_286d8967_u0<=1'h1;
else reg_286d8967_u0<=reg_286d8967_u0;
end
assign and_u3291_u0=reg_286d8967_u0&port_4f7891d8_;
assign add_u634=syncEnable_u1354_u0+32'h1;
assign add_u635=add_u634+32'h0;
always @(posedge CLK or posedge block_GO_delayed_result_delayed_u16 or posedge or_u1359_u0)
begin
if (or_u1359_u0)
reg_7f26d385_u0<=1'h0;
else if (block_GO_delayed_result_delayed_u16)
reg_7f26d385_u0<=1'h1;
else reg_7f26d385_u0<=reg_7f26d385_u0;
end
assign and_u3292_u0=reg_7f26d385_u0&port_4f7891d8_;
assign or_u1359_u0=and_u3292_u0|RESET;
always @(posedge CLK)
begin
if (and_u3293_u0)
syncEnable_u1343<=add_u633;
end
always @(posedge CLK)
begin
if (and_u3293_u0)
syncEnable_u1344_u0<=port_52e16905_;
end
always @(posedge CLK)
begin
if (and_u3293_u0)
syncEnable_u1345_u0<=add_u635;
end
always @(posedge CLK)
begin
if (and_u3293_u0)
syncEnable_u1346_u0<=port_0dfedef3_;
end
always @(posedge CLK or posedge RESET)
begin
if (RESET)
block_GO_delayed_u90<=1'h0;
else block_GO_delayed_u90<=and_u3293_u0;
end
always @(posedge CLK or posedge RESET)
begin
if (RESET)
block_GO_delayed_result_delayed_u16<=1'h0;
else block_GO_delayed_result_delayed_u16<=block_GO_delayed_u90;
end
always @(posedge CLK)
begin
if (and_u3293_u0)
syncEnable_u1347_u0<=port_52e16905_;
end
assign or_u1360_u0=block_GO_delayed_u90|block_GO_delayed_result_delayed_u16;
assign mux_u1803=(block_GO_delayed_u90)?syncEnable_u1346_u0:syncEnable_u1347_u0;
assign mux_u1804_u0=({32{block_GO_delayed_u90}}&syncEnable_u1343)|({32{block_GO_delayed_result_delayed_u16}}&syncEnable_u1345_u0)|({32{and_u3293_u0}}&add_u632);
always @(posedge CLK)
begin
if (and_u3293_u0)
syncEnable_u1348_u0<=port_0dfedef3_;
end
assign mux_u1805_u0=(reg_0e9994dd_result_delayed_result_delayed_u0)?1'h1:syncEnable_u1351_u0;
always @(posedge CLK or posedge RESET)
begin
if (RESET)
reg_0e9994dd_u0<=1'h0;
else reg_0e9994dd_u0<=and_delayed_u377_u0;
end
always @(posedge CLK or posedge RESET)
begin
if (RESET)
reg_0e9994dd_result_delayed_u0<=1'h0;
else reg_0e9994dd_result_delayed_u0<=reg_0e9994dd_u0;
end
always @(posedge CLK or posedge RESET)
begin
if (RESET)
reg_0e9994dd_result_delayed_result_delayed_u0<=1'h0;
else reg_0e9994dd_result_delayed_result_delayed_u0<=reg_0e9994dd_result_delayed_u0;
end
assign or_u1361_u0=reg_0e9994dd_result_delayed_result_delayed_u0|and_delayed_u376;
assign and_u3293_u0=and_u3287_u0&block_GO_delayed_u91_u0;
assign mux_u1806_u0=(reg_0e9994dd_result_delayed_result_delayed_u0)?syncEnable_u1344_u0:syncEnable_u1350_u0;
always @(posedge CLK)
begin
if (block_GO_delayed_u91_u0)
syncEnable_u1349_u0<=syncEnable_u1357_u0;
end
assign and_u3294_u0=and_u3288_u0&block_GO_delayed_u91_u0;
assign mux_u1807_u0=(reg_0e9994dd_result_delayed_result_delayed_u0)?syncEnable_u1348_u0:syncEnable_u1349_u0;
always @(posedge CLK or posedge RESET)
begin
if (RESET)
and_delayed_u376<=1'h0;
else and_delayed_u376<=and_u3294_u0;
end
always @(posedge CLK)
begin
if (block_GO_delayed_u91_u0)
syncEnable_u1350_u0<=syncEnable_u1358_u0;
end
always @(posedge CLK)
begin
if (block_GO_delayed_u91_u0)
syncEnable_u1351_u0<=syncEnable_u1353_u0;
end
always @(posedge CLK or posedge RESET)
begin
if (RESET)
and_delayed_u377_u0<=1'h0;
else and_delayed_u377_u0<=and_u3293_u0;
end
assign add_u636=mux_u1814_u0+32'h1;
assign or_u1362_u0=and_u3297_u0|and_u3293_u0;
assign mux_u1808_u0=({32{or_u1360_u0}}&mux_u1804_u0)|({32{and_u3297_u0}}&add_u629)|({32{and_u3293_u0}}&mux_u1804_u0);
always @(posedge CLK)
begin
if (and_u3297_u0)
syncEnable_u1352_u0<=port_0dfedef3_;
end
always @(posedge CLK)
begin
if (and_u3297_u0)
syncEnable_u1353_u0<=mux_u1813_u0;
end
always @(posedge CLK)
begin
if (and_u3297_u0)
syncEnable_u1354_u0<=mux_u1814_u0;
end
always @(posedge CLK or posedge RESET)
begin
if (RESET)
block_GO_delayed_u91_u0<=1'h0;
else block_GO_delayed_u91_u0<=and_u3297_u0;
end
always @(posedge CLK)
begin
if (and_u3297_u0)
syncEnable_u1355_u0<=port_52e16905_;
end
always @(posedge CLK)
begin
if (and_u3297_u0)
syncEnable_u1356_u0<=add_u636;
end
assign mux_u1809_u0=(and_u3297_u0)?add:add_u630;
assign or_u1363_u0=and_u3297_u0|and_u3293_u0;
always @(posedge CLK)
begin
if (and_u3297_u0)
syncEnable_u1357_u0<=mux_u1811_u0;
end
always @(posedge CLK)
begin
if (and_u3297_u0)
syncEnable_u1358_u0<=mux_u1812_u0;
end
assign lessThan_a_signed={1'b0, mux_u1814_u0};
assign lessThan_b_signed=33'h64;
assign lessThan=lessThan_a_signed<lessThan_b_signed;
assign and_u3295_u0=or_u1364_u0&not_u741_u0;
assign not_u741_u0=~lessThan;
assign and_u3296_u0=or_u1364_u0&lessThan;
always @(posedge CLK)
begin
if (or_u1364_u0)
syncEnable_u1359_u0<=mux_u1815_u0;
end
assign and_u3297_u0=and_u3296_u0&or_u1364_u0;
assign mux_u1810_u0=(and_u3284_u0)?mux_u1818_u0:syncEnable_u1352_u0;
assign mux_u1811_u0=(and_u3284_u0)?mux_u1817_u0:mux_u1807_u0;
assign mux_u1812_u0=(and_u3284_u0)?mux_u1821_u0:mux_u1806_u0;
assign mux_u1813_u0=(and_u3284_u0)?1'h0:mux_u1805_u0;
assign mux_u1814_u0=(and_u3284_u0)?32'h0:syncEnable_u1356_u0;
assign or_u1364_u0=and_u3284_u0|or_u1361_u0;
assign mux_u1815_u0=(and_u3284_u0)?mux_u1820_u0:syncEnable_u1355_u0;
assign add_u637=mux_u1816_u0+16'h1;
assign latch_389d3bde_out=(or_u1361_u0)?syncEnable_u1352_u0:latch_389d3bde_reg;
always @(posedge CLK)
begin
if (or_u1361_u0)
latch_389d3bde_reg<=syncEnable_u1352_u0;
end
always @(posedge CLK or posedge RESET)
begin
if (RESET)
reg_31b9425c_u0<=1'h0;
else reg_31b9425c_u0<=or_u1361_u0;
end
always @(posedge CLK)
begin
if (bus_6feed8aa_)
scoreboard_11404e06_reg0<=1'h0;
else if (or_u1361_u0)
scoreboard_11404e06_reg0<=1'h1;
else scoreboard_11404e06_reg0<=scoreboard_11404e06_reg0;
end
assign scoreboard_11404e06_resOr0=or_u1361_u0|scoreboard_11404e06_reg0;
assign scoreboard_11404e06_and=scoreboard_11404e06_resOr0&scoreboard_11404e06_resOr1;
assign bus_6feed8aa_=scoreboard_11404e06_and|RESET;
always @(posedge CLK)
begin
if (bus_6feed8aa_)
scoreboard_11404e06_reg1<=1'h0;
else if (reg_31b9425c_u0)
scoreboard_11404e06_reg1<=1'h1;
else scoreboard_11404e06_reg1<=scoreboard_11404e06_reg1;
end
assign scoreboard_11404e06_resOr1=reg_31b9425c_u0|scoreboard_11404e06_reg1;
assign latch_6d66588e_out=(or_u1361_u0)?syncEnable_u1359_u0:latch_6d66588e_reg;
always @(posedge CLK)
begin
if (or_u1361_u0)
latch_6d66588e_reg<=syncEnable_u1359_u0;
end
always @(posedge CLK)
begin
if (or_u1361_u0)
latch_08fd632f_reg<=mux_u1806_u0;
end
assign latch_08fd632f_out=(or_u1361_u0)?mux_u1806_u0:latch_08fd632f_reg;
assign latch_6fe3daf6_out=(or_u1361_u0)?mux_u1805_u0:latch_6fe3daf6_reg;
always @(posedge CLK)
begin
if (or_u1361_u0)
latch_6fe3daf6_reg<=mux_u1805_u0;
end
always @(posedge CLK)
begin
if (and_u3284_u0)
syncEnable_u1360_u0<=add_u637;
end
always @(posedge CLK)
begin
if (or_u1361_u0)
latch_481fda23_reg<=mux_u1807_u0;
end
assign latch_481fda23_out=(or_u1361_u0)?mux_u1807_u0:latch_481fda23_reg;
assign lessThan_u85_a_unsigned=mux_u1816_u0;
assign lessThan_u85_b_unsigned=16'h65;
assign lessThan_u85=lessThan_u85_a_unsigned<lessThan_u85_b_unsigned;
assign andOp=lessThan_u85&mux_u1819_u0;
assign and_u3298_u0=or_u1365_u0&not_u742_u0;
assign and_u3299_u0=or_u1365_u0&andOp;
assign not_u742_u0=~andOp;
assign mux_u1816_u0=(GO)?16'h0:fbReg_idx_u42;
always @(posedge CLK)
begin
if (scoreboard_11404e06_and)
fbReg_tmp_row1_u42<=latch_481fda23_out;
end
assign mux_u1817_u0=(GO)?32'h0:fbReg_tmp_row1_u42;
assign or_u1365_u0=GO|loopControl_u89;
always @(posedge CLK)
begin
if (scoreboard_11404e06_and)
fbReg_swapped_u42<=latch_6fe3daf6_out;
end
always @(posedge CLK)
begin
if (scoreboard_11404e06_and)
fbReg_tmp_row_u42<=latch_6d66588e_out;
end
assign mux_u1818_u0=(GO)?32'h0:fbReg_tmp_row0_u42;
assign mux_u1819_u0=(GO)?1'h0:fbReg_swapped_u42;
always @(posedge CLK)
begin
if (scoreboard_11404e06_and)
fbReg_tmp_u42<=latch_08fd632f_out;
end
always @(posedge CLK)
begin
if (GO)
syncEnable_u1361_u0<=RESET;
end
always @(posedge CLK)
begin
if (scoreboard_11404e06_and)
fbReg_tmp_row0_u42<=latch_389d3bde_out;
end
assign mux_u1820_u0=(GO)?32'h0:fbReg_tmp_row_u42;
always @(posedge CLK)
begin
if (scoreboard_11404e06_and)
fbReg_idx_u42<=syncEnable_u1360_u0;
end
always @(posedge CLK or posedge syncEnable_u1361_u0)
begin
if (syncEnable_u1361_u0)
loopControl_u89<=1'h0;
else loopControl_u89<=scoreboard_11404e06_and;
end
assign mux_u1821_u0=(GO)?32'h0:fbReg_tmp_u42;
assign and_u3300_u0=reg_079be379_u0&port_483b0649_;
assign simplePinWrite=port_52e16905_[7:0];
assign simplePinWrite_u466=16'h1&{16{1'h1}};
assign simplePinWrite_u467=reg_079be379_u0&{1{reg_079be379_u0}};
assign mux_u1822_u0=(or_u1363_u0)?mux_u1809_u0:32'h32;
assign or_u1366_u0=or_u1363_u0|reg_079be379_u0;
always @(posedge CLK or posedge RESET)
begin
if (RESET)
reg_079be379_u0<=1'h0;
else reg_079be379_u0<=and_u3283_u0;
end
always @(posedge CLK or posedge RESET)
begin
if (RESET)
reg_079be379_result_delayed_u0<=1'h0;
else reg_079be379_result_delayed_u0<=reg_079be379_u0;
end
assign RESULT=GO;
assign RESULT_u1969=32'h0;
assign RESULT_u1970=or_u1366_u0;
assign RESULT_u1971=mux_u1822_u0;
assign RESULT_u1972=3'h1;
assign RESULT_u1973=or_u1362_u0;
assign RESULT_u1974=mux_u1808_u0;
assign RESULT_u1975=3'h1;
assign RESULT_u1976=or_u1360_u0;
assign RESULT_u1977=mux_u1808_u0;
assign RESULT_u1978=mux_u1803;
assign RESULT_u1979=3'h1;
assign RESULT_u1980=simplePinWrite;
assign RESULT_u1981=simplePinWrite_u467;
assign RESULT_u1982=simplePinWrite_u466;
assign DONE=reg_079be379_result_delayed_u0;
endmodule



module medianRow3_globalreset_physical_60677802_(bus_18530ae3_, bus_2d088eaa_, bus_1e8a6260_);
input		bus_18530ae3_;
input		bus_2d088eaa_;
output		bus_1e8a6260_;
reg		final_u52=1'h1;
reg		cross_u52=1'h0;
wire		and_721d0cf9_u0;
wire		not_330de7c9_u0;
wire		or_7bc31d66_u0;
reg		glitch_u52=1'h0;
reg		sample_u52=1'h0;
always @(posedge bus_18530ae3_)
begin
final_u52<=not_330de7c9_u0;
end
always @(posedge bus_18530ae3_)
begin
cross_u52<=sample_u52;
end
assign and_721d0cf9_u0=cross_u52&glitch_u52;
assign not_330de7c9_u0=~and_721d0cf9_u0;
assign or_7bc31d66_u0=bus_2d088eaa_|final_u52;
always @(posedge bus_18530ae3_)
begin
glitch_u52<=cross_u52;
end
assign bus_1e8a6260_=or_7bc31d66_u0;
always @(posedge bus_18530ae3_)
begin
sample_u52<=1'h1;
end
endmodule



module medianRow3_receive(CLK, RESET, GO, port_48d875af_, port_5f0d9a55_, port_53143199_, RESULT, RESULT_u1983, RESULT_u1984, RESULT_u1985, RESULT_u1986, RESULT_u1987, RESULT_u1988, DONE);
input		CLK;
input		RESET;
input		GO;
input	[31:0]	port_48d875af_;
input		port_5f0d9a55_;
input	[7:0]	port_53143199_;
output		RESULT;
output	[31:0]	RESULT_u1983;
output		RESULT_u1984;
output	[31:0]	RESULT_u1985;
output	[31:0]	RESULT_u1986;
output	[2:0]	RESULT_u1987;
output		RESULT_u1988;
output		DONE;
wire		simplePinWrite;
wire	[31:0]	add;
wire		or_u1367_u0;
wire		and_u3301_u0;
reg		reg_67dba0b2_u0=1'h0;
wire	[31:0]	add_u638;
reg		reg_5cda7efb_u0=1'h0;
assign simplePinWrite=GO&{1{GO}};
assign add=port_48d875af_+32'h0;
assign or_u1367_u0=and_u3301_u0|RESET;
assign and_u3301_u0=reg_67dba0b2_u0&port_5f0d9a55_;
always @(posedge CLK or posedge GO or posedge or_u1367_u0)
begin
if (or_u1367_u0)
reg_67dba0b2_u0<=1'h0;
else if (GO)
reg_67dba0b2_u0<=1'h1;
else reg_67dba0b2_u0<=reg_67dba0b2_u0;
end
assign add_u638=port_48d875af_+32'h1;
always @(posedge CLK or posedge RESET)
begin
if (RESET)
reg_5cda7efb_u0<=1'h0;
else reg_5cda7efb_u0<=GO;
end
assign RESULT=GO;
assign RESULT_u1983=add_u638;
assign RESULT_u1984=GO;
assign RESULT_u1985=add;
assign RESULT_u1986={24'b0, port_53143199_};
assign RESULT_u1987=3'h1;
assign RESULT_u1988=simplePinWrite;
assign DONE=reg_5cda7efb_u0;
endmodule



module medianRow3_endianswapper_31b3f383_(endianswapper_31b3f383_in, endianswapper_31b3f383_out);
input		endianswapper_31b3f383_in;
output		endianswapper_31b3f383_out;
assign endianswapper_31b3f383_out=endianswapper_31b3f383_in;
endmodule



module medianRow3_endianswapper_625029f4_(endianswapper_625029f4_in, endianswapper_625029f4_out);
input		endianswapper_625029f4_in;
output		endianswapper_625029f4_out;
assign endianswapper_625029f4_out=endianswapper_625029f4_in;
endmodule



module medianRow3_stateVar_fsmState_medianRow3(bus_6a4d280b_, bus_78dd91e2_, bus_063b7a30_, bus_1173d449_, bus_540b1976_);
input		bus_6a4d280b_;
input		bus_78dd91e2_;
input		bus_063b7a30_;
input		bus_1173d449_;
output		bus_540b1976_;
wire		endianswapper_31b3f383_out;
wire		endianswapper_625029f4_out;
reg		stateVar_fsmState_medianRow3_u4=1'h0;
medianRow3_endianswapper_31b3f383_ medianRow3_endianswapper_31b3f383__1(.endianswapper_31b3f383_in(stateVar_fsmState_medianRow3_u4), 
  .endianswapper_31b3f383_out(endianswapper_31b3f383_out));
medianRow3_endianswapper_625029f4_ medianRow3_endianswapper_625029f4__1(.endianswapper_625029f4_in(bus_1173d449_), 
  .endianswapper_625029f4_out(endianswapper_625029f4_out));
always @(posedge bus_6a4d280b_ or posedge bus_78dd91e2_)
begin
if (bus_78dd91e2_)
stateVar_fsmState_medianRow3_u4<=1'h0;
else if (bus_063b7a30_)
stateVar_fsmState_medianRow3_u4<=endianswapper_625029f4_out;
end
assign bus_540b1976_=endianswapper_31b3f383_out;
endmodule



module medianRow3_simplememoryreferee_51b7974f_(bus_3ee70140_, bus_64afef21_, bus_10d604dd_, bus_36c7d3ac_, bus_1b19a75e_, bus_19ee9cde_, bus_3262fe74_, bus_2523f18a_, bus_259da384_, bus_17da45ba_, bus_45b27ee7_, bus_57e9a613_, bus_01da4310_, bus_22e0ab4d_);
input		bus_3ee70140_;
input		bus_64afef21_;
input		bus_10d604dd_;
input	[31:0]	bus_36c7d3ac_;
input		bus_1b19a75e_;
input	[31:0]	bus_19ee9cde_;
input	[2:0]	bus_3262fe74_;
output	[31:0]	bus_2523f18a_;
output	[31:0]	bus_259da384_;
output		bus_17da45ba_;
output		bus_45b27ee7_;
output	[2:0]	bus_57e9a613_;
output	[31:0]	bus_01da4310_;
output		bus_22e0ab4d_;
assign bus_2523f18a_=32'h0;
assign bus_259da384_=bus_19ee9cde_;
assign bus_17da45ba_=1'h0;
assign bus_45b27ee7_=bus_1b19a75e_;
assign bus_57e9a613_=3'h1;
assign bus_01da4310_=bus_36c7d3ac_;
assign bus_22e0ab4d_=bus_10d604dd_;
endmodule


