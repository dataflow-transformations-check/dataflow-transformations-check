// __  ___ __ ___  _ __   ___  ___ 
// \ \/ / '__/ _ \| '_ \ / _ \/ __|
//  >  <| | | (_) | | | | (_) \__ \
// /_/\_\_|  \___/|_| |_|\___/|___/
// 
// Xronos synthesizer version
// Run date: Fri 23 Feb 2018 14:28:03 +0000
// 

module medianRow4(RESET, in1_COUNT, in1_SEND, in1_ACK, CLK, median_COUNT, median_RDY, in1_DATA, median_DATA, median_ACK, median_SEND);
input		RESET;
input	[15:0]	in1_COUNT;
input		in1_SEND;
output		in1_ACK;
input		CLK;
output	[15:0]	median_COUNT;
input		median_RDY;
input	[7:0]	in1_DATA;
output	[7:0]	median_DATA;
input		median_ACK;
wire		receive_done;
wire		compute_median_done;
output		median_SEND;
wire		receive_go;
wire		compute_median_go;
wire	[31:0]	bus_11f763ef_;
wire	[31:0]	bus_7e603dde_;
wire		bus_2a4bb5e4_;
wire		bus_3fc0479f_;
wire		bus_11d1dc56_;
wire	[31:0]	bus_6e603f5f_;
wire	[2:0]	bus_648d8fb3_;
wire		bus_740926b4_;
wire	[31:0]	bus_39ee271c_;
wire		bus_113c1025_;
wire		bus_78ffd854_;
wire		bus_7daabdd8_;
wire	[31:0]	bus_7d6eb73e_;
wire		bus_4c68d9cd_;
wire	[31:0]	bus_27972bca_;
wire	[31:0]	bus_7f77ae2b_;
wire		bus_47501f30_;
wire	[2:0]	bus_29ca0263_;
wire	[31:0]	compute_median_u604;
wire	[2:0]	compute_median_u609;
wire	[31:0]	compute_median_u608;
wire		compute_median_u610;
wire	[31:0]	compute_median_u607;
wire	[2:0]	compute_median_u612;
wire	[7:0]	compute_median_u613;
wire		compute_median;
wire	[2:0]	compute_median_u605;
wire		compute_median_u606;
wire		compute_median_u614;
wire	[31:0]	compute_median_u602;
wire		compute_median_u603;
wire		medianRow4_compute_median_instance_DONE;
wire	[31:0]	compute_median_u611;
wire	[15:0]	compute_median_u615;
wire		receive_u263;
wire		receive_u259;
wire	[31:0]	receive_u260;
wire		receive;
wire	[31:0]	receive_u258;
wire	[31:0]	receive_u261;
wire	[2:0]	receive_u262;
wire		medianRow4_receive_instance_DONE;
wire		bus_5d0c87b5_;
wire	[31:0]	bus_197064a7_;
wire	[31:0]	bus_60d7e1d0_;
wire		bus_2463ba1f_;
wire		bus_766ee8ce_;
wire		bus_22c5d798_;
wire		bus_5c265c2c_;
wire		scheduler_u772;
wire		scheduler;
wire		scheduler_u771;
wire		scheduler_u770;
wire		medianRow4_scheduler_instance_DONE;
assign in1_ACK=receive_u263;
assign median_COUNT=compute_median_u615;
assign median_DATA=compute_median_u613;
assign receive_done=bus_5c265c2c_;
assign compute_median_done=bus_22c5d798_;
assign median_SEND=compute_median_u614;
assign receive_go=scheduler_u771;
assign compute_median_go=scheduler_u772;
medianRow4_simplememoryreferee_008adb8a_ medianRow4_simplememoryreferee_008adb8a__1(.bus_7794dca3_(CLK), 
  .bus_2020260d_(bus_766ee8ce_), .bus_3796db19_(bus_5d0c87b5_), .bus_4602f2a1_(bus_197064a7_), 
  .bus_6618c438_(compute_median_u603), .bus_3317ee2b_(compute_median_u604), .bus_4fabc816_(3'h1), 
  .bus_7e603dde_(bus_7e603dde_), .bus_11f763ef_(bus_11f763ef_), .bus_3fc0479f_(bus_3fc0479f_), 
  .bus_11d1dc56_(bus_11d1dc56_), .bus_648d8fb3_(bus_648d8fb3_), .bus_6e603f5f_(bus_6e603f5f_), 
  .bus_2a4bb5e4_(bus_2a4bb5e4_));
medianRow4_Kicker_53 medianRow4_Kicker_53_1(.CLK(CLK), .RESET(bus_766ee8ce_), .bus_740926b4_(bus_740926b4_));
medianRow4_stateVar_i medianRow4_stateVar_i_1(.bus_29b15487_(CLK), .bus_3bf082a0_(bus_766ee8ce_), 
  .bus_6d821f3b_(receive), .bus_71ee0594_(receive_u258), .bus_2f910e4a_(compute_median), 
  .bus_43d0f0d0_(32'h0), .bus_39ee271c_(bus_39ee271c_));
medianRow4_stateVar_fsmState_medianRow4 medianRow4_stateVar_fsmState_medianRow4_1(.bus_72369970_(CLK), 
  .bus_64cc8874_(bus_766ee8ce_), .bus_7d35edce_(scheduler), .bus_3b01a62c_(scheduler_u770), 
  .bus_113c1025_(bus_113c1025_));
medianRow4_simplememoryreferee_4998740e_ medianRow4_simplememoryreferee_4998740e__1(.bus_1cbea823_(CLK), 
  .bus_618c93a4_(bus_766ee8ce_), .bus_43dc5a72_(bus_2463ba1f_), .bus_6ca9c116_(bus_60d7e1d0_), 
  .bus_455a8b10_(receive_u259), .bus_144588dd_({24'b0, receive_u261[7:0]}), .bus_4794f65f_(receive_u260), 
  .bus_11bda4a2_(3'h1), .bus_7bd5fd68_(compute_median_u610), .bus_77a6f84c_(compute_median_u606), 
  .bus_175cc2ee_(compute_median_u608), .bus_178bfa3b_(compute_median_u607), .bus_524aa457_(3'h1), 
  .bus_7f77ae2b_(bus_7f77ae2b_), .bus_27972bca_(bus_27972bca_), .bus_47501f30_(bus_47501f30_), 
  .bus_4c68d9cd_(bus_4c68d9cd_), .bus_29ca0263_(bus_29ca0263_), .bus_78ffd854_(bus_78ffd854_), 
  .bus_7d6eb73e_(bus_7d6eb73e_), .bus_7daabdd8_(bus_7daabdd8_));
medianRow4_compute_median medianRow4_compute_median_instance(.CLK(CLK), .RESET(bus_766ee8ce_), 
  .GO(compute_median_go), .port_283edd5f_(bus_2a4bb5e4_), .port_33c94ff8_(bus_6e603f5f_), 
  .port_6e70ec83_(bus_7daabdd8_), .port_3ae74673_(bus_7daabdd8_), .port_7107bb46_(bus_7d6eb73e_), 
  .DONE(medianRow4_compute_median_instance_DONE), .RESULT(compute_median), .RESULT_u1989(compute_median_u602), 
  .RESULT_u1990(compute_median_u603), .RESULT_u1991(compute_median_u604), .RESULT_u1992(compute_median_u605), 
  .RESULT_u1996(compute_median_u606), .RESULT_u1997(compute_median_u607), .RESULT_u1998(compute_median_u608), 
  .RESULT_u1999(compute_median_u609), .RESULT_u1993(compute_median_u610), .RESULT_u1994(compute_median_u611), 
  .RESULT_u1995(compute_median_u612), .RESULT_u2000(compute_median_u613), .RESULT_u2001(compute_median_u614), 
  .RESULT_u2002(compute_median_u615));
medianRow4_receive medianRow4_receive_instance(.CLK(CLK), .RESET(bus_766ee8ce_), 
  .GO(receive_go), .port_2837912c_(bus_39ee271c_), .port_068a2adb_(bus_78ffd854_), 
  .port_031c7566_(in1_DATA), .DONE(medianRow4_receive_instance_DONE), .RESULT(receive), 
  .RESULT_u2003(receive_u258), .RESULT_u2004(receive_u259), .RESULT_u2005(receive_u260), 
  .RESULT_u2006(receive_u261), .RESULT_u2007(receive_u262), .RESULT_u2008(receive_u263));
medianRow4_structuralmemory_325af1a9_ medianRow4_structuralmemory_325af1a9__1(.CLK_u89(CLK), 
  .bus_1cc9c3d9_(bus_766ee8ce_), .bus_131c3e71_(bus_11f763ef_), .bus_51fb455d_(3'h1), 
  .bus_29cfd5d3_(bus_11d1dc56_), .bus_5be914d7_(bus_27972bca_), .bus_2b1d0d63_(3'h1), 
  .bus_53a86a59_(bus_4c68d9cd_), .bus_5711d689_(bus_47501f30_), .bus_3fb70c0a_(bus_7f77ae2b_), 
  .bus_197064a7_(bus_197064a7_), .bus_5d0c87b5_(bus_5d0c87b5_), .bus_60d7e1d0_(bus_60d7e1d0_), 
  .bus_2463ba1f_(bus_2463ba1f_));
medianRow4_globalreset_physical_3f2bad1e_ medianRow4_globalreset_physical_3f2bad1e__1(.bus_1dcbf8f2_(CLK), 
  .bus_5ddb6052_(RESET), .bus_766ee8ce_(bus_766ee8ce_));
assign bus_22c5d798_=medianRow4_compute_median_instance_DONE&{1{medianRow4_compute_median_instance_DONE}};
assign bus_5c265c2c_=medianRow4_receive_instance_DONE&{1{medianRow4_receive_instance_DONE}};
medianRow4_scheduler medianRow4_scheduler_instance(.CLK(CLK), .RESET(bus_766ee8ce_), 
  .GO(bus_740926b4_), .port_6e1723e8_(bus_113c1025_), .port_5b5d1610_(bus_39ee271c_), 
  .port_72a70685_(median_RDY), .port_26f7a5b8_(in1_SEND), .port_092bee70_(receive_done), 
  .port_777686a0_(compute_median_done), .DONE(medianRow4_scheduler_instance_DONE), 
  .RESULT(scheduler), .RESULT_u2009(scheduler_u770), .RESULT_u2010(scheduler_u771), 
  .RESULT_u2011(scheduler_u772));
endmodule



module medianRow4_simplememoryreferee_008adb8a_(bus_7794dca3_, bus_2020260d_, bus_3796db19_, bus_4602f2a1_, bus_6618c438_, bus_3317ee2b_, bus_4fabc816_, bus_7e603dde_, bus_11f763ef_, bus_3fc0479f_, bus_11d1dc56_, bus_648d8fb3_, bus_6e603f5f_, bus_2a4bb5e4_);
input		bus_7794dca3_;
input		bus_2020260d_;
input		bus_3796db19_;
input	[31:0]	bus_4602f2a1_;
input		bus_6618c438_;
input	[31:0]	bus_3317ee2b_;
input	[2:0]	bus_4fabc816_;
output	[31:0]	bus_7e603dde_;
output	[31:0]	bus_11f763ef_;
output		bus_3fc0479f_;
output		bus_11d1dc56_;
output	[2:0]	bus_648d8fb3_;
output	[31:0]	bus_6e603f5f_;
output		bus_2a4bb5e4_;
assign bus_7e603dde_=32'h0;
assign bus_11f763ef_=bus_3317ee2b_;
assign bus_3fc0479f_=1'h0;
assign bus_11d1dc56_=bus_6618c438_;
assign bus_648d8fb3_=3'h1;
assign bus_6e603f5f_=bus_4602f2a1_;
assign bus_2a4bb5e4_=bus_3796db19_;
endmodule



module medianRow4_Kicker_53(CLK, RESET, bus_740926b4_);
input		CLK;
input		RESET;
output		bus_740926b4_;
reg		kicker_1=1'h0;
reg		kicker_2=1'h0;
wire		bus_1803362c_;
wire		bus_6d6d83d3_;
wire		bus_28ccc060_;
reg		kicker_res=1'h0;
wire		bus_5eda2f46_;
always @(posedge CLK)
begin
kicker_1<=bus_5eda2f46_;
end
assign bus_740926b4_=kicker_res;
always @(posedge CLK)
begin
kicker_2<=bus_1803362c_;
end
assign bus_1803362c_=bus_5eda2f46_&kicker_1;
assign bus_6d6d83d3_=kicker_1&bus_5eda2f46_&bus_28ccc060_;
assign bus_28ccc060_=~kicker_2;
always @(posedge CLK)
begin
kicker_res<=bus_6d6d83d3_;
end
assign bus_5eda2f46_=~RESET;
endmodule



module medianRow4_endianswapper_57480d65_(endianswapper_57480d65_in, endianswapper_57480d65_out);
input	[31:0]	endianswapper_57480d65_in;
output	[31:0]	endianswapper_57480d65_out;
assign endianswapper_57480d65_out=endianswapper_57480d65_in;
endmodule



module medianRow4_endianswapper_598ad8f4_(endianswapper_598ad8f4_in, endianswapper_598ad8f4_out);
input	[31:0]	endianswapper_598ad8f4_in;
output	[31:0]	endianswapper_598ad8f4_out;
assign endianswapper_598ad8f4_out=endianswapper_598ad8f4_in;
endmodule



module medianRow4_stateVar_i(bus_29b15487_, bus_3bf082a0_, bus_6d821f3b_, bus_71ee0594_, bus_2f910e4a_, bus_43d0f0d0_, bus_39ee271c_);
input		bus_29b15487_;
input		bus_3bf082a0_;
input		bus_6d821f3b_;
input	[31:0]	bus_71ee0594_;
input		bus_2f910e4a_;
input	[31:0]	bus_43d0f0d0_;
output	[31:0]	bus_39ee271c_;
wire		or_446bc432_u0;
wire	[31:0]	mux_4b2c87d6_u0;
reg	[31:0]	stateVar_i_u44=32'h0;
wire	[31:0]	endianswapper_57480d65_out;
wire	[31:0]	endianswapper_598ad8f4_out;
assign bus_39ee271c_=endianswapper_598ad8f4_out;
assign or_446bc432_u0=bus_6d821f3b_|bus_2f910e4a_;
assign mux_4b2c87d6_u0=(bus_6d821f3b_)?bus_71ee0594_:32'h0;
always @(posedge bus_29b15487_ or posedge bus_3bf082a0_)
begin
if (bus_3bf082a0_)
stateVar_i_u44<=32'h0;
else if (or_446bc432_u0)
stateVar_i_u44<=endianswapper_57480d65_out;
end
medianRow4_endianswapper_57480d65_ medianRow4_endianswapper_57480d65__1(.endianswapper_57480d65_in(mux_4b2c87d6_u0), 
  .endianswapper_57480d65_out(endianswapper_57480d65_out));
medianRow4_endianswapper_598ad8f4_ medianRow4_endianswapper_598ad8f4__1(.endianswapper_598ad8f4_in(stateVar_i_u44), 
  .endianswapper_598ad8f4_out(endianswapper_598ad8f4_out));
endmodule



module medianRow4_endianswapper_540e59fe_(endianswapper_540e59fe_in, endianswapper_540e59fe_out);
input		endianswapper_540e59fe_in;
output		endianswapper_540e59fe_out;
assign endianswapper_540e59fe_out=endianswapper_540e59fe_in;
endmodule



module medianRow4_endianswapper_335970bb_(endianswapper_335970bb_in, endianswapper_335970bb_out);
input		endianswapper_335970bb_in;
output		endianswapper_335970bb_out;
assign endianswapper_335970bb_out=endianswapper_335970bb_in;
endmodule



module medianRow4_stateVar_fsmState_medianRow4(bus_72369970_, bus_64cc8874_, bus_7d35edce_, bus_3b01a62c_, bus_113c1025_);
input		bus_72369970_;
input		bus_64cc8874_;
input		bus_7d35edce_;
input		bus_3b01a62c_;
output		bus_113c1025_;
wire		endianswapper_540e59fe_out;
wire		endianswapper_335970bb_out;
reg		stateVar_fsmState_medianRow4_u4=1'h0;
medianRow4_endianswapper_540e59fe_ medianRow4_endianswapper_540e59fe__1(.endianswapper_540e59fe_in(stateVar_fsmState_medianRow4_u4), 
  .endianswapper_540e59fe_out(endianswapper_540e59fe_out));
medianRow4_endianswapper_335970bb_ medianRow4_endianswapper_335970bb__1(.endianswapper_335970bb_in(bus_3b01a62c_), 
  .endianswapper_335970bb_out(endianswapper_335970bb_out));
assign bus_113c1025_=endianswapper_540e59fe_out;
always @(posedge bus_72369970_ or posedge bus_64cc8874_)
begin
if (bus_64cc8874_)
stateVar_fsmState_medianRow4_u4<=1'h0;
else if (bus_7d35edce_)
stateVar_fsmState_medianRow4_u4<=endianswapper_335970bb_out;
end
endmodule



module medianRow4_simplememoryreferee_4998740e_(bus_1cbea823_, bus_618c93a4_, bus_43dc5a72_, bus_6ca9c116_, bus_455a8b10_, bus_144588dd_, bus_4794f65f_, bus_11bda4a2_, bus_7bd5fd68_, bus_77a6f84c_, bus_175cc2ee_, bus_178bfa3b_, bus_524aa457_, bus_7f77ae2b_, bus_27972bca_, bus_47501f30_, bus_4c68d9cd_, bus_29ca0263_, bus_78ffd854_, bus_7d6eb73e_, bus_7daabdd8_);
input		bus_1cbea823_;
input		bus_618c93a4_;
input		bus_43dc5a72_;
input	[31:0]	bus_6ca9c116_;
input		bus_455a8b10_;
input	[31:0]	bus_144588dd_;
input	[31:0]	bus_4794f65f_;
input	[2:0]	bus_11bda4a2_;
input		bus_7bd5fd68_;
input		bus_77a6f84c_;
input	[31:0]	bus_175cc2ee_;
input	[31:0]	bus_178bfa3b_;
input	[2:0]	bus_524aa457_;
output	[31:0]	bus_7f77ae2b_;
output	[31:0]	bus_27972bca_;
output		bus_47501f30_;
output		bus_4c68d9cd_;
output	[2:0]	bus_29ca0263_;
output		bus_78ffd854_;
output	[31:0]	bus_7d6eb73e_;
output		bus_7daabdd8_;
wire		or_4149558a_u0;
wire		or_59d2f0f1_u0;
reg		done_qual_u214=1'h0;
wire		and_4416d4b5_u0;
wire	[31:0]	mux_1474efb1_u0;
wire		or_559c0e5a_u0;
wire		not_21da86d0_u0;
wire		and_59511a1a_u0;
reg		done_qual_u215_u0=1'h0;
wire	[31:0]	mux_14a206d7_u0;
wire		or_159390e5_u0;
wire		or_24955522_u0;
wire		not_5e6db1f3_u0;
assign or_4149558a_u0=bus_455a8b10_|bus_77a6f84c_;
assign or_59d2f0f1_u0=bus_455a8b10_|done_qual_u215_u0;
always @(posedge bus_1cbea823_)
begin
if (bus_618c93a4_)
done_qual_u214<=1'h0;
else done_qual_u214<=or_559c0e5a_u0;
end
assign and_4416d4b5_u0=or_24955522_u0&bus_43dc5a72_;
assign mux_1474efb1_u0=(bus_455a8b10_)?bus_4794f65f_:bus_178bfa3b_;
assign or_559c0e5a_u0=bus_7bd5fd68_|bus_77a6f84c_;
assign not_21da86d0_u0=~bus_43dc5a72_;
assign and_59511a1a_u0=or_59d2f0f1_u0&bus_43dc5a72_;
always @(posedge bus_1cbea823_)
begin
if (bus_618c93a4_)
done_qual_u215_u0<=1'h0;
else done_qual_u215_u0<=bus_455a8b10_;
end
assign mux_14a206d7_u0=(bus_455a8b10_)?{24'b0, bus_144588dd_[7:0]}:bus_175cc2ee_;
assign or_159390e5_u0=bus_455a8b10_|or_559c0e5a_u0;
assign or_24955522_u0=or_559c0e5a_u0|done_qual_u214;
assign bus_7f77ae2b_=mux_14a206d7_u0;
assign bus_27972bca_=mux_1474efb1_u0;
assign bus_47501f30_=or_4149558a_u0;
assign bus_4c68d9cd_=or_159390e5_u0;
assign bus_29ca0263_=3'h1;
assign bus_78ffd854_=and_59511a1a_u0;
assign bus_7d6eb73e_=bus_6ca9c116_;
assign bus_7daabdd8_=and_4416d4b5_u0;
assign not_5e6db1f3_u0=~bus_43dc5a72_;
endmodule



module medianRow4_compute_median(CLK, RESET, GO, port_283edd5f_, port_33c94ff8_, port_3ae74673_, port_7107bb46_, port_6e70ec83_, RESULT, RESULT_u1989, RESULT_u1990, RESULT_u1991, RESULT_u1992, RESULT_u1993, RESULT_u1994, RESULT_u1995, RESULT_u1996, RESULT_u1997, RESULT_u1998, RESULT_u1999, RESULT_u2000, RESULT_u2001, RESULT_u2002, DONE);
input		CLK;
input		RESET;
input		GO;
input		port_283edd5f_;
input	[31:0]	port_33c94ff8_;
input		port_3ae74673_;
input	[31:0]	port_7107bb46_;
input		port_6e70ec83_;
output		RESULT;
output	[31:0]	RESULT_u1989;
output		RESULT_u1990;
output	[31:0]	RESULT_u1991;
output	[2:0]	RESULT_u1992;
output		RESULT_u1993;
output	[31:0]	RESULT_u1994;
output	[2:0]	RESULT_u1995;
output		RESULT_u1996;
output	[31:0]	RESULT_u1997;
output	[31:0]	RESULT_u1998;
output	[2:0]	RESULT_u1999;
output	[7:0]	RESULT_u2000;
output		RESULT_u2001;
output	[15:0]	RESULT_u2002;
output		DONE;
wire		and_u3302_u0;
wire	[15:0]	lessThan_a_unsigned;
wire		lessThan;
wire	[15:0]	lessThan_b_unsigned;
wire		andOp;
wire		and_u3303_u0;
wire		and_u3304_u0;
wire		not_u743_u0;
wire		and_u3305_u0;
wire		lessThan_u86;
wire signed	[32:0]	lessThan_u86_a_signed;
wire signed	[32:0]	lessThan_u86_b_signed;
wire		and_u3306_u0;
wire		and_u3307_u0;
wire		not_u744_u0;
wire		and_u3308_u0;
reg	[31:0]	syncEnable_u1362=32'h0;
wire	[31:0]	add;
wire		and_u3309_u0;
wire	[31:0]	add_u639;
wire	[31:0]	add_u640;
wire		and_u3310_u0;
wire		greaterThan;
wire signed	[31:0]	greaterThan_a_signed;
wire signed	[31:0]	greaterThan_b_signed;
wire		and_u3311_u0;
wire		and_u3312_u0;
wire		not_u745_u0;
wire	[31:0]	add_u641;
wire		and_u3313_u0;
wire	[31:0]	add_u642;
wire	[31:0]	add_u643;
wire		and_u3314_u0;
wire	[31:0]	add_u644;
wire		and_u3315_u0;
wire		or_u1368_u0;
reg		reg_0cbc9196_u0=1'h0;
wire	[31:0]	add_u645;
wire	[31:0]	add_u646;
reg		reg_43a27022_u0=1'h0;
wire		or_u1369_u0;
wire		and_u3316_u0;
reg	[31:0]	syncEnable_u1363_u0=32'h0;
reg	[31:0]	syncEnable_u1364_u0=32'h0;
reg	[31:0]	syncEnable_u1365_u0=32'h0;
reg	[31:0]	syncEnable_u1366_u0=32'h0;
wire		or_u1370_u0;
wire	[31:0]	mux_u1823;
wire	[31:0]	mux_u1824_u0;
reg	[31:0]	syncEnable_u1367_u0=32'h0;
reg	[31:0]	syncEnable_u1368_u0=32'h0;
reg		block_GO_delayed_u92=1'h0;
reg		block_GO_delayed_result_delayed_u17=1'h0;
reg		reg_4ad644f6_u0=1'h0;
wire		mux_u1825_u0;
wire		and_u3317_u0;
reg		and_delayed_u378=1'h0;
wire	[31:0]	mux_u1826_u0;
reg		syncEnable_u1369_u0=1'h0;
wire		or_u1371_u0;
reg		reg_23fc968a_u0=1'h0;
reg		reg_23fc968a_result_delayed_u0=1'h0;
reg		reg_23fc968a_result_delayed_result_delayed_u0=1'h0;
reg	[31:0]	syncEnable_u1370_u0=32'h0;
reg	[31:0]	syncEnable_u1371_u0=32'h0;
wire		and_u3318_u0;
wire	[31:0]	mux_u1827_u0;
wire	[31:0]	add_u647;
reg	[31:0]	syncEnable_u1372_u0=32'h0;
reg	[31:0]	syncEnable_u1373_u0=32'h0;
reg	[31:0]	syncEnable_u1374_u0=32'h0;
reg	[31:0]	syncEnable_u1375_u0=32'h0;
wire	[31:0]	mux_u1828_u0;
wire		or_u1372_u0;
reg	[31:0]	syncEnable_u1376_u0=32'h0;
wire		or_u1373_u0;
wire	[31:0]	mux_u1829_u0;
reg		block_GO_delayed_u93_u0=1'h0;
reg	[31:0]	syncEnable_u1377_u0=32'h0;
reg		syncEnable_u1378_u0=1'h0;
reg		syncEnable_u1379_u0=1'h0;
wire		mux_u1830_u0;
wire	[31:0]	mux_u1831_u0;
wire	[31:0]	mux_u1832_u0;
wire	[31:0]	mux_u1833_u0;
wire		or_u1374_u0;
wire	[31:0]	mux_u1834_u0;
wire	[31:0]	mux_u1835_u0;
wire	[15:0]	add_u648;
reg		reg_3e272867_u0=1'h0;
wire	[31:0]	latch_64052881_out;
reg	[31:0]	latch_64052881_reg=32'h0;
wire	[31:0]	latch_74608de7_out;
reg	[31:0]	latch_74608de7_reg=32'h0;
reg	[31:0]	latch_17b57e1c_reg=32'h0;
wire	[31:0]	latch_17b57e1c_out;
wire		scoreboard_4ee7d968_and;
reg		scoreboard_4ee7d968_reg1=1'h0;
wire		scoreboard_4ee7d968_resOr1;
reg		scoreboard_4ee7d968_reg0=1'h0;
wire		scoreboard_4ee7d968_resOr0;
wire		bus_508f5eac_;
reg	[15:0]	syncEnable_u1380_u0=16'h0;
reg		latch_56e315b7_reg=1'h0;
wire		latch_56e315b7_out;
reg	[31:0]	latch_0ecb7db2_reg=32'h0;
wire	[31:0]	latch_0ecb7db2_out;
wire		mux_u1836_u0;
reg	[31:0]	fbReg_tmp_row_u43=32'h0;
reg	[31:0]	fbReg_tmp_u43=32'h0;
wire	[31:0]	mux_u1837_u0;
reg	[31:0]	fbReg_tmp_row1_u43=32'h0;
wire	[31:0]	mux_u1838_u0;
reg		syncEnable_u1381_u0=1'h0;
reg	[31:0]	fbReg_tmp_row0_u43=32'h0;
reg		fbReg_swapped_u43=1'h0;
wire		or_u1375_u0;
wire	[31:0]	mux_u1839_u0;
reg	[15:0]	fbReg_idx_u43=16'h0;
reg		loopControl_u90=1'h0;
wire	[15:0]	mux_u1840_u0;
wire	[31:0]	mux_u1841_u0;
wire		and_u3319_u0;
wire	[7:0]	simplePinWrite;
wire		simplePinWrite_u468;
wire	[15:0]	simplePinWrite_u469;
reg		reg_32d7ba96_u0=1'h0;
reg		reg_32d7ba96_result_delayed_u0=1'h0;
wire		or_u1376_u0;
wire	[31:0]	mux_u1842_u0;
assign and_u3302_u0=and_u3303_u0&or_u1375_u0;
assign lessThan_a_unsigned=mux_u1840_u0;
assign lessThan_b_unsigned=16'h65;
assign lessThan=lessThan_a_unsigned<lessThan_b_unsigned;
assign andOp=lessThan&mux_u1836_u0;
assign and_u3303_u0=or_u1375_u0&andOp;
assign and_u3304_u0=or_u1375_u0&not_u743_u0;
assign not_u743_u0=~andOp;
assign and_u3305_u0=and_u3304_u0&or_u1375_u0;
assign lessThan_u86_a_signed={1'b0, mux_u1835_u0};
assign lessThan_u86_b_signed=33'h64;
assign lessThan_u86=lessThan_u86_a_signed<lessThan_u86_b_signed;
assign and_u3306_u0=or_u1374_u0&lessThan_u86;
assign and_u3307_u0=or_u1374_u0&not_u744_u0;
assign not_u744_u0=~lessThan_u86;
assign and_u3308_u0=and_u3306_u0&or_u1374_u0;
always @(posedge CLK)
begin
if (or_u1374_u0)
syncEnable_u1362<=mux_u1834_u0;
end
assign add=mux_u1835_u0+32'h0;
assign and_u3309_u0=and_u3308_u0&port_283edd5f_;
assign add_u639=mux_u1835_u0+32'h1;
assign add_u640=add_u639+32'h0;
assign and_u3310_u0=and_u3308_u0&port_6e70ec83_;
assign greaterThan_a_signed=syncEnable_u1377_u0;
assign greaterThan_b_signed=syncEnable_u1373_u0;
assign greaterThan=greaterThan_a_signed>greaterThan_b_signed;
assign and_u3311_u0=block_GO_delayed_u93_u0&not_u745_u0;
assign and_u3312_u0=block_GO_delayed_u93_u0&greaterThan;
assign not_u745_u0=~greaterThan;
assign add_u641=syncEnable_u1375_u0+32'h0;
assign and_u3313_u0=and_u3318_u0&port_283edd5f_;
assign add_u642=syncEnable_u1375_u0+32'h1;
assign add_u643=add_u642+32'h0;
assign and_u3314_u0=and_u3318_u0&port_6e70ec83_;
assign add_u644=syncEnable_u1375_u0+32'h0;
assign and_u3315_u0=reg_0cbc9196_u0&port_6e70ec83_;
assign or_u1368_u0=and_u3315_u0|RESET;
always @(posedge CLK or posedge block_GO_delayed_u92 or posedge or_u1368_u0)
begin
if (or_u1368_u0)
reg_0cbc9196_u0<=1'h0;
else if (block_GO_delayed_u92)
reg_0cbc9196_u0<=1'h1;
else reg_0cbc9196_u0<=reg_0cbc9196_u0;
end
assign add_u645=syncEnable_u1375_u0+32'h1;
assign add_u646=add_u645+32'h0;
always @(posedge CLK or posedge block_GO_delayed_result_delayed_u17 or posedge or_u1369_u0)
begin
if (or_u1369_u0)
reg_43a27022_u0<=1'h0;
else if (block_GO_delayed_result_delayed_u17)
reg_43a27022_u0<=1'h1;
else reg_43a27022_u0<=reg_43a27022_u0;
end
assign or_u1369_u0=and_u3316_u0|RESET;
assign and_u3316_u0=reg_43a27022_u0&port_6e70ec83_;
always @(posedge CLK)
begin
if (and_u3318_u0)
syncEnable_u1363_u0<=port_33c94ff8_;
end
always @(posedge CLK)
begin
if (and_u3318_u0)
syncEnable_u1364_u0<=port_33c94ff8_;
end
always @(posedge CLK)
begin
if (and_u3318_u0)
syncEnable_u1365_u0<=port_7107bb46_;
end
always @(posedge CLK)
begin
if (and_u3318_u0)
syncEnable_u1366_u0<=port_7107bb46_;
end
assign or_u1370_u0=block_GO_delayed_u92|block_GO_delayed_result_delayed_u17;
assign mux_u1823=({32{block_GO_delayed_u92}}&syncEnable_u1368_u0)|({32{block_GO_delayed_result_delayed_u17}}&syncEnable_u1367_u0)|({32{and_u3318_u0}}&add_u643);
assign mux_u1824_u0=(block_GO_delayed_u92)?syncEnable_u1365_u0:syncEnable_u1364_u0;
always @(posedge CLK)
begin
if (and_u3318_u0)
syncEnable_u1367_u0<=add_u646;
end
always @(posedge CLK)
begin
if (and_u3318_u0)
syncEnable_u1368_u0<=add_u644;
end
always @(posedge CLK or posedge RESET)
begin
if (RESET)
block_GO_delayed_u92<=1'h0;
else block_GO_delayed_u92<=and_u3318_u0;
end
always @(posedge CLK or posedge RESET)
begin
if (RESET)
block_GO_delayed_result_delayed_u17<=1'h0;
else block_GO_delayed_result_delayed_u17<=block_GO_delayed_u92;
end
always @(posedge CLK or posedge RESET)
begin
if (RESET)
reg_4ad644f6_u0<=1'h0;
else reg_4ad644f6_u0<=reg_23fc968a_result_delayed_result_delayed_u0;
end
assign mux_u1825_u0=(and_delayed_u378)?syncEnable_u1369_u0:1'h1;
assign and_u3317_u0=and_u3311_u0&block_GO_delayed_u93_u0;
always @(posedge CLK or posedge RESET)
begin
if (RESET)
and_delayed_u378<=1'h0;
else and_delayed_u378<=and_u3317_u0;
end
assign mux_u1826_u0=(and_delayed_u378)?syncEnable_u1371_u0:syncEnable_u1366_u0;
always @(posedge CLK)
begin
if (block_GO_delayed_u93_u0)
syncEnable_u1369_u0<=syncEnable_u1378_u0;
end
assign or_u1371_u0=and_delayed_u378|reg_4ad644f6_u0;
always @(posedge CLK or posedge RESET)
begin
if (RESET)
reg_23fc968a_u0<=1'h0;
else reg_23fc968a_u0<=and_u3318_u0;
end
always @(posedge CLK or posedge RESET)
begin
if (RESET)
reg_23fc968a_result_delayed_u0<=1'h0;
else reg_23fc968a_result_delayed_u0<=reg_23fc968a_u0;
end
always @(posedge CLK or posedge RESET)
begin
if (RESET)
reg_23fc968a_result_delayed_result_delayed_u0<=1'h0;
else reg_23fc968a_result_delayed_result_delayed_u0<=reg_23fc968a_result_delayed_u0;
end
always @(posedge CLK)
begin
if (block_GO_delayed_u93_u0)
syncEnable_u1370_u0<=syncEnable_u1374_u0;
end
always @(posedge CLK)
begin
if (block_GO_delayed_u93_u0)
syncEnable_u1371_u0<=syncEnable_u1376_u0;
end
assign and_u3318_u0=and_u3312_u0&block_GO_delayed_u93_u0;
assign mux_u1827_u0=(and_delayed_u378)?syncEnable_u1370_u0:syncEnable_u1363_u0;
assign add_u647=mux_u1835_u0+32'h1;
always @(posedge CLK)
begin
if (and_u3308_u0)
syncEnable_u1372_u0<=add_u647;
end
always @(posedge CLK)
begin
if (and_u3308_u0)
syncEnable_u1373_u0<=port_7107bb46_;
end
always @(posedge CLK)
begin
if (and_u3308_u0)
syncEnable_u1374_u0<=mux_u1831_u0;
end
always @(posedge CLK)
begin
if (and_u3308_u0)
syncEnable_u1375_u0<=mux_u1835_u0;
end
assign mux_u1828_u0=({32{or_u1370_u0}}&mux_u1823)|({32{and_u3308_u0}}&add_u640)|({32{and_u3318_u0}}&mux_u1823);
assign or_u1372_u0=and_u3308_u0|and_u3318_u0;
always @(posedge CLK)
begin
if (and_u3308_u0)
syncEnable_u1376_u0<=mux_u1832_u0;
end
assign or_u1373_u0=and_u3308_u0|and_u3318_u0;
assign mux_u1829_u0=(and_u3308_u0)?add:add_u641;
always @(posedge CLK or posedge RESET)
begin
if (RESET)
block_GO_delayed_u93_u0<=1'h0;
else block_GO_delayed_u93_u0<=and_u3308_u0;
end
always @(posedge CLK)
begin
if (and_u3308_u0)
syncEnable_u1377_u0<=port_33c94ff8_;
end
always @(posedge CLK)
begin
if (and_u3308_u0)
syncEnable_u1378_u0<=mux_u1830_u0;
end
always @(posedge CLK)
begin
if (or_u1374_u0)
syncEnable_u1379_u0<=mux_u1830_u0;
end
assign mux_u1830_u0=(or_u1371_u0)?mux_u1825_u0:1'h0;
assign mux_u1831_u0=(or_u1371_u0)?mux_u1827_u0:mux_u1839_u0;
assign mux_u1832_u0=(or_u1371_u0)?mux_u1826_u0:mux_u1841_u0;
assign mux_u1833_u0=(or_u1371_u0)?syncEnable_u1377_u0:mux_u1838_u0;
assign or_u1374_u0=or_u1371_u0|and_u3302_u0;
assign mux_u1834_u0=(or_u1371_u0)?syncEnable_u1373_u0:mux_u1837_u0;
assign mux_u1835_u0=(or_u1371_u0)?syncEnable_u1372_u0:32'h0;
assign add_u648=mux_u1840_u0+16'h1;
always @(posedge CLK or posedge RESET)
begin
if (RESET)
reg_3e272867_u0<=1'h0;
else reg_3e272867_u0<=or_u1371_u0;
end
assign latch_64052881_out=(or_u1371_u0)?syncEnable_u1377_u0:latch_64052881_reg;
always @(posedge CLK)
begin
if (or_u1371_u0)
latch_64052881_reg<=syncEnable_u1377_u0;
end
assign latch_74608de7_out=(or_u1371_u0)?mux_u1827_u0:latch_74608de7_reg;
always @(posedge CLK)
begin
if (or_u1371_u0)
latch_74608de7_reg<=mux_u1827_u0;
end
always @(posedge CLK)
begin
if (or_u1371_u0)
latch_17b57e1c_reg<=syncEnable_u1362;
end
assign latch_17b57e1c_out=(or_u1371_u0)?syncEnable_u1362:latch_17b57e1c_reg;
assign scoreboard_4ee7d968_and=scoreboard_4ee7d968_resOr0&scoreboard_4ee7d968_resOr1;
always @(posedge CLK)
begin
if (bus_508f5eac_)
scoreboard_4ee7d968_reg1<=1'h0;
else if (or_u1371_u0)
scoreboard_4ee7d968_reg1<=1'h1;
else scoreboard_4ee7d968_reg1<=scoreboard_4ee7d968_reg1;
end
assign scoreboard_4ee7d968_resOr1=or_u1371_u0|scoreboard_4ee7d968_reg1;
always @(posedge CLK)
begin
if (bus_508f5eac_)
scoreboard_4ee7d968_reg0<=1'h0;
else if (reg_3e272867_u0)
scoreboard_4ee7d968_reg0<=1'h1;
else scoreboard_4ee7d968_reg0<=scoreboard_4ee7d968_reg0;
end
assign scoreboard_4ee7d968_resOr0=reg_3e272867_u0|scoreboard_4ee7d968_reg0;
assign bus_508f5eac_=scoreboard_4ee7d968_and|RESET;
always @(posedge CLK)
begin
if (and_u3302_u0)
syncEnable_u1380_u0<=add_u648;
end
always @(posedge CLK)
begin
if (or_u1371_u0)
latch_56e315b7_reg<=syncEnable_u1379_u0;
end
assign latch_56e315b7_out=(or_u1371_u0)?syncEnable_u1379_u0:latch_56e315b7_reg;
always @(posedge CLK)
begin
if (or_u1371_u0)
latch_0ecb7db2_reg<=mux_u1826_u0;
end
assign latch_0ecb7db2_out=(or_u1371_u0)?mux_u1826_u0:latch_0ecb7db2_reg;
assign mux_u1836_u0=(GO)?1'h0:fbReg_swapped_u43;
always @(posedge CLK)
begin
if (scoreboard_4ee7d968_and)
fbReg_tmp_row_u43<=latch_64052881_out;
end
always @(posedge CLK)
begin
if (scoreboard_4ee7d968_and)
fbReg_tmp_u43<=latch_74608de7_out;
end
assign mux_u1837_u0=(GO)?32'h0:fbReg_tmp_row0_u43;
always @(posedge CLK)
begin
if (scoreboard_4ee7d968_and)
fbReg_tmp_row1_u43<=latch_0ecb7db2_out;
end
assign mux_u1838_u0=(GO)?32'h0:fbReg_tmp_row_u43;
always @(posedge CLK)
begin
if (GO)
syncEnable_u1381_u0<=RESET;
end
always @(posedge CLK)
begin
if (scoreboard_4ee7d968_and)
fbReg_tmp_row0_u43<=latch_17b57e1c_out;
end
always @(posedge CLK)
begin
if (scoreboard_4ee7d968_and)
fbReg_swapped_u43<=latch_56e315b7_out;
end
assign or_u1375_u0=GO|loopControl_u90;
assign mux_u1839_u0=(GO)?32'h0:fbReg_tmp_u43;
always @(posedge CLK)
begin
if (scoreboard_4ee7d968_and)
fbReg_idx_u43<=syncEnable_u1380_u0;
end
always @(posedge CLK or posedge syncEnable_u1381_u0)
begin
if (syncEnable_u1381_u0)
loopControl_u90<=1'h0;
else loopControl_u90<=scoreboard_4ee7d968_and;
end
assign mux_u1840_u0=(GO)?16'h0:fbReg_idx_u43;
assign mux_u1841_u0=(GO)?32'h0:fbReg_tmp_row1_u43;
assign and_u3319_u0=reg_32d7ba96_u0&port_283edd5f_;
assign simplePinWrite=port_33c94ff8_[7:0];
assign simplePinWrite_u468=reg_32d7ba96_u0&{1{reg_32d7ba96_u0}};
assign simplePinWrite_u469=16'h1&{16{1'h1}};
always @(posedge CLK or posedge RESET)
begin
if (RESET)
reg_32d7ba96_u0<=1'h0;
else reg_32d7ba96_u0<=and_u3305_u0;
end
always @(posedge CLK or posedge RESET)
begin
if (RESET)
reg_32d7ba96_result_delayed_u0<=1'h0;
else reg_32d7ba96_result_delayed_u0<=reg_32d7ba96_u0;
end
assign or_u1376_u0=or_u1373_u0|reg_32d7ba96_u0;
assign mux_u1842_u0=(or_u1373_u0)?mux_u1829_u0:32'h32;
assign RESULT=GO;
assign RESULT_u1989=32'h0;
assign RESULT_u1990=or_u1376_u0;
assign RESULT_u1991=mux_u1842_u0;
assign RESULT_u1992=3'h1;
assign RESULT_u1993=or_u1372_u0;
assign RESULT_u1994=mux_u1828_u0;
assign RESULT_u1995=3'h1;
assign RESULT_u1996=or_u1370_u0;
assign RESULT_u1997=mux_u1828_u0;
assign RESULT_u1998=mux_u1824_u0;
assign RESULT_u1999=3'h1;
assign RESULT_u2000=simplePinWrite;
assign RESULT_u2001=simplePinWrite_u468;
assign RESULT_u2002=simplePinWrite_u469;
assign DONE=reg_32d7ba96_result_delayed_u0;
endmodule



module medianRow4_receive(CLK, RESET, GO, port_2837912c_, port_068a2adb_, port_031c7566_, RESULT, RESULT_u2003, RESULT_u2004, RESULT_u2005, RESULT_u2006, RESULT_u2007, RESULT_u2008, DONE);
input		CLK;
input		RESET;
input		GO;
input	[31:0]	port_2837912c_;
input		port_068a2adb_;
input	[7:0]	port_031c7566_;
output		RESULT;
output	[31:0]	RESULT_u2003;
output		RESULT_u2004;
output	[31:0]	RESULT_u2005;
output	[31:0]	RESULT_u2006;
output	[2:0]	RESULT_u2007;
output		RESULT_u2008;
output		DONE;
wire		simplePinWrite;
wire	[31:0]	add;
wire		or_u1377_u0;
wire		and_u3320_u0;
reg		reg_638e5412_u0=1'h0;
wire	[31:0]	add_u649;
reg		reg_5eadfe86_u0=1'h0;
assign simplePinWrite=GO&{1{GO}};
assign add=port_2837912c_+32'h0;
assign or_u1377_u0=and_u3320_u0|RESET;
assign and_u3320_u0=reg_638e5412_u0&port_068a2adb_;
always @(posedge CLK or posedge GO or posedge or_u1377_u0)
begin
if (or_u1377_u0)
reg_638e5412_u0<=1'h0;
else if (GO)
reg_638e5412_u0<=1'h1;
else reg_638e5412_u0<=reg_638e5412_u0;
end
assign add_u649=port_2837912c_+32'h1;
always @(posedge CLK or posedge RESET)
begin
if (RESET)
reg_5eadfe86_u0<=1'h0;
else reg_5eadfe86_u0<=GO;
end
assign RESULT=GO;
assign RESULT_u2003=add_u649;
assign RESULT_u2004=GO;
assign RESULT_u2005=add;
assign RESULT_u2006={24'b0, port_031c7566_};
assign RESULT_u2007=3'h1;
assign RESULT_u2008=simplePinWrite;
assign DONE=reg_5eadfe86_u0;
endmodule



module medianRow4_forge_memory_101x32_139(CLK, ENA, WEA, DINA, ENB, ADDRA, ADDRB, DOUTA, DOUTB, DONEA, DONEB);
input		CLK;
input		ENA;
input		WEA;
input	[31:0]	DINA;
input		ENB;
input	[31:0]	ADDRA;
input	[31:0]	ADDRB;
output	[31:0]	DOUTA;
output	[31:0]	DOUTB;
output		DONEA;
output		DONEB;
wire		wea_0;
wire	[31:0]	pre_douta_0;
wire	[31:0]	pre_doutb_0;
wire		wea_1;
wire	[31:0]	pre_douta_1;
wire	[31:0]	pre_doutb_1;
reg		wea_done;
reg		rea_done;
reg	[31:0]	mux_outa;
reg	[31:0]	mux_outb;
assign wea_0=WEA&(ADDRA[31:6]==26'h0);
assign wea_1=WEA&(ADDRA[31:6]==26'h1);
always @(posedge CLK)
begin
wea_done<=WEA;
end
assign DONEA=rea_done|wea_done;
always @(posedge CLK)
begin
end
assign DONEB=ENB;
always @(pre_douta_0 or pre_douta_1 or ADDRA)
begin
case (ADDRA[31:6])26'd0:mux_outa=pre_douta_0;
26'd1:mux_outa=pre_douta_1;
default:mux_outa=32'h0;
endcase end
assign DOUTA=mux_outa;
always @(pre_doutb_0 or pre_doutb_1 or ADDRB)
begin
case (ADDRB[31:6])26'd0:mux_outb=pre_doutb_0;
26'd1:mux_outb=pre_doutb_1;
default:mux_outb=32'h0;
endcase end
assign DOUTB=mux_outb;
// Memory array element: COL: 0, ROW: 0
//  Initialization of Dual Port LUT ram now done through explicit
// parameter setting.
RAM64X1D#(.INIT(64'h0000000000000000))RAM64X1D_instance_2752(.WCLK(CLK), .WE(wea_0), 
  .A0(ADDRA[0]), .A1(ADDRA[1]), .A2(ADDRA[2]), .A3(ADDRA[3]), .A4(ADDRA[4]), .A5(ADDRA[5]), 
  .D(DINA[0]), .SPO(pre_douta_0[0]), .DPRA0(ADDRB[0]), .DPRA1(ADDRB[1]), .DPRA2(ADDRB[2]), 
  .DPRA3(ADDRB[3]), .DPRA4(ADDRB[4]), .DPRA5(ADDRB[5]), .DPO(pre_doutb_0[0]));
// Memory array element: COL: 0, ROW: 1
//  Initialization of Dual Port LUT ram now done through explicit
// parameter setting.
RAM64X1D#(.INIT(64'h0000000000000000))RAM64X1D_instance_2753(.WCLK(CLK), .WE(wea_0), 
  .A0(ADDRA[0]), .A1(ADDRA[1]), .A2(ADDRA[2]), .A3(ADDRA[3]), .A4(ADDRA[4]), .A5(ADDRA[5]), 
  .D(DINA[1]), .SPO(pre_douta_0[1]), .DPRA0(ADDRB[0]), .DPRA1(ADDRB[1]), .DPRA2(ADDRB[2]), 
  .DPRA3(ADDRB[3]), .DPRA4(ADDRB[4]), .DPRA5(ADDRB[5]), .DPO(pre_doutb_0[1]));
// Memory array element: COL: 0, ROW: 2
//  Initialization of Dual Port LUT ram now done through explicit
// parameter setting.
RAM64X1D#(.INIT(64'h0000000000000000))RAM64X1D_instance_2754(.WCLK(CLK), .WE(wea_0), 
  .A0(ADDRA[0]), .A1(ADDRA[1]), .A2(ADDRA[2]), .A3(ADDRA[3]), .A4(ADDRA[4]), .A5(ADDRA[5]), 
  .D(DINA[2]), .SPO(pre_douta_0[2]), .DPRA0(ADDRB[0]), .DPRA1(ADDRB[1]), .DPRA2(ADDRB[2]), 
  .DPRA3(ADDRB[3]), .DPRA4(ADDRB[4]), .DPRA5(ADDRB[5]), .DPO(pre_doutb_0[2]));
// Memory array element: COL: 0, ROW: 3
//  Initialization of Dual Port LUT ram now done through explicit
// parameter setting.
RAM64X1D#(.INIT(64'h0000000000000000))RAM64X1D_instance_2755(.WCLK(CLK), .WE(wea_0), 
  .A0(ADDRA[0]), .A1(ADDRA[1]), .A2(ADDRA[2]), .A3(ADDRA[3]), .A4(ADDRA[4]), .A5(ADDRA[5]), 
  .D(DINA[3]), .SPO(pre_douta_0[3]), .DPRA0(ADDRB[0]), .DPRA1(ADDRB[1]), .DPRA2(ADDRB[2]), 
  .DPRA3(ADDRB[3]), .DPRA4(ADDRB[4]), .DPRA5(ADDRB[5]), .DPO(pre_doutb_0[3]));
// Memory array element: COL: 0, ROW: 4
//  Initialization of Dual Port LUT ram now done through explicit
// parameter setting.
RAM64X1D#(.INIT(64'h0000000000000000))RAM64X1D_instance_2756(.WCLK(CLK), .WE(wea_0), 
  .A0(ADDRA[0]), .A1(ADDRA[1]), .A2(ADDRA[2]), .A3(ADDRA[3]), .A4(ADDRA[4]), .A5(ADDRA[5]), 
  .D(DINA[4]), .SPO(pre_douta_0[4]), .DPRA0(ADDRB[0]), .DPRA1(ADDRB[1]), .DPRA2(ADDRB[2]), 
  .DPRA3(ADDRB[3]), .DPRA4(ADDRB[4]), .DPRA5(ADDRB[5]), .DPO(pre_doutb_0[4]));
// Memory array element: COL: 0, ROW: 5
//  Initialization of Dual Port LUT ram now done through explicit
// parameter setting.
RAM64X1D#(.INIT(64'h0000000000000000))RAM64X1D_instance_2757(.WCLK(CLK), .WE(wea_0), 
  .A0(ADDRA[0]), .A1(ADDRA[1]), .A2(ADDRA[2]), .A3(ADDRA[3]), .A4(ADDRA[4]), .A5(ADDRA[5]), 
  .D(DINA[5]), .SPO(pre_douta_0[5]), .DPRA0(ADDRB[0]), .DPRA1(ADDRB[1]), .DPRA2(ADDRB[2]), 
  .DPRA3(ADDRB[3]), .DPRA4(ADDRB[4]), .DPRA5(ADDRB[5]), .DPO(pre_doutb_0[5]));
// Memory array element: COL: 0, ROW: 6
//  Initialization of Dual Port LUT ram now done through explicit
// parameter setting.
RAM64X1D#(.INIT(64'h0000000000000000))RAM64X1D_instance_2758(.WCLK(CLK), .WE(wea_0), 
  .A0(ADDRA[0]), .A1(ADDRA[1]), .A2(ADDRA[2]), .A3(ADDRA[3]), .A4(ADDRA[4]), .A5(ADDRA[5]), 
  .D(DINA[6]), .SPO(pre_douta_0[6]), .DPRA0(ADDRB[0]), .DPRA1(ADDRB[1]), .DPRA2(ADDRB[2]), 
  .DPRA3(ADDRB[3]), .DPRA4(ADDRB[4]), .DPRA5(ADDRB[5]), .DPO(pre_doutb_0[6]));
// Memory array element: COL: 0, ROW: 7
//  Initialization of Dual Port LUT ram now done through explicit
// parameter setting.
RAM64X1D#(.INIT(64'h0000000000000000))RAM64X1D_instance_2759(.WCLK(CLK), .WE(wea_0), 
  .A0(ADDRA[0]), .A1(ADDRA[1]), .A2(ADDRA[2]), .A3(ADDRA[3]), .A4(ADDRA[4]), .A5(ADDRA[5]), 
  .D(DINA[7]), .SPO(pre_douta_0[7]), .DPRA0(ADDRB[0]), .DPRA1(ADDRB[1]), .DPRA2(ADDRB[2]), 
  .DPRA3(ADDRB[3]), .DPRA4(ADDRB[4]), .DPRA5(ADDRB[5]), .DPO(pre_doutb_0[7]));
// Memory array element: COL: 0, ROW: 8
//  Initialization of Dual Port LUT ram now done through explicit
// parameter setting.
RAM64X1D#(.INIT(64'h0000000000000000))RAM64X1D_instance_2760(.WCLK(CLK), .WE(wea_0), 
  .A0(ADDRA[0]), .A1(ADDRA[1]), .A2(ADDRA[2]), .A3(ADDRA[3]), .A4(ADDRA[4]), .A5(ADDRA[5]), 
  .D(DINA[8]), .SPO(pre_douta_0[8]), .DPRA0(ADDRB[0]), .DPRA1(ADDRB[1]), .DPRA2(ADDRB[2]), 
  .DPRA3(ADDRB[3]), .DPRA4(ADDRB[4]), .DPRA5(ADDRB[5]), .DPO(pre_doutb_0[8]));
// Memory array element: COL: 0, ROW: 9
//  Initialization of Dual Port LUT ram now done through explicit
// parameter setting.
RAM64X1D#(.INIT(64'h0000000000000000))RAM64X1D_instance_2761(.WCLK(CLK), .WE(wea_0), 
  .A0(ADDRA[0]), .A1(ADDRA[1]), .A2(ADDRA[2]), .A3(ADDRA[3]), .A4(ADDRA[4]), .A5(ADDRA[5]), 
  .D(DINA[9]), .SPO(pre_douta_0[9]), .DPRA0(ADDRB[0]), .DPRA1(ADDRB[1]), .DPRA2(ADDRB[2]), 
  .DPRA3(ADDRB[3]), .DPRA4(ADDRB[4]), .DPRA5(ADDRB[5]), .DPO(pre_doutb_0[9]));
// Memory array element: COL: 0, ROW: 10
//  Initialization of Dual Port LUT ram now done through explicit
// parameter setting.
RAM64X1D#(.INIT(64'h0000000000000000))RAM64X1D_instance_2762(.WCLK(CLK), .WE(wea_0), 
  .A0(ADDRA[0]), .A1(ADDRA[1]), .A2(ADDRA[2]), .A3(ADDRA[3]), .A4(ADDRA[4]), .A5(ADDRA[5]), 
  .D(DINA[10]), .SPO(pre_douta_0[10]), .DPRA0(ADDRB[0]), .DPRA1(ADDRB[1]), .DPRA2(ADDRB[2]), 
  .DPRA3(ADDRB[3]), .DPRA4(ADDRB[4]), .DPRA5(ADDRB[5]), .DPO(pre_doutb_0[10]));
// Memory array element: COL: 0, ROW: 11
//  Initialization of Dual Port LUT ram now done through explicit
// parameter setting.
RAM64X1D#(.INIT(64'h0000000000000000))RAM64X1D_instance_2763(.WCLK(CLK), .WE(wea_0), 
  .A0(ADDRA[0]), .A1(ADDRA[1]), .A2(ADDRA[2]), .A3(ADDRA[3]), .A4(ADDRA[4]), .A5(ADDRA[5]), 
  .D(DINA[11]), .SPO(pre_douta_0[11]), .DPRA0(ADDRB[0]), .DPRA1(ADDRB[1]), .DPRA2(ADDRB[2]), 
  .DPRA3(ADDRB[3]), .DPRA4(ADDRB[4]), .DPRA5(ADDRB[5]), .DPO(pre_doutb_0[11]));
// Memory array element: COL: 0, ROW: 12
//  Initialization of Dual Port LUT ram now done through explicit
// parameter setting.
RAM64X1D#(.INIT(64'h0000000000000000))RAM64X1D_instance_2764(.WCLK(CLK), .WE(wea_0), 
  .A0(ADDRA[0]), .A1(ADDRA[1]), .A2(ADDRA[2]), .A3(ADDRA[3]), .A4(ADDRA[4]), .A5(ADDRA[5]), 
  .D(DINA[12]), .SPO(pre_douta_0[12]), .DPRA0(ADDRB[0]), .DPRA1(ADDRB[1]), .DPRA2(ADDRB[2]), 
  .DPRA3(ADDRB[3]), .DPRA4(ADDRB[4]), .DPRA5(ADDRB[5]), .DPO(pre_doutb_0[12]));
// Memory array element: COL: 0, ROW: 13
//  Initialization of Dual Port LUT ram now done through explicit
// parameter setting.
RAM64X1D#(.INIT(64'h0000000000000000))RAM64X1D_instance_2765(.WCLK(CLK), .WE(wea_0), 
  .A0(ADDRA[0]), .A1(ADDRA[1]), .A2(ADDRA[2]), .A3(ADDRA[3]), .A4(ADDRA[4]), .A5(ADDRA[5]), 
  .D(DINA[13]), .SPO(pre_douta_0[13]), .DPRA0(ADDRB[0]), .DPRA1(ADDRB[1]), .DPRA2(ADDRB[2]), 
  .DPRA3(ADDRB[3]), .DPRA4(ADDRB[4]), .DPRA5(ADDRB[5]), .DPO(pre_doutb_0[13]));
// Memory array element: COL: 0, ROW: 14
//  Initialization of Dual Port LUT ram now done through explicit
// parameter setting.
RAM64X1D#(.INIT(64'h0000000000000000))RAM64X1D_instance_2766(.WCLK(CLK), .WE(wea_0), 
  .A0(ADDRA[0]), .A1(ADDRA[1]), .A2(ADDRA[2]), .A3(ADDRA[3]), .A4(ADDRA[4]), .A5(ADDRA[5]), 
  .D(DINA[14]), .SPO(pre_douta_0[14]), .DPRA0(ADDRB[0]), .DPRA1(ADDRB[1]), .DPRA2(ADDRB[2]), 
  .DPRA3(ADDRB[3]), .DPRA4(ADDRB[4]), .DPRA5(ADDRB[5]), .DPO(pre_doutb_0[14]));
// Memory array element: COL: 0, ROW: 15
//  Initialization of Dual Port LUT ram now done through explicit
// parameter setting.
RAM64X1D#(.INIT(64'h0000000000000000))RAM64X1D_instance_2767(.WCLK(CLK), .WE(wea_0), 
  .A0(ADDRA[0]), .A1(ADDRA[1]), .A2(ADDRA[2]), .A3(ADDRA[3]), .A4(ADDRA[4]), .A5(ADDRA[5]), 
  .D(DINA[15]), .SPO(pre_douta_0[15]), .DPRA0(ADDRB[0]), .DPRA1(ADDRB[1]), .DPRA2(ADDRB[2]), 
  .DPRA3(ADDRB[3]), .DPRA4(ADDRB[4]), .DPRA5(ADDRB[5]), .DPO(pre_doutb_0[15]));
// Memory array element: COL: 0, ROW: 16
//  Initialization of Dual Port LUT ram now done through explicit
// parameter setting.
RAM64X1D#(.INIT(64'h0000000000000000))RAM64X1D_instance_2768(.WCLK(CLK), .WE(wea_0), 
  .A0(ADDRA[0]), .A1(ADDRA[1]), .A2(ADDRA[2]), .A3(ADDRA[3]), .A4(ADDRA[4]), .A5(ADDRA[5]), 
  .D(DINA[16]), .SPO(pre_douta_0[16]), .DPRA0(ADDRB[0]), .DPRA1(ADDRB[1]), .DPRA2(ADDRB[2]), 
  .DPRA3(ADDRB[3]), .DPRA4(ADDRB[4]), .DPRA5(ADDRB[5]), .DPO(pre_doutb_0[16]));
// Memory array element: COL: 0, ROW: 17
//  Initialization of Dual Port LUT ram now done through explicit
// parameter setting.
RAM64X1D#(.INIT(64'h0000000000000000))RAM64X1D_instance_2769(.WCLK(CLK), .WE(wea_0), 
  .A0(ADDRA[0]), .A1(ADDRA[1]), .A2(ADDRA[2]), .A3(ADDRA[3]), .A4(ADDRA[4]), .A5(ADDRA[5]), 
  .D(DINA[17]), .SPO(pre_douta_0[17]), .DPRA0(ADDRB[0]), .DPRA1(ADDRB[1]), .DPRA2(ADDRB[2]), 
  .DPRA3(ADDRB[3]), .DPRA4(ADDRB[4]), .DPRA5(ADDRB[5]), .DPO(pre_doutb_0[17]));
// Memory array element: COL: 0, ROW: 18
//  Initialization of Dual Port LUT ram now done through explicit
// parameter setting.
RAM64X1D#(.INIT(64'h0000000000000000))RAM64X1D_instance_2770(.WCLK(CLK), .WE(wea_0), 
  .A0(ADDRA[0]), .A1(ADDRA[1]), .A2(ADDRA[2]), .A3(ADDRA[3]), .A4(ADDRA[4]), .A5(ADDRA[5]), 
  .D(DINA[18]), .SPO(pre_douta_0[18]), .DPRA0(ADDRB[0]), .DPRA1(ADDRB[1]), .DPRA2(ADDRB[2]), 
  .DPRA3(ADDRB[3]), .DPRA4(ADDRB[4]), .DPRA5(ADDRB[5]), .DPO(pre_doutb_0[18]));
// Memory array element: COL: 0, ROW: 19
//  Initialization of Dual Port LUT ram now done through explicit
// parameter setting.
RAM64X1D#(.INIT(64'h0000000000000000))RAM64X1D_instance_2771(.WCLK(CLK), .WE(wea_0), 
  .A0(ADDRA[0]), .A1(ADDRA[1]), .A2(ADDRA[2]), .A3(ADDRA[3]), .A4(ADDRA[4]), .A5(ADDRA[5]), 
  .D(DINA[19]), .SPO(pre_douta_0[19]), .DPRA0(ADDRB[0]), .DPRA1(ADDRB[1]), .DPRA2(ADDRB[2]), 
  .DPRA3(ADDRB[3]), .DPRA4(ADDRB[4]), .DPRA5(ADDRB[5]), .DPO(pre_doutb_0[19]));
// Memory array element: COL: 0, ROW: 20
//  Initialization of Dual Port LUT ram now done through explicit
// parameter setting.
RAM64X1D#(.INIT(64'h0000000000000000))RAM64X1D_instance_2772(.WCLK(CLK), .WE(wea_0), 
  .A0(ADDRA[0]), .A1(ADDRA[1]), .A2(ADDRA[2]), .A3(ADDRA[3]), .A4(ADDRA[4]), .A5(ADDRA[5]), 
  .D(DINA[20]), .SPO(pre_douta_0[20]), .DPRA0(ADDRB[0]), .DPRA1(ADDRB[1]), .DPRA2(ADDRB[2]), 
  .DPRA3(ADDRB[3]), .DPRA4(ADDRB[4]), .DPRA5(ADDRB[5]), .DPO(pre_doutb_0[20]));
// Memory array element: COL: 0, ROW: 21
//  Initialization of Dual Port LUT ram now done through explicit
// parameter setting.
RAM64X1D#(.INIT(64'h0000000000000000))RAM64X1D_instance_2773(.WCLK(CLK), .WE(wea_0), 
  .A0(ADDRA[0]), .A1(ADDRA[1]), .A2(ADDRA[2]), .A3(ADDRA[3]), .A4(ADDRA[4]), .A5(ADDRA[5]), 
  .D(DINA[21]), .SPO(pre_douta_0[21]), .DPRA0(ADDRB[0]), .DPRA1(ADDRB[1]), .DPRA2(ADDRB[2]), 
  .DPRA3(ADDRB[3]), .DPRA4(ADDRB[4]), .DPRA5(ADDRB[5]), .DPO(pre_doutb_0[21]));
// Memory array element: COL: 0, ROW: 22
//  Initialization of Dual Port LUT ram now done through explicit
// parameter setting.
RAM64X1D#(.INIT(64'h0000000000000000))RAM64X1D_instance_2774(.WCLK(CLK), .WE(wea_0), 
  .A0(ADDRA[0]), .A1(ADDRA[1]), .A2(ADDRA[2]), .A3(ADDRA[3]), .A4(ADDRA[4]), .A5(ADDRA[5]), 
  .D(DINA[22]), .SPO(pre_douta_0[22]), .DPRA0(ADDRB[0]), .DPRA1(ADDRB[1]), .DPRA2(ADDRB[2]), 
  .DPRA3(ADDRB[3]), .DPRA4(ADDRB[4]), .DPRA5(ADDRB[5]), .DPO(pre_doutb_0[22]));
// Memory array element: COL: 0, ROW: 23
//  Initialization of Dual Port LUT ram now done through explicit
// parameter setting.
RAM64X1D#(.INIT(64'h0000000000000000))RAM64X1D_instance_2775(.WCLK(CLK), .WE(wea_0), 
  .A0(ADDRA[0]), .A1(ADDRA[1]), .A2(ADDRA[2]), .A3(ADDRA[3]), .A4(ADDRA[4]), .A5(ADDRA[5]), 
  .D(DINA[23]), .SPO(pre_douta_0[23]), .DPRA0(ADDRB[0]), .DPRA1(ADDRB[1]), .DPRA2(ADDRB[2]), 
  .DPRA3(ADDRB[3]), .DPRA4(ADDRB[4]), .DPRA5(ADDRB[5]), .DPO(pre_doutb_0[23]));
// Memory array element: COL: 0, ROW: 24
//  Initialization of Dual Port LUT ram now done through explicit
// parameter setting.
RAM64X1D#(.INIT(64'h0000000000000000))RAM64X1D_instance_2776(.WCLK(CLK), .WE(wea_0), 
  .A0(ADDRA[0]), .A1(ADDRA[1]), .A2(ADDRA[2]), .A3(ADDRA[3]), .A4(ADDRA[4]), .A5(ADDRA[5]), 
  .D(DINA[24]), .SPO(pre_douta_0[24]), .DPRA0(ADDRB[0]), .DPRA1(ADDRB[1]), .DPRA2(ADDRB[2]), 
  .DPRA3(ADDRB[3]), .DPRA4(ADDRB[4]), .DPRA5(ADDRB[5]), .DPO(pre_doutb_0[24]));
// Memory array element: COL: 0, ROW: 25
//  Initialization of Dual Port LUT ram now done through explicit
// parameter setting.
RAM64X1D#(.INIT(64'h0000000000000000))RAM64X1D_instance_2777(.WCLK(CLK), .WE(wea_0), 
  .A0(ADDRA[0]), .A1(ADDRA[1]), .A2(ADDRA[2]), .A3(ADDRA[3]), .A4(ADDRA[4]), .A5(ADDRA[5]), 
  .D(DINA[25]), .SPO(pre_douta_0[25]), .DPRA0(ADDRB[0]), .DPRA1(ADDRB[1]), .DPRA2(ADDRB[2]), 
  .DPRA3(ADDRB[3]), .DPRA4(ADDRB[4]), .DPRA5(ADDRB[5]), .DPO(pre_doutb_0[25]));
// Memory array element: COL: 0, ROW: 26
//  Initialization of Dual Port LUT ram now done through explicit
// parameter setting.
RAM64X1D#(.INIT(64'h0000000000000000))RAM64X1D_instance_2778(.WCLK(CLK), .WE(wea_0), 
  .A0(ADDRA[0]), .A1(ADDRA[1]), .A2(ADDRA[2]), .A3(ADDRA[3]), .A4(ADDRA[4]), .A5(ADDRA[5]), 
  .D(DINA[26]), .SPO(pre_douta_0[26]), .DPRA0(ADDRB[0]), .DPRA1(ADDRB[1]), .DPRA2(ADDRB[2]), 
  .DPRA3(ADDRB[3]), .DPRA4(ADDRB[4]), .DPRA5(ADDRB[5]), .DPO(pre_doutb_0[26]));
// Memory array element: COL: 0, ROW: 27
//  Initialization of Dual Port LUT ram now done through explicit
// parameter setting.
RAM64X1D#(.INIT(64'h0000000000000000))RAM64X1D_instance_2779(.WCLK(CLK), .WE(wea_0), 
  .A0(ADDRA[0]), .A1(ADDRA[1]), .A2(ADDRA[2]), .A3(ADDRA[3]), .A4(ADDRA[4]), .A5(ADDRA[5]), 
  .D(DINA[27]), .SPO(pre_douta_0[27]), .DPRA0(ADDRB[0]), .DPRA1(ADDRB[1]), .DPRA2(ADDRB[2]), 
  .DPRA3(ADDRB[3]), .DPRA4(ADDRB[4]), .DPRA5(ADDRB[5]), .DPO(pre_doutb_0[27]));
// Memory array element: COL: 0, ROW: 28
//  Initialization of Dual Port LUT ram now done through explicit
// parameter setting.
RAM64X1D#(.INIT(64'h0000000000000000))RAM64X1D_instance_2780(.WCLK(CLK), .WE(wea_0), 
  .A0(ADDRA[0]), .A1(ADDRA[1]), .A2(ADDRA[2]), .A3(ADDRA[3]), .A4(ADDRA[4]), .A5(ADDRA[5]), 
  .D(DINA[28]), .SPO(pre_douta_0[28]), .DPRA0(ADDRB[0]), .DPRA1(ADDRB[1]), .DPRA2(ADDRB[2]), 
  .DPRA3(ADDRB[3]), .DPRA4(ADDRB[4]), .DPRA5(ADDRB[5]), .DPO(pre_doutb_0[28]));
// Memory array element: COL: 0, ROW: 29
//  Initialization of Dual Port LUT ram now done through explicit
// parameter setting.
RAM64X1D#(.INIT(64'h0000000000000000))RAM64X1D_instance_2781(.WCLK(CLK), .WE(wea_0), 
  .A0(ADDRA[0]), .A1(ADDRA[1]), .A2(ADDRA[2]), .A3(ADDRA[3]), .A4(ADDRA[4]), .A5(ADDRA[5]), 
  .D(DINA[29]), .SPO(pre_douta_0[29]), .DPRA0(ADDRB[0]), .DPRA1(ADDRB[1]), .DPRA2(ADDRB[2]), 
  .DPRA3(ADDRB[3]), .DPRA4(ADDRB[4]), .DPRA5(ADDRB[5]), .DPO(pre_doutb_0[29]));
// Memory array element: COL: 0, ROW: 30
//  Initialization of Dual Port LUT ram now done through explicit
// parameter setting.
RAM64X1D#(.INIT(64'h0000000000000000))RAM64X1D_instance_2782(.WCLK(CLK), .WE(wea_0), 
  .A0(ADDRA[0]), .A1(ADDRA[1]), .A2(ADDRA[2]), .A3(ADDRA[3]), .A4(ADDRA[4]), .A5(ADDRA[5]), 
  .D(DINA[30]), .SPO(pre_douta_0[30]), .DPRA0(ADDRB[0]), .DPRA1(ADDRB[1]), .DPRA2(ADDRB[2]), 
  .DPRA3(ADDRB[3]), .DPRA4(ADDRB[4]), .DPRA5(ADDRB[5]), .DPO(pre_doutb_0[30]));
// Memory array element: COL: 0, ROW: 31
//  Initialization of Dual Port LUT ram now done through explicit
// parameter setting.
RAM64X1D#(.INIT(64'h0000000000000000))RAM64X1D_instance_2783(.WCLK(CLK), .WE(wea_0), 
  .A0(ADDRA[0]), .A1(ADDRA[1]), .A2(ADDRA[2]), .A3(ADDRA[3]), .A4(ADDRA[4]), .A5(ADDRA[5]), 
  .D(DINA[31]), .SPO(pre_douta_0[31]), .DPRA0(ADDRB[0]), .DPRA1(ADDRB[1]), .DPRA2(ADDRB[2]), 
  .DPRA3(ADDRB[3]), .DPRA4(ADDRB[4]), .DPRA5(ADDRB[5]), .DPO(pre_doutb_0[31]));
// Memory array element: COL: 1, ROW: 0
//  Initialization of Dual Port LUT ram now done through explicit
// parameter setting.
RAM64X1D#(.INIT(64'h0000000000000000))RAM64X1D_instance_2784(.WCLK(CLK), .WE(wea_1), 
  .A0(ADDRA[0]), .A1(ADDRA[1]), .A2(ADDRA[2]), .A3(ADDRA[3]), .A4(ADDRA[4]), .A5(ADDRA[5]), 
  .D(DINA[0]), .SPO(pre_douta_1[0]), .DPRA0(ADDRB[0]), .DPRA1(ADDRB[1]), .DPRA2(ADDRB[2]), 
  .DPRA3(ADDRB[3]), .DPRA4(ADDRB[4]), .DPRA5(ADDRB[5]), .DPO(pre_doutb_1[0]));
// Memory array element: COL: 1, ROW: 1
//  Initialization of Dual Port LUT ram now done through explicit
// parameter setting.
RAM64X1D#(.INIT(64'h0000000000000000))RAM64X1D_instance_2785(.WCLK(CLK), .WE(wea_1), 
  .A0(ADDRA[0]), .A1(ADDRA[1]), .A2(ADDRA[2]), .A3(ADDRA[3]), .A4(ADDRA[4]), .A5(ADDRA[5]), 
  .D(DINA[1]), .SPO(pre_douta_1[1]), .DPRA0(ADDRB[0]), .DPRA1(ADDRB[1]), .DPRA2(ADDRB[2]), 
  .DPRA3(ADDRB[3]), .DPRA4(ADDRB[4]), .DPRA5(ADDRB[5]), .DPO(pre_doutb_1[1]));
// Memory array element: COL: 1, ROW: 2
//  Initialization of Dual Port LUT ram now done through explicit
// parameter setting.
RAM64X1D#(.INIT(64'h0000000000000000))RAM64X1D_instance_2786(.WCLK(CLK), .WE(wea_1), 
  .A0(ADDRA[0]), .A1(ADDRA[1]), .A2(ADDRA[2]), .A3(ADDRA[3]), .A4(ADDRA[4]), .A5(ADDRA[5]), 
  .D(DINA[2]), .SPO(pre_douta_1[2]), .DPRA0(ADDRB[0]), .DPRA1(ADDRB[1]), .DPRA2(ADDRB[2]), 
  .DPRA3(ADDRB[3]), .DPRA4(ADDRB[4]), .DPRA5(ADDRB[5]), .DPO(pre_doutb_1[2]));
// Memory array element: COL: 1, ROW: 3
//  Initialization of Dual Port LUT ram now done through explicit
// parameter setting.
RAM64X1D#(.INIT(64'h0000000000000000))RAM64X1D_instance_2787(.WCLK(CLK), .WE(wea_1), 
  .A0(ADDRA[0]), .A1(ADDRA[1]), .A2(ADDRA[2]), .A3(ADDRA[3]), .A4(ADDRA[4]), .A5(ADDRA[5]), 
  .D(DINA[3]), .SPO(pre_douta_1[3]), .DPRA0(ADDRB[0]), .DPRA1(ADDRB[1]), .DPRA2(ADDRB[2]), 
  .DPRA3(ADDRB[3]), .DPRA4(ADDRB[4]), .DPRA5(ADDRB[5]), .DPO(pre_doutb_1[3]));
// Memory array element: COL: 1, ROW: 4
//  Initialization of Dual Port LUT ram now done through explicit
// parameter setting.
RAM64X1D#(.INIT(64'h0000000000000000))RAM64X1D_instance_2788(.WCLK(CLK), .WE(wea_1), 
  .A0(ADDRA[0]), .A1(ADDRA[1]), .A2(ADDRA[2]), .A3(ADDRA[3]), .A4(ADDRA[4]), .A5(ADDRA[5]), 
  .D(DINA[4]), .SPO(pre_douta_1[4]), .DPRA0(ADDRB[0]), .DPRA1(ADDRB[1]), .DPRA2(ADDRB[2]), 
  .DPRA3(ADDRB[3]), .DPRA4(ADDRB[4]), .DPRA5(ADDRB[5]), .DPO(pre_doutb_1[4]));
// Memory array element: COL: 1, ROW: 5
//  Initialization of Dual Port LUT ram now done through explicit
// parameter setting.
RAM64X1D#(.INIT(64'h0000000000000000))RAM64X1D_instance_2789(.WCLK(CLK), .WE(wea_1), 
  .A0(ADDRA[0]), .A1(ADDRA[1]), .A2(ADDRA[2]), .A3(ADDRA[3]), .A4(ADDRA[4]), .A5(ADDRA[5]), 
  .D(DINA[5]), .SPO(pre_douta_1[5]), .DPRA0(ADDRB[0]), .DPRA1(ADDRB[1]), .DPRA2(ADDRB[2]), 
  .DPRA3(ADDRB[3]), .DPRA4(ADDRB[4]), .DPRA5(ADDRB[5]), .DPO(pre_doutb_1[5]));
// Memory array element: COL: 1, ROW: 6
//  Initialization of Dual Port LUT ram now done through explicit
// parameter setting.
RAM64X1D#(.INIT(64'h0000000000000000))RAM64X1D_instance_2790(.WCLK(CLK), .WE(wea_1), 
  .A0(ADDRA[0]), .A1(ADDRA[1]), .A2(ADDRA[2]), .A3(ADDRA[3]), .A4(ADDRA[4]), .A5(ADDRA[5]), 
  .D(DINA[6]), .SPO(pre_douta_1[6]), .DPRA0(ADDRB[0]), .DPRA1(ADDRB[1]), .DPRA2(ADDRB[2]), 
  .DPRA3(ADDRB[3]), .DPRA4(ADDRB[4]), .DPRA5(ADDRB[5]), .DPO(pre_doutb_1[6]));
// Memory array element: COL: 1, ROW: 7
//  Initialization of Dual Port LUT ram now done through explicit
// parameter setting.
RAM64X1D#(.INIT(64'h0000000000000000))RAM64X1D_instance_2791(.WCLK(CLK), .WE(wea_1), 
  .A0(ADDRA[0]), .A1(ADDRA[1]), .A2(ADDRA[2]), .A3(ADDRA[3]), .A4(ADDRA[4]), .A5(ADDRA[5]), 
  .D(DINA[7]), .SPO(pre_douta_1[7]), .DPRA0(ADDRB[0]), .DPRA1(ADDRB[1]), .DPRA2(ADDRB[2]), 
  .DPRA3(ADDRB[3]), .DPRA4(ADDRB[4]), .DPRA5(ADDRB[5]), .DPO(pre_doutb_1[7]));
// Memory array element: COL: 1, ROW: 8
//  Initialization of Dual Port LUT ram now done through explicit
// parameter setting.
RAM64X1D#(.INIT(64'h0000000000000000))RAM64X1D_instance_2792(.WCLK(CLK), .WE(wea_1), 
  .A0(ADDRA[0]), .A1(ADDRA[1]), .A2(ADDRA[2]), .A3(ADDRA[3]), .A4(ADDRA[4]), .A5(ADDRA[5]), 
  .D(DINA[8]), .SPO(pre_douta_1[8]), .DPRA0(ADDRB[0]), .DPRA1(ADDRB[1]), .DPRA2(ADDRB[2]), 
  .DPRA3(ADDRB[3]), .DPRA4(ADDRB[4]), .DPRA5(ADDRB[5]), .DPO(pre_doutb_1[8]));
// Memory array element: COL: 1, ROW: 9
//  Initialization of Dual Port LUT ram now done through explicit
// parameter setting.
RAM64X1D#(.INIT(64'h0000000000000000))RAM64X1D_instance_2793(.WCLK(CLK), .WE(wea_1), 
  .A0(ADDRA[0]), .A1(ADDRA[1]), .A2(ADDRA[2]), .A3(ADDRA[3]), .A4(ADDRA[4]), .A5(ADDRA[5]), 
  .D(DINA[9]), .SPO(pre_douta_1[9]), .DPRA0(ADDRB[0]), .DPRA1(ADDRB[1]), .DPRA2(ADDRB[2]), 
  .DPRA3(ADDRB[3]), .DPRA4(ADDRB[4]), .DPRA5(ADDRB[5]), .DPO(pre_doutb_1[9]));
// Memory array element: COL: 1, ROW: 10
//  Initialization of Dual Port LUT ram now done through explicit
// parameter setting.
RAM64X1D#(.INIT(64'h0000000000000000))RAM64X1D_instance_2794(.WCLK(CLK), .WE(wea_1), 
  .A0(ADDRA[0]), .A1(ADDRA[1]), .A2(ADDRA[2]), .A3(ADDRA[3]), .A4(ADDRA[4]), .A5(ADDRA[5]), 
  .D(DINA[10]), .SPO(pre_douta_1[10]), .DPRA0(ADDRB[0]), .DPRA1(ADDRB[1]), .DPRA2(ADDRB[2]), 
  .DPRA3(ADDRB[3]), .DPRA4(ADDRB[4]), .DPRA5(ADDRB[5]), .DPO(pre_doutb_1[10]));
// Memory array element: COL: 1, ROW: 11
//  Initialization of Dual Port LUT ram now done through explicit
// parameter setting.
RAM64X1D#(.INIT(64'h0000000000000000))RAM64X1D_instance_2795(.WCLK(CLK), .WE(wea_1), 
  .A0(ADDRA[0]), .A1(ADDRA[1]), .A2(ADDRA[2]), .A3(ADDRA[3]), .A4(ADDRA[4]), .A5(ADDRA[5]), 
  .D(DINA[11]), .SPO(pre_douta_1[11]), .DPRA0(ADDRB[0]), .DPRA1(ADDRB[1]), .DPRA2(ADDRB[2]), 
  .DPRA3(ADDRB[3]), .DPRA4(ADDRB[4]), .DPRA5(ADDRB[5]), .DPO(pre_doutb_1[11]));
// Memory array element: COL: 1, ROW: 12
//  Initialization of Dual Port LUT ram now done through explicit
// parameter setting.
RAM64X1D#(.INIT(64'h0000000000000000))RAM64X1D_instance_2796(.WCLK(CLK), .WE(wea_1), 
  .A0(ADDRA[0]), .A1(ADDRA[1]), .A2(ADDRA[2]), .A3(ADDRA[3]), .A4(ADDRA[4]), .A5(ADDRA[5]), 
  .D(DINA[12]), .SPO(pre_douta_1[12]), .DPRA0(ADDRB[0]), .DPRA1(ADDRB[1]), .DPRA2(ADDRB[2]), 
  .DPRA3(ADDRB[3]), .DPRA4(ADDRB[4]), .DPRA5(ADDRB[5]), .DPO(pre_doutb_1[12]));
// Memory array element: COL: 1, ROW: 13
//  Initialization of Dual Port LUT ram now done through explicit
// parameter setting.
RAM64X1D#(.INIT(64'h0000000000000000))RAM64X1D_instance_2797(.WCLK(CLK), .WE(wea_1), 
  .A0(ADDRA[0]), .A1(ADDRA[1]), .A2(ADDRA[2]), .A3(ADDRA[3]), .A4(ADDRA[4]), .A5(ADDRA[5]), 
  .D(DINA[13]), .SPO(pre_douta_1[13]), .DPRA0(ADDRB[0]), .DPRA1(ADDRB[1]), .DPRA2(ADDRB[2]), 
  .DPRA3(ADDRB[3]), .DPRA4(ADDRB[4]), .DPRA5(ADDRB[5]), .DPO(pre_doutb_1[13]));
// Memory array element: COL: 1, ROW: 14
//  Initialization of Dual Port LUT ram now done through explicit
// parameter setting.
RAM64X1D#(.INIT(64'h0000000000000000))RAM64X1D_instance_2798(.WCLK(CLK), .WE(wea_1), 
  .A0(ADDRA[0]), .A1(ADDRA[1]), .A2(ADDRA[2]), .A3(ADDRA[3]), .A4(ADDRA[4]), .A5(ADDRA[5]), 
  .D(DINA[14]), .SPO(pre_douta_1[14]), .DPRA0(ADDRB[0]), .DPRA1(ADDRB[1]), .DPRA2(ADDRB[2]), 
  .DPRA3(ADDRB[3]), .DPRA4(ADDRB[4]), .DPRA5(ADDRB[5]), .DPO(pre_doutb_1[14]));
// Memory array element: COL: 1, ROW: 15
//  Initialization of Dual Port LUT ram now done through explicit
// parameter setting.
RAM64X1D#(.INIT(64'h0000000000000000))RAM64X1D_instance_2799(.WCLK(CLK), .WE(wea_1), 
  .A0(ADDRA[0]), .A1(ADDRA[1]), .A2(ADDRA[2]), .A3(ADDRA[3]), .A4(ADDRA[4]), .A5(ADDRA[5]), 
  .D(DINA[15]), .SPO(pre_douta_1[15]), .DPRA0(ADDRB[0]), .DPRA1(ADDRB[1]), .DPRA2(ADDRB[2]), 
  .DPRA3(ADDRB[3]), .DPRA4(ADDRB[4]), .DPRA5(ADDRB[5]), .DPO(pre_doutb_1[15]));
// Memory array element: COL: 1, ROW: 16
//  Initialization of Dual Port LUT ram now done through explicit
// parameter setting.
RAM64X1D#(.INIT(64'h0000000000000000))RAM64X1D_instance_2800(.WCLK(CLK), .WE(wea_1), 
  .A0(ADDRA[0]), .A1(ADDRA[1]), .A2(ADDRA[2]), .A3(ADDRA[3]), .A4(ADDRA[4]), .A5(ADDRA[5]), 
  .D(DINA[16]), .SPO(pre_douta_1[16]), .DPRA0(ADDRB[0]), .DPRA1(ADDRB[1]), .DPRA2(ADDRB[2]), 
  .DPRA3(ADDRB[3]), .DPRA4(ADDRB[4]), .DPRA5(ADDRB[5]), .DPO(pre_doutb_1[16]));
// Memory array element: COL: 1, ROW: 17
//  Initialization of Dual Port LUT ram now done through explicit
// parameter setting.
RAM64X1D#(.INIT(64'h0000000000000000))RAM64X1D_instance_2801(.WCLK(CLK), .WE(wea_1), 
  .A0(ADDRA[0]), .A1(ADDRA[1]), .A2(ADDRA[2]), .A3(ADDRA[3]), .A4(ADDRA[4]), .A5(ADDRA[5]), 
  .D(DINA[17]), .SPO(pre_douta_1[17]), .DPRA0(ADDRB[0]), .DPRA1(ADDRB[1]), .DPRA2(ADDRB[2]), 
  .DPRA3(ADDRB[3]), .DPRA4(ADDRB[4]), .DPRA5(ADDRB[5]), .DPO(pre_doutb_1[17]));
// Memory array element: COL: 1, ROW: 18
//  Initialization of Dual Port LUT ram now done through explicit
// parameter setting.
RAM64X1D#(.INIT(64'h0000000000000000))RAM64X1D_instance_2802(.WCLK(CLK), .WE(wea_1), 
  .A0(ADDRA[0]), .A1(ADDRA[1]), .A2(ADDRA[2]), .A3(ADDRA[3]), .A4(ADDRA[4]), .A5(ADDRA[5]), 
  .D(DINA[18]), .SPO(pre_douta_1[18]), .DPRA0(ADDRB[0]), .DPRA1(ADDRB[1]), .DPRA2(ADDRB[2]), 
  .DPRA3(ADDRB[3]), .DPRA4(ADDRB[4]), .DPRA5(ADDRB[5]), .DPO(pre_doutb_1[18]));
// Memory array element: COL: 1, ROW: 19
//  Initialization of Dual Port LUT ram now done through explicit
// parameter setting.
RAM64X1D#(.INIT(64'h0000000000000000))RAM64X1D_instance_2803(.WCLK(CLK), .WE(wea_1), 
  .A0(ADDRA[0]), .A1(ADDRA[1]), .A2(ADDRA[2]), .A3(ADDRA[3]), .A4(ADDRA[4]), .A5(ADDRA[5]), 
  .D(DINA[19]), .SPO(pre_douta_1[19]), .DPRA0(ADDRB[0]), .DPRA1(ADDRB[1]), .DPRA2(ADDRB[2]), 
  .DPRA3(ADDRB[3]), .DPRA4(ADDRB[4]), .DPRA5(ADDRB[5]), .DPO(pre_doutb_1[19]));
// Memory array element: COL: 1, ROW: 20
//  Initialization of Dual Port LUT ram now done through explicit
// parameter setting.
RAM64X1D#(.INIT(64'h0000000000000000))RAM64X1D_instance_2804(.WCLK(CLK), .WE(wea_1), 
  .A0(ADDRA[0]), .A1(ADDRA[1]), .A2(ADDRA[2]), .A3(ADDRA[3]), .A4(ADDRA[4]), .A5(ADDRA[5]), 
  .D(DINA[20]), .SPO(pre_douta_1[20]), .DPRA0(ADDRB[0]), .DPRA1(ADDRB[1]), .DPRA2(ADDRB[2]), 
  .DPRA3(ADDRB[3]), .DPRA4(ADDRB[4]), .DPRA5(ADDRB[5]), .DPO(pre_doutb_1[20]));
// Memory array element: COL: 1, ROW: 21
//  Initialization of Dual Port LUT ram now done through explicit
// parameter setting.
RAM64X1D#(.INIT(64'h0000000000000000))RAM64X1D_instance_2805(.WCLK(CLK), .WE(wea_1), 
  .A0(ADDRA[0]), .A1(ADDRA[1]), .A2(ADDRA[2]), .A3(ADDRA[3]), .A4(ADDRA[4]), .A5(ADDRA[5]), 
  .D(DINA[21]), .SPO(pre_douta_1[21]), .DPRA0(ADDRB[0]), .DPRA1(ADDRB[1]), .DPRA2(ADDRB[2]), 
  .DPRA3(ADDRB[3]), .DPRA4(ADDRB[4]), .DPRA5(ADDRB[5]), .DPO(pre_doutb_1[21]));
// Memory array element: COL: 1, ROW: 22
//  Initialization of Dual Port LUT ram now done through explicit
// parameter setting.
RAM64X1D#(.INIT(64'h0000000000000000))RAM64X1D_instance_2806(.WCLK(CLK), .WE(wea_1), 
  .A0(ADDRA[0]), .A1(ADDRA[1]), .A2(ADDRA[2]), .A3(ADDRA[3]), .A4(ADDRA[4]), .A5(ADDRA[5]), 
  .D(DINA[22]), .SPO(pre_douta_1[22]), .DPRA0(ADDRB[0]), .DPRA1(ADDRB[1]), .DPRA2(ADDRB[2]), 
  .DPRA3(ADDRB[3]), .DPRA4(ADDRB[4]), .DPRA5(ADDRB[5]), .DPO(pre_doutb_1[22]));
// Memory array element: COL: 1, ROW: 23
//  Initialization of Dual Port LUT ram now done through explicit
// parameter setting.
RAM64X1D#(.INIT(64'h0000000000000000))RAM64X1D_instance_2807(.WCLK(CLK), .WE(wea_1), 
  .A0(ADDRA[0]), .A1(ADDRA[1]), .A2(ADDRA[2]), .A3(ADDRA[3]), .A4(ADDRA[4]), .A5(ADDRA[5]), 
  .D(DINA[23]), .SPO(pre_douta_1[23]), .DPRA0(ADDRB[0]), .DPRA1(ADDRB[1]), .DPRA2(ADDRB[2]), 
  .DPRA3(ADDRB[3]), .DPRA4(ADDRB[4]), .DPRA5(ADDRB[5]), .DPO(pre_doutb_1[23]));
// Memory array element: COL: 1, ROW: 24
//  Initialization of Dual Port LUT ram now done through explicit
// parameter setting.
RAM64X1D#(.INIT(64'h0000000000000000))RAM64X1D_instance_2808(.WCLK(CLK), .WE(wea_1), 
  .A0(ADDRA[0]), .A1(ADDRA[1]), .A2(ADDRA[2]), .A3(ADDRA[3]), .A4(ADDRA[4]), .A5(ADDRA[5]), 
  .D(DINA[24]), .SPO(pre_douta_1[24]), .DPRA0(ADDRB[0]), .DPRA1(ADDRB[1]), .DPRA2(ADDRB[2]), 
  .DPRA3(ADDRB[3]), .DPRA4(ADDRB[4]), .DPRA5(ADDRB[5]), .DPO(pre_doutb_1[24]));
// Memory array element: COL: 1, ROW: 25
//  Initialization of Dual Port LUT ram now done through explicit
// parameter setting.
RAM64X1D#(.INIT(64'h0000000000000000))RAM64X1D_instance_2809(.WCLK(CLK), .WE(wea_1), 
  .A0(ADDRA[0]), .A1(ADDRA[1]), .A2(ADDRA[2]), .A3(ADDRA[3]), .A4(ADDRA[4]), .A5(ADDRA[5]), 
  .D(DINA[25]), .SPO(pre_douta_1[25]), .DPRA0(ADDRB[0]), .DPRA1(ADDRB[1]), .DPRA2(ADDRB[2]), 
  .DPRA3(ADDRB[3]), .DPRA4(ADDRB[4]), .DPRA5(ADDRB[5]), .DPO(pre_doutb_1[25]));
// Memory array element: COL: 1, ROW: 26
//  Initialization of Dual Port LUT ram now done through explicit
// parameter setting.
RAM64X1D#(.INIT(64'h0000000000000000))RAM64X1D_instance_2810(.WCLK(CLK), .WE(wea_1), 
  .A0(ADDRA[0]), .A1(ADDRA[1]), .A2(ADDRA[2]), .A3(ADDRA[3]), .A4(ADDRA[4]), .A5(ADDRA[5]), 
  .D(DINA[26]), .SPO(pre_douta_1[26]), .DPRA0(ADDRB[0]), .DPRA1(ADDRB[1]), .DPRA2(ADDRB[2]), 
  .DPRA3(ADDRB[3]), .DPRA4(ADDRB[4]), .DPRA5(ADDRB[5]), .DPO(pre_doutb_1[26]));
// Memory array element: COL: 1, ROW: 27
//  Initialization of Dual Port LUT ram now done through explicit
// parameter setting.
RAM64X1D#(.INIT(64'h0000000000000000))RAM64X1D_instance_2811(.WCLK(CLK), .WE(wea_1), 
  .A0(ADDRA[0]), .A1(ADDRA[1]), .A2(ADDRA[2]), .A3(ADDRA[3]), .A4(ADDRA[4]), .A5(ADDRA[5]), 
  .D(DINA[27]), .SPO(pre_douta_1[27]), .DPRA0(ADDRB[0]), .DPRA1(ADDRB[1]), .DPRA2(ADDRB[2]), 
  .DPRA3(ADDRB[3]), .DPRA4(ADDRB[4]), .DPRA5(ADDRB[5]), .DPO(pre_doutb_1[27]));
// Memory array element: COL: 1, ROW: 28
//  Initialization of Dual Port LUT ram now done through explicit
// parameter setting.
RAM64X1D#(.INIT(64'h0000000000000000))RAM64X1D_instance_2812(.WCLK(CLK), .WE(wea_1), 
  .A0(ADDRA[0]), .A1(ADDRA[1]), .A2(ADDRA[2]), .A3(ADDRA[3]), .A4(ADDRA[4]), .A5(ADDRA[5]), 
  .D(DINA[28]), .SPO(pre_douta_1[28]), .DPRA0(ADDRB[0]), .DPRA1(ADDRB[1]), .DPRA2(ADDRB[2]), 
  .DPRA3(ADDRB[3]), .DPRA4(ADDRB[4]), .DPRA5(ADDRB[5]), .DPO(pre_doutb_1[28]));
// Memory array element: COL: 1, ROW: 29
//  Initialization of Dual Port LUT ram now done through explicit
// parameter setting.
RAM64X1D#(.INIT(64'h0000000000000000))RAM64X1D_instance_2813(.WCLK(CLK), .WE(wea_1), 
  .A0(ADDRA[0]), .A1(ADDRA[1]), .A2(ADDRA[2]), .A3(ADDRA[3]), .A4(ADDRA[4]), .A5(ADDRA[5]), 
  .D(DINA[29]), .SPO(pre_douta_1[29]), .DPRA0(ADDRB[0]), .DPRA1(ADDRB[1]), .DPRA2(ADDRB[2]), 
  .DPRA3(ADDRB[3]), .DPRA4(ADDRB[4]), .DPRA5(ADDRB[5]), .DPO(pre_doutb_1[29]));
// Memory array element: COL: 1, ROW: 30
//  Initialization of Dual Port LUT ram now done through explicit
// parameter setting.
RAM64X1D#(.INIT(64'h0000000000000000))RAM64X1D_instance_2814(.WCLK(CLK), .WE(wea_1), 
  .A0(ADDRA[0]), .A1(ADDRA[1]), .A2(ADDRA[2]), .A3(ADDRA[3]), .A4(ADDRA[4]), .A5(ADDRA[5]), 
  .D(DINA[30]), .SPO(pre_douta_1[30]), .DPRA0(ADDRB[0]), .DPRA1(ADDRB[1]), .DPRA2(ADDRB[2]), 
  .DPRA3(ADDRB[3]), .DPRA4(ADDRB[4]), .DPRA5(ADDRB[5]), .DPO(pre_doutb_1[30]));
// Memory array element: COL: 1, ROW: 31
//  Initialization of Dual Port LUT ram now done through explicit
// parameter setting.
RAM64X1D#(.INIT(64'h0000000000000000))RAM64X1D_instance_2815(.WCLK(CLK), .WE(wea_1), 
  .A0(ADDRA[0]), .A1(ADDRA[1]), .A2(ADDRA[2]), .A3(ADDRA[3]), .A4(ADDRA[4]), .A5(ADDRA[5]), 
  .D(DINA[31]), .SPO(pre_douta_1[31]), .DPRA0(ADDRB[0]), .DPRA1(ADDRB[1]), .DPRA2(ADDRB[2]), 
  .DPRA3(ADDRB[3]), .DPRA4(ADDRB[4]), .DPRA5(ADDRB[5]), .DPO(pre_doutb_1[31]));
endmodule



module medianRow4_structuralmemory_325af1a9_(CLK_u89, bus_1cc9c3d9_, bus_131c3e71_, bus_51fb455d_, bus_29cfd5d3_, bus_5be914d7_, bus_2b1d0d63_, bus_53a86a59_, bus_5711d689_, bus_3fb70c0a_, bus_197064a7_, bus_5d0c87b5_, bus_60d7e1d0_, bus_2463ba1f_);
input		CLK_u89;
input		bus_1cc9c3d9_;
input	[31:0]	bus_131c3e71_;
input	[2:0]	bus_51fb455d_;
input		bus_29cfd5d3_;
input	[31:0]	bus_5be914d7_;
input	[2:0]	bus_2b1d0d63_;
input		bus_53a86a59_;
input		bus_5711d689_;
input	[31:0]	bus_3fb70c0a_;
output	[31:0]	bus_197064a7_;
output		bus_5d0c87b5_;
output	[31:0]	bus_60d7e1d0_;
output		bus_2463ba1f_;
wire		and_1ecb740e_u0;
wire		not_4987bce0_u0;
reg		logicalMem_138c535d_we_delay0_u0=1'h0;
wire	[31:0]	bus_7c1f0485_;
wire	[31:0]	bus_73a8cfd3_;
wire		or_2fbb9f89_u0;
wire		or_4c30910f_u0;
assign bus_197064a7_=bus_7c1f0485_;
assign bus_5d0c87b5_=bus_29cfd5d3_;
assign bus_60d7e1d0_=bus_73a8cfd3_;
assign bus_2463ba1f_=or_4c30910f_u0;
assign and_1ecb740e_u0=bus_53a86a59_&not_4987bce0_u0;
assign not_4987bce0_u0=~bus_5711d689_;
always @(posedge CLK_u89 or posedge bus_1cc9c3d9_)
begin
if (bus_1cc9c3d9_)
logicalMem_138c535d_we_delay0_u0<=1'h0;
else logicalMem_138c535d_we_delay0_u0<=bus_5711d689_;
end
medianRow4_forge_memory_101x32_139 medianRow4_forge_memory_101x32_139_instance0(.CLK(CLK_u89), 
  .ENA(or_2fbb9f89_u0), .WEA(bus_5711d689_), .DINA(bus_3fb70c0a_), .ADDRA(bus_5be914d7_), 
  .DOUTA(bus_73a8cfd3_), .DONEA(), .ENB(bus_29cfd5d3_), .ADDRB(bus_131c3e71_), .DOUTB(bus_7c1f0485_), 
  .DONEB());
assign or_2fbb9f89_u0=bus_53a86a59_|bus_5711d689_;
assign or_4c30910f_u0=and_1ecb740e_u0|logicalMem_138c535d_we_delay0_u0;
endmodule



module medianRow4_globalreset_physical_3f2bad1e_(bus_1dcbf8f2_, bus_5ddb6052_, bus_766ee8ce_);
input		bus_1dcbf8f2_;
input		bus_5ddb6052_;
output		bus_766ee8ce_;
reg		sample_u53=1'h0;
wire		not_200855da_u0;
wire		or_1c2661b5_u0;
wire		and_42c30046_u0;
reg		glitch_u53=1'h0;
reg		final_u53=1'h1;
reg		cross_u53=1'h0;
always @(posedge bus_1dcbf8f2_)
begin
sample_u53<=1'h1;
end
assign not_200855da_u0=~and_42c30046_u0;
assign or_1c2661b5_u0=bus_5ddb6052_|final_u53;
assign and_42c30046_u0=cross_u53&glitch_u53;
always @(posedge bus_1dcbf8f2_)
begin
glitch_u53<=cross_u53;
end
always @(posedge bus_1dcbf8f2_)
begin
final_u53<=not_200855da_u0;
end
assign bus_766ee8ce_=or_1c2661b5_u0;
always @(posedge bus_1dcbf8f2_)
begin
cross_u53<=sample_u53;
end
endmodule



module medianRow4_scheduler(CLK, RESET, GO, port_6e1723e8_, port_5b5d1610_, port_72a70685_, port_26f7a5b8_, port_092bee70_, port_777686a0_, RESULT, RESULT_u2009, RESULT_u2010, RESULT_u2011, DONE);
input		CLK;
input		RESET;
input		GO;
input		port_6e1723e8_;
input	[31:0]	port_5b5d1610_;
input		port_72a70685_;
input		port_26f7a5b8_;
input		port_092bee70_;
input		port_777686a0_;
output		RESULT;
output		RESULT_u2009;
output		RESULT_u2010;
output		RESULT_u2011;
output		DONE;
wire		lessThan;
wire signed	[31:0]	lessThan_a_signed;
wire signed	[31:0]	lessThan_b_signed;
wire signed	[31:0]	equals_a_signed;
wire		equals;
wire signed	[31:0]	equals_b_signed;
wire signed	[31:0]	equals_u197_a_signed;
wire		equals_u197;
wire signed	[31:0]	equals_u197_b_signed;
wire		not_u746_u0;
wire		and_u3321_u0;
wire		and_u3322_u0;
wire		andOp;
wire		and_u3323_u0;
wire		and_u3324_u0;
wire		not_u747_u0;
wire		simplePinWrite;
wire		and_u3325_u0;
wire		and_u3326_u0;
wire signed	[31:0]	equals_u198_b_signed;
wire		equals_u198;
wire signed	[31:0]	equals_u198_a_signed;
wire		and_u3327_u0;
wire		and_u3328_u0;
wire		not_u748_u0;
wire		andOp_u62;
wire		not_u749_u0;
wire		and_u3329_u0;
wire		and_u3330_u0;
wire		simplePinWrite_u470;
wire		and_u3331_u0;
wire		and_u3332_u0;
wire		not_u750_u0;
wire		and_u3333_u0;
wire		and_u3334_u0;
wire		not_u751_u0;
wire		simplePinWrite_u471;
wire		and_u3335_u0;
reg		and_delayed_u379=1'h0;
wire		or_u1378_u0;
wire		and_u3336_u0;
wire		or_u1379_u0;
wire		and_u3337_u0;
wire		and_u3338_u0;
reg		and_delayed_u380_u0=1'h0;
wire		and_u3339_u0;
wire		or_u1380_u0;
wire		mux_u1843;
wire		and_u3340_u0;
wire		or_u1381_u0;
reg		and_delayed_u381_u0=1'h0;
wire		or_u1382_u0;
wire		and_u3341_u0;
wire		and_u3342_u0;
reg		and_delayed_u382_u0=1'h0;
wire		or_u1383_u0;
wire		mux_u1844_u0;
wire		receive_go_merge;
wire		and_u3343_u0;
wire		or_u1384_u0;
reg		loopControl_u91=1'h0;
reg		syncEnable_u1382=1'h0;
reg		reg_6ea19b29_u0=1'h0;
wire		or_u1385_u0;
wire		mux_u1845_u0;
reg		reg_6ea19b29_result_delayed_u0=1'h0;
assign lessThan_a_signed=port_5b5d1610_;
assign lessThan_b_signed=32'h65;
assign lessThan=lessThan_a_signed<lessThan_b_signed;
assign equals_a_signed=port_5b5d1610_;
assign equals_b_signed=32'h65;
assign equals=equals_a_signed==equals_b_signed;
assign equals_u197_a_signed={31'b0, port_6e1723e8_};
assign equals_u197_b_signed=32'h0;
assign equals_u197=equals_u197_a_signed==equals_u197_b_signed;
assign not_u746_u0=~equals_u197;
assign and_u3321_u0=and_u3343_u0&not_u746_u0;
assign and_u3322_u0=and_u3343_u0&equals_u197;
assign andOp=lessThan&port_26f7a5b8_;
assign and_u3323_u0=and_u3326_u0&andOp;
assign and_u3324_u0=and_u3326_u0&not_u747_u0;
assign not_u747_u0=~andOp;
assign simplePinWrite=and_u3325_u0&{1{and_u3325_u0}};
assign and_u3325_u0=and_u3323_u0&and_u3326_u0;
assign and_u3326_u0=and_u3322_u0&and_u3343_u0;
assign equals_u198_a_signed={31'b0, port_6e1723e8_};
assign equals_u198_b_signed=32'h1;
assign equals_u198=equals_u198_a_signed==equals_u198_b_signed;
assign and_u3327_u0=and_u3343_u0&not_u748_u0;
assign and_u3328_u0=and_u3343_u0&equals_u198;
assign not_u748_u0=~equals_u198;
assign andOp_u62=lessThan&port_26f7a5b8_;
assign not_u749_u0=~andOp_u62;
assign and_u3329_u0=and_u3341_u0&not_u749_u0;
assign and_u3330_u0=and_u3341_u0&andOp_u62;
assign simplePinWrite_u470=and_u3340_u0&{1{and_u3340_u0}};
assign and_u3331_u0=and_u3339_u0&equals;
assign and_u3332_u0=and_u3339_u0&not_u750_u0;
assign not_u750_u0=~equals;
assign and_u3333_u0=and_u3337_u0&port_72a70685_;
assign and_u3334_u0=and_u3337_u0&not_u751_u0;
assign not_u751_u0=~port_72a70685_;
assign simplePinWrite_u471=and_u3336_u0&{1{and_u3336_u0}};
assign and_u3335_u0=and_u3334_u0&and_u3337_u0;
always @(posedge CLK or posedge RESET)
begin
if (RESET)
and_delayed_u379<=1'h0;
else and_delayed_u379<=and_u3335_u0;
end
assign or_u1378_u0=port_777686a0_|and_delayed_u379;
assign and_u3336_u0=and_u3333_u0&and_u3337_u0;
assign or_u1379_u0=or_u1378_u0|and_delayed_u380_u0;
assign and_u3337_u0=and_u3331_u0&and_u3339_u0;
assign and_u3338_u0=and_u3332_u0&and_u3339_u0;
always @(posedge CLK or posedge RESET)
begin
if (RESET)
and_delayed_u380_u0<=1'h0;
else and_delayed_u380_u0<=and_u3338_u0;
end
assign and_u3339_u0=and_u3329_u0&and_u3341_u0;
assign or_u1380_u0=and_u3340_u0|and_u3336_u0;
assign mux_u1843=(and_u3340_u0)?1'h1:1'h0;
assign and_u3340_u0=and_u3330_u0&and_u3341_u0;
assign or_u1381_u0=and_delayed_u381_u0|or_u1379_u0;
always @(posedge CLK or posedge RESET)
begin
if (RESET)
and_delayed_u381_u0<=1'h0;
else and_delayed_u381_u0<=and_u3340_u0;
end
assign or_u1382_u0=and_delayed_u382_u0|or_u1381_u0;
assign and_u3341_u0=and_u3328_u0&and_u3343_u0;
assign and_u3342_u0=and_u3327_u0&and_u3343_u0;
always @(posedge CLK or posedge RESET)
begin
if (RESET)
and_delayed_u382_u0<=1'h0;
else and_delayed_u382_u0<=and_u3342_u0;
end
assign or_u1383_u0=and_u3325_u0|or_u1380_u0;
assign mux_u1844_u0=(and_u3325_u0)?1'h1:mux_u1843;
assign receive_go_merge=simplePinWrite|simplePinWrite_u470;
assign and_u3343_u0=or_u1384_u0&or_u1384_u0;
assign or_u1384_u0=reg_6ea19b29_result_delayed_u0|loopControl_u91;
always @(posedge CLK or posedge syncEnable_u1382)
begin
if (syncEnable_u1382)
loopControl_u91<=1'h0;
else loopControl_u91<=or_u1382_u0;
end
always @(posedge CLK)
begin
if (reg_6ea19b29_result_delayed_u0)
syncEnable_u1382<=RESET;
end
always @(posedge CLK or posedge RESET)
begin
if (RESET)
reg_6ea19b29_u0<=1'h0;
else reg_6ea19b29_u0<=GO;
end
assign or_u1385_u0=GO|or_u1383_u0;
assign mux_u1845_u0=(GO)?1'h0:mux_u1844_u0;
always @(posedge CLK or posedge RESET)
begin
if (RESET)
reg_6ea19b29_result_delayed_u0<=1'h0;
else reg_6ea19b29_result_delayed_u0<=reg_6ea19b29_u0;
end
assign RESULT=or_u1385_u0;
assign RESULT_u2009=mux_u1845_u0;
assign RESULT_u2010=receive_go_merge;
assign RESULT_u2011=simplePinWrite_u471;
assign DONE=1'h0;
endmodule


