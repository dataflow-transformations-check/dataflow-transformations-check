// __  ___ __ ___  _ __   ___  ___ 
// \ \/ / '__/ _ \| '_ \ / _ \/ __|
//  >  <| | | (_) | | | | (_) \__ \
// /_/\_\_|  \___/|_| |_|\___/|___/
// 
// Xronos synthesizer version
// Run date: Fri 23 Feb 2018 14:27:57 +0000
// 

module split(out3_SEND, out2_RDY, in1_COUNT, out3_ACK, out1_ACK, out4_RDY, out2_DATA, out2_COUNT, out3_RDY, CLK, out1_COUNT, out4_COUNT, out1_RDY, out4_SEND, in1_SEND, out3_COUNT, out4_DATA, out2_ACK, out3_DATA, out2_SEND, in1_ACK, RESET, out1_SEND, out4_ACK, out1_DATA, in1_DATA);
output		out3_SEND;
wire		reset_done;
input		out2_RDY;
input	[15:0]	in1_COUNT;
input		out3_ACK;
input		out1_ACK;
input		out4_RDY;
output	[7:0]	out2_DATA;
wire		fanOut2_done;
output	[15:0]	out2_COUNT;
input		out3_RDY;
input		CLK;
output	[15:0]	out1_COUNT;
wire		fanOut1_done;
wire		reset_go;
wire		fanOut4_done;
output	[15:0]	out4_COUNT;
input		out1_RDY;
output		out4_SEND;
wire		fanOut4_go;
input		in1_SEND;
output	[15:0]	out3_COUNT;
output	[7:0]	out4_DATA;
input		out2_ACK;
output	[7:0]	out3_DATA;
wire		fanOut3_go;
output		out2_SEND;
output		in1_ACK;
input		RESET;
wire		fanOut3_done;
wire		fanOut2_go;
wire		fanOut1_go;
output		out1_SEND;
input		out4_ACK;
output	[7:0]	out1_DATA;
input	[7:0]	in1_DATA;
wire		or_0c2108b0_u0;
wire	[7:0]	fanOut2_u27;
wire	[31:0]	fanOut2_u24;
wire		fanOut2;
wire		split_fanOut2_instance_DONE;
wire		fanOut2_u25;
wire	[15:0]	fanOut2_u28;
wire		fanOut2_u26;
wire		bus_65031429_;
wire		bus_6939e6ff_;
wire		fanOut4_u26;
wire		fanOut4;
wire	[7:0]	fanOut4_u27;
wire	[31:0]	fanOut4_u24;
wire		fanOut4_u25;
wire		split_fanOut4_instance_DONE;
wire	[15:0]	fanOut4_u28;
wire		bus_37ec0121_;
wire	[31:0]	reset_u0;
wire		split_reset_instance_DONE;
wire		reset;
wire		bus_10df8d94_;
wire		scheduler_u752;
wire		scheduler_u755;
wire		scheduler_u753;
wire		scheduler_u754;
wire		split_scheduler_instance_DONE;
wire	[1:0]	scheduler_u750;
wire		scheduler_u751;
wire		scheduler;
wire		bus_34124308_;
wire		bus_509edea5_;
wire		fanOut3_u26;
wire		fanOut3;
wire	[7:0]	fanOut3_u28;
wire	[15:0]	fanOut3_u27;
wire		split_fanOut3_instance_DONE;
wire	[31:0]	fanOut3_u24;
wire		fanOut3_u25;
wire		bus_1e7bfd70_;
wire		split_fanOut1_instance_DONE;
wire		fanOut1_u27;
wire		fanOut1;
wire	[31:0]	fanOut1_u24;
wire	[15:0]	fanOut1_u28;
wire		fanOut1_u25;
wire	[7:0]	fanOut1_u26;
wire	[31:0]	bus_22b1f515_;
wire	[1:0]	bus_1df21558_;
assign out3_SEND=fanOut3_u25;
assign reset_done=bus_10df8d94_;
assign out2_DATA=fanOut2_u27;
assign fanOut2_done=bus_37ec0121_;
assign out2_COUNT=fanOut2_u28;
assign out1_COUNT=fanOut1_u28;
assign fanOut1_done=bus_34124308_;
assign reset_go=scheduler_u751;
assign fanOut4_done=bus_65031429_;
assign out4_COUNT=fanOut4_u28;
assign out4_SEND=fanOut4_u25;
assign fanOut4_go=scheduler_u752;
assign out3_COUNT=fanOut3_u27;
assign out4_DATA=fanOut4_u27;
assign out3_DATA=fanOut3_u28;
assign fanOut3_go=scheduler_u754;
assign out2_SEND=fanOut2_u25;
assign in1_ACK=or_0c2108b0_u0;
assign fanOut3_done=bus_6939e6ff_;
assign fanOut2_go=scheduler_u755;
assign fanOut1_go=scheduler_u753;
assign out1_SEND=fanOut1_u27;
assign out1_DATA=fanOut1_u26;
assign or_0c2108b0_u0=fanOut1_u25|fanOut2_u26|fanOut3_u26|fanOut4_u26;
split_fanOut2 split_fanOut2_instance(.CLK(CLK), .RESET(bus_509edea5_), .GO(fanOut2_go), 
  .port_2fc40f81_(bus_22b1f515_), .port_0bdeff74_(in1_DATA), .DONE(split_fanOut2_instance_DONE), 
  .RESULT(fanOut2), .RESULT_u1876(fanOut2_u24), .RESULT_u1877(fanOut2_u25), .RESULT_u1878(fanOut2_u26), 
  .RESULT_u1879(fanOut2_u27), .RESULT_u1880(fanOut2_u28));
assign bus_65031429_=split_fanOut4_instance_DONE&{1{split_fanOut4_instance_DONE}};
assign bus_6939e6ff_=split_fanOut3_instance_DONE&{1{split_fanOut3_instance_DONE}};
split_fanOut4 split_fanOut4_instance(.CLK(CLK), .RESET(bus_509edea5_), .GO(fanOut4_go), 
  .port_4118448a_(bus_22b1f515_), .port_7a2094cf_(in1_DATA), .DONE(split_fanOut4_instance_DONE), 
  .RESULT(fanOut4), .RESULT_u1881(fanOut4_u24), .RESULT_u1882(fanOut4_u25), .RESULT_u1883(fanOut4_u26), 
  .RESULT_u1884(fanOut4_u27), .RESULT_u1885(fanOut4_u28));
assign bus_37ec0121_=split_fanOut2_instance_DONE&{1{split_fanOut2_instance_DONE}};
split_reset split_reset_instance(.CLK(CLK), .RESET(bus_509edea5_), .GO(reset_go), 
  .DONE(split_reset_instance_DONE), .RESULT(reset), .RESULT_u1886(reset_u0));
assign bus_10df8d94_=split_reset_instance_DONE&{1{split_reset_instance_DONE}};
split_scheduler split_scheduler_instance(.CLK(CLK), .RESET(bus_509edea5_), .GO(bus_1e7bfd70_), 
  .port_551a9a68_(bus_1df21558_), .port_7a023679_(bus_22b1f515_), .port_22d0f7af_(reset_done), 
  .port_780c9938_(in1_SEND), .port_4732fe56_(fanOut4_done), .port_4ac0fda3_(fanOut2_done), 
  .port_28994613_(fanOut3_done), .port_78a53f9c_(out3_RDY), .port_2ce9e88c_(out1_RDY), 
  .port_67354296_(out4_RDY), .port_5e3744f0_(out2_RDY), .port_4bae73da_(fanOut1_done), 
  .DONE(split_scheduler_instance_DONE), .RESULT(scheduler), .RESULT_u1887(scheduler_u750), 
  .RESULT_u1888(scheduler_u751), .RESULT_u1889(scheduler_u752), .RESULT_u1890(scheduler_u753), 
  .RESULT_u1891(scheduler_u754), .RESULT_u1892(scheduler_u755));
assign bus_34124308_=split_fanOut1_instance_DONE&{1{split_fanOut1_instance_DONE}};
split_globalreset_physical_7c389e52_ split_globalreset_physical_7c389e52__1(.bus_2c68f0a1_(CLK), 
  .bus_5badd0de_(RESET), .bus_509edea5_(bus_509edea5_));
split_fanOut3 split_fanOut3_instance(.CLK(CLK), .RESET(bus_509edea5_), .GO(fanOut3_go), 
  .port_72e2a144_(bus_22b1f515_), .port_7f6f20a8_(in1_DATA), .DONE(split_fanOut3_instance_DONE), 
  .RESULT(fanOut3), .RESULT_u1893(fanOut3_u24), .RESULT_u1894(fanOut3_u25), .RESULT_u1895(fanOut3_u26), 
  .RESULT_u1896(fanOut3_u27), .RESULT_u1897(fanOut3_u28));
split_Kicker_48 split_Kicker_48_1(.CLK(CLK), .RESET(bus_509edea5_), .bus_1e7bfd70_(bus_1e7bfd70_));
split_fanOut1 split_fanOut1_instance(.CLK(CLK), .RESET(bus_509edea5_), .GO(fanOut1_go), 
  .port_68d5869c_(bus_22b1f515_), .port_4512f14f_(in1_DATA), .DONE(split_fanOut1_instance_DONE), 
  .RESULT(fanOut1), .RESULT_u1898(fanOut1_u24), .RESULT_u1899(fanOut1_u25), .RESULT_u1900(fanOut1_u26), 
  .RESULT_u1901(fanOut1_u27), .RESULT_u1902(fanOut1_u28));
split_stateVar_i split_stateVar_i_1(.bus_63d2984a_(CLK), .bus_38a7a4ab_(bus_509edea5_), 
  .bus_1a97fd19_(reset), .bus_68f75520_(32'h0), .bus_6200a497_(fanOut1), .bus_5ca69ad0_(fanOut1_u24), 
  .bus_2b9d3e62_(fanOut2), .bus_1976e616_(fanOut2_u24), .bus_67c38bb4_(fanOut3), 
  .bus_4c57e8a0_(fanOut3_u24), .bus_6cfca4f4_(fanOut4), .bus_029a9be7_(fanOut4_u24), 
  .bus_22b1f515_(bus_22b1f515_));
split_stateVar_fsmState_split split_stateVar_fsmState_split_1(.bus_00e4a6da_(CLK), 
  .bus_47e3b766_(bus_509edea5_), .bus_2a92e769_(scheduler), .bus_55e7888a_(scheduler_u750), 
  .bus_1df21558_(bus_1df21558_));
endmodule



module split_fanOut2(CLK, RESET, GO, port_2fc40f81_, port_0bdeff74_, RESULT, RESULT_u1876, RESULT_u1877, RESULT_u1878, RESULT_u1879, RESULT_u1880, DONE);
input		CLK;
input		RESET;
input		GO;
input	[31:0]	port_2fc40f81_;
input	[7:0]	port_0bdeff74_;
output		RESULT;
output	[31:0]	RESULT_u1876;
output		RESULT_u1877;
output		RESULT_u1878;
output	[7:0]	RESULT_u1879;
output	[15:0]	RESULT_u1880;
output		DONE;
wire		simplePinWrite;
wire	[31:0]	add;
wire	[7:0]	simplePinWrite_u422;
wire		simplePinWrite_u423;
wire	[15:0]	simplePinWrite_u424;
reg		reg_1da43154_u0=1'h0;
assign simplePinWrite=GO&{1{GO}};
assign add=port_2fc40f81_+32'h1;
assign simplePinWrite_u422=port_0bdeff74_;
assign simplePinWrite_u423=GO&{1{GO}};
assign simplePinWrite_u424=16'h1&{16{1'h1}};
always @(posedge CLK or posedge RESET)
begin
if (RESET)
reg_1da43154_u0<=1'h0;
else reg_1da43154_u0<=GO;
end
assign RESULT=GO;
assign RESULT_u1876=add;
assign RESULT_u1877=simplePinWrite_u423;
assign RESULT_u1878=simplePinWrite;
assign RESULT_u1879=simplePinWrite_u422;
assign RESULT_u1880=simplePinWrite_u424;
assign DONE=reg_1da43154_u0;
endmodule



module split_fanOut4(CLK, RESET, GO, port_4118448a_, port_7a2094cf_, RESULT, RESULT_u1881, RESULT_u1882, RESULT_u1883, RESULT_u1884, RESULT_u1885, DONE);
input		CLK;
input		RESET;
input		GO;
input	[31:0]	port_4118448a_;
input	[7:0]	port_7a2094cf_;
output		RESULT;
output	[31:0]	RESULT_u1881;
output		RESULT_u1882;
output		RESULT_u1883;
output	[7:0]	RESULT_u1884;
output	[15:0]	RESULT_u1885;
output		DONE;
wire		simplePinWrite;
wire	[31:0]	add;
wire		simplePinWrite_u425;
wire	[15:0]	simplePinWrite_u426;
wire	[7:0]	simplePinWrite_u427;
reg		reg_207ec91a_u0=1'h0;
assign simplePinWrite=GO&{1{GO}};
assign add=port_4118448a_+32'h1;
assign simplePinWrite_u425=GO&{1{GO}};
assign simplePinWrite_u426=16'h1&{16{1'h1}};
assign simplePinWrite_u427=port_7a2094cf_;
always @(posedge CLK or posedge RESET)
begin
if (RESET)
reg_207ec91a_u0<=1'h0;
else reg_207ec91a_u0<=GO;
end
assign RESULT=GO;
assign RESULT_u1881=add;
assign RESULT_u1882=simplePinWrite_u425;
assign RESULT_u1883=simplePinWrite;
assign RESULT_u1884=simplePinWrite_u427;
assign RESULT_u1885=simplePinWrite_u426;
assign DONE=reg_207ec91a_u0;
endmodule



module split_reset(CLK, RESET, GO, RESULT, RESULT_u1886, DONE);
input		CLK;
input		RESET;
input		GO;
output		RESULT;
output	[31:0]	RESULT_u1886;
output		DONE;
reg		reg_6c7698cd_u0=1'h0;
always @(posedge CLK or posedge RESET)
begin
if (RESET)
reg_6c7698cd_u0<=1'h0;
else reg_6c7698cd_u0<=GO;
end
assign RESULT=GO;
assign RESULT_u1886=32'h0;
assign DONE=reg_6c7698cd_u0;
endmodule



module split_scheduler(CLK, RESET, GO, port_551a9a68_, port_7a023679_, port_22d0f7af_, port_780c9938_, port_4732fe56_, port_4ac0fda3_, port_28994613_, port_78a53f9c_, port_2ce9e88c_, port_67354296_, port_5e3744f0_, port_4bae73da_, RESULT, RESULT_u1887, RESULT_u1888, RESULT_u1889, RESULT_u1890, RESULT_u1891, RESULT_u1892, DONE);
input		CLK;
input		RESET;
input		GO;
input	[1:0]	port_551a9a68_;
input	[31:0]	port_7a023679_;
input		port_22d0f7af_;
input		port_780c9938_;
input		port_4732fe56_;
input		port_4ac0fda3_;
input		port_28994613_;
input		port_78a53f9c_;
input		port_2ce9e88c_;
input		port_67354296_;
input		port_5e3744f0_;
input		port_4bae73da_;
output		RESULT;
output	[1:0]	RESULT_u1887;
output		RESULT_u1888;
output		RESULT_u1889;
output		RESULT_u1890;
output		RESULT_u1891;
output		RESULT_u1892;
output		DONE;
wire signed	[31:0]	equals_b_signed;
wire		equals;
wire signed	[31:0]	equals_a_signed;
wire signed	[31:0]	lessThan_a_signed;
wire signed	[31:0]	lessThan_b_signed;
wire		lessThan;
wire signed	[31:0]	lessThan_u80_a_signed;
wire signed	[31:0]	lessThan_u80_b_signed;
wire		lessThan_u80;
wire signed	[31:0]	lessThan_u81_b_signed;
wire		lessThan_u81;
wire signed	[31:0]	lessThan_u81_a_signed;
wire		lessThan_u82;
wire signed	[31:0]	lessThan_u82_b_signed;
wire signed	[31:0]	lessThan_u82_a_signed;
wire signed	[31:0]	equals_u184_b_signed;
wire signed	[31:0]	equals_u184_a_signed;
wire		equals_u184;
wire		not_u688_u0;
wire		and_u3086_u0;
wire		and_u3087_u0;
wire		not_u689_u0;
wire		and_u3088_u0;
wire		and_u3089_u0;
wire		simplePinWrite;
wire		andOp;
wire		not_u690_u0;
wire		and_u3090_u0;
wire		and_u3091_u0;
wire		not_u691_u0;
wire		and_u3092_u0;
wire		and_u3093_u0;
wire		simplePinWrite_u428;
wire		and_u3094_u0;
wire		and_u3095_u0;
wire		and_u3096_u0;
wire		or_u1304_u0;
wire	[1:0]	mux_u1746;
wire		and_u3097_u0;
wire		and_u3098_u0;
wire signed	[31:0]	equals_u185_b_signed;
wire		equals_u185;
wire signed	[31:0]	equals_u185_a_signed;
wire		and_u3099_u0;
wire		not_u692_u0;
wire		and_u3100_u0;
wire		and_u3101_u0;
wire		not_u693_u0;
wire		and_u3102_u0;
wire		simplePinWrite_u429;
wire		andOp_u56;
wire		not_u694_u0;
wire		and_u3103_u0;
wire		and_u3104_u0;
wire		not_u695_u0;
wire		and_u3105_u0;
wire		and_u3106_u0;
wire		simplePinWrite_u430;
wire		and_u3107_u0;
wire		and_u3108_u0;
wire		and_u3109_u0;
wire		and_u3110_u0;
wire		or_u1305_u0;
wire	[1:0]	mux_u1747_u0;
wire		and_u3111_u0;
wire signed	[31:0]	equals_u186_b_signed;
wire		equals_u186;
wire signed	[31:0]	equals_u186_a_signed;
wire		not_u696_u0;
wire		and_u3112_u0;
wire		and_u3113_u0;
wire		not_u697_u0;
wire		and_u3114_u0;
wire		and_u3115_u0;
wire		simplePinWrite_u431;
wire		andOp_u57;
wire		and_u3116_u0;
wire		not_u698_u0;
wire		and_u3117_u0;
wire		not_u699_u0;
wire		and_u3118_u0;
wire		and_u3119_u0;
wire		simplePinWrite_u432;
wire		and_u3120_u0;
wire		and_u3121_u0;
wire		and_u3122_u0;
wire	[1:0]	mux_u1748_u0;
wire		or_u1306_u0;
wire		and_u3123_u0;
wire		and_u3124_u0;
wire		equals_u187;
wire signed	[31:0]	equals_u187_a_signed;
wire signed	[31:0]	equals_u187_b_signed;
wire		not_u700_u0;
wire		and_u3125_u0;
wire		and_u3126_u0;
wire		and_u3127_u0;
wire		not_u701_u0;
wire		and_u3128_u0;
wire		simplePinWrite_u433;
wire		andOp_u58;
wire		and_u3129_u0;
wire		not_u702_u0;
wire		and_u3130_u0;
wire		and_u3131_u0;
wire		not_u703_u0;
wire		and_u3132_u0;
wire		simplePinWrite_u434;
wire		and_u3133_u0;
wire		and_u3134_u0;
wire		or_u1307_u0;
wire	[1:0]	mux_u1749_u0;
wire		and_u3135_u0;
wire		and_u3136_u0;
wire		and_u3137_u0;
wire		reset_go_merge;
wire	[1:0]	mux_u1750_u0;
wire		or_u1308_u0;
reg		reg_3c35093a_u0=1'h0;
wire		and_u3138_u0;
wire		or_u1309_u0;
reg		reg_282474ae_u0=1'h0;
reg		reg_282474ae_result_delayed_u0=1'h0;
wire	[1:0]	mux_u1751_u0;
wire		or_u1310_u0;
assign equals_a_signed=port_7a023679_;
assign equals_b_signed=32'h65;
assign equals=equals_a_signed==equals_b_signed;
assign lessThan_a_signed=port_7a023679_;
assign lessThan_b_signed=32'h65;
assign lessThan=lessThan_a_signed<lessThan_b_signed;
assign lessThan_u80_a_signed=port_7a023679_;
assign lessThan_u80_b_signed=32'h65;
assign lessThan_u80=lessThan_u80_a_signed<lessThan_u80_b_signed;
assign lessThan_u81_a_signed=port_7a023679_;
assign lessThan_u81_b_signed=32'h65;
assign lessThan_u81=lessThan_u81_a_signed<lessThan_u81_b_signed;
assign lessThan_u82_a_signed=port_7a023679_;
assign lessThan_u82_b_signed=32'h65;
assign lessThan_u82=lessThan_u82_a_signed<lessThan_u82_b_signed;
assign equals_u184_a_signed={30'b0, port_551a9a68_};
assign equals_u184_b_signed=32'h0;
assign equals_u184=equals_u184_a_signed==equals_u184_b_signed;
assign not_u688_u0=~equals_u184;
assign and_u3086_u0=and_u3138_u0&not_u688_u0;
assign and_u3087_u0=and_u3138_u0&equals_u184;
assign not_u689_u0=~equals;
assign and_u3088_u0=and_u3098_u0&equals;
assign and_u3089_u0=and_u3098_u0&not_u689_u0;
assign simplePinWrite=and_u3097_u0&{1{and_u3097_u0}};
assign andOp=lessThan&port_780c9938_;
assign not_u690_u0=~andOp;
assign and_u3090_u0=and_u3096_u0&andOp;
assign and_u3091_u0=and_u3096_u0&not_u690_u0;
assign not_u691_u0=~port_2ce9e88c_;
assign and_u3092_u0=and_u3095_u0&not_u691_u0;
assign and_u3093_u0=and_u3095_u0&port_2ce9e88c_;
assign simplePinWrite_u428=and_u3094_u0&{1{and_u3094_u0}};
assign and_u3094_u0=and_u3093_u0&and_u3095_u0;
assign and_u3095_u0=and_u3090_u0&and_u3096_u0;
assign and_u3096_u0=and_u3089_u0&and_u3098_u0;
assign or_u1304_u0=and_u3097_u0|and_u3094_u0;
assign mux_u1746=(and_u3097_u0)?2'h1:2'h0;
assign and_u3097_u0=and_u3088_u0&and_u3098_u0;
assign and_u3098_u0=and_u3087_u0&and_u3138_u0;
assign equals_u185_a_signed={30'b0, port_551a9a68_};
assign equals_u185_b_signed=32'h1;
assign equals_u185=equals_u185_a_signed==equals_u185_b_signed;
assign and_u3099_u0=and_u3138_u0&not_u692_u0;
assign not_u692_u0=~equals_u185;
assign and_u3100_u0=and_u3138_u0&equals_u185;
assign and_u3101_u0=and_u3111_u0&not_u693_u0;
assign not_u693_u0=~equals;
assign and_u3102_u0=and_u3111_u0&equals;
assign simplePinWrite_u429=and_u3109_u0&{1{and_u3109_u0}};
assign andOp_u56=lessThan_u80&port_780c9938_;
assign not_u694_u0=~andOp_u56;
assign and_u3103_u0=and_u3110_u0&andOp_u56;
assign and_u3104_u0=and_u3110_u0&not_u694_u0;
assign not_u695_u0=~port_5e3744f0_;
assign and_u3105_u0=and_u3108_u0&not_u695_u0;
assign and_u3106_u0=and_u3108_u0&port_5e3744f0_;
assign simplePinWrite_u430=and_u3107_u0&{1{and_u3107_u0}};
assign and_u3107_u0=and_u3106_u0&and_u3108_u0;
assign and_u3108_u0=and_u3103_u0&and_u3110_u0;
assign and_u3109_u0=and_u3102_u0&and_u3111_u0;
assign and_u3110_u0=and_u3101_u0&and_u3111_u0;
assign or_u1305_u0=and_u3109_u0|and_u3107_u0;
assign mux_u1747_u0=(and_u3109_u0)?2'h2:2'h1;
assign and_u3111_u0=and_u3100_u0&and_u3138_u0;
assign equals_u186_a_signed={30'b0, port_551a9a68_};
assign equals_u186_b_signed=32'h2;
assign equals_u186=equals_u186_a_signed==equals_u186_b_signed;
assign not_u696_u0=~equals_u186;
assign and_u3112_u0=and_u3138_u0&not_u696_u0;
assign and_u3113_u0=and_u3138_u0&equals_u186;
assign not_u697_u0=~equals;
assign and_u3114_u0=and_u3124_u0&equals;
assign and_u3115_u0=and_u3124_u0&not_u697_u0;
assign simplePinWrite_u431=and_u3122_u0&{1{and_u3122_u0}};
assign andOp_u57=lessThan_u81&port_780c9938_;
assign and_u3116_u0=and_u3123_u0&andOp_u57;
assign not_u698_u0=~andOp_u57;
assign and_u3117_u0=and_u3123_u0&not_u698_u0;
assign not_u699_u0=~port_78a53f9c_;
assign and_u3118_u0=and_u3121_u0&port_78a53f9c_;
assign and_u3119_u0=and_u3121_u0&not_u699_u0;
assign simplePinWrite_u432=and_u3120_u0&{1{and_u3120_u0}};
assign and_u3120_u0=and_u3118_u0&and_u3121_u0;
assign and_u3121_u0=and_u3116_u0&and_u3123_u0;
assign and_u3122_u0=and_u3114_u0&and_u3124_u0;
assign mux_u1748_u0=(and_u3122_u0)?2'h3:2'h2;
assign or_u1306_u0=and_u3122_u0|and_u3120_u0;
assign and_u3123_u0=and_u3115_u0&and_u3124_u0;
assign and_u3124_u0=and_u3113_u0&and_u3138_u0;
assign equals_u187_a_signed={30'b0, port_551a9a68_};
assign equals_u187_b_signed=32'h3;
assign equals_u187=equals_u187_a_signed==equals_u187_b_signed;
assign not_u700_u0=~equals_u187;
assign and_u3125_u0=and_u3138_u0&not_u700_u0;
assign and_u3126_u0=and_u3138_u0&equals_u187;
assign and_u3127_u0=and_u3137_u0&equals;
assign not_u701_u0=~equals;
assign and_u3128_u0=and_u3137_u0&not_u701_u0;
assign simplePinWrite_u433=and_u3135_u0&{1{and_u3135_u0}};
assign andOp_u58=lessThan_u82&port_780c9938_;
assign and_u3129_u0=and_u3136_u0&andOp_u58;
assign not_u702_u0=~andOp_u58;
assign and_u3130_u0=and_u3136_u0&not_u702_u0;
assign and_u3131_u0=and_u3134_u0&port_67354296_;
assign not_u703_u0=~port_67354296_;
assign and_u3132_u0=and_u3134_u0&not_u703_u0;
assign simplePinWrite_u434=and_u3133_u0&{1{and_u3133_u0}};
assign and_u3133_u0=and_u3131_u0&and_u3134_u0;
assign and_u3134_u0=and_u3129_u0&and_u3136_u0;
assign or_u1307_u0=and_u3135_u0|and_u3133_u0;
assign mux_u1749_u0=(and_u3135_u0)?2'h0:2'h3;
assign and_u3135_u0=and_u3127_u0&and_u3137_u0;
assign and_u3136_u0=and_u3128_u0&and_u3137_u0;
assign and_u3137_u0=and_u3126_u0&and_u3138_u0;
assign reset_go_merge=simplePinWrite|simplePinWrite_u429|simplePinWrite_u431|simplePinWrite_u433;
assign mux_u1750_u0=({2{or_u1304_u0}}&{1'b0, mux_u1746[0]})|({2{or_u1305_u0}}&mux_u1747_u0)|({2{or_u1306_u0}}&{1'b1, mux_u1748_u0[0]})|({2{or_u1307_u0}}&{mux_u1749_u0[0], mux_u1749_u0[0]});
assign or_u1308_u0=or_u1304_u0|or_u1305_u0|or_u1306_u0|or_u1307_u0;
always @(posedge CLK or posedge RESET)
begin
if (RESET)
reg_3c35093a_u0<=1'h0;
else reg_3c35093a_u0<=and_u3138_u0;
end
assign and_u3138_u0=or_u1309_u0&or_u1309_u0;
assign or_u1309_u0=reg_282474ae_result_delayed_u0|reg_3c35093a_u0;
always @(posedge CLK or posedge RESET)
begin
if (RESET)
reg_282474ae_u0<=1'h0;
else reg_282474ae_u0<=GO;
end
always @(posedge CLK or posedge RESET)
begin
if (RESET)
reg_282474ae_result_delayed_u0<=1'h0;
else reg_282474ae_result_delayed_u0<=reg_282474ae_u0;
end
assign mux_u1751_u0=(GO)?2'h0:mux_u1750_u0;
assign or_u1310_u0=GO|or_u1308_u0;
assign RESULT=or_u1310_u0;
assign RESULT_u1887=mux_u1751_u0;
assign RESULT_u1888=reset_go_merge;
assign RESULT_u1889=simplePinWrite_u434;
assign RESULT_u1890=simplePinWrite_u428;
assign RESULT_u1891=simplePinWrite_u432;
assign RESULT_u1892=simplePinWrite_u430;
assign DONE=1'h0;
endmodule



module split_globalreset_physical_7c389e52_(bus_2c68f0a1_, bus_5badd0de_, bus_509edea5_);
input		bus_2c68f0a1_;
input		bus_5badd0de_;
output		bus_509edea5_;
reg		final_u48=1'h1;
reg		glitch_u48=1'h0;
wire		and_28e6fa84_u0;
reg		cross_u48=1'h0;
reg		sample_u48=1'h0;
wire		or_4da4ca73_u0;
wire		not_1e7c9c63_u0;
always @(posedge bus_2c68f0a1_)
begin
final_u48<=not_1e7c9c63_u0;
end
always @(posedge bus_2c68f0a1_)
begin
glitch_u48<=cross_u48;
end
assign and_28e6fa84_u0=cross_u48&glitch_u48;
assign bus_509edea5_=or_4da4ca73_u0;
always @(posedge bus_2c68f0a1_)
begin
cross_u48<=sample_u48;
end
always @(posedge bus_2c68f0a1_)
begin
sample_u48<=1'h1;
end
assign or_4da4ca73_u0=bus_5badd0de_|final_u48;
assign not_1e7c9c63_u0=~and_28e6fa84_u0;
endmodule



module split_fanOut3(CLK, RESET, GO, port_72e2a144_, port_7f6f20a8_, RESULT, RESULT_u1893, RESULT_u1894, RESULT_u1895, RESULT_u1896, RESULT_u1897, DONE);
input		CLK;
input		RESET;
input		GO;
input	[31:0]	port_72e2a144_;
input	[7:0]	port_7f6f20a8_;
output		RESULT;
output	[31:0]	RESULT_u1893;
output		RESULT_u1894;
output		RESULT_u1895;
output	[15:0]	RESULT_u1896;
output	[7:0]	RESULT_u1897;
output		DONE;
wire		simplePinWrite;
wire	[31:0]	add;
wire		simplePinWrite_u435;
wire	[7:0]	simplePinWrite_u436;
wire	[15:0]	simplePinWrite_u437;
reg		reg_46557e32_u0=1'h0;
assign simplePinWrite=GO&{1{GO}};
assign add=port_72e2a144_+32'h1;
assign simplePinWrite_u435=GO&{1{GO}};
assign simplePinWrite_u436=port_7f6f20a8_;
assign simplePinWrite_u437=16'h1&{16{1'h1}};
always @(posedge CLK or posedge RESET)
begin
if (RESET)
reg_46557e32_u0<=1'h0;
else reg_46557e32_u0<=GO;
end
assign RESULT=GO;
assign RESULT_u1893=add;
assign RESULT_u1894=simplePinWrite_u435;
assign RESULT_u1895=simplePinWrite;
assign RESULT_u1896=simplePinWrite_u437;
assign RESULT_u1897=simplePinWrite_u436;
assign DONE=reg_46557e32_u0;
endmodule



module split_Kicker_48(CLK, RESET, bus_1e7bfd70_);
input		CLK;
input		RESET;
output		bus_1e7bfd70_;
reg		kicker_2=1'h0;
reg		kicker_1=1'h0;
wire		bus_2da5b549_;
wire		bus_14615974_;
reg		kicker_res=1'h0;
wire		bus_109aa22e_;
wire		bus_5538e8a6_;
always @(posedge CLK)
begin
kicker_2<=bus_2da5b549_;
end
assign bus_1e7bfd70_=kicker_res;
always @(posedge CLK)
begin
kicker_1<=bus_14615974_;
end
assign bus_2da5b549_=bus_14615974_&kicker_1;
assign bus_14615974_=~RESET;
always @(posedge CLK)
begin
kicker_res<=bus_109aa22e_;
end
assign bus_109aa22e_=kicker_1&bus_14615974_&bus_5538e8a6_;
assign bus_5538e8a6_=~kicker_2;
endmodule



module split_fanOut1(CLK, RESET, GO, port_68d5869c_, port_4512f14f_, RESULT, RESULT_u1898, RESULT_u1899, RESULT_u1900, RESULT_u1901, RESULT_u1902, DONE);
input		CLK;
input		RESET;
input		GO;
input	[31:0]	port_68d5869c_;
input	[7:0]	port_4512f14f_;
output		RESULT;
output	[31:0]	RESULT_u1898;
output		RESULT_u1899;
output	[7:0]	RESULT_u1900;
output		RESULT_u1901;
output	[15:0]	RESULT_u1902;
output		DONE;
wire		simplePinWrite;
wire	[31:0]	add;
wire	[7:0]	simplePinWrite_u438;
wire	[15:0]	simplePinWrite_u439;
wire		simplePinWrite_u440;
reg		reg_4191293f_u0=1'h0;
assign simplePinWrite=GO&{1{GO}};
assign add=port_68d5869c_+32'h1;
assign simplePinWrite_u438=port_4512f14f_;
assign simplePinWrite_u439=16'h1&{16{1'h1}};
assign simplePinWrite_u440=GO&{1{GO}};
always @(posedge CLK or posedge RESET)
begin
if (RESET)
reg_4191293f_u0<=1'h0;
else reg_4191293f_u0<=GO;
end
assign RESULT=GO;
assign RESULT_u1898=add;
assign RESULT_u1899=simplePinWrite;
assign RESULT_u1900=simplePinWrite_u438;
assign RESULT_u1901=simplePinWrite_u440;
assign RESULT_u1902=simplePinWrite_u439;
assign DONE=reg_4191293f_u0;
endmodule



module split_endianswapper_430b2c8a_(endianswapper_430b2c8a_in, endianswapper_430b2c8a_out);
input	[31:0]	endianswapper_430b2c8a_in;
output	[31:0]	endianswapper_430b2c8a_out;
assign endianswapper_430b2c8a_out=endianswapper_430b2c8a_in;
endmodule



module split_endianswapper_206e9be8_(endianswapper_206e9be8_in, endianswapper_206e9be8_out);
input	[31:0]	endianswapper_206e9be8_in;
output	[31:0]	endianswapper_206e9be8_out;
assign endianswapper_206e9be8_out=endianswapper_206e9be8_in;
endmodule



module split_stateVar_i(bus_63d2984a_, bus_38a7a4ab_, bus_1a97fd19_, bus_68f75520_, bus_6200a497_, bus_5ca69ad0_, bus_2b9d3e62_, bus_1976e616_, bus_67c38bb4_, bus_4c57e8a0_, bus_6cfca4f4_, bus_029a9be7_, bus_22b1f515_);
input		bus_63d2984a_;
input		bus_38a7a4ab_;
input		bus_1a97fd19_;
input	[31:0]	bus_68f75520_;
input		bus_6200a497_;
input	[31:0]	bus_5ca69ad0_;
input		bus_2b9d3e62_;
input	[31:0]	bus_1976e616_;
input		bus_67c38bb4_;
input	[31:0]	bus_4c57e8a0_;
input		bus_6cfca4f4_;
input	[31:0]	bus_029a9be7_;
output	[31:0]	bus_22b1f515_;
reg	[31:0]	stateVar_i_u40=32'h0;
wire		or_37fc21bd_u0;
wire	[31:0]	endianswapper_430b2c8a_out;
wire	[31:0]	endianswapper_206e9be8_out;
wire	[31:0]	mux_6b47516d_u0;
always @(posedge bus_63d2984a_ or posedge bus_38a7a4ab_)
begin
if (bus_38a7a4ab_)
stateVar_i_u40<=32'h0;
else if (or_37fc21bd_u0)
stateVar_i_u40<=endianswapper_430b2c8a_out;
end
assign or_37fc21bd_u0=bus_1a97fd19_|bus_6200a497_|bus_2b9d3e62_|bus_67c38bb4_|bus_6cfca4f4_;
split_endianswapper_430b2c8a_ split_endianswapper_430b2c8a__1(.endianswapper_430b2c8a_in(mux_6b47516d_u0), 
  .endianswapper_430b2c8a_out(endianswapper_430b2c8a_out));
split_endianswapper_206e9be8_ split_endianswapper_206e9be8__1(.endianswapper_206e9be8_in(stateVar_i_u40), 
  .endianswapper_206e9be8_out(endianswapper_206e9be8_out));
assign mux_6b47516d_u0=({32{bus_1a97fd19_}}&32'h0)|({32{bus_6200a497_}}&bus_5ca69ad0_)|({32{bus_2b9d3e62_}}&bus_1976e616_)|({32{bus_67c38bb4_}}&bus_4c57e8a0_)|({32{bus_6cfca4f4_}}&bus_029a9be7_);
assign bus_22b1f515_=endianswapper_206e9be8_out;
endmodule



module split_endianswapper_524ede3e_(endianswapper_524ede3e_in, endianswapper_524ede3e_out);
input	[1:0]	endianswapper_524ede3e_in;
output	[1:0]	endianswapper_524ede3e_out;
assign endianswapper_524ede3e_out=endianswapper_524ede3e_in;
endmodule



module split_endianswapper_489a3809_(endianswapper_489a3809_in, endianswapper_489a3809_out);
input	[1:0]	endianswapper_489a3809_in;
output	[1:0]	endianswapper_489a3809_out;
assign endianswapper_489a3809_out=endianswapper_489a3809_in;
endmodule



module split_stateVar_fsmState_split(bus_00e4a6da_, bus_47e3b766_, bus_2a92e769_, bus_55e7888a_, bus_1df21558_);
input		bus_00e4a6da_;
input		bus_47e3b766_;
input		bus_2a92e769_;
input	[1:0]	bus_55e7888a_;
output	[1:0]	bus_1df21558_;
wire	[1:0]	endianswapper_524ede3e_out;
wire	[1:0]	endianswapper_489a3809_out;
reg	[1:0]	stateVar_fsmState_split_u4=2'h0;
split_endianswapper_524ede3e_ split_endianswapper_524ede3e__1(.endianswapper_524ede3e_in(stateVar_fsmState_split_u4), 
  .endianswapper_524ede3e_out(endianswapper_524ede3e_out));
split_endianswapper_489a3809_ split_endianswapper_489a3809__1(.endianswapper_489a3809_in(bus_55e7888a_), 
  .endianswapper_489a3809_out(endianswapper_489a3809_out));
assign bus_1df21558_=endianswapper_524ede3e_out;
always @(posedge bus_00e4a6da_ or posedge bus_47e3b766_)
begin
if (bus_47e3b766_)
stateVar_fsmState_split_u4<=2'h0;
else if (bus_2a92e769_)
stateVar_fsmState_split_u4<=endianswapper_489a3809_out;
end
endmodule


