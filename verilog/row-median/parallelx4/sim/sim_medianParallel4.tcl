## ############################################################################
## __  ___ __ ___  _ __   ___  ___ 
## \ \/ / '__/ _ \| '_ \ / _ \/ __|
##  >  <| | | (_) | | | | (_) \__ \
## /_/\_\_|  \___/|_| |_|\___/|___/
## ############################################################################
## Xronos synthesizer
## Simulation Launch TCL Script file for Network: medianParallel4 
## Date: 2018/02/23 14:28:04
## ############################################################################

## Set paths
set Lib "../lib/"
set Rtl "../rtl"
set RtlGoDone "../rtl/rtlGoDone"

## Create SystemBuilder design library
vlib SystemBuilder
vmap SystemBuilder SystemBuilder

## Compile the SystemBuilder Library from sources
vcom -reportprogress 300 -work SystemBuilder $Lib/systemBuilder/vhdl/sbtypes.vhdl
vcom -reportprogress 300 -work SystemBuilder $Lib/systemBuilder/vhdl/sbfifo.vhdl
vcom -reportprogress 300 -work SystemBuilder $Lib/systemBuilder/vhdl/sbfifo_behavioral.vhdl

## Create the work design library
if {[file exist work]} {vdel -all -lib work}
vlib work
vmap work work

## Compile the glbl constans given by Xilinx 
vlog -work work ../lib/simulation/glbl.v


## Compile network instances and add them to work library	
vlog -work work $Rtl/split.v
vlog -work work $Rtl/join.v
vlog -work work $Rtl/medianRow2.v
vlog -work work $Rtl/medianRow1.v
vlog -work work $Rtl/medianRow3.v
vlog -work work $Rtl/medianRow4.v

## Compile the Top Network
vcom -93 -check_synthesis -quiet -work work $Rtl/medianParallel4.vhd


## Start VSIM
vsim -voptargs="+acc" -L unisims_ver -L simprims_ver -t ns work.glbl work.medianParallel4
	
## Add clock(s) and reset signal
add wave -noupdate -divider -height 20 "CLK & RESET"

add wave sim:/medianParallel4/CLK
	add wave sim:/medianParallel4/RESET
	
	
	## Change radix to decimal
	radix -decimal


add wave -noupdate -divider -height 20 no_outPort
add wave sim:/medianParallel4/outPort_DATA
add wave sim:/medianParallel4/outPort_SEND
add wave sim:/medianParallel4/outPort_ACK
add wave sim:/medianParallel4/outPort_RDY
## Freeze ACK and RDY at 1
force -freeze sim:/medianParallel4/outPort_ACK 1 0
force -freeze sim:/medianParallel4/outPort_RDY 1 0



add wave -noupdate -divider -height 20 i_split
add wave sim:/medianParallel4/i_split/CLK
add wave sim:/medianParallel4/i_split/in1_DATA
add wave sim:/medianParallel4/i_split/in1_ACK
add wave sim:/medianParallel4/i_split/in1_SEND
add wave sim:/medianParallel4/i_split/out1_DATA
add wave sim:/medianParallel4/i_split/out1_ACK
add wave sim:/medianParallel4/i_split/out1_SEND
add wave sim:/medianParallel4/i_split/out1_RDY

add wave sim:/medianParallel4/i_split/out2_DATA
add wave sim:/medianParallel4/i_split/out2_ACK
add wave sim:/medianParallel4/i_split/out2_SEND
add wave sim:/medianParallel4/i_split/out2_RDY

add wave sim:/medianParallel4/i_split/out3_DATA
add wave sim:/medianParallel4/i_split/out3_ACK
add wave sim:/medianParallel4/i_split/out3_SEND
add wave sim:/medianParallel4/i_split/out3_RDY

add wave sim:/medianParallel4/i_split/out4_DATA
add wave sim:/medianParallel4/i_split/out4_ACK
add wave sim:/medianParallel4/i_split/out4_SEND
add wave sim:/medianParallel4/i_split/out4_RDY

add wave -noupdate -divider -height 20 i_join
add wave sim:/medianParallel4/i_join/CLK
add wave sim:/medianParallel4/i_join/in1_DATA
add wave sim:/medianParallel4/i_join/in1_ACK
add wave sim:/medianParallel4/i_join/in1_SEND

add wave sim:/medianParallel4/i_join/in2_DATA
add wave sim:/medianParallel4/i_join/in2_ACK
add wave sim:/medianParallel4/i_join/in2_SEND

add wave sim:/medianParallel4/i_join/in3_DATA
add wave sim:/medianParallel4/i_join/in3_ACK
add wave sim:/medianParallel4/i_join/in3_SEND

add wave sim:/medianParallel4/i_join/in4_DATA
add wave sim:/medianParallel4/i_join/in4_ACK
add wave sim:/medianParallel4/i_join/in4_SEND
add wave sim:/medianParallel4/i_join/out1_DATA
add wave sim:/medianParallel4/i_join/out1_ACK
add wave sim:/medianParallel4/i_join/out1_SEND
add wave sim:/medianParallel4/i_join/out1_RDY

add wave -noupdate -divider -height 20 i_medianRow2
add wave sim:/medianParallel4/i_medianRow2/CLK
add wave sim:/medianParallel4/i_medianRow2/in1_DATA
add wave sim:/medianParallel4/i_medianRow2/in1_ACK
add wave sim:/medianParallel4/i_medianRow2/in1_SEND
add wave sim:/medianParallel4/i_medianRow2/median_DATA
add wave sim:/medianParallel4/i_medianRow2/median_ACK
add wave sim:/medianParallel4/i_medianRow2/median_SEND
add wave sim:/medianParallel4/i_medianRow2/median_RDY

add wave -noupdate -divider -height 20 i_medianRow1
add wave sim:/medianParallel4/i_medianRow1/CLK
add wave sim:/medianParallel4/i_medianRow1/in1_DATA
add wave sim:/medianParallel4/i_medianRow1/in1_ACK
add wave sim:/medianParallel4/i_medianRow1/in1_SEND
add wave sim:/medianParallel4/i_medianRow1/median_DATA
add wave sim:/medianParallel4/i_medianRow1/median_ACK
add wave sim:/medianParallel4/i_medianRow1/median_SEND
add wave sim:/medianParallel4/i_medianRow1/median_RDY

add wave -noupdate -divider -height 20 i_medianRow3
add wave sim:/medianParallel4/i_medianRow3/CLK
add wave sim:/medianParallel4/i_medianRow3/in1_DATA
add wave sim:/medianParallel4/i_medianRow3/in1_ACK
add wave sim:/medianParallel4/i_medianRow3/in1_SEND
add wave sim:/medianParallel4/i_medianRow3/median_DATA
add wave sim:/medianParallel4/i_medianRow3/median_ACK
add wave sim:/medianParallel4/i_medianRow3/median_SEND
add wave sim:/medianParallel4/i_medianRow3/median_RDY

add wave -noupdate -divider -height 20 i_medianRow4
add wave sim:/medianParallel4/i_medianRow4/CLK
add wave sim:/medianParallel4/i_medianRow4/in1_DATA
add wave sim:/medianParallel4/i_medianRow4/in1_ACK
add wave sim:/medianParallel4/i_medianRow4/in1_SEND
add wave sim:/medianParallel4/i_medianRow4/median_DATA
add wave sim:/medianParallel4/i_medianRow4/median_ACK
add wave sim:/medianParallel4/i_medianRow4/median_SEND
add wave sim:/medianParallel4/i_medianRow4/median_RDY

## FIFO FULL
add wave -noupdate -divider -height 20 "FIFO FULL"
add wave -label split_in1_full sim:/medianParallel4/q_ai_split_in1/full
add wave -label split_in1_almost_full sim:/medianParallel4/q_ai_split_in1/almost_full
add wave -label join_in1_full sim:/medianParallel4/q_ai_join_in1/full
add wave -label join_in1_almost_full sim:/medianParallel4/q_ai_join_in1/almost_full
add wave -label join_in2_full sim:/medianParallel4/q_ai_join_in2/full
add wave -label join_in2_almost_full sim:/medianParallel4/q_ai_join_in2/almost_full
add wave -label join_in3_full sim:/medianParallel4/q_ai_join_in3/full
add wave -label join_in3_almost_full sim:/medianParallel4/q_ai_join_in3/almost_full
add wave -label join_in4_full sim:/medianParallel4/q_ai_join_in4/full
add wave -label join_in4_almost_full sim:/medianParallel4/q_ai_join_in4/almost_full
add wave -label medianRow2_in1_full sim:/medianParallel4/q_ai_medianRow2_in1/full
add wave -label medianRow2_in1_almost_full sim:/medianParallel4/q_ai_medianRow2_in1/almost_full
add wave -label medianRow1_in1_full sim:/medianParallel4/q_ai_medianRow1_in1/full
add wave -label medianRow1_in1_almost_full sim:/medianParallel4/q_ai_medianRow1_in1/almost_full
add wave -label medianRow3_in1_full sim:/medianParallel4/q_ai_medianRow3_in1/full
add wave -label medianRow3_in1_almost_full sim:/medianParallel4/q_ai_medianRow3_in1/almost_full
add wave -label medianRow4_in1_full sim:/medianParallel4/q_ai_medianRow4_in1/full
add wave -label medianRow4_in1_almost_full sim:/medianParallel4/q_ai_medianRow4_in1/almost_full


