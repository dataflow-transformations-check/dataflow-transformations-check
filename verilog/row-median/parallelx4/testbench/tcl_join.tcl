## ############################################################################
## __  ___ __ ___  _ __   ___  ___ 
## \ \/ / '__/ _ \| '_ \ / _ \/ __|
##  >  <| | | (_) | | | | (_) \__ \
## /_/\_\_|  \___/|_| |_|\___/|___/
## ############################################################################
## Xronos synthesizer
## Testbench TCL Script file for Actor: join 
## Date: 2018/02/23 14:27:59
## ############################################################################

## Set paths
set Lib "../lib/"
set LibSim "../lib/simulation"
set Rtl "../rtl"
set RtlGoDone "../rtl/rtlGoDone"
set Testbench "vhd"

## Create SystemBuilder design library
vlib SystemBuilder
vmap SystemBuilder SystemBuilder

## Compile the SystemBuilder Library from sources
vcom -reportprogress 300 -work SystemBuilder $Lib/systemBuilder/vhdl/sbtypes.vhdl
vcom -reportprogress 300 -work SystemBuilder $Lib/systemBuilder/vhdl/sbfifo.vhdl
vcom -reportprogress 300 -work SystemBuilder $Lib/systemBuilder/vhdl/sbfifo_behavioral.vhdl

## Create the work design library
if {[file exist work_join]} {vdel -all -lib work_join}
vlib work_join
vmap work_join work_join

## Compile the glbl constans given by Xilinx 
vlog -work work_join ../lib/simulation/glbl.v


# Compile sim package
vcom -93 -reportprogress 30 -work work_join $LibSim/sim_package.vhd
## Compile network instances and add them to work library	
vlog -work work_join $Rtl/join.v


## Compile the Testbench VHD
vcom -93 -check_synthesis -quiet -work work_join $Testbench/join_tb.vhd

## Start VSIM
vsim -voptargs="+acc" -L unisims_ver -L simprims_ver -t ns work_join.glbl work_join.join_tb
	
## Add clock(s) and reset signal
add wave -noupdate -divider -height 20 "CLK & RESET"

add wave sim:/join_tb/CLK
	add wave sim:/join_tb/RESET
	
	
	## Change radix to decimal
	radix -decimal

add wave -noupdate -divider -height 20  "Inputs: i_join"
add wave -label in1_DATA sim:/join_tb/i_join/in1_DATA
add wave -label in1_ACK sim:/join_tb/i_join/in1_ACK 
add wave -label in1_SEND sim:/join_tb/i_join/in1_SEND 

add wave -label in2_DATA sim:/join_tb/i_join/in2_DATA
add wave -label in2_ACK sim:/join_tb/i_join/in2_ACK 
add wave -label in2_SEND sim:/join_tb/i_join/in2_SEND 

add wave -label in3_DATA sim:/join_tb/i_join/in3_DATA
add wave -label in3_ACK sim:/join_tb/i_join/in3_ACK 
add wave -label in3_SEND sim:/join_tb/i_join/in3_SEND 

add wave -label in4_DATA sim:/join_tb/i_join/in4_DATA
add wave -label in4_ACK sim:/join_tb/i_join/in4_ACK 
add wave -label in4_SEND sim:/join_tb/i_join/in4_SEND 
add wave -noupdate -divider -height 20 "Outputs: i_join"
add wave -label out1_DATA sim:/join_tb/i_join/out1_DATA 
add wave -label out1_SEND sim:/join_tb/i_join/out1_ACK
add wave -label out1_SEND sim:/join_tb/i_join/out1_SEND
add wave -label out1_RDY sim:/join_tb/i_join/out1_RDY
add wave -noupdate -divider -height 20 "Go & Done" 
