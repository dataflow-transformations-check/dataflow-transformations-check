## ############################################################################
## __  ___ __ ___  _ __   ___  ___ 
## \ \/ / '__/ _ \| '_ \ / _ \/ __|
##  >  <| | | (_) | | | | (_) \__ \
## /_/\_\_|  \___/|_| |_|\___/|___/
## ############################################################################
## Xronos synthesizer
## Testbench TCL Script file for Network: medianParallel4 
## Date: 2018/02/23 14:28:04
## ############################################################################

## Set paths
set Lib "../lib/"
set LibSim "../lib/simulation"
set Rtl "../rtl"
set RtlGoDone "../rtl/rtlGoDone"
set Testbench "vhd"

## Create SystemBuilder design library
vlib SystemBuilder
vmap SystemBuilder SystemBuilder

## Compile the SystemBuilder Library from sources
vcom -reportprogress 300 -work SystemBuilder $Lib/systemBuilder/vhdl/sbtypes.vhdl
vcom -reportprogress 300 -work SystemBuilder $Lib/systemBuilder/vhdl/sbfifo.vhdl
vcom -reportprogress 300 -work SystemBuilder $Lib/systemBuilder/vhdl/sbfifo_behavioral.vhdl

## Create the work design library
if {[file exist work_medianParallel4]} {vdel -all -lib work_medianParallel4}
vlib work_medianParallel4
vmap work_medianParallel4 work_medianParallel4

## Compile the glbl constans given by Xilinx 
vlog -work work_medianParallel4 ../lib/simulation/glbl.v


# Compile sim package
vcom -93 -reportprogress 30 -work work_medianParallel4 $LibSim/sim_package.vhd
## Compile network instances and add them to work library	
vlog -work work_medianParallel4 $Rtl/split.v
vlog -work work_medianParallel4 $Rtl/join.v
vlog -work work_medianParallel4 $Rtl/medianRow2.v
vlog -work work_medianParallel4 $Rtl/medianRow1.v
vlog -work work_medianParallel4 $Rtl/medianRow3.v
vlog -work work_medianParallel4 $Rtl/medianRow4.v

## Compile the Top Network
vcom -93 -check_synthesis -quiet -work work_medianParallel4 $Rtl/medianParallel4.vhd

## Compile the Testbench VHD
vcom -93 -check_synthesis -quiet -work work_medianParallel4 $Testbench/medianParallel4_tb.vhd

## Start VSIM
vsim -voptargs="+acc" -L unisims_ver -L simprims_ver -t ns work_medianParallel4.glbl work_medianParallel4.medianParallel4_tb
	
## Add clock(s) and reset signal
add wave -noupdate -divider -height 20 "CLK & RESET"

add wave sim:/medianParallel4_tb/CLK
	add wave sim:/medianParallel4_tb/RESET
	
	
	## Change radix to decimal
	radix -decimal

add wave -noupdate -divider -height 20 ni_inPort
add wave sim:/medianParallel4_tb/inPort_DATA
add wave sim:/medianParallel4_tb/inPort_ACK
add wave sim:/medianParallel4_tb/inPort_SEND

add wave -noupdate -divider -height 20 no_outPort
add wave sim:/medianParallel4_tb/outPort_DATA
add wave sim:/medianParallel4_tb/outPort_SEND
add wave sim:/medianParallel4_tb/outPort_ACK
add wave sim:/medianParallel4_tb/outPort_RDY



add wave -noupdate -divider -height 20 i_split
add wave sim:/medianParallel4_tb/i_medianParallel4/i_split/CLK
add wave sim:/medianParallel4_tb/i_medianParallel4/i_split/in1_DATA
add wave sim:/medianParallel4_tb/i_medianParallel4/i_split/in1_ACK
add wave sim:/medianParallel4_tb/i_medianParallel4/i_split/in1_SEND
add wave sim:/medianParallel4_tb/i_medianParallel4/i_split/out1_DATA
add wave sim:/medianParallel4_tb/i_medianParallel4/i_split/out1_ACK
add wave sim:/medianParallel4_tb/i_medianParallel4/i_split/out1_SEND
add wave sim:/medianParallel4_tb/i_medianParallel4/i_split/out1_RDY

add wave sim:/medianParallel4_tb/i_medianParallel4/i_split/out2_DATA
add wave sim:/medianParallel4_tb/i_medianParallel4/i_split/out2_ACK
add wave sim:/medianParallel4_tb/i_medianParallel4/i_split/out2_SEND
add wave sim:/medianParallel4_tb/i_medianParallel4/i_split/out2_RDY

add wave sim:/medianParallel4_tb/i_medianParallel4/i_split/out3_DATA
add wave sim:/medianParallel4_tb/i_medianParallel4/i_split/out3_ACK
add wave sim:/medianParallel4_tb/i_medianParallel4/i_split/out3_SEND
add wave sim:/medianParallel4_tb/i_medianParallel4/i_split/out3_RDY

add wave sim:/medianParallel4_tb/i_medianParallel4/i_split/out4_DATA
add wave sim:/medianParallel4_tb/i_medianParallel4/i_split/out4_ACK
add wave sim:/medianParallel4_tb/i_medianParallel4/i_split/out4_SEND
add wave sim:/medianParallel4_tb/i_medianParallel4/i_split/out4_RDY

add wave -noupdate -divider -height 20 i_join
add wave sim:/medianParallel4_tb/i_medianParallel4/i_join/CLK
add wave sim:/medianParallel4_tb/i_medianParallel4/i_join/in1_DATA
add wave sim:/medianParallel4_tb/i_medianParallel4/i_join/in1_ACK
add wave sim:/medianParallel4_tb/i_medianParallel4/i_join/in1_SEND

add wave sim:/medianParallel4_tb/i_medianParallel4/i_join/in2_DATA
add wave sim:/medianParallel4_tb/i_medianParallel4/i_join/in2_ACK
add wave sim:/medianParallel4_tb/i_medianParallel4/i_join/in2_SEND

add wave sim:/medianParallel4_tb/i_medianParallel4/i_join/in3_DATA
add wave sim:/medianParallel4_tb/i_medianParallel4/i_join/in3_ACK
add wave sim:/medianParallel4_tb/i_medianParallel4/i_join/in3_SEND

add wave sim:/medianParallel4_tb/i_medianParallel4/i_join/in4_DATA
add wave sim:/medianParallel4_tb/i_medianParallel4/i_join/in4_ACK
add wave sim:/medianParallel4_tb/i_medianParallel4/i_join/in4_SEND
add wave sim:/medianParallel4_tb/i_medianParallel4/i_join/out1_DATA
add wave sim:/medianParallel4_tb/i_medianParallel4/i_join/out1_ACK
add wave sim:/medianParallel4_tb/i_medianParallel4/i_join/out1_SEND
add wave sim:/medianParallel4_tb/i_medianParallel4/i_join/out1_RDY

add wave -noupdate -divider -height 20 i_medianRow2
add wave sim:/medianParallel4_tb/i_medianParallel4/i_medianRow2/CLK
add wave sim:/medianParallel4_tb/i_medianParallel4/i_medianRow2/in1_DATA
add wave sim:/medianParallel4_tb/i_medianParallel4/i_medianRow2/in1_ACK
add wave sim:/medianParallel4_tb/i_medianParallel4/i_medianRow2/in1_SEND
add wave sim:/medianParallel4_tb/i_medianParallel4/i_medianRow2/median_DATA
add wave sim:/medianParallel4_tb/i_medianParallel4/i_medianRow2/median_ACK
add wave sim:/medianParallel4_tb/i_medianParallel4/i_medianRow2/median_SEND
add wave sim:/medianParallel4_tb/i_medianParallel4/i_medianRow2/median_RDY

add wave -noupdate -divider -height 20 i_medianRow1
add wave sim:/medianParallel4_tb/i_medianParallel4/i_medianRow1/CLK
add wave sim:/medianParallel4_tb/i_medianParallel4/i_medianRow1/in1_DATA
add wave sim:/medianParallel4_tb/i_medianParallel4/i_medianRow1/in1_ACK
add wave sim:/medianParallel4_tb/i_medianParallel4/i_medianRow1/in1_SEND
add wave sim:/medianParallel4_tb/i_medianParallel4/i_medianRow1/median_DATA
add wave sim:/medianParallel4_tb/i_medianParallel4/i_medianRow1/median_ACK
add wave sim:/medianParallel4_tb/i_medianParallel4/i_medianRow1/median_SEND
add wave sim:/medianParallel4_tb/i_medianParallel4/i_medianRow1/median_RDY

add wave -noupdate -divider -height 20 i_medianRow3
add wave sim:/medianParallel4_tb/i_medianParallel4/i_medianRow3/CLK
add wave sim:/medianParallel4_tb/i_medianParallel4/i_medianRow3/in1_DATA
add wave sim:/medianParallel4_tb/i_medianParallel4/i_medianRow3/in1_ACK
add wave sim:/medianParallel4_tb/i_medianParallel4/i_medianRow3/in1_SEND
add wave sim:/medianParallel4_tb/i_medianParallel4/i_medianRow3/median_DATA
add wave sim:/medianParallel4_tb/i_medianParallel4/i_medianRow3/median_ACK
add wave sim:/medianParallel4_tb/i_medianParallel4/i_medianRow3/median_SEND
add wave sim:/medianParallel4_tb/i_medianParallel4/i_medianRow3/median_RDY

add wave -noupdate -divider -height 20 i_medianRow4
add wave sim:/medianParallel4_tb/i_medianParallel4/i_medianRow4/CLK
add wave sim:/medianParallel4_tb/i_medianParallel4/i_medianRow4/in1_DATA
add wave sim:/medianParallel4_tb/i_medianParallel4/i_medianRow4/in1_ACK
add wave sim:/medianParallel4_tb/i_medianParallel4/i_medianRow4/in1_SEND
add wave sim:/medianParallel4_tb/i_medianParallel4/i_medianRow4/median_DATA
add wave sim:/medianParallel4_tb/i_medianParallel4/i_medianRow4/median_ACK
add wave sim:/medianParallel4_tb/i_medianParallel4/i_medianRow4/median_SEND
add wave sim:/medianParallel4_tb/i_medianParallel4/i_medianRow4/median_RDY

## FIFO FULL
add wave -noupdate -divider -height 20 "FIFO FULL"
add wave -label split_in1_full sim:/medianParallel4_tb/i_medianParallel4/q_ai_split_in1/full
add wave -label split_in1_almost_full sim:/medianParallel4_tb/i_medianParallel4/q_ai_split_in1/almost_full
add wave -label join_in1_full sim:/medianParallel4_tb/i_medianParallel4/q_ai_join_in1/full
add wave -label join_in1_almost_full sim:/medianParallel4_tb/i_medianParallel4/q_ai_join_in1/almost_full
add wave -label join_in2_full sim:/medianParallel4_tb/i_medianParallel4/q_ai_join_in2/full
add wave -label join_in2_almost_full sim:/medianParallel4_tb/i_medianParallel4/q_ai_join_in2/almost_full
add wave -label join_in3_full sim:/medianParallel4_tb/i_medianParallel4/q_ai_join_in3/full
add wave -label join_in3_almost_full sim:/medianParallel4_tb/i_medianParallel4/q_ai_join_in3/almost_full
add wave -label join_in4_full sim:/medianParallel4_tb/i_medianParallel4/q_ai_join_in4/full
add wave -label join_in4_almost_full sim:/medianParallel4_tb/i_medianParallel4/q_ai_join_in4/almost_full
add wave -label medianRow2_in1_full sim:/medianParallel4_tb/i_medianParallel4/q_ai_medianRow2_in1/full
add wave -label medianRow2_in1_almost_full sim:/medianParallel4_tb/i_medianParallel4/q_ai_medianRow2_in1/almost_full
add wave -label medianRow1_in1_full sim:/medianParallel4_tb/i_medianParallel4/q_ai_medianRow1_in1/full
add wave -label medianRow1_in1_almost_full sim:/medianParallel4_tb/i_medianParallel4/q_ai_medianRow1_in1/almost_full
add wave -label medianRow3_in1_full sim:/medianParallel4_tb/i_medianParallel4/q_ai_medianRow3_in1/full
add wave -label medianRow3_in1_almost_full sim:/medianParallel4_tb/i_medianParallel4/q_ai_medianRow3_in1/almost_full
add wave -label medianRow4_in1_full sim:/medianParallel4_tb/i_medianParallel4/q_ai_medianRow4_in1/full
add wave -label medianRow4_in1_almost_full sim:/medianParallel4_tb/i_medianParallel4/q_ai_medianRow4_in1/almost_full

