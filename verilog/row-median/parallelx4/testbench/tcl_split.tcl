## ############################################################################
## __  ___ __ ___  _ __   ___  ___ 
## \ \/ / '__/ _ \| '_ \ / _ \/ __|
##  >  <| | | (_) | | | | (_) \__ \
## /_/\_\_|  \___/|_| |_|\___/|___/
## ############################################################################
## Xronos synthesizer
## Testbench TCL Script file for Actor: split 
## Date: 2018/02/23 14:27:58
## ############################################################################

## Set paths
set Lib "../lib/"
set LibSim "../lib/simulation"
set Rtl "../rtl"
set RtlGoDone "../rtl/rtlGoDone"
set Testbench "vhd"

## Create SystemBuilder design library
vlib SystemBuilder
vmap SystemBuilder SystemBuilder

## Compile the SystemBuilder Library from sources
vcom -reportprogress 300 -work SystemBuilder $Lib/systemBuilder/vhdl/sbtypes.vhdl
vcom -reportprogress 300 -work SystemBuilder $Lib/systemBuilder/vhdl/sbfifo.vhdl
vcom -reportprogress 300 -work SystemBuilder $Lib/systemBuilder/vhdl/sbfifo_behavioral.vhdl

## Create the work design library
if {[file exist work_split]} {vdel -all -lib work_split}
vlib work_split
vmap work_split work_split

## Compile the glbl constans given by Xilinx 
vlog -work work_split ../lib/simulation/glbl.v


# Compile sim package
vcom -93 -reportprogress 30 -work work_split $LibSim/sim_package.vhd
## Compile network instances and add them to work library	
vlog -work work_split $Rtl/split.v


## Compile the Testbench VHD
vcom -93 -check_synthesis -quiet -work work_split $Testbench/split_tb.vhd

## Start VSIM
vsim -voptargs="+acc" -L unisims_ver -L simprims_ver -t ns work_split.glbl work_split.split_tb
	
## Add clock(s) and reset signal
add wave -noupdate -divider -height 20 "CLK & RESET"

add wave sim:/split_tb/CLK
	add wave sim:/split_tb/RESET
	
	
	## Change radix to decimal
	radix -decimal

add wave -noupdate -divider -height 20  "Inputs: i_split"
add wave -label in1_DATA sim:/split_tb/i_split/in1_DATA
add wave -label in1_ACK sim:/split_tb/i_split/in1_ACK 
add wave -label in1_SEND sim:/split_tb/i_split/in1_SEND 
add wave -noupdate -divider -height 20 "Outputs: i_split"
add wave -label out1_DATA sim:/split_tb/i_split/out1_DATA 
add wave -label out1_SEND sim:/split_tb/i_split/out1_ACK
add wave -label out1_SEND sim:/split_tb/i_split/out1_SEND
add wave -label out1_RDY sim:/split_tb/i_split/out1_RDY

add wave -label out2_DATA sim:/split_tb/i_split/out2_DATA 
add wave -label out2_SEND sim:/split_tb/i_split/out2_ACK
add wave -label out2_SEND sim:/split_tb/i_split/out2_SEND
add wave -label out2_RDY sim:/split_tb/i_split/out2_RDY

add wave -label out3_DATA sim:/split_tb/i_split/out3_DATA 
add wave -label out3_SEND sim:/split_tb/i_split/out3_ACK
add wave -label out3_SEND sim:/split_tb/i_split/out3_SEND
add wave -label out3_RDY sim:/split_tb/i_split/out3_RDY

add wave -label out4_DATA sim:/split_tb/i_split/out4_DATA 
add wave -label out4_SEND sim:/split_tb/i_split/out4_ACK
add wave -label out4_SEND sim:/split_tb/i_split/out4_SEND
add wave -label out4_RDY sim:/split_tb/i_split/out4_RDY
add wave -noupdate -divider -height 20 "Go & Done" 
