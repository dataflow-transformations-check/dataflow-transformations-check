-- ----------------------------------------------------------------------------
-- __  ___ __ ___  _ __   ___  ___ 
-- \ \/ / '__/ _ \| '_ \ / _ \/ __|
--  >  <| | | (_) | | | | (_) \__ \
-- /_/\_\_|  \___/|_| |_|\___/|___/
-- ----------------------------------------------------------------------------
-- Xronos synthesizer
-- Testbench for Instance: join 
-- Date: 2018/02/23 14:27:59
-- ----------------------------------------------------------------------------

library ieee, SystemBuilder;
use ieee.std_logic_1164.all;
use ieee.std_logic_unsigned.all;
use ieee.numeric_std.all;
use std.textio.all;

library work;
use work.sim_package.all;

entity join_tb is
end join_tb;

architecture arch_join_tb of join_tb is
	-----------------------------------------------------------------------
	-- Component declaration
	-----------------------------------------------------------------------
	component join
	port(
	    in1_data : IN std_logic_vector(7 downto 0);
	    in1_send : IN std_logic;
	    in1_ack : OUT std_logic;
	    in1_count : IN std_logic_vector(15 downto 0);
	    in2_data : IN std_logic_vector(7 downto 0);
	    in2_send : IN std_logic;
	    in2_ack : OUT std_logic;
	    in2_count : IN std_logic_vector(15 downto 0);
	    in3_data : IN std_logic_vector(7 downto 0);
	    in3_send : IN std_logic;
	    in3_ack : OUT std_logic;
	    in3_count : IN std_logic_vector(15 downto 0);
	    in4_data : IN std_logic_vector(7 downto 0);
	    in4_send : IN std_logic;
	    in4_ack : OUT std_logic;
	    in4_count : IN std_logic_vector(15 downto 0);
	    out1_data : OUT std_logic_vector(7 downto 0);
	    out1_send : OUT std_logic;
	    out1_ack : IN std_logic;
	    out1_rdy : IN std_logic;
	    out1_count : OUT std_logic_vector(15 downto 0);
	    CLK: IN std_logic;
	    RESET: IN std_logic);
	end component join;
	
		-----------------------------------------------------------------------
		-- Achitecure signals & constants
		-----------------------------------------------------------------------
		constant PERIOD : time := 100 ns;
		constant DUTY_CYCLE : real := 0.5;
		constant OFFSET : time := 100 ns;
		-- Severity level and testbench type types
		type severity_level is (note, warning, error, failure);
		type tb_type is (after_reset, read_file, CheckRead);
		
		-- Component input(s) signals
		signal tb_FSM_in1 : tb_type;
		file sim_file_join_in1 : text is "fifoTraces/join_in1.txt";
		signal in1_data : std_logic_vector(7 downto 0) := (others => '0');
		signal in1_send : std_logic := '0';
		signal in1_ack : std_logic;
		signal in1_rdy : std_logic;
		signal in1_count : std_logic_vector(15 downto 0) := (others => '0');
		-- Input component queue
		signal q_in1_data : std_logic_vector(7 downto 0) := (others => '0');
		signal q_in1_send : std_logic := '0';
		signal q_in1_ack : std_logic;
		signal q_in1_rdy : std_logic;
		signal q_in1_count : std_logic_vector(15 downto 0) := (others => '0');
		signal tb_FSM_in2 : tb_type;
		file sim_file_join_in2 : text is "fifoTraces/join_in2.txt";
		signal in2_data : std_logic_vector(7 downto 0) := (others => '0');
		signal in2_send : std_logic := '0';
		signal in2_ack : std_logic;
		signal in2_rdy : std_logic;
		signal in2_count : std_logic_vector(15 downto 0) := (others => '0');
		-- Input component queue
		signal q_in2_data : std_logic_vector(7 downto 0) := (others => '0');
		signal q_in2_send : std_logic := '0';
		signal q_in2_ack : std_logic;
		signal q_in2_rdy : std_logic;
		signal q_in2_count : std_logic_vector(15 downto 0) := (others => '0');
		signal tb_FSM_in3 : tb_type;
		file sim_file_join_in3 : text is "fifoTraces/join_in3.txt";
		signal in3_data : std_logic_vector(7 downto 0) := (others => '0');
		signal in3_send : std_logic := '0';
		signal in3_ack : std_logic;
		signal in3_rdy : std_logic;
		signal in3_count : std_logic_vector(15 downto 0) := (others => '0');
		-- Input component queue
		signal q_in3_data : std_logic_vector(7 downto 0) := (others => '0');
		signal q_in3_send : std_logic := '0';
		signal q_in3_ack : std_logic;
		signal q_in3_rdy : std_logic;
		signal q_in3_count : std_logic_vector(15 downto 0) := (others => '0');
		signal tb_FSM_in4 : tb_type;
		file sim_file_join_in4 : text is "fifoTraces/join_in4.txt";
		signal in4_data : std_logic_vector(7 downto 0) := (others => '0');
		signal in4_send : std_logic := '0';
		signal in4_ack : std_logic;
		signal in4_rdy : std_logic;
		signal in4_count : std_logic_vector(15 downto 0) := (others => '0');
		-- Input component queue
		signal q_in4_data : std_logic_vector(7 downto 0) := (others => '0');
		signal q_in4_send : std_logic := '0';
		signal q_in4_ack : std_logic;
		signal q_in4_rdy : std_logic;
		signal q_in4_count : std_logic_vector(15 downto 0) := (others => '0');
		
		-- Component Output(s) signals
		signal tb_FSM_out1 : tb_type;
		file sim_file_join_out1 : text is "fifoTraces/join_out1.txt";
		signal out1_data : std_logic_vector(7 downto 0) := (others => '0');
		signal out1_send : std_logic;
		signal out1_ack : std_logic := '0';
		signal out1_rdy : std_logic := '0';
		signal out1_count : std_logic_vector(15 downto 0) := (others => '0');
		
	
		-- GoDone Weights Output Files
		
		signal count : integer range 255 downto 0 := 0;
		signal CLK : std_logic := '0';
		signal reset : std_logic := '0';
		
begin
	
	i_join : join 
	port map(
		in1_data => q_in1_data,
		in1_send => q_in1_send,
		in1_ack => q_in1_ack,
		in1_count => q_in1_count,
		
		in2_data => q_in2_data,
		in2_send => q_in2_send,
		in2_ack => q_in2_ack,
		in2_count => q_in2_count,
		
		in3_data => q_in3_data,
		in3_send => q_in3_send,
		in3_ack => q_in3_ack,
		in3_count => q_in3_count,
		
		in4_data => q_in4_data,
		in4_send => q_in4_send,
		in4_ack => q_in4_ack,
		in4_count => q_in4_count,
		
		out1_data => out1_data,
		out1_send => out1_send,
		out1_ack => out1_ack,
		out1_rdy => out1_rdy,
		out1_count => out1_count,
		CLK => CLK,
		reset => reset);
	
	-- Input(s) queues
	q_in1 : entity systemBuilder.Queue(behavioral)
	generic map(length => 512, width => 8)
	port map(
		OUT_DATA => q_in1_data,
		OUT_SEND => q_in1_send,
		OUT_ACK => q_in1_ack,
		OUT_COUNT => q_in1_count,
	
		IN_DATA => in1_data,
		IN_SEND => in1_send,
		IN_ACK => in1_ack,
		IN_RDY => in1_rdy,
		IN_COUNT => in1_count,

		CLK => CLK,
		reset => reset);
	
	q_in2 : entity systemBuilder.Queue(behavioral)
	generic map(length => 512, width => 8)
	port map(
		OUT_DATA => q_in2_data,
		OUT_SEND => q_in2_send,
		OUT_ACK => q_in2_ack,
		OUT_COUNT => q_in2_count,
	
		IN_DATA => in2_data,
		IN_SEND => in2_send,
		IN_ACK => in2_ack,
		IN_RDY => in2_rdy,
		IN_COUNT => in2_count,

		CLK => CLK,
		reset => reset);
	
	q_in3 : entity systemBuilder.Queue(behavioral)
	generic map(length => 512, width => 8)
	port map(
		OUT_DATA => q_in3_data,
		OUT_SEND => q_in3_send,
		OUT_ACK => q_in3_ack,
		OUT_COUNT => q_in3_count,
	
		IN_DATA => in3_data,
		IN_SEND => in3_send,
		IN_ACK => in3_ack,
		IN_RDY => in3_rdy,
		IN_COUNT => in3_count,

		CLK => CLK,
		reset => reset);
	
	q_in4 : entity systemBuilder.Queue(behavioral)
	generic map(length => 512, width => 8)
	port map(
		OUT_DATA => q_in4_data,
		OUT_SEND => q_in4_send,
		OUT_ACK => q_in4_ack,
		OUT_COUNT => q_in4_count,
	
		IN_DATA => in4_data,
		IN_SEND => in4_send,
		IN_ACK => in4_ack,
		IN_RDY => in4_rdy,
		IN_COUNT => in4_count,

		CLK => CLK,
		reset => reset);

	-- Clock process
	
	clockProcess : process
	begin
	wait for OFFSET;
		clockLOOP : loop
			CLK <= '0';
			wait for (PERIOD - (PERIOD * DUTY_CYCLE));
			CLK <= '1';
			wait for (PERIOD * DUTY_CYCLE);
		end loop clockLOOP;
	end process;
	
	-- Reset process
	resetProcess : process
	begin
		wait for OFFSET;
		-- reset state for 100 ns.
		RESET <= '1';
		wait for 100 ns;
		RESET <= '0';
		wait;
	end process;

	
	-- Input(s) Waveform Generation
	WaveGen_Proc_In : process (CLK)
		variable Input_bit : integer range 2147483647 downto - 2147483648;
		variable line_number : line;
	begin
		if rising_edge(CLK) then
		-- Input port: in1 Waveform Generation
			case tb_FSM_in1 is
				when after_reset =>
					count <= count + 1;
					if (count = 15) then
						tb_FSM_in1 <= read_file;
						count <= 0;
					end if;
				when read_file =>
					if (not endfile (sim_file_join_in1)) then
						readline(sim_file_join_in1, line_number);
						if (line_number'length > 0 and line_number(1) /= '/') then
							read(line_number, input_bit);
							in1_data <= std_logic_vector(to_unsigned(input_bit, 8));
							in1_send <= '1';
							tb_FSM_in1 <= CheckRead;
						end if;
					end if;
				when CheckRead =>
					if (not endfile (sim_file_join_in1)) and in1_ack = '1' then
						readline(sim_file_join_in1, line_number);
						if (line_number'length > 0 and line_number(1) /= '/') then
							read(line_number, input_bit);
							in1_data <= std_logic_vector(to_unsigned(input_bit, 8));
							in1_send <= '1';
						end if;
					elsif (endfile (sim_file_join_in1)) then
						in1_send <= '0';
					end if;
				when others => null;
			end case;
		
		-- Input port: in2 Waveform Generation
			case tb_FSM_in2 is
				when after_reset =>
					count <= count + 1;
					if (count = 15) then
						tb_FSM_in2 <= read_file;
						count <= 0;
					end if;
				when read_file =>
					if (not endfile (sim_file_join_in2)) then
						readline(sim_file_join_in2, line_number);
						if (line_number'length > 0 and line_number(1) /= '/') then
							read(line_number, input_bit);
							in2_data <= std_logic_vector(to_unsigned(input_bit, 8));
							in2_send <= '1';
							tb_FSM_in2 <= CheckRead;
						end if;
					end if;
				when CheckRead =>
					if (not endfile (sim_file_join_in2)) and in2_ack = '1' then
						readline(sim_file_join_in2, line_number);
						if (line_number'length > 0 and line_number(1) /= '/') then
							read(line_number, input_bit);
							in2_data <= std_logic_vector(to_unsigned(input_bit, 8));
							in2_send <= '1';
						end if;
					elsif (endfile (sim_file_join_in2)) then
						in2_send <= '0';
					end if;
				when others => null;
			end case;
		
		-- Input port: in3 Waveform Generation
			case tb_FSM_in3 is
				when after_reset =>
					count <= count + 1;
					if (count = 15) then
						tb_FSM_in3 <= read_file;
						count <= 0;
					end if;
				when read_file =>
					if (not endfile (sim_file_join_in3)) then
						readline(sim_file_join_in3, line_number);
						if (line_number'length > 0 and line_number(1) /= '/') then
							read(line_number, input_bit);
							in3_data <= std_logic_vector(to_unsigned(input_bit, 8));
							in3_send <= '1';
							tb_FSM_in3 <= CheckRead;
						end if;
					end if;
				when CheckRead =>
					if (not endfile (sim_file_join_in3)) and in3_ack = '1' then
						readline(sim_file_join_in3, line_number);
						if (line_number'length > 0 and line_number(1) /= '/') then
							read(line_number, input_bit);
							in3_data <= std_logic_vector(to_unsigned(input_bit, 8));
							in3_send <= '1';
						end if;
					elsif (endfile (sim_file_join_in3)) then
						in3_send <= '0';
					end if;
				when others => null;
			end case;
		
		-- Input port: in4 Waveform Generation
			case tb_FSM_in4 is
				when after_reset =>
					count <= count + 1;
					if (count = 15) then
						tb_FSM_in4 <= read_file;
						count <= 0;
					end if;
				when read_file =>
					if (not endfile (sim_file_join_in4)) then
						readline(sim_file_join_in4, line_number);
						if (line_number'length > 0 and line_number(1) /= '/') then
							read(line_number, input_bit);
							in4_data <= std_logic_vector(to_unsigned(input_bit, 8));
							in4_send <= '1';
							tb_FSM_in4 <= CheckRead;
						end if;
					end if;
				when CheckRead =>
					if (not endfile (sim_file_join_in4)) and in4_ack = '1' then
						readline(sim_file_join_in4, line_number);
						if (line_number'length > 0 and line_number(1) /= '/') then
							read(line_number, input_bit);
							in4_data <= std_logic_vector(to_unsigned(input_bit, 8));
							in4_send <= '1';
						end if;
					elsif (endfile (sim_file_join_in4)) then
						in4_send <= '0';
					end if;
				when others => null;
			end case;
		end if;
	end process WaveGen_Proc_In;
	
	-- Output(s) waveform Generation
	out1_ack <= out1_send;
	out1_rdy <= '1';
	
	WaveGen_Proc_Out : process (CLK)
		variable Input_bit   : integer range 2147483647 downto - 2147483648;
		variable line_number : line;
		variable sequence_out1 : integer := 0;
	begin
		if (rising_edge(CLK)) then
		-- Output port: out1 Waveform Generation
			if (not endfile (sim_file_join_out1) and out1_send = '1') then
				readline(sim_file_join_out1, line_number);
					if (line_number'length > 0 and line_number(1) /= '/') then
						read(line_number, input_bit);
						assert (out1_data  = std_logic_vector(to_unsigned(input_bit, 8)))
						report "on port out1 incorrect value computed : " & str(to_integer(unsigned(out1_data))) & " instead of : " & str(input_bit) & " sequence " & str(sequence_out1)
						severity failure;
						
						assert (out1_data /= std_logic_vector(to_unsigned(input_bit, 8)))
						report "on port out1 correct value computed : " & str(to_integer(unsigned(out1_data))) & " equals : " & str(input_bit) & " sequence " & str(sequence_out1)
						severity note;
						sequence_out1 := sequence_out1 + 1;
					end if;
			end if;
		end if;			
	end process WaveGen_Proc_Out;
	
end architecture arch_join_tb; 
