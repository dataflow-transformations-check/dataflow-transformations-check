// __  ___ __ ___  _ __   ___  ___ 
// \ \/ / '__/ _ \| '_ \ / _ \/ __|
//  >  <| | | (_) | | | | (_) \__ \
// /_/\_\_|  \___/|_| |_|\___/|___/
// 
// Xronos synthesizer version
// Run date: Fri 23 Feb 2018 11:32:19 +0000
// 

module median(median_SEND, CLK, median_RDY, median_COUNT, RESET, median_DATA, in1_ACK, in1_COUNT, in1_SEND, in1_DATA, median_ACK);
output		median_SEND;
input		CLK;
input		median_RDY;
output	[15:0]	median_COUNT;
input		RESET;
output	[7:0]	median_DATA;
wire		compute_median_done;
output		in1_ACK;
input	[15:0]	in1_COUNT;
input		in1_SEND;
wire		receive_go;
wire		compute_median_go;
input	[7:0]	in1_DATA;
wire		receive_done;
input		median_ACK;
wire		bus_17786912_;
wire	[31:0]	bus_0f3ac66c_;
wire	[31:0]	bus_1f8cf92f_;
wire		bus_1956eb65_;
wire	[2:0]	bus_0f3c43fc_;
wire		bus_1cdd3b70_;
wire		bus_45babdcf_;
wire	[31:0]	bus_716dabe0_;
wire	[31:0]	compute_median_u8;
wire	[31:0]	compute_median_u0;
wire	[2:0]	compute_median_u10;
wire	[7:0]	compute_median_u13;
wire	[31:0]	compute_median_u2;
wire	[15:0]	compute_median_u11;
wire		compute_median_u12;
wire		compute_median_u7;
wire	[2:0]	compute_median_u3;
wire		compute_median_u4;
wire		median_compute_median_instance_DONE;
wire	[2:0]	compute_median_u6;
wire		compute_median;
wire	[31:0]	compute_median_u9;
wire	[31:0]	compute_median_u5;
wire		compute_median_u1;
wire		bus_4f91408e_;
wire	[2:0]	bus_53822b52_;
wire	[31:0]	bus_5af12cce_;
wire	[31:0]	bus_7dd6ccce_;
wire	[31:0]	bus_38d4bb64_;
wire		bus_49896a33_;
wire		bus_408b4fb8_;
wire		bus_111e0485_;
wire		bus_7428458a_;
wire		bus_0c8846ff_;
wire		bus_36b6dbe6_;
wire		bus_1df83f13_;
wire	[31:0]	bus_050f0d61_;
wire	[31:0]	bus_3ec9115c_;
wire		scheduler_u0;
wire		scheduler;
wire		scheduler_u1;
wire		median_scheduler_instance_DONE;
wire		scheduler_u2;
wire		median_receive_instance_DONE;
wire	[31:0]	receive_u3;
wire		receive_u5;
wire		receive;
wire	[31:0]	receive_u0;
wire	[2:0]	receive_u4;
wire		receive_u1;
wire	[31:0]	receive_u2;
wire		bus_49a1e211_;
wire		bus_0e1a355b_;
wire	[31:0]	bus_1cb1b5e7_;
assign median_SEND=compute_median_u12;
assign median_COUNT=compute_median_u11;
assign median_DATA=compute_median_u13;
assign compute_median_done=bus_17786912_;
assign in1_ACK=receive_u5;
assign receive_go=scheduler_u1;
assign compute_median_go=scheduler_u2;
assign receive_done=bus_0e1a355b_;
assign bus_17786912_=median_compute_median_instance_DONE&{1{median_compute_median_instance_DONE}};
median_simplememoryreferee_31f83d7e_ median_simplememoryreferee_31f83d7e__1(.bus_5e1ecb7b_(CLK), 
  .bus_4510fc53_(bus_0c8846ff_), .bus_60b0cad8_(bus_36b6dbe6_), .bus_432ad641_(bus_050f0d61_), 
  .bus_62745a7d_(compute_median_u1), .bus_7f72a35b_(compute_median_u2), .bus_5736a8de_(3'h1), 
  .bus_1f8cf92f_(bus_1f8cf92f_), .bus_716dabe0_(bus_716dabe0_), .bus_1cdd3b70_(bus_1cdd3b70_), 
  .bus_1956eb65_(bus_1956eb65_), .bus_0f3c43fc_(bus_0f3c43fc_), .bus_0f3ac66c_(bus_0f3ac66c_), 
  .bus_45babdcf_(bus_45babdcf_));
median_compute_median median_compute_median_instance(.CLK(CLK), .RESET(bus_0c8846ff_), 
  .GO(compute_median_go), .port_7108e507_(bus_45babdcf_), .port_174c54f1_(bus_0f3ac66c_), 
  .port_4613254f_(bus_7428458a_), .port_442ccc2b_(bus_38d4bb64_), .port_48cec9ff_(bus_7428458a_), 
  .RESULT(compute_median), .RESULT_u0(compute_median_u0), .RESULT_u8(compute_median_u1), 
  .RESULT_u9(compute_median_u2), .RESULT_u10(compute_median_u3), .RESULT_u1(compute_median_u4), 
  .RESULT_u2(compute_median_u5), .RESULT_u3(compute_median_u6), .RESULT_u4(compute_median_u7), 
  .RESULT_u5(compute_median_u8), .RESULT_u6(compute_median_u9), .RESULT_u7(compute_median_u10), 
  .RESULT_u11(compute_median_u11), .RESULT_u12(compute_median_u12), .RESULT_u13(compute_median_u13), 
  .DONE(median_compute_median_instance_DONE));
median_Kicker_0 median_Kicker_0_1(.CLK(CLK), .RESET(bus_0c8846ff_), .bus_4f91408e_(bus_4f91408e_));
median_simplememoryreferee_7b450cc8_ median_simplememoryreferee_7b450cc8__1(.bus_2c4c0b94_(CLK), 
  .bus_3c9c1fff_(bus_0c8846ff_), .bus_26d19f9b_(bus_1df83f13_), .bus_054bb205_(bus_3ec9115c_), 
  .bus_37e86666_(receive_u1), .bus_4fcbb409_({24'b0, receive_u3[7:0]}), .bus_78bdc7f4_(receive_u2), 
  .bus_0fc8b651_(3'h1), .bus_598fb6fc_(compute_median_u4), .bus_20df7f94_(compute_median_u7), 
  .bus_6a2dbbbd_(compute_median_u9), .bus_2d07ab6b_(compute_median_u8), .bus_7894dbc6_(3'h1), 
  .bus_7dd6ccce_(bus_7dd6ccce_), .bus_5af12cce_(bus_5af12cce_), .bus_49896a33_(bus_49896a33_), 
  .bus_408b4fb8_(bus_408b4fb8_), .bus_53822b52_(bus_53822b52_), .bus_111e0485_(bus_111e0485_), 
  .bus_38d4bb64_(bus_38d4bb64_), .bus_7428458a_(bus_7428458a_));
median_globalreset_physical_2ed1cfc2_ median_globalreset_physical_2ed1cfc2__1(.bus_22196b3e_(CLK), 
  .bus_406f6bac_(RESET), .bus_0c8846ff_(bus_0c8846ff_));
median_structuralmemory_0e906503_ median_structuralmemory_0e906503__1(.CLK_u0(CLK), 
  .bus_50eadaff_(bus_0c8846ff_), .bus_55d8c80f_(bus_5af12cce_), .bus_5666c327_(3'h1), 
  .bus_67045deb_(bus_408b4fb8_), .bus_14a66fca_(bus_49896a33_), .bus_08bdcda2_(bus_7dd6ccce_), 
  .bus_17cdf95b_(bus_716dabe0_), .bus_307e6f2c_(3'h1), .bus_07eeda1f_(bus_1956eb65_), 
  .bus_3ec9115c_(bus_3ec9115c_), .bus_1df83f13_(bus_1df83f13_), .bus_050f0d61_(bus_050f0d61_), 
  .bus_36b6dbe6_(bus_36b6dbe6_));
median_scheduler median_scheduler_instance(.CLK(CLK), .RESET(bus_0c8846ff_), .GO(bus_4f91408e_), 
  .port_6d827604_(bus_49a1e211_), .port_5152fb59_(bus_1cb1b5e7_), .port_28a828b3_(in1_SEND), 
  .port_7dd939ef_(median_RDY), .port_2fb4bf16_(receive_done), .port_0c8374e8_(compute_median_done), 
  .RESULT(scheduler), .RESULT_u14(scheduler_u0), .RESULT_u15(scheduler_u1), .RESULT_u16(scheduler_u2), 
  .DONE(median_scheduler_instance_DONE));
median_receive median_receive_instance(.CLK(CLK), .RESET(bus_0c8846ff_), .GO(receive_go), 
  .port_35ec2878_(bus_1cb1b5e7_), .port_65355017_(bus_111e0485_), .port_38ee27c6_(in1_DATA), 
  .RESULT(receive), .RESULT_u17(receive_u0), .RESULT_u18(receive_u1), .RESULT_u19(receive_u2), 
  .RESULT_u20(receive_u3), .RESULT_u21(receive_u4), .RESULT_u22(receive_u5), .DONE(median_receive_instance_DONE));
median_stateVar_fsmState_median median_stateVar_fsmState_median_1(.bus_474ddb5c_(CLK), 
  .bus_2fdc1657_(bus_0c8846ff_), .bus_3273b19d_(scheduler), .bus_54ecd834_(scheduler_u0), 
  .bus_49a1e211_(bus_49a1e211_));
assign bus_0e1a355b_=median_receive_instance_DONE&{1{median_receive_instance_DONE}};
median_stateVar_i median_stateVar_i_1(.bus_5872f0b5_(CLK), .bus_3585a683_(bus_0c8846ff_), 
  .bus_6bbc6583_(receive), .bus_3caddcb4_(receive_u0), .bus_2efc8353_(compute_median), 
  .bus_7e5894da_(32'h0), .bus_1cb1b5e7_(bus_1cb1b5e7_));
endmodule



module median_simplememoryreferee_31f83d7e_(bus_5e1ecb7b_, bus_4510fc53_, bus_60b0cad8_, bus_432ad641_, bus_62745a7d_, bus_7f72a35b_, bus_5736a8de_, bus_1f8cf92f_, bus_716dabe0_, bus_1cdd3b70_, bus_1956eb65_, bus_0f3c43fc_, bus_0f3ac66c_, bus_45babdcf_);
input		bus_5e1ecb7b_;
input		bus_4510fc53_;
input		bus_60b0cad8_;
input	[31:0]	bus_432ad641_;
input		bus_62745a7d_;
input	[31:0]	bus_7f72a35b_;
input	[2:0]	bus_5736a8de_;
output	[31:0]	bus_1f8cf92f_;
output	[31:0]	bus_716dabe0_;
output		bus_1cdd3b70_;
output		bus_1956eb65_;
output	[2:0]	bus_0f3c43fc_;
output	[31:0]	bus_0f3ac66c_;
output		bus_45babdcf_;
assign bus_1f8cf92f_=32'h0;
assign bus_716dabe0_=bus_7f72a35b_;
assign bus_1cdd3b70_=1'h0;
assign bus_1956eb65_=bus_62745a7d_;
assign bus_0f3c43fc_=3'h1;
assign bus_0f3ac66c_=bus_432ad641_;
assign bus_45babdcf_=bus_60b0cad8_;
endmodule



module median_compute_median(CLK, RESET, GO, port_4613254f_, port_442ccc2b_, port_48cec9ff_, port_7108e507_, port_174c54f1_, DONE, RESULT, RESULT_u0, RESULT_u1, RESULT_u2, RESULT_u3, RESULT_u4, RESULT_u5, RESULT_u6, RESULT_u7, RESULT_u8, RESULT_u9, RESULT_u10, RESULT_u11, RESULT_u12, RESULT_u13);
input		CLK;
input		RESET;
input		GO;
input		port_4613254f_;
input	[31:0]	port_442ccc2b_;
input		port_48cec9ff_;
input		port_7108e507_;
input	[31:0]	port_174c54f1_;
output		DONE;
output		RESULT;
output	[31:0]	RESULT_u0;
output		RESULT_u1;
output	[31:0]	RESULT_u2;
output	[2:0]	RESULT_u3;
output		RESULT_u4;
output	[31:0]	RESULT_u5;
output	[31:0]	RESULT_u6;
output	[2:0]	RESULT_u7;
output		RESULT_u8;
output	[31:0]	RESULT_u9;
output	[2:0]	RESULT_u10;
output	[15:0]	RESULT_u11;
output		RESULT_u12;
output	[7:0]	RESULT_u13;
wire		lessThan;
wire signed	[32:0]	lessThan_a_signed;
wire signed	[32:0]	lessThan_b_signed;
wire		and_u0_u0;
wire		and_u1_u0;
wire		not_u0_u0;
wire		and_u2_u0;
reg	[31:0]	syncEnable_u0=32'h0;
reg		syncEnable_u1_u0=1'h0;
reg	[31:0]	syncEnable_u2_u0=32'h0;
wire	[31:0]	add;
wire		and_u3_u0;
wire	[31:0]	add_u0;
wire	[31:0]	add_u1;
wire		and_u4_u0;
wire signed	[31:0]	greaterThan_a_signed;
wire signed	[31:0]	greaterThan_b_signed;
wire		greaterThan;
wire		not_u1_u0;
wire		and_u5_u0;
wire		and_u6_u0;
wire	[31:0]	add_u2;
wire		and_u7_u0;
wire	[31:0]	add_u3;
wire	[31:0]	add_u4;
wire		and_u8_u0;
wire	[31:0]	add_u5;
wire		and_u9_u0;
reg		reg_7f3ea2cd_u0=1'h0;
wire		or_u0_u0;
wire	[31:0]	add_u6;
wire	[31:0]	add_u7;
wire		or_u1_u0;
reg		reg_156ae380_u0=1'h0;
wire		and_u10_u0;
reg	[31:0]	syncEnable_u3_u0=32'h0;
wire	[31:0]	mux_u0;
wire	[31:0]	mux_u1_u0;
wire		or_u2_u0;
reg	[31:0]	syncEnable_u4_u0=32'h0;
reg	[31:0]	syncEnable_u5_u0=32'h0;
reg		block_GO_delayed_u0=1'h0;
reg	[31:0]	syncEnable_u6_u0=32'h0;
reg		block_GO_delayed_result_delayed_u0=1'h0;
reg	[31:0]	syncEnable_u7_u0=32'h0;
reg	[31:0]	syncEnable_u8_u0=32'h0;
reg		reg_579a34fb_u0=1'h0;
wire		and_u11_u0;
wire	[31:0]	mux_u2_u0;
reg		reg_58a387ab_u0=1'h0;
reg		reg_03f4af5d_u0=1'h0;
reg	[31:0]	syncEnable_u9_u0=32'h0;
wire		or_u3_u0;
reg		and_delayed_u0=1'h0;
reg		reg_579a34fb_result_delayed_u0=1'h0;
wire		mux_u3_u0;
reg	[31:0]	syncEnable_u10_u0=32'h0;
wire		and_u12_u0;
wire	[31:0]	mux_u4_u0;
reg		syncEnable_u11_u0=1'h0;
wire	[31:0]	add_u8;
reg	[31:0]	syncEnable_u12_u0=32'h0;
reg		block_GO_delayed_u1_u0=1'h0;
reg	[31:0]	syncEnable_u13_u0=32'h0;
reg	[31:0]	syncEnable_u14_u0=32'h0;
reg	[31:0]	syncEnable_u15_u0=32'h0;
reg		syncEnable_u16_u0=1'h0;
reg	[31:0]	syncEnable_u17_u0=32'h0;
reg	[31:0]	syncEnable_u18_u0=32'h0;
wire		or_u4_u0;
wire	[31:0]	mux_u5_u0;
wire	[31:0]	mux_u6_u0;
wire		or_u5_u0;
wire		or_u6_u0;
wire	[31:0]	mux_u7_u0;
wire	[31:0]	mux_u8_u0;
wire	[31:0]	mux_u9_u0;
wire	[31:0]	mux_u10_u0;
wire	[31:0]	mux_u11_u0;
wire		mux_u12_u0;
wire	[15:0]	add_u9;
reg	[31:0]	latch_14166892_reg=32'h0;
wire	[31:0]	latch_14166892_out;
wire	[31:0]	latch_1fa40449_out;
reg	[31:0]	latch_1fa40449_reg=32'h0;
reg	[15:0]	syncEnable_u19_u0=16'h0;
reg		reg_27e5eeb7_u0=1'h0;
reg	[31:0]	latch_616c27df_reg=32'h0;
wire	[31:0]	latch_616c27df_out;
wire		latch_1c5f62c7_out;
reg		latch_1c5f62c7_reg=1'h0;
reg	[31:0]	latch_20ca2813_reg=32'h0;
wire	[31:0]	latch_20ca2813_out;
wire		bus_79c91bce_;
reg		scoreboard_1eead035_reg0=1'h0;
wire		scoreboard_1eead035_resOr1;
wire		scoreboard_1eead035_and;
wire		scoreboard_1eead035_resOr0;
reg		scoreboard_1eead035_reg1=1'h0;
wire	[15:0]	lessThan_u0_a_unsigned;
wire	[15:0]	lessThan_u0_b_unsigned;
wire		lessThan_u0;
wire		andOp;
wire		and_u13_u0;
wire		not_u2_u0;
wire		and_u14_u0;
wire		and_u15_u0;
wire		and_u16_u0;
wire	[31:0]	mux_u13_u0;
wire	[31:0]	mux_u14_u0;
reg	[31:0]	fbReg_tmp_u0=32'h0;
reg		syncEnable_u20_u0=1'h0;
reg	[31:0]	fbReg_tmp_row0_u0=32'h0;
reg	[15:0]	fbReg_idx_u0=16'h0;
reg		fbReg_swapped_u0=1'h0;
wire	[31:0]	mux_u15_u0;
reg		loopControl_u0=1'h0;
reg	[31:0]	fbReg_tmp_row1_u0=32'h0;
wire		or_u7_u0;
wire		mux_u16_u0;
reg	[31:0]	fbReg_tmp_row_u0=32'h0;
wire	[15:0]	mux_u17_u0;
wire	[31:0]	mux_u18_u0;
wire		and_u17_u0;
wire	[15:0]	simplePinWrite;
wire		simplePinWrite_u0;
wire	[7:0]	simplePinWrite_u1;
reg		reg_05082a2e_u0=1'h0;
wire		or_u8_u0;
wire	[31:0]	mux_u19_u0;
reg		reg_05082a2e_result_delayed_u0=1'h0;
assign lessThan_a_signed={1'b0, mux_u9_u0};
assign lessThan_b_signed=33'h64;
assign lessThan=lessThan_a_signed<lessThan_b_signed;
assign and_u0_u0=or_u6_u0&not_u0_u0;
assign and_u1_u0=or_u6_u0&lessThan;
assign not_u0_u0=~lessThan;
assign and_u2_u0=and_u1_u0&or_u6_u0;
always @(posedge CLK)
begin
if (or_u6_u0)
syncEnable_u0<=mux_u11_u0;
end
always @(posedge CLK)
begin
if (or_u6_u0)
syncEnable_u1_u0<=mux_u12_u0;
end
always @(posedge CLK)
begin
if (or_u6_u0)
syncEnable_u2_u0<=mux_u8_u0;
end
assign add=mux_u9_u0+32'h0;
assign and_u3_u0=and_u2_u0&port_7108e507_;
assign add_u0=mux_u9_u0+32'h1;
assign add_u1=add_u0+32'h0;
assign and_u4_u0=and_u2_u0&port_48cec9ff_;
assign greaterThan_a_signed=syncEnable_u18_u0;
assign greaterThan_b_signed=syncEnable_u14_u0;
assign greaterThan=greaterThan_a_signed>greaterThan_b_signed;
assign not_u1_u0=~greaterThan;
assign and_u5_u0=block_GO_delayed_u1_u0&greaterThan;
assign and_u6_u0=block_GO_delayed_u1_u0&not_u1_u0;
assign add_u2=syncEnable_u15_u0+32'h0;
assign and_u7_u0=and_u11_u0&port_7108e507_;
assign add_u3=syncEnable_u15_u0+32'h1;
assign add_u4=add_u3+32'h0;
assign and_u8_u0=and_u11_u0&port_48cec9ff_;
assign add_u5=syncEnable_u15_u0+32'h0;
assign and_u9_u0=reg_7f3ea2cd_u0&port_48cec9ff_;
always @(posedge CLK or posedge block_GO_delayed_u0 or posedge or_u0_u0)
begin
if (or_u0_u0)
reg_7f3ea2cd_u0<=1'h0;
else if (block_GO_delayed_u0)
reg_7f3ea2cd_u0<=1'h1;
else reg_7f3ea2cd_u0<=reg_7f3ea2cd_u0;
end
assign or_u0_u0=and_u9_u0|RESET;
assign add_u6=syncEnable_u15_u0+32'h1;
assign add_u7=add_u6+32'h0;
assign or_u1_u0=and_u10_u0|RESET;
always @(posedge CLK or posedge block_GO_delayed_result_delayed_u0 or posedge or_u1_u0)
begin
if (or_u1_u0)
reg_156ae380_u0<=1'h0;
else if (block_GO_delayed_result_delayed_u0)
reg_156ae380_u0<=1'h1;
else reg_156ae380_u0<=reg_156ae380_u0;
end
assign and_u10_u0=reg_156ae380_u0&port_48cec9ff_;
always @(posedge CLK)
begin
if (and_u11_u0)
syncEnable_u3_u0<=add_u7;
end
assign mux_u0=(block_GO_delayed_u0)?syncEnable_u7_u0:syncEnable_u5_u0;
assign mux_u1_u0=({32{block_GO_delayed_u0}}&syncEnable_u6_u0)|({32{block_GO_delayed_result_delayed_u0}}&syncEnable_u3_u0)|({32{and_u11_u0}}&add_u4);
assign or_u2_u0=block_GO_delayed_u0|block_GO_delayed_result_delayed_u0;
always @(posedge CLK)
begin
if (and_u11_u0)
syncEnable_u4_u0<=port_442ccc2b_;
end
always @(posedge CLK)
begin
if (and_u11_u0)
syncEnable_u5_u0<=port_174c54f1_;
end
always @(posedge CLK or posedge RESET)
begin
if (RESET)
block_GO_delayed_u0<=1'h0;
else block_GO_delayed_u0<=and_u11_u0;
end
always @(posedge CLK)
begin
if (and_u11_u0)
syncEnable_u6_u0<=add_u5;
end
always @(posedge CLK or posedge RESET)
begin
if (RESET)
block_GO_delayed_result_delayed_u0<=1'h0;
else block_GO_delayed_result_delayed_u0<=block_GO_delayed_u0;
end
always @(posedge CLK)
begin
if (and_u11_u0)
syncEnable_u7_u0<=port_442ccc2b_;
end
always @(posedge CLK)
begin
if (and_u11_u0)
syncEnable_u8_u0<=port_174c54f1_;
end
always @(posedge CLK or posedge RESET)
begin
if (RESET)
reg_579a34fb_u0<=1'h0;
else reg_579a34fb_u0<=and_delayed_u0;
end
assign and_u11_u0=and_u5_u0&block_GO_delayed_u1_u0;
assign mux_u2_u0=(reg_58a387ab_u0)?syncEnable_u8_u0:syncEnable_u10_u0;
always @(posedge CLK or posedge RESET)
begin
if (RESET)
reg_58a387ab_u0<=1'h0;
else reg_58a387ab_u0<=reg_579a34fb_result_delayed_u0;
end
always @(posedge CLK or posedge RESET)
begin
if (RESET)
reg_03f4af5d_u0<=1'h0;
else reg_03f4af5d_u0<=and_u12_u0;
end
always @(posedge CLK)
begin
if (block_GO_delayed_u1_u0)
syncEnable_u9_u0<=syncEnable_u12_u0;
end
assign or_u3_u0=reg_58a387ab_u0|reg_03f4af5d_u0;
always @(posedge CLK or posedge RESET)
begin
if (RESET)
and_delayed_u0<=1'h0;
else and_delayed_u0<=and_u11_u0;
end
always @(posedge CLK or posedge RESET)
begin
if (RESET)
reg_579a34fb_result_delayed_u0<=1'h0;
else reg_579a34fb_result_delayed_u0<=reg_579a34fb_u0;
end
assign mux_u3_u0=(reg_58a387ab_u0)?1'h1:syncEnable_u11_u0;
always @(posedge CLK)
begin
if (block_GO_delayed_u1_u0)
syncEnable_u10_u0<=syncEnable_u17_u0;
end
assign and_u12_u0=and_u6_u0&block_GO_delayed_u1_u0;
assign mux_u4_u0=(reg_58a387ab_u0)?syncEnable_u4_u0:syncEnable_u9_u0;
always @(posedge CLK)
begin
if (block_GO_delayed_u1_u0)
syncEnable_u11_u0<=syncEnable_u16_u0;
end
assign add_u8=mux_u9_u0+32'h1;
always @(posedge CLK)
begin
if (and_u2_u0)
syncEnable_u12_u0<=mux_u10_u0;
end
always @(posedge CLK or posedge RESET)
begin
if (RESET)
block_GO_delayed_u1_u0<=1'h0;
else block_GO_delayed_u1_u0<=and_u2_u0;
end
always @(posedge CLK)
begin
if (and_u2_u0)
syncEnable_u13_u0<=add_u8;
end
always @(posedge CLK)
begin
if (and_u2_u0)
syncEnable_u14_u0<=port_442ccc2b_;
end
always @(posedge CLK)
begin
if (and_u2_u0)
syncEnable_u15_u0<=mux_u9_u0;
end
always @(posedge CLK)
begin
if (and_u2_u0)
syncEnable_u16_u0<=mux_u12_u0;
end
always @(posedge CLK)
begin
if (and_u2_u0)
syncEnable_u17_u0<=mux_u11_u0;
end
always @(posedge CLK)
begin
if (and_u2_u0)
syncEnable_u18_u0<=port_174c54f1_;
end
assign or_u4_u0=and_u2_u0|and_u11_u0;
assign mux_u5_u0=({32{or_u2_u0}}&mux_u1_u0)|({32{and_u2_u0}}&add_u1)|({32{and_u11_u0}}&mux_u1_u0);
assign mux_u6_u0=(and_u2_u0)?add:add_u2;
assign or_u5_u0=and_u2_u0|and_u11_u0;
assign or_u6_u0=and_u15_u0|or_u3_u0;
assign mux_u7_u0=(and_u15_u0)?mux_u13_u0:syncEnable_u14_u0;
assign mux_u8_u0=(and_u15_u0)?mux_u15_u0:syncEnable_u18_u0;
assign mux_u9_u0=(and_u15_u0)?32'h0:syncEnable_u13_u0;
assign mux_u10_u0=(and_u15_u0)?mux_u18_u0:mux_u4_u0;
assign mux_u11_u0=(and_u15_u0)?mux_u14_u0:mux_u2_u0;
assign mux_u12_u0=(and_u15_u0)?1'h0:mux_u3_u0;
assign add_u9=mux_u17_u0+16'h1;
always @(posedge CLK)
begin
if (or_u3_u0)
latch_14166892_reg<=syncEnable_u14_u0;
end
assign latch_14166892_out=(or_u3_u0)?syncEnable_u14_u0:latch_14166892_reg;
assign latch_1fa40449_out=(or_u3_u0)?mux_u4_u0:latch_1fa40449_reg;
always @(posedge CLK)
begin
if (or_u3_u0)
latch_1fa40449_reg<=mux_u4_u0;
end
always @(posedge CLK)
begin
if (and_u15_u0)
syncEnable_u19_u0<=add_u9;
end
always @(posedge CLK or posedge RESET)
begin
if (RESET)
reg_27e5eeb7_u0<=1'h0;
else reg_27e5eeb7_u0<=or_u3_u0;
end
always @(posedge CLK)
begin
if (or_u3_u0)
latch_616c27df_reg<=syncEnable_u0;
end
assign latch_616c27df_out=(or_u3_u0)?syncEnable_u0:latch_616c27df_reg;
assign latch_1c5f62c7_out=(or_u3_u0)?syncEnable_u1_u0:latch_1c5f62c7_reg;
always @(posedge CLK)
begin
if (or_u3_u0)
latch_1c5f62c7_reg<=syncEnable_u1_u0;
end
always @(posedge CLK)
begin
if (or_u3_u0)
latch_20ca2813_reg<=syncEnable_u2_u0;
end
assign latch_20ca2813_out=(or_u3_u0)?syncEnable_u2_u0:latch_20ca2813_reg;
assign bus_79c91bce_=scoreboard_1eead035_and|RESET;
always @(posedge CLK)
begin
if (bus_79c91bce_)
scoreboard_1eead035_reg0<=1'h0;
else if (or_u3_u0)
scoreboard_1eead035_reg0<=1'h1;
else scoreboard_1eead035_reg0<=scoreboard_1eead035_reg0;
end
assign scoreboard_1eead035_resOr1=reg_27e5eeb7_u0|scoreboard_1eead035_reg1;
assign scoreboard_1eead035_and=scoreboard_1eead035_resOr0&scoreboard_1eead035_resOr1;
assign scoreboard_1eead035_resOr0=or_u3_u0|scoreboard_1eead035_reg0;
always @(posedge CLK)
begin
if (bus_79c91bce_)
scoreboard_1eead035_reg1<=1'h0;
else if (reg_27e5eeb7_u0)
scoreboard_1eead035_reg1<=1'h1;
else scoreboard_1eead035_reg1<=scoreboard_1eead035_reg1;
end
assign lessThan_u0_a_unsigned=mux_u17_u0;
assign lessThan_u0_b_unsigned=16'h65;
assign lessThan_u0=lessThan_u0_a_unsigned<lessThan_u0_b_unsigned;
assign andOp=lessThan_u0&mux_u16_u0;
assign and_u13_u0=or_u7_u0&andOp;
assign not_u2_u0=~andOp;
assign and_u14_u0=or_u7_u0&not_u2_u0;
assign and_u15_u0=and_u13_u0&or_u7_u0;
assign and_u16_u0=and_u14_u0&or_u7_u0;
assign mux_u13_u0=(GO)?32'h0:fbReg_tmp_row0_u0;
assign mux_u14_u0=(GO)?32'h0:fbReg_tmp_u0;
always @(posedge CLK)
begin
if (scoreboard_1eead035_and)
fbReg_tmp_u0<=latch_616c27df_out;
end
always @(posedge CLK)
begin
if (GO)
syncEnable_u20_u0<=RESET;
end
always @(posedge CLK)
begin
if (scoreboard_1eead035_and)
fbReg_tmp_row0_u0<=latch_14166892_out;
end
always @(posedge CLK)
begin
if (scoreboard_1eead035_and)
fbReg_idx_u0<=syncEnable_u19_u0;
end
always @(posedge CLK)
begin
if (scoreboard_1eead035_and)
fbReg_swapped_u0<=latch_1c5f62c7_out;
end
assign mux_u15_u0=(GO)?32'h0:fbReg_tmp_row_u0;
always @(posedge CLK or posedge syncEnable_u20_u0)
begin
if (syncEnable_u20_u0)
loopControl_u0<=1'h0;
else loopControl_u0<=scoreboard_1eead035_and;
end
always @(posedge CLK)
begin
if (scoreboard_1eead035_and)
fbReg_tmp_row1_u0<=latch_1fa40449_out;
end
assign or_u7_u0=GO|loopControl_u0;
assign mux_u16_u0=(GO)?1'h0:fbReg_swapped_u0;
always @(posedge CLK)
begin
if (scoreboard_1eead035_and)
fbReg_tmp_row_u0<=latch_20ca2813_out;
end
assign mux_u17_u0=(GO)?16'h0:fbReg_idx_u0;
assign mux_u18_u0=(GO)?32'h0:fbReg_tmp_row1_u0;
assign and_u17_u0=reg_05082a2e_u0&port_7108e507_;
assign simplePinWrite=16'h1&{16{1'h1}};
assign simplePinWrite_u0=reg_05082a2e_u0&{1{reg_05082a2e_u0}};
assign simplePinWrite_u1=port_174c54f1_[7:0];
always @(posedge CLK or posedge RESET)
begin
if (RESET)
reg_05082a2e_u0<=1'h0;
else reg_05082a2e_u0<=and_u16_u0;
end
assign or_u8_u0=or_u5_u0|reg_05082a2e_u0;
assign mux_u19_u0=(or_u5_u0)?mux_u6_u0:32'h32;
always @(posedge CLK or posedge RESET)
begin
if (RESET)
reg_05082a2e_result_delayed_u0<=1'h0;
else reg_05082a2e_result_delayed_u0<=reg_05082a2e_u0;
end
assign DONE=reg_05082a2e_result_delayed_u0;
assign RESULT=GO;
assign RESULT_u0=32'h0;
assign RESULT_u1=or_u4_u0;
assign RESULT_u2=mux_u5_u0;
assign RESULT_u3=3'h1;
assign RESULT_u4=or_u2_u0;
assign RESULT_u5=mux_u5_u0;
assign RESULT_u6=mux_u0;
assign RESULT_u7=3'h1;
assign RESULT_u8=or_u8_u0;
assign RESULT_u9=mux_u19_u0;
assign RESULT_u10=3'h1;
assign RESULT_u11=simplePinWrite;
assign RESULT_u12=simplePinWrite_u0;
assign RESULT_u13=simplePinWrite_u1;
endmodule



module median_Kicker_0(CLK, RESET, bus_4f91408e_);
input		CLK;
input		RESET;
output		bus_4f91408e_;
wire		bus_3c3ca5c7_;
wire		bus_227f76e9_;
reg		kicker_res=1'h0;
reg		kicker_1=1'h0;
reg		kicker_2=1'h0;
wire		bus_0d24e3ca_;
wire		bus_09ac4cb3_;
assign bus_3c3ca5c7_=~RESET;
assign bus_4f91408e_=kicker_res;
assign bus_227f76e9_=bus_3c3ca5c7_&kicker_1;
always @(posedge CLK)
begin
kicker_res<=bus_0d24e3ca_;
end
always @(posedge CLK)
begin
kicker_1<=bus_3c3ca5c7_;
end
always @(posedge CLK)
begin
kicker_2<=bus_227f76e9_;
end
assign bus_0d24e3ca_=kicker_1&bus_3c3ca5c7_&bus_09ac4cb3_;
assign bus_09ac4cb3_=~kicker_2;
endmodule



module median_simplememoryreferee_7b450cc8_(bus_2c4c0b94_, bus_3c9c1fff_, bus_26d19f9b_, bus_054bb205_, bus_37e86666_, bus_4fcbb409_, bus_78bdc7f4_, bus_0fc8b651_, bus_598fb6fc_, bus_20df7f94_, bus_6a2dbbbd_, bus_2d07ab6b_, bus_7894dbc6_, bus_7dd6ccce_, bus_5af12cce_, bus_49896a33_, bus_408b4fb8_, bus_53822b52_, bus_111e0485_, bus_38d4bb64_, bus_7428458a_);
input		bus_2c4c0b94_;
input		bus_3c9c1fff_;
input		bus_26d19f9b_;
input	[31:0]	bus_054bb205_;
input		bus_37e86666_;
input	[31:0]	bus_4fcbb409_;
input	[31:0]	bus_78bdc7f4_;
input	[2:0]	bus_0fc8b651_;
input		bus_598fb6fc_;
input		bus_20df7f94_;
input	[31:0]	bus_6a2dbbbd_;
input	[31:0]	bus_2d07ab6b_;
input	[2:0]	bus_7894dbc6_;
output	[31:0]	bus_7dd6ccce_;
output	[31:0]	bus_5af12cce_;
output		bus_49896a33_;
output		bus_408b4fb8_;
output	[2:0]	bus_53822b52_;
output		bus_111e0485_;
output	[31:0]	bus_38d4bb64_;
output		bus_7428458a_;
wire		not_02b10fb1_u0;
reg		done_qual_u0=1'h0;
wire	[31:0]	mux_00dde218_u0;
wire		or_46259443_u0;
wire	[31:0]	mux_09bef0d7_u0;
wire		or_570f1786_u0;
wire		not_569e5fb5_u0;
reg		done_qual_u1_u0=1'h0;
wire		and_32af2ca1_u0;
wire		or_75b08d63_u0;
wire		and_03392f00_u0;
wire		or_08233858_u0;
wire		or_2c25eb5e_u0;
assign not_02b10fb1_u0=~bus_26d19f9b_;
always @(posedge bus_2c4c0b94_)
begin
if (bus_3c9c1fff_)
done_qual_u0<=1'h0;
else done_qual_u0<=or_570f1786_u0;
end
assign mux_00dde218_u0=(bus_37e86666_)?{24'b0, bus_4fcbb409_[7:0]}:bus_6a2dbbbd_;
assign or_46259443_u0=bus_37e86666_|done_qual_u1_u0;
assign mux_09bef0d7_u0=(bus_37e86666_)?bus_78bdc7f4_:bus_2d07ab6b_;
assign or_570f1786_u0=bus_598fb6fc_|bus_20df7f94_;
assign not_569e5fb5_u0=~bus_26d19f9b_;
always @(posedge bus_2c4c0b94_)
begin
if (bus_3c9c1fff_)
done_qual_u1_u0<=1'h0;
else done_qual_u1_u0<=bus_37e86666_;
end
assign and_32af2ca1_u0=or_46259443_u0&bus_26d19f9b_;
assign or_75b08d63_u0=bus_37e86666_|bus_20df7f94_;
assign bus_7dd6ccce_=mux_00dde218_u0;
assign bus_5af12cce_=mux_09bef0d7_u0;
assign bus_49896a33_=or_75b08d63_u0;
assign bus_408b4fb8_=or_08233858_u0;
assign bus_53822b52_=3'h1;
assign bus_111e0485_=and_32af2ca1_u0;
assign bus_38d4bb64_=bus_054bb205_;
assign bus_7428458a_=and_03392f00_u0;
assign and_03392f00_u0=or_2c25eb5e_u0&bus_26d19f9b_;
assign or_08233858_u0=bus_37e86666_|or_570f1786_u0;
assign or_2c25eb5e_u0=or_570f1786_u0|done_qual_u0;
endmodule



module median_globalreset_physical_2ed1cfc2_(bus_22196b3e_, bus_406f6bac_, bus_0c8846ff_);
input		bus_22196b3e_;
input		bus_406f6bac_;
output		bus_0c8846ff_;
wire		and_27326270_u0;
wire		or_3f2178f5_u0;
reg		final_u0=1'h1;
reg		sample_u0=1'h0;
reg		cross_u0=1'h0;
wire		not_064a5a31_u0;
reg		glitch_u0=1'h0;
assign bus_0c8846ff_=or_3f2178f5_u0;
assign and_27326270_u0=cross_u0&glitch_u0;
assign or_3f2178f5_u0=bus_406f6bac_|final_u0;
always @(posedge bus_22196b3e_)
begin
final_u0<=not_064a5a31_u0;
end
always @(posedge bus_22196b3e_)
begin
sample_u0<=1'h1;
end
always @(posedge bus_22196b3e_)
begin
cross_u0<=sample_u0;
end
assign not_064a5a31_u0=~and_27326270_u0;
always @(posedge bus_22196b3e_)
begin
glitch_u0<=cross_u0;
end
endmodule



module median_forge_memory_101x32_1(CLK, ENA, WEA, DINA, ENB, ADDRA, ADDRB, DOUTA, DOUTB, DONEA, DONEB);
input		CLK;
input		ENA;
input		WEA;
input	[31:0]	DINA;
input		ENB;
input	[31:0]	ADDRA;
input	[31:0]	ADDRB;
output	[31:0]	DOUTA;
output	[31:0]	DOUTB;
output		DONEA;
output		DONEB;
wire		wea_0;
wire	[31:0]	pre_douta_0;
wire	[31:0]	pre_doutb_0;
wire		wea_1;
wire	[31:0]	pre_douta_1;
wire	[31:0]	pre_doutb_1;
reg		wea_done;
reg		rea_done;
reg	[31:0]	mux_outa;
reg	[31:0]	mux_outb;
assign wea_0=WEA&(ADDRA[31:6]==26'h0);
assign wea_1=WEA&(ADDRA[31:6]==26'h1);
always @(posedge CLK)
begin
wea_done<=WEA;
end
assign DONEA=rea_done|wea_done;
always @(posedge CLK)
begin
end
assign DONEB=ENB;
always @(pre_douta_0 or pre_douta_1 or ADDRA)
begin
case (ADDRA[31:6])26'd0:mux_outa=pre_douta_0;
26'd1:mux_outa=pre_douta_1;
default:mux_outa=32'h0;
endcase end
assign DOUTA=mux_outa;
always @(pre_doutb_0 or pre_doutb_1 or ADDRB)
begin
case (ADDRB[31:6])26'd0:mux_outb=pre_doutb_0;
26'd1:mux_outb=pre_doutb_1;
default:mux_outb=32'h0;
endcase end
assign DOUTB=mux_outb;
// Memory array element: COL: 0, ROW: 0
//  Initialization of Dual Port LUT ram now done through explicit
// parameter setting.
RAM64X1D#(.INIT(64'h0000000000000000))RAM64X1D_instance_0(.WCLK(CLK), .WE(wea_0), 
  .A0(ADDRA[0]), .A1(ADDRA[1]), .A2(ADDRA[2]), .A3(ADDRA[3]), .A4(ADDRA[4]), .A5(ADDRA[5]), 
  .D(DINA[0]), .SPO(pre_douta_0[0]), .DPRA0(ADDRB[0]), .DPRA1(ADDRB[1]), .DPRA2(ADDRB[2]), 
  .DPRA3(ADDRB[3]), .DPRA4(ADDRB[4]), .DPRA5(ADDRB[5]), .DPO(pre_doutb_0[0]));
// Memory array element: COL: 0, ROW: 1
//  Initialization of Dual Port LUT ram now done through explicit
// parameter setting.
RAM64X1D#(.INIT(64'h0000000000000000))RAM64X1D_instance_1(.WCLK(CLK), .WE(wea_0), 
  .A0(ADDRA[0]), .A1(ADDRA[1]), .A2(ADDRA[2]), .A3(ADDRA[3]), .A4(ADDRA[4]), .A5(ADDRA[5]), 
  .D(DINA[1]), .SPO(pre_douta_0[1]), .DPRA0(ADDRB[0]), .DPRA1(ADDRB[1]), .DPRA2(ADDRB[2]), 
  .DPRA3(ADDRB[3]), .DPRA4(ADDRB[4]), .DPRA5(ADDRB[5]), .DPO(pre_doutb_0[1]));
// Memory array element: COL: 0, ROW: 2
//  Initialization of Dual Port LUT ram now done through explicit
// parameter setting.
RAM64X1D#(.INIT(64'h0000000000000000))RAM64X1D_instance_2(.WCLK(CLK), .WE(wea_0), 
  .A0(ADDRA[0]), .A1(ADDRA[1]), .A2(ADDRA[2]), .A3(ADDRA[3]), .A4(ADDRA[4]), .A5(ADDRA[5]), 
  .D(DINA[2]), .SPO(pre_douta_0[2]), .DPRA0(ADDRB[0]), .DPRA1(ADDRB[1]), .DPRA2(ADDRB[2]), 
  .DPRA3(ADDRB[3]), .DPRA4(ADDRB[4]), .DPRA5(ADDRB[5]), .DPO(pre_doutb_0[2]));
// Memory array element: COL: 0, ROW: 3
//  Initialization of Dual Port LUT ram now done through explicit
// parameter setting.
RAM64X1D#(.INIT(64'h0000000000000000))RAM64X1D_instance_3(.WCLK(CLK), .WE(wea_0), 
  .A0(ADDRA[0]), .A1(ADDRA[1]), .A2(ADDRA[2]), .A3(ADDRA[3]), .A4(ADDRA[4]), .A5(ADDRA[5]), 
  .D(DINA[3]), .SPO(pre_douta_0[3]), .DPRA0(ADDRB[0]), .DPRA1(ADDRB[1]), .DPRA2(ADDRB[2]), 
  .DPRA3(ADDRB[3]), .DPRA4(ADDRB[4]), .DPRA5(ADDRB[5]), .DPO(pre_doutb_0[3]));
// Memory array element: COL: 0, ROW: 4
//  Initialization of Dual Port LUT ram now done through explicit
// parameter setting.
RAM64X1D#(.INIT(64'h0000000000000000))RAM64X1D_instance_4(.WCLK(CLK), .WE(wea_0), 
  .A0(ADDRA[0]), .A1(ADDRA[1]), .A2(ADDRA[2]), .A3(ADDRA[3]), .A4(ADDRA[4]), .A5(ADDRA[5]), 
  .D(DINA[4]), .SPO(pre_douta_0[4]), .DPRA0(ADDRB[0]), .DPRA1(ADDRB[1]), .DPRA2(ADDRB[2]), 
  .DPRA3(ADDRB[3]), .DPRA4(ADDRB[4]), .DPRA5(ADDRB[5]), .DPO(pre_doutb_0[4]));
// Memory array element: COL: 0, ROW: 5
//  Initialization of Dual Port LUT ram now done through explicit
// parameter setting.
RAM64X1D#(.INIT(64'h0000000000000000))RAM64X1D_instance_5(.WCLK(CLK), .WE(wea_0), 
  .A0(ADDRA[0]), .A1(ADDRA[1]), .A2(ADDRA[2]), .A3(ADDRA[3]), .A4(ADDRA[4]), .A5(ADDRA[5]), 
  .D(DINA[5]), .SPO(pre_douta_0[5]), .DPRA0(ADDRB[0]), .DPRA1(ADDRB[1]), .DPRA2(ADDRB[2]), 
  .DPRA3(ADDRB[3]), .DPRA4(ADDRB[4]), .DPRA5(ADDRB[5]), .DPO(pre_doutb_0[5]));
// Memory array element: COL: 0, ROW: 6
//  Initialization of Dual Port LUT ram now done through explicit
// parameter setting.
RAM64X1D#(.INIT(64'h0000000000000000))RAM64X1D_instance_6(.WCLK(CLK), .WE(wea_0), 
  .A0(ADDRA[0]), .A1(ADDRA[1]), .A2(ADDRA[2]), .A3(ADDRA[3]), .A4(ADDRA[4]), .A5(ADDRA[5]), 
  .D(DINA[6]), .SPO(pre_douta_0[6]), .DPRA0(ADDRB[0]), .DPRA1(ADDRB[1]), .DPRA2(ADDRB[2]), 
  .DPRA3(ADDRB[3]), .DPRA4(ADDRB[4]), .DPRA5(ADDRB[5]), .DPO(pre_doutb_0[6]));
// Memory array element: COL: 0, ROW: 7
//  Initialization of Dual Port LUT ram now done through explicit
// parameter setting.
RAM64X1D#(.INIT(64'h0000000000000000))RAM64X1D_instance_7(.WCLK(CLK), .WE(wea_0), 
  .A0(ADDRA[0]), .A1(ADDRA[1]), .A2(ADDRA[2]), .A3(ADDRA[3]), .A4(ADDRA[4]), .A5(ADDRA[5]), 
  .D(DINA[7]), .SPO(pre_douta_0[7]), .DPRA0(ADDRB[0]), .DPRA1(ADDRB[1]), .DPRA2(ADDRB[2]), 
  .DPRA3(ADDRB[3]), .DPRA4(ADDRB[4]), .DPRA5(ADDRB[5]), .DPO(pre_doutb_0[7]));
// Memory array element: COL: 0, ROW: 8
//  Initialization of Dual Port LUT ram now done through explicit
// parameter setting.
RAM64X1D#(.INIT(64'h0000000000000000))RAM64X1D_instance_8(.WCLK(CLK), .WE(wea_0), 
  .A0(ADDRA[0]), .A1(ADDRA[1]), .A2(ADDRA[2]), .A3(ADDRA[3]), .A4(ADDRA[4]), .A5(ADDRA[5]), 
  .D(DINA[8]), .SPO(pre_douta_0[8]), .DPRA0(ADDRB[0]), .DPRA1(ADDRB[1]), .DPRA2(ADDRB[2]), 
  .DPRA3(ADDRB[3]), .DPRA4(ADDRB[4]), .DPRA5(ADDRB[5]), .DPO(pre_doutb_0[8]));
// Memory array element: COL: 0, ROW: 9
//  Initialization of Dual Port LUT ram now done through explicit
// parameter setting.
RAM64X1D#(.INIT(64'h0000000000000000))RAM64X1D_instance_9(.WCLK(CLK), .WE(wea_0), 
  .A0(ADDRA[0]), .A1(ADDRA[1]), .A2(ADDRA[2]), .A3(ADDRA[3]), .A4(ADDRA[4]), .A5(ADDRA[5]), 
  .D(DINA[9]), .SPO(pre_douta_0[9]), .DPRA0(ADDRB[0]), .DPRA1(ADDRB[1]), .DPRA2(ADDRB[2]), 
  .DPRA3(ADDRB[3]), .DPRA4(ADDRB[4]), .DPRA5(ADDRB[5]), .DPO(pre_doutb_0[9]));
// Memory array element: COL: 0, ROW: 10
//  Initialization of Dual Port LUT ram now done through explicit
// parameter setting.
RAM64X1D#(.INIT(64'h0000000000000000))RAM64X1D_instance_10(.WCLK(CLK), .WE(wea_0), 
  .A0(ADDRA[0]), .A1(ADDRA[1]), .A2(ADDRA[2]), .A3(ADDRA[3]), .A4(ADDRA[4]), .A5(ADDRA[5]), 
  .D(DINA[10]), .SPO(pre_douta_0[10]), .DPRA0(ADDRB[0]), .DPRA1(ADDRB[1]), .DPRA2(ADDRB[2]), 
  .DPRA3(ADDRB[3]), .DPRA4(ADDRB[4]), .DPRA5(ADDRB[5]), .DPO(pre_doutb_0[10]));
// Memory array element: COL: 0, ROW: 11
//  Initialization of Dual Port LUT ram now done through explicit
// parameter setting.
RAM64X1D#(.INIT(64'h0000000000000000))RAM64X1D_instance_11(.WCLK(CLK), .WE(wea_0), 
  .A0(ADDRA[0]), .A1(ADDRA[1]), .A2(ADDRA[2]), .A3(ADDRA[3]), .A4(ADDRA[4]), .A5(ADDRA[5]), 
  .D(DINA[11]), .SPO(pre_douta_0[11]), .DPRA0(ADDRB[0]), .DPRA1(ADDRB[1]), .DPRA2(ADDRB[2]), 
  .DPRA3(ADDRB[3]), .DPRA4(ADDRB[4]), .DPRA5(ADDRB[5]), .DPO(pre_doutb_0[11]));
// Memory array element: COL: 0, ROW: 12
//  Initialization of Dual Port LUT ram now done through explicit
// parameter setting.
RAM64X1D#(.INIT(64'h0000000000000000))RAM64X1D_instance_12(.WCLK(CLK), .WE(wea_0), 
  .A0(ADDRA[0]), .A1(ADDRA[1]), .A2(ADDRA[2]), .A3(ADDRA[3]), .A4(ADDRA[4]), .A5(ADDRA[5]), 
  .D(DINA[12]), .SPO(pre_douta_0[12]), .DPRA0(ADDRB[0]), .DPRA1(ADDRB[1]), .DPRA2(ADDRB[2]), 
  .DPRA3(ADDRB[3]), .DPRA4(ADDRB[4]), .DPRA5(ADDRB[5]), .DPO(pre_doutb_0[12]));
// Memory array element: COL: 0, ROW: 13
//  Initialization of Dual Port LUT ram now done through explicit
// parameter setting.
RAM64X1D#(.INIT(64'h0000000000000000))RAM64X1D_instance_13(.WCLK(CLK), .WE(wea_0), 
  .A0(ADDRA[0]), .A1(ADDRA[1]), .A2(ADDRA[2]), .A3(ADDRA[3]), .A4(ADDRA[4]), .A5(ADDRA[5]), 
  .D(DINA[13]), .SPO(pre_douta_0[13]), .DPRA0(ADDRB[0]), .DPRA1(ADDRB[1]), .DPRA2(ADDRB[2]), 
  .DPRA3(ADDRB[3]), .DPRA4(ADDRB[4]), .DPRA5(ADDRB[5]), .DPO(pre_doutb_0[13]));
// Memory array element: COL: 0, ROW: 14
//  Initialization of Dual Port LUT ram now done through explicit
// parameter setting.
RAM64X1D#(.INIT(64'h0000000000000000))RAM64X1D_instance_14(.WCLK(CLK), .WE(wea_0), 
  .A0(ADDRA[0]), .A1(ADDRA[1]), .A2(ADDRA[2]), .A3(ADDRA[3]), .A4(ADDRA[4]), .A5(ADDRA[5]), 
  .D(DINA[14]), .SPO(pre_douta_0[14]), .DPRA0(ADDRB[0]), .DPRA1(ADDRB[1]), .DPRA2(ADDRB[2]), 
  .DPRA3(ADDRB[3]), .DPRA4(ADDRB[4]), .DPRA5(ADDRB[5]), .DPO(pre_doutb_0[14]));
// Memory array element: COL: 0, ROW: 15
//  Initialization of Dual Port LUT ram now done through explicit
// parameter setting.
RAM64X1D#(.INIT(64'h0000000000000000))RAM64X1D_instance_15(.WCLK(CLK), .WE(wea_0), 
  .A0(ADDRA[0]), .A1(ADDRA[1]), .A2(ADDRA[2]), .A3(ADDRA[3]), .A4(ADDRA[4]), .A5(ADDRA[5]), 
  .D(DINA[15]), .SPO(pre_douta_0[15]), .DPRA0(ADDRB[0]), .DPRA1(ADDRB[1]), .DPRA2(ADDRB[2]), 
  .DPRA3(ADDRB[3]), .DPRA4(ADDRB[4]), .DPRA5(ADDRB[5]), .DPO(pre_doutb_0[15]));
// Memory array element: COL: 0, ROW: 16
//  Initialization of Dual Port LUT ram now done through explicit
// parameter setting.
RAM64X1D#(.INIT(64'h0000000000000000))RAM64X1D_instance_16(.WCLK(CLK), .WE(wea_0), 
  .A0(ADDRA[0]), .A1(ADDRA[1]), .A2(ADDRA[2]), .A3(ADDRA[3]), .A4(ADDRA[4]), .A5(ADDRA[5]), 
  .D(DINA[16]), .SPO(pre_douta_0[16]), .DPRA0(ADDRB[0]), .DPRA1(ADDRB[1]), .DPRA2(ADDRB[2]), 
  .DPRA3(ADDRB[3]), .DPRA4(ADDRB[4]), .DPRA5(ADDRB[5]), .DPO(pre_doutb_0[16]));
// Memory array element: COL: 0, ROW: 17
//  Initialization of Dual Port LUT ram now done through explicit
// parameter setting.
RAM64X1D#(.INIT(64'h0000000000000000))RAM64X1D_instance_17(.WCLK(CLK), .WE(wea_0), 
  .A0(ADDRA[0]), .A1(ADDRA[1]), .A2(ADDRA[2]), .A3(ADDRA[3]), .A4(ADDRA[4]), .A5(ADDRA[5]), 
  .D(DINA[17]), .SPO(pre_douta_0[17]), .DPRA0(ADDRB[0]), .DPRA1(ADDRB[1]), .DPRA2(ADDRB[2]), 
  .DPRA3(ADDRB[3]), .DPRA4(ADDRB[4]), .DPRA5(ADDRB[5]), .DPO(pre_doutb_0[17]));
// Memory array element: COL: 0, ROW: 18
//  Initialization of Dual Port LUT ram now done through explicit
// parameter setting.
RAM64X1D#(.INIT(64'h0000000000000000))RAM64X1D_instance_18(.WCLK(CLK), .WE(wea_0), 
  .A0(ADDRA[0]), .A1(ADDRA[1]), .A2(ADDRA[2]), .A3(ADDRA[3]), .A4(ADDRA[4]), .A5(ADDRA[5]), 
  .D(DINA[18]), .SPO(pre_douta_0[18]), .DPRA0(ADDRB[0]), .DPRA1(ADDRB[1]), .DPRA2(ADDRB[2]), 
  .DPRA3(ADDRB[3]), .DPRA4(ADDRB[4]), .DPRA5(ADDRB[5]), .DPO(pre_doutb_0[18]));
// Memory array element: COL: 0, ROW: 19
//  Initialization of Dual Port LUT ram now done through explicit
// parameter setting.
RAM64X1D#(.INIT(64'h0000000000000000))RAM64X1D_instance_19(.WCLK(CLK), .WE(wea_0), 
  .A0(ADDRA[0]), .A1(ADDRA[1]), .A2(ADDRA[2]), .A3(ADDRA[3]), .A4(ADDRA[4]), .A5(ADDRA[5]), 
  .D(DINA[19]), .SPO(pre_douta_0[19]), .DPRA0(ADDRB[0]), .DPRA1(ADDRB[1]), .DPRA2(ADDRB[2]), 
  .DPRA3(ADDRB[3]), .DPRA4(ADDRB[4]), .DPRA5(ADDRB[5]), .DPO(pre_doutb_0[19]));
// Memory array element: COL: 0, ROW: 20
//  Initialization of Dual Port LUT ram now done through explicit
// parameter setting.
RAM64X1D#(.INIT(64'h0000000000000000))RAM64X1D_instance_20(.WCLK(CLK), .WE(wea_0), 
  .A0(ADDRA[0]), .A1(ADDRA[1]), .A2(ADDRA[2]), .A3(ADDRA[3]), .A4(ADDRA[4]), .A5(ADDRA[5]), 
  .D(DINA[20]), .SPO(pre_douta_0[20]), .DPRA0(ADDRB[0]), .DPRA1(ADDRB[1]), .DPRA2(ADDRB[2]), 
  .DPRA3(ADDRB[3]), .DPRA4(ADDRB[4]), .DPRA5(ADDRB[5]), .DPO(pre_doutb_0[20]));
// Memory array element: COL: 0, ROW: 21
//  Initialization of Dual Port LUT ram now done through explicit
// parameter setting.
RAM64X1D#(.INIT(64'h0000000000000000))RAM64X1D_instance_21(.WCLK(CLK), .WE(wea_0), 
  .A0(ADDRA[0]), .A1(ADDRA[1]), .A2(ADDRA[2]), .A3(ADDRA[3]), .A4(ADDRA[4]), .A5(ADDRA[5]), 
  .D(DINA[21]), .SPO(pre_douta_0[21]), .DPRA0(ADDRB[0]), .DPRA1(ADDRB[1]), .DPRA2(ADDRB[2]), 
  .DPRA3(ADDRB[3]), .DPRA4(ADDRB[4]), .DPRA5(ADDRB[5]), .DPO(pre_doutb_0[21]));
// Memory array element: COL: 0, ROW: 22
//  Initialization of Dual Port LUT ram now done through explicit
// parameter setting.
RAM64X1D#(.INIT(64'h0000000000000000))RAM64X1D_instance_22(.WCLK(CLK), .WE(wea_0), 
  .A0(ADDRA[0]), .A1(ADDRA[1]), .A2(ADDRA[2]), .A3(ADDRA[3]), .A4(ADDRA[4]), .A5(ADDRA[5]), 
  .D(DINA[22]), .SPO(pre_douta_0[22]), .DPRA0(ADDRB[0]), .DPRA1(ADDRB[1]), .DPRA2(ADDRB[2]), 
  .DPRA3(ADDRB[3]), .DPRA4(ADDRB[4]), .DPRA5(ADDRB[5]), .DPO(pre_doutb_0[22]));
// Memory array element: COL: 0, ROW: 23
//  Initialization of Dual Port LUT ram now done through explicit
// parameter setting.
RAM64X1D#(.INIT(64'h0000000000000000))RAM64X1D_instance_23(.WCLK(CLK), .WE(wea_0), 
  .A0(ADDRA[0]), .A1(ADDRA[1]), .A2(ADDRA[2]), .A3(ADDRA[3]), .A4(ADDRA[4]), .A5(ADDRA[5]), 
  .D(DINA[23]), .SPO(pre_douta_0[23]), .DPRA0(ADDRB[0]), .DPRA1(ADDRB[1]), .DPRA2(ADDRB[2]), 
  .DPRA3(ADDRB[3]), .DPRA4(ADDRB[4]), .DPRA5(ADDRB[5]), .DPO(pre_doutb_0[23]));
// Memory array element: COL: 0, ROW: 24
//  Initialization of Dual Port LUT ram now done through explicit
// parameter setting.
RAM64X1D#(.INIT(64'h0000000000000000))RAM64X1D_instance_24(.WCLK(CLK), .WE(wea_0), 
  .A0(ADDRA[0]), .A1(ADDRA[1]), .A2(ADDRA[2]), .A3(ADDRA[3]), .A4(ADDRA[4]), .A5(ADDRA[5]), 
  .D(DINA[24]), .SPO(pre_douta_0[24]), .DPRA0(ADDRB[0]), .DPRA1(ADDRB[1]), .DPRA2(ADDRB[2]), 
  .DPRA3(ADDRB[3]), .DPRA4(ADDRB[4]), .DPRA5(ADDRB[5]), .DPO(pre_doutb_0[24]));
// Memory array element: COL: 0, ROW: 25
//  Initialization of Dual Port LUT ram now done through explicit
// parameter setting.
RAM64X1D#(.INIT(64'h0000000000000000))RAM64X1D_instance_25(.WCLK(CLK), .WE(wea_0), 
  .A0(ADDRA[0]), .A1(ADDRA[1]), .A2(ADDRA[2]), .A3(ADDRA[3]), .A4(ADDRA[4]), .A5(ADDRA[5]), 
  .D(DINA[25]), .SPO(pre_douta_0[25]), .DPRA0(ADDRB[0]), .DPRA1(ADDRB[1]), .DPRA2(ADDRB[2]), 
  .DPRA3(ADDRB[3]), .DPRA4(ADDRB[4]), .DPRA5(ADDRB[5]), .DPO(pre_doutb_0[25]));
// Memory array element: COL: 0, ROW: 26
//  Initialization of Dual Port LUT ram now done through explicit
// parameter setting.
RAM64X1D#(.INIT(64'h0000000000000000))RAM64X1D_instance_26(.WCLK(CLK), .WE(wea_0), 
  .A0(ADDRA[0]), .A1(ADDRA[1]), .A2(ADDRA[2]), .A3(ADDRA[3]), .A4(ADDRA[4]), .A5(ADDRA[5]), 
  .D(DINA[26]), .SPO(pre_douta_0[26]), .DPRA0(ADDRB[0]), .DPRA1(ADDRB[1]), .DPRA2(ADDRB[2]), 
  .DPRA3(ADDRB[3]), .DPRA4(ADDRB[4]), .DPRA5(ADDRB[5]), .DPO(pre_doutb_0[26]));
// Memory array element: COL: 0, ROW: 27
//  Initialization of Dual Port LUT ram now done through explicit
// parameter setting.
RAM64X1D#(.INIT(64'h0000000000000000))RAM64X1D_instance_27(.WCLK(CLK), .WE(wea_0), 
  .A0(ADDRA[0]), .A1(ADDRA[1]), .A2(ADDRA[2]), .A3(ADDRA[3]), .A4(ADDRA[4]), .A5(ADDRA[5]), 
  .D(DINA[27]), .SPO(pre_douta_0[27]), .DPRA0(ADDRB[0]), .DPRA1(ADDRB[1]), .DPRA2(ADDRB[2]), 
  .DPRA3(ADDRB[3]), .DPRA4(ADDRB[4]), .DPRA5(ADDRB[5]), .DPO(pre_doutb_0[27]));
// Memory array element: COL: 0, ROW: 28
//  Initialization of Dual Port LUT ram now done through explicit
// parameter setting.
RAM64X1D#(.INIT(64'h0000000000000000))RAM64X1D_instance_28(.WCLK(CLK), .WE(wea_0), 
  .A0(ADDRA[0]), .A1(ADDRA[1]), .A2(ADDRA[2]), .A3(ADDRA[3]), .A4(ADDRA[4]), .A5(ADDRA[5]), 
  .D(DINA[28]), .SPO(pre_douta_0[28]), .DPRA0(ADDRB[0]), .DPRA1(ADDRB[1]), .DPRA2(ADDRB[2]), 
  .DPRA3(ADDRB[3]), .DPRA4(ADDRB[4]), .DPRA5(ADDRB[5]), .DPO(pre_doutb_0[28]));
// Memory array element: COL: 0, ROW: 29
//  Initialization of Dual Port LUT ram now done through explicit
// parameter setting.
RAM64X1D#(.INIT(64'h0000000000000000))RAM64X1D_instance_29(.WCLK(CLK), .WE(wea_0), 
  .A0(ADDRA[0]), .A1(ADDRA[1]), .A2(ADDRA[2]), .A3(ADDRA[3]), .A4(ADDRA[4]), .A5(ADDRA[5]), 
  .D(DINA[29]), .SPO(pre_douta_0[29]), .DPRA0(ADDRB[0]), .DPRA1(ADDRB[1]), .DPRA2(ADDRB[2]), 
  .DPRA3(ADDRB[3]), .DPRA4(ADDRB[4]), .DPRA5(ADDRB[5]), .DPO(pre_doutb_0[29]));
// Memory array element: COL: 0, ROW: 30
//  Initialization of Dual Port LUT ram now done through explicit
// parameter setting.
RAM64X1D#(.INIT(64'h0000000000000000))RAM64X1D_instance_30(.WCLK(CLK), .WE(wea_0), 
  .A0(ADDRA[0]), .A1(ADDRA[1]), .A2(ADDRA[2]), .A3(ADDRA[3]), .A4(ADDRA[4]), .A5(ADDRA[5]), 
  .D(DINA[30]), .SPO(pre_douta_0[30]), .DPRA0(ADDRB[0]), .DPRA1(ADDRB[1]), .DPRA2(ADDRB[2]), 
  .DPRA3(ADDRB[3]), .DPRA4(ADDRB[4]), .DPRA5(ADDRB[5]), .DPO(pre_doutb_0[30]));
// Memory array element: COL: 0, ROW: 31
//  Initialization of Dual Port LUT ram now done through explicit
// parameter setting.
RAM64X1D#(.INIT(64'h0000000000000000))RAM64X1D_instance_31(.WCLK(CLK), .WE(wea_0), 
  .A0(ADDRA[0]), .A1(ADDRA[1]), .A2(ADDRA[2]), .A3(ADDRA[3]), .A4(ADDRA[4]), .A5(ADDRA[5]), 
  .D(DINA[31]), .SPO(pre_douta_0[31]), .DPRA0(ADDRB[0]), .DPRA1(ADDRB[1]), .DPRA2(ADDRB[2]), 
  .DPRA3(ADDRB[3]), .DPRA4(ADDRB[4]), .DPRA5(ADDRB[5]), .DPO(pre_doutb_0[31]));
// Memory array element: COL: 1, ROW: 0
//  Initialization of Dual Port LUT ram now done through explicit
// parameter setting.
RAM64X1D#(.INIT(64'h0000000000000000))RAM64X1D_instance_32(.WCLK(CLK), .WE(wea_1), 
  .A0(ADDRA[0]), .A1(ADDRA[1]), .A2(ADDRA[2]), .A3(ADDRA[3]), .A4(ADDRA[4]), .A5(ADDRA[5]), 
  .D(DINA[0]), .SPO(pre_douta_1[0]), .DPRA0(ADDRB[0]), .DPRA1(ADDRB[1]), .DPRA2(ADDRB[2]), 
  .DPRA3(ADDRB[3]), .DPRA4(ADDRB[4]), .DPRA5(ADDRB[5]), .DPO(pre_doutb_1[0]));
// Memory array element: COL: 1, ROW: 1
//  Initialization of Dual Port LUT ram now done through explicit
// parameter setting.
RAM64X1D#(.INIT(64'h0000000000000000))RAM64X1D_instance_33(.WCLK(CLK), .WE(wea_1), 
  .A0(ADDRA[0]), .A1(ADDRA[1]), .A2(ADDRA[2]), .A3(ADDRA[3]), .A4(ADDRA[4]), .A5(ADDRA[5]), 
  .D(DINA[1]), .SPO(pre_douta_1[1]), .DPRA0(ADDRB[0]), .DPRA1(ADDRB[1]), .DPRA2(ADDRB[2]), 
  .DPRA3(ADDRB[3]), .DPRA4(ADDRB[4]), .DPRA5(ADDRB[5]), .DPO(pre_doutb_1[1]));
// Memory array element: COL: 1, ROW: 2
//  Initialization of Dual Port LUT ram now done through explicit
// parameter setting.
RAM64X1D#(.INIT(64'h0000000000000000))RAM64X1D_instance_34(.WCLK(CLK), .WE(wea_1), 
  .A0(ADDRA[0]), .A1(ADDRA[1]), .A2(ADDRA[2]), .A3(ADDRA[3]), .A4(ADDRA[4]), .A5(ADDRA[5]), 
  .D(DINA[2]), .SPO(pre_douta_1[2]), .DPRA0(ADDRB[0]), .DPRA1(ADDRB[1]), .DPRA2(ADDRB[2]), 
  .DPRA3(ADDRB[3]), .DPRA4(ADDRB[4]), .DPRA5(ADDRB[5]), .DPO(pre_doutb_1[2]));
// Memory array element: COL: 1, ROW: 3
//  Initialization of Dual Port LUT ram now done through explicit
// parameter setting.
RAM64X1D#(.INIT(64'h0000000000000000))RAM64X1D_instance_35(.WCLK(CLK), .WE(wea_1), 
  .A0(ADDRA[0]), .A1(ADDRA[1]), .A2(ADDRA[2]), .A3(ADDRA[3]), .A4(ADDRA[4]), .A5(ADDRA[5]), 
  .D(DINA[3]), .SPO(pre_douta_1[3]), .DPRA0(ADDRB[0]), .DPRA1(ADDRB[1]), .DPRA2(ADDRB[2]), 
  .DPRA3(ADDRB[3]), .DPRA4(ADDRB[4]), .DPRA5(ADDRB[5]), .DPO(pre_doutb_1[3]));
// Memory array element: COL: 1, ROW: 4
//  Initialization of Dual Port LUT ram now done through explicit
// parameter setting.
RAM64X1D#(.INIT(64'h0000000000000000))RAM64X1D_instance_36(.WCLK(CLK), .WE(wea_1), 
  .A0(ADDRA[0]), .A1(ADDRA[1]), .A2(ADDRA[2]), .A3(ADDRA[3]), .A4(ADDRA[4]), .A5(ADDRA[5]), 
  .D(DINA[4]), .SPO(pre_douta_1[4]), .DPRA0(ADDRB[0]), .DPRA1(ADDRB[1]), .DPRA2(ADDRB[2]), 
  .DPRA3(ADDRB[3]), .DPRA4(ADDRB[4]), .DPRA5(ADDRB[5]), .DPO(pre_doutb_1[4]));
// Memory array element: COL: 1, ROW: 5
//  Initialization of Dual Port LUT ram now done through explicit
// parameter setting.
RAM64X1D#(.INIT(64'h0000000000000000))RAM64X1D_instance_37(.WCLK(CLK), .WE(wea_1), 
  .A0(ADDRA[0]), .A1(ADDRA[1]), .A2(ADDRA[2]), .A3(ADDRA[3]), .A4(ADDRA[4]), .A5(ADDRA[5]), 
  .D(DINA[5]), .SPO(pre_douta_1[5]), .DPRA0(ADDRB[0]), .DPRA1(ADDRB[1]), .DPRA2(ADDRB[2]), 
  .DPRA3(ADDRB[3]), .DPRA4(ADDRB[4]), .DPRA5(ADDRB[5]), .DPO(pre_doutb_1[5]));
// Memory array element: COL: 1, ROW: 6
//  Initialization of Dual Port LUT ram now done through explicit
// parameter setting.
RAM64X1D#(.INIT(64'h0000000000000000))RAM64X1D_instance_38(.WCLK(CLK), .WE(wea_1), 
  .A0(ADDRA[0]), .A1(ADDRA[1]), .A2(ADDRA[2]), .A3(ADDRA[3]), .A4(ADDRA[4]), .A5(ADDRA[5]), 
  .D(DINA[6]), .SPO(pre_douta_1[6]), .DPRA0(ADDRB[0]), .DPRA1(ADDRB[1]), .DPRA2(ADDRB[2]), 
  .DPRA3(ADDRB[3]), .DPRA4(ADDRB[4]), .DPRA5(ADDRB[5]), .DPO(pre_doutb_1[6]));
// Memory array element: COL: 1, ROW: 7
//  Initialization of Dual Port LUT ram now done through explicit
// parameter setting.
RAM64X1D#(.INIT(64'h0000000000000000))RAM64X1D_instance_39(.WCLK(CLK), .WE(wea_1), 
  .A0(ADDRA[0]), .A1(ADDRA[1]), .A2(ADDRA[2]), .A3(ADDRA[3]), .A4(ADDRA[4]), .A5(ADDRA[5]), 
  .D(DINA[7]), .SPO(pre_douta_1[7]), .DPRA0(ADDRB[0]), .DPRA1(ADDRB[1]), .DPRA2(ADDRB[2]), 
  .DPRA3(ADDRB[3]), .DPRA4(ADDRB[4]), .DPRA5(ADDRB[5]), .DPO(pre_doutb_1[7]));
// Memory array element: COL: 1, ROW: 8
//  Initialization of Dual Port LUT ram now done through explicit
// parameter setting.
RAM64X1D#(.INIT(64'h0000000000000000))RAM64X1D_instance_40(.WCLK(CLK), .WE(wea_1), 
  .A0(ADDRA[0]), .A1(ADDRA[1]), .A2(ADDRA[2]), .A3(ADDRA[3]), .A4(ADDRA[4]), .A5(ADDRA[5]), 
  .D(DINA[8]), .SPO(pre_douta_1[8]), .DPRA0(ADDRB[0]), .DPRA1(ADDRB[1]), .DPRA2(ADDRB[2]), 
  .DPRA3(ADDRB[3]), .DPRA4(ADDRB[4]), .DPRA5(ADDRB[5]), .DPO(pre_doutb_1[8]));
// Memory array element: COL: 1, ROW: 9
//  Initialization of Dual Port LUT ram now done through explicit
// parameter setting.
RAM64X1D#(.INIT(64'h0000000000000000))RAM64X1D_instance_41(.WCLK(CLK), .WE(wea_1), 
  .A0(ADDRA[0]), .A1(ADDRA[1]), .A2(ADDRA[2]), .A3(ADDRA[3]), .A4(ADDRA[4]), .A5(ADDRA[5]), 
  .D(DINA[9]), .SPO(pre_douta_1[9]), .DPRA0(ADDRB[0]), .DPRA1(ADDRB[1]), .DPRA2(ADDRB[2]), 
  .DPRA3(ADDRB[3]), .DPRA4(ADDRB[4]), .DPRA5(ADDRB[5]), .DPO(pre_doutb_1[9]));
// Memory array element: COL: 1, ROW: 10
//  Initialization of Dual Port LUT ram now done through explicit
// parameter setting.
RAM64X1D#(.INIT(64'h0000000000000000))RAM64X1D_instance_42(.WCLK(CLK), .WE(wea_1), 
  .A0(ADDRA[0]), .A1(ADDRA[1]), .A2(ADDRA[2]), .A3(ADDRA[3]), .A4(ADDRA[4]), .A5(ADDRA[5]), 
  .D(DINA[10]), .SPO(pre_douta_1[10]), .DPRA0(ADDRB[0]), .DPRA1(ADDRB[1]), .DPRA2(ADDRB[2]), 
  .DPRA3(ADDRB[3]), .DPRA4(ADDRB[4]), .DPRA5(ADDRB[5]), .DPO(pre_doutb_1[10]));
// Memory array element: COL: 1, ROW: 11
//  Initialization of Dual Port LUT ram now done through explicit
// parameter setting.
RAM64X1D#(.INIT(64'h0000000000000000))RAM64X1D_instance_43(.WCLK(CLK), .WE(wea_1), 
  .A0(ADDRA[0]), .A1(ADDRA[1]), .A2(ADDRA[2]), .A3(ADDRA[3]), .A4(ADDRA[4]), .A5(ADDRA[5]), 
  .D(DINA[11]), .SPO(pre_douta_1[11]), .DPRA0(ADDRB[0]), .DPRA1(ADDRB[1]), .DPRA2(ADDRB[2]), 
  .DPRA3(ADDRB[3]), .DPRA4(ADDRB[4]), .DPRA5(ADDRB[5]), .DPO(pre_doutb_1[11]));
// Memory array element: COL: 1, ROW: 12
//  Initialization of Dual Port LUT ram now done through explicit
// parameter setting.
RAM64X1D#(.INIT(64'h0000000000000000))RAM64X1D_instance_44(.WCLK(CLK), .WE(wea_1), 
  .A0(ADDRA[0]), .A1(ADDRA[1]), .A2(ADDRA[2]), .A3(ADDRA[3]), .A4(ADDRA[4]), .A5(ADDRA[5]), 
  .D(DINA[12]), .SPO(pre_douta_1[12]), .DPRA0(ADDRB[0]), .DPRA1(ADDRB[1]), .DPRA2(ADDRB[2]), 
  .DPRA3(ADDRB[3]), .DPRA4(ADDRB[4]), .DPRA5(ADDRB[5]), .DPO(pre_doutb_1[12]));
// Memory array element: COL: 1, ROW: 13
//  Initialization of Dual Port LUT ram now done through explicit
// parameter setting.
RAM64X1D#(.INIT(64'h0000000000000000))RAM64X1D_instance_45(.WCLK(CLK), .WE(wea_1), 
  .A0(ADDRA[0]), .A1(ADDRA[1]), .A2(ADDRA[2]), .A3(ADDRA[3]), .A4(ADDRA[4]), .A5(ADDRA[5]), 
  .D(DINA[13]), .SPO(pre_douta_1[13]), .DPRA0(ADDRB[0]), .DPRA1(ADDRB[1]), .DPRA2(ADDRB[2]), 
  .DPRA3(ADDRB[3]), .DPRA4(ADDRB[4]), .DPRA5(ADDRB[5]), .DPO(pre_doutb_1[13]));
// Memory array element: COL: 1, ROW: 14
//  Initialization of Dual Port LUT ram now done through explicit
// parameter setting.
RAM64X1D#(.INIT(64'h0000000000000000))RAM64X1D_instance_46(.WCLK(CLK), .WE(wea_1), 
  .A0(ADDRA[0]), .A1(ADDRA[1]), .A2(ADDRA[2]), .A3(ADDRA[3]), .A4(ADDRA[4]), .A5(ADDRA[5]), 
  .D(DINA[14]), .SPO(pre_douta_1[14]), .DPRA0(ADDRB[0]), .DPRA1(ADDRB[1]), .DPRA2(ADDRB[2]), 
  .DPRA3(ADDRB[3]), .DPRA4(ADDRB[4]), .DPRA5(ADDRB[5]), .DPO(pre_doutb_1[14]));
// Memory array element: COL: 1, ROW: 15
//  Initialization of Dual Port LUT ram now done through explicit
// parameter setting.
RAM64X1D#(.INIT(64'h0000000000000000))RAM64X1D_instance_47(.WCLK(CLK), .WE(wea_1), 
  .A0(ADDRA[0]), .A1(ADDRA[1]), .A2(ADDRA[2]), .A3(ADDRA[3]), .A4(ADDRA[4]), .A5(ADDRA[5]), 
  .D(DINA[15]), .SPO(pre_douta_1[15]), .DPRA0(ADDRB[0]), .DPRA1(ADDRB[1]), .DPRA2(ADDRB[2]), 
  .DPRA3(ADDRB[3]), .DPRA4(ADDRB[4]), .DPRA5(ADDRB[5]), .DPO(pre_doutb_1[15]));
// Memory array element: COL: 1, ROW: 16
//  Initialization of Dual Port LUT ram now done through explicit
// parameter setting.
RAM64X1D#(.INIT(64'h0000000000000000))RAM64X1D_instance_48(.WCLK(CLK), .WE(wea_1), 
  .A0(ADDRA[0]), .A1(ADDRA[1]), .A2(ADDRA[2]), .A3(ADDRA[3]), .A4(ADDRA[4]), .A5(ADDRA[5]), 
  .D(DINA[16]), .SPO(pre_douta_1[16]), .DPRA0(ADDRB[0]), .DPRA1(ADDRB[1]), .DPRA2(ADDRB[2]), 
  .DPRA3(ADDRB[3]), .DPRA4(ADDRB[4]), .DPRA5(ADDRB[5]), .DPO(pre_doutb_1[16]));
// Memory array element: COL: 1, ROW: 17
//  Initialization of Dual Port LUT ram now done through explicit
// parameter setting.
RAM64X1D#(.INIT(64'h0000000000000000))RAM64X1D_instance_49(.WCLK(CLK), .WE(wea_1), 
  .A0(ADDRA[0]), .A1(ADDRA[1]), .A2(ADDRA[2]), .A3(ADDRA[3]), .A4(ADDRA[4]), .A5(ADDRA[5]), 
  .D(DINA[17]), .SPO(pre_douta_1[17]), .DPRA0(ADDRB[0]), .DPRA1(ADDRB[1]), .DPRA2(ADDRB[2]), 
  .DPRA3(ADDRB[3]), .DPRA4(ADDRB[4]), .DPRA5(ADDRB[5]), .DPO(pre_doutb_1[17]));
// Memory array element: COL: 1, ROW: 18
//  Initialization of Dual Port LUT ram now done through explicit
// parameter setting.
RAM64X1D#(.INIT(64'h0000000000000000))RAM64X1D_instance_50(.WCLK(CLK), .WE(wea_1), 
  .A0(ADDRA[0]), .A1(ADDRA[1]), .A2(ADDRA[2]), .A3(ADDRA[3]), .A4(ADDRA[4]), .A5(ADDRA[5]), 
  .D(DINA[18]), .SPO(pre_douta_1[18]), .DPRA0(ADDRB[0]), .DPRA1(ADDRB[1]), .DPRA2(ADDRB[2]), 
  .DPRA3(ADDRB[3]), .DPRA4(ADDRB[4]), .DPRA5(ADDRB[5]), .DPO(pre_doutb_1[18]));
// Memory array element: COL: 1, ROW: 19
//  Initialization of Dual Port LUT ram now done through explicit
// parameter setting.
RAM64X1D#(.INIT(64'h0000000000000000))RAM64X1D_instance_51(.WCLK(CLK), .WE(wea_1), 
  .A0(ADDRA[0]), .A1(ADDRA[1]), .A2(ADDRA[2]), .A3(ADDRA[3]), .A4(ADDRA[4]), .A5(ADDRA[5]), 
  .D(DINA[19]), .SPO(pre_douta_1[19]), .DPRA0(ADDRB[0]), .DPRA1(ADDRB[1]), .DPRA2(ADDRB[2]), 
  .DPRA3(ADDRB[3]), .DPRA4(ADDRB[4]), .DPRA5(ADDRB[5]), .DPO(pre_doutb_1[19]));
// Memory array element: COL: 1, ROW: 20
//  Initialization of Dual Port LUT ram now done through explicit
// parameter setting.
RAM64X1D#(.INIT(64'h0000000000000000))RAM64X1D_instance_52(.WCLK(CLK), .WE(wea_1), 
  .A0(ADDRA[0]), .A1(ADDRA[1]), .A2(ADDRA[2]), .A3(ADDRA[3]), .A4(ADDRA[4]), .A5(ADDRA[5]), 
  .D(DINA[20]), .SPO(pre_douta_1[20]), .DPRA0(ADDRB[0]), .DPRA1(ADDRB[1]), .DPRA2(ADDRB[2]), 
  .DPRA3(ADDRB[3]), .DPRA4(ADDRB[4]), .DPRA5(ADDRB[5]), .DPO(pre_doutb_1[20]));
// Memory array element: COL: 1, ROW: 21
//  Initialization of Dual Port LUT ram now done through explicit
// parameter setting.
RAM64X1D#(.INIT(64'h0000000000000000))RAM64X1D_instance_53(.WCLK(CLK), .WE(wea_1), 
  .A0(ADDRA[0]), .A1(ADDRA[1]), .A2(ADDRA[2]), .A3(ADDRA[3]), .A4(ADDRA[4]), .A5(ADDRA[5]), 
  .D(DINA[21]), .SPO(pre_douta_1[21]), .DPRA0(ADDRB[0]), .DPRA1(ADDRB[1]), .DPRA2(ADDRB[2]), 
  .DPRA3(ADDRB[3]), .DPRA4(ADDRB[4]), .DPRA5(ADDRB[5]), .DPO(pre_doutb_1[21]));
// Memory array element: COL: 1, ROW: 22
//  Initialization of Dual Port LUT ram now done through explicit
// parameter setting.
RAM64X1D#(.INIT(64'h0000000000000000))RAM64X1D_instance_54(.WCLK(CLK), .WE(wea_1), 
  .A0(ADDRA[0]), .A1(ADDRA[1]), .A2(ADDRA[2]), .A3(ADDRA[3]), .A4(ADDRA[4]), .A5(ADDRA[5]), 
  .D(DINA[22]), .SPO(pre_douta_1[22]), .DPRA0(ADDRB[0]), .DPRA1(ADDRB[1]), .DPRA2(ADDRB[2]), 
  .DPRA3(ADDRB[3]), .DPRA4(ADDRB[4]), .DPRA5(ADDRB[5]), .DPO(pre_doutb_1[22]));
// Memory array element: COL: 1, ROW: 23
//  Initialization of Dual Port LUT ram now done through explicit
// parameter setting.
RAM64X1D#(.INIT(64'h0000000000000000))RAM64X1D_instance_55(.WCLK(CLK), .WE(wea_1), 
  .A0(ADDRA[0]), .A1(ADDRA[1]), .A2(ADDRA[2]), .A3(ADDRA[3]), .A4(ADDRA[4]), .A5(ADDRA[5]), 
  .D(DINA[23]), .SPO(pre_douta_1[23]), .DPRA0(ADDRB[0]), .DPRA1(ADDRB[1]), .DPRA2(ADDRB[2]), 
  .DPRA3(ADDRB[3]), .DPRA4(ADDRB[4]), .DPRA5(ADDRB[5]), .DPO(pre_doutb_1[23]));
// Memory array element: COL: 1, ROW: 24
//  Initialization of Dual Port LUT ram now done through explicit
// parameter setting.
RAM64X1D#(.INIT(64'h0000000000000000))RAM64X1D_instance_56(.WCLK(CLK), .WE(wea_1), 
  .A0(ADDRA[0]), .A1(ADDRA[1]), .A2(ADDRA[2]), .A3(ADDRA[3]), .A4(ADDRA[4]), .A5(ADDRA[5]), 
  .D(DINA[24]), .SPO(pre_douta_1[24]), .DPRA0(ADDRB[0]), .DPRA1(ADDRB[1]), .DPRA2(ADDRB[2]), 
  .DPRA3(ADDRB[3]), .DPRA4(ADDRB[4]), .DPRA5(ADDRB[5]), .DPO(pre_doutb_1[24]));
// Memory array element: COL: 1, ROW: 25
//  Initialization of Dual Port LUT ram now done through explicit
// parameter setting.
RAM64X1D#(.INIT(64'h0000000000000000))RAM64X1D_instance_57(.WCLK(CLK), .WE(wea_1), 
  .A0(ADDRA[0]), .A1(ADDRA[1]), .A2(ADDRA[2]), .A3(ADDRA[3]), .A4(ADDRA[4]), .A5(ADDRA[5]), 
  .D(DINA[25]), .SPO(pre_douta_1[25]), .DPRA0(ADDRB[0]), .DPRA1(ADDRB[1]), .DPRA2(ADDRB[2]), 
  .DPRA3(ADDRB[3]), .DPRA4(ADDRB[4]), .DPRA5(ADDRB[5]), .DPO(pre_doutb_1[25]));
// Memory array element: COL: 1, ROW: 26
//  Initialization of Dual Port LUT ram now done through explicit
// parameter setting.
RAM64X1D#(.INIT(64'h0000000000000000))RAM64X1D_instance_58(.WCLK(CLK), .WE(wea_1), 
  .A0(ADDRA[0]), .A1(ADDRA[1]), .A2(ADDRA[2]), .A3(ADDRA[3]), .A4(ADDRA[4]), .A5(ADDRA[5]), 
  .D(DINA[26]), .SPO(pre_douta_1[26]), .DPRA0(ADDRB[0]), .DPRA1(ADDRB[1]), .DPRA2(ADDRB[2]), 
  .DPRA3(ADDRB[3]), .DPRA4(ADDRB[4]), .DPRA5(ADDRB[5]), .DPO(pre_doutb_1[26]));
// Memory array element: COL: 1, ROW: 27
//  Initialization of Dual Port LUT ram now done through explicit
// parameter setting.
RAM64X1D#(.INIT(64'h0000000000000000))RAM64X1D_instance_59(.WCLK(CLK), .WE(wea_1), 
  .A0(ADDRA[0]), .A1(ADDRA[1]), .A2(ADDRA[2]), .A3(ADDRA[3]), .A4(ADDRA[4]), .A5(ADDRA[5]), 
  .D(DINA[27]), .SPO(pre_douta_1[27]), .DPRA0(ADDRB[0]), .DPRA1(ADDRB[1]), .DPRA2(ADDRB[2]), 
  .DPRA3(ADDRB[3]), .DPRA4(ADDRB[4]), .DPRA5(ADDRB[5]), .DPO(pre_doutb_1[27]));
// Memory array element: COL: 1, ROW: 28
//  Initialization of Dual Port LUT ram now done through explicit
// parameter setting.
RAM64X1D#(.INIT(64'h0000000000000000))RAM64X1D_instance_60(.WCLK(CLK), .WE(wea_1), 
  .A0(ADDRA[0]), .A1(ADDRA[1]), .A2(ADDRA[2]), .A3(ADDRA[3]), .A4(ADDRA[4]), .A5(ADDRA[5]), 
  .D(DINA[28]), .SPO(pre_douta_1[28]), .DPRA0(ADDRB[0]), .DPRA1(ADDRB[1]), .DPRA2(ADDRB[2]), 
  .DPRA3(ADDRB[3]), .DPRA4(ADDRB[4]), .DPRA5(ADDRB[5]), .DPO(pre_doutb_1[28]));
// Memory array element: COL: 1, ROW: 29
//  Initialization of Dual Port LUT ram now done through explicit
// parameter setting.
RAM64X1D#(.INIT(64'h0000000000000000))RAM64X1D_instance_61(.WCLK(CLK), .WE(wea_1), 
  .A0(ADDRA[0]), .A1(ADDRA[1]), .A2(ADDRA[2]), .A3(ADDRA[3]), .A4(ADDRA[4]), .A5(ADDRA[5]), 
  .D(DINA[29]), .SPO(pre_douta_1[29]), .DPRA0(ADDRB[0]), .DPRA1(ADDRB[1]), .DPRA2(ADDRB[2]), 
  .DPRA3(ADDRB[3]), .DPRA4(ADDRB[4]), .DPRA5(ADDRB[5]), .DPO(pre_doutb_1[29]));
// Memory array element: COL: 1, ROW: 30
//  Initialization of Dual Port LUT ram now done through explicit
// parameter setting.
RAM64X1D#(.INIT(64'h0000000000000000))RAM64X1D_instance_62(.WCLK(CLK), .WE(wea_1), 
  .A0(ADDRA[0]), .A1(ADDRA[1]), .A2(ADDRA[2]), .A3(ADDRA[3]), .A4(ADDRA[4]), .A5(ADDRA[5]), 
  .D(DINA[30]), .SPO(pre_douta_1[30]), .DPRA0(ADDRB[0]), .DPRA1(ADDRB[1]), .DPRA2(ADDRB[2]), 
  .DPRA3(ADDRB[3]), .DPRA4(ADDRB[4]), .DPRA5(ADDRB[5]), .DPO(pre_doutb_1[30]));
// Memory array element: COL: 1, ROW: 31
//  Initialization of Dual Port LUT ram now done through explicit
// parameter setting.
RAM64X1D#(.INIT(64'h0000000000000000))RAM64X1D_instance_63(.WCLK(CLK), .WE(wea_1), 
  .A0(ADDRA[0]), .A1(ADDRA[1]), .A2(ADDRA[2]), .A3(ADDRA[3]), .A4(ADDRA[4]), .A5(ADDRA[5]), 
  .D(DINA[31]), .SPO(pre_douta_1[31]), .DPRA0(ADDRB[0]), .DPRA1(ADDRB[1]), .DPRA2(ADDRB[2]), 
  .DPRA3(ADDRB[3]), .DPRA4(ADDRB[4]), .DPRA5(ADDRB[5]), .DPO(pre_doutb_1[31]));
endmodule



module median_structuralmemory_0e906503_(CLK_u0, bus_50eadaff_, bus_55d8c80f_, bus_5666c327_, bus_67045deb_, bus_14a66fca_, bus_08bdcda2_, bus_17cdf95b_, bus_307e6f2c_, bus_07eeda1f_, bus_3ec9115c_, bus_1df83f13_, bus_050f0d61_, bus_36b6dbe6_);
input		CLK_u0;
input		bus_50eadaff_;
input	[31:0]	bus_55d8c80f_;
input	[2:0]	bus_5666c327_;
input		bus_67045deb_;
input		bus_14a66fca_;
input	[31:0]	bus_08bdcda2_;
input	[31:0]	bus_17cdf95b_;
input	[2:0]	bus_307e6f2c_;
input		bus_07eeda1f_;
output	[31:0]	bus_3ec9115c_;
output		bus_1df83f13_;
output	[31:0]	bus_050f0d61_;
output		bus_36b6dbe6_;
wire		or_0d1ae73f_u0;
wire		not_192ce3e0_u0;
reg		logicalMem_1d2dc21d_we_delay0_u0=1'h0;
wire	[31:0]	bus_473ff4a8_;
wire	[31:0]	bus_0adedd71_;
wire		and_059eb142_u0;
wire		or_58a9ebf7_u0;
assign or_0d1ae73f_u0=bus_67045deb_|bus_14a66fca_;
assign not_192ce3e0_u0=~bus_14a66fca_;
always @(posedge CLK_u0 or posedge bus_50eadaff_)
begin
if (bus_50eadaff_)
logicalMem_1d2dc21d_we_delay0_u0<=1'h0;
else logicalMem_1d2dc21d_we_delay0_u0<=bus_14a66fca_;
end
median_forge_memory_101x32_1 median_forge_memory_101x32_1_instance0(.CLK(CLK_u0), 
  .ENA(or_0d1ae73f_u0), .WEA(bus_14a66fca_), .DINA(bus_08bdcda2_), .ADDRA(bus_55d8c80f_), 
  .DOUTA(bus_473ff4a8_), .DONEA(), .ENB(bus_07eeda1f_), .ADDRB(bus_17cdf95b_), .DOUTB(bus_0adedd71_), 
  .DONEB());
assign and_059eb142_u0=bus_67045deb_&not_192ce3e0_u0;
assign or_58a9ebf7_u0=and_059eb142_u0|logicalMem_1d2dc21d_we_delay0_u0;
assign bus_3ec9115c_=bus_473ff4a8_;
assign bus_1df83f13_=or_58a9ebf7_u0;
assign bus_050f0d61_=bus_0adedd71_;
assign bus_36b6dbe6_=bus_07eeda1f_;
endmodule



module median_scheduler(CLK, RESET, GO, port_6d827604_, port_5152fb59_, port_28a828b3_, port_7dd939ef_, port_2fb4bf16_, port_0c8374e8_, DONE, RESULT, RESULT_u14, RESULT_u15, RESULT_u16);
input		CLK;
input		RESET;
input		GO;
input		port_6d827604_;
input	[31:0]	port_5152fb59_;
input		port_28a828b3_;
input		port_7dd939ef_;
input		port_2fb4bf16_;
input		port_0c8374e8_;
output		DONE;
output		RESULT;
output		RESULT_u14;
output		RESULT_u15;
output		RESULT_u16;
wire		and_u18_u0;
wire		lessThan;
wire signed	[31:0]	lessThan_a_signed;
wire signed	[31:0]	lessThan_b_signed;
wire signed	[31:0]	equals_a_signed;
wire		equals;
wire signed	[31:0]	equals_b_signed;
wire signed	[31:0]	equals_u0_a_signed;
wire		equals_u0;
wire signed	[31:0]	equals_u0_b_signed;
wire		and_u19_u0;
wire		and_u20_u0;
wire		not_u3_u0;
wire		andOp;
wire		and_u21_u0;
wire		not_u4_u0;
wire		and_u22_u0;
wire		simplePinWrite;
wire		and_u23_u0;
wire		and_u24_u0;
wire		equals_u1;
wire signed	[31:0]	equals_u1_a_signed;
wire signed	[31:0]	equals_u1_b_signed;
wire		and_u25_u0;
wire		not_u5_u0;
wire		and_u26_u0;
wire		andOp_u0;
wire		not_u6_u0;
wire		and_u27_u0;
wire		and_u28_u0;
wire		simplePinWrite_u2;
wire		and_u29_u0;
wire		and_u30_u0;
wire		not_u7_u0;
wire		not_u8_u0;
wire		and_u31_u0;
wire		and_u32_u0;
wire		simplePinWrite_u3;
wire		and_u33_u0;
wire		or_u9_u0;
reg		reg_5b79c8a4_u0=1'h0;
wire		and_u34_u0;
wire		or_u10_u0;
wire		and_u35_u0;
reg		reg_42a8a07e_u0=1'h0;
wire		and_u36_u0;
wire		or_u11_u0;
reg		reg_612b7c86_u0=1'h0;
wire		and_u37_u0;
wire		and_u38_u0;
wire		mux_u20;
wire		or_u12_u0;
wire		or_u13_u0;
reg		reg_45e412dd_u0=1'h0;
wire		and_u39_u0;
wire		and_u40_u0;
wire		or_u14_u0;
wire		mux_u21_u0;
wire		receive_go_merge;
wire		or_u15_u0;
reg		loopControl_u1=1'h0;
reg		syncEnable_u21=1'h0;
reg		reg_3b2104ca_u0=1'h0;
reg		reg_3b2104ca_result_delayed_u0=1'h0;
wire		mux_u22_u0;
wire		or_u16_u0;
assign and_u18_u0=or_u15_u0&or_u15_u0;
assign lessThan_a_signed=port_5152fb59_;
assign lessThan_b_signed=32'h65;
assign lessThan=lessThan_a_signed<lessThan_b_signed;
assign equals_a_signed=port_5152fb59_;
assign equals_b_signed=32'h65;
assign equals=equals_a_signed==equals_b_signed;
assign equals_u0_a_signed={31'b0, port_6d827604_};
assign equals_u0_b_signed=32'h0;
assign equals_u0=equals_u0_a_signed==equals_u0_b_signed;
assign and_u19_u0=and_u18_u0&not_u3_u0;
assign and_u20_u0=and_u18_u0&equals_u0;
assign not_u3_u0=~equals_u0;
assign andOp=lessThan&port_28a828b3_;
assign and_u21_u0=and_u24_u0&andOp;
assign not_u4_u0=~andOp;
assign and_u22_u0=and_u24_u0&not_u4_u0;
assign simplePinWrite=and_u23_u0&{1{and_u23_u0}};
assign and_u23_u0=and_u21_u0&and_u24_u0;
assign and_u24_u0=and_u20_u0&and_u18_u0;
assign equals_u1_a_signed={31'b0, port_6d827604_};
assign equals_u1_b_signed=32'h1;
assign equals_u1=equals_u1_a_signed==equals_u1_b_signed;
assign and_u25_u0=and_u18_u0&equals_u1;
assign not_u5_u0=~equals_u1;
assign and_u26_u0=and_u18_u0&not_u5_u0;
assign andOp_u0=lessThan&port_28a828b3_;
assign not_u6_u0=~andOp_u0;
assign and_u27_u0=and_u39_u0&andOp_u0;
assign and_u28_u0=and_u39_u0&not_u6_u0;
assign simplePinWrite_u2=and_u37_u0&{1{and_u37_u0}};
assign and_u29_u0=and_u38_u0&equals;
assign and_u30_u0=and_u38_u0&not_u7_u0;
assign not_u7_u0=~equals;
assign not_u8_u0=~port_7dd939ef_;
assign and_u31_u0=and_u35_u0&port_7dd939ef_;
assign and_u32_u0=and_u35_u0&not_u8_u0;
assign simplePinWrite_u3=and_u33_u0&{1{and_u33_u0}};
assign and_u33_u0=and_u31_u0&and_u35_u0;
assign or_u9_u0=port_0c8374e8_|reg_5b79c8a4_u0;
always @(posedge CLK or posedge RESET)
begin
if (RESET)
reg_5b79c8a4_u0<=1'h0;
else reg_5b79c8a4_u0<=and_u34_u0;
end
assign and_u34_u0=and_u32_u0&and_u35_u0;
assign or_u10_u0=or_u9_u0|reg_42a8a07e_u0;
assign and_u35_u0=and_u29_u0&and_u38_u0;
always @(posedge CLK or posedge RESET)
begin
if (RESET)
reg_42a8a07e_u0<=1'h0;
else reg_42a8a07e_u0<=and_u36_u0;
end
assign and_u36_u0=and_u30_u0&and_u38_u0;
assign or_u11_u0=reg_612b7c86_u0|or_u10_u0;
always @(posedge CLK or posedge RESET)
begin
if (RESET)
reg_612b7c86_u0<=1'h0;
else reg_612b7c86_u0<=and_u37_u0;
end
assign and_u37_u0=and_u27_u0&and_u39_u0;
assign and_u38_u0=and_u28_u0&and_u39_u0;
assign mux_u20=(and_u37_u0)?1'h1:1'h0;
assign or_u12_u0=and_u37_u0|and_u33_u0;
assign or_u13_u0=or_u11_u0|reg_45e412dd_u0;
always @(posedge CLK or posedge RESET)
begin
if (RESET)
reg_45e412dd_u0<=1'h0;
else reg_45e412dd_u0<=and_u40_u0;
end
assign and_u39_u0=and_u25_u0&and_u18_u0;
assign and_u40_u0=and_u26_u0&and_u18_u0;
assign or_u14_u0=and_u23_u0|or_u12_u0;
assign mux_u21_u0=(and_u23_u0)?1'h1:mux_u20;
assign receive_go_merge=simplePinWrite|simplePinWrite_u2;
assign or_u15_u0=reg_3b2104ca_result_delayed_u0|loopControl_u1;
always @(posedge CLK or posedge syncEnable_u21)
begin
if (syncEnable_u21)
loopControl_u1<=1'h0;
else loopControl_u1<=or_u13_u0;
end
always @(posedge CLK)
begin
if (reg_3b2104ca_result_delayed_u0)
syncEnable_u21<=RESET;
end
always @(posedge CLK or posedge RESET)
begin
if (RESET)
reg_3b2104ca_u0<=1'h0;
else reg_3b2104ca_u0<=GO;
end
always @(posedge CLK or posedge RESET)
begin
if (RESET)
reg_3b2104ca_result_delayed_u0<=1'h0;
else reg_3b2104ca_result_delayed_u0<=reg_3b2104ca_u0;
end
assign mux_u22_u0=(GO)?1'h0:mux_u21_u0;
assign or_u16_u0=GO|or_u14_u0;
assign DONE=1'h0;
assign RESULT=or_u16_u0;
assign RESULT_u14=mux_u22_u0;
assign RESULT_u15=receive_go_merge;
assign RESULT_u16=simplePinWrite_u3;
endmodule



module median_receive(CLK, RESET, GO, port_35ec2878_, port_65355017_, port_38ee27c6_, DONE, RESULT, RESULT_u17, RESULT_u18, RESULT_u19, RESULT_u20, RESULT_u21, RESULT_u22);
input		CLK;
input		RESET;
input		GO;
input	[31:0]	port_35ec2878_;
input		port_65355017_;
input	[7:0]	port_38ee27c6_;
output		DONE;
output		RESULT;
output	[31:0]	RESULT_u17;
output		RESULT_u18;
output	[31:0]	RESULT_u19;
output	[31:0]	RESULT_u20;
output	[2:0]	RESULT_u21;
output		RESULT_u22;
wire		simplePinWrite;
wire	[31:0]	add;
reg		reg_0102bfe1_u0=1'h0;
wire		and_u41_u0;
wire		or_u17_u0;
wire	[31:0]	add_u10;
reg		reg_2d4cf0cd_u0=1'h0;
assign simplePinWrite=GO&{1{GO}};
assign add=port_35ec2878_+32'h0;
always @(posedge CLK or posedge GO or posedge or_u17_u0)
begin
if (or_u17_u0)
reg_0102bfe1_u0<=1'h0;
else if (GO)
reg_0102bfe1_u0<=1'h1;
else reg_0102bfe1_u0<=reg_0102bfe1_u0;
end
assign and_u41_u0=reg_0102bfe1_u0&port_65355017_;
assign or_u17_u0=and_u41_u0|RESET;
assign add_u10=port_35ec2878_+32'h1;
always @(posedge CLK or posedge RESET)
begin
if (RESET)
reg_2d4cf0cd_u0<=1'h0;
else reg_2d4cf0cd_u0<=GO;
end
assign DONE=reg_2d4cf0cd_u0;
assign RESULT=GO;
assign RESULT_u17=add_u10;
assign RESULT_u18=GO;
assign RESULT_u19=add;
assign RESULT_u20={24'b0, port_38ee27c6_};
assign RESULT_u21=3'h1;
assign RESULT_u22=simplePinWrite;
endmodule



module median_endianswapper_6d8848f8_(endianswapper_6d8848f8_in, endianswapper_6d8848f8_out);
input		endianswapper_6d8848f8_in;
output		endianswapper_6d8848f8_out;
assign endianswapper_6d8848f8_out=endianswapper_6d8848f8_in;
endmodule



module median_endianswapper_142d1895_(endianswapper_142d1895_in, endianswapper_142d1895_out);
input		endianswapper_142d1895_in;
output		endianswapper_142d1895_out;
assign endianswapper_142d1895_out=endianswapper_142d1895_in;
endmodule



module median_stateVar_fsmState_median(bus_474ddb5c_, bus_2fdc1657_, bus_3273b19d_, bus_54ecd834_, bus_49a1e211_);
input		bus_474ddb5c_;
input		bus_2fdc1657_;
input		bus_3273b19d_;
input		bus_54ecd834_;
output		bus_49a1e211_;
wire		endianswapper_6d8848f8_out;
wire		endianswapper_142d1895_out;
reg		stateVar_fsmState_median_u0=1'h0;
median_endianswapper_6d8848f8_ median_endianswapper_6d8848f8__1(.endianswapper_6d8848f8_in(stateVar_fsmState_median_u0), 
  .endianswapper_6d8848f8_out(endianswapper_6d8848f8_out));
median_endianswapper_142d1895_ median_endianswapper_142d1895__1(.endianswapper_142d1895_in(bus_54ecd834_), 
  .endianswapper_142d1895_out(endianswapper_142d1895_out));
always @(posedge bus_474ddb5c_ or posedge bus_2fdc1657_)
begin
if (bus_2fdc1657_)
stateVar_fsmState_median_u0<=1'h0;
else if (bus_3273b19d_)
stateVar_fsmState_median_u0<=endianswapper_142d1895_out;
end
assign bus_49a1e211_=endianswapper_6d8848f8_out;
endmodule



module median_endianswapper_101b9171_(endianswapper_101b9171_in, endianswapper_101b9171_out);
input	[31:0]	endianswapper_101b9171_in;
output	[31:0]	endianswapper_101b9171_out;
assign endianswapper_101b9171_out=endianswapper_101b9171_in;
endmodule



module median_endianswapper_72f31bd8_(endianswapper_72f31bd8_in, endianswapper_72f31bd8_out);
input	[31:0]	endianswapper_72f31bd8_in;
output	[31:0]	endianswapper_72f31bd8_out;
assign endianswapper_72f31bd8_out=endianswapper_72f31bd8_in;
endmodule



module median_stateVar_i(bus_5872f0b5_, bus_3585a683_, bus_6bbc6583_, bus_3caddcb4_, bus_2efc8353_, bus_7e5894da_, bus_1cb1b5e7_);
input		bus_5872f0b5_;
input		bus_3585a683_;
input		bus_6bbc6583_;
input	[31:0]	bus_3caddcb4_;
input		bus_2efc8353_;
input	[31:0]	bus_7e5894da_;
output	[31:0]	bus_1cb1b5e7_;
reg	[31:0]	stateVar_i_u0=32'h0;
wire	[31:0]	mux_67f86782_u0;
wire	[31:0]	endianswapper_101b9171_out;
wire	[31:0]	endianswapper_72f31bd8_out;
wire		or_5297352b_u0;
always @(posedge bus_5872f0b5_ or posedge bus_3585a683_)
begin
if (bus_3585a683_)
stateVar_i_u0<=32'h0;
else if (or_5297352b_u0)
stateVar_i_u0<=endianswapper_101b9171_out;
end
assign bus_1cb1b5e7_=endianswapper_72f31bd8_out;
assign mux_67f86782_u0=(bus_6bbc6583_)?bus_3caddcb4_:32'h0;
median_endianswapper_101b9171_ median_endianswapper_101b9171__1(.endianswapper_101b9171_in(mux_67f86782_u0), 
  .endianswapper_101b9171_out(endianswapper_101b9171_out));
median_endianswapper_72f31bd8_ median_endianswapper_72f31bd8__1(.endianswapper_72f31bd8_in(stateVar_i_u0), 
  .endianswapper_72f31bd8_out(endianswapper_72f31bd8_out));
assign or_5297352b_u0=bus_6bbc6583_|bus_2efc8353_;
endmodule


