-- ----------------------------------------------------------------------------
-- __  ___ __ ___  _ __   ___  ___ 
-- \ \/ / '__/ _ \| '_ \ / _ \/ __|
--  >  <| | | (_) | | | | (_) \__ \
-- /_/\_\_|  \___/|_| |_|\___/|___/
-- ----------------------------------------------------------------------------
-- Xronos synthesizer
-- Top level Network: rowMedian 
-- Date: 2018/02/23 11:32:26
-- ----------------------------------------------------------------------------

-- ----------------------------------------------------------------------------
-- Clock Domain(s) Information on the Network "rowMedian"
--
-- Network input port(s) clock domain:
--	in1 --> CLK
-- Network output port(s) clock domain:
-- 	out1 --> CLK
-- Actor(s) clock domains:
--	median (median) --> CLK

library ieee;
library SystemBuilder;

use ieee.std_logic_1164.all;

-- ----------------------------------------------------------------------------
-- Entity Declaration
-- ----------------------------------------------------------------------------
entity rowMedian is
port(
	 -- XDF Network Input(s)
	 in1_data : in std_logic_vector(7 downto 0);
	 in1_send : in std_logic;
	 in1_ack : out std_logic;
	 in1_rdy : out std_logic;
	 in1_count : in std_logic_vector(15 downto 0);
	 -- XDF Network Output(s)
	 out1_data : out std_logic_vector(7 downto 0);
	 out1_send : out std_logic;
	 out1_ack : in std_logic;
	 out1_rdy : in std_logic;
	 out1_count : out std_logic_vector(15 downto 0);
	 -- Clock(s) and Reset
	 CLK : in std_logic;
	 RESET : in std_logic);
end entity rowMedian;

-- ----------------------------------------------------------------------------
-- Architecture Declaration
-- ----------------------------------------------------------------------------
architecture rtl of rowMedian is
	-- --------------------------------------------------------------------------
	-- Internal Signals
	-- --------------------------------------------------------------------------

	-- Clock(s) and Reset signal
	signal clocks, resets: std_logic_vector(0 downto 0);

	-- Network Input Port(s)
	signal ni_in1_data : std_logic_vector(7 downto 0);
	signal ni_in1_send : std_logic;
	signal ni_in1_ack : std_logic;
	signal ni_in1_rdy : std_logic;
	signal ni_in1_count : std_logic_vector(15 downto 0);
	
	-- Network Input Port Fanout(s)
	signal nif_in1_data : std_logic_vector(7 downto 0);
	signal nif_in1_send : std_logic_vector(0 downto 0);
	signal nif_in1_ack : std_logic_vector(0 downto 0);
	signal nif_in1_rdy : std_logic_vector(0 downto 0);
	signal nif_in1_count : std_logic_vector(15 downto 0);
	
	-- Network Output Port(s) 
	signal no_out1_data : std_logic_vector(7 downto 0);
	signal no_out1_send : std_logic;
	signal no_out1_ack : std_logic;
	signal no_out1_rdy : std_logic;
	signal no_out1_count : std_logic_vector(15 downto 0);
	
	-- Actors Input/Output and Output fanout signals
	signal ai_median_in1_data : std_logic_vector(7 downto 0);
	signal ai_median_in1_send : std_logic;
	signal ai_median_in1_ack : std_logic;
	signal ai_median_in1_count : std_logic_vector(15 downto 0);
	
	signal ao_median_median_data : std_logic_vector(7 downto 0);
	signal ao_median_median_send : std_logic;
	signal ao_median_median_ack : std_logic;
	signal ao_median_median_rdy : std_logic;
	signal ao_median_median_count : std_logic_vector(15 downto 0);
	
	signal aof_median_median_data : std_logic_vector(7 downto 0);
	signal aof_median_median_send : std_logic_vector(0 downto 0);
	signal aof_median_median_ack : std_logic_vector(0 downto 0);
	signal aof_median_median_rdy : std_logic_vector(0 downto 0);
	signal aof_median_median_count : std_logic_vector(15 downto 0);

	-- --------------------------------------------------------------------------
	-- Network Instances
	-- --------------------------------------------------------------------------
	component median is
	port(
	     -- Instance median Input(s)
	     in1_data : in std_logic_vector(7 downto 0);
	     in1_send : in std_logic;
	     in1_ack : out std_logic;
	     in1_count : in std_logic_vector(15 downto 0);
	     -- Instance median Output(s)
	     median_data : out std_logic_vector(7 downto 0);
	     median_send : out std_logic;
	     median_ack : in std_logic;
	     median_rdy : in std_logic;
	     median_count : out std_logic_vector(15 downto 0);
	     clk: in std_logic;
	     reset: in std_logic);
	end component median;
	

begin
	-- Reset Controller
	rcon: entity SystemBuilder.resetController(behavioral)
	generic map(count => 1)
	port map( 
	         clocks => clocks, 
	         reset_in => reset, 
	         resets => resets);
	
	clocks(0) <= CLK;

	-- --------------------------------------------------------------------------
	-- Actor instances
	-- --------------------------------------------------------------------------
	i_median : component median
	port map(
		-- Instance median Input(s)
		in1_data => ai_median_in1_data,
		in1_send => ai_median_in1_send,
		in1_ack => ai_median_in1_ack,
		in1_count => ai_median_in1_count,
		-- Instance median Output(s)
		median_data => ao_median_median_data,
		median_send => ao_median_median_send,
		median_ack => ao_median_median_ack,
		median_rdy => ao_median_median_rdy,
		median_count => ao_median_median_count,
		-- Clock and Reset
	clk => clocks(0),
	--clk => clocks(0),
	reset => resets(0));
	
	-- --------------------------------------------------------------------------
	-- Nework Input Fanouts
	-- --------------------------------------------------------------------------
	f_ni_in1 : entity SystemBuilder.Fanout(behavioral)
	generic map (fanout => 1, width => 8)
	port map(
		-- Fanout In
		In_data => ni_in1_data,
		In_send => ni_in1_send,
		In_ack => ni_in1_ack,
		In_rdy => ni_in1_rdy,
		In_count => ni_in1_count,
		-- Fanout Out
		Out_data => nif_in1_data,
		Out_send => nif_in1_send,
		Out_ack => nif_in1_ack,
		Out_rdy => nif_in1_rdy,
		Out_count => nif_in1_count,
		-- Clock & Reset
		clk => clocks(0),
		reset => resets(0));

	-- --------------------------------------------------------------------------
	-- Actor Output Fanouts
	-- --------------------------------------------------------------------------
	f_ao_median_median : entity SystemBuilder.Fanout(behavioral)
	generic map (fanout => 1, width => 8)
	port map(
		-- Fanout In
		In_data => ao_median_median_data,
		In_send => ao_median_median_send,
		In_ack => ao_median_median_ack,
		In_rdy => ao_median_median_rdy,
		In_count => ao_median_median_count,
		-- Fanout Out
		Out_data => aof_median_median_data,
		Out_send => aof_median_median_send,
		Out_ack => aof_median_median_ack,
		Out_rdy => aof_median_median_rdy,
		Out_count => aof_median_median_count,
		-- Clock & Reset
		clk => clocks(0),
		reset => resets(0));

	-- --------------------------------------------------------------------------
	-- Queues
	-- --------------------------------------------------------------------------
	q_ai_median_in1 : entity SystemBuilder.Queue(behavioral)
	generic map (length => 4096, width => 8)
	port map(
		-- Queue Out
		Out_data => ai_median_in1_data,
		Out_send => ai_median_in1_send,
		Out_ack => ai_median_in1_ack,
		Out_count => ai_median_in1_count,
		-- Queue In
		In_data => nif_in1_data,
		In_send => nif_in1_send(0),
		In_ack => nif_in1_ack(0),
		In_rdy => nif_in1_rdy(0),
		In_count => nif_in1_count,
		-- Clock & Reset
		clk => clocks(0),
		reset => resets(0)
	);
	
	q_no_out1 : entity SystemBuilder.Queue(behavioral)
	generic map (length => 64, width => 8)
	port map(
		-- Queue Out
		Out_data => no_out1_data,
		Out_send => no_out1_send,
		Out_ack => no_out1_ack,
		Out_count => no_out1_count,
		-- Queue In
		In_data => aof_median_median_data,
		In_send => aof_median_median_send(0),
		In_ack => aof_median_median_ack(0),
		In_rdy => aof_median_median_rdy(0),
		In_count => aof_median_median_count,
		-- Clock & Reset
		clk => clocks(0),
		reset => resets(0)
	);

	-- --------------------------------------------------------------------------
	-- Network port(s) instantiation
	-- --------------------------------------------------------------------------
	
	-- Output Port(s) Instantiation
	out1_data <= no_out1_data;
	out1_send <= no_out1_send;
	no_out1_ack <= out1_ack;
	no_out1_rdy <= out1_rdy;
	out1_count <= no_out1_count;
	
	-- Input Port(s) Instantiation
	ni_in1_data <= in1_data;
	ni_in1_send <= in1_send;
	in1_ack <= ni_in1_ack;
	in1_rdy <= ni_in1_rdy;
	ni_in1_count <= in1_count;
end architecture rtl;
-- ----------------------------------------------------------------------------
-- ----------------------------------------------------------------------------
-- ----------------------------------------------------------------------------
