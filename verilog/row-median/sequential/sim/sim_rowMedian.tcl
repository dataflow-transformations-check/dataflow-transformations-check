## ############################################################################
## __  ___ __ ___  _ __   ___  ___ 
## \ \/ / '__/ _ \| '_ \ / _ \/ __|
##  >  <| | | (_) | | | | (_) \__ \
## /_/\_\_|  \___/|_| |_|\___/|___/
## ############################################################################
## Xronos synthesizer
## Simulation Launch TCL Script file for Network: rowMedian 
## Date: 2018/02/23 11:32:26
## ############################################################################

## Set paths
set Lib "../lib/"
set Rtl "../rtl"
set RtlGoDone "../rtl/rtlGoDone"

## Create SystemBuilder design library
vlib SystemBuilder
vmap SystemBuilder SystemBuilder

## Compile the SystemBuilder Library from sources
vcom -reportprogress 300 -work SystemBuilder $Lib/systemBuilder/vhdl/sbtypes.vhdl
vcom -reportprogress 300 -work SystemBuilder $Lib/systemBuilder/vhdl/sbfifo.vhdl
vcom -reportprogress 300 -work SystemBuilder $Lib/systemBuilder/vhdl/sbfifo_behavioral.vhdl

## Create the work design library
if {[file exist work]} {vdel -all -lib work}
vlib work
vmap work work

## Compile the glbl constans given by Xilinx 
vlog -work work ../lib/simulation/glbl.v


## Compile network instances and add them to work library	
vlog -work work $Rtl/median.v

## Compile the Top Network
vcom -93 -check_synthesis -quiet -work work $Rtl/rowMedian.vhd


## Start VSIM
vsim -voptargs="+acc" -L unisims_ver -L simprims_ver -t ns work.glbl work.rowMedian
	
## Add clock(s) and reset signal
add wave -noupdate -divider -height 20 "CLK & RESET"

add wave sim:/rowMedian/CLK
	add wave sim:/rowMedian/RESET
	
	
	## Change radix to decimal
	radix -decimal


add wave -noupdate -divider -height 20 no_out1
add wave sim:/rowMedian/out1_DATA
add wave sim:/rowMedian/out1_SEND
add wave sim:/rowMedian/out1_ACK
add wave sim:/rowMedian/out1_RDY
## Freeze ACK and RDY at 1
force -freeze sim:/rowMedian/out1_ACK 1 0
force -freeze sim:/rowMedian/out1_RDY 1 0



add wave -noupdate -divider -height 20 i_median
add wave sim:/rowMedian/i_median/CLK
add wave sim:/rowMedian/i_median/in1_DATA
add wave sim:/rowMedian/i_median/in1_ACK
add wave sim:/rowMedian/i_median/in1_SEND
add wave sim:/rowMedian/i_median/median_DATA
add wave sim:/rowMedian/i_median/median_ACK
add wave sim:/rowMedian/i_median/median_SEND
add wave sim:/rowMedian/i_median/median_RDY

## FIFO FULL
add wave -noupdate -divider -height 20 "FIFO FULL"
add wave -label median_in1_full sim:/rowMedian/q_ai_median_in1/full
add wave -label median_in1_almost_full sim:/rowMedian/q_ai_median_in1/almost_full


