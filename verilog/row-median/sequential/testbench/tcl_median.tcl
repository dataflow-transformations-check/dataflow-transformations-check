## ############################################################################
## __  ___ __ ___  _ __   ___  ___ 
## \ \/ / '__/ _ \| '_ \ / _ \/ __|
##  >  <| | | (_) | | | | (_) \__ \
## /_/\_\_|  \___/|_| |_|\___/|___/
## ############################################################################
## Xronos synthesizer
## Testbench TCL Script file for Actor: median 
## Date: 2018/02/23 11:32:25
## ############################################################################

## Set paths
set Lib "../lib/"
set LibSim "../lib/simulation"
set Rtl "../rtl"
set RtlGoDone "../rtl/rtlGoDone"
set Testbench "vhd"

## Create SystemBuilder design library
vlib SystemBuilder
vmap SystemBuilder SystemBuilder

## Compile the SystemBuilder Library from sources
vcom -reportprogress 300 -work SystemBuilder $Lib/systemBuilder/vhdl/sbtypes.vhdl
vcom -reportprogress 300 -work SystemBuilder $Lib/systemBuilder/vhdl/sbfifo.vhdl
vcom -reportprogress 300 -work SystemBuilder $Lib/systemBuilder/vhdl/sbfifo_behavioral.vhdl

## Create the work design library
if {[file exist work_median]} {vdel -all -lib work_median}
vlib work_median
vmap work_median work_median

## Compile the glbl constans given by Xilinx 
vlog -work work_median ../lib/simulation/glbl.v


# Compile sim package
vcom -93 -reportprogress 30 -work work_median $LibSim/sim_package.vhd
## Compile network instances and add them to work library	
vlog -work work_median $Rtl/median.v


## Compile the Testbench VHD
vcom -93 -check_synthesis -quiet -work work_median $Testbench/median_tb.vhd

## Start VSIM
vsim -voptargs="+acc" -L unisims_ver -L simprims_ver -t ns work_median.glbl work_median.median_tb
	
## Add clock(s) and reset signal
add wave -noupdate -divider -height 20 "CLK & RESET"

add wave sim:/median_tb/CLK
	add wave sim:/median_tb/RESET
	
	
	## Change radix to decimal
	radix -decimal

add wave -noupdate -divider -height 20  "Inputs: i_median"
add wave -label in1_DATA sim:/median_tb/i_median/in1_DATA
add wave -label in1_ACK sim:/median_tb/i_median/in1_ACK 
add wave -label in1_SEND sim:/median_tb/i_median/in1_SEND 
add wave -noupdate -divider -height 20 "Outputs: i_median"
add wave -label median_DATA sim:/median_tb/i_median/median_DATA 
add wave -label median_SEND sim:/median_tb/i_median/median_ACK
add wave -label median_SEND sim:/median_tb/i_median/median_SEND
add wave -label median_RDY sim:/median_tb/i_median/median_RDY
add wave -noupdate -divider -height 20 "Go & Done" 
