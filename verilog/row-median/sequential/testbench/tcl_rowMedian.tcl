## ############################################################################
## __  ___ __ ___  _ __   ___  ___ 
## \ \/ / '__/ _ \| '_ \ / _ \/ __|
##  >  <| | | (_) | | | | (_) \__ \
## /_/\_\_|  \___/|_| |_|\___/|___/
## ############################################################################
## Xronos synthesizer
## Testbench TCL Script file for Network: rowMedian 
## Date: 2018/02/23 11:32:26
## ############################################################################

## Set paths
set Lib "../lib/"
set LibSim "../lib/simulation"
set Rtl "../rtl"
set RtlGoDone "../rtl/rtlGoDone"
set Testbench "vhd"

## Create SystemBuilder design library
vlib SystemBuilder
vmap SystemBuilder SystemBuilder

## Compile the SystemBuilder Library from sources
vcom -reportprogress 300 -work SystemBuilder $Lib/systemBuilder/vhdl/sbtypes.vhdl
vcom -reportprogress 300 -work SystemBuilder $Lib/systemBuilder/vhdl/sbfifo.vhdl
vcom -reportprogress 300 -work SystemBuilder $Lib/systemBuilder/vhdl/sbfifo_behavioral.vhdl

## Create the work design library
if {[file exist work_rowMedian]} {vdel -all -lib work_rowMedian}
vlib work_rowMedian
vmap work_rowMedian work_rowMedian

## Compile the glbl constans given by Xilinx 
vlog -work work_rowMedian ../lib/simulation/glbl.v


# Compile sim package
vcom -93 -reportprogress 30 -work work_rowMedian $LibSim/sim_package.vhd
## Compile network instances and add them to work library	
vlog -work work_rowMedian $Rtl/median.v

## Compile the Top Network
vcom -93 -check_synthesis -quiet -work work_rowMedian $Rtl/rowMedian.vhd

## Compile the Testbench VHD
vcom -93 -check_synthesis -quiet -work work_rowMedian $Testbench/rowMedian_tb.vhd

## Start VSIM
vsim -voptargs="+acc" -L unisims_ver -L simprims_ver -t ns work_rowMedian.glbl work_rowMedian.rowMedian_tb
	
## Add clock(s) and reset signal
add wave -noupdate -divider -height 20 "CLK & RESET"

add wave sim:/rowMedian_tb/CLK
	add wave sim:/rowMedian_tb/RESET
	
	
	## Change radix to decimal
	radix -decimal

add wave -noupdate -divider -height 20 ni_in1
add wave sim:/rowMedian_tb/in1_DATA
add wave sim:/rowMedian_tb/in1_ACK
add wave sim:/rowMedian_tb/in1_SEND

add wave -noupdate -divider -height 20 no_out1
add wave sim:/rowMedian_tb/out1_DATA
add wave sim:/rowMedian_tb/out1_SEND
add wave sim:/rowMedian_tb/out1_ACK
add wave sim:/rowMedian_tb/out1_RDY



add wave -noupdate -divider -height 20 i_median
add wave sim:/rowMedian_tb/i_rowMedian/i_median/CLK
add wave sim:/rowMedian_tb/i_rowMedian/i_median/in1_DATA
add wave sim:/rowMedian_tb/i_rowMedian/i_median/in1_ACK
add wave sim:/rowMedian_tb/i_rowMedian/i_median/in1_SEND
add wave sim:/rowMedian_tb/i_rowMedian/i_median/median_DATA
add wave sim:/rowMedian_tb/i_rowMedian/i_median/median_ACK
add wave sim:/rowMedian_tb/i_rowMedian/i_median/median_SEND
add wave sim:/rowMedian_tb/i_rowMedian/i_median/median_RDY

## FIFO FULL
add wave -noupdate -divider -height 20 "FIFO FULL"
add wave -label median_in1_full sim:/rowMedian_tb/i_rowMedian/q_ai_median_in1/full
add wave -label median_in1_almost_full sim:/rowMedian_tb/i_rowMedian/q_ai_median_in1/almost_full

